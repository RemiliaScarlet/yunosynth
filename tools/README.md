# tools

TODO: Write a description here

## How do I get set up?

TODO: Write installation instructions here

## Usage

TODO: Write usage instructions here

## Development

TODO: Write development instructions here

### Style info

I use a somewhat non-standard style for my code.

- Keep lines 118 characters or shorter.  Obviously sometimes you can't, but
  please try.  Use 80 or 115 characters for Markdown files, though.
- Please use pascalCase for variable and method names.  Use CamelCase for type
  names.  Use UPPER_SNAKE_CASE for constants.
- ALWAYS put parentheses around method parameters, except for these methods:
  `puts`, `pp`, `p`, `raise`, `sleep`, `spawn`, `loop`, and `exit`.
- Always the full `do |foo|...end` syntax with blocks, except when it's all on
  one line, then always use { and }.
- The type name for exceptions end with Error.  For example, `ExternalProgramError`.

### How do I contribute?

1. Go to <REPO URL HERE>
   and clone the Fossil repository.
2. Create a new branch for your feature.
3. Push locally to the new branch.
4. Create a [bundle](https://fossil-scm.org/home/help?cmd=bundle) with Fossil
   that contains your changes.
5. Get in contact with me.

## Contributors

* Remilia Scarlet - creator and maintainer
  * Homepage: [https://remilia.sdf.org/](https://remilia.sdf.org/)
  * Mastodon: [@MistressRemilia@social.sdf.org](https://social.sdf.org/@MistressRemilia)
  * Email: zremiliaz@postzeoz.jpz  My real address does not contain Z's

