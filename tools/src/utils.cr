require "libremiliacr"

module VgmConv::Utils
  extend self
  FULL_WIDTH_CHARS = [
    0x3040..0x309F,   # Hiragana
    0x30A0..0x30FF,   # Katakana
    0x31F0..0x31FF,   # Katakana Phonetic Extensions
    0x3200..0x32FF,   # Enclosed CJK Letters and Months
    0x1AFF0..0x1AFFF, # Kana Extended-B
    0x1B000..0x1B0FF, # Kana Supplement
    0x1B100..0x1B12F, # Kana Extended-A
    0x1B130..0x1B16F, # Small Kana Extension
    0x4E00..0x9FFF,   # CJK Unified Ideographs
    0x3000..0x303F,   # CJK Symbols and Punctuation
    0xFF01..0xFF60,   # Full-width alternates (Halfwidth and Fullwidth Forms)
    0xFFE0..0xFFE6,   # Full-width alternates (Halfwidth and Fullwidth Forms)
    0x1100..0x11FF,   # Hangul
    0x3130..0x318F,   # Hangul
    0xA960..0xA97F,   # Hangul
    0xD7B0..0xD7FF,   # Hangul
    0xF900..0xFAFF,   # CJK Compatibility Ideographs
    0x2F800..0x2FA1F, # CJK Compatibility Ideographs Supplement
    0x2F00..0x2FDF,   # CJK Radicals / Kangxi Radicals
    0x2E80..0x2EFF,   # CJK Radicals Supplement
    0x3400..0x4DBF,   # CJK Unified Ideographs Extension A
    0x20000..0x2A6DF, # CJK Unified Ideographs Extension B
    0x2A700..0x2B73F, # CJK Unified Ideographs Extension C
    0x2B740..0x2B81F, # CJK Unified Ideographs Extension D
    0x2B820..0x2CEAF, # CJK Unified Ideographs Extension E
    0x2CEB0..0x2EBEF, # CJK Unified Ideographs Extension F
    0x30000..0x3134F, # CJK Unified Ideographs Extension G
    0x31350..0x323AF  # CJK Unified Ideographs Extension H
  ]

  HALF_WIDTH_CHARS = [
    0xFF61..0xFFDC, # Half-width alternates (Halfwidth and Fullwidth Forms)
    0xFFE8..0xFFEE  # Half-width alternates (Halfwidth and Fullwidth Forms)
  ]

  # This is not _exactly_ like `wcswidth()` in POSIX, but is similar.
  # All CJK characters are treated as 2-columns wide, while others are
  # treated as 1-column wide.  This returns the width in columns as if
  # the string was rendered in a terminal.
  def wcswidth(str : String) : Int32
    total = 0
    str.each_char do |char|

      case
      when char.ascii? || HALF_WIDTH_CHARS.any? &.includes?(char.ord)
        total += 1
      when FULL_WIDTH_CHARS.any? &.includes?(char.ord)
        total += 2
      else
        # Treat as single width
        total += 1
      end
    end
    total
  end

  # If the width of `origStr`, as determined by `Utils.wcswidth`, is
  # less than `maxCols`, this returns `origStr` padded with spaces.
  # Otherwise it returns up to `maxCols` worth of columns of text out
  # of `origStr`.
  def getClippedWcStr(origStr : String, maxCols : Int32) : String
    total = 0
    String.build do |str|
      origStr.each_char do |char|
        case
        when char.ascii? || HALF_WIDTH_CHARS.any? &.includes?(char.ord)
          break if total + 1 > maxCols
          total += 1
        when FULL_WIDTH_CHARS.any? &.includes?(char.ord)
          break if total + 2 > maxCols
          total += 2
        else
          # Treat as single width
          break if total + 1 > maxCols
          total += 1
        end
        str << char
      end

      if total < maxCols
        (maxCols - total).times do |_|
          str << ' '
        end
      end
    end
  end

  # Constructs a new string using `field` such that, if it is longer than
  # `width`, it will be cut short and "..." will be appended.  If it is not
  # longer than `width`, then it will be printed and extra space will be added
  # so that it fills `width` characters on screen.
  #
  # This treats non-ascii characters as double-wide characters.
  @[AlwaysInline]
  def clipField(field : String, columns : Int32) : String
    fieldWidth = wcswidth(field) # This is the width in columns.
    if fieldWidth > columns
      "#{getClippedWcStr(field, columns - 3)}..."
    elsif fieldWidth != columns
      getClippedWcStr(field, columns)
    else
      field
    end
  end

  # Constructs a new string using `line` such that, if it is longer than
  # `width`, it will be cut short and "..." will be appended.  If it is not
  # longer than `width`, then it will be printed as-is.
  #
  # If `pad` is true, then `line` will be padded with spaces so that it fills 80
  # characters on screen.  Otherwise it will be returned verbatim.
  @[AlwaysInline]
  def clipLine(line : String, pad : Bool = false) : String
    clipField(line, 80).tap do |x|
      unless pad
        x.strip
      else
        x
      end
    end
  end

  def self.getShortenedLine(line : String, maxSize : Int32 = 80) : String
    return line if maxSize < 20
    if line.size > maxSize
      leftPart = maxSize.tdiv(2) - 3
      rightPart = -(leftPart + 3)
      "#{line[0..leftPart]}...#{line[rightPart..]}"
    else
      line

    end
  end
end
