#### VgmConv - Bulk VGM Converter
#### Copyright (C) 2023 Remilia Scarlet <remilia@posteo.jp>
####
#### This program is free software: you can redistribute it and/or
#### modify it under the terms of the GNU Affero General Public
#### License as published by the Free Software Foundation, either
#### version 3 of the License, or (at your option) any later version.
####
#### This program is distributed in the hope that it will be useful,
#### but WITHOUT ANY WARRANTY; without even the implied warranty of
#### MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#### Affero General Public License for more details.
####
#### You should have received a copy of the GNU Affero General Public License
#### along with this program.  If not, see <https://www.gnu.org/licenses/>.
require "compress/gzip"
require "zstd"
require "libremiliacr"
require "./utils"

####
#### VgmConv
####
#### The purpose of this program is to quickly bulk-convert VGM files from one
#### compression scheme to another.  For example, if you have a set of VGM files
#### and half of them are .vgb and the other half are .vgz, but you want them
#### all .vgb, you can use this to convert all VGMs that aren't compressed with
#### BZip2 to .vgb files.
####
#### This will auto rename VGM files so they have the correct extension.
####

{% begin %}
  {% if compare_versions(Zstd::VERSION, "1.2.0") <= 0 %}
    {% puts "Monkey patching the ZStandard bindings to fix a memory leak" %}
    class Zstd::Compress::IO
      def close : Nil
        return if @closed
        @closed = true
        write_loop Lib::ZstdEndDirective::ZstdEEnd
        @io.close if @sync_close
      ensure
        @ctx.close
      end
    end

    class Zstd::Decompress::IO
      def close : Nil
        return if @closed
        @closed = true
        @io.close if @sync_close
      ensure
        @ctx.close
      end
    end
  {% end %}
{% end %}

module VgmConv
  VERSION = "0.1.0"
  PROGRAM_LONG_NAME = "VgmConv"

  EXT_ZSTD         = ".vgzst"
  EXT_BZIP2        = ".vgb"
  EXT_GZIP         = ".vgz"
  EXT_UNCOMPRESSED = ".vgm"

  enum VgmOp
    Zstd
    Bzip2
    Gzip
    Uncompressed
  end

  # Command line arguments
  @@args : RemiLib::Args::ArgParser = RemiLib::Args::ArgParser.new(PROGRAM_LONG_NAME, VERSION)
  class_getter args

  # A `Job` is a file we intend to process.  The instance represents the file as
  # it is compressed/decompressed/renamed/whatever.
  class Job
    getter origKind : VgmOp
    getter origFilename : Path
    getter destKind : VgmOp
    getter destFilename : Path

    protected def initialize(@origFilename : Path, @origKind : VgmOp)
      @destKind = case
                  when VgmConv.args["zstd"].called? then VgmOp::Zstd
                  when VgmConv.args["bzip2"].called? then VgmOp::Bzip2
                  when VgmConv.args["gzip"].called? then VgmOp::Gzip
                  when VgmConv.args["uncompressed"].called? then VgmOp::Uncompressed
                  else raise "Don't know what to do with #{@origFilename}"
                  end

      baseName = @origFilename.basename
      dirName = @origFilename.dirname
      ext = @origFilename.extension
      baseName = baseName[..-(ext.size + 1)]
      @destFilename = case @destKind
                      in .zstd? then Path[dirName, "#{baseName}.vgzst"]
                      in .bzip2? then Path[dirName, "#{baseName}.vgb"]
                      in .gzip? then Path[dirName, "#{baseName}.vgz"]
                      in .uncompressed? then Path[dirName, "#{baseName}.vgm"]
                      end

      @origFilename = @origFilename.expand
      @destFilename = @destFilename.expand
    end

    # Attempt to create a new `Job` instance from the given file.  If it can't
    # be processed by this program, this returns `nil`.
    def self.maybeCreate(filename : Path) : Job?
      buf = Bytes.new(4)

      # We don't look at file extensions, we look at magic bytes.
      begin
        File.open(filename) do |file|
          # Check the kind of file based on the magic bytes.  If it's a
          # compressed file, decompress the first four bytes and check if it's a
          # compressed VGM.  Return a new Job if possible, or nil otheriwse.
          file.read_fully(buf)
          case
          when isVgm?(buf)
            if !VgmConv.args["uncompressed"].called? ||
               (VgmConv.args["uncompressed"].called? && filename.extension.downcase != ".vgm")
              return Job.new(filename, VgmOp::Uncompressed)
            end

          when isZstd?(buf)
            if !VgmConv.args["zstd"].called? ||
               (VgmConv.args["zstd"].called? && filename.extension.downcase != ".vgzst")
              file.rewind
              Zstd::Decompress::IO.open(file) do |zstd|
                zstd.read_fully(buf)
                if isVgm?(buf)
                  return Job.new(filename, VgmOp::Zstd)
                end
              end
            end

          when isBzip2?(buf)
            if !VgmConv.args["bzip2"].called? ||
               (VgmConv.args["bzip2"].called? && filename.extension.downcase != ".vgb")
              file.rewind
              RemiLib::Compression::BZip2::Reader.open(file) do |bz|
                bz.read_fully(buf)
                if isVgm?(buf)
                  return Job.new(filename, VgmOp::Bzip2)
                end
              end
            end

          when isGz?(buf)
            if !VgmConv.args["gzip"].called? ||
               (VgmConv.args["gzip"].called? && filename.extension.downcase != ".vgz")
              file.rewind
              Compress::Gzip::Reader.open(file) do |gzio|
                gzio.read_fully(buf)
                if isVgm?(buf)
                  return Job.new(filename, VgmOp::Gzip)
                end
              end
            end
          end
        end
      rescue Exception # We skip any files that causes errors while reading.
      end

      nil
    end

    private def self.isVgm?(buf : Bytes) : Bool
      # "VGM "
      buf[0] == 0x56 && buf[1] == 0x67 && buf[2] == 0x6D && buf[3] == 0x20
    end

    private def self.isZstd?(buf : Bytes) : Bool
      # ZStandard just has four specific bytes
      buf[0] == 0x28 && buf[1] == 0xB5 && buf[2] == 0x2f && buf[3] == 0xFD
    end

    private def self.isBzip2?(buf : Bytes) : Bool
      # "BZh" followed by 1 through 9 as ASCII
      buf[0] == 0x42 && buf[1] == 0x5A && buf[2] == 0x68 &&
        (buf[3] >= 0x31 && buf[3] <= 0x39)
    end

    private def self.isGz?(buf : Bytes) : Bool
      # 0x1F 0x8B followed by 0x08, where the 0x08 is for "DEFLATE"
      buf[0] == 0x1F && buf[1] == 0x8B && buf[2] == 0x08
    end

    def run : Nil
      case @origKind
      # Uncompressed -> something else
      in .uncompressed?
        case @destKind
        in .uncompressed?
          unless @origFilename == @destFilename
            # Rename
            File.copy(@origFilename, @destFilename)
            File.delete(@origFilename)
          end

        in .zstd?
          File.open(@origFilename, "rb") do |infile|
            File.open(@destFilename, "wb") do |outfile|
              Zstd::Compress::IO.open(outfile, 22) do |outcomp|
                IO.copy(infile, outcomp)
              end
            end
          end
          File.delete(@origFilename)

        in .bzip2?
          File.open(@origFilename, "rb") do |infile|
            File.open(@destFilename, "wb") do |outfile|
              RemiLib::Compression::BZip2::Writer.open(outfile, 9) do |outcomp|
                IO.copy(infile, outcomp)
              end
            end
          end
          File.delete(@origFilename)

        in .gzip?
          File.open(@origFilename, "rb") do |infile|
            File.open(@destFilename, "wb") do |outfile|
              Compress::Gzip::Writer.open(outfile, 9) do |outcomp|
                IO.copy(infile, outcomp)
              end
            end
          end
          File.delete(@origFilename)
        end

      # ZStandard -> something else
      in .zstd?
        case @destKind
        in .uncompressed?
          File.open(@origFilename, "rb") do |infile|
            File.open(@destFilename, "wb") do |outfile|
              Zstd::Decompress::IO.open(infile) do |incomp|
                IO.copy(incomp, outfile)
              end
            end
          end
          File.delete(@origFilename)

        in .zstd?
          raise "Should not have selected zstd -> zstd"

        in .bzip2?
          File.open(@origFilename, "rb") do |infile|
            File.open(@destFilename, "wb") do |outfile|
              Zstd::Decompress::IO.open(infile) do |incomp|
                RemiLib::Compression::BZip2::Writer.open(outfile, 9) do |outcomp|
                  IO.copy(incomp, outcomp)
                end
              end
            end
          end
          File.delete(@origFilename)

        in .gzip?
          File.open(@origFilename, "rb") do |infile|
            File.open(@destFilename, "wb") do |outfile|
              Zstd::Decompress::IO.open(infile) do |incomp|
                Compress::Gzip::Writer.open(outfile, Compress::Gzip::BEST_COMPRESSION) do |outcomp|
                  IO.copy(incomp, outcomp)
                end
              end
            end
          end
          File.delete(@origFilename)
        end

      # BZip2 -> something else
      in .bzip2?
        case @destKind
        in .uncompressed?
          File.open(@origFilename, "rb") do |infile|
            File.open(@destFilename, "wb") do |outfile|
              RemiLib::Compression::BZip2::Reader.open(infile) do |incomp|
                IO.copy(incomp, outfile)
              end
            end
          end
          File.delete(@origFilename)

        in .zstd?
          File.open(@origFilename, "rb") do |infile|
            File.open(@destFilename, "wb") do |outfile|
              RemiLib::Compression::BZip2::Reader.open(infile) do |incomp|
                Zstd::Compress::IO.open(outfile, level: 22) do |outcomp|
                  IO.copy(incomp, outcomp)
                end
              end
            end
          end
          File.delete(@origFilename)

        in .bzip2?
          raise "Should not have selected bzip2 -> bzip2"

        in .gzip?
          File.open(@origFilename, "rb") do |infile|
            File.open(@destFilename, "wb") do |outfile|
              RemiLib::Compression::BZip2::Reader.open(infile) do |incomp|
                Compress::Gzip::Writer.open(outfile, Compress::Gzip::BEST_COMPRESSION) do |outcomp|
                  IO.copy(incomp, outcomp)
                end
              end
            end
          end
          File.delete(@origFilename)
        end

      # GZip -> something else
      in .gzip?
        case @destKind
        in .uncompressed?
          File.open(@origFilename, "rb") do |infile|
            File.open(@destFilename, "wb") do |outfile|
              Compress::Gzip::Reader.open(infile) do |incomp|
                IO.copy(incomp, outfile)
              end
            end
          end
          File.delete(@origFilename)

        in .zstd?
          File.open(@origFilename, "rb") do |infile|
            File.open(@destFilename, "wb") do |outfile|
              Compress::Gzip::Reader.open(infile) do |incomp|
                Zstd::Compress::IO.open(outfile, level: 22) do |outcomp|
                  IO.copy(incomp, outcomp)
                end
              end
            end
          end
          File.delete(@origFilename)

        in .bzip2?
          File.open(@origFilename, "rb") do |infile|
            File.open(@destFilename, "wb") do |outfile|
              Compress::Gzip::Reader.open(infile) do |incomp|
                RemiLib::Compression::BZip2::Writer.open(outfile, 9) do |outcomp|
                  IO.copy(incomp, outcomp)
                end
              end
            end
          end
          File.delete(@origFilename)

        in .gzip?
          raise "Should not have selected gzip -> gzip"
        end
      end # case @origKind
    end # def run : Nil
  end

  # Main application class.
  class Program
    @jobs : Array(Job) = [] of Job
    @termWidth : UInt16 = 80u16


    def initialize
      ##
      ## Command Line Arguments
      ##
      VgmConv.args.addFlag("recurse", 'r', help: "Recurse subdirectories")
      VgmConv.args.addFlag("links", 'L', help: "Follow symlinks")
      VgmConv.args.addFlag("overwrite", 'O', help: "Overwrite existing files")
      VgmConv.args.addFlag("dry-run", 'n', help: "Don't modify anything, just print what the tool would do")
      VgmConv.args.addInt("num-workers", 'w',
                          minimum: 1i64,
                          maximum: Int32::MAX.to_i64!,
                          default: 2,
                          help: "Number of concurrent jobs (default: 2)")
      #VgmConv.args.addFlag("use-local-bzip2",
      #                     help: "Use the bzip2 command line program instead of the internal implementation")

      VgmConv.args.addFlag("zstd", 'z', group: "Formats", help: "Convert files to vgzst (ZStandard compression)")
      VgmConv.args.addFlag("bzip2", 'b', group: "Formats", help: "Convert files to vgb (BZip2 compression)")
      VgmConv.args.addFlag("gzip", 'g', group: "Formats", help: "Convert files to vgz (GZip compression)")
      VgmConv.args.addFlag("uncompressed", 'u', group: "Formats", help: "Convert files to vgm (uncompressed)")

      VgmConv.args.usageLine = "Usage: #{Path[PROGRAM_NAME].basename} [options] <directories...>"
      VgmConv.args.postHelpText = "Keep in mind that some compression schemes (e.g. ZStandard) can use quite a bit
of memory.  Keep this in mind when setting the number of workers."

      ##
      ## Initialization
      ##

      # Parse the command line
      VgmConv.args.parse

      # Check compression scheme arguments.
      compressionArgs = ["zstd", "bzip2", "gzip", "uncompressed"]
                          .map { |x| VgmConv.args[x].called? }
                          .count(&.itself)
      if compressionArgs > 1
        raise RemiLib::Args::ArgumentError.new("Only one compression scheme can be used")
      elsif compressionArgs == 0
        raise RemiLib::Args::ArgumentError.new("You must select a compression scheme")
      end

      # Temporary!
      if VgmConv.args["bzip2"].called?
        abort("BZip2 compression is temporarily disabled until some internal bugs are fixed.")
      end

      # # Check if we want to use the local bzip2 program.
      # if VgmConv.args["use-local-bzip2"].called?
      #   if prog = Process.find_executable("bzip2")
      #     VgmConv.bzip2Prog = prog
      #   else
      #     RemiLib.log.fatal("--use-local-bzip2 specified, but cannot find the bzip2 program")
      #   end

      #   unless VgmConv.args["bzip2"].called?
      #     RemiLib.log.warn("Ignoring --use-local-bzip2, not compressing to bzip2")
      #   end
      # end

      # Ensure we have some valid directories.
      if VgmConv.args.positionalArgs.empty?
        raise RemiLib::Args::ArgumentError.new("No directories specified")
      else
        VgmConv.args.positionalArgs.each do |dir|
          unless Dir.exists?(dir)
            raise RemiLib::Args::ArgumentError.new("Directory does not exist: #{dir}")
          end
        end
      end
    end

    # Collects names (as `Path` instances) of files we can potentially process.
    private def collectFiles(names : Array(String), base : Bool = false) : Array(Path)
      {% begin %}
        ret = [] of Path

        # Local handles on these so we don't call the #called? method so much.
        recurse = VgmConv.args["recurse"].called?
        followLinks = VgmConv.args["links"].called?

        # Update the terminal width if the OS supports it.
        {% if flag?(:unix) %}
          _, @termWidth = RemiLib::Console.getWinSize
        {% end %}

        # Look at each name, converting it to an absolute path first.
        names.each do |name|
          path = name.is_a?(Path) ? name : Path[name].expand

          if Dir.exists?(path) && (recurse || base)
            if !File.symlink?(path) || (File.symlink?(path) && followLinks)
              # Recurse into this path to find more files.
              ret += collectFiles(Dir.glob(Path[path, "*"]))
            end

          elsif File.exists?(path)
            if !File.symlink?(path) || (File.symlink?(path) && followLinks)
              # File found!
              ret << path
            end
          end
        end

        ret
      {% end %}
    end

    # Filters out a set of filenames, adding the ones that we are able to
    # process to @jobs.  The set of files is processed in parallel.
    private def filterFiles(files : Array(Path)) : Nil
      total = Atomic(UInt64).new(0u64)
      allSent = Atomic(UInt8).new(0u8)
      jobChan = Channel(Job).new(4)
      doneChan = Channel(Bool).new(1)

      # Update the terminal width if the OS supports it.
      {% if flag?(:unix) %}
        _, @termWidth = RemiLib::Console.getWinSize
      {% end %}

      # This fiber will collect the new Job instances.
      spawn do
        {% begin %}
          until total.get == 0 && allSent.get != 0
            select
            when msg = jobChan.receive
              @jobs << msg
              total.sub(1)
              RemiLib::Console.cursor(STDOUT, 0)
              RemiLib::Console.erase(STDOUT)
              STDOUT << Utils.getShortenedLine("Adding... #{msg.origFilename}", @termWidth)
            else
              sleep 10.milliseconds
            end
          end
          STDOUT << '\n'
          doneChan.send(true)
        {% end %}
      end

      # Process files in parallel using a job pool.
      pool = RemiLib::JobPool(Path).new(VgmConv.args["num-workers"].str.to_i32)
      pool.run(files) do |file|
        if newJob = Job.maybeCreate(file)
          # Only add jobs if they won't overwrite existing files, or if
          # --overwrite was used.
          if !File.exists?(newJob.destFilename) ||
             (File.exists?(newJob.destFilename) && VgmConv.args["overwrite"].called?)
            total.add(1)
            jobChan.send(newJob)
          end
        end
      end

      # Wait for things to finish.
      allSent.set(1)
      doneChan.receive
    end

    alias JobChannel = Channel(Int64|Bool|Exception)
    alias PrinterChannel = Channel(Tuple(Array(Exception), Int64))

    private def runPrinter(total, jobDoneChan : JobChannel, printerDoneChan : PrinterChannel) : Nil
      spawn do
        errs = [] of Exception
        sizeDifference : Int64 = 0
        RemiLib::Console.withProgress("Converting", total) do |bar|
          until bar.step == total
            select
            when result = jobDoneChan.receive
              case result
              in Exception
                errs << result
                bar.pump
                bar.label = "#{bar.step} / #{total}"
              in Bool
                break
              in Int64
                  bar.pump
                  bar.label = "#{bar.step} / #{total}"
                  sizeDifference += result
              end
              STDOUT.flush
            end
          end
        end
        printerDoneChan.send({errs, sizeDifference})
      end
    end

    def run
      # Find files to process and turn them into Jobs.
      STDOUT << "Looking for files files... "
      start = Time.monotonic
      newFiles = collectFiles(VgmConv.args.positionalArgs, true)
      format("done, discovered ~:d potential file~:p~%", newFiles.size)
      filterFiles(newFiles)

      # Make sure we have something to do.
      if @jobs.empty?
        puts "No files found matching the criteria"
        exit 0
      else
        format("  ~:d #{@jobs.size == 1 ? "is a" : "are"} VGM file~:p to process~%", @jobs.size)
      end

      if VgmConv.args["dry-run"].called?
        @jobs.each do |job|
          puts "#{job.origFilename} => #{job.destFilename}"
        end
        puts "=== Dry run finished ==="
        exit 0
      end

      jobChan = JobChannel.new(1)
      printerChan = PrinterChannel.new(1)
      pool = RemiLib::JobPool(Job).new(VgmConv.args["num-workers"].str.to_i32)
      format("Processing using ~:d worker~:p~%", pool.workers)

      runPrinter(@jobs.size, jobChan, printerChan)

      pool.run(@jobs) do |job|
        begin
          startSize = File.size(job.origFilename).to_i64!
          job.run
          jobChan.send(File.size(job.destFilename).to_i64! - startSize)
        rescue err : Exception
          jobChan.send(Exception.new("While processing #{job.origFilename}: #{err}"))
        end
        Fiber.yield
      end
      finish = Time.monotonic

      jobChan.send(true)
      errs, sizeDifference = printerChan.receive
      unless errs.empty?
        errs.each { |err| RemiLib.log.error("#{err}") }
      end

      puts "Time taken: #{finish - start}"
      puts "Total size difference: #{sizeDifference.prettySize}"
    end
  end
end

begin
  VgmConv::Program.new.run
rescue err : RemiLib::Args::ArgumentError
  RemiLib.log.fatal("#{err}")
#rescue err : Exception
#  RemiLib.log.fatal(err)
end
