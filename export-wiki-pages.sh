#!/bin/bash
####
#### Exports all wiki pages found in the repository to the `./wiki` directory.
#### This directory will be created if it does not exist.  FILES ARE
#### OVERWRITTEN WITHOUT QUESTION.
####

set -e

# Check to see if the wiki directory exists
if [[ ! -d wiki ]]; then
   mkdir -v wiki
fi

# Parse command line
DOIMPORT=0
while [[ $# -gt 0 ]]; do
    case $1 in
        --import | -i)
            DOIMPORT=1
            ;;
        *)
            echo "Usage: `basename "$0"` [ --import / -i ]"
            exit 1
            ;;
    esac
    shift
done

OLDIFS=$IFS
IFS=$'\n'
PAGES=( $(fossil wiki list) )
IFS=$OLDIFS

if [[ $DOIMPORT -eq 1 ]]; then
    # See if the timestamp file exists
    if [[ -f wiki/TIMESTAMP ]]; then
        TIMESTAMP=$(cat wiki/TIMESTAMP)
    else
        TIMESTAMP=0
    fi

    for i in wiki/*; do
        # Skip the timestamp file
        if [[ $i == "TIMESTAMP" ]]; then
            continue
        fi

        # Has the file been updated?
        if [[ $(date -r "$i" +%s) -gt $TIMESTAMP ]]; then
            # Import it if we can
            origName="$(attr -q -g origpage $i || echo "")"
            if [[ "$origName" != "" ]]; then
                echo "Importing $i to $origName"
                fossil wiki commit "$origName" "$i"
            else
                echo "Original name not set for $i"
            fi
        fi
    done
else
    # Export all wiki pages
    for i in $(seq 0 ${#PAGES[@]} ); do
        name="${PAGES[i]}"
        if [[ $name == "" ]]; then
            continue
        fi

        filename="wiki/$(echo "$name" | tr ' ' '-' | tr '[:upper:]' '[:lower:]')"
        echo "Exporting \"$name\" -> $filename"
        fossil wiki export "$name" "$filename"
        attr -q -s origpage -V "$name" "$filename"
    done

    # Make the timestamp file
    date +%s > wiki/TIMESTAMP
fi
