require "../lib/libremiliacr/src/libremiliacr"
require "../src/yunosynth.cr"

if ARGV.size == 0
  puts "Usage: vgminfo <vgmfile>"
  exit 1
end

vgm = Yuno::VgmFile.load(ARGV[0])

format("Track: ~a
Track (jp): ~a
Game: ~a
Game (jp): ~a
System: ~a
System (jp): ~a
Author: ~a
Author (jp): ~a
Release Date: ~a
Creator: ~a
Notes: ~a

",
       vgm.gd3Tag.trackNameEn, vgm.gd3Tag.trackNameJp,
       vgm.gd3Tag.gameNameEn, vgm.gd3Tag.gameNameJp,
       vgm.gd3Tag.systemNameEn, vgm.gd3Tag.systemNameJp,
       vgm.gd3Tag.authorNameEn, vgm.gd3Tag.authorNameJp,
       vgm.gd3Tag.releaseDate, vgm.gd3Tag.creator,
       vgm.gd3Tag.notes)

puts "Total samples: #{vgm.header.totalSamples}"
puts "Samples per loop: #{vgm.header.loopSamples}"
printf("VGM Spec Version: $%04x\n", vgm.header.version)

STDOUT << "Chips used: "
player = Yuno::VgmPlayer.new(vgm, Yuno::VgmPlayer.minBufferSize(44100))
used = player.chipNamesUsed
used.each do |chip|
  STDOUT << "\n * " << chip
end
STDOUT << '\n'
