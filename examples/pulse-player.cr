require "../lib/libremiliacr/src/libremiliacr"
require "../src/yunosynth"

@[Link(ldflags: "`pkg-config libpulse-simple --libs`")]
lib Pulse
  alias PaSimple = Pointer(Void)

  enum PaSampleFormat
    PA_SAMPLE_U8
    PA_SAMPLE_ALAW
    PA_SAMPLE_ULAW
    PA_SAMPLE_S16LE
    PA_SAMPLE_S16BE
    PA_SAMPLE_FLOAT32LE
    PA_SAMPLE_FLOAT32BE
    PA_SAMPLE_S32LE
    PA_SAMPLE_S32BE
    PA_SAMPLE_S24LE
    PA_SAMPLE_S24BE
    PA_SAMPLE_S24_32LE
    PA_SAMPLE_S24_32BE
    PA_SAMPLE_MAX
    PA_SAMPLE_INVALID = -1
  end

  enum PaStreamDirection
    PA_STREAM_NODIRECTION
    PA_STREAM_PLAYBACK
    PA_STREAM_RECORD
    PA_STREAM_UPLOAD
  end

  enum PaChannelPosition
    PA_CHANNEL_POSITION_INVALID = -1
    PA_CHANNEL_POSITION_MONO = 0
    PA_CHANNEL_POSITION_FRONT_LEFT
    PA_CHANNEL_POSITION_FRONT_RIGHT
    PA_CHANNEL_POSITION_FRONT_CENTER
    PA_CHANNEL_POSITION_LEFT = PA_CHANNEL_POSITION_FRONT_LEFT
    PA_CHANNEL_POSITION_RIGHT = PA_CHANNEL_POSITION_FRONT_RIGHT
    PA_CHANNEL_POSITION_CENTER = PA_CHANNEL_POSITION_FRONT_CENTER
    PA_CHANNEL_POSITION_REAR_CENTER
    PA_CHANNEL_POSITION_REAR_LEFT
    PA_CHANNEL_POSITION_REAR_RIGHT
    PA_CHANNEL_POSITION_LFE
    PA_CHANNEL_POSITION_SUBWOOFER = PA_CHANNEL_POSITION_LFE
    PA_CHANNEL_POSITION_FRONT_LEFT_OF_CENTER
    PA_CHANNEL_POSITION_FRONT_RIGHT_OF_CENTER
    PA_CHANNEL_POSITION_SIDE_LEFT
    PA_CHANNEL_POSITION_SIDE_RIGHT
    PA_CHANNEL_POSITION_AUX0
    PA_CHANNEL_POSITION_AUX1
    PA_CHANNEL_POSITION_AUX2
    PA_CHANNEL_POSITION_AUX3
    PA_CHANNEL_POSITION_AUX4
    PA_CHANNEL_POSITION_AUX5
    PA_CHANNEL_POSITION_AUX6
    PA_CHANNEL_POSITION_AUX7
    PA_CHANNEL_POSITION_AUX8
    PA_CHANNEL_POSITION_AUX9
    PA_CHANNEL_POSITION_AUX10
    PA_CHANNEL_POSITION_AUX11
    PA_CHANNEL_POSITION_AUX12
    PA_CHANNEL_POSITION_AUX13
    PA_CHANNEL_POSITION_AUX14
    PA_CHANNEL_POSITION_AUX15
    PA_CHANNEL_POSITION_AUX16
    PA_CHANNEL_POSITION_AUX17
    PA_CHANNEL_POSITION_AUX18
    PA_CHANNEL_POSITION_AUX19
    PA_CHANNEL_POSITION_AUX20
    PA_CHANNEL_POSITION_AUX21
    PA_CHANNEL_POSITION_AUX22
    PA_CHANNEL_POSITION_AUX23
    PA_CHANNEL_POSITION_AUX24
    PA_CHANNEL_POSITION_AUX25
    PA_CHANNEL_POSITION_AUX26
    PA_CHANNEL_POSITION_AUX27
    PA_CHANNEL_POSITION_AUX28
    PA_CHANNEL_POSITION_AUX29
    PA_CHANNEL_POSITION_AUX30
    PA_CHANNEL_POSITION_AUX31
    PA_CHANNEL_POSITION_TOP_CENTER
    PA_CHANNEL_POSITION_TOP_FRONT_LEFT
    PA_CHANNEL_POSITION_TOP_FRONT_RIGHT
    PA_CHANNEL_POSITION_TOP_FRONT_CENTER
    PA_CHANNEL_POSITION_TOP_REAR_LEFT
    PA_CHANNEL_POSITION_TOP_REAR_RIGHT
    PA_CHANNEL_POSITION_TOP_REAR_CENTER
    PA_CHANNEL_POSITION_MAX
  end

  struct PaSampleSpec
    format : PaSampleFormat
    rate : UInt32
    channels : UInt8
  end

  struct PaChannelMap
    channels : UInt8
    map : PaChannelPosition[32]
  end

  struct PaBufferAttr
    maxLength : UInt32
    tLength : UInt32
    preBuf : UInt32
    minReq : UInt32
    fragSize : UInt32
  end

  fun pa_simple_new(server : LibC::Char*, name : LibC::Char*, dir : PaStreamDirection, dev : LibC::Char*,
                    streamName : LibC::Char*, spec : PaSampleSpec*, map : PaChannelMap*, attr : PaBufferAttr*,
                    error : LibC::Int*) : PaSimple
  fun pa_simple_free(simple : PaSimple) : Void
  fun pa_simple_write(simple : PaSimple, data : Void*, len : LibC::SizeT, err : LibC::Int*) : LibC::Int
  fun pa_simple_drain(simple : PaSimple, err : LibC::Int*) : LibC::Int
  fun pa_strerror(err : LibC::Int) : LibC::Char*
end

module PulsePlayer
  VERSION = "0.1.0"

  class Program

    @file : String
    @stream : Pulse::PaSimple = Pulse::PaSimple.null
    @loops : Int32 = -1
    @sampleRate : UInt32 = 44100_u32

    def initialize
      @file = ""

      case ARGV.size
      when 0
        STDERR << "No file given\n"
        exit 1
      when 1
        @file = ARGV[0]
      when 2
        @file = ARGV[0]
        @loops = ARGV[1].to_i32? || abort "Bad loop count"
      when 3
        @file = ARGV[0]
        @loops = ARGV[1].to_i32? || abort "Bad loop count"
        @sampleRate = ARGV[2].to_u32? || abort "Bad sample rate"
        if @sampleRate < 8000
          abort "Sample rate too low"
        end
      else
        STDERR << "Usage: pulse-player <filename> [loop count] [sample rate]\n"
        STDERR << %|

Loop Count can be 0 (loop indefinitely), positive (loop a specific amount of
times) or negative (never loop).  Note that VGMs that do not have loop
information will not be looped, regardless.  Default is to never loop.

Sample Rate is in hertz must be >= 8000.  Default is 44100.
|
      end

      if @file.empty? || !File.exists?(@file)
        abort "Cannot find that file"
      end
    end

    def run : Nil
      spec = Pulse::PaSampleSpec.new
      spec.format = Pulse::PaSampleFormat::PA_SAMPLE_FLOAT32LE
      spec.rate = @sampleRate.to_i32!
      spec.channels = 2
      @stream = Pulse.pa_simple_new(nil, PROGRAM_NAME, Pulse::PaStreamDirection::PA_STREAM_PLAYBACK,
                                    nil, # default device
                                    "VGM playback",
                                    pointerof(spec),
                                    nil, # Default channel map
                                    nil, # Default buffering attributes
                                    out err)

      if @stream.null?
        errStr : String = String.new(Pulse.pa_strerror(err))
        RemiLib.log.fatal("Could not initialize PulseAudio: #{errStr}")
      end

      # Load VGM
      vgm = Yuno::VgmFile.load(ARGV[0])
      format("Track: ~a
Track (jp): ~a
Game: ~a
Game (jp): ~a
System: ~a
System (jp): ~a
Author: ~a
Author (jp): ~a
Release Date: ~a
Creator: ~a
Notes: ~a

",
             vgm.gd3Tag.trackNameEn, vgm.gd3Tag.trackNameJp,
             vgm.gd3Tag.gameNameEn, vgm.gd3Tag.gameNameJp,
             vgm.gd3Tag.systemNameEn, vgm.gd3Tag.systemNameJp,
             vgm.gd3Tag.authorNameEn, vgm.gd3Tag.authorNameJp,
             vgm.gd3Tag.releaseDate, vgm.gd3Tag.creator,
             vgm.gd3Tag.notes)

      # Print VGM info
      unless vgm.validateChips
        puts "Error: This VGM uses chips that currently aren't supported by YunoSynth:"
        vgm.unsupportedChips.each do |ct|
          puts "  * #{ct}"
        end
        exit 1
      end

      settings = Yuno::VgmPlayerSettings.new
      settings.sampleRate = @sampleRate
      settings.ym2151Core = :mame # Change this to :nuked to try out the Nuked
      # core.  You'll almost certainly want to use
      # `rake pulse-player[speed]` for this.
      bufferSize = Yuno::VgmPlayer.minBufferSize(@sampleRate) * 8
      player = Yuno::VgmPlayer.new(vgm, bufferSize, settings)
      #player.play(true)
      player.play
      dest = Slice(Float32).new(bufferSize * 2, 0.0f32) # *2 to interleave left and right

      # Print the names of the chips that are used.
      STDOUT << "Chips used: "
      used = player.chipNamesUsed
      used.each_with_index do |chip, idx|
        STDOUT << chip
        STDOUT << ", " unless idx == used.size - 1
      end
      STDOUT << '\n'

      puts "Sample Rate: #{settings.sampleRate}"
      if vgm.hasLoopInfo?
        if @loops > 0
          format("Looping: ~:d time~:p~%", @loops)
        elsif @loops == 0
          puts "Looping: indefinitely"
        else
          puts "Looping: no"
        end
      else
        puts "Looping: no (VGM has no loop information)"
      end

      puts "VGM header volume modifier: #{player.volumeModifier}"
      puts "Total samples: #{vgm.header.totalSamples}"

      # We only loop forever if the VGM has looping information, and if @loops
      # is 0.
      loopForever = (@loops == 0 && vgm.hasLoopInfo?)

      # We now use @loops as an ending value below.  If it's negative (meaning
      # "never loop"), change @loops to 0 so that the looping works correctly.
      @loops = 0 if @loops < 0

      # Do the playback loop.  We either loop forever, or a specific number of
      # times.
      while loopForever || player.timesPlayed <= @loops
        # Render
        player.render(dest)

        # Send the interleaved audio to the audio device.
        writeBuffer(dest)
      end

      unless player.atEnd?
        # Keep "playing" for three additional seconds to fade out.
        #
        # needLoops is the sample rate divided by the buffer size, which should
        # give us just about one second worth of audio.
        puts "Fading..."

        # Get the total loops we'll need to do.  We use half the buffer size
        # here since the destination is interleaved data.
        secondsToFade : Int32 = 5
        needLoops : UInt32 = (settings.sampleRate // (dest.size >> 1)) * secondsToFade

        # Calculate our fade coefficient.
        targetDB : Float64 = -80.0 # in decibels
        samplesNeeded : Int32 = bufferSize * needLoops # one buffer full times the number of times we'll loop
        fadeCoeff : Float64 = 10 ** (0.05 * (targetDB / samplesNeeded)) # in linear
        volAdjust : Float64 = 1.0 # Initial starting multiplier

        # Continue rendering, fading out as we go.
        needLoops.times do |_|
          player.render(dest)

          # Fade out
          dest.map_with_index! do |x, idx|
            volAdjust *= fadeCoeff if idx.odd?
            x * volAdjust
          end

          # Exit early if we've reached silence.
          break if dest.all? &.==(0)

          # Send the data to the audio device.
          writeBuffer(dest)
        end
      end

      puts "Playback finished"
    end

    @[AlwaysInline]
    def writeBuffer(buf : Slice(Float32)) : Nil
      result = Pulse.pa_simple_write(@stream, buf.to_unsafe, buf.size * sizeof(Float32), out writeError)
      unless result == 0
        errStr = String.new(Pulse.pa_strerror(writeError))
        RemiLib.log.fatal("Could not write data to PulseAudio: #{errStr}")
      end
    end
  end
end

PulsePlayer::Program.new.run
