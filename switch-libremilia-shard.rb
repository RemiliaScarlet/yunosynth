#!/usr/bin/env ruby
####
#### This is a small utility script that can be used to quickly change the
#### libremiliacr Shard information in shard.yml.  It mainly exists just because
#### it's often quicker to run this than edit the file by hand if libremiliacr
#### work is being done.
####
require "optparse"
require "yaml"

URL = "https://chiselapp.com/user/MistressRemilia/repository/libremiliacr"

args = {:latest => false, :commit => nil, :version => nil, :tag => nil}
OptionParser.new do |opts|
  opts.banner = "Usage: switch-libremilia-shard.rb --latest
Usage: switch-libremilia-shard.rb --tag <tag>
Usage: switch-libremilia-shard.rb --commit <commit ID>
Usage: switch-libremilia-shard.rb --version <version>"

  opts.on("-l", "--latest", "Use latest version (path: ../libremilia)") do |_|
    args[:latest] = true
  end

  opts.on("-tTAG", "--tagTAG", "Use a specific tag") do |v|
    args[:tag] = v
  end

  opts.on("-cCOMMIT", "--commit=COMMIT", "Use specific commit") do |v|
    args[:commit] = v
  end

  opts.on("-vVERSION", "--version=VERSION", "Use version specifier") do |v|
    args[:version] = v
  end
end.parse!

data = YAML.load_file("shard.yml")

if args[:latest]
  abort "Cannot combine options" if args[:commit] || args[:version] || args[:tag]
  data["dependencies"]["libremiliacr"]["path"] = "../libremiliacr"
  data["dependencies"]["libremiliacr"].delete("fossil")
  data["dependencies"]["libremiliacr"].delete("commit")
  data["dependencies"]["libremiliacr"].delete("version")
  data["dependencies"]["libremiliacr"].delete("tag")
elsif args[:commit]
  abort "Cannot combine options" if args[:latest] || args[:version] || args[:tag]
  data = YAML.load_file("shard.yml")
  data["dependencies"]["libremiliacr"].delete("path")
  data["dependencies"]["libremiliacr"]["fossil"] = URL
  data["dependencies"]["libremiliacr"]["commit"] = args[:commit]
  data["dependencies"]["libremiliacr"].delete("version")
  data["dependencies"]["libremiliacr"].delete("tag")
elsif args[:version]
  abort "Cannot combine options" if args[:commit] || args[:latest] || args[:tag]
  data["dependencies"]["libremiliacr"].delete("path")
  data["dependencies"]["libremiliacr"]["fossil"] = URL
  data["dependencies"]["libremiliacr"].delete("commit")
  data["dependencies"]["libremiliacr"]["version"] = args[:version]
  data["dependencies"]["libremiliacr"].delete("tag")
elsif args[:tag]
  abort "Cannot combine options" if args[:commit] || args[:latest] || args[:version]
  data["dependencies"]["libremiliacr"].delete("path")
  data["dependencies"]["libremiliacr"]["fossil"] = URL
  data["dependencies"]["libremiliacr"].delete("commit")
  data["dependencies"]["libremiliacr"].delete("version")
  data["dependencies"]["libremiliacr"]["tag"] = args[:tag]
end

puts data.to_yaml
