# YunoSynth

YunoSynth is a high-performance [VGM](https://vgmrips.net/) playback library
written entirely in the [Crystal](https://crystal-lang.org/) programming
language.  The goal is to provide native VGM playback in Crystal without
bindings and almost no dependencies, a cleaned-up version of VGMPlay's code, and
performance on par with VGMPlay.

An example of a program that uses YunoSynth is
[Benben](https://chiselapp.com/user/MistressRemilia/repository/benben/), a
full-featured command line VGM player and VGM->WAV converter by the same author.

Wanna support this project? [Buy Remilia a coffee on Ko-Fi](https://ko-fi.com/L4L614QNC), or support her through Liberapay.

<a href='https://ko-fi.com/L4L614QNC' target='_blank'><img height='36' style='border:0px;height:36px;' src='https://cdn.ko-fi.com/cdn/kofi2.png?v=3' border='0' alt='Buy Me a Coffee at ko-fi.com' /></a>
<a href="https://liberapay.com/RemiliaScarlet/donate"><img alt="Donate using Liberapay" src="https://liberapay.com/assets/widgets/donate.svg"></a>

## Example videos

These were recorded using Benben.

* Sega Genesis/Mega Drive - Elemental Master - ["Like The Wind"](https://www.youtube.com/watch?v=C5BBMAXoaNo)
* MSX - Impact MuSiX Disk #2 - ["Ending 0.01"](https://www.youtube.com/watch?v=2CTEHr9jZAc)
* Sega Genesis/Mega Drive - Sonic the Hedgehog - ["Green Hill Zone"](https://www.youtube.com/watch?v=wACcjdCMSjc)
* Arcade - Toki - [Stage 1 music](https://www.youtube.com/watch?v=007OWk7mDsQ)
* Arcade - After Burner II - ["After Burner (Melody Version)"](https://www.youtube.com/watch?v=MsUECjuEU2w)
* Neo Geo - Metal Slug X - ["Judgement -X-"](https://www.youtube.com/watch?v=K6YXQpaaR14)
* X68000 - Granada - ["Advance 'GRANADA' (Opening Theme)"](https://www.youtube.com/watch?v=TxkB5BpGb_w)
* PC-98 - Rusty - ["Queen in The Night"](https://www.youtube.com/watch?v=pKPR-95oY3Q)
* PC Engine/TurboGrafx-16 - Soldier Blade - ["Operation 1"](https://www.youtube.com/watch?v=EWUp27Hx2Xo)

## Features

* Rendering of VGM files to PCM in multiple formats.
* High quality resampling.
* Compressed (gzip) VGM loading, as well as additional compression methods:
  * Support for VGMs that are compressed with BZip2 are also supported.
  * Support for VGMs that are compressed with ZStandard are also supported.
* Full GD3 tag support.
* DAC support.

### Implemented Chips

More chips will be added as time goes on.

* Bandai Wonderswan
* Capcom DL-1425 QSound
* Ensoniq ES5503
* General Instruments AY-1-8910 and similar
* Hudson HuC6280 (two different cores)
* Irem GA20
* Konami K051649
* Konami K054539
* Konami K053260
* Namco C140 / Namco 219 ASIC
* Namco C352
* NEC uPD7759
* Nintendo Entertainment System
* Nintendo Famicom Disk System
* Nintendo GameBoy (DMG)
* Nintendo Virtual Boy (VSU-VUE)
* OKI MSM6258
* OKI MSM6295
* Philips SAA1099
* Ricoh RF5C164
* Sega 32x PWM
* Sega SegaPCM
* Sega MultiPCM
* Seta X1-010
* Texas Instruments SN76489 and similar
* Yamaha Y8950
* Yamaha YM2151 (two different cores)
* Yamaha YM2203
* Yamaha YM2413/Konami VRC7
* Yamaha YM2608
* Yamaha YM2610/YM2610B
* Yamaha YM2612
* Yamaha YM3526 (OPL)
* Yamaha YM3812 (OPL2)
* Yamaha YMF262 (OPL3)
* Yamaha YMZ280B

## Usage

Add this to your `shard.yml`:

```yaml
dependencies:
  yunosynth:
    fossil: https://chiselapp.com/user/MistressRemilia/repository/yunosynth
```

You will need [Fossil](https://fossil-scm.org/) installed to clone this repo and
the dependency.  You will also need Shards v0.17.1 or later.  If you have an
earlier version of Shards, you will need to to build the [latest
version](https://github.com/crystal-lang/shards/) manually.

### Example Programs

To compile these example programs, first do a `shards update` in the repository
root.  Then you can compile debug builds of them using `rake`.  Run `rake
examples[list]` to see some optimization options.

* `examples/vgminfo.cr`: Prints GD3 tag information and some basic info about a VGM file.
* `examples/pulse-player.cr`: A basic VGM/VGZ player using PulseAudio for the
  backend.  Run it without any arguments to see its usage.

## Version Control

YunoSynth sources are managed using [Fossil](https://www.fossil-scm.org/), a
distributed version control system. The [Fossil
repository](https://chiselapp.com/user/MistressRemilia/repository/yunosynth/)
contains the urtext, tickets, and wiki.

If you are reading this on GitLab or some other Git repository or service, then
you are looking at a mirror. The names of check-ins and other artifacts in a Git
mirror are different from the official names for those objects. The official
names for check-ins are found in a footer on the check-in comment for authorized
mirrors. The official check-in name can also be seen in the manifest.uuid file
in the root of the tree. Always use the official name, not the Git-name, when
communicating about a YunoSynth check-in.

## Development

[Development Milestones](wiki?name=Milestones) are listed in the wiki.

If you want to add a new chip, please make sure to read [Adding
Chips](wiki?name=Adding+Chips) for notes and hints.

### Compile-time Flags

YunoSynth supports a few compile-time defines, which can be used by passing the
`-D` option to the compiler (for example, `-Dyunosynth_debug`).

* `yunosynth_debug`: Print various debug information to the console.
* `yunosynth_wd40`: Enable various micro-optimizations that may possibly do
  unsafe things in exchange for some small speed boosts.

### Style info

I use a somewhat non-standard style for my code.

- Keep lines 118 characters or shorter.  Obviously sometimes you can't, but
  please try your best.  Use 80 or 115 characters for Markdown files, though.
- Please use `pascalCase` for variable and method names, `CamelCase` for type
  names, and `UPPER_SNAKE_CASE` for constants.
- Put parentheses around method parameters, except for these methods: `puts`,
  `pp`, `p`, `raise`, `sleep`, `spawn`, `loop`, and `exit`.
- Always the full `do |foo|...end` syntax with blocks, except when it's all on
  one line or an anonymous function, then use either { and } or the normal `do
  |foo|...end`.
- The type name for exceptions end with `Error`.  For example,
  `ExternalProgramError`.
- Add type information everywhere you can.
- Uppercase hex literals except where 8 might be mistaken for a capital 'b'.
- No tabs, always spaces.

### How do I contribute?

1. Go to [https://chiselapp.com/user/MistressRemilia/repository/yunosynth](https://chiselapp.com/user/MistressRemilia/repository/yunosynth)
   and clone the Fossil repository.
2. Create a new branch for your feature.
3. Push locally to the new branch.
4. Create a [bundle](https://fossil-scm.org/home/help?cmd=bundle) with Fossil
   that contains your changes.
5. Get in contact with me.

## Contributors

* Remilia Scarlet - creator and maintainer
  * Homepage: [https://remilia.sdf.org/](https://remilia.sdf.org/)
  * Fediverse: [@remilia@nanako-さめ.mooo.com](https://nanako-さめ.mooo.com/@remilia)
  * Email: zremiliaz@postzeoz.jpz  My real address does not contain Z's

## Links and Licenses

YunoSynth itself is under the [GNU Affero General Public License version 3](https://www.gnu.org/licenses/agpl-3.0.html).

The emulation cores, which were all ported by hand to Crystal, have various
other licenses, which can be found in the [licenses
folder](https://chiselapp.com/user/MistressRemilia/repository/yunosynth/dir?ci=tip&name=licenses)
in this repository.  Most of them are from the [MAME](https://www.mamedev.org/)
project.

Much of the playback code is based on heavily modified code from
[VGMPlay](https://github.com/vgmrips/vgmplay/) by Valley Bell, et al.
