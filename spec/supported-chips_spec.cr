require "./spec_helper"

describe "Supported/Unsupported Chip Sets" do
  it "No chips are in both sets" do
    supported = Yuno.supportedChips
    unsupported = Yuno.unsupportedChips
    (supported - unsupported).should eq supported
    (unsupported - supported).should eq unsupported

    supported << Yuno::ChipType::Unknown # The Unknown type should be ignored
    (supported.size + unsupported.size).should eq Yuno::ChipType.values.size
  end
end
