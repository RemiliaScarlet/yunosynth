#### YunoSynth
#### Copyright (C) 2023-2024 Remilia Scarlet <remilia@posteo.jp>
#### Based on VGMPlay, Copyright (C) Valley Bell
####
#### This program is free software: you can redistribute it and/or
#### modify it under the terms of the GNU Affero General Public
#### License as published by the Free Software Foundation, either
#### version 3 of the License, or (at your option) any later version.
####
#### This program is distributed in the hope that it will be useful,
#### but WITHOUT ANY WARRANTY; without even the implied warranty of
#### MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#### Affero General Public License for more details.
####
#### You should have received a copy of the GNU Affero General Public License
#### along with this program.  If not, see <https://www.gnu.org/licenses/>.
require "../common"

module Yuno
  private class DacController
    private class Control
      property destChipType : ChipType = ChipType::Unknown
      property destChipIndex : UInt8 = 0
      property destCommand : UInt16 = 0
      property commandSize : UInt8 = 0

      # The frequency at which the commands are sent, in hertz.
      property frequency : UInt32 = 0

      property dataLen : UInt32 = 0
      property data : Bytes = Bytes.new(0)
      property dataStart : UInt32 = 0
      property stepSize : UInt8 = 0
      property stepBase : UInt8 = 0
      property commandsToSend : UInt32 = 0

      # Running Bits:  0 (01) - is playing
      #                2 (04) - loop sample (simple loop from start to end)
      #                4 (10) - already sent this command
      #                7 (80) - disabled
      property running : UInt8 = 0

      property reverse : UInt8 = 0
      property step : UInt32 = 0
      property pos : UInt32 = 0
      property remainingCommands : UInt32 = 0
      property realPos : UInt32 = 0
      property dataStep : UInt8 = 0

      def initialize
      end

      # Resets the instance to its initial state.
      def reset : Nil
        Yuno.dlog!("Resetting DacController::Control for #{@destChipType} index #{@destChipIndex}")
        @destCommand = 0
        @commandSize = 0
        @frequency = 0
        @dataLen = 0
        @data = Bytes.new(0)
        @dataStart = 0
        @stepSize = 0
        @stepBase = 0
        @running = 0
        @reverse = 0
        @step = 0
        @pos = 0
        @realPos = 0
        @remainingCommands = 0
      end

      # Returns `true` if the instance is running, or `false` otherwise.
      @[AlwaysInline]
      def running? : Bool
        !(Yuno.bitflag?(@running, 0x80))
      end
    end
  end
end
