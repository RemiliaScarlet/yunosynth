#### YunoSynth
#### Copyright (C) 2023-2024 Remilia Scarlet <remilia@posteo.jp>
#### Based on VGMPlay, Copyright (C) Valley Bell
####
#### This program is free software: you can redistribute it and/or
#### modify it under the terms of the GNU Affero General Public
#### License as published by the Free Software Foundation, either
#### version 3 of the License, or (at your option) any later version.
####
#### This program is distributed in the hope that it will be useful,
#### but WITHOUT ANY WARRANTY; without even the implied warranty of
#### MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#### Affero General Public License for more details.
####
#### You should have received a copy of the GNU Affero General Public License
#### along with this program.  If not, see <https://www.gnu.org/licenses/>.

module Yuno
  private class DacController
    private class BankTable
      property compressionType : UInt8 = 0
      property compressionSubType : UInt8 = 0
      property bitDec : UInt8 = 0
      property bitCmp : UInt8 = 0
      property entryCount : UInt16 = 0
      property entries : Bytes = Bytes.new(0)

      def initialize
      end
    end
  end
end
