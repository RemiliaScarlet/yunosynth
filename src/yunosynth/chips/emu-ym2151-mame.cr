#### YM2151 Emulator.
####
#### (c) 1997-2002 Jarek Burczynski (s0246@poczta.onet.pl, bujar@mame.net)
#### Some of the optimizing ideas by Tatsuyuki Satoh
#### Crystal port Copyright (C) 2023-2024 Remilia Scarlet
#### BSD 3-Clause License
####
#### Version 2.150 final beta May, 11th 2002
####
####
#### I would like to thank following people for making this project possible:
####
#### Beauty Planets - for making a lot of real YM2151 samples and providing
#### additional informations about the chip. Also for the time spent making
#### the samples and the speed of replying to my endless requests.
####
#### Shigeharu Isoda - for general help, for taking time to scan his YM2151
#### Japanese Manual first of all, and answering MANY of my questions.
####
#### Nao - for giving me some info about YM2151 and pointing me to Shigeharu.
#### Also for creating fmemu (which I still use to test the emulator).
####
#### Aaron Giles and Chris Hardy - they made some samples of one of my favourite
#### arcade games so I could compare it to my emulator.
####
#### Bryan McPhail and Tim (powerjaw) - for making some samples.
####
#### Ishmair - for the datasheet and motivation.
require "./emu-ym2151-mame-tables"

####
#### Yamaha YM2151 sound chip emulator internal interface.
####

module Yuno::Chips
  class YM2151 < AbstractChip
    private class YM2151Mame < Yuno::AbstractEmulator
      ###
      ### Internal classes
      ###

      class Operator
        # Accumulated operator phase.
        property phase : UInt32 = 0

        # Operator frequency count.
        property freq : UInt32 = 0

        # Current detune 1 phase increment/decrement value.
        property dt1 : Int32 = 0

        # Frequency count multiply.
        property mul : UInt32 = 0

        # DT1 index multiplied by 32.
        property dt1i : UInt32 = 0

        # Current detune 2 value.
        property dt2 : UInt32 = 0

        # Operator output "direction".
        property connect : Pointer(Int32) = Pointer(Int32).malloc(1, 0)

        ###
        ### M1 (operator 0) data.  Other operators don't use these.
        ###

        # Where to put the delayed sample.  Only M1 (operator 0) is filled with
        # this data.
        property memConnect : Pointer(Int32) = Pointer(Int32).malloc(1, 0)

        # Delayed sample value.
        property memValue : Int32 = 0

        ###
        ### Channel-specific data.  Note: Each operator number 0 contains
        ### channel specific data.
        ###

        # Feedback shift value for operator 0 in each channel.
        property fbShift : UInt32 = 0

        # Current operator feedback value (only used by operator 0)
        property fbOutCurrent : Int32 = 0

        # Previous operator feedback value (only used by operator 0)
        property fbOutPrev : Int32 = 0

        # Channel KC (copied to all operators)
        property kc : UInt32 = 0
        property kci : UInt32 = 768 # For speedup

        # Channel PMS.
        property pms : UInt32 = 0

        # Channel AMS.
        property ams : UInt32 = 0

        ###
        ### End of channel-specific data.
        ###

        # LFO amplitude modulation enable mask.
        property amMask : UInt32 = 0

        # Envelope state: 4 = attack(AR), 3 =decay(D1R), 2 = sustain(D2R), 1 = release(RR), 0 = off.
        property state : UInt32 = 0

        # Attack state.
        property egShAr : UInt8 = 0

        # Attack state.
        property egSelAr : UInt8 = 0

        # Total attenuation level.
        property tl : UInt32 = 0

        # Current envelope attenuation level.
        property volume : Int32 = MAX_ATT_INDEX

        # Decay state.
        property egShD1r : UInt8 = 0

        # Decay state.
        property egSelD1r : UInt8 = 0

        # Envelope switches to sustain state after reaching this level.
        property d1l : UInt32 = 0

        # Sustain state.
        property egShD2r : UInt8 = 0

        # Sustain state
        property egSelD2r : UInt8 = 0

        # Release state
        property egShRr : UInt8 = 0

        # Release state
        property egSelRr : UInt8 = 0

        # 0 = last key was KEY OFF, 1 = last key was KEY ON.
        property key : UInt32 = 0

        # Key scale.
        property ks : UInt32 = 0

        # Attack rate.
        property ar : UInt32 = 0

        # Decay rate.
        property d1r : UInt32 = 0

        # Sustain rate.
        property d2r : UInt32 = 0

        # Release rate.
        property rr : UInt32 = 0

        def initialize
        end

        @[AlwaysInline]
        def keyOn(keySet : UInt32, egCount : UInt32) : Nil
          if @key == 0
            @phase = 0 # Clear phase
            @state = EG_ATT # Key On = Attack
            {% if flag?(:yunosynth_wd40) %}
              @volume += ((~@volume) * (EG_INC.get!(@egSelAr + ((egCount >> @egShAr) & 7)))) >> 4
            {% else %}
              @volume += ((~@volume) * (EG_INC[@egSelAr + ((egCount >> @egShAr) & 7)])) >> 4
            {% end %}

            if @volume <= MIN_ATT_INDEX
              @volume = MIN_ATT_INDEX
              @state = EG_DEC
            end
          end

          @key |= keySet
        end

        @[AlwaysInline]
        def keyOff(keyClr : UInt32) : Nil
          if @key != 0
            @key &= keyClr
            if @key == 0 && @state > EG_REL # Key Off = Release
              @state = EG_REL
            end
          end
        end
      end

      ###
      ### Fields
      ###

      # The 32 operators.
      @operators : Slice(Operator)

      # Channel output masks (0xFFFFFFFF = enable).
      @pan : Array(UInt32) = Array(UInt32).new(16, 0u32)

      # Used for muting.
      @muted : Array(UInt8) = Array(UInt8).new(8, 0u8)

      # Global envelope generator counter.
      @egCount : UInt32 = 0

      # Global envelope generator counter works at frequency = chipclock / 64 / 3
      @egTimer : UInt32 = 0

      # Step for egTimer
      @egTimerAdd : UInt32 = 0

      # Envelope generator timer overflows every 3 samples (on real chips).
      @egTimerOverflow : UInt32 = 0

      # Accumulated LFO phase (0 to 255)
      @lfoPhase : UInt32 = 0

      # LFO timer
      @lfoTimer : UInt32 = 0

      # Step of lfoTimer
      @lfoTimerAdd : UInt32 = 0

      # LFO generates new output when lfoTimer reaches this value.
      @lfoOverflow : UInt32 = 0

      # LFO phase increment counter.
      @lfoCounter : UInt32 = 0

      # Step for lfoCounter
      @lfoCounterAdd : UInt32 = 0

      # LFO waveform select (0 = saw, 1 = square, 2 = triangle, 3 = random noise).
      @lfoWsel : UInt8 = 0

      # LFO amplitude modulation depth.
      @amd : UInt8 = 0

      # LFO phase modulation depth.
      @pmd : Int8 = 0

      # LFO current AM output.
      @lfa : UInt32 = 0

      # LFO current PM output.
      @lfp : Int32 = 0

      # TEST register.
      @test : UInt8 = 0

      # Output control pins (bit1 = CT2, bit0 = CT1).
      @ct : UInt8 = 0

      # Noise enable/period register (bit 7 = noise enable, bits 4-0 = noise period).
      @noise : UInt32 = 0

      # 17 bit noise shift register.
      @noiseRng : UInt32 = 0

      # Current noise 'phase'.
      @noiseP : UInt32 = 0

      # Current noise period.
      @noiseF : UInt32 = 0

      # CSM key on/key off sequence request.
      @csmReq : UInt32 = 0

      # IRQ enable for timer B (bit 3) and timer A (bit 2).  Bit 7 = CSM mode
      # (key on to all slots, everytime timer A overflows).
      @irqEnable : UInt32 = 0

      # Chip status (BUSY, IRQ Flags, etc.)
      @status : UInt32 = 0

      # Channels connections.
      @connect : Array(UInt8) = Array(UInt8).new(8, 0u8)

      # Timer A enable (0 = disabled)
      @timA : UInt8 = 0

      # Timer B enable (0 = disabled)
      @timB : UInt8 = 0

      # Current value of timer A.
      @timAVal : Int32 = 0

      # Current value of timer B.
      @timBVal : Int32 = 0

      # Timer A deltas
      @timATab : Array(UInt32) = Array(UInt32).new(1024, 0)

      # Timer B deltas
      @timBTab : Array(UInt32) = Array(UInt32).new(256, 0)

      # Timer A index.
      @timerAIndex : UInt32 = 0

      # Timer B index.
      @timerBIndex : UInt32 = 0

      # Timer A previous index.
      @timerAIndexOld : UInt32 = 0

      # Timer B previous index.
      @timerBIndexOld : UInt32 = 0

      # Frequency-deltas to get the closest frequency possible.
      #   There are 11 octaves because of DT2 (max 950 cents over base frequency)
      #   and LFO phase modulation (max 800 cents below AND over base frequency)
      #   Summary:   octave  explanation
      #              0       note code - LFO PM
      #              1       note code
      #              2       note code
      #              3       note code
      #              4       note code
      #              5       note code
      #              6       note code
      #              7       note code
      #              8       note code
      #              9       note code + DT2 + LFO PM
      #              10      note code + DT2 + LFO PM
      @freq : Array(UInt32) = Array(UInt32).new(11 * 768, 0) # 11 octaves, 768 cents per octave

      # Frequency deltas for DT1.  These deltas alter operator frequency after
      # it has been taken from the frequency-deltas table.
      @dt1Freq : Array(Int32) = Array(Int32).new(8 * 32, 0)

      # 17-bit noise generator periods.
      @noiseTab : Array(UInt32) = Array(UInt32).new(32, 0)

      # Chip clock in hertz.
      @clock : UInt32

      # Chip sampling frequency in hertz.
      @sampFreq : UInt32 = 0

      ##
      ## These are for speedup purposes only.
      ##

      @chanout : Array(Int32) = Array(Int32).new(8, 0)
      @m2 : Int32 = 0
      @c1 : Int32 = 0
      @c2 : Int32 = 0
      @mem : Int32 = 0

      ###
      ### Methods
      ###

      def initialize(@clock : UInt32, rate : UInt32)
        @operators = Slice(Operator).new(32) { |_| Operator.new }
        initTables

        # Do this now to avoid division by 0 in initChipTables
        @sampFreq = (rate != 0 ? rate : 44100u32)

        initChipTables

        @lfoTimerAdd = ((1u32 << LFO_SH) * (@clock / 64.0) / @sampFreq).to_u32!
        @egTimerAdd = ((1u32 << EG_SH) * (@clock / 64.0) / @sampFreq).to_u32!
        @egTimerOverflow = 3u32 * (1u32 << EG_SH)

        unmuteAll
      end

      def write(reg : Int32, val : Int32) : Nil
        kcChannel : UInt32 = 0
        op : Pointer(Operator) = @operators.to_unsafe + ((reg & 0x07) * 4 + ((reg & 0x18) >> 3))
        op0 : Operator = op[0]

        # Adjust bus to 8 bits
        reg &= 0xFF
        val &= 0xFF

        case reg & 0xE0
        when 0x00
          case reg
          when 0x01 # LFO reset (bit 1), Test Register (other bits)
            @test = val.to_u8!
            @lfoPhase = 0 if Yuno.bitflag?(val, 2)

          when 0x08
            envelopeKOnKOff(@operators.to_unsafe + ((val & 7) * 4), val)

          when 0x0F # Noise mode enable, noise period
            @noise = val.to_u32!
            @noiseF = @noiseTab.get!(val & 0x1F)

          when 0x10 # Timer A high
            @timerAIndex = (@timerAIndex & 0x003) | (val << 2).to_u32!

          when 0x11 # Timer A low
            @timerAIndex = (@timerAIndex & 0x3FC) | (val & 3)

          when 0x12 # Timer B
            @timerBIndex = val.to_u32!

          when 0x14 # CSM, irq flag reset, irq enable, timer start/stop
            @irqEnable = val.to_u32! # bit 3 = timer B, bit 2 = timer A, bit 7 = CSM

            if Yuno.bitflag?(val, 0x10) # Reset timer A irq flag
              @status &= ~1u32
            end

            if Yuno.bitflag?(val, 0x20) # Reset timer B irq flag
              @status &= ~2u32
            end

            if Yuno.bitflag?(val, 0x02)
              # Load and start timer B
              if @timB == 0
                @timB = 1
                @timBVal = @timBTab.get!(@timerBIndex).to_i32!
              end
            else
              # Stop timer B
              @timB = 0
            end

            if Yuno.bitflag?(val, 0x01)
              # Load and start timer A
              if @timA == 0
                @timA = 1
                @timAVal = @timATab.get!(@timerAIndex).to_i32!
              end
            else
              # Stop timer A
              @timA = 0
            end

          when 0x18 # LFO frequency
            @lfoOverflow = ((1u32 << ((15u32 - (val >> 4).to_u32!) + 3)) * (1u32 << LFO_SH)).to_u32!
            @lfoCounterAdd = 0x10u32 + (val & 0x0F).to_u32!

          when 0x19 # PMD (bit 7 == 1) or AMD (bit 7 == 0)
            if Yuno.bitflag?(val, 0x80)
              @pmd = (val & 0x7F).to_i8!
            else
              @amd = (val & 0x7F).to_u8!
            end

          when 0x1b # CT1, CT2, LFO waveform
            @ct = (val >> 6).to_u8!
            @lfoWsel = (val & 3).to_u8!

          else
            Yuno.dlog!(sprintf("YM2151 write $%02x to undocumented register $%02x\n", val, reg))
          end

        when 0x20
          op = @operators.to_unsafe + ((reg & 7) * 4)
          case reg & 0x18
          when 0x00 # RL enable, Feedback, Connection
            op[0].fbShift = (((val >> 3) & 7) != 0 ? (((val >> 3) & 7) + 6) : 0).to_u32!
            @pan.put!((reg & 7) * 2, ((val & 0x40) != 0 ? ~0u32 : 0).to_u32!)
            @pan.put!((reg & 7) * 2 + 1, ((val & 0x80) != 0 ? ~0u32 : 0).to_u32!)
            @connect.put!(reg & 7, (val & 7).to_u8!)
            setConnect(op, reg & 7, val & 7)

          when 0x08 # Key code
            val &= 0x7F
            if val != op[0].kc
              kcChannel = (val.to_u32! - (val.to_u32! >> 2)) * 64
              kcChannel += 768
              kcChannel |= (op[0].kci & 63)

              op[0].kc  = val.to_u32!
              op[0].kci = kcChannel
              op[1].kc  = val.to_u32!
              op[1].kci = kcChannel
              op[2].kc  = val.to_u32!
              op[2].kci = kcChannel
              op[3].kc  = val.to_u32!
              op[3].kci = kcChannel

              kc : UInt32 = val.to_u32! >> 2

              op[0].dt1  = @dt1Freq[op[0].dt1i + kc]
              op[0].freq = ((@freq[kcChannel + op[0].dt2] + op[0].dt1) * op[0].mul) >> 1

              op[1].dt1  = @dt1Freq[op[1].dt1i + kc]
              op[1].freq = ((@freq[kcChannel + op[1].dt2] + op[1].dt1) * op[1].mul) >> 1

              op[2].dt1  = @dt1Freq[op[2].dt1i + kc]
              op[2].freq = ((@freq[kcChannel + op[2].dt2] + op[2].dt1) * op[2].mul) >> 1

              op[3].dt1  = @dt1Freq[op[3].dt1i + kc]
              op[3].freq = ((@freq[kcChannel + op[3].dt2] + op[3].dt1) * op[3].mul) >> 1

              refreshEG(op)
            end

          when 0x10 # Key fraction
            val >>= 2
            if val != (op[0].kci & 63)
              kcChannel = val.to_u32!
              kcChannel |= (op[0].kci & ~63u32)

              op[0].kci = kcChannel
              op[1].kci = kcChannel
              op[2].kci = kcChannel
              op[3].kci = kcChannel

              op[0].freq = ((@freq[kcChannel + op[0].dt2] + op[0].dt1) * op[0].mul) >> 1
              op[1].freq = ((@freq[kcChannel + op[1].dt2] + op[1].dt1) * op[1].mul) >> 1
              op[2].freq = ((@freq[kcChannel + op[2].dt2] + op[2].dt1) * op[2].mul) >> 1
              op[3].freq = ((@freq[kcChannel + op[3].dt2] + op[3].dt1) * op[3].mul) >> 1
            end

          when 0x18 # PMS, AMS
            op[0].pms = ((val >> 4) & 7).to_u32!
            op[0].ams = (val & 3).to_u32!
          end

        when 0x40 # DT1, MUL
          oldDt1i : UInt32 = op0.dt1i
          oldMul : UInt32 = op0.mul

          op0.dt1i = (val.to_u32! & 0x70) << 1
          op0.mul = if (val & 0x0F) != 0
                        (val.to_u32! & 0x0F) << 1
                      else
                        1u32
                      end

          if oldDt1i != op0.dt1i
            {% if flag?(:yunosynth_wd40) %}
              op0.dt1 = @dt1Freq.get!(op0.dt1i + (op0.kc >> 2))
            {% else %}
              op0.dt1 = @dt1Freq[op0.dt1i + (op0.kc >> 2)]
            {% end %}
          end

          if oldDt1i != op0.dt1i || oldMul != op0.mul
            {% if flag?(:yunosynth_wd40) %}
              op0.freq = ((@freq.get!(op0.kci + op0.dt2) + op0.dt1) * op0.mul) >> 1
            {% else %}
              op0.freq = ((@freq[op0.kci + op0.dt2] + op0.dt1) * op0.mul) >> 1
            {% end %}
          end

        when 0x60 # TL
          op0.tl = (val.to_u32! & 0x7F) << (ENV_BITS - 7) # 7bit TL

        when 0x80 # KS, AR
          oldks : UInt32 = op0.ks
          oldar : UInt32 = op0.ar

          op0.ks = (5 - (val >> 6)).to_u32!
          op0.ar = ((val & 0x1F) != 0 ? (32u32 + ((val.to_u32! & 0x1F) << 1)) : 0).to_u32!

          if op0.ar != oldar || op0.ks != oldks
            if op0.ar + (op0.kc >> op0.ks) < 32 + 62
              {% if flag?(:yunosynth_wd40) %}
                op0.egShAr = @egRateShift.get!(op0.ar + (op0.kc >> op0.ks))
                op0.egSelAr = @egRateSelect.get!(op0.ar + (op0.kc >> op0.ks))
              {% else %}
                op0.egShAr = @egRateShift[op0.ar + (op0.kc >> op0.ks)]
                op0.egSelAr = @egRateSelect[op0.ar + (op0.kc >> op0.ks)]
              {% end %}
            else
              op0.egShAr = 0
              op0.egSelAr = 17u8 * RATE_STEPS
            end
          end

          if op0.ks != oldks
            {% if flag?(:yunosynth_wd40) %}
              op0.egShD1r = @egRateShift.get!(op0.d1r + (op0.kc >> op0.ks))
              op0.egSelD1r= @egRateSelect.get!(op0.d1r + (op0.kc >> op0.ks))
              op0.egShD2r = @egRateShift.get!(op0.d2r + (op0.kc >> op0.ks))
              op0.egSelD2r= @egRateSelect.get!(op0.d2r + (op0.kc >> op0.ks))
              op0.egShRr  = @egRateShift.get!(op0.rr + (op0.kc >> op0.ks))
              op0.egSelRr = @egRateSelect.get!(op0.rr + (op0.kc >> op0.ks))
            {% else %}
              op0.egShD1r = @egRateShift[op0.d1r + (op0.kc >> op0.ks)]
              op0.egSelD1r= @egRateSelect[op0.d1r + (op0.kc >> op0.ks)]
              op0.egShD2r = @egRateShift[op0.d2r + (op0.kc >> op0.ks)]
              op0.egSelD2r= @egRateSelect[op0.d2r + (op0.kc >> op0.ks)]
              op0.egShRr  = @egRateShift[op0.rr + (op0.kc >> op0.ks)]
              op0.egSelRr = @egRateSelect[op0.rr + (op0.kc >> op0.ks)]
            {% end %}
          end

        when 0xA0 # LFO AM enable, D1R
          op0.amMask = ((val & 0x80) != 0 ? ~0u32 : 0).to_u32!
          op0.d1r = ((val & 0x1F) != 0 ? (32 + ((val.to_u32! & 0x1F) << 1)) : 0).to_u32!
          {% if flag?(:yunosynth_wd40) %}
            op0.egShD1r = @egRateShift.get!(op0.d1r + (op0.kc >> op0.ks))
            op0.egSelD1r = @egRateSelect.get!(op0.d1r + (op0.kc >> op0.ks))
          {% else %}
            op0.egShD1r = @egRateShift[op0.d1r + (op0.kc >> op0.ks)]
            op0.egSelD1r = @egRateSelect[op0.d1r + (op0.kc >> op0.ks)]
          {% end %}

        when 0xC0 # DT2, D2R
          oldDt2 : UInt32 = op0.dt2
          op0.dt2 = @dt2Tab.get!(val >> 6)
          if op0.dt2 != oldDt2
            {% if flag?(:yunosynth_wd40) %}
              op0.freq = ((@freq.get!(op0.kci + op0.dt2) + op0.dt1) * op0.mul) >> 1
            {% else %}
              op0.freq = ((@freq[op0.kci + op0.dt2] + op0.dt1) * op0.mul) >> 1
            {% end %}
          end

          op0.d2r = ((val & 0x1F) != 0 ? (32 + ((val.to_u32! & 0x1F) << 1)) : 0).to_u32!
          {% if flag?(:yunosynth_wd40) %}
            op0.egShD2r = @egRateShift.get!(op0.d2r + (op0.kc >> op0.ks))
            op0.egSelD2r= @egRateSelect.get!(op0.d2r + (op0.kc >> op0.ks))
          {% else %}
            op0.egShD2r = @egRateShift[op0.d2r + (op0.kc >> op0.ks)]
            op0.egSelD2r= @egRateSelect[op0.d2r + (op0.kc >> op0.ks)]
          {% end %}

        when 0xE0 # D1L, RR
          op0.d1l = @d1lTab.get!(val >> 4) # Val is 8-bit and d1lTab only has 16 elements
          op0.rr = (34 + ((val.to_u32! & 0x0F) << 2)).to_u32!
          {% if flag?(:yunosynth_wd40) %}
            op0.egShRr = @egRateShift.get!(op0.rr + (op0.kc >> op0.ks))
            op0.egSelRr = @egRateSelect.get!(op0.rr + (op0.kc >> op0.ks))
          {% else %}
            op0.egShRr = @egRateShift[op0.rr + (op0.kc >> op0.ks)]
            op0.egSelRr = @egRateSelect[op0.rr + (op0.kc >> op0.ks)]
          {% end %}
        end
      end

      def readStatus : UInt32
        @status
      end

      def reset : Nil
        32.times do |i|
          @operators[i] = Operator.new
          @operators[i].volume = MAX_ATT_INDEX
          @operators[i].kci = 768 # Min kci value
        end

        @egTimer = 0
        @egCount = 0

        @lfoTimer = 0
        @lfoCounter = 0
        @lfoPhase = 0
        @lfoWsel = 0
        @pmd = 0
        @amd = 0
        @lfa = 0
        @lfp = 0

        @test = 0

        @irqEnable = 0
        @timA = 0
        @timB = 0
        @timAVal = 0
        @timBVal = 0
        @timerAIndex = 0
        @timerBIndex = 0
        @timerAIndexOld = 0
        @timerBIndexOld = 0

        @noise = 0
        @noiseRng = 0
        @noiseP = 0
        @noiseF = @noiseTab[0]

        @csmReq = 0
        @status = 0

        write(0x1b, 0) # Only because of CT1, CT2 output pins
        write(0x18, 0) # Set LFO frequency

        # Set operators
        (0x20...0x100).each do |i|
          write(i, 0)
        end
      end

      def update(outputs : OutputBuffers, samples : UInt32) : Nil
        smpL : Int32 = 0
        smpR : Int32 = 0
        outL : Slice(Int32) = outputs[0]
        outR : Slice(Int32) = outputs[1]

        # Check once, then we can avoid bounds checks below.
        if outL.size < samples || outR.size < samples
          raise "YM2151 update: Bad number of samples (expected #{samples}, " \
                "but buffers have #{outL.size} and #{outR.size})"
        end

        if @timB != 0
          @timBVal -= (samples.to_i32! << TIMER_SH)
          if @timBVal <= 0
            @timBVal += @timBTab.get!(@timerBIndex)
            @status |= 2 if Yuno.bitflag?(@irqEnable, 0x08)
          end
        end

        samples.times do |i|
          advanceEG
          @chanout.fill(0)

          chanCalc(0)
          chanCalc(1)
          chanCalc(2)
          chanCalc(3)
          chanCalc(4)
          chanCalc(5)
          chanCalc(6)
          chan7Calc

          smpL  = @chanout.get!(0) & @pan.get!(0)
          smpR  = @chanout.get!(0) & @pan.get!(1)
          smpL += @chanout.get!(1) & @pan.get!(2)
          smpR += @chanout.get!(1) & @pan.get!(3)
          smpL += @chanout.get!(2) & @pan.get!(4)
          smpR += @chanout.get!(2) & @pan.get!(5)
          smpL += @chanout.get!(3) & @pan.get!(6)
          smpR += @chanout.get!(3) & @pan.get!(7)
          smpL += @chanout.get!(4) & @pan.get!(8)
          smpR += @chanout.get!(4) & @pan.get!(9)
          smpL += @chanout.get!(5) & @pan.get!(10)
          smpR += @chanout.get!(5) & @pan.get!(11)
          smpL += @chanout.get!(6) & @pan.get!(12)
          smpR += @chanout.get!(6) & @pan.get!(13)
          smpL += @chanout.get!(7) & @pan.get!(14)
          smpR += @chanout.get!(7) & @pan.get!(15)

          # Remi: Commented out because FINAL_SH is always 0 for us.
          #smpL >>= FINAL_SH
          #smpR >>= FINAL_SH

          outL.put!(i, smpL)
          outR.put!(i, smpR)

          if @timA != 0
            @timAVal -= (1 << TIMER_SH)
            if @timAVal <= 0
              @timAVal += @timATab.get!(@timerAIndex)
              @status |= 1 if Yuno.bitflag?(@irqEnable, 0x04)
              @csmReq = 2 if Yuno.bitflag?(@irqEnable, 0x80)
            end
          end

          advance
        end
      end

      def muteMask=(mask : UInt32) : Nil
        8.times do |ch|
          @muted[ch] = ((mask >> ch) & 0x01).to_u8!
        end
      end

      def unmuteAll : Nil
        @muted.fill(0)
      end

      ###
      ### Private Methods
      ###

      private def initTables : Nil
        m : Float64 = 0.0
        o : Float64 = 0.0
        n : Int32 = 0

        TL_RES_LEN.times do |x|
          m = (1 << 16) / (2.0 ** ((x + 1) * (ENV_STEP / 4.0) / 8.0))
          m = m.floor

          #
          # We never reach (1 << 16) here due to the (x + 1).  Result fits
          # within 16 bits maximum.
          #

          n = m.to_i32! # 16 bits here
          n >>= 4 # 12 bits here.
          if (n & 1) != 0  # Round to closest
            n = (n >> 1) + 1
          else
            n >>= 1
          end

          # 11 bits here (rounded)
          n <<= 2 # 13 bits here (as in real chip)
          @tlTab[x * 2] = n
          @tlTab[x * 2 + 1] = -@tlTab[x * 2]

          (1...13).each do |i|
            @tlTab[x * 2 + 0 + i * 2 * TL_RES_LEN] =  @tlTab[x * 2 + 0] >> i
            @tlTab[x * 2 + 1 + i * 2 * TL_RES_LEN] = -@tlTab[x * 2 + 0 + i * 2 * TL_RES_LEN]
          end

          # STDOUT << sprintf("tl %04i", x*2)
          # 13.times do |i|
          #   STDOUT << printf(", [%02i] %4i", i*2, @tlTab[ x*2 + i*2*TL_RES_LEN ]);
          # end
          # STDOUT << '\n'
        end

        SIN_LEN.times do |i|
          # non-standard sinus
          m = Math.sin(((i * 2) + 1) * Math::PI / SIN_LEN) # Verified on the real chip

          #
          # We never reach zero here due to ((i * 2) + 1)
          #

          if m > 0.0
            o = 8.0 * Math.log(1.0 / m) / Math.log(2.0) # Convert to decibels
          else
            o = 8.0 * Math.log(-1.0 / m) / Math.log(2.0) # Convert to decibels
          end

          o = o / (ENV_STEP / 4.0)

          n = (2.0 * o).to_i32!
          if (n & 1) != 0 # Round to closest
            n = (n >> 1) + 1
          else
            n >>= 1
          end

          @sinTab[i] = (n * 2 + (m >= 0.0 ? 0 : 1)).to_u32!
        end

        # Calculate the d1lTab table
        16.times do |i|
          m = (i != 15 ? i : (i + 16)) * (4.0 / ENV_STEP) # Every 3dB except for all bits = 1 = 45 + 48 dB
          @d1lTab[i] = m.to_u32!
        end
      end

      private def initChipTables : Nil
        phaseInc : Float64 = 0.0
        hz : Float64 = 0.0
        pom : Float64 = 0.0
        scaler : Float64 = (@clock / 64.0) / @sampFreq.to_f64!

        #
        # This loop calculates Hertz values for notes from c-0 to b-7
        # including 64 cents (100 / 64 that is 1.5625 of real cent) per note
        # i * 100 / 64 / 1200 is equal to i / 768
        #

        # Real chip works with 10 bits fixed point values (10.10).
        # -10 here because phaseIncRom table values are already in 10.10 format.
        mult : Float64 = (1 << (FREQ_SH - 10)).to_f64!

        768.times do |i|
          # 3.4375 Hz is note A; C# is 4 semitones higher.
          hz = 1000.0

          phaseInc = @phaseIncRom[i].to_f64! # real chip phase increment
          phaseInc *= scaler # adjust

          # Octave 2 - reference octave
          @freq[768 + 2 * 768 + i] = (phaseInc * mult).to_u32! & 0xFFFFFFC0 # Adjust to X.10 fixed point

          # Octave 0 and octave 1
          2.times do |j|
            @freq[768 + j * 768 + i] = (@freq[768 + 2 * 768 + i] >> (2 - j)) & 0xFFFFFFC0 # Adjust to X.10 fixed point
          end

          # Octave 3 to 7
          (3...8).each do |j|
            @freq[768 + j * 768 + i] = @freq[768 + 2 * 768 + i] << (j - 2)
          end

          # pom = @freq[768+2*768+i].to_f64! / (1 << FREQ_SH).to_f64!
          # pom = pom * @sampFreq.to_f64! / SIN_LEN.to_f64!
          # STDOUT << sprintf("1freq[%4i][%08x]= real %20.15f Hz  emul %20.15f Hz\n",
          #                   i, @freq[ 768+2*768+i ], hz, pom)
        end

        # Octave -1 (all equal to: oct 0 _KC_00_, _KF_00_)
        768.times do |i|
          @freq[0 * 768 + i] = @freq[1 * 768 + 0]
        end

        # Octave 8 and 9 (all equal to: oct 7, _KC_14_, _KF_63_)
        (8...10).each do |j|
          768.times do |i|
            @freq[768 + j * 768 + i] = @freq[768 + 8 * 768 - 1]
          end
        end

        # (11 * 768).times do |i|
        #   pom = @freq[i].to_f64! / (1 << FREQ_SH).to_f64!
        #   pom = pom * @sampFreq.to_f64! / SIN_LEN.to_f64!
        #   STDOUT << sprintf("freq[%4i][%08x]= emul %20.15f Hz\n", i, @freq[i], pom)
        # end

        mult = (1 << FREQ_SH).to_f64!
        4.times do |j|
          32.times do |i|
            hz = (@dt1Tab[j * 32 + i].to_f64! * (@clock / 64.0)) / (1 << 20).to_f64!

            # Calculate phase increment
            phaseInc = (hz * SIN_LEN) / @sampFreq

            # Positive and negative values
            @dt1Freq[(j + 0) * 32 + i] = (phaseInc * mult).to_i32!
            @dt1Freq[(j + 4) * 32 + i] = -@dt1Freq[(j + 0) * 32 + i]

            # x = j * 32 + i
            # pom = @dt1Freq[x].to_f64! / mult
            # pom = pom * @sampFreq.to_f64! / SIN_LEN.to_f64!
            # STDOUT << sprintf("DT1(%03i)[%02i %02i][%08x]= real %19.15f Hz  emul %19.15f Hz\n",
            #                   x, j, i, @dt1Freq[x], hz, pom)
          end
        end

        # Calculate timers' deltas.
        # User's manual pages 15 and 16.
        mult = (1 << TIMER_SH).to_f64!
        1024.times do |i|
          # ASG 980324: changed to compute both tim_A_tab and timer_A_time
          pom = 64.0 * (1024 - i) / @clock
          @timATab[i] = (pom * @sampFreq * mult).to_u32! # Number of samples that timer period takes (fixed point).
        end

        256.times do |i|
          # ASG 980324: changed to compute both tim_B_tab and timer_B_time
          pom = 1024.0 * (256 - i) / @clock
          @timBTab[i] = (pom * @sampFreq * mult).to_u32! # Number of samples that timer period takes (fixed point).
        end

        # Calculate noise period table
        scaler = (@clock / 64.0) / @sampFreq
        32.times do |i|
          j = (i != 31 ? i : 30) # Rate 30 and 31 are the same
          j = 32 - j
          j = (65536.0 / (j * 32.0)).to_i32! # NUmber of samples per one shift of the shift register.
          @noiseTab[i] = (j * 64 * scaler).to_u32!
        end
      end

      @[AlwaysInline]
      private def envelopeKOnKOff(op : Pointer(Operator), v : Int) : Nil
        if Yuno.bitflag?(v, 0x08) # M1
          op[0].keyOn(1u32, @egCount)
        else
          op[0].keyOff(~1u32)
        end

        if Yuno.bitflag?(v, 0x20) # M2
          op[1].keyOn(1u32, @egCount)
        else
          op[1].keyOff(~1u32)
        end

        if Yuno.bitflag?(v, 0x10) # C1
          op[2].keyOn(1u32, @egCount)
        else
          op[2].keyOff(~1u32)
        end

        if Yuno.bitflag?(v, 0x40) # C2
          op[3].keyOn(1u32, @egCount)
        else
          op[3].keyOff(~1u32)
        end
      end

      private def setConnect(oms : Pointer(Operator), cha : Int, v : Int) : Nil
        om1 : Operator = oms[0]
        om2 : Operator = oms[1]
        oc1 : Operator = oms[2]

        # Set connect algorithm.
        # MEM is simply one sample delay,

        case v & 7
        when 0
          # M1---C1---MEM---M2---C2---OUT
          om1.connect = pointerof(@c1)
          oc1.connect = pointerof(@mem)
          om2.connect = pointerof(@c2)
          om1.memConnect = pointerof(@m2)

        when 1
          # M1------+-MEM---M2---C2---OUT
          #      C1-+
          om1.connect = pointerof(@mem)
          oc1.connect = pointerof(@mem)
          om2.connect = pointerof(@c2)
          om1.memConnect = pointerof(@m2)

        when 2
          # M1-----------------+-C2---OUT
          #      C1---MEM---M2-+
          om1.connect = pointerof(@c2)
          oc1.connect = pointerof(@mem)
          om2.connect = pointerof(@c2)
          om1.memConnect = pointerof(@m2)

        when 3
          # M1---C1---MEM------+-C2---OUT
          #                 M2-+
          om1.connect = pointerof(@c1)
          oc1.connect = pointerof(@mem)
          om2.connect = pointerof(@c2)
          om1.memConnect = pointerof(@c2)

        when 4
          # M1---C1-+-OUT
          # M2---C2-+
          # MEM: not used
          om1.connect = pointerof(@c1)
          oc1.connect = @chanout.to_unsafe + cha
          om2.connect = pointerof(@c2)
          om1.memConnect = pointerof(@mem)        # store it anywhere where it will not be used

        when 5
          #    +----C1----+
          # M1-+-MEM---M2-+-OUT
          #    +----C2----+
          om1.connect = Pointer(Int32).null # special mark
          oc1.connect = @chanout.to_unsafe + cha
          om2.connect = @chanout.to_unsafe + cha
          om1.memConnect = pointerof(@m2)

        when 6
          # M1---C1-+
          #      M2-+-OUT
          #      C2-+
          # MEM: not used
          om1.connect = pointerof(@c1)
          oc1.connect = @chanout.to_unsafe + cha
          om2.connect = @chanout.to_unsafe + cha
          om1.memConnect = pointerof(@mem)        # store it anywhere where it will not be used

        when 7
          # M1-+
          # C1-+-OUT
          # M2-+
          # C2-+
          # MEM: not used
          om1.connect = @chanout.to_unsafe + cha
          oc1.connect = @chanout.to_unsafe + cha
          om2.connect = @chanout.to_unsafe + cha
          om1.memConnect = pointerof(@mem)        # store it anywhere where it will not be used
        end
      end

      @[AlwaysInline]
      private def refreshEG(ops : Pointer(Operator)) : Nil
        opIdx = 0
        op = ops[opIdx]

        kc : UInt32 = ops[0].kc
        v : UInt32 = kc >> op.ks

        if (op.ar + v) < 32 + 62
          op.egShAr = @egRateShift.get!(op.ar + v)
          op.egSelAr = @egRateSelect.get!(op.ar + v)
        else
          op.egShAr = 0
          op.egSelAr = 17u8 * RATE_STEPS
        end
        op.egShD1r  = @egRateShift[op.d1r + v]
        op.egSelD1r = @egRateSelect.get!(op.d1r + v)
        op.egShD2r  = @egRateShift[op.d2r + v]
        op.egSelD2r = @egRateSelect.get!(op.d2r + v)
        op.egShRr   = @egRateShift[op.rr + v]
        op.egSelRr  = @egRateSelect.get!(op.rr + v)

        opIdx += 1
        op = ops[opIdx]

        v = kc >> op.ks
        if (op.ar + v) < 32 + 62
          op.egShAr  = @egRateShift.get!(op.ar  + v)
          op.egSelAr = @egRateSelect.get!(op.ar  + v)
        else
          op.egShAr  = 0
          op.egSelAr = 17u8 * RATE_STEPS
        end
        op.egShD1r = @egRateShift [op.d1r + v]
        op.egSelD1r= @egRateSelect.get!(op.d1r + v)
        op.egShD2r = @egRateShift [op.d2r + v]
        op.egSelD2r= @egRateSelect.get!(op.d2r + v)
        op.egShRr  = @egRateShift [op.rr  + v]
        op.egSelRr = @egRateSelect.get!(op.rr  + v)

        opIdx += 1
        op = ops[opIdx]

        v = kc >> op.ks
        if (op.ar + v) < 32+62
          op.egShAr  = @egRateShift.get!(op.ar  + v)
          op.egSelAr = @egRateSelect.get!(op.ar  + v)
        else
          op.egShAr  = 0
          op.egSelAr = 17u8 * RATE_STEPS
        end
        op.egShD1r = @egRateShift [op.d1r + v]
        op.egSelD1r= @egRateSelect.get!(op.d1r + v)
        op.egShD2r = @egRateShift [op.d2r + v]
        op.egSelD2r= @egRateSelect.get!(op.d2r + v)
        op.egShRr  = @egRateShift [op.rr  + v]
        op.egSelRr = @egRateSelect.get!(op.rr  + v)

        opIdx += 1
        op = ops[opIdx]

        v = kc >> op.ks
        if (op.ar+v) < 32 + 62
          op.egShAr  = @egRateShift.get!(op.ar  + v)
          op.egSelAr = @egRateSelect.get!(op.ar  + v)
        else
          op.egShAr  = 0
          op.egSelAr = 17u8 * RATE_STEPS
        end
        op.egShD1r = @egRateShift [op.d1r + v]
        op.egSelD1r= @egRateSelect.get!(op.d1r + v)
        op.egShD2r = @egRateShift [op.d2r + v]
        op.egSelD2r= @egRateSelect.get!(op.d2r + v)
        op.egShRr  = @egRateShift [op.rr  + v]
        op.egSelRr = @egRateSelect.get!(op.rr  + v)
      end

      @[AlwaysInline]
      private def opCalc(op : Operator, env : UInt32, pm : Int32) : Int32
        # STDOUT << sprintf("%i -> %i, %i -> %i -> %i -> %i\n",
        #                   pm,
        #                   (op.phase & ~FREQ_MASK),
        #                   (pm << 15),
        #                   ((op.phase & ~FREQ_MASK).to_i64! + (pm << 15).to_i64!),
        #                   (((op.phase & ~FREQ_MASK).to_i64! + (pm << 15).to_i64!) >> FREQ_SH),
        #                   (((op.phase & ~FREQ_MASK).to_i64! + (pm << 15).to_i64!) >> FREQ_SH) & SIN_MASK)

        x : UInt32 = ((env.to_u64! << 3) +
                      @sinTab.get!((((op.phase & ~FREQ_MASK) &+ (pm << 15)).to_i32! >> FREQ_SH) & SIN_MASK)).to_u32!

        if x >= TL_TAB_LEN
          0
        else
          @tlTab.get!(x)
        end
      end

      @[AlwaysInline]
      private def opCalc1(op : Operator, env : UInt32, pm : Int32) : Int32
        i : Int32 = ((op.phase & ~FREQ_MASK) &+ pm).to_i32!
        x : UInt32 = ((env.to_u64! << 3) + @sinTab.get!((i >> FREQ_SH) & SIN_MASK)).to_u32!
        if x >= TL_TAB_LEN
          0
        else
          @tlTab.get!(x)
        end
      end

      @[AlwaysInline]
      private def volumeCalc(op, am)
        op.tl + op.volume.to_u32! + (am & op.amMask)
      end

      @[AlwaysInline]
      private def chanCalc(chan : UInt32) : Nil
        {% if flag?(:yunosynth_wd40) %}
          return if @muted.get!(chan) != 0
        {% else %}
          return if @muted[chan] != 0
        {% end %}

        @m2 = 0
        @c1 = 0
        @c2 = 0
        @mem = 0

        ops : Pointer(Operator) = @operators.to_unsafe + (chan * 4) # M1
        op0 = ops[0]

        op0.memConnect.value = op0.memValue

        am : UInt32 = 0
        am = @lfa << (op0.ams - 1) if op0.ams != 0
        env : UInt32 = volumeCalc(op0, am)

        output : Int32 = op0.fbOutPrev + op0.fbOutCurrent
        op0.fbOutPrev = op0.fbOutCurrent

        if op0.connect.null?
          # Algorithm 5
          @mem = op0.fbOutPrev
          @c1 = op0.fbOutPrev
          @c2 = op0.fbOutPrev
        else
          # Other algorithms
          op0.connect.value = op0.fbOutPrev
        end

        op0.fbOutCurrent = 0
        if env < ENV_QUIET
          output = 0 if op0.fbShift == 0
          op0.fbOutCurrent = opCalc1(op0, env, (output << op0.fbShift))
        end

        env = volumeCalc(ops[1], am) # M2
        if env < ENV_QUIET
          ops[1].connect.value += opCalc(ops[1], env, @m2)
        end

        env = volumeCalc(ops[2], am) # C1
        if env < ENV_QUIET
          ops[2].connect.value += opCalc(ops[2], env, @c1)
        end

        env = volumeCalc(ops[3], am) # C2
        if env < ENV_QUIET
          @chanout[chan] += opCalc(ops[3], env, @c2)
        end

        @chanout.put!(chan, @chanout.get!(chan).clamp(-16384, 16384))
        op0.memValue = @mem
      end

      @[AlwaysInline]
      private def chan7Calc : Nil
        return if @muted.get!(7) != 0

        @m2 = 0
        @c1 = 0
        @c2 = 0
        @mem = 0

        ops : Pointer(Operator) = @operators.to_unsafe + (7 * 4) # M1
        op0 = ops[0]

        op0.memConnect.value = op0.memValue

        am : UInt32 = 0
        am = @lfa << (op0.ams - 1) if op0.ams != 0
        env : UInt32 = volumeCalc(op0, am)

        output : Int32 = op0.fbOutPrev + op0.fbOutCurrent
        op0.fbOutPrev = op0.fbOutCurrent

        if op0.connect.null?
          # Algorithm 5
          @mem = op0.fbOutPrev
          @c1 = op0.fbOutPrev
          @c2 = op0.fbOutPrev
        else
          # Other algorithms
          op0.connect.value = op0.fbOutPrev
        end

        op0.fbOutCurrent = 0
        if env < ENV_QUIET
          output = 0 if op0.fbShift == 0
          op0.fbOutCurrent = opCalc1(op0, env, (output << op0.fbShift))
        end

        env = volumeCalc(ops[1], am) # M2
        if env < ENV_QUIET
          ops[1].connect.value += opCalc(ops[1], env, @m2)
        end

        env = volumeCalc(ops[2], am) # C1
        if env < ENV_QUIET
          ops[2].connect.value += opCalc(ops[2], env, @c1)
        end

        env = volumeCalc(ops[3], am) # C2
        if Yuno.bitflag?(@noise, 0x80)
          noiseout : Int32 = 0
          if env < 0x3FF
            noiseout = ((env ^ 0x3FF) * 2).to_i32! # Range of the YM2151 noise output is -2044 to 2040
          end
          @chanout.incf!(7, ((@noiseRng & 0x10000) != 0 ? noiseout : -noiseout)) # bit 17 -> output
        elsif env < ENV_QUIET
          @chanout.incf!(7, opCalc(ops[3], env, @c2))
        end

        @chanout.put!(7, @chanout.get!(7).clamp(-16384, 16384))
        op0.memValue = @mem
      end

      # The 'rate' is calculated from following formula (example on decay rate):
      #   rks = notecode after key scaling (a value from 0 to 31)
      #   DR = value written to the chip register
      #   rate = 2*DR + rks; (max rate = 2*31+31 = 93)
      # Four MSBs of the 'rate' above are the 'main' rate (from 00 to 15)
      # Two LSBs of the 'rate' above are the value 'x' (the shape type).
      # (eg. '11 2' means that 'rate' is 11*4+2=46)
      #
      # NOTE: A 'sample' in the description below is actually 3 output samples,
      # thats because the Envelope Generator clock is equal to internal_clock/3.
      #
      # Single '-' (minus) character in the diagrams below represents one sample
      # on the output; this is for rates 11 x (11 0, 11 1, 11 2 and 11 3)
      #
      # these 'main' rates:
      # 00 x: single '-' = 2048 samples; (ie. level can change every 2048 samples)
      # 01 x: single '-' = 1024 samples;
      # 02 x: single '-' = 512 samples;
      # 03 x: single '-' = 256 samples;
      # 04 x: single '-' = 128 samples;
      # 05 x: single '-' = 64 samples;
      # 06 x: single '-' = 32 samples;
      # 07 x: single '-' = 16 samples;
      # 08 x: single '-' = 8 samples;
      # 09 x: single '-' = 4 samples;
      # 10 x: single '-' = 2 samples;
      # 11 x: single '-' = 1 sample; (ie. level can change every 1 sample)
      #
      # Shapes for rates 11 x look like this:
      # rate:       step:
      # 11 0        01234567
      #
      # level:
      # 0           --
      # 1             --
      # 2               --
      # 3                 --
      #
      # rate:       step:
      # 11 1        01234567
      #
      # level:
      # 0           --
      # 1             --
      # 2               -
      # 3                -
      # 4                 --
      #
      # rate:       step:
      # 11 2        01234567
      #
      # level:
      # 0           --
      # 1             -
      # 2              -
      # 3               --
      # 4                 -
      # 5                  -
      #
      # rate:       step:
      # 11 3        01234567
      #
      # level:
      # 0           --
      # 1             -
      # 2              -
      # 3               -
      # 4                -
      # 5                 -
      # 6                  -
      #
      #
      # For rates 12 x, 13 x, 14 x and 15 x output level changes on every
      # sample - this means that the waveform looks like this: (but the level
      # changes by different values on different steps)
      # 12 3        01234567
      #
      # 0           -
      # 2            -
      # 4             -
      # 8              -
      # 10              -
      # 12               -
      # 14                -
      # 18                 -
      # 20                  -
      #
      # Notes about the timing:
      # ----------------------
      #
      # 1. Synchronism
      #
      # Output level of each two (or more) voices running at the same 'main' rate
      # (eg 11 0 and 11 1 in the diagram below) will always be changing in sync,
      # even if there're started with some delay.
      #
      # Note that, in the diagram below, the decay phase in channel 0 starts at
      # sample #2, while in channel 1 it starts at sample #6. Anyway, both channels
      # will always change their levels at exactly the same (following) samples.
      #
      # (S - start point of this channel, A-attack phase, D-decay phase):
      #
      # step:
      # 01234567012345670123456
      #
      # channel 0:
      #   --
      #  |  --
      #  |    -
      #  |     -
      #  |      --
      #  |        --
      # |           --
      # |             -
      # |              -
      # |               --
      # AADDDDDDDDDDDDDDDD
      # S
      #
      # 01234567012345670123456
      # channel 1:
      #       -
      #      | -
      #      |  --
      #      |    --
      #      |      --
      #      |        -
      #     |          -
      #     |           --
      #     |             --
      #     |               --
      #     AADDDDDDDDDDDDDDDD
      #     S
      # 01234567012345670123456
      #
      #
      # 2. Shifted (delayed) synchronism
      #
      # Output of each two (or more) voices running at different 'main' rate
      # (9 1, 10 1 and 11 1 in the diagrams below) will always be changing
      # in 'delayed-sync' (even if there're started with some delay as in "1.")
      #
      # Note that the shapes are delayed by exactly one sample per one 'main' rate
      # increment. (Normally one would expect them to start at the same samples.)
      #
      # See diagram below (* - start point of the shape).
      #
      # cycle:
      # 0123456701234567012345670123456701234567012345670123456701234567
      #
      # rate 09 1
      # *-------
      #         --------
      #                 ----
      #                     ----
      #                         --------
      #                                 *-------
      #                                 |       --------
      #                                 |               ----
      #                                 |                   ----
      #                                 |                       --------
      # rate 10 1                       |
      # --                              |
      #   *---                          |
      #       ----                      |
      #           --                    |
      #             --                  |
      #               ----              |
      #                   *---          |
      #                   |   ----      |
      #                   |       --    | | <- one step (two samples) delay between 9 1 and 10 1
      #                   |         --  | |
      #                   |           ----|
      #                   |               *---
      #                   |                   ----
      #                   |                       --
      #                   |                         --
      #                   |                           ----
      # rate 11 1         |
      # -                 |
      #  --               |
      #    *-             |
      #      --           |
      #        -          |
      #         -         |
      #          --       |
      #            *-     |
      #              --   |
      #                -  || <- one step (one sample) delay between 10 1 and 11 1
      #                 - ||
      #                  --|
      #                    *-
      #                      --
      #                        -
      #                         -
      #                          --
      #                            *-
      #                              --
      #                                -
      #                                 -
      #                                  --
      @[AlwaysInline]
      private def advanceEG : Nil
        i : UInt32 = 0
        opIdx : Int32 = 0
        @egTimer += @egTimerAdd
        while @egTimer >= @egTimerOverflow
          @egTimer -= @egTimerOverflow
          @egCount += 1

          # Envelope generator
          opIdx = 0
          op : Operator = @operators.get!(opIdx) # CH 0 M1
          i = 32
          loop do
            case op.state
            when EG_ATT # Attack phase
              if (@egCount & ((1 << op.egShAr) - 1)) == 0
                {% if flag?(:yunosynth_wd40) %}
                  op.volume += (~op.volume * (EG_INC.get!(op.egSelAr + ((@egCount >> op.egShAr) & 7)))) >> 4
                {% else %}
                  op.volume += (~op.volume * (EG_INC[op.egSelAr + ((@egCount >> op.egShAr) & 7)])) >> 4
                {% end %}
                if op.volume <= MIN_ATT_INDEX
                  op.volume = MIN_ATT_INDEX
                  op.state = EG_DEC
                end
              end

            when EG_DEC # Decay phase
              if (@egCount & ((1 << op.egShD1r) - 1)) == 0
                {% if flag?(:yunosynth_wd40) %}
                  op.volume += EG_INC.get!(op.egSelD1r + ((@egCount >> op.egShD1r) & 7))
                {% else %}
                  op.volume += EG_INC[op.egSelD1r + ((@egCount >> op.egShD1r) & 7)]
                {% end %}
                op.state = EG_SUS if op.volume >= op.d1l
              end

            when EG_SUS # Sustain phase
              if (@egCount & ((1 << op.egShD2r) - 1)) == 0
                {% if flag?(:yunosynth_wd40) %}
                  op.volume += EG_INC.get!(op.egSelD2r + ((@egCount >> op.egShD2r) & 7))
                {% else %}
                  op.volume += EG_INC[op.egSelD2r + ((@egCount >> op.egShD2r) & 7)]
                {% end %}
                if op.volume >= MAX_ATT_INDEX
                  op.volume = MAX_ATT_INDEX
                  op.state = EG_OFF
                end
              end

            when EG_REL # Release phase
              if (@egCount & ((1 << op.egShRr) - 1)) == 0
                {% if flag?(:yunosynth_wd40) %}
                  op.volume += EG_INC.get!(op.egSelRr + ((@egCount >> op.egShRr) & 7))
                {% else %}
                  op.volume += EG_INC[op.egSelRr + ((@egCount >> op.egShRr) & 7)]
                {% end %}
                if op.volume >= MAX_ATT_INDEX
                  op.volume = MAX_ATT_INDEX
                  op.state = EG_OFF
                end
              end
            end

            i -= 1
            break if i == 0
            opIdx += 1
            op = @operators.get!(opIdx)
          end
        end
      end

      @[AlwaysInline]
      private def advance : Nil
        ops : Pointer(Operator) = @operators.to_unsafe
        a : Int32 = 0
        x : Int32 = 0
        opIdx : Int32 = 0

        if Yuno.bitflag?(@test, 2)
          @lfoPhase = 0
        else
          @lfoTimer += @lfoTimerAdd
          if @lfoTimer >= @lfoOverflow
            @lfoTimer -= @lfoOverflow
            @lfoCounter += @lfoCounterAdd
            @lfoPhase += (@lfoCounter >> 4)
            @lfoPhase &= 255
            @lfoCounter &= 15
          end
        end

        i : UInt32 = @lfoPhase

        # Calculate LFO AM and PM waveform value (all verified on real chip,
        # except for noise algorithm which is impossible to analyse).
        case @lfoWsel
        when 0 # Sawtooth
          # AM: 255 down to 0
          # PM: 0 to 127, -127 to 0 (at PMD=127: LFP = 0 to 126, -126 to 0)
          a = 255 - i
          x = if i < 128
                i.to_i32!
              else
                i.to_i32! - 255
              end

        when 1 # Square
          # AM: 255, 0
          # PM: 128,-128 (LFP = exactly +PMD, -PMD)
          if i < 128
            a = 255
            x = 128
          else
            a = 0
            x = -128
          end

        when 2 # Triangle
          # AM: 255 down to 1 step -2; 0 up to 254 step +2
          # PM: 0 to 126 step +2, 127 to 1 step -2, 0 to -126 step -2, -127 to -1 step +2
          a = if i < 128
                255 - (i.to_i32! * 2)
              else
                (i.to_i32! * 2) - 256
              end

          if i < 64                 # i = 0..63
            x = i.to_i32! * 2       # 0 to 126 step + 2
          elsif i < 128             # i = 64..127
            x = 255 - i.to_i32! * 2 # 127 to 1 step - 2
          elsif i < 192             # i = 128..191
            x = 256 - i.to_i32! * 2 # 0 to -126 step -2
          else                      # i = 192..255
            x = i.to_i32! * 2 - 511 # -127 to -1 step + 2
          end

        else # Random
          # the real algorithm is unknown !!!
          # We just use a snapshot of data from real chip
          #
          # AM: range 0 to 255
          # PM: range -128 to 127

          {% if flag?(:yunosynth_wd40) %}
            a = @lfoNoiseWaveform.get!(i).to_i32!
          {% else %}
            a = @lfoNoiseWaveform[i].to_i32!
          {% end %}
          x = a - 128
        end

        @lfa = (a * @amd.to_i32!).tdiv(128).to_u32!
        @lfp = (x * @pmd.to_i32!).tdiv(128)

        # The Noise Generator of the YM2151 is 17-bit shift register.
        # Input to the bit16 is negated (bit0 XOR bit3) (EXNOR).
        # Output of the register is negated (bit0 XOR bit3).
        # Simply use bit16 as the noise output.
        @noiseP += @noiseF
        i = @noiseP >> 16 # Number of events (shifts of the shift register)
        @noiseP &= 0xFFFF

        j : UInt32 = 0
        while i != 0
          j = ((@noiseRng ^ (@noiseRng >> 3)) & 1) ^ 1
          @noiseRng = (j << 16) | (@noiseRng >> 1)
          i -= 1
        end

        # Phase generator
        opIdx = 0
        i = 8
        loop do
          op = ops[opIdx]
          if op.pms != 0 # Only when phase modulation from LFO is enabled for this channel
            modInd : Int32 = @lfp # -128..127 (8-bits signed)
            if op.pms < 6
              modInd >>= (6 - op.pms)
            else
              modInd <<= (op.pms - 5)
            end

            if modInd != 0
              kcChannel : UInt32 = op.kci + modInd
              (opIdx...(opIdx + 4)).each do |newIdx|
                {% if flag?(:yunosynth_wd40) %}
                  ops[newIdx].phase = ops[newIdx].phase &+ (((@freq.get!(kcChannel + ops[newIdx].dt2) +
                                                              ops[newIdx].dt1) * ops[newIdx].mul) >> 1)
                {% else %}
                  ops[newIdx].phase = ops[newIdx].phase &+ (((@freq[kcChannel + ops[newIdx].dt2] +
                                                              ops[newIdx].dt1) * ops[newIdx].mul) >> 1)
                {% end %}
              end
            else
              # Phase modulation from LFO is equal to zero
              (opIdx...(opIdx + 4)).each do |newIdx|
                ops[newIdx].phase = ops[newIdx].phase &+ ops[newIdx].freq
              end
            end
          else
            # Phase modulation from LFO is disabled
            (opIdx...(opIdx + 4)).each do |newIdx|
              ops[newIdx].phase = ops[newIdx].phase &+ ops[newIdx].freq
            end
          end

          opIdx += 4
          i -= 1
          break if i == 0
        end

        # CSM is calculated *after* the phase generator calculations (verified on real chip)
        # CSM keyon line seems to be ORed with the KO line inside of the chip.
        # The result is that it only works when KO (register 0x08) is off, ie. 0
        #
        # Interesting effect is that when timer A is set to 1023, the KEY ON happens
        # on every sample, so there is no KEY OFF at all - the result is that
        # the sound played is the same as after normal KEY ON.

        if @csmReq != 0
          if @csmReq == 2 # Key on
            opIdx = 0 # CH 0 M1
            op = ops[opIdx]
            i = 32
            loop do
              op.keyOn(2u32, @egCount)
              i -= 1
              break if i == 0
              opIdx += 1
              op = ops[opIdx]
            end
            @csmReq = 1
          else # Key off
            opIdx = 0 # CH 0 M1
            op = ops[opIdx]
            i = 32
            loop do
              op.keyOff(~2u32)
              i -= 1
              break if i == 0
              opIdx += 1
              op = ops[opIdx]
            end
            @csmReq = 0
          end
        end
      end
    end
  end
end
