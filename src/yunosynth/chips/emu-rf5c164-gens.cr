#### PCM RF5C164 emulator
#### Copyright (c) 2002 by Stéphane Dallongeville
#### Crystal port Copyright (c) 2023-2024 Remilia Scarlet
####
#### Written by Stéphane Dallongeville (gens@consolemul.com) */
#### Based on the Gens project

module Yuno::Chips
  # RF5C164 sound chip emulator.
  class Rf5c164 < Yuno::AbstractChip
    # Actual implementation of the C352 emulator based on Mame's implementation
    private class Rf5c164Gens < Yuno::AbstractEmulator
      PCM_STEP_SHIFT = 11
      RAM_SIZE = 64 * 1024 # 65536
      NUM_CHANNELS = 8

      private class Channel
        property env      : UInt32 = 0 # Envelope register
        property pan      : UInt32 = 0 # Pan register
        property mulL     : UInt32 = 0 # Envelope and pan product left
        property mulR     : UInt32 = 0 # Envelope and pan product right
        property stAddr   : UInt32 = 0 # Start address register
        property loopAddr : UInt32 = 0 # Loop address register
        property addr     : UInt32 = 0 # Current address register
        property step     : UInt32 = 0 # Frequency register
        property stepB    : UInt32 = 0 # Frequency register binary
        property enable   : UInt32 = 0 # Channel on/off register
        property data     : Int32  = 0 # Wave data
        property muted    : UInt8  = 0

        def initialize
        end

        def reset
          @enable = 0
          @env = 0
          @pan = 0
          @stAddr = 0
          @addr = 0
          @loopAddr = 0
          @step = 0
          @stepB = 0
          @data = 0
        end
      end

      @rate : Float64 = 0.0
      protected property smpl0Patch : Int32 = 0
      @enable : Int32 = 0
      @curChan : UInt32 = 0u32
      @bank : UInt32 = 0u32
      @channels : Array(Channel)
      @ramSize : Int32 = RAM_SIZE
      @ram : Array(UInt8) = Array(UInt8).new(RAM_SIZE, 0u8)

      def initialize(clock : UInt32)
        @channels = Array(Channel).new(NUM_CHANNELS) { |_| Channel.new }
        reset
        unless clock == 0
          @rate = (31.8 * 1024) / clock
          @channels.each do |ch|
            ch.step = (ch.stepB * @rate).to_u32!
          end
        end
      end

      def reset : Nil
        @ram.fill(0)
        @enable = 0
        @curChan = 0u32
        @bank = 0u32
        @channels.each do |ch|
          ch.reset
        end
      end

      def write(reg : UInt32, data : UInt32) : Nil
        chan = @channels[@curChan]
        data &= 0xFF

        case reg
        when 0x00 # Envelope register
          chan.env = data
          chan.mulL = (data * (chan.pan & 0x0F)) >> 5
          chan.mulR = (data * (chan.pan >> 4)) >> 5

        when 0x01 # Pan register
          chan.pan = data
          chan.mulL = ((data & 0x0F) * chan.env) >> 5
          chan.mulR = ((data >> 4) * chan.env) >> 5

        when 0x02 # Frequency step registers (low nibble)
          chan.stepB &= 0xFF00
          chan.stepB += data
          chan.step = (chan.stepB * @rate).to_u32!

        when 0x03 # Frequency step register (high nibble)
          chan.stepB &= 0x00FF
          chan.stepB += data << 8
          chan.step = (chan.stepB * @rate).to_u32!

        when 0x04 # Loop address register (low nibble)
          chan.loopAddr &= 0xFF00
          chan.loopAddr += data

        when 0x05 # Loop address register (high nibble)
          chan.loopAddr &= 0x00FF
          chan.loopAddr += data << 8

        when 0x06 # Start address register
          chan.stAddr = data << (PCM_STEP_SHIFT + 8)

        when 0x07 # Control register
          if Yuno.bitflag?(data, 0x40)
            # Select channel
            @curChan = data & 0x07
          else
            # Mod is L
            @bank = (data & 0x0F) << 12
          end

          # Sounding bit, used as a mask
          @enable = Yuno.bitflag?(data, 0x80) ? 0xFF : 0

        when 0x08 # Sound on/off register
          data ^= 0xFF

          @channels.each_with_index do |ch, idx|
            ch.addr = ch.stAddr if ch.enable == 0
            ch.enable = data & (1 << idx)
          end
        end
      end

      @[AlwaysInline]
      def memWrite(offset : Int16, data : UInt8) : Nil
        @ram[@bank | offset] = data
      end

      def writeRam(dataStart : Int32, dataLength : Int32, ramData : Slice(UInt8)) : Nil
        dataStart |= @bank
        return if dataStart >= @ramSize
        dataLength = @ramSize - dataStart if dataStart + dataLength > @ramSize

        # Copy the memory
        dataLength.times do |i|
          # This can be guaranteed by the checks above
          @ram.put!(i + dataStart, ramData.get!(i))
        end
      end

      def update(outputs : OutputBuffers, samples : UInt32) : Nil
        outL : Slice(Int32) = outputs[0]
        outR : Slice(Int32) = outputs[1]
        addr : UInt32 = 0
        j : Int32 = 0
        k : UInt32 = 0u32

        # Check once, then we can avoid bounds checks below.
        if outL.size < samples || outR.size < samples
          raise "RF5C164 update: Bad number of samples (expected #{samples}, " \
                "but buffers have #{outL.size} and #{outR.size})"
        end

        # Clear bufers
        outL.fill(0, 0, samples)
        outR.fill(0, 0, samples)

        # If the chip is disabled, just return
        return if @enable == 0

        @channels.each do |ch|
          # Only loop when playing
          if ch.enable != 0 && ch.muted == 0
            addr = ch.addr >> PCM_STEP_SHIFT

            j = 0
            while j < samples # no samples.times because j is sometimes adjusted if the channel loops
              # Test for loop signal
              if @ram[addr] == 0xFF
                addr = ch.loopAddr
                ch.addr = addr << PCM_STEP_SHIFT
                if @ram[addr] == 0xFF
                  break
                else
                  j -= 1
                end
              else
                if Yuno.bitflag?(@ram[addr], 0x80)
                  ch.data = (@ram[addr] & 0x7F).to_i32!
                  outL[j] = outL[j] &- (ch.data &* ch.mulL)
                  outR[j] = outR[j] &- (ch.data &* ch.mulR)
                else
                  ch.data = @ram[addr].to_i32!

                  # This improves the sound of Cosmic Fantasy Stories, although
                  # it's definately false behaviour.
                  if ch.data == 0 && @smpl0Patch != 0
                    ch.data -= 0x7F
                  end

                  outL[j] = outL[j] &+ (ch.data &* ch.mulL)
                  outR[j] = outR[j] &+ (ch.data &* ch.mulR)
                end

                # Update address register
                k = addr + 1
                ch.addr = (ch.addr + ch.step) & 0x7FFFFFFF
                addr = ch.addr >> PCM_STEP_SHIFT

                while k < addr
                  if @ram[k] == 0xFF
                    addr = ch.loopAddr
                    ch.addr = addr << PCM_STEP_SHIFT
                    break
                  end
                  k += 1
                end
              end

              j += 1
            end

            if @ram[addr] == 0xFF
              ch.addr = ch.loopAddr << PCM_STEP_SHIFT
            end
          end
        end
      end

      def muteMask=(mask : UInt32)
        NUM_CHANNELS.times do |idx|
          @channels.get!(idx).muted = ((mask >> idx) & 1).to_u8!
        end
      end
    end
  end
end
