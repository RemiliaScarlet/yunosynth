#### YunoSynth
#### Copyright (C) 2023-2024 Remilia Scarlet <remilia@posteo.jp>
#### Portions based on VGMPlay, Copyright (C) Valley Bell
####
#### This program is free software: you can redistribute it and/or
#### modify it under the terms of the GNU Affero General Public
#### License as published by the Free Software Foundation, either
#### version 3 of the License, or (at your option) any later version.
####
#### This program is distributed in the hope that it will be useful,
#### but WITHOUT ANY WARRANTY; without even the implied warranty of
#### MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#### Affero General Public License for more details.
####
#### You should have received a copy of the GNU Affero General Public License
#### along with this program.  If not, see <https://www.gnu.org/licenses/>.
require "./emu-ym2151-mame"

####
#### Yamaha YM2151 sound chip emulator interface
####

module Yuno::Chips
  # YM2151 sound chip emulator.
  class YM2151 < Yuno::AbstractChip
    CHIP_ID = 0x03_u32

    enum Core
      Mame
      Nuked

      @[AlwaysInline]
      def symbol : Symbol
        case self
        in .mame? then :mame
        in .nuked? then :nuked
        end
      end
    end

    @clockFromHeader : UInt32 = 0
    @chip : YM2151Mame|YM2151Nuked|Nil = nil
    @samplingMode : UInt8 = 0
    @chipSampleRate : UInt32 = 0
    @lastReg : UInt8 = 0

    def initialize(chipNum : Int32, absChipCount : Int32, vgm : VgmFile, playbackSampleRate : UInt32,
                   newSamplingMode : UInt8, newPlayerSampleRate : UInt32, *, emuCore : Core|Symbol|Nil = nil,
                   flags : ChipFlags? = nil)
      @clockFromHeader = vgm.getAlternateChipClock(chipNum, Yuno::ChipType::Ym2151) || vgm.header.ym2151Clock
      @clockFromHeader &= 0x3FFFFFFF
      @playerSampleRate = newPlayerSampleRate
      chipCount = (vgm.header.ym2151Clock & 0x40000000) != 0 ? 2 : 1
      initFields(vgm, emuCore, playbackSampleRate, newSamplingMode, chipCount)
    end

    protected def initialize
    end

    protected def initFields(vgm : VgmFile, emuCore : Core|Symbol|Nil, playbackSampleRate : UInt32,
                             newSamplingMode : UInt8, chipCount : Int32) : Nil
      @volume = vgm.getChipVolume(self, ChipType::Ym2151, chipCount)
      @samplingMode = newSamplingMode
      @core = case emuCore
              when Core then emuCore.symbol
              when Symbol then emuCore
              else YM2151.defaultEmuCore
              end
      case @core
      when :mame
        @chipSampleRate = @clockFromHeader <= 0 ? playbackSampleRate : @clockFromHeader
      when :nuked
        @chipSampleRate = @clockFromHeader <= 0 ? playbackSampleRate : @clockFromHeader
      else raise YunoError.new("Unsupported emulation core for YM2151: #{@core}")
      end
    end

    def type : ChipType
      ChipType::Ym2151
    end

    def name : String
      "Yamaha YM2151"
    end

    def shortName : String
      "YM2151"
    end

    def id : UInt32
      CHIP_ID
    end

    def emuCore : Symbol
      @core
    end

    def self.defaultEmuCore : Symbol
      :mame
    end

    def start(chipIndex : UInt8, clock : UInt32, flags : ChipFlags? = nil, previousChip : AbstractChip? = nil) : UInt32
      # Update sample rate
      @sampleRate = clock.tdiv(64)
      if (@samplingMode == 0x01 && @sampleRate < @chipSampleRate) || @samplingMode == 0x02
        @sampleRate = @chipSampleRate
      end

      case @core
      when :mame
        @chip = YM2151Mame.new(clock, @sampleRate)
        @sampleRate
      when :nuked
        ch = YM2151Nuked.new
        ch = YM2151Nuked.reset(ch, @sampleRate, clock)
        @chip = ch
        @sampleRate
      else raise YunoError.new("Unsupported emulation core for YM2151: #{@core}")
      end
    end

    def getClock : UInt32
      @clockFromHeader
    end

    def getStartFlags(vgm : VgmFile) : ChipFlags?
      nil
    end

    def update(outputs : OutputBuffers, samples : Int) : Nil
      ch = @chip.not_nil!
      case ch
      when YM2151Mame then ch.update(outputs, samples.to_u32!)
      when YM2151Nuked then ch.update(outputs, samples.to_u32!)
      end
    end

    def reset(chipIndex : UInt8) : Nil
      ch = @chip.not_nil!
      if ch.is_a?(YM2151Nuked)
        @chip = YM2151Nuked.reset(ch, 0, 0)
      else
        ch.as(YM2151Mame).reset
      end
    end

    def read(chipIndex : UInt8, offset : Int) : UInt8|UInt16|UInt32
      ch = @chip.not_nil!
      case ch
      when YM2151Mame
        if Yuno.bitflag?(offset, 1)
          ch.readStatus
        else
          0xFFu32
        end
      when YM2151Nuked
        # TODO
        0u32
      else
        raise "YM2151 private instance not accounted for in read"
      end
    end

    def write(chipIndex : UInt8, offset : Int, data : Int, port : UInt8 = 0) : Nil
      ch = @chip.not_nil!
      case ch
      when YM2151Nuked
        ch.writeBuffered(offset.to_u32!, data.to_u8!)
      when YM2151Mame
        if Yuno.bitflag?(offset, 1)
          ch.write(@lastReg.to_i32!, data.to_i32!)
        else
          @lastReg = data.to_u8!
        end
      end
    end

    def writeDac(port : Int, command : Int, data : Int) : Nil
      write(0, 0x00, command)
      write(0, 0x01, data)
    end

    @[AlwaysInline]
    def statusPortRead(chipIndex : UInt8, offset : Int) : UInt8
      read(chipIndex, 1).to_u8!
    end

    @[AlwaysInline]
    def registerPortWrite(chipIndex : UInt8, offset : Int, data : UInt8) : Nil
      write(chipIndex, 0, data)
    end

    @[AlwaysInline]
    def dataPortWrite(chipIndex : UInt8, offset : Int, data : UInt8) : Nil
      write(chipIndex, 1, data)
    end

    def setMuteMask(chipIndex : UInt8, mask : UInt32) : Nil
      @chip.not_nil!.muteMask = mask
    end

    def getVolModifier : UInt32
      @volume.to_u32
    end

    def baseVolume : UInt16
      0x100_u16
    end

    # The internal interface for the chip.
    @[AlwaysInline]
    def chip : AbstractEmulator
      @chip.not_nil!
    end
  end
end
