####                            -= Seta Hardware =-
####
####                    driver by   Luca Elia (l.elia@tin.it)
####                    rewrite by Manbow-J(manbowj@hamal.freemail.ne.jp)
####                    BSD 3-Clause License
####                    Crystal port by Remilia Scarlet (remilia@sdf.org)
####
####                    X1-010 Seta Custom Sound Chip (80 Pin PQFP)
####
#### Custom programmed Mitsubishi M60016 Gate Array, 3608 gates, 148 Max I/O ports
####
####    The X1-010 is 16 Voices sound generator, each channel gets it's
####    waveform from RAM (128 bytes per waveform, 8 bit unsigned data)
####    or sampling PCM(8bit unsigned data).
####
#### Registers:
####    8 registers per channel (mapped to the lower bytes of 16 words on the 68K)
####
####    Reg:    Bits:       Meaning:
####
####    0       7654 3---
####            ---- -2--   PCM/Waveform repeat flag (0:Ones 1:Repeat) (*1)
####            ---- --1-   Sound out select (0:PCM 1:Waveform)
####            ---- ---0   Key on / off
####
####    1       7654 ----   PCM Volume 1 (L?)
####            ---- 3210   PCM Volume 2 (R?)
####                        Waveform No.
####
####    2                   PCM Frequency
####                        Waveform Pitch Lo
####
####    3                   Waveform Pitch Hi
####
####    4                   PCM Sample Start / 0x1000           [Start/End in bytes]
####                        Waveform Envelope Time
####
####    5                   PCM Sample End 0x100 - (Sample End / 0x1000)    [PCM ROM is Max 1MB?]
####                        Waveform Envelope No.
####    6                   Reserved
####    7                   Reserved
####
####    offset 0x0000 - 0x0fff  Wave form data
####    offset 0x1000 - 0x1fff  Envelope data
####
####    *1 : when 0 is specified, hardware interrupt is caused(allways return soon)
####

module Yuno::Chips
  # ameba:disable Style/TypeNames
  class X1_010 < Yuno::AbstractChip
    # Actual implementation of the X1-010 emulator based on Mame's implementation
    # ameba:disable Style/TypeNames
    private class X1_010Mame < Yuno::AbstractEmulator
      NUM_CHANNELS = 16

      FREQ_BASE_BITS = 14  # Frequency fixed decimal shift bits
      ENV_BASE_BITS = 16 # Wave form envelope fixed decimal shift bits
      VOL_BASE = (2 * 32 * 256).tdiv(30) # Volume base

      struct Channel
        property status    : UInt8 = 0u8
        property volume    : UInt8 = 0u8 # volume / wave form no.
        property frequency : UInt8 = 0u8 # frequency / pitch lo
        property pitchHi   : UInt8 = 0u8 # reserved / pitch hi
        property start     : UInt8 = 0u8 # start address / envelope time
        property finish    : UInt8 = 0u8 # end address / envelope no.
        property reserve1  : UInt8 = 0u8
        property reserve2  : UInt8 = 0u8

        def initialize
        end
      end

      @rate : UInt32 = 0 # Output sampling rate (Hz)
      @rom : Bytes = Bytes.new(0)
      @reg : Bytes = Bytes.new(0x2000) # X1-010 Register & wave form area
      @smpOffset : Array(UInt32) = Array(UInt32).new(NUM_CHANNELS, 0u32)
      @envOffset : Array(UInt32) = Array(UInt32).new(NUM_CHANNELS, 0u32)
      @baseClock : UInt32 = 0u32
      @muted : Array(Bool) = Array(Bool).new(NUM_CHANNELS, false)

      def initialize(@baseClock : UInt32, @rate : UInt32)
        Yuno.dlog!("Base clock for X1-010: #{@baseClock}")
        Yuno.dlog!("Sample rate for X1-010: #{@rate}")
      end

      def reset : Nil
        @reg.fill(0u8)
        @smpOffset.fill(0u32)
        @envOffset.fill(0u32)
      end

      @[AlwaysInline]
      def read(offset : Int) : UInt8
        @reg[offset]
      end

      @[AlwaysInline]
      def write(offset : Int, data : UInt8) : Nil
        channel = offset.tdiv(sizeof(Channel))
        reg = offset % sizeof(Channel)

        if channel < NUM_CHANNELS && reg == 0 && !Yuno.bitflag?(@reg[offset], 1) && Yuno.bitflag?(data, 1)
          @smpOffset.put!(channel, 0u32)
          @envOffset.put!(channel, 0u32)
        end

        # We've already accessed @reg[offset], so we're safe
        @reg.put!(offset, data)
      end

      def writeRom(romSize : Int32, dataStart : Int32, dataLength : Int32, romData : Slice(UInt8)) : Nil
        if @rom.size != romSize
          @rom = Bytes.new(romSize, 0xFF_u8)
        end

        return if dataStart > romSize
        dataLength = romSize - dataStart if dataStart + dataLength > romSize

        # Copy the memory
        dataLength.times do |i|
          # This can be guaranteed by the checks above
          @rom.unsafe_put(i + dataStart, romData.unsafe_fetch(i))
        end
      end

      @[AlwaysInline]
      def update(outputs : OutputBuffers, samples : UInt32) : Nil
        outL : Slice(Int32) = outputs[0]
        outR : Slice(Int32) = outputs[1]
        outLPos : Int32 = 0
        outRPos : Int32 = 0
        start : Pointer(Int8) = Pointer(Int8).null
        finish : Pointer(Int8) = Pointer(Int8).null
        env : Pointer(UInt8) = Pointer(UInt8).null
        div : UInt8 = 0u8
        vol : UInt8 = 0u8
        volL : Int32 = 0
        volR : Int32 = 0
        smpOffs : UInt32 = 0u32
        smpStep : UInt32 = 0u32
        envOffs : UInt32 = 0u32
        envStep : UInt32 = 0u32
        freq : UInt32 = 0u32
        delta : UInt32 = 0u32
        data : Int8 = 0i8

        # Check once, then we can avoid bounds checks below.
        if outL.size < samples || outR.size < samples
          raise "X1-010 update: Bad number of samples (expected #{samples}, " \
                "but buffers have #{outL.size} and #{outR.size})"
        end

        # Clear the buffers
        outL.fill(0, 0, samples)
        outR.fill(0, 0, samples)

        NUM_CHANNELS.times do |ch|
          # Remi: Get a pointer to a Channel using the bytes in @reg.  We need
          # this to be a pointer because the data in @reg might be updated
          # below.
          reg : Pointer(Channel) = (@reg.to_unsafe + (ch * sizeof(Channel))).unsafe_as(Pointer(Channel))

          if Yuno.bitflag?(reg[0].status, 1) && !@muted.get!(ch)
            outLPos = 0
            outRPos = 0
            # Key on
            div = Yuno.bitflag?(reg[0].status, 0x80) ? 1u8 : 0u8
            if !Yuno.bitflag?(reg[0].status, 2)
              # PCM sampling
              start = (@rom.to_unsafe + (reg[0].start.to_u32! * 0x1000)).unsafe_as(Pointer(Int8))
              finish = (@rom.to_unsafe + ((0x100 - reg[0].finish) * 0x1000)).unsafe_as(Pointer(Int8))
              volL = ((reg[0].volume >> 4) & 0xF).to_i32! * VOL_BASE
              volR = (reg[0].volume & 0xF).to_i32! * VOL_BASE
              smpOffs = @smpOffset.get!(ch)
              freq = (reg[0].frequency >> div).to_u32!

              # Meta Fox does write the frequency register, but this is a hack
              # to make it "work" with the current setup.  This is broken for
              # Arbalester (it writes 8), but that'll be fixed later.
              freq = 4u32 if freq == 0
              smpStep = (@baseClock / 8192.0 * freq * (1 << FREQ_BASE_BITS) / @rate + 0.5).to_u32!

              samples.times do |_|
                delta = smpOffs >> FREQ_BASE_BITS
                # Sample ended?
                if (start + delta) >= finish
                  reg[0].status &= ~0x01 # Key off
                  break
                end

                data = (start + delta).value
                outL.incf!(outLPos, (data.to_i32! * volL).tdiv(256))
                outR.incf!(outRPos, (data.to_i32! * volR).tdiv(256))
                outLPos += 1
                outRPos += 1
                smpOffs = smpOffs &+ smpStep
              end

              @smpOffset.put!(ch, smpOffs)

            else # !Yuno.bitflag?(reg[0].status, 2)
              # Wave form
              start = (@reg.to_unsafe + (reg[0].volume.to_u32! * 128 + 0x1000)).unsafe_as(Pointer(Int8))
              smpOffs = @smpOffset.get!(ch)
              freq = ((reg[0].pitchHi.to_u32! << 8) + reg[0].frequency) >> div
              smpStep = (@baseClock / 128.0 / 1024.0 / 4.0 * freq * (1 << FREQ_BASE_BITS) / @rate + 0.5).to_u32!
              env = @reg.to_unsafe + (reg[0].finish.to_u32! * 128)
              envOffs = @envOffset.get!(ch)
              envStep = (@baseClock / 128.0 / 1024.0 / 4.0 * reg[0].start * (1 << ENV_BASE_BITS) / @rate + 0.5).to_u32!

              samples.times do |_|
                delta = envOffs >> ENV_BASE_BITS

                # Envelope one shot mode
                if Yuno.bitflag?(reg[0].status, 4) && delta >= 0x80
                  reg[0].status &= ~0x01 # Key off
                  break
                end

                vol = (env + (delta & 0x7F)).value
                volL = ((vol >> 4) & 0xF).to_i32! * VOL_BASE
                volR = (vol & 0xF).to_i32! * VOL_BASE
                data = (start + ((smpOffs >> FREQ_BASE_BITS) & 0x7F)).value
                outL.incf!(outLPos, (data.to_i32! &* volL).tdiv(256))
                outR.incf!(outRPos, (data.to_i32! &* volR).tdiv(256))
                outLPos += 1
                outRPos += 1
                smpOffs = smpOffs &+ smpStep
                envOffs = envOffs &+ envStep
              end

              @smpOffset.put!(ch, smpOffs)
              @envOffset.put!(ch, envOffs)
            end
          end
        end
      end

      def muteMask=(mask : UInt32)
        NUM_CHANNELS.times do |i|
          @muted[i] = Yuno.bitflag?(mask >> i, 1)
        end
      end
    end
  end
end
