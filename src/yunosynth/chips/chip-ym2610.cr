#### YunoSynth
#### Copyright (C) 2023-2024 Remilia Scarlet <remilia@posteo.jp>
#### Based on VGMPlay, Copyright (C) Valley Bell
####
#### This program is free software: you can redistribute it and/or
#### modify it under the terms of the GNU Affero General Public
#### License as published by the Free Software Foundation, either
#### version 3 of the License, or (at your option) any later version.
####
#### This program is distributed in the hope that it will be useful,
#### but WITHOUT ANY WARRANTY; without even the implied warranty of
#### MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#### Affero General Public License for more details.
####
#### You should have received a copy of the GNU Affero General Public License
#### along with this program.  If not, see <https://www.gnu.org/licenses/>.
require "./emu-opn-mame/ym2610-mame"
require "./chip-ay8910"

####
#### Yamaha YM2610 sound chip emulator interface
####

module Yuno::Chips
  # YM2610 sound chip emulator.
  class YM2610 < Yuno::AbstractChip
    CHIP_ID = 0x08_u32

    @clockFromHeader : UInt32 = 0
    @chip : YM2610Mame? = nil
    @samplingMode : UInt8 = 0
    @chipSampleRate : UInt32 = 0
    @ayClock : UInt32 = 0
    @psgIntf : OPNMame::SsgCallbacks = OPNMame::SsgCallbacks.new
    @is2610B : Bool = false

    def initialize(chipNum : Int32, absChipCount : Int32, vgm : VgmFile, playbackSampleRate : UInt32,
                   newSamplingMode : UInt8, newPlayerSampleRate : UInt32, *, emuCore : Symbol? = nil,
                   flags : ChipFlags? = nil)
      @clockFromHeader = vgm.getAlternateChipClock(chipNum, Yuno::ChipType::Ym2610) || vgm.header.ym2610Clock
      @clockFromHeader &= 0xBFFFFFFF
      @playerSampleRate = newPlayerSampleRate
      @is2610B = (vgm.header.ym2610Clock & 0x80000000) != 0
      chipCount = (vgm.header.ym2610Clock & 0x40000000) != 0 ? 2 : 1
      initFields(vgm, emuCore, playbackSampleRate, newSamplingMode, chipCount)
    end

    protected def initialize
    end

    protected def initFields(vgm : VgmFile, emuCore : Symbol?, playbackSampleRate : UInt32, newSamplingMode : UInt8,
                             chipCount : Int32) : Nil
      @volume = vgm.getChipVolume(self, ChipType::Ym2610, chipCount)
      @core = emuCore || YM2610.defaultEmuCore
      case @core
      when :mame
        @samplingMode = newSamplingMode
        @chipSampleRate = @clockFromHeader <= 0 ? playbackSampleRate : @clockFromHeader

        # The YM2610 has an internal YM2149.
        # KLUDGE pass 0 as the current chip number and absChipCount
        paired = Ay8910.new(0, 0, vgm, playbackSampleRate, newSamplingMode, @playerSampleRate,
                            # We always want the Emu2149 core.
                            emuCore: :emu2149)
        self.isPaired = true
        paired.updateFn = ->self.updatePaired(OutputBuffers, UInt32)
        paired.volume = vgm.getChipVolume(self, ChipType::Ym2610, chipCount, ChipType::Ay8910)
        @paired = paired

        @psgIntf = OPNMame::SsgCallbacks.new(->self.psgClock=(UInt32), ->self.psgWrite(Int32, Int32),
                                             ->self.psgRead, ->self.psgReset)
      else raise YunoError.new("Unsupported emulation core for YM2610: #{@core}")
      end
    end

    def type : ChipType
      ChipType::Ym2610
    end

    def name : String
      if @is2610B
        "Yamaha YM2610B"
      else
        "Yamaha YM2610"
      end
    end

    def shortName : String
      if @is2610B
        "YM2610B"
      else
        "YM2610"
      end
    end

    def id : UInt32
      CHIP_ID
    end

    def emuCore : Symbol
      @core
    end

    def self.defaultEmuCore : Symbol
      :mame
    end

    def start(chipIndex : UInt8, clock : UInt32, flags : ChipFlags? = nil, previousChip : AbstractChip? = nil) : UInt32
      # Update sample rate
      clock &= 0x7FFFFFFF
      @sampleRate = clock.tdiv(72)
      if (@samplingMode == 0x01 && @sampleRate < @chipSampleRate) || @samplingMode == 0x02
        @sampleRate = @chipSampleRate
      end

      ayClock = clock.tdiv(4)
      ayRate = ayClock.tdiv(8)
      @paired.as(Ay8910).startYm(ayClock, ayRate, flags.as?(Ay8910::Ay8910Flags))

      case @core
      when :mame
        @chip = YM2610Mame.new(->self.updateRequest, self, clock & 0x7FFFFFFF, @sampleRate, @psgIntf)
        #@chip.not_nil!.setSampleRateChangeCB(->AbstractChip.changeChipSampleRate(AbstractChip, UInt32),
        #                                     @paired.not_nil!)
      else raise YunoError.new("Unsupported emulation core for YM2610: #{@core}")
      end

      @sampleRate
    end

    def getClock : UInt32
      @clockFromHeader
    end

    def getStartFlags(vgm : VgmFile) : ChipFlags?
      Ay8910::Ay8910Flags.new(vgm.header.ay8910Flags)
    end

    def update(outputs : OutputBuffers, samples : Int) : Nil
      unless @is2610B
        @chip.not_nil!.update(outputs, samples.to_u32!)
      else
        @chip.not_nil!.update2610B(outputs, samples.to_u32!)
      end
    end

    @[AlwaysInline]
    def updateRequest : Nil
      @chip.not_nil!.update2610B(Yuno::FAKE_BUF, 0)
    end

    def updatePaired(outputs : OutputBuffers, samples : Int) : Nil
      psgUpdate(outputs, samples)
    end

    def reset(chipIndex : UInt8) : Nil
      @chip.not_nil!.reset
    end

    def read(chipIndex : UInt8, offset : Int) : UInt8|UInt16|UInt32
      @chip.not_nil!.read(offset & 3)
    end

    def write(chipIndex : UInt8, offset : Int, data : Int, port : UInt8 = 0) : Nil
      @chip.not_nil!.write(offset & 3, data.to_u8!)
    end

    @[AlwaysInline]
    def readPortRead(chipIndex : UInt8, offset : Int) : UInt8
      read(chipIndex, 1).to_u8!
    end

    @[AlwaysInline]
    def statusPortARead(chipIndex : UInt8, offset : Int) : UInt8
      read(chipIndex, 0).to_u8!
    end

    @[AlwaysInline]
    def statusPortBRead(chipIndex : UInt8, offset : Int) : UInt8
      read(chipIndex, 2).to_u8!
    end

    @[AlwaysInline]
    def controlPortAWrite(chipIndex : UInt8, offset : Int, data : Int) : Nil
      write(chipIndex, 0, data)
    end

    @[AlwaysInline]
    def controlPortBWrite(chipIndex : UInt8, offset : Int, data : Int) : Nil
      write(chipIndex, 2, data)
    end

    @[AlwaysInline]
    def dataPortAWrite(chipIndex : UInt8, offset : Int, data : Int) : Nil
      write(chipIndex, 1, data)
    end

    @[AlwaysInline]
    def dataPortBWrite(chipIndex : UInt8, offset : Int, data : Int) : Nil
      write(chipIndex, 3, data)
    end

    def setMuteMask(chipIndex : UInt8, mask : UInt32) : Nil
      raise "Unimplemented, use Yuno::Chips::YM2610#setMuteMask(UInt8, UInt32, UInt32)"
    end

    @[AlwaysInline]
    def setMuteMask(chipIndex : UInt8, mask : UInt32, maskAY : UInt32) : Nil
      @chip.not_nil!.muteMask = mask
      if paired = @paired
        paired.muteMask = maskAY
      end
    end

    def getVolModifier : UInt32
      @volume.to_u32 + @paired.not_nil!.volume
    end

    def baseVolume : UInt16
      0x80_u16
    end

    @[AlwaysInline]
    def psgClock=(value : UInt32) : Nil
      @paired.not_nil!.as(Ay8910).clock = value
    end

    @[AlwaysInline]
    def psgWrite(address : Int, data : Int)
      @paired.not_nil!.as(Ay8910).writeIO(address, data)
    end

    @[AlwaysInline]
    def psgRead : UInt8
      @paired.not_nil!.as(Ay8910).readIO
    end

    @[AlwaysInline]
    def psgReset : Nil
      @paired.not_nil!.as(Ay8910).reset(1)
    end

    @[AlwaysInline]
    def psgUpdate(outputs : OutputBuffers, samples : Int) : Nil
      @paired.not_nil!.update(outputs, samples.to_u32!)
    end

    @[AlwaysInline]
    def psgStereoMask=(mask : UInt32) : Nil
      @paired.not_nil!.chip.as(Ay8910::Ay8910Emu2149).stereoMask = mask
    end

    # The internal interface for the chip.
    @[AlwaysInline]
    def chip : AbstractEmulator
      @chip.not_nil!
    end

    @[AlwaysInline]
    def writeRom(romID : UInt8, romSize : Int32, dataStart : Int32, dataLength : Int32, romData : Slice(UInt8)) : Nil
      @chip.not_nil!.writePcmRom(romID, romSize, dataStart, dataLength, romData)
    end
  end
end
