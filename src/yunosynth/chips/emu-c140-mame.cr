#### Namco C140 Emulator
#### Copyright (C) 2002-2006 R. Belmont
#### Copyright (C) 2023-2024 Remilia Scarlet
#### BSD 3-Clause License
####
#### Simulator based on AMUSE sources.
#### The C140 sound chip is used by Namco System 2 and System 21
#### The 219 ASIC (which incorporates a modified C140) is used by Namco NA-1 and NA-2
#### This chip controls 24 channels (C140) or 16 (219) of PCM.
#### 16 bytes are associated with each channel.
#### Channels can be 8 bit signed PCM, or 12 bit signed PCM.
####
#### Timer behavior is not yet handled.
####
#### Unmapped registers:
####     0x1f8:timer interval?   (Nx0.1 ms)
####     0x1fa:irq ack? timer restart?
####     0x1fe:timer switch?(0:off 1:on)
####
#### --------------
####
####     ASIC "219" notes
####
####     On the 219 ASIC used on NA-1 and NA-2, the high registers have the following
####     meaning instead:
####     0x1f7: bank for voices 0-3
####     0x1f1: bank for voices 4-7
####     0x1f3: bank for voices 8-11
####     0x1f5: bank for voices 12-15
####
####     Some games (bkrtmaq, xday2) write to 0x1fd for voices 12-15 instead.  Probably the bank registers
####     mirror at 1f8, in which case 1ff is also 0-3, 1f9 is also 4-7, 1fb is also 8-11, and 1fd is also 12-15.
####
####     Each bank is 0x20000 (128k), and the voice addresses on the 219 are all multiplied by 2.
####     Additionally, the 219's base pitch is the same as the C352's (42667).  But these changes
####     are IMO not sufficient to make this a separate file - all the other registers are
####     fully compatible.
####
####     Finally, the 219 only has 16 voices.
####
####     2000.06.26  CAB     fixed compressed pcm playback
####     2002.07.20  R.Belmont   added support for multiple banking types
####     2006.01.08  R.Belmont   added support for NA-1/2 "219" derivative
####     2023.03.15  Remilia Scarlet   Ported to Crystal

module Yuno::Chips
  class C140 < Yuno::AbstractChip
    # Actual implementation of the C140 emulator based on Mame's implementation
    private class C140Mame < Yuno::AbstractEmulator
      MAX_VOICES = 24

      ASIC_219_BANKS = [ 0x1F7_i16, 0x1F1_i16, 0x1F3_i16, 0x1F5_i16 ]

      enum BankingType
        System2  = 0
        System21 = 1
        Asic219  = 2
      end

      struct Registers
        property volumeRight : UInt8 = 0
        property volumeLeft : UInt8 = 0
        property frequencyMsb : UInt8 = 0
        property frequencyLsb : UInt8 = 0
        property bank : UInt8 = 0
        property mode : UInt8 = 0
        property startMsb : UInt8 = 0
        property startLsb : UInt8 = 0
        property endMsb : UInt8 = 0
        property endLsb : UInt8 = 0
        property loopMsb : UInt8 = 0
        property loopLsb : UInt8 = 0

        def initialize
        end

        def initialize(registers : Array(UInt8), start : Int)
          # Remi: This replaces an unsafe cast of a uint8 slice to one of these.
          @volumeRight  = registers[start]
          @volumeLeft   = registers[start + 1]
          @frequencyMsb = registers[start + 2]
          @frequencyLsb = registers[start + 3]
          @bank         = registers[start + 4]
          @mode         = registers[start + 5]
          @startMsb     = registers[start + 6]
          @startLsb     = registers[start + 7]
          @endMsb       = registers[start + 8]
          @endLsb       = registers[start + 9]
          @loopMsb      = registers[start + 10]
          @loopLsb      = registers[start + 11]
        end
      end

      class Voice
        property ptOffset : Int32 = 0
        property pos : Int32 = 0
        property key : Int32 = 0

        ###
        ### Work
        ###

        property lastDt : Int32 = 0
        property prevDt : Int32 = 0
        property dltDt : Int32 = 0

        ###
        ### Reg
        ###

        property rvol : Int32 = 0
        property lvol : Int32 = 0
        property frequency : Int32 = 0
        property bank : Int32 = 0
        property mode : Int32 = 0

        property sampleStart : Int32 = 0
        property sampleEnd : Int32 = 0
        property sampleLoop : Int32 = 0
        property muted : UInt8 = 0

        def initialize
        end
      end

      @sampleRate : UInt32 = 0
      @bankingType : BankingType = BankingType::System2

      @baseRate : UInt32 = 0
      @promSize : UInt32 = 0
      @prom : Bytes = Bytes.new(0)
      @reg : Array(UInt8) = Array(UInt8).new(0x200, 0u8)

      @pcmTable : Array(Int16) = Array(Int16).new(8, 0i16) # 2000.06.26 CAB

      @voices : Array(Voice)

      def initialize(@baseRate : UInt32, @sampleRate : UInt32, @bankingType : BankingType)
        @voices = Array(Voice).new(MAX_VOICES) { |_| Voice.new }

        segBase = 0
        8.times do |i|
          @pcmTable[i] = segBase.to_i16! # Segment base value
          segBase += (16 << i)
        end

        unmuteAll
      end

      protected def rate
        @sampleRate
      end

      @[AlwaysInline]
      def read(offset : Int) : UInt8
        @reg[offset & 0x1FF]
      end

      @[AlwaysInline]
      def write(offset : Int32, data : UInt8) : Nil
        offset &= 0x1FF

        # mirror the bank registers on the 219, fixes bkrtmaq (and probably
        # xday2 based on notes in the HLE)
        if offset >= 0x1F8 && @bankingType.asic219?
          offset -= 8
        end

        @reg[offset] = data
        if offset < 0x180
          v = @voices[offset >> 4]

          if (offset & 0xF) == 0x5
            if Yuno.bitflag?(data, 0x80)
              vreg = Registers.new(@reg, offset & 0x1F0)
              v.key      = 1
              v.ptOffset = 0
              v.pos      = 0
              v.lastDt   = 0
              v.prevDt   = 0
              v.dltDt    = 0
              v.bank     = vreg.bank.to_i32!
              v.mode     = data.to_i32!

              # on the 219 asic, addresses are in words
              if @bankingType.asic219?
                v.sampleLoop  = (vreg.loopMsb.to_i32!  * 256 + vreg.loopLsb.to_i32!)  * 2
                v.sampleStart = (vreg.startMsb.to_i32! * 256 + vreg.startLsb.to_i32!) * 2
                v.sampleEnd   = (vreg.endMsb.to_i32!   * 256 + vreg.endLsb.to_i32!)   * 2
              else
                v.sampleLoop  = vreg.loopMsb.to_i32!  * 256 + vreg.loopLsb.to_i32!
                v.sampleStart = vreg.startMsb.to_i32! * 256 + vreg.startLsb.to_i32!
                v.sampleEnd   = vreg.endMsb.to_i32!   * 256 + vreg.endLsb.to_i32!
              end
            else
              v.key = 0
            end
          end # if (offset & 0xF) == 0x5
        end # if offset < 0x180
      end

      # find_sample: compute the actual address of a sample given it's address
      # and banking registers, as well as the board type.
      #
      # I suspect in "real life" this works like the Sega MultiPCM where the
      # banking is done by a small PAL or GAL external to the sound chip, which
      # can be switched per-game or at least per-PCB revision as addressing
      # range needs grow.
      @[AlwaysInline]
      private def findSample(adrs : Int32, bank : Int32, voice : Int) : Int32
        adrs = (bank << 16) + adrs
        case @bankingType
        in .system2?
          ((adrs & 0x200000) >> 2) | (adrs & 0x7FFFF)
        in .system21?
          # similar to System 2's.
          ((adrs & 0x300000) >> 1) + (adrs & 0x7FFFF)
        in .asic219?
          # ASIC219's banking is fairly simple
          ((@reg[ASIC_219_BANKS[voice.tdiv(4)]] & 0x03).to_i32! * 0x20000) + adrs
        end
      end

      def reset : Nil
        @reg.fill(0u8)
        @voices = Array(Voice).new(MAX_VOICES) { |_| Voice.new }
      end

      @[AlwaysInline]
      def muteMask=(mask : UInt32) : Nil
        MAX_VOICES.times do |i|
          @voices[i].muted = ((mask >> i) & 0x01).to_u8!
        end
      end

      @[AlwaysInline]
      def unmuteAll : Nil
        MAX_VOICES.times { |i| @voices[i].muted = 0 }
      end

      def writeRom(romSize : Int32, dataStart : Int32, dataLength : Int32, romData : Slice(UInt8)) : Nil
        if @prom.size != romSize
          @prom = Bytes.new(romSize, 0xFF_u8)
          @promSize = romSize.to_u32!
        end

        return if dataStart > romSize
        dataLength = romSize - dataStart if dataStart + dataLength > romSize

        # Copy the memory
        dataLength.times do |i|
          # This can be guaranteed by the checks above
          @prom.unsafe_put(i + dataStart, romData.unsafe_fetch(i))
        end
      end

      @[AlwaysInline]
      def update(outputs : OutputBuffers, samples : UInt32) : Nil
        return if @prom.empty?

        outL : Slice(Int32) = outputs[0]
        outR : Slice(Int32) = outputs[1]
        rvol : Int32 = 0
        lvol : Int32 = 0
        dt : Int32 = 0
        sdt : Int32 = 0
        start : Int32 = 0
        finish : Int32 = 0
        size : Int32 = 0
        frequency : Int32 = 0
        delta : Int32 = 0
        offset : Int32 = 0
        pos : Int32 = 0
        count : Int32 = 0
        voiceCount : Int32 = 0
        lastDt : Int32 = 0
        prevDt : Int32 = 0
        dltDt : Int32 = 0
        pbase : Float32 = (@baseRate.to_f32! * 2.0_f32) / @sampleRate.to_f32!
        outPos : Int32 = 0

        # Check once, then we can avoid bounds checks below.
        if outL.size < samples || outR.size < samples
          raise "C140 update: Bad number of samples (expected #{samples}, " \
                "but buffers have #{outL.size} and #{outR.size})"
        end

        samples = @sampleRate if samples > @sampleRate

        # Clear the buffers
        outL.fill(0, 0, samples)
        outR.fill(0, 0, samples)

        # Determine how many voices need updating.
        voiceCount = @bankingType.asic219? ? 16 : 24

        # Render
        voiceCount.times do |i|
          voice : Voice = @voices[i]
          vreg : Registers = Registers.new(@reg, i * 16)

          # Only consider active voices
          if voice.key != 0 && voice.muted == 0
            frequency = vreg.frequencyMsb.to_i32! * 256 + vreg.frequencyLsb.to_i32!

            # Move to next voice if there is no frequency value set.
            next if frequency == 0

            # Delta = frequency * ((8MHz/374)*2 / sample rate)
            delta = (frequency.to_f32! * pbase).to_i32!

            # Calculate left and right channel volumes
            lvol = (vreg.volumeLeft.to_i32! * 32).tdiv(MAX_VOICES)  # 32 ch -> 24 ch
            rvol = (vreg.volumeRight.to_i32! * 32).tdiv(MAX_VOICES)

            # Reset output position
            outPos = 0

            # Retrieve sample start/end and calculate size.
            start = voice.sampleStart
            finish = voice.sampleEnd
            size = finish - start

            # Retrieve base pointer to the sample data
            pSampleData = Pointer(Int8).new(@prom.to_unsafe.address) + findSample(start, voice.bank, i)

            # Fetch back previous data pointers
            offset = voice.ptOffset
            pos = voice.pos
            lastDt = voice.lastDt
            prevDt = voice.prevDt
            dltDt = voice.dltDt

            # Switch on data type - Compressed PCM is only for C140.
            if Yuno.bitflag?(voice.mode, 8) && !@bankingType.asic219?
              # Compressed PCM (maybe correct...)
              # Loop for enough to fill sample buffer as requested.
              samples.times do |_|
                offset += delta
                count = (offset >> 16) & 0x7FFF
                offset &= 0xFFFF
                pos += count

                # Check for end of the sample
                if pos >= size
                  # Check if it's a looping sample, then either stop or loop.
                  if Yuno.bitflag?(voice.mode, 0x10)
                    pos = voice.sampleLoop - start
                  else
                    voice.key = 0
                    break
                  end
                end

                # Read the chosen sample byte.
                dt = pSampleData[pos].to_i32!

                # Decompress to 13-bit range (2000.06.26 CAB)
                sdt = dt >> 3 # Signed
                sdt = if sdt < 0
                        (sdt << (dt & 7)) - @pcmTable[dt & 7]
                      else
                        (sdt << (dt & 7)) + @pcmTable[dt & 7]
                      end
                prevDt = lastDt
                lastDt = sdt
                dltDt = lastDt - prevDt

                # Calculate the sample value
                dt = ((dltDt * offset) >> 16) + prevDt

                # Write the data to the sample buffers
                outL.unsafe_put(outPos, outL.unsafe_fetch(outPos) + ((dt * lvol) >> 10))
                outR.unsafe_put(outPos, outR.unsafe_fetch(outPos) + ((dt * rvol) >> 10))
                outPos += 1
              end
            else
              # Linear 8-bit signed PCM
              samples.times do |_|
                offset += delta
                count = (offset >> 16) & 0x7FFF
                offset &= 0xFFFF
                pos += count

                # Check for the end of the sample
                if pos >= size
                  # Check if it's a looping sample, then either stop or loop.
                  if Yuno.bitflag?(voice.mode, 0x10)
                    pos = voice.sampleLoop - start
                  else
                    voice.key = 0
                    break
                  end
                end

                if count != 0
                  prevDt = lastDt

                  if @bankingType.asic219?
                    lastDt = pSampleData[pos ^ 0x01].to_i32!

                    # Sign + magnitude format
                    if Yuno.bitflag?(voice.mode, 0x01) && Yuno.bitflag?(lastDt, 0x80)
                      lastDt = -(lastDt & 0x7F)
                    end

                    # Sign flip
                    if Yuno.bitflag?(voice.mode, 0x40)
                      lastDt = -lastDt
                    end
                  else
                    lastDt = pSampleData[pos].to_i32!
                  end

                  dltDt = lastDt - prevDt
                end # if count != 0

                # Calculate the sample value
                dt = ((dltDt * offset) >> 16) + prevDt

                # Write the data to the sample buffers
                outL.unsafe_put(outPos, outL.unsafe_fetch(outPos) + ((dt * lvol) >> 5))
                outR.unsafe_put(outPos, outR.unsafe_fetch(outPos) + ((dt * rvol) >> 5))
                outPos += 1
              end
            end # if Yuno.bitflag?(voice.mode, 8) && !@bankingType.asic219?

            # Save positional data for next callback.
            voice.ptOffset = offset
            voice.pos = pos
            voice.lastDt = lastDt
            voice.prevDt = prevDt
            voice.dltDt = dltDt
          end # if voice.key != 0 ....
        end

        # Final volume scaling
        samples.times do |i|
          outL.unsafe_put(i, outL.unsafe_fetch(i) * 8)
          outR.unsafe_put(i, outR.unsafe_fetch(i) * 8)
        end
      end
    end
  end
end
