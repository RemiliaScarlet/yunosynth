#### YunoSynth
#### Copyright (C) 2023-2024 Remilia Scarlet <remilia@posteo.jp>
#### Based on VGMPlay, Copyright (C) Valley Bell
####
#### This program is free software: you can redistribute it and/or
#### modify it under the terms of the GNU Affero General Public
#### License as published by the Free Software Foundation, either
#### version 3 of the License, or (at your option) any later version.
####
#### This program is distributed in the hope that it will be useful,
#### but WITHOUT ANY WARRANTY; without even the implied warranty of
#### MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#### Affero General Public License for more details.
####
#### You should have received a copy of the GNU Affero General Public License
#### along with this program.  If not, see <https://www.gnu.org/licenses/>.
require "./emu-opn-mame/ym2612-mame"
require "./chip-ay8910"

####
#### Yamaha YM2612 sound chip emulator interface
####

module Yuno::Chips
  # YM2612 sound chip emulator.
  class YM2612 < Yuno::AbstractChip
    CHIP_ID = 0x02_u32

    class YM2612Flags < ChipFlags
      property flags : UInt8 = 0u8 # Set to 0x0003u8 to boost the wave channel

      def initialize
      end
    end

    @clockFromHeader : UInt32 = 0
    @chip : YM2612Mame? = nil
    @samplingMode : UInt8 = 0
    @chipSampleRate : UInt32 = 0
    @subType : UInt8 = 0

    def initialize(chipNum : Int32, absChipCount : Int32, vgm : VgmFile, playbackSampleRate : UInt32,
                   newSamplingMode : UInt8, newPlayerSampleRate : UInt32, *, emuCore : Symbol? = nil,
                   flags : ChipFlags? = nil)
      @clockFromHeader = vgm.getAlternateChipClock(chipNum, Yuno::ChipType::Ym2612) || vgm.header.ym2612Clock

      # Remi: Apparently not all VGM files set this bit correctly...
      @subType = Yuno.bitflag?(@clockFromHeader, 0x80000000) ? 0x01_u8 : 0x00_u8

      @clockFromHeader &= 0xBFFFFFFF
      @playerSampleRate = newPlayerSampleRate
      chipCount = Yuno.bitflag?(vgm.header.ym2612Clock, 0x40000000) ? 2 : 1
      initFields(vgm, emuCore, playbackSampleRate, newSamplingMode, chipCount)
    end

    protected def initialize
    end

    protected def initFields(vgm : VgmFile, emuCore : Symbol?, playbackSampleRate : UInt32, newSamplingMode : UInt8,
                             chipCount : Int32) : Nil
      @volume = vgm.getChipVolume(self, ChipType::Ym2612, chipCount)
      @core = emuCore || YM2612.defaultEmuCore
      case @core
      when :mame
        @samplingMode = newSamplingMode
        @chipSampleRate = @clockFromHeader <= 0 ? playbackSampleRate : @clockFromHeader

      else raise YunoError.new("Unsupported emulation core for YM2612: #{@core}")
      end
    end

    def type : ChipType
      ChipType::Ym2612
    end

    def name : String
      case @subType
      when 0x01 then "Yamaha YM3438"
      else "Yamaha YM2612"
      end
    end

    def shortName : String
      case @subType
      when 0x01 then "YM3438"
      else "YM2612"
      end
    end

    def id : UInt32
      CHIP_ID
    end

    def emuCore : Symbol
      @core
    end

    def self.defaultEmuCore : Symbol
      :mame
    end

    def start(chipIndex : UInt8, clock : UInt32, flags : ChipFlags? = nil, previousChip : AbstractChip? = nil) : UInt32
      raise "YM2612 requires flags to start" unless flags && flags.is_a?(YM2612Flags)
      Yuno.dlog!("Start flags for YM2612: #{flags}")

      # Update sample rate
      clock &= 0x3FFFFFFF

      @sampleRate = clock.tdiv(72)
      if !(@core == :mame && Yuno.bitflag?(flags.as(YM2612Flags).flags, 0x04))
        Yuno.dlog!("Double-rate for YM2612!")
        @sampleRate = @sampleRate.tdiv(2)
      end

      if (@samplingMode == 0x01 && @sampleRate < @chipSampleRate) || @samplingMode == 0x02
        @sampleRate = @chipSampleRate
      end

      Yuno.dlog!("YM2612 Clock: #{clock}, Sample Rate: #{@sampleRate}")
      case @core
      when :mame
        @chip = YM2612Mame.new(->self.updateRequest, self, clock, @sampleRate, flags.as(YM2612Flags).flags)
        #@chip.not_nil!.setSampleRateChangeCB(->AbstractChip.changeChipSampleRate(AbstractChip, UInt32),
        #                                     @paired.not_nil!)
      else raise YunoError.new("Unsupported emulation core for YM2612: #{@core}")
      end

      @sampleRate
    end

    def getClock : UInt32
      @clockFromHeader
    end

    def getStartFlags(vgm : VgmFile) : ChipFlags?
      YM2612Flags.new
    end

    def update(outputs : OutputBuffers, samples : Int) : Nil
      @chip.not_nil!.update(outputs, samples.to_u32!)
    end

    @[AlwaysInline]
    def updateRequest : Nil
      @chip.not_nil!.update(Yuno::FAKE_BUF, 0)
    end

    def reset(chipIndex : UInt8) : Nil
      @chip.not_nil!.reset
    end

    def read(chipIndex : UInt8, offset : Int) : UInt8|UInt16|UInt32
      @chip.not_nil!.read(offset & 3)
    end

    def write(chipIndex : UInt8, offset : Int, data : Int, port : UInt8 = 0) : Nil
      @chip.not_nil!.write(offset & 3, data.to_u8!)
    end

    def writeDac(port : Int, command : Int, data : Int) : Nil
      write(0, port << 1, command)
      write(0, (port << 1) | 0x01, data)
    end

    @[AlwaysInline]
    def statusPortARead(chipIndex : UInt8, offset : Int) : UInt8
      read(chipIndex, 0).to_u8!
    end

    @[AlwaysInline]
    def statusPortBRead(chipIndex : UInt8, offset : Int) : UInt8
      read(chipIndex, 2).to_u8!
    end

    @[AlwaysInline]
    def statusPortARead(chipIndex : UInt8, offset : Int) : UInt8
      read(chipIndex, 1).to_u8!
    end

    @[AlwaysInline]
    def statusPortBRead(chipIndex : UInt8, offset : Int) : UInt8
      read(chipIndex, 3).to_u8!
    end

    @[AlwaysInline]
    def controlPortAWrite(chipIndex : UInt8, offset : Int, data : Int) : Nil
      write(chipIndex, 0, data)
    end

    @[AlwaysInline]
    def controlPortBWrite(chipIndex : UInt8, offset : Int, data : Int) : Nil
      write(chipIndex, 2, data)
    end

    @[AlwaysInline]
    def dataPortAWrite(chipIndex : UInt8, offset : Int, data : Int) : Nil
      write(chipIndex, 1, data)
    end

    @[AlwaysInline]
    def dataPortBWrite(chipIndex : UInt8, offset : Int, data : Int) : Nil
      write(chipIndex, 3, data)
    end

    def setMuteMask(chipIndex : UInt8, mask : UInt32) : Nil
      raise "Unimplemented, use Yuno::Chips::YM2612#setMuteMask(UInt8, UInt32, UInt32)"
    end

    @[AlwaysInline]
    def setMuteMask(chipIndex : UInt8, mask : UInt32, maskAY : UInt32) : Nil
      @chip.not_nil!.muteMask = mask
      if paired = @paired
        paired.muteMask = maskAY
      end
    end

    def getVolModifier : UInt32
      @volume.to_u32
    end

    def baseVolume : UInt16
      0x100_u16
    end

    # The internal interface for the chip.
    @[AlwaysInline]
    def chip : AbstractEmulator
      @chip.not_nil!
    end

    # Used as a workaround for some VGMs (e.g. YM2612 files trimmed with
    # VGMTool).  When this is set to `true`, then the internal emulator behaves
    # slightly differently.  This is meant to be used only during
    # initialization, such as in `Yuno::VgmPlayer#play`.
    def isVgmInit=(value : Bool) : Nil
      @chip.not_nil!.isVgmInit = value
    end
  end
end
