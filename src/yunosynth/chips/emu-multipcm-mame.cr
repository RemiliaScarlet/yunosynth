#### Sega System 32 Multi/Model 1/Model 2 custom PCM chip (315-5560) emulation.
####
#### by Miguel Angel Horna (ElSemi) for Model 2 Emulator and MAME.
#### Information by R.Belmont and the YMF278B (OPL4) manual.
#### Crystal port Copyright (C) 2023-2024 Remilia Scarlet
#### BSD 3-Clause License
####
#### voice registers:
#### 0: Pan
#### 1: Index of sample
#### 2: LSB of pitch (low 2 bits seem unused so)
#### 3: MSB of pitch (ooooppppppppppxx) (o=octave (4 bit signed), p=pitch (10 bits), x=unused?
#### 4: voice control: top bit = 1 for key on, 0 for key off
#### 5: bit 0: 0: interpolate volume changes, 1: direct set volume,
####    bits 1-7 = volume attenuate (0=max, 7f=min)
#### 6: LFO frequency + Phase LFO depth
#### 7: Amplitude LFO size
####
#### The first sample ROM contains a variable length table with 12
#### bytes per instrument/sample. This is very similar to the YMF278B.
####
#### The first 3 bytes are the offset into the file (big endian).
#### The next 2 are the loop start offset into the file (big endian)
#### The next 2 are the 2's complement of the total sample size (big endian)
#### The next byte is LFO freq + depth (copied to reg 6 ?)
#### The next 3 are envelope params (Attack, Decay1 and 2, sustain level, release, Key Rate Scaling)
#### The next byte is Amplitude LFO size (copied to reg 7 ?)
####
#### TODO
#### - The YM278B manual states that the chip supports 512 instruments. The MultiPCM probably supports them
#### too but the high bit position is unknown (probably reg 2 low bit). Any game use more than 256?

module Yuno::Chips
  class MultiPCM < Yuno::AbstractChip
    private class MultiPCMMame < Yuno::AbstractEmulator
      CLOCK_DIV = 180.0_f32

      VAL_2_CHAN = [
        0,  1,  2,  3,  4,  5,  6 , -1,
        7,  8,  9,  10, 11, 12, 13, -1,
        14, 15, 16, 17, 18, 19, 20, -1,
        21, 22, 23, 24, 25, 26, 27, -1
      ]

      SHIFT = 12
      RATE = 44100.0

      BASE_TIMES = [
        0.0, 0.0, 0.0, 0.0, 6222.95, 4978.37, 4148.66, 3556.01, 3111.47, 2489.21,
        2074.33, 1778.00, 1555.74, 1244.63, 1037.19, 889.02, 777.87, 622.31,
        518.59, 444.54, 388.93, 311.16, 259.32, 222.27, 194.47, 155.60, 129.66,
        111.16, 97.23, 77.82, 64.85, 55.60, 48.62, 38.91, 32.43, 27.80, 24.31,
        19.46, 16.24, 13.92, 12.15, 9.75, 8.12, 6.98, 6.08, 4.90, 4.08, 3.49,
        3.04, 2.49, 2.13, 1.90, 1.72, 1.41, 1.18, 1.04, 0.91, 0.73, 0.59, 0.50,
        0.45, 0.45, 0.45, 0.45
      ]

      AR_2_DR = 14.32833

      EG_SHIFT = 16
      LFO_SHIFT = 8

      PLFO_TRI = Array(Int32).new(256, 0)
      ALFO_TRI = Array(Int32).new(256, 0)

      LFO_FREQ = [
        # In hertz
        0.168_f32, 2.019_f32, 3.196_f32,4.206_f32, 5.215_f32, 5.888_f32,
        6.224_f32, 7.066_f32
      ]

      enum State
        Attack
        Decay1
        Decay2
        Release
      end

      class Sample
        property start : UInt32 = 0
        property loopPoint : UInt32 = 0
        property stop : UInt32 = 0
        property ar  : UInt8 = 0
        property dr1 : UInt8 = 0
        property dr2 : UInt8 = 0
        property dl  : UInt8 = 0
        property rr  : UInt8 = 0
        property krs : UInt8 = 0
        property lfoVib : UInt8 = 0
        property am : UInt8 = 0

        def initialize
        end
      end

      class Eg
        property volume : Int32 = 0
        property state  : State = State::Attack
        property step   : Int32 = 0
        property ar     : Int32 = 0 # Attack rate
        property d1r    : Int32 = 0 # Decay 1 rate
        property d2r    : Int32 = 0 # Decay 2 rate
        property rr     : Int32 = 0 # Release rate
        property dl     : Int32 = 0 # Decay level

        def initialize
        end
      end

      class Lfo
        PSCALE = [
          # In cents
          0.0_f32, 3.378_f32, 5.065_f32, 6.750_f32, 10.114_f32, 20.170_f32,
          40.180_f32, 79.307_f32
        ]

        ASCALE = [
          # In dB
          0.0_f32, 0.4_f32, 0.8_f32, 1.5_f32, 3.0_f32, 6.0_f32, 12.0_f32, 24.0_f32
        ]

        @@pscales = Array(Array(Int32)).new(8) { |_| Array(Int32).new(256, 0) }
        @@ascales = Array(Array(Int32)).new(8) { |_| Array(Int32).new(256, 0) }

        property phase : UInt16 = 0
        property phaseStep : UInt32 = 0
        property table : Array(Int32) = [] of Int32
        property scale : Array(Int32) = [] of Int32

        def initialize
        end

        def self.initLFOs : Nil
          a : Int32 = 0 # Amplitude
          f : Int32 = 0 # Phase

          256.times do |i|
            # Tri
            a = if i < 128
                  255 - (i * 2)
                else
                  (i * 2) - 256
                end

            f = case
                when i < 64 then i * 2
                when i < 128 then 255 - i * 2
                when i < 192 then 256 - i * 2
                else i * 2 - 511
                end

            ALFO_TRI[i] = a
            PLFO_TRI[i] = f
          end

          8.times do |s|
            limit : Float32 = PSCALE[s]
            (-128...128).each do |i|
              @@pscales[s][i + 128] = toCents((limit * i.to_f32!) / 128.0_f32).to_i32!
            end

            limit = -ASCALE[s]
            256.times do |i|
              @@ascales[s][i] = toDb((limit * i.to_f32!) / 256.0_f32).to_i32!
            end
          end
        end

        def self.lfix(v)
          ((1 << LFO_SHIFT).to_f32! * v.to_f32!).to_u32!
        end

        def self.toDb(v)
          lfix(10.0_f32 ** (v / 20.0_f32))
        end

        def self.toCents(v)
          lfix(2.0_f32 ** (v / 1200.0_f32))
        end

        @[AlwaysInline]
        def plfoStep : Int32
          @phase = @phase &+ @phaseStep
          f : Int32 = @table[(@phase >> LFO_SHIFT) & 0xFF]
          f = @scale[f + 128]
          f << (SHIFT - LFO_SHIFT)
        end

        @[AlwaysInline]
        def alfoStep : Int32
          @phase = @phase &+ @phaseStep
          f : Int32 = @table[(@phase >> LFO_SHIFT) & 0xFF]
          @scale[f] << (SHIFT - LFO_SHIFT)
        end

        @[AlwaysInline]
        def lfoComputeStep(lfof : UInt32, lfos : UInt32, alfo : Int32, rate : Float32) : Nil
          step : Float32 = LFO_FREQ[lfof] * 256.0_f32 / rate
          @phaseStep = ((1 << LFO_SHIFT).to_f32! * step).to_u32!

          if alfo != 0
            @table = ALFO_TRI
            @scale = @@ascales[lfos]
          else
            @table = PLFO_TRI
            @scale = @@pscales[lfos]
          end
        end
      end

      class Slot
        @@lin2ExpVol : Array(Int32) = Array(Int32).new(0x400, 0)

        property num : UInt8 = 0
        property regs : Bytes = Bytes.new(8)
        property? playing : Bool = false
        property sample : Sample = Sample.new
        property base : UInt32 = 0
        property offset : UInt32 = 0
        property step : UInt32 = 0
        property pan : UInt32 = 0
        property tl : UInt32 = 0
        property dstTl : UInt32 = 0
        property tlStep : Int32 = 0
        property prev : Int16 = 0
        property eg : Eg = Eg.new
        property plfo : Lfo = Lfo.new # Phase LFO
        property alfo : Lfo = Lfo.new # AM lfo
        property muted : UInt8 = 0

        def initialize
        end

        def self.initRamps : Nil
          0x400.times do |i|
            db : Float32 = -(96.0_f32 - (96.0_f32 * i.to_f32! / 0x400.to_f32!))
            @@lin2ExpVol[i] = ((10.0f32 ** (db / 20.0_f32)) * (1 << SHIFT).to_f32!).to_i32!
          end
        end

        @[AlwaysInline]
        private def getRate(steps : Array(UInt32), rate : Int32, val : Int32) : Int32
          r : UInt32 = (4 * val + rate).to_u32!
          return steps[0].to_i32! if val == 0
          return steps[0x3F].to_i32! if val == 0xF
          return steps[0x3F].to_i32! if r > 0x3F
          steps[r].to_i32!
        end

        @[AlwaysInline]
        def egCalc(arStep, drStep) : Nil
          octave : Int32 = (((@regs.unsafe_fetch(3).to_i32! >> 4) - 1) & 0xF).to_i32!
          octave -= 16 if Yuno.bitflag?(octave, 8)

          rate : Int32 = if @sample.krs != 0xF
                           (octave + @sample.krs) * 2 + ((@regs.unsafe_fetch(3) >> 3) & 1)
                         else
                           0
                         end

          @eg.ar  = getRate(arStep, rate, @sample.ar.to_i32!)
          @eg.d1r = getRate(drStep, rate, @sample.dr1.to_i32!)
          @eg.d2r = getRate(drStep, rate, @sample.dr2.to_i32!)
          @eg.rr  = getRate(drStep, rate, @sample.rr.to_i32!)
          @eg.dl  = 0xF - @sample.dl
        end

        def egUpdate : Int32
          case @eg.state
          when .attack?
            @eg.volume += @eg.ar
            if @eg.volume >= (0x3FF << EG_SHIFT)
              @eg.state = State::Decay1

              if @eg.d1r >= (0x400 << EG_SHIFT)
                # Skip Decay1, go directly to Decay2
                @eg.state = State::Decay2
              end

              @eg.volume = 0x3FF << EG_SHIFT
            end

          when .decay1?
            @eg.volume -= @eg.d1r
            @eg.volume = 0 if @eg.volume < 0

            if (@eg.volume >> EG_SHIFT) <= (@eg.dl << (10 - 4))
              @eg.state = State::Decay2
            end

          when .decay2?
            @eg.volume -= @eg.d2r
            @eg.volume = 0 if @eg.volume < 0

          when .release?
            @eg.volume -= @eg.rr
            if @eg.volume <= 0
              @eg.volume = 0
              @playing = false
            end

          else return 1 << SHIFT
          end

          @@lin2ExpVol[@eg.volume >> EG_SHIFT]
        end
      end

      @samples : Array(Sample)
      @slots : Array(Slot)
      @curSlot : UInt32 = 0
      @address : UInt32 = 0
      @bank : UInt32 = 0
      @bankR : UInt32 = 0
      @bankL : UInt32 = 0
      @rate : Float32 = 0.0
      @romMask : UInt32 = 0
      @romSize : UInt32 = 0
      @rom : Array(Int8) = Array(Int8).new(0, 0)
      @arStep : Array(UInt32) = Array(UInt32).new(0x40, 0)
      @drStep : Array(UInt32) = Array(UInt32).new(0x40, 0)
      @fnsTable : Array(UInt32) = Array(UInt32).new(0x400, 0)

      @lpanTable : Array(Int32) = Array(Int32).new(0x800, 0)
      @rpanTable : Array(Int32) = Array(Int32).new(0x800, 0)
      @tlSteps : Slice(Int32) = Slice(Int32).new(2, 0)
      @didWarn : UInt8 = 0

      def initialize(clock : UInt32)
        @samples = Array(Sample).new(0x200) { |_| Sample.new } # 512 samples max
        @slots = Array(Slot).new(28) { |_| Slot.new }
        @rate = clock.to_f32! / CLOCK_DIV

        segaDB : Float32 = 0.0
        tl : Float32 = 0.0
        lpan : Float32 = 0.0
        rpan : Float32 = 0.0
        itl : UInt8 = 0
        ipan : UInt8 = 0

        0x800.times do |i|
          lpan = 0
          rpan = 0

          itl = (i & 0x7F).to_u8!
          ipan = ((i >> 7) & 0xF).to_u8!

          segaDB = itl.to_f32! * -24.0_f32 / 0x40.to_f32!
          tl = 10.0f32 ** (segaDB / 20.0f32)

          case
          when ipan == 0x8
            lpan = 0.0f32
            rpan = 0.0f32

          when ipan == 0
            lpan = 1.0f32
            rpan = 1.0f32

          when Yuno.bitflag?(ipan, 0x8)
            lpan = 1.0f32
            ipan = 0x10_u8 - ipan
            segaDB = ipan.to_f32! * -12.0_f32 / 4.0_f32
            rpan = 10.0f32 ** (segaDB / 20.0f32)
            rpan = 0.0f32 if (ipan & 0x7) == 7

          else
            rpan = 1.0f32
            segaDB = ipan.to_f32! * -12.0_f32 / 4.0_f32
            lpan = 10.0f32 ** (segaDB / 20.0f32)
            lpan = 0.0f32 if (ipan & 0x7) == 7
          end

          tl /= 4.0_f32

          @lpanTable[i] = fix(lpan * tl).to_i32!
          @rpanTable[i] = fix(rpan * tl).to_i32!
        end

        # Pitch steps
        0x400.times do |i|
          fcent : Float32 = @rate * (1024.0_f32 + i.to_f32!) / 1024.0_f32
          @fnsTable[i] = ((1 << SHIFT).to_f32! * fcent).to_u32!
        end

        # Envelope steps
        0x40.times do |i|
          # Times are based on a clock of 44100 Hz, then adjusted to the real chip clock.
          @arStep[i] = ((0x400 << EG_SHIFT).to_f32! / (BASE_TIMES[i] * 44100.0_f32 / 1000.0_f32)).to_u32!
          @drStep[i] = ((0x400 << EG_SHIFT).to_f32! / (BASE_TIMES[i] * AR_2_DR * 44100.0_f32 / 1000.0_f32)).to_u32!
        end

        @arStep[0] = 0
        @arStep[1] = 0
        @arStep[2] = 0
        @arStep[3] = 0
        @arStep[0x3F] = 0x400_u32 << EG_SHIFT
        @drStep[0] = 0
        @drStep[1] = 0
        @drStep[2] = 0
        @drStep[3] = 0

        # TL interpolation steps.  Lower first, then upper.
        @tlSteps[0] = ((-(0x80 << SHIFT)).to_f32! / (78.2_f32 * 44100.0_f32 / 1000.0_f32)).to_i32!
        @tlSteps[1] = ((0x80 << SHIFT).to_f32!  / (78.2_f32 * 44100.0_f32 / 1000.0_f32)).to_i32!

        # Build the linear->exponential ramps
        Slot.initRamps

        Lfo.initLFOs
        setBank(0x00, 0x00)
      end

      def start : UInt32
        (@rate + 0.5).to_u32!
      end

      def update(outputs : OutputBuffers, samples : UInt32) : Nil
        outL = outputs[0]
        outR = outputs[1]

        # Check once, then we can avoid bounds checks below.
        if outL.size <= samples || outR.size <= samples
          raise "MultiPCM update: Bad number of samples (expected #{samples}, " \
                "but buffers have #{outL.size} and #{outR.size})"
        end

        outL.fill(0, 0, samples)
        outR.fill(0, 0, samples)

        smpl : Int32 = 0
        smpr : Int32 = 0
        vol : UInt32 = 0
        addr : UInt32 = 0
        sampleData : Int32 = 0
        step : UInt32 = 0
        csample : Int16 = 0
        fpart : UInt64 = 0

        samples.times do |i|
          smpl = 0
          smpr = 0

          @slots.each do |slot|
            # Only look at active slots
            if slot.playing? && slot.muted == 0
              vol = (slot.tl >> SHIFT) | (slot.pan << 7)
              addr = slot.offset >> SHIFT
              step = slot.step
              csample = (@rom[(slot.base + addr) & @romMask].to_i16! << 8).to_i16!
              fpart = (slot.offset & ((1 << SHIFT) - 1)).to_u64!
              sampleData = (((csample.to_i64! * fpart) &+
                             (slot.prev.to_i64! * ((1i64 << SHIFT) - fpart))) >> SHIFT).to_i32!

              if Yuno.bitflag?(slot.regs.unsafe_fetch(6), 7) # Vibrato enabled
                step = step * slot.plfo.plfoStep
                step >>= SHIFT
              end

              slot.offset += step
              if slot.offset >= (slot.sample.stop << SHIFT)
                slot.offset = slot.sample.loopPoint << SHIFT
              end

              if (addr ^ (slot.offset >> SHIFT)) != 0
                slot.prev = csample
              end

              if (slot.tl >> SHIFT) != slot.dstTl
                slot.tl += slot.tlStep
              end

              if Yuno.bitflag?(slot.regs.unsafe_fetch(7), 7) # Tremolo enabled
                sampleData = sampleData * slot.alfo.alfoStep
                sampleData >>= SHIFT
              end

              sampleData = (sampleData * slot.egUpdate) >> 10

              smpl += (@lpanTable[vol] * sampleData) >> SHIFT
              smpr += (@rpanTable[vol] * sampleData) >> SHIFT
            end
          end

          outL.unsafe_put(i, smpl)
          outR.unsafe_put(i, smpr)
        end
      end

      def reset : Nil
        28_u8.times do |i|
          @slots[i].num = i
          @slots[i].playing = false
        end
      end

      @[AlwaysInline]
      def write(offset : Int, data : UInt8) : Nil
        case offset
        when 0 then writeSlot(@slots[@curSlot], @address.to_i32!, data) # Data write
        when 1 then @curSlot = VAL_2_CHAN[data & 0x1F].to_u32! # Set slot number
        when 2 then @address = (data > 7 ? 7 : data).to_u32! # Set address
        end
      end

      @[AlwaysInline]
      def setBank(leftOffs : UInt32, rightOffs : UInt32) : Nil
        @bankL = leftOffs
        @bankR = rightOffs
        @bank = @bankR != 0 ? @bankR : @bankL
      end

      @[AlwaysInline]
      def bankWrite(offset : UInt8, data : UInt16) : Nil
        @bankL = data.to_u32! << 16
        @bankR = data.to_u32! << 16
        @bank = @bankR != 0 ? @bankR : @bankL

        {% if flag?(:yunosynth_debug) %}
          if @bankL != 0 && @bankR != 0 && @bankL != @bankR
            Yuno.warn("YMW Banks: #{@bankL} != #{@bankR}")
          else
            Yuno.dlog!("YMW Banks: #{@bankL} / #{@bankR}")
          end
        {% end %}
      end

      def writeRom(romSize : Int32, dataStart : Int32, dataLength : Int32, romData : Slice(UInt8)) : Nil
        if @romSize != romSize
          @rom = Array(Int8).new(romSize, 0xFF.to_i8!)
          @romSize = romSize.to_u32!

          @romMask = 1
          while @romMask < romSize
            @romMask <<= 1
          end
          @romMask -= 1
        end

        return if dataStart > romSize
        dataLength = romSize - dataStart if (dataStart + dataLength) > romSize

        dataLength.times do |i|
          @rom[dataStart + i] = romData[i].to_i8!
        end

        if dataStart < 0x200 * 12
          512.times do |curSample|
            smp = @samples[curSample]
            ptSmp = @rom.unsafe_as(Array(UInt8)).to_unsafe + curSample * 12

            smp.start = ptSmp[0].to_u32! << 16 | (ptSmp[1].to_u32! << 8) | ptSmp[2].to_u32!
            smp.loopPoint = (ptSmp[3].to_u32! << 8) | ptSmp[4].to_u32!
            smp.stop = 0xFFFF_u32 - ((ptSmp[5].to_u32! << 8) | ptSmp[6].to_u32!)
            smp.lfoVib = ptSmp[7]
            smp.dr1 = ptSmp[8] & 0xF
            smp.ar = (ptSmp[8] >> 4) & 0xF
            smp.dr2 = ptSmp[9] & 0xF
            smp.dl  = (ptSmp[9] >> 4) & 0xF
            smp.rr = ptSmp[10] & 0xF
            smp.krs = (ptSmp[10] >> 4) & 0xF
            smp.am = ptSmp[11]
          end
        end
      end

      def muteMask=(mask : UInt32) : Nil
        @slots.each_with_index do |slot, idx|
          slot.muted = (mask >> idx) & 0x01
        end
      end

      @[AlwaysInline]
      private def fix(v)
        ((1 << SHIFT).to_f32! * v.to_f32!).to_u32!
      end

      private def writeSlot(slot : Slot, reg : Int32, data : UInt8) : Nil
        slot.regs[reg] = data

        case reg
        when 0 # Pan pot
          slot.pan = ((data >> 4) & 0xF).to_u32!

        when 1 # # Sample
          # According to YMF278 sample write causes some base params written to
          # the regs (envelope + lfos) the game should never change the sample
          # while playing.
          smp = @samples[slot.regs.unsafe_fetch(1).to_u16! + ((slot.regs.unsafe_fetch(2) & 1).to_u16! << 8)]
          writeSlot(slot, 6, smp.lfoVib)
          writeSlot(slot, 7, smp.am)

        when 2, 3 # Pitch
          oct : UInt32 = (((slot.regs.unsafe_fetch(3).to_i32! >> 4) - 1) & 0xF).to_u32!
          pitch : UInt32 = ((slot.regs.unsafe_fetch(3).to_u32! & 0xF) << 6) | (slot.regs.unsafe_fetch(2) >> 2)
          pitch = @fnsTable[pitch]

          if Yuno.bitflag?(oct, 0x8)
            pitch >>= (16 - oct)
          else
            pitch <<= oct
          end

          slot.step = (pitch / @rate).to_u32!

        when 4 # Key on/off (and more?)
          if Yuno.bitflag?(data, 0x80)
            # Key On
            sampleID : UInt16 = slot.regs.unsafe_fetch(1).to_u16! + ((slot.regs.unsafe_fetch(2) & 1).to_u16! << 8)
            if @bank != 0 && sampleID > 0x100
              {% if flag?(:yunosynth_debug) %}
                unless Yuno.bitflag?(@didWarn, 0x02)
                  @didWarn |= 0x02
                  Yuno.warn("YMW Warning: SEGA Banking + playing sample with ID 100$ or over")
                end
              {% end %}

              sampleID &= 0x0FF
            end

            slot.sample = @samples[sampleID]
            slot.playing = true
            slot.base = slot.sample.start
            slot.offset = 0
            slot.prev = 0
            slot.tl = slot.dstTl << SHIFT
            slot.egCalc(@arStep, @drStep)
            slot.eg.state = State::Attack
            slot.eg.volume = 0

            if @bank != 0 && slot.base >= 0x100000
              {% if flag?(:yunosynth_debug) %}
                otherBnk : Bool = if Yuno.bitflag?(slot.pan, 8)
                                    @bank != @bankL
                                  else
                                    @bank != @bankR
                                  end
                if otherBnk
                  unless Yuno.bitflag?(@didWarn, 0x01)
                    @didWarn |= 0x01
                    Yuno.warn("YMW Warning: Playing sound on possibly unintended bank")
                  end
                end
              {% end %}

              slot.base = (slot.base & 0xFFFFF) | @bank
            end
          else
            if slot.playing?
              if slot.sample.rr != 0xF
                slot.eg.state = State::Release
              else
                slot.playing = false
              end
            end
          end

        when 5 # TL + Interpolation
          slot.dstTl = ((data >> 1) & 0x7F).to_u32!
          if (data & 1) == 0 # Interpolate TL
            if (slot.tl >> SHIFT) > slot.dstTl
              slot.tlStep = @tlSteps.unsafe_fetch(0) # Decrease
            else
              slot.tlStep = @tlSteps.unsafe_fetch(1) # Increase
            end
          else
            slot.tl = slot.dstTl << SHIFT
          end

        when 6 # LFO frequency + PLFO
          if data != 0
            slot.plfo.lfoComputeStep((slot.regs.unsafe_fetch(6) >> 3) & 7,
                                     slot.regs.unsafe_fetch(6) & 7,
                                     0,
                                     @rate)
            slot.alfo.lfoComputeStep((slot.regs.unsafe_fetch(6) >> 3) & 7,
                                     slot.regs.unsafe_fetch(7) & 7,
                                     1,
                                     @rate)
          end

        when 7 # ALFO
          if data != 0
            slot.plfo.lfoComputeStep((slot.regs.unsafe_fetch(6).to_u32! >> 3) & 7,
                                     slot.regs.unsafe_fetch(6).to_u32! & 7,
                                     0,
                                     @rate)
            slot.alfo.lfoComputeStep((slot.regs.unsafe_fetch(6).to_u32! >> 3) & 7,
                                     slot.regs.unsafe_fetch(7).to_u32! & 7,
                                     1,
                                     @rate)
          end
        end
      end
    end
  end
end
