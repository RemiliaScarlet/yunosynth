#### Capcom DL-1425 QSound emulator
#### ==============================
####
#### by superctr (Ian Karlsson)
#### with thanks to Valley Bell
####
#### 2018-05-12 - 2018-05-15
####
#### Crystal port Copyright (C) 2023-2024 Remilia Scarlet
#### BSD 3-Clause License
####

module Yuno::Chips
  class QSound < Yuno::AbstractChip
    private class QSoundCtr < Yuno::AbstractEmulator
      ###
      ### Constants
      ###

      DRY_MIX_TABLE = [
        -16384_i16, -16384_i16, -16384_i16, -16384_i16, -16384_i16, -16384_i16, -16384_i16, -16384_i16,
        -16384_i16, -16384_i16, -16384_i16, -16384_i16, -16384_i16, -16384_i16, -16384_i16, -16384_i16,
        -16384_i16, -14746_i16, -13107_i16, -11633_i16, -10486_i16, -9175_i16, -8520_i16, -7209_i16,
        -6226_i16, -5226_i16, -4588_i16, -3768_i16, -3277_i16, -2703_i16, -2130_i16, -1802_i16,
        0_i16
      ]

      WET_MIX_TABLE = [
        0_i16, -1638_i16, -1966_i16, -2458_i16, -2949_i16, -3441_i16, -4096_i16, -4669_i16,
        -4915_i16, -5120_i16, -5489_i16, -6144_i16, -7537_i16, -8831_i16, -9339_i16, -9830_i16,
        -10240_i16, -10322_i16, -10486_i16, -10568_i16, -10650_i16, -11796_i16, -12288_i16, -12288_i16,
        -12534_i16, -12648_i16, -12780_i16, -12829_i16, -12943_i16, -13107_i16, -13418_i16, -14090_i16,
        -16384_i16
      ]

      LINEAR_MIX_TABLE = [
        -16379_i16, -16338_i16, -16257_i16, -16135_i16, -15973_i16, -15772_i16, -15531_i16, -15251_i16,
        -14934_i16, -14580_i16, -14189_i16, -13763_i16, -13303_i16, -12810_i16, -12284_i16, -11729_i16,
        -11729_i16, -11144_i16, -10531_i16, -9893_i16, -9229_i16, -8543_i16, -7836_i16, -7109_i16,
        -6364_i16, -5604_i16, -4829_i16, -4043_i16, -3246_i16, -2442_i16, -1631_i16, -817_i16,
        0_i16
      ]

      FILTER_DATA = [
        [# d53 - 0
          0i16, 0i16, 0i16, 6i16, 44i16, -24i16, -53i16, -10i16, 59i16, -40i16,
          -27i16, 1i16, 39i16, -27i16, 56i16, 127i16, 174i16, 36i16, -13i16,
          49i16, 212i16, 142i16, 143i16, -73i16, -20i16, 66i16, -108i16,
          -117i16, -399i16, -265i16, -392i16, -569i16, -473i16, -71i16, 95i16,
          -319i16, -218i16, -230i16, 331i16, 638i16, 449i16, 477i16, -180i16,
          532i16, 1107i16, 750i16, 9899i16, 3828i16, -2418i16, 1071i16, -176i16,
          191i16, -431i16, 64i16, 117i16, -150i16, -274i16, -97i16, -238i16,
          165i16, 166i16, 250i16, -19i16, 4i16, 37i16, 204i16, 186i16, -6i16,
          140i16, -77i16, -1i16, 1i16, 18i16, -10i16, -151i16, -149i16, -103i16,
          -9i16, 55i16, 23i16, -102i16, -97i16, -11i16, 13i16, -48i16, -27i16,
          5i16, 18i16, -61i16, -30i16, 64i16, 72i16, 0i16, 0i16, 0i16,
        ],

        [# db2 - 1 - default left filter
          0i16, 0i16, 0i16, 85i16, 24i16, -76i16, -123i16, -86i16, -29i16,
          -14i16, -20i16, -7i16, 6i16, -28i16, -87i16, -89i16, -5i16, 100i16,
          154i16, 160i16, 150i16, 118i16, 41i16, -48i16, -78i16, -23i16, 59i16,
          83i16, -2i16, -176i16, -333i16, -344i16, -203i16, -66i16, -39i16,
          2i16, 224i16, 495i16, 495i16, 280i16, 432i16, 1340i16, 2483i16,
          5377i16, 1905i16, 658i16, 0i16, 97i16, 347i16, 285i16, 35i16, -95i16,
          -78i16, -82i16, -151i16, -192i16, -171i16, -149i16, -147i16, -113i16,
          -22i16, 71i16, 118i16, 129i16, 127i16, 110i16, 71i16, 31i16, 20i16,
          36i16, 46i16, 23i16, -27i16, -63i16, -53i16, -21i16, -19i16, -60i16,
          -92i16, -69i16, -12i16, 25i16, 29i16, 30i16, 40i16, 41i16, 29i16,
          30i16, 46i16, 39i16, -15i16, -74i16, 0i16, 0i16, 0i16
        ],

        [ # e11 - 2 - default right filter
          0i16, 0i16, 0i16, 23i16, 42i16, 47i16, 29i16, 10i16, 2i16, -14i16,
          -54i16, -92i16, -93i16, -70i16, -64i16, -77i16, -57i16, 18i16, 94i16,
          113i16, 87i16, 69i16, 67i16, 50i16, 25i16, 29i16, 58i16, 62i16, 24i16,
          -39i16, -131i16, -256i16, -325i16, -234i16, -45i16, 58i16, 78i16,
          223i16, 485i16, 496i16, 127i16, 6i16, 857i16, 2283i16, 2683i16,
          4928i16, 1328i16, 132i16, 79i16, 314i16, 189i16, -80i16, -90i16,
          35i16, -21i16, -186i16, -195i16, -99i16, -136i16, -258i16, -189i16,
          82i16, 257i16, 185i16, 53i16, 41i16, 84i16, 68i16, 38i16, 63i16,
          77i16, 14i16, -60i16, -71i16, -71i16, -120i16, -151i16, -84i16, 14i16,
          29i16, -8i16, 7i16, 66i16, 69i16, 12i16, -3i16, 54i16, 92i16, 52i16,
          -6i16, -15i16, -2i16, 0i16, 0i16, 0i16
        ],

        [ # e70 - 3
          0i16, 0i16, 0i16, 2i16, -28i16, -37i16, -17i16, 0i16, -9i16, -22i16,
          -3i16, 35i16, 52i16, 39i16, 20i16, 7i16, -6i16, 2i16, 55i16, 121i16,
          129i16, 67i16, 8i16, 1i16, 9i16, -6i16, -16i16, 16i16, 66i16, 96i16,
          118i16, 130i16, 75i16, -47i16, -92i16, 43i16, 223i16, 239i16, 151i16,
          219i16, 440i16, 475i16, 226i16, 206i16, 940i16, 2100i16, 2663i16,
          4980i16, 865i16, 49i16, -33i16, 186i16, 231i16, 103i16, 42i16, 114i16,
          191i16, 184i16, 116i16, 29i16, -47i16, -72i16, -21i16, 60i16, 96i16,
          68i16, 31i16, 32i16, 63i16, 87i16, 76i16, 39i16, 7i16, 14i16, 55i16,
          85i16, 67i16, 18i16, -12i16, -3i16, 21i16, 34i16, 29i16, 6i16, -27i16,
          -49i16, -37i16, -2i16, 16i16, 0i16, -21i16, -16i16, 0i16, 0i16, 0i16,
        ],

        [ # ecf - 4
          0i16, 0i16, 0i16, 48i16, 7i16, -22i16, -29i16, -10i16, 24i16, 54i16,
          59i16, 29i16, -36i16, -117i16, -185i16, -213i16, -185i16, -99i16,
          13i16, 90i16, 83i16, 24i16, -5i16, 23i16, 53i16, 47i16, 38i16, 56i16,
          67i16, 57i16, 75i16, 107i16, 16i16, -242i16, -440i16, -355i16,
          -120i16, -33i16, -47i16, 152i16, 501i16, 472i16, -57i16, -292i16,
          544i16, 1937i16, 2277i16, 6145i16, 1240i16, 153i16, 47i16, 200i16,
          152i16, 36i16, 64i16, 134i16, 74i16, -82i16, -208i16, -266i16,
          -268i16, -188i16, -42i16, 65i16, 74i16, 56i16, 89i16, 133i16, 114i16,
          44i16, -3i16, -1i16, 17i16, 29i16, 29i16, -2i16, -76i16, -156i16,
          -187i16, -151i16, -85i16, -31i16, -5i16, 7i16, 20i16, 32i16, 24i16,
          -5i16, -20i16, 6i16, 48i16, 62i16, 0i16, 0i16, 0i16
        ]
      ]

      FILTER_DATA_2 = [
        # f2e - following 95 values used for "disable output" filter 0i16, 0i16,
        0i16, 0i16, 0i16, 0i16, 0i16, 0i16, 0i16, 0i16, 0i16, 0i16, 0i16, 0i16,
        0i16, 0i16, 0i16, 0i16, 0i16, 0i16, 0i16, 0i16, 0i16, 0i16, 0i16, 0i16,
        0i16, 0i16, 0i16, 0i16, 0i16, 0i16, 0i16, 0i16, 0i16, 0i16, 0i16, 0i16,
        0i16, 0i16, 0i16, 0i16, 0i16, 0i16, 0i16, 0i16, 0i16, 0i16, 0i16, 0i16,
        0i16, 0i16, 0i16, 0i16, 0i16, 0i16, 0i16, 0i16, 0i16, 0i16, 0i16, 0i16,
        0i16, 0i16, 0i16, 0i16, 0i16, 0i16, 0i16,

        # f73 - following 45 values used for "mode 2" filter (overlaps with f2e)
        0i16, 0i16, 0i16, 0i16, 0i16, 0i16, 0i16, 0i16, 0i16, 0i16, 0i16, 0i16,
        0i16, 0i16, 0i16, 0i16, 0i16, 0i16, 0i16, 0i16, 0i16, 0i16, 0i16, 0i16,
        0i16, 0i16, -371i16, -196i16, -268i16, -512i16, -303i16, -315i16,
        -184i16, -76i16, 276i16, -256i16, 298i16, 196i16, 990i16, 236i16,
        1114i16, -126i16, 4377i16, 6549i16, 791i16,

        # fa0 - filtering disabled (for 95-taps) (use fa3 or fa4 for mode2 filters)
        0i16, 0i16, 0i16, 0i16, 0i16, 0i16, 0i16, 0i16, 0i16, 0i16, 0i16, 0i16,
        0i16, 0i16, 0i16, 0i16, 0i16, 0i16, 0i16, 0i16, 0i16, 0i16, 0i16, 0i16,
        0i16, 0i16, 0i16, 0i16, 0i16, 0i16, 0i16, 0i16, 0i16, 0i16, 0i16, 0i16,
        0i16, 0i16, 0i16, 0i16, 0i16, 0i16, 0i16, 0i16, 0i16, 0i16, 0i16,
        -16384i16, 0i16, 0i16, 0i16, 0i16, 0i16, 0i16, 0i16, 0i16, 0i16, 0i16,
        0i16, 0i16, 0i16, 0i16, 0i16, 0i16, 0i16, 0i16, 0i16, 0i16, 0i16, 0i16,
        0i16, 0i16, 0i16, 0i16, 0i16, 0i16, 0i16, 0i16, 0i16, 0i16, 0i16, 0i16,
        0i16, 0i16, 0i16, 0i16, 0i16, 0i16, 0i16, 0i16, 0i16, 0i16, 0i16, 0i16,
        0i16
      ]

      ADPCM_STEP_TABLE = [
        154i16,  154i16,  128i16,  102i16,  77i16,  58i16,  58i16,  58i16,
        58i16,  58i16,  58i16,  58i16,  77i16,  102i16,  128i16,  154
      ]

      STATE_INIT_1 = 0x288_u16
      STATE_INIT_2 = 0x61a_u16
      STATE_REFRESH_1 = 0x039_u16
      STATE_REFRESH_2 = 0x04F_u16
      STATE_NORMAL_1 = 0x314_u16
      STATE_NORMAL_2 = 0x6B2_u16

      PAN_TABLE_LEFT = 0
      PAN_TABLE_RIGHT = 1
      PAN_TABLE_DRY = 0
      PAN_TABLE_WET = 1

      ###
      ### Internal Classes
      ###

      class Voice
        property bank : UInt16 = 0
        property addr : Int16 = 0 # Top word is the sample address
        property phase : UInt16 = 0
        property rate : UInt16 = 0
        property loopLen : Int16 = 0
        property endAddr : Int16 = 0
        property volume : Int16 = 0
        property echo : Int16 = 0

        def initialize
        end

        @[AlwaysInline]
        def reset : Nil
          @bank = 0
          @addr = 0
          @phase = 0
          @rate = 0
          @loopLen = 0
          @endAddr = 0
          @volume = 0
          @echo = 0
        end

        protected def bankAddr
          pointerof(@bank)
        end

        protected def addrAddr
          pointerof(@addr).unsafe_as(Pointer(UInt16))
        end

        protected def rateAddr
          pointerof(@rate)
        end

        protected def phaseAddr
          pointerof(@phase)
        end

        protected def loopLenAddr
          Pointer(UInt16).new(pointerof(@loopLen).address)
        end

        protected def endAddrAddr
          Pointer(UInt16).new(pointerof(@endAddr).address)
        end

        protected def volumeAddr
          Pointer(UInt16).new(pointerof(@volume).address)
        end

        protected def echoAddr
          Pointer(UInt16).new(pointerof(@echo).address)
        end
      end

      class Adpcm
        property startAddr : UInt16 = 0
        property endAddr : UInt16 = 0
        property bank : UInt16 = 0
        property volume : Int16 = 0
        property flag : UInt16 = 0
        property curVolume : Int16 = 0
        property stepSize : Int16 = 0
        property curAddr : UInt16 = 0

        def initialize
        end

        @[AlwaysInline]
        def reset : Nil
          @startAddr = 0
          @endAddr = 0
          @bank = 0
          @volume = 0
          @flag = 0
          @curVolume = 0
          @stepSize = 0
          @curAddr = 0
        end

        protected def startAddrAddr
          pointerof(@startAddr)
        end

        protected def endAddrAddr
          pointerof(@endAddr)
        end

        protected def bankAddr
          pointerof(@bank)
        end

        protected def flagAddr
          pointerof(@flag)
        end

        protected def volumeAddr
          Pointer(UInt16).new(pointerof(@volume).address)
        end
      end

      # Q1 Filter
      class Fir
        property tapCount : Int32 = 0 # Usually 95
        property delayPos : Int32 = 0
        property tablePos : Int16 = 0
        property taps : Array(Int16) = Array(Int16).new(95, 0)
        property delayLine : Array(Int16) = Array(Int16).new(95, 0)

        def initialize
        end

        @[AlwaysInline]
        def reset : Nil
          @tapCount = 0
          @delayPos = 0
          @tablePos = 0
          @taps.fill(0)
          @delayLine.fill(0)
        end

        protected def tablePosAddr
          Pointer(UInt16).new(pointerof(@tablePos).address)
        end

        # Apply tyhe FIR filter used as the Q1 transfer function.
        @[AlwaysInline]
        def fir(input : Int16) : Int32
          output : Int32 = 0
          tap : Int32 = 0

          while tap < @tapCount - 1
            output -= (@taps[tap].to_i32! * @delayLine[@delayPos].to_i32!) << 2
            @delayPos += 1
            @delayPos = 0 if @delayPos >= @tapCount - 1
            tap += 1
          end

          output -= (@taps[tap].to_i32! * input.to_i32!) << 2

          @delayLine[@delayPos] = input
          @delayPos += 1
          @delayPos = 0 if @delayPos >= @tapCount - 1

          output
        end
      end

      # Delay line
      class Delay
        property delay : Int16 = 0
        property volume : Int16 = 0
        property writePos : Int16 = 0
        property readPos : Int16 = 0
        property delayLine : Array(Int16) = Array(Int16).new(51, 0)

        def initialize
        end

        @[AlwaysInline]
        def reset : Nil
          @delay = 0
          @volume = 0
          @writePos = 0
          @readPos = 0
          @delayLine.fill(0)
        end

        protected def delayAddr
          Pointer(UInt16).new(pointerof(@delay).address)
        end

        protected def volumeAddr
          Pointer(UInt16).new(pointerof(@volume).address)
        end

        @[AlwaysInline]
        def delay(input : Int32) : Int32
          @delayLine[@writePos] = (input >> 16).to_i16!
          @writePos += 1
          @writePos = 0 if @writePos >= @delayLine.size

          output : Int32 = @delayLine[@readPos].to_i32! * @volume.to_i32!
          @readPos += 1
          @readPos = 0 if @readPos >= @delayLine.size

          output
        end

        @[AlwaysInline]
        def update : Nil
          newReadPos : Int16 = ((@writePos.to_i32! - @delay.to_i32!) % 51).to_i16!
          newReadPos += 51 if newReadPos < 0
          @readPos = newReadPos
        end
      end

      class Echo
        property endPos : UInt16 = 0
        property feedback : Int16 = 0
        property length : Int16 = 0
        property lastSample : Int16 = 0
        property delayLine : Array(Int16) = Array(Int16).new(1024, 0)
        property delayPos : Int16 = 0

        def initialize
        end

        @[AlwaysInline]
        def reset : Nil
          @endPos = 0
          @feedback = 0
          @length = 0
          @lastSample = 0
          @delayLine.fill(0)
          @delayPos = 0
        end

        protected def endPosAddr
          pointerof(@endPos)
        end

        protected def feedbackAddr
          Pointer(UInt16).new(pointerof(@feedback).address)
        end

        # The echo effect is pretty simple. A moving average filter is used on the
        # output from the delay line to smooth samples over time.
        @[AlwaysInline]
        def echo(input : Int32) : Int16
          # Get average of last two samples from the delay line.
          oldSample : Int32 = @delayLine[@delayPos].to_i32!
          lastSample : Int32 = @lastSample.to_i32!

          @lastSample = oldSample.to_i16!
          oldSample = (oldSample + lastSample) >> 1

          # Add current sample to the delay line.
          newSample : Int32 = input + ((oldSample * @feedback.to_i32!) << 2)
          @delayLine[@delayPos] = (newSample >> 16).to_i16!
          @delayPos += 1
          @delayPos = 0 if @delayPos >= @length

          oldSample.to_i16!
        end
      end

      ###
      ### Fields
      ###

      @romData : Bytes = Bytes.new(0)
      @romMask : UInt32 = 0
      @muteMask : UInt32 = 0

      @dataLatch : UInt16 = 0
      @output : Array(Int16) = [0i16, 0i16]

      @panTables : Array(Array(Array(Int16)))

      @voices : Array(Voice)
      @adpcm : Array(Adpcm)

      @voicePan : Array(UInt16) = Array(UInt16).new(16 + 3, 0)
      @voiceOutput : Array(Int16) = Array(Int16).new(16 + 3, 0)

      @echo : Echo = Echo.new
      @filter : Array(Fir)
      @altFilter : Array(Fir)

      @wet : Array(Delay)
      @dry : Array(Delay)

      @state : UInt16 = 0
      @nextState : UInt16 = 0

      @delayUpdate : UInt16 = 0

      @stateCounter : Int32 = 0
      @readyFlag : UInt8 = 0

      @registerMap : Array(Pointer(UInt16))

      getter clockRate : UInt32 = 0

      ###
      ### Methods
      ###

      def initialize(clock : UInt32)
        @panTables = Array(Array(Array(Int16))).new(2) do |_|
          Array(Array(Int16)).new(2) do |_|
            Array(Int16).new(98, 0)
          end
        end

        @voices = Array(Voice).new(16) { |_| Voice.new }
        @adpcm = Array(Adpcm).new(3) { |_| Adpcm.new }

        @filter = Array(Fir).new(2) { |_| Fir.new }
        @altFilter = Array(Fir).new(2) { |_| Fir.new }

        @wet = Array(Delay).new(2) { |_| Delay.new }
        @dry = Array(Delay).new(2) { |_| Delay.new }

        @registerMap = Array(Pointer(UInt16)).new(256) { |_| Pointer(UInt16).null }

        self.muteMask = 0x00

        initPanTables
        initRegisterMap

        @clockRate = clock.tdiv(2).tdiv(1248)
      end

      def reset : Nil
        @readyFlag = 0
        @output[0] = 0
        @output[1] = 0
        @state = 0
        @stateCounter = 0
      end

      def read(offset : Int) : UInt8
        @readyFlag
      end

      @[AlwaysInline]
      def write(offset : Int, data : UInt8) : Nil
        case offset
        when 0 then @dataLatch = (@dataLatch & 0x00FF) | (data.to_u16! << 8)
        when 1 then @dataLatch = (@dataLatch & 0xFF00) | data
        when 2 then writeData(data, @dataLatch)
        end
      end

      @[AlwaysInline]
      def writeData(address : UInt8, data : UInt16) : Nil
        dest = @registerMap[address]
        unless dest.null?
          dest.value = data
        end
        @readyFlag = 0
      end

      @[AlwaysInline]
      def update(outputs : OutputBuffers, samples : UInt32) : Nil
        outL : Slice(Int32) = outputs[0]
        outR : Slice(Int32) = outputs[1]

        # Check once, then we can avoid bounds checks below.
        if outL.size < samples || outR.size < samples
          raise "QSound update: Bad number of samples (expected #{samples}, " \
                "but buffers have #{outL.size} and #{outR.size})"
        end

        #outL.fill(0, 0, samples)
        #outR.fill(0, 0, samples)
        samples.times do |i|
          updateSample
          outL.unsafe_put(i, @output.unsafe_fetch(0).to_i32!)
          outR.unsafe_put(i, @output.unsafe_fetch(1).to_i32!)
        end
      end

      def writeRom(romSize : Int32, dataStart : Int32, dataLength : Int32, newData : Slice(UInt8)) : Nil
        if @romData.size != romSize
          @romData = Bytes.new(romSize, 0xFF_u8)
          @romMask = -1.to_u32!
        end

        return if dataStart > romSize
        dataLength = romSize - dataStart if dataStart + dataLength > romSize

        # Copy the memory
        dataLength.times do |i|
          # This can be guaranteed by the checks above
          @romData.unsafe_put(i + dataStart, newData.unsafe_fetch(i))
        end
      end

      def muteMask=(mask : UInt32) : Nil
        @muteMask = mask
      end

      def waiteBusy : Nil
        while @readyFlag == 0
          updateSample
        end
      end

      ###
      ### Private Methods
      ###

      private def initPanTables : Nil
        33.times do |i|
          # Dry mixing levels
          @panTables[PAN_TABLE_LEFT][PAN_TABLE_DRY][i] = DRY_MIX_TABLE[i]
          @panTables[PAN_TABLE_RIGHT][PAN_TABLE_DRY][i] = DRY_MIX_TABLE[32 - i]

          # Wet mixing levels
          @panTables[PAN_TABLE_LEFT][PAN_TABLE_WET][i] = WET_MIX_TABLE[i]
          @panTables[PAN_TABLE_RIGHT][PAN_TABLE_WET][i] = WET_MIX_TABLE[32 - i]

          # Linear panning, only for dry component.  Wet component is muted.
          @panTables[PAN_TABLE_LEFT][PAN_TABLE_DRY][i + 0x30] = LINEAR_MIX_TABLE[i]
          @panTables[PAN_TABLE_RIGHT][PAN_TABLE_DRY][i + 0x30] = LINEAR_MIX_TABLE[32 - i]
        end
      end

      private def initRegisterMap : Nil
        # Unused registers
        256.times do |i|
          @registerMap[i] = Pointer(UInt16).null
        end

        # PCM registers
        16.times do |i| # PCM voices
          @registerMap[(i << 3) + 0] = @voices[(i + 1) % 16].bankAddr # Bank applies to the next channel.
          @registerMap[(i << 3) + 1] = @voices[i].addrAddr # Current sample position and start position
          @registerMap[(i << 3) + 2] = @voices[i].rateAddr # 4.12 fixed point decimal
          @registerMap[(i << 3) + 3] = @voices[i].phaseAddr
          @registerMap[(i << 3) + 4] = @voices[i].loopLenAddr
          @registerMap[(i << 3) + 5] = @voices[i].endAddrAddr
          @registerMap[(i << 3) + 6] = @voices[i].volumeAddr
          @registerMap[(i << 3) + 7] = Pointer(UInt16).null
          @registerMap[i + 0x80] = @voicePan.to_unsafe + i
          @registerMap[i + 0xBa] = @voices[i].echoAddr
        end

        # ADPCM registers
        3.times do |i| # ADPCM voices
          # ADPCM sample rate is fixed to 8khz. (one channel is updated every third sample)
          @registerMap[(i << 2) + 0xCA] = @adpcm[i].startAddrAddr
          @registerMap[(i << 2) + 0xCB] = @adpcm[i].endAddrAddr
          @registerMap[(i << 2) + 0xCC] = @adpcm[i].bankAddr
          @registerMap[(i << 2) + 0xCD] = @adpcm[i].volumeAddr
          @registerMap[i + 0xD6] = @adpcm[i].flagAddr # Non-zero to start ADPCM playback
          @registerMap[i + 0x90] = @voicePan.to_unsafe + (16 + i)
        end

        # QSound registers
        @registerMap[0x93] = @echo.feedbackAddr
        @registerMap[0xD9] = @echo.endPosAddr
        @registerMap[0xE2] = pointerof(@delayUpdate) # Non-zero to update delays
        @registerMap[0xE3] = pointerof(@nextState)

        2.times do |i| # Left and right
          @registerMap[(i << 1) + 0xDA] = @filter[i].tablePosAddr
          @registerMap[(i << 1) + 0xDE] = @wet[i].delayAddr
          @registerMap[(i << 1) + 0xE4] = @wet[i].volumeAddr

          @registerMap[(i << 1) + 0xDB] = @altFilter[i].tablePosAddr
          @registerMap[(i << 1) + 0xDF] = @dry[i].delayAddr
          @registerMap[(i << 1) + 0xE5] = @dry[i].volumeAddr
        end
      end

      @[AlwaysInline]
      private def getSample(bank : UInt16, address : UInt16) : Int16
        return 0i16 if @romMask == 0 # No ROM loaded
        return 0i16 if (bank & 0x8000) == 0 # Ignore attempts to read from DSP program ROM

        bank &= 0x7FFF
        romAddr : UInt32 = (bank.to_u32! << 16) | address
        sampleData : Int16 = @romData[romAddr].to_i16!

        # MAME currently expands the 8 bit ROM data to 16 bits this way.
        (sampleData << 8) | sampleData
      end

      @[AlwaysInline]
      private def getFilterTable(offset : UInt16) : Pointer(Int16)
        if offset >= 0xF2E && offset < 0xFFF
          FILTER_DATA_2.to_unsafe + (offset - 0xF2E) # Overlapping filter data
        else
          index = (offset - 0xD53).tdiv(95)
          if index >= 0 && index < 5
            FILTER_DATA[index].to_unsafe
          else
            Pointer(Int16).null
          end
        end
      end

      # Updates one DSP sample
      private def updateSample : Nil
        case @state
        when STATE_INIT_1, STATE_INIT_2 then stateInit
        when STATE_REFRESH_1 then stateRefreshFilter1
        when STATE_REFRESH_2 then stateRefreshFilter2
        when STATE_NORMAL_1, STATE_NORMAL_2 then stateNormalUpdate
        else stateInit
        end
      end

      # Initialization routine
      private def stateInit : Nil
        mode : Int32 = (@state == STATE_INIT_2 ? 1 : 0)

        # We're busy for four samples, including the filter refresh.
        if @stateCounter >= 2
          @stateCounter = 0
          @state = @nextState
          return
        elsif @stateCounter == 1
          @stateCounter += 1
          return
        end

        @voices.each &.reset
        @adpcm.each &.reset
        @filter.each &.reset
        @altFilter.each &.reset
        @wet.each &.reset
        @dry.each &.reset
        @echo.reset

        19.times do |i|
          @voicePan[i] = 0x120
          @voiceOutput[i] = 0
        end

        16.times do |i|
          @voices[i].bank = 0x8000
        end

        3.times do |i|
          @adpcm[i].bank = 0x8000
        end

        if mode == 0
          # Mode 1
          @wet[0].delay = 0
          @dry[0].delay = 46
          @wet[1].delay = 0
          @dry[1].delay = 48
          @filter[0].tablePos = 0xDB2
          @filter[1].tablePos = 0xE11
          @echo.endPos = 0x554_u16 + 6u16
          @nextState = STATE_REFRESH_1
        else
          # Mode 2
          @wet[0].delay = 1
          @dry[0].delay = 0
          @wet[1].delay = 0
          @dry[1].delay = 0
          @filter[0].tablePos = 0xF73
          @filter[1].tablePos = 0xFA4
          @altFilter[0].tablePos = 0xF73
          @altFilter[1].tablePos = 0xFA4
          @echo.endPos = 0x53C_u16 + 6u16
          @nextState = STATE_REFRESH_2
        end

        @wet[0].volume = 0x3FFF
        @dry[0].volume = 0x3FFF
        @wet[1].volume = 0x3FFF
        @dry[1].volume = 0x3FFF

        @delayUpdate = 1
        @readyFlag = 0
        @stateCounter = 1
      end

      # Updates filter parameters for mode 1
      private def stateRefreshFilter1 : Nil
        2.times do |ch|
          @filter[ch].delayPos = 0
          @filter[ch].tapCount = 95

          table = getFilterTable(@filter[ch].tablePos.to_u16!)
          unless table.null?
            @filter[ch].taps.to_unsafe.copy_from(table, 95)
          end
        end

        @nextState = STATE_NORMAL_1
        @state = STATE_NORMAL_1
      end

      # Updates filter parameters for mode 2
      private def stateRefreshFilter2 : Nil
        2.times do |ch|
          @filter[ch].delayPos = 0
          @filter[ch].tapCount = 45

          table = getFilterTable(@filter[ch].tablePos.to_u16!)
          unless table.null?
            @filter[ch].taps.to_unsafe.copy_from(table, 45)
          end

          @altFilter[ch].delayPos = 0
          @altFilter[ch].tapCount = 44

          table = getFilterTable(@altFilter[ch].tablePos.to_u16!)
          unless table.null?
            @altFilter[ch].taps.to_unsafe.copy_from(table, 44)
          end
        end

        @nextState = STATE_NORMAL_2
        @state = STATE_NORMAL_2
      end

      # Updates a PCM voice.  There are 16 voices, each are updated every sample
      # with full rate and volume control.
      @[AlwaysInline]
      private def pcmUpdate(voiceNum : Int, echoOut : Pointer(Int32)) : Int16
        return 0i16 if Yuno.bitflag?(@muteMask, (1 << voiceNum))

        voice = @voices[voiceNum]

        # Read sample from ROM and apply volume
        output : Int16 = ((voice.volume.to_i32! * getSample(voice.bank, voice.addr.to_u16!).to_i32!) >> 14).to_i16!
        echoOut.value += (output &* voice.echo) << 2

        # Add delta to the phase and loop back if required
        newPhase : Int32 = voice.rate.to_i32! + ((voice.addr.to_i32! << 12) | (voice.phase.to_i32! >> 4))

        if (newPhase >> 12) >= voice.endAddr
          newPhase -= (voice.loopLen.to_i32! << 12)
        end

        newPhase = newPhase.clamp(-0x8000000, 0x7FFFFFF)
        voice.addr = (newPhase >> 12).to_i16!
        voice.phase = ((newPhase << 4) & 0xFFFF).to_u16!

        output
      end

      # Updates an ADPCM voice.  There are 3 voices, one is updated every sample
      # (effectively making the ADPCM rate 1/3 of the master sample rate), and
      # volume is set when starting samples only.  The ADPCM algorithm is
      # supposedly similar to Yamaha ADPCM. It also seems like Capcom never used
      # it, so this was not emulated in the earlier QSound emulators.
      @[AlwaysInline]
      private def adpcmUpdate(voiceNum : Int, nibble : Int) : Nil
        if Yuno.bitflag?(@muteMask, (1 << (16 + voiceNum)))
          @voiceOutput[16 + voiceNum] = 0
          return
        end

        voice = @adpcm[voiceNum]
        delta : Int32 = 0
        step : Int8 = 0

        if nibble == 0
          # Mute voice when it reaches the end address.
          if voice.curAddr == voice.endAddr
            voice.curVolume = 0
          end

          # Playback start flag
          if voice.flag != 0
            @voiceOutput[16 + voiceNum] = 0
            voice.flag = 0
            voice.stepSize = 10
            voice.curVolume = voice.volume
            voice.curAddr = voice.startAddr
          end

          # Get top nibble
          step = (getSample(voice.bank, voice.curAddr) >> 8).to_i8!
        else
          # Get bottom nibble
          step = (getSample(voice.bank, voice.curAddr) >> 4).to_i8!
          voice.curAddr = voice.curAddr &+ 1
        end

        # Shift with sign extend
        step >>= 4

        delta = ((1 + (step.to_i32! << 1).abs) * voice.stepSize.to_i32!) >> 1
        delta = -delta if step <= 0
        delta += @voiceOutput[16 + voiceNum]
        delta = delta.clamp(-32768, 32767)

        @voiceOutput[16 + voiceNum] = ((delta * voice.curVolume) >> 16).to_i16!

        voice.stepSize = ((ADPCM_STEP_TABLE[8 + step].to_i32! * voice.stepSize.to_i32!) >> 6).clamp(1, 2000).to_i16!
      end

      # Process a sample update
      private def stateNormalUpdate : Nil
        echoInput : Int32 = 0

        @readyFlag = 0x80

        # Recalculate echo length
        if @state == STATE_NORMAL_2
          @echo.length = (@echo.endPos - 0x53C).to_i16!
        else
          @echo.length = (@echo.endPos - 0x554).to_i16!
        end

        @echo.length = @echo.length.clamp(0i16, 1024i16)

        # Update PCM voices
        16.times { |v| @voiceOutput[v] = pcmUpdate(v, pointerof(echoInput)) }

        # Update ADPCM voices (one every third sample)
        adpcmUpdate(@stateCounter % 3, @stateCounter.tdiv(3))

        echoOutput : Int16 = @echo.echo(echoInput)

        # Now, we do the magic stuff.
        wet : Int32 = 0
        dry : Int32 = 0
        output : Int32 = 0
        panIndex : UInt16 = 0

        2.times do |ch|
          wet = (ch == 1 ? (echoOutput.to_i32! << 14) : 0)
          dry = (ch == 0 ? (echoOutput.to_i32! << 14) : 0)
          output = 0

          19.times do |v|
            panIndex = @voicePan[v] - 0x110
            panIndex = 97 if panIndex > 97

            # Apply different volume tables on the dry and wet inputs.
            dry -= (@voiceOutput[v].to_i32! * @panTables[ch][PAN_TABLE_DRY][panIndex].to_i32!)
            wet -= (@voiceOutput[v].to_i32! * @panTables[ch][PAN_TABLE_WET][panIndex].to_i32!)
          end

          # Saturate accumulated voices
          dry = dry.clamp(-0x1fffffff, 0x1fffffff) << 2
          wet = wet.clamp(-0x1fffffff, 0x1fffffff) << 2

          # Apply FIR filter on "wet" input.
          wet = @filter[ch].fir((wet >> 16).to_i16!)

          # In mode 2, we do this on the "dry" input, too.
          if @state == STATE_NORMAL_2
            dry = @altFilter[ch].fir((dry >> 16).to_i16!)
          end

          # Output goes through a delay line and attenuation.
          output = (@wet[ch].delay(wet) + @dry[ch].delay(dry))

          # DSP round function
          output = ((output + 0x2000) & ~0x3FFFi32) >> 14
          @output[ch] = output.clamp(-0x7FFF, 0x7FFF).to_i16!

          if @delayUpdate != 0
            @wet[ch].update
            @dry[ch].update
          end
        end

        @delayUpdate = 0

        # After six samples, the next state is executed.
        @stateCounter += 1
        if @stateCounter > 5
          @stateCounter = 0
          @state = @nextState
        end
      end
    end
  end
end
