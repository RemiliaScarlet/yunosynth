#### YunoSynth
#### Copyright (C) 2023-2024 Remilia Scarlet <remilia@posteo.jp>
#### Portions based on VGMPlay, Copyright (C) Valley Bell
####
#### This program is free software: you can redistribute it and/or
#### modify it under the terms of the GNU Affero General Public
#### License as published by the Free Software Foundation, either
#### version 3 of the License, or (at your option) any later version.
####
#### This program is distributed in the hope that it will be useful,
#### but WITHOUT ANY WARRANTY; without even the implied warranty of
#### MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#### Affero General Public License for more details.
####
#### You should have received a copy of the GNU Affero General Public License
#### along with this program.  If not, see <https://www.gnu.org/licenses/>.
require "./emu-qsound-ctr"

####
#### Capcom DL-1425 QSound sound chip emulator interface
####

module Yuno::Chips
  # Capcom DL-1425 QSound sound chip emulator interface
  class QSound < Yuno::AbstractChip
    CHIP_ID = 0x1F_u32

    @clockFromHeader : UInt32 = 0
    @samplingMode : UInt8 = 0
    @chipSampleRate : UInt32 = 0
    @chip : QSoundCtr? = nil

    # Fix broken optimization of old VGMs that cause problems with the Ctr core.
    @keyOnHack : UInt8 = 0

    @startAddrCache : Array(UInt16) = Array(UInt16).new(16, 0)
    @pitchCache : Array(UInt16) = Array(UInt16).new(16, 0)
    @dataLatch : UInt16 = 0u16

    def initialize(chipNum : Int32, absChipCount : Int32, vgm : VgmFile, playbackSampleRate : UInt32,
                   newSamplingMode : UInt8, newPlayerSampleRate : UInt32, *, emuCore : Symbol? = nil,
                   flags : ChipFlags? = nil)
      @clockFromHeader = vgm.getAlternateChipClock(chipNum, Yuno::ChipType::Qsound) || vgm.header.qsoundClock
      @clockFromHeader &= 0xBFFFFFFF
      @playerSampleRate = newPlayerSampleRate
      chipCount = (vgm.header.qsoundClock & 0x40000000) != 0 ? 2 : 1
      initFields(vgm, emuCore, playbackSampleRate, newSamplingMode, chipCount, flags)
    end

    protected def initialize
    end

    protected def initFields(vgm : VgmFile, emuCore : Symbol?, playbackSampleRate : UInt32, newSamplingMode : UInt8,
                             chipCount : Int32, flags : ChipFlags?) : Nil
      @volume = vgm.getChipVolume(self, ChipType::Qsound, chipCount)
      @core = emuCore || QSound.defaultEmuCore
      case @core
      when :ctr
        @samplingMode = newSamplingMode
        @chipSampleRate = @clockFromHeader <= 0 ? playbackSampleRate : @clockFromHeader
      else raise YunoError.new("Unsupported emulation core for QSound: #{@core}")
      end
    end

    def type : ChipType
      ChipType::Qsound
    end

    def name : String
      "Capcom DL-1425 QSound"
    end

    def shortName : String
      "QSound"
    end

    def id : UInt32
      CHIP_ID
    end

    def emuCore : Symbol
      @core
    end

    def self.defaultEmuCore : Symbol
      :ctr
    end

    def start(chipIndex : UInt8, clock : UInt32, flags : ChipFlags? = nil, previousChip : AbstractChip? = nil) : UInt32
      case @core
      when :ctr
        if clock < 10000000
          clock = clock &* 15
          @keyOnHack = 1u8
        end

        @chip = QSoundCtr.new(clock)
        @sampleRate = @chip.not_nil!.clockRate # Update sample rate
        @sampleRate
      else raise YunoError.new("Unsupported emulation core for QSound: #{@core}")
      end
    end

    def getClock : UInt32
      @clockFromHeader
    end

    def getStartFlags(vgm : VgmFile) : ChipFlags?
      nil
    end

    def update(outputs : OutputBuffers, samples : Int) : Nil
      @chip.not_nil!.update(outputs, samples.to_u32!)
    end

    def reset(chipIndex : UInt8) : Nil
      @chip.not_nil!.reset
    end

    def read(chipIndex : UInt8, offset : Int) : UInt8|UInt16|UInt32
      @chip.not_nil!.read(offset)
    end

    def write(chipIndex : UInt8, offset : Int, data : Int, port : UInt8 = 0) : Nil
      case @core
      when :ctr
        chip = @chip.not_nil!.as(QSoundCtr)

        if @keyOnHack != 0
          # Need to handle three cases, as vgm_cmp can remove writes to both
          # phase and bank registers, depending on version:
          #
          #   - start address was written before end/loop, but phase register is
          #     written.
          #   - as above, but phase is not written (we use bank as a backup
          #     then).
          #   - voice parameters are written during a note (we can't rewrite the
          #     address then).
          case offset
          when 0
            @dataLatch = (@dataLatch & 0x00FF) | (data.to_u16! << 8)
          when 1
            @dataLatch = (@dataLatch & 0xFF00) | data.to_u16!
          when 2
            unless data > 0x7F
              ch : UInt8 = (data >> 3).to_u8!
              case data & 7
              when 1 # Start address write
                @startAddrCache[ch] = @dataLatch

              when 2 # Pitch write
                # Old HLE assumed writing a non-zero value after a zero value
                # was Key On.
                if @pitchCache[ch] == 0 && @dataLatch != 0
                  chip.writeData((ch << 3) + 1, @startAddrCache[ch])
                end

                @pitchCache[ch] = @dataLatch

              when 3 # Phase (old HLE also assumed this was Key On)
                chip.writeData((ch << 3) + 1, @startAddrCache[ch])
              end
            end
          end
        end

        chip.write(offset, data.to_u8!)

        # Need to wait until the chip is ready before we start writing to it.
        # We do this by time travel.
        if offset == 2 && data == 0xE3
          chip.waiteBusy
        end
      end
    end

    def writeDac(port : Int, command : Int, data : Int) : Nil
      write(0, 0x00, port)    # Data MSB
      write(0, 0x00, command) # Data LSB
      write(0, 0x00, data)    # Register
    end

    def setMuteMask(chipIndex : UInt8, mask : UInt32) : Nil
      @chip.not_nil!.muteMask = mask
    end

    def getVolModifier : UInt32
      @volume.to_u32
    end

    def baseVolume : UInt16
      0x100_u16
    end

    @[AlwaysInline]
    def writeRom(romSize : Int32, dataStart : Int32, dataLength : Int32, romData : Slice(UInt8)) : Nil
      @chip.not_nil!.writeRom(romSize, dataStart, dataLength, romData)
    end

    # The internal interface for the chip.
    @[AlwaysInline]
    def chip : AbstractEmulator
      @chip.not_nil!
    end
  end
end
