#### YM2151 Emulator.
####
#### (c) 1997-2002 Jarek Burczynski (s0246@poczta.onet.pl, bujar@mame.net)
#### Some of the optimizing ideas by Tatsuyuki Satoh
#### Crystal port Copyright (C) 2023-2024 Remilia Scarlet
#### BSD 3-Clause License
####
#### Version 2.150 final beta May, 11th 2002
####
####
#### I would like to thank following people for making this project possible:
####
#### Beauty Planets - for making a lot of real YM2151 samples and providing
#### additional informations about the chip. Also for the time spent making
#### the samples and the speed of replying to my endless requests.
####
#### Shigeharu Isoda - for general help, for taking time to scan his YM2151
#### Japanese Manual first of all, and answering MANY of my questions.
####
#### Nao - for giving me some info about YM2151 and pointing me to Shigeharu.
#### Also for creating fmemu (which I still use to test the emulator).
####
#### Aaron Giles and Chris Hardy - they made some samples of one of my favourite
#### arcade games so I could compare it to my emulator.
####
#### Bryan McPhail and Tim (powerjaw) - for making some samples.
####
#### Ishmair - for the datasheet and motivation.

####
#### Yamaha YM2151 Mame sound chip tables and constants.
####

module Yuno::Chips
  class YM2151 < AbstractChip
    private class YM2151Mame < Yuno::AbstractEmulator
      ###
      ### Constants and Tables
      ###

      SAMPLE_BITS = 16

      FREQ_SH  = 16 # 16.16 fixed point (frequency calculations)
      EG_SH    = 16 # 16.16 fixed point (envelope generator timing)
      LFO_SH   = 10 # 20.10 fixed point (LFO calculations)
      TIMER_SH = 16 # 16.16 fixed point (timers calculations)

      FREQ_MASK = (1u32 << FREQ_SH) - 1u32

      ENV_BITS = 10
      ENV_LEN = 1 << ENV_BITS
      ENV_STEP = 128.0 / ENV_LEN

      MAX_ATT_INDEX = ENV_LEN - 1 # 1023
      MIN_ATT_INDEX = 0

      EG_ATT = 4_u32
      EG_DEC = 3_u32
      EG_SUS = 2_u32
      EG_REL = 1_u32
      EG_OFF = 0_u32

      SIN_BITS = 10
      SIN_LEN = 1u32 << SIN_BITS
      SIN_MASK = SIN_LEN - 1u32

      TL_RES_LEN = 256 # 8 bit addressing (real chip)

      TL_TAB_LEN = 13 * 2 * TL_RES_LEN
      @tlTab : Array(Int32) = Array(Int32).new(TL_TAB_LEN, 0)

      ENV_QUIET = TL_TAB_LEN >> 3

      # Sine wave table in decibel scale.
      @sinTab : Array(UInt32) = Array(UInt32).new(SIN_LEN, 0)

      # Translate from D1L to volume index (16 D1L levels)
      @d1lTab : Array(UInt32) = Array(UInt32).new(16, 0)

      RATE_STEPS = 8u8
      EG_INC = [
        # cycle: 0 1  2 3  4 5  6 7
        0u8,1u8, 0u8,1u8, 0u8,1u8, 0u8,1u8, # rates 00..11 0 (increment by 0 or 1)
        0u8,1u8, 0u8,1u8, 1u8,1u8, 0u8,1u8, # rates 00..11 1
        0u8,1u8, 1u8,1u8, 0u8,1u8, 1u8,1u8, # rates 00..11 2
        0u8,1u8, 1u8,1u8, 1u8,1u8, 1u8,1u8, # rates 00..11 3

        1u8,1u8, 1u8,1u8, 1u8,1u8, 1u8,1u8, # rate 12 0 (increment by 1)
        1u8,1u8, 1u8,2u8, 1u8,1u8, 1u8,2u8, # rate 12 1
        1u8,2u8, 1u8,2u8, 1u8,2u8, 1u8,2u8, # rate 12 2
        1u8,2u8, 2u8,2u8, 1u8,2u8, 2u8,2u8, # rate 12 3

        2u8,2u8, 2u8,2u8, 2u8,2u8, 2u8,2u8, # rate 13 0 (increment by 2)
        2u8,2u8, 2u8,4u8, 2u8,2u8, 2u8,4u8, # rate 13 1
        2u8,4u8, 2u8,4u8, 2u8,4u8, 2u8,4u8, # rate 13 2
        2u8,4u8, 4u8,4u8, 2u8,4u8, 4u8,4u8, # rate 13 3

        4u8,4u8, 4u8,4u8, 4u8,4u8, 4u8,4u8, # rate 14 0 (increment by 4)
        4u8,4u8, 4u8,8u8, 4u8,4u8, 4u8,8u8, # rate 14 1
        4u8,8u8, 4u8,8u8, 4u8,8u8, 4u8,8u8, # rate 14 2
        4u8,8u8, 8u8,8u8, 4u8,8u8, 8u8,8u8, # rate 14 3

        8u8,8u8, 8u8,8u8, 8u8,8u8, 8u8,8u8, # rates 15 0, 15 1, 15 2u8, 15 3 (increment by 8)
        16u8,16u8,16u8,16u8,16u8,16u8,16u8,16u8, # rates 15 2, 15 3 for attack
        0u8,0u8, 0u8,0u8, 0u8,0u8, 0u8,0u8, # infinity rates for attack and decay(s)
      ]

      # Envelope Generator rates (32 + 64 rates + 32 RKS).
      @egRateSelect : Array(UInt8) = [
        # 32 fake (infinite time) rates
        18u8 * RATE_STEPS, 18u8 * RATE_STEPS, 18u8 * RATE_STEPS, 18u8 * RATE_STEPS, 18u8 * RATE_STEPS,
        18u8 * RATE_STEPS, 18u8 * RATE_STEPS, 18u8 * RATE_STEPS, 18u8 * RATE_STEPS, 18u8 * RATE_STEPS,
        18u8 * RATE_STEPS, 18u8 * RATE_STEPS, 18u8 * RATE_STEPS, 18u8 * RATE_STEPS, 18u8 * RATE_STEPS,
        18u8 * RATE_STEPS, 18u8 * RATE_STEPS, 18u8 * RATE_STEPS, 18u8 * RATE_STEPS, 18u8 * RATE_STEPS,
        18u8 * RATE_STEPS, 18u8 * RATE_STEPS, 18u8 * RATE_STEPS, 18u8 * RATE_STEPS, 18u8 * RATE_STEPS,
        18u8 * RATE_STEPS, 18u8 * RATE_STEPS, 18u8 * RATE_STEPS, 18u8 * RATE_STEPS, 18u8 * RATE_STEPS,
        18u8 * RATE_STEPS, 18u8 * RATE_STEPS,

        # rates 00-11
        0u8 * RATE_STEPS, 1u8 * RATE_STEPS, 2u8 * RATE_STEPS, 3u8 * RATE_STEPS,
        0u8 * RATE_STEPS, 1u8 * RATE_STEPS, 2u8 * RATE_STEPS, 3u8 * RATE_STEPS,
        0u8 * RATE_STEPS, 1u8 * RATE_STEPS, 2u8 * RATE_STEPS, 3u8 * RATE_STEPS,
        0u8 * RATE_STEPS, 1u8 * RATE_STEPS, 2u8 * RATE_STEPS, 3u8 * RATE_STEPS,
        0u8 * RATE_STEPS, 1u8 * RATE_STEPS, 2u8 * RATE_STEPS, 3u8 * RATE_STEPS,
        0u8 * RATE_STEPS, 1u8 * RATE_STEPS, 2u8 * RATE_STEPS, 3u8 * RATE_STEPS,
        0u8 * RATE_STEPS, 1u8 * RATE_STEPS, 2u8 * RATE_STEPS, 3u8 * RATE_STEPS,
        0u8 * RATE_STEPS, 1u8 * RATE_STEPS, 2u8 * RATE_STEPS, 3u8 * RATE_STEPS,
        0u8 * RATE_STEPS, 1u8 * RATE_STEPS, 2u8 * RATE_STEPS, 3u8 * RATE_STEPS,
        0u8 * RATE_STEPS, 1u8 * RATE_STEPS, 2u8 * RATE_STEPS, 3u8 * RATE_STEPS,
        0u8 * RATE_STEPS, 1u8 * RATE_STEPS, 2u8 * RATE_STEPS, 3u8 * RATE_STEPS,
        0u8 * RATE_STEPS, 1u8 * RATE_STEPS, 2u8 * RATE_STEPS, 3u8 * RATE_STEPS,

        # rate 12
        4u8 * RATE_STEPS, 5u8 * RATE_STEPS, 6u8 * RATE_STEPS, 7u8 * RATE_STEPS,

        # rate 13
        8u8 * RATE_STEPS, 9u8 * RATE_STEPS, 10u8 * RATE_STEPS, 11u8 * RATE_STEPS,

        # rate 14
        12u8 * RATE_STEPS, 13u8 * RATE_STEPS, 14u8 * RATE_STEPS, 15u8 * RATE_STEPS,

        # rate 15
        16u8 * RATE_STEPS, 16u8 * RATE_STEPS, 16u8 * RATE_STEPS, 16u8 * RATE_STEPS,

        # 32 fake rates (same as 15 3)
        16u8 * RATE_STEPS, 16u8 * RATE_STEPS, 16u8 * RATE_STEPS, 16u8 * RATE_STEPS, 16u8 * RATE_STEPS,
        16u8 * RATE_STEPS, 16u8 * RATE_STEPS, 16u8 * RATE_STEPS, 16u8 * RATE_STEPS, 16u8 * RATE_STEPS,
        16u8 * RATE_STEPS, 16u8 * RATE_STEPS, 16u8 * RATE_STEPS, 16u8 * RATE_STEPS, 16u8 * RATE_STEPS,
        16u8 * RATE_STEPS, 16u8 * RATE_STEPS, 16u8 * RATE_STEPS, 16u8 * RATE_STEPS, 16u8 * RATE_STEPS,
        16u8 * RATE_STEPS, 16u8 * RATE_STEPS, 16u8 * RATE_STEPS, 16u8 * RATE_STEPS, 16u8 * RATE_STEPS,
        16u8 * RATE_STEPS, 16u8 * RATE_STEPS, 16u8 * RATE_STEPS, 16u8 * RATE_STEPS, 16u8 * RATE_STEPS,
        16u8 * RATE_STEPS, 16u8 * RATE_STEPS
      ]

      @egRateShift : Array(UInt8) = [
        # 32 infinite time rates
        0u8, 0u8, 0u8, 0u8, 0u8, 0u8, 0u8, 0u8,
        0u8, 0u8, 0u8, 0u8, 0u8, 0u8, 0u8, 0u8,
        0u8, 0u8, 0u8, 0u8, 0u8, 0u8, 0u8, 0u8,
        0u8, 0u8, 0u8, 0u8, 0u8, 0u8, 0u8, 0u8,

        # rates 00-11
        11u8, 11u8, 11u8, 11u8,
        10u8, 10u8, 10u8, 10u8,
        9u8, 9u8, 9u8, 9u8,
        8u8, 8u8, 8u8, 8u8,
        7u8, 7u8, 7u8, 7u8,
        6u8, 6u8, 6u8, 6u8,
        5u8, 5u8, 5u8, 5u8,
        4u8, 4u8, 4u8, 4u8,
        3u8, 3u8, 3u8, 3u8,
        2u8, 2u8, 2u8, 2u8,
        1u8, 1u8, 1u8, 1u8,
        0u8, 0u8, 0u8, 0u8,

        # rate 12
        0u8, 0u8, 0u8, 0u8,

        # rate 13
        0u8, 0u8, 0u8, 0u8,

        # rate 14
        0u8, 0u8, 0u8, 0u8,

        # rate 15
        0u8, 0u8, 0u8, 0u8,

        # 32 dummy rates (same as 15 3)
        0u8, 0u8, 0u8, 0u8, 0u8, 0u8, 0u8, 0u8,
        0u8, 0u8, 0u8, 0u8, 0u8, 0u8, 0u8, 0u8,
        0u8, 0u8, 0u8, 0u8, 0u8, 0u8, 0u8, 0u8,
        0u8, 0u8, 0u8, 0u8, 0u8, 0u8, 0u8, 0u8
      ]

      # DT2 defines offset in cents from base note
      #
      #   This table defines offset in frequency-deltas table.
      #   User's Manual page 22
      #
      #   Values below were calculated using formula: value =  orig.val / 1.5625
      #
      #   DT2=0 DT2=1 DT2=2 DT2=3
      #   0     600   781   950
      @dt2Tab : Array(UInt32) = [0u32, 384u32, 500u32, 608u32]


      # DT1 defines offset in Hertz from base note.  This table is converted
      # while initialization...  Detune table shown in YM2151 User's Manual is
      # wrong (verified on the real chip).
      @dt1Tab : Array(UInt8) = [
        # DT1=0
        0u8, 0u8, 0u8, 0u8, 0u8, 0u8, 0u8, 0u8, 0u8, 0u8, 0u8, 0u8, 0u8, 0u8, 0u8, 0u8,
        0u8, 0u8, 0u8, 0u8, 0u8, 0u8, 0u8, 0u8, 0u8, 0u8, 0u8, 0u8, 0u8, 0u8, 0u8, 0u8,

        # DT1=1
        0u8, 0u8, 0u8, 0u8, 1u8, 1u8, 1u8, 1u8, 1u8, 1u8, 1u8, 1u8, 2u8, 2u8, 2u8, 2u8,
        2u8, 3u8, 3u8, 3u8, 4u8, 4u8, 4u8, 5u8, 5u8, 6u8, 6u8, 7u8, 8u8, 8u8, 8u8, 8u8,

        # DT1=2
        1u8, 1u8, 1u8, 1u8, 2u8, 2u8, 2u8, 2u8, 2u8, 3u8, 3u8, 3u8, 4u8, 4u8, 4u8, 5u8,
        5u8, 6u8, 6u8, 7u8, 8u8, 8u8, 9u8, 10u8, 11u8, 12u8, 13u8, 14u8, 16u8, 16u8, 16u8, 16u8,

        # DT1=3
        2u8, 2u8, 2u8, 2u8, 2u8, 3u8, 3u8, 3u8, 4u8, 4u8, 4u8, 5u8, 5u8, 6u8, 6u8, 7u8,
        8u8, 8u8, 9u8, 10u8, 11u8, 12u8, 13u8, 14u8, 16u8, 17u8, 19u8, 20u8, 22u8, 22u8, 22u8, 22u8
      ]

      @phaseIncRom : Array(UInt16) = [
        1299u16, 1300u16, 1301u16, 1302u16, 1303u16, 1304u16, 1305u16, 1306u16,
        1308u16, 1309u16, 1310u16, 1311u16, 1313u16, 1314u16, 1315u16, 1316u16,
        1318u16, 1319u16, 1320u16, 1321u16, 1322u16, 1323u16, 1324u16, 1325u16,
        1327u16, 1328u16, 1329u16, 1330u16, 1332u16, 1333u16, 1334u16, 1335u16,
        1337u16, 1338u16, 1339u16, 1340u16, 1341u16, 1342u16, 1343u16, 1344u16,
        1346u16, 1347u16, 1348u16, 1349u16, 1351u16, 1352u16, 1353u16, 1354u16,
        1356u16, 1357u16, 1358u16, 1359u16, 1361u16, 1362u16, 1363u16, 1364u16,
        1366u16, 1367u16, 1368u16, 1369u16, 1371u16, 1372u16, 1373u16, 1374u16,
        1376u16, 1377u16, 1378u16, 1379u16, 1381u16, 1382u16, 1383u16, 1384u16,
        1386u16, 1387u16, 1388u16, 1389u16, 1391u16, 1392u16, 1393u16, 1394u16,
        1396u16, 1397u16, 1398u16, 1399u16, 1401u16, 1402u16, 1403u16, 1404u16,
        1406u16, 1407u16, 1408u16, 1409u16, 1411u16, 1412u16, 1413u16, 1414u16,
        1416u16, 1417u16, 1418u16, 1419u16, 1421u16, 1422u16, 1423u16, 1424u16,
        1426u16, 1427u16, 1429u16, 1430u16, 1431u16, 1432u16, 1434u16, 1435u16,
        1437u16, 1438u16, 1439u16, 1440u16, 1442u16, 1443u16, 1444u16, 1445u16,
        1447u16, 1448u16, 1449u16, 1450u16, 1452u16, 1453u16, 1454u16, 1455u16,
        1458u16, 1459u16, 1460u16, 1461u16, 1463u16, 1464u16, 1465u16, 1466u16,
        1468u16, 1469u16, 1471u16, 1472u16, 1473u16, 1474u16, 1476u16, 1477u16,
        1479u16, 1480u16, 1481u16, 1482u16, 1484u16, 1485u16, 1486u16, 1487u16,
        1489u16, 1490u16, 1492u16, 1493u16, 1494u16, 1495u16, 1497u16, 1498u16,
        1501u16, 1502u16, 1503u16, 1504u16, 1506u16, 1507u16, 1509u16, 1510u16,
        1512u16, 1513u16, 1514u16, 1515u16, 1517u16, 1518u16, 1520u16, 1521u16,
        1523u16, 1524u16, 1525u16, 1526u16, 1528u16, 1529u16, 1531u16, 1532u16,
        1534u16, 1535u16, 1536u16, 1537u16, 1539u16, 1540u16, 1542u16, 1543u16,
        1545u16, 1546u16, 1547u16, 1548u16, 1550u16, 1551u16, 1553u16, 1554u16,
        1556u16, 1557u16, 1558u16, 1559u16, 1561u16, 1562u16, 1564u16, 1565u16,
        1567u16, 1568u16, 1569u16, 1570u16, 1572u16, 1573u16, 1575u16, 1576u16,
        1578u16, 1579u16, 1580u16, 1581u16, 1583u16, 1584u16, 1586u16, 1587u16,
        1590u16, 1591u16, 1592u16, 1593u16, 1595u16, 1596u16, 1598u16, 1599u16,
        1601u16, 1602u16, 1604u16, 1605u16, 1607u16, 1608u16, 1609u16, 1610u16,
        1613u16, 1614u16, 1615u16, 1616u16, 1618u16, 1619u16, 1621u16, 1622u16,
        1624u16, 1625u16, 1627u16, 1628u16, 1630u16, 1631u16, 1632u16, 1633u16,
        1637u16, 1638u16, 1639u16, 1640u16, 1642u16, 1643u16, 1645u16, 1646u16,
        1648u16, 1649u16, 1651u16, 1652u16, 1654u16, 1655u16, 1656u16, 1657u16,
        1660u16, 1661u16, 1663u16, 1664u16, 1666u16, 1667u16, 1669u16, 1670u16,
        1672u16, 1673u16, 1675u16, 1676u16, 1678u16, 1679u16, 1681u16, 1682u16,
        1685u16, 1686u16, 1688u16, 1689u16, 1691u16, 1692u16, 1694u16, 1695u16,
        1697u16, 1698u16, 1700u16, 1701u16, 1703u16, 1704u16, 1706u16, 1707u16,
        1709u16, 1710u16, 1712u16, 1713u16, 1715u16, 1716u16, 1718u16, 1719u16,
        1721u16, 1722u16, 1724u16, 1725u16, 1727u16, 1728u16, 1730u16, 1731u16,
        1734u16, 1735u16, 1737u16, 1738u16, 1740u16, 1741u16, 1743u16, 1744u16,
        1746u16, 1748u16, 1749u16, 1751u16, 1752u16, 1754u16, 1755u16, 1757u16,
        1759u16, 1760u16, 1762u16, 1763u16, 1765u16, 1766u16, 1768u16, 1769u16,
        1771u16, 1773u16, 1774u16, 1776u16, 1777u16, 1779u16, 1780u16, 1782u16,
        1785u16, 1786u16, 1788u16, 1789u16, 1791u16, 1793u16, 1794u16, 1796u16,
        1798u16, 1799u16, 1801u16, 1802u16, 1804u16, 1806u16, 1807u16, 1809u16,
        1811u16, 1812u16, 1814u16, 1815u16, 1817u16, 1819u16, 1820u16, 1822u16,
        1824u16, 1825u16, 1827u16, 1828u16, 1830u16, 1832u16, 1833u16, 1835u16,
        1837u16, 1838u16, 1840u16, 1841u16, 1843u16, 1845u16, 1846u16, 1848u16,
        1850u16, 1851u16, 1853u16, 1854u16, 1856u16, 1858u16, 1859u16, 1861u16,
        1864u16, 1865u16, 1867u16, 1868u16, 1870u16, 1872u16, 1873u16, 1875u16,
        1877u16, 1879u16, 1880u16, 1882u16, 1884u16, 1885u16, 1887u16, 1888u16,
        1891u16, 1892u16, 1894u16, 1895u16, 1897u16, 1899u16, 1900u16, 1902u16,
        1904u16, 1906u16, 1907u16, 1909u16, 1911u16, 1912u16, 1914u16, 1915u16,
        1918u16, 1919u16, 1921u16, 1923u16, 1925u16, 1926u16, 1928u16, 1930u16,
        1932u16, 1933u16, 1935u16, 1937u16, 1939u16, 1940u16, 1942u16, 1944u16,
        1946u16, 1947u16, 1949u16, 1951u16, 1953u16, 1954u16, 1956u16, 1958u16,
        1960u16, 1961u16, 1963u16, 1965u16, 1967u16, 1968u16, 1970u16, 1972u16,
        1975u16, 1976u16, 1978u16, 1980u16, 1982u16, 1983u16, 1985u16, 1987u16,
        1989u16, 1990u16, 1992u16, 1994u16, 1996u16, 1997u16, 1999u16, 2001u16,
        2003u16, 2004u16, 2006u16, 2008u16, 2010u16, 2011u16, 2013u16, 2015u16,
        2017u16, 2019u16, 2021u16, 2022u16, 2024u16, 2026u16, 2028u16, 2029u16,
        2032u16, 2033u16, 2035u16, 2037u16, 2039u16, 2041u16, 2043u16, 2044u16,
        2047u16, 2048u16, 2050u16, 2052u16, 2054u16, 2056u16, 2058u16, 2059u16,
        2062u16, 2063u16, 2065u16, 2067u16, 2069u16, 2071u16, 2073u16, 2074u16,
        2077u16, 2078u16, 2080u16, 2082u16, 2084u16, 2086u16, 2088u16, 2089u16,
        2092u16, 2093u16, 2095u16, 2097u16, 2099u16, 2101u16, 2103u16, 2104u16,
        2107u16, 2108u16, 2110u16, 2112u16, 2114u16, 2116u16, 2118u16, 2119u16,
        2122u16, 2123u16, 2125u16, 2127u16, 2129u16, 2131u16, 2133u16, 2134u16,
        2137u16, 2139u16, 2141u16, 2142u16, 2145u16, 2146u16, 2148u16, 2150u16,
        2153u16, 2154u16, 2156u16, 2158u16, 2160u16, 2162u16, 2164u16, 2165u16,
        2168u16, 2170u16, 2172u16, 2173u16, 2176u16, 2177u16, 2179u16, 2181u16,
        2185u16, 2186u16, 2188u16, 2190u16, 2192u16, 2194u16, 2196u16, 2197u16,
        2200u16, 2202u16, 2204u16, 2205u16, 2208u16, 2209u16, 2211u16, 2213u16,
        2216u16, 2218u16, 2220u16, 2222u16, 2223u16, 2226u16, 2227u16, 2230u16,
        2232u16, 2234u16, 2236u16, 2238u16, 2239u16, 2242u16, 2243u16, 2246u16,
        2249u16, 2251u16, 2253u16, 2255u16, 2256u16, 2259u16, 2260u16, 2263u16,
        2265u16, 2267u16, 2269u16, 2271u16, 2272u16, 2275u16, 2276u16, 2279u16,
        2281u16, 2283u16, 2285u16, 2287u16, 2288u16, 2291u16, 2292u16, 2295u16,
        2297u16, 2299u16, 2301u16, 2303u16, 2304u16, 2307u16, 2308u16, 2311u16,
        2315u16, 2317u16, 2319u16, 2321u16, 2322u16, 2325u16, 2326u16, 2329u16,
        2331u16, 2333u16, 2335u16, 2337u16, 2338u16, 2341u16, 2342u16, 2345u16,
        2348u16, 2350u16, 2352u16, 2354u16, 2355u16, 2358u16, 2359u16, 2362u16,
        2364u16, 2366u16, 2368u16, 2370u16, 2371u16, 2374u16, 2375u16, 2378u16,
        2382u16, 2384u16, 2386u16, 2388u16, 2389u16, 2392u16, 2393u16, 2396u16,
        2398u16, 2400u16, 2402u16, 2404u16, 2407u16, 2410u16, 2411u16, 2414u16,
        2417u16, 2419u16, 2421u16, 2423u16, 2424u16, 2427u16, 2428u16, 2431u16,
        2433u16, 2435u16, 2437u16, 2439u16, 2442u16, 2445u16, 2446u16, 2449u16,
        2452u16, 2454u16, 2456u16, 2458u16, 2459u16, 2462u16, 2463u16, 2466u16,
        2468u16, 2470u16, 2472u16, 2474u16, 2477u16, 2480u16, 2481u16, 2484u16,
        2488u16, 2490u16, 2492u16, 2494u16, 2495u16, 2498u16, 2499u16, 2502u16,
        2504u16, 2506u16, 2508u16, 2510u16, 2513u16, 2516u16, 2517u16, 2520u16,
        2524u16, 2526u16, 2528u16, 2530u16, 2531u16, 2534u16, 2535u16, 2538u16,
        2540u16, 2542u16, 2544u16, 2546u16, 2549u16, 2552u16, 2553u16, 2556u16,
        2561u16, 2563u16, 2565u16, 2567u16, 2568u16, 2571u16, 2572u16, 2575u16,
        2577u16, 2579u16, 2581u16, 2583u16, 2586u16, 2589u16, 2590u16, 2593u16
      ]

      @lfoNoiseWaveform : Array(UInt8) = [
        0xFFu8, 0xEEu8, 0xD3u8, 0x80u8, 0x58u8, 0xDAu8, 0x7Fu8, 0x94u8, 0x9Eu8,
        0xE3u8, 0xFAu8, 0x00u8, 0x4Du8, 0xFAu8, 0xFFu8, 0x6Au8, 0x7Au8, 0xDEu8,
        0x49u8, 0xF6u8, 0x00u8, 0x33u8, 0xBBu8, 0x63u8, 0x91u8, 0x60u8, 0x51u8,
        0xFFu8, 0x00u8, 0xD8u8, 0x7Fu8, 0xDEu8, 0xDCu8, 0x73u8, 0x21u8, 0x85u8,
        0xB2u8, 0x9Cu8, 0x5Du8, 0x24u8, 0xCDu8, 0x91u8, 0x9Eu8, 0x76u8, 0x7Fu8,
        0x20u8, 0xFBu8, 0xF3u8, 0x00u8, 0xA6u8, 0x3Eu8, 0x42u8, 0x27u8, 0x69u8,
        0xAEu8, 0x33u8, 0x45u8, 0x44u8, 0x11u8, 0x41u8, 0x72u8, 0x73u8, 0xDFu8,
        0xA2u8,

        0x32u8, 0xBDu8, 0x7Eu8, 0xA8u8, 0x13u8, 0xEBu8, 0xD3u8, 0x15u8, 0xDDu8,
        0xFBu8, 0xC9u8, 0x9Du8, 0x61u8, 0x2Fu8, 0xBEu8, 0x9Du8, 0x23u8, 0x65u8,
        0x51u8, 0x6Au8, 0x84u8, 0xF9u8, 0xC9u8, 0xD7u8, 0x23u8, 0xBFu8, 0x65u8,
        0x19u8, 0xDCu8, 0x03u8, 0xF3u8, 0x24u8, 0x33u8, 0xB6u8, 0x1Eu8, 0x57u8,
        0x5Cu8, 0xACu8, 0x25u8, 0x89u8, 0x4Du8, 0xC5u8, 0x9Cu8, 0x99u8, 0x15u8,
        0x07u8, 0xCFu8, 0xBAu8, 0xC5u8, 0x9Bu8, 0x15u8, 0x4Du8, 0x8Du8, 0x2Au8,
        0x1Eu8, 0x1Fu8, 0xEAu8, 0x2Bu8, 0x2Fu8, 0x64u8, 0xA9u8, 0x50u8, 0x3Du8,
        0xABu8,

        0x50u8, 0x77u8, 0xE9u8, 0xC0u8, 0xACu8, 0x6Du8, 0x3Fu8, 0xCAu8, 0xCFu8,
        0x71u8, 0x7Du8, 0x80u8, 0xA6u8, 0xFDu8, 0xFFu8, 0xB5u8, 0xBDu8, 0x6Fu8,
        0x24u8, 0x7Bu8, 0x00u8, 0x99u8, 0x5Du8, 0xB1u8, 0x48u8, 0xB0u8, 0x28u8,
        0x7Fu8, 0x80u8, 0xECu8, 0xBFu8, 0x6Fu8, 0x6Eu8, 0x39u8, 0x90u8, 0x42u8,
        0xD9u8, 0x4Eu8, 0x2Eu8, 0x12u8, 0x66u8, 0xC8u8, 0xCFu8, 0x3Bu8, 0x3Fu8,
        0x10u8, 0x7Du8, 0x79u8, 0x00u8, 0xD3u8, 0x1Fu8, 0x21u8, 0x93u8, 0x34u8,
        0xD7u8, 0x19u8, 0x22u8, 0xA2u8, 0x08u8, 0x20u8, 0xB9u8, 0xB9u8, 0xEFu8,
        0x51u8,

        0x99u8, 0xDEu8, 0xBFu8, 0xD4u8, 0x09u8, 0x75u8, 0xE9u8, 0x8Au8, 0xEEu8,
        0xFDu8, 0xE4u8, 0x4Eu8, 0x30u8, 0x17u8, 0xDFu8, 0xCEu8, 0x11u8, 0xB2u8,
        0x28u8, 0x35u8, 0xC2u8, 0x7Cu8, 0x64u8, 0xEBu8, 0x91u8, 0x5Fu8, 0x32u8,
        0x0Cu8, 0x6Eu8, 0x00u8, 0xF9u8, 0x92u8, 0x19u8, 0xDBu8, 0x8Fu8, 0xABu8,
        0xAEu8, 0xD6u8, 0x12u8, 0xC4u8, 0x26u8, 0x62u8, 0xCEu8, 0xCCu8, 0x0Au8,
        0x03u8, 0xE7u8, 0xDDu8, 0xE2u8, 0x4Du8, 0x8Au8, 0xA6u8, 0x46u8, 0x95u8,
        0x0Fu8, 0x8Fu8, 0xF5u8, 0x15u8, 0x97u8, 0x32u8, 0xD4u8, 0x28u8, 0x1Eu8,
        0x55u8
      ]
    end
  end
end
