#### Konami 051649 sound chip emulator
#### Copyright (C) Bryan McPhail
#### Copyright (C) 2023-2024 Remilia Scarlet
#### BSD-3-Clause License
####
#### The original C++ sources were pieced together by Bryan McPhail from a
#### combination of Namco Sound, Amuse by Cab, Haunted Castle schematics and
#### whoever first figured out SCC!
####
#### The Common Lisp port was done by Remilia Scarlet.
####
#### The 051649 is a 5 channel sound generator, each channel gets its waveform
#### from RAM (32 bytes per waveform, 8 bit signed data).
####
#### This sound chip is the same as the sound chip in some Konami megaROM
#### cartridges for the MSX. It is actually well researched and documented:
####
####   http://bifi.msxnet.org/msxnet/tech/scc.html
####
#### Thanks to Sean Young (sean@mess.org) for some bugfixes.
####
#### K052539 is more or less equivalent to this chip except channel 5 does not
#### share waveram with channel 4.

module Yuno::Chips
  # K051649 sound chip emulator.
  class K051649 < Yuno::AbstractChip
    # Actual implementation of the K051649 emulator based on Mame's implementation
    private class K051649Mame < Yuno::AbstractEmulator
      NUM_VOICES = 5
      DEF_GAIN = 8
      FREQ_BITS = 16

      private class Channel
        property counter : UInt64 = 0
        property frequency : Int32 = 0
        property volume : Int32 = 0
        property key : Int32 = 0
        property waveRam : Array(Int8) = Array(Int8).new(32, 0)
        property muted : UInt8 = 0

        def initialize
        end
      end

      ##
      ## Global Sound Parameters
      ##

      @mclock : Int32 = 0
      @rate : Int32 = 0

      ##
      ## Mixer table and internal buffers
      ##

      @mixerTable : Array(Int16) = Array(Int16).new(0, 0)
      @mixerLookupPos : Int32 = 0
      @mixerBuffer : Array(Int16) = Array(Int16).new(0, 0)

      ##
      ## Other stuff
      ##

      @channels : Array(Channel)
      @curReg : UInt8 = 0
      @test : UInt8 = 0

      def initialize
        @channels = Array(Channel).new(NUM_VOICES) { |_| Channel.new }
      end

      def start(clock : UInt32) : UInt32
        rate = (clock & 0x7FFFFFFF).tdiv(16)
        @mclock = (clock & 0x7FFFFFFF).to_i32!
        @rate = rate.to_i32!
        @mixerBuffer = Array(Int16).new(rate, 0)
        initMixerTable
        @channels.each &.muted=(0)
        rate
      end

      def reset : Nil
        @channels.each do |chan|
          chan.frequency = 0
          chan.volume = 0
          chan.counter = 0
          chan.key = 0
        end

        @test = 0
        @curReg = 0u8
      end

      @[AlwaysInline]
      private def waveformWrite(offset : Int32, data : UInt8) : Nil
        unless Yuno.bitflag?(@test, 0x40) || (Yuno.bitflag?(@test, 0x80) && offset >= 0x60)
          if offset >= 0x60
            @channels[3].waveRam[offset & 0x1F] = data.to_i8!
            @channels[4].waveRam[offset & 0x1F] = data.to_i8!
          else
            @channels[offset >> 5].waveRam[offset & 0x1F] = data.to_i8!
          end
        end
      end

      @[AlwaysInline]
      private def waveformWriteK052539(offset : Int32, data : UInt8) : Nil
        # Is the waveram read-only?
        unless Yuno.bitflag?(@test, 0x40)
          # Nope!  Write the data
          @channels[offset >> 5].waveRam[offset & 0x1F] = data.to_i8!
        end
      end

      @[AlwaysInline]
      private def volumeWrite(offset : Int32, data : UInt8) : Nil
        @channels[offset & 0x07].volume = data.to_i32! & 0x0F
      end

      @[AlwaysInline]
      private def frequencyWrite(offset : Int32, data : UInt8) : Nil
        chan = @channels[offset >> 1]

        # Bit 5 in the test resgister will reset the internal counter
        if Yuno.bitflag?(@test, 0x20)
          chan.counter = ~0u64
        elsif chan.frequency < 9
          chan.counter |= ((1u64 << FREQ_BITS) - 1)
        end

        # Update frequency
        if Yuno.bitflag?(offset, 1)
          chan.frequency = (chan.frequency & 0x0FF) | ((data.to_u32! << 8) & 0xF00)
        else
          chan.frequency = (chan.frequency & 0xF00) | data.to_u32!
        end

        # Valley Ball: Behavior according to openMSX
        chan.counter &= 0xFFFF0000
      end

      @[AlwaysInline]
      private def keyOnOffWrite(offset : Int32, data : UInt8) : Nil
        @channels.each do |chan|
          chan.key = (data & 1).to_i32!
          data = data >> 1
        end
      end

      @[AlwaysInline]
      def write(offset : Int32, data : UInt8) : Nil
        case offset & 1
        when 0x00 then @curReg = data
        when 0x01
          case offset >> 1
          when 0x00 then waveformWrite(@curReg.to_i32!, data)
          when 0x01 then frequencyWrite(@curReg.to_i32!, data)
          when 0x02 then volumeWrite(@curReg.to_i32!, data)
          when 0x03 then keyOnOffWrite(@curReg.to_i32!, data)
          when 0x04 then waveformWriteK052539(@curReg.to_i32!, data)
          when 0x05 then @test = data
          end
        end
      end

      def update(outputs : OutputBuffers, samples : UInt32) : Nil
        outL : Slice(Int32) = outputs[0]
        outR : Slice(Int32) = outputs[1]
        bufPosL : Int32 = 0
        bufPosR : Int32 = 0
        mixPos : Int32 = 0
        offset : Int64 = 0
        step : Int64 = 0
        v : Int64 = 0
        c : UInt64 = 0

        # Check once, then we can avoid bounds checks below.
        if outL.size < samples || outR.size < samples
          raise "K051649 update: Bad number of samples (expected #{samples}, " \
                "but buffers have #{outL.size} and #{outR.size})"
        end

        # Reset the mixer buffer
        @mixerBuffer.fill(0, 0, samples)

        # Render
        @channels.each do |chan|
          # Only look at active channels.  Also, a channel is halted for frequencies <= 8.
          if chan.frequency > 8 && chan.muted == 0
            v = chan.volume.to_i64! * chan.key.to_i64!
            c = chan.counter
            step = ((@mclock.to_i64! * (1 << FREQ_BITS)) / ((chan.frequency + 1f32) * 16f32 * (@rate / 32.0f32)) +
                    0.5f32).to_i64!
            mixPos = 0

            # Add the rendered data
            samples.times do |_|
              c += step
              offset = ((c >> FREQ_BITS) & 0x1F).to_i64!
              @mixerBuffer[mixPos] += (chan.waveRam[offset].to_i32! * v) >> 3
              mixPos += 1
            end

            # Update the counter
            chan.counter = c
          end
        end

        # Mix down
        mixPos = 0
        data : Int16 = 0
        samples.times do |_|
          data = @mixerTable[@mixerLookupPos + @mixerBuffer[mixPos]]
          outL.unsafe_put(bufPosL, data.to_i32!)
          outR.unsafe_put(bufPosR, data.to_i32!)
          bufPosL += 1
          bufPosR += 1
          mixPos += 1
        end
      end

      def unmuteAll : Nil
        @channels.each &.muted=(0)
      end

      @[AlwaysInline]
      def muteMask=(mask : UInt32) : Nil
        @channels.each_with_index do |chan, idx|
          chan.muted = (mask >> idx) & 0x01
        end
      end

      private def initMixerTable : Nil
        count = NUM_VOICES * 256
        @mixerTable = Array(Int16).new(2 * count, 0)

        # Go to the middle of the table
        @mixerLookupPos = count

        # Fill in the table - 16-bit case
        val : Int16 = 0
        count.times do |i|
          val = Math.min((i * DEF_GAIN * 16).tdiv(NUM_VOICES), 32768).to_i16
          @mixerTable[@mixerLookupPos + i] = val
          @mixerTable[@mixerLookupPos + (-i)] = -val
        end
      end
    end
  end
end
