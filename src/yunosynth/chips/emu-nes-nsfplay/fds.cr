#### Ported from NSFPlay 2.3 to VGMPlay (including C++ -> C conversion)
#### by Valley Bell on 26 September 2013
####
#### Ported to Crystal by Remilia Scarlet
####
require "./counter"

module Yuno::Chips
  class Nes < Yuno::AbstractChip
    # Actual implementation of the Famicom Disk System sound chip emulator based on NFSPlay's
    # implementation
    private class NesFdsNsfPlay < Yuno::AbstractEmulator
      DEFAULT_CLOCK = 1789772.0
      DEFAULT_RATE = 44100

      OPT_CUTOFF        = 0
      OPT_4085_RESET    = 1
      OPT_WRITE_PROTECT = 2
      OPT_END           = 3

      EMOD = 0
      EVOL = 1

      RC_BITS = 12

      TMOD = 0
      TWAV = 1

      MAIN_VOL = (2.4 * 1223.0) # max FDS vol vs max APU square (arbitrarily 1223).  8-bit approximation.
      MAX_OUT = (32.0 * 63.0) # value that should map to main vol

      MAINS = [
        ((MAIN_VOL / MAX_OUT) * 256.0 * 2.0 / 2.0).to_i32!,
        ((MAIN_VOL / MAX_OUT) * 256.0 * 2.0 / 3.0).to_i32!,
        ((MAIN_VOL / MAX_OUT) * 256.0 * 2.0 / 4.0).to_i32!,
        ((MAIN_VOL / MAX_OUT) * 256.0 * 2.0 / 5.0).to_i32!
      ] of Int32

      BIAS = [ 0, 1, 2, 4, 0, -4, -2, -1 ]

      @rate : Float64 = 0.0
      @clock : Float64 = 0.0
      @mask : Int32 = 0
      @smL : Int32 = 128
      @smR : Int32 = 128
      @fout : Int32 = 0 # Current output
      @option : Array(Int32) = Array(Int32).new(OPT_END, 0)

      @mainIO : Bool = false
      @mainVol : UInt8 = 0u8
      @lastFreq : Int32 = 0 # For trackinfo.  Remi: wav UInt32.  Remi TODO: it seems to be unused.
      @lastVol : Int32 = 0 # For trackinfo.  Remi: wav UInt32.  Remi TODO: it seems to be unused.

      # Two wavetables
      @wave : Array(Array(Int32)) = [
        Array(Int32).new(64, 0),
        Array(Int32).new(64, 0)
      ]
      @freq : Array(UInt32) = [0, 0] of UInt32
      @phase : Array(UInt32) = [0, 0] of UInt32
      @wavWrite : Bool = false
      @wavHalt : Bool = false
      @envHalt : Bool = false
      @modHalt : Bool = false
      @modPos : UInt32 = 0u32
      @modWritePos : UInt32 = 0u32

      # Two ramp envelopes
      @envMode : Array(Bool) = [false, false]
      @envDisable : Array(Bool) = [false, false]
      @envTimer : Array(UInt32) = [0, 0] of UInt32
      @envSpeed : Array(UInt32) = [0, 0] of UInt32
      @envOut : Array(UInt32) = [0, 0] of UInt32
      @mainEnvSpeed : UInt32 = 0u32

      # 1-pole RC lowpass filter
      @rcAccum : Int32 = 0
      @rcK : Int32 = 0
      @rcL : Int32 = 1 << RC_BITS

      @tickCount : Counter = Counter.new
      @tickLast : UInt32 = 0u32

      def initialize(clock : Int, rate : Int)
        @option[OPT_CUTOFF] = 2000
        @option[OPT_4085_RESET] = 0
        @option[OPT_WRITE_PROTECT] = 0 # Not used here
        self.clock = clock.to_f64!
        self.rate = rate.to_f64!
        reset
      end

      def mask=(m : Int) : Nil
        @mask = (m & 1).to_i32!
      end

      def setStereoMask(trk : Int, mixL : Int16, mixR : Int16) : Nil
        return if trk < 0 || trk > 1
        @smL = mixL.to_i32!
        @smR = mixR.to_i32!
      end

      def clock=(@clock : Float64) : Nil
      end

      def rate=(@rate : Float64) : Nil
        @tickCount.initValues(@clock, @rate)
        @tickLast = 0

        # Configure lowpass filter
        cutoff : Float64 = @option.get!(OPT_CUTOFF).to_f64!
        leak : Float64 = if cutoff > 0
                           Math.exp(-2.0 * 3.14159 * cutoff / @rate)
                         else
                           0.0
                         end
        @rcK = (leak * (1 << RC_BITS)).to_i32!
        @rcL = (1 << RC_BITS) - @rcK
      end

      def setOption(id : Int, val : Int) : Nil
        @option[id] = val.to_i32! if id < OPT_END
        self.rate = @rate if id == OPT_CUTOFF # Update cutoff immediately
      end

      def reset : Nil
        @mainIO = true
        @mainVol = 0u8
        @lastFreq = 0
        @lastVol = 0

        @rcAccum = 0

        2.times { |i| @wave[i].fill(0) }
        @freq.fill(0u32)
        @phase.fill(0u32)
        @wavWrite = false
        @wavHalt = true
        @envHalt = true
        @modHalt = true
        @modPos = 0u32
        @modWritePos = 0u32

        @envMode.fill(false)
        @envDisable.fill(true)
        @envTimer.fill(0)
        @envSpeed.fill(0)
        @envOut.fill(0)
        @mainEnvSpeed = 0xFF_u32

        # NOTE: the FDS BIOS reset only does the following related to audio:
        #   $4023 = $00
        #   $4023 = $83 enables main_io
        #   $4080 = $80 output volume = 0, envelope disabled
        #   $408A = $FF main envelope speed set to slowest
        write(0x4023, 0x00)
        write(0x4023, 0x83)
        write(0x4080, 0x80)
        write(0x408A, 0xFF)

        # Reset other stuff
        write(0x4082, 0x00) # wav freq 0
        write(0x4083, 0x80) # wav disable
        write(0x4084, 0x80) # mod strength 0
        write(0x4085, 0x00) # mod position 0
        write(0x4086, 0x00) # mod freq 0
        write(0x4087, 0x80) # mod disable
        write(0x4089, 0x00) # wav write disable, max global volume}
      end

      def tick(clocks : UInt32) : Nil
        # Clock envelopes
        if !@envHalt && !@wavHalt && @mainEnvSpeed != 0
          period : UInt32 = 0

          # Remi: Manually unrolled the 2.times loop
          unless @envDisable.get!(0)
            @envTimer.incf!(0, clocks)
            period = ((@envSpeed.get!(0) + 1) * @mainEnvSpeed) << 3
            while @envTimer.get!(0) >= period
              # Clock the envelope
              if @envMode.get!(0)
                @envOut.incf!(0) if @envOut.get!(0) < 32
              else
                @envOut.decf!(0) if @envOut.get!(0) > 0
              end
              @envTimer.decf!(0, period)
            end
          end

          unless @envDisable.get!(1)
            @envTimer.incf!(1, clocks)
            period = ((@envSpeed.get!(1) + 1) * @mainEnvSpeed) << 3
            while @envTimer.get!(1) >= period
              # Clock the envelope
              if @envMode.get!(1)
                @envOut.incf!(1) if @envOut.get!(1) < 32
              else
                @envOut.decf!(1) if @envOut.get!(1) > 0
              end
              @envTimer.decf!(1, period)
            end
          end
        end

        # Clock the mod table
        unless @modHalt
          # Advance phase, adjust for modulator
          startPos : UInt32 = @phase.get!(TMOD) >> 16
          @phase.incf!(TMOD, clocks * @freq.get!(TMOD))
          endPos : UInt32 = @phase.get!(TMOD) >> 16

          # Wrap the phase to the 64-step table (+ 16 bit accumulator)
          @phase.put!(TMOD, @phase.get!(TMOD) & 0x3FFFFF)

          # Execute all clocked steps
          wv : Int32 = 0
          pm = startPos
          while pm < endPos # Changed from a Range#each to a while loop for better performance
            wv = @wave.get!(TMOD).get!(pm & 0x3F)
            if wv == 4 # 4 resets mod position
              @modPos = 0
            else
              @modPos = @modPos &+ BIAS.get!(wv)
              @modPos &= 0x7F # 7-bit clamp
            end
            pm += 1
          end
        end

        # Clock the wavetable
        unless @wavHalt
          mod : Int32 = 0

          # Complex mod calculation
          if @envOut.get!(EMOD) != 0 # Skip if the modulator is off
            # Convert modPos to 7-bit signed
            pos : Int32 = @modPos < 64 ? @modPos.to_i32! : @modPos.to_i32! - 128

            # Multiply pos by gain, shift off 4 bits but with odd "rounding"
            # behavior.
            temp : Int32 = pos * @envOut.get!(EMOD)
            rem : Int32 = temp & 0x0F
            temp >>= 4
            if rem > 0 &&  !Yuno.bitflag?(temp, 0x80)
              if pos <= 0
                temp -= 1
              else
                temp += 2
              end
            end

            # Wrap if range is exceeded
            while temp >= 192
              temp -= 256
            end
            while temp < -64
              temp += 256
            end

            # Multiply result by pitch, shift off 6 bits, round to nearest.
            temp = (@freq.get!(TWAV) * temp).to_i32!
            rem = temp & 0x3F
            temp >>= 6
            temp += 1 if rem >= 32

            mod = temp
          end

          # Advance wavetable position
          freq : Int32 = @freq.get!(TWAV).to_i32! + mod
          @phase.incf!(TWAV, clocks * freq)
          @phase.put!(TWAV, @phase.get!(TWAV) & 0x3FFFFF) # Wrap

          # Store for trackinfo
          @lastFreq = freq
        end

        # Output volume caps at 32
        volOut : Int32 = @envOut.get!(EVOL).to_i32!
        volOut = 32 if volOut > 32

        # Final output
        unless @wavWrite
          @fout = @wave.get!(TWAV).get!((@phase.get!(TWAV) >> 16) & 0x3F) * volOut
        end

        # NOTE: During wav_halt, the unit still outputs (at phase 0) and volume
        # can affect it if the first sample is nonzero.  Haven't worked out 100%
        # of the conditions for volume to effect (vol envelope does not seem to
        # run, but am unsure) but this implementation is very close to correct.

        # Store the trackinfo
        @lastVol = volOut
      end

      def update(outputs : OutputBuffers, samples : UInt32) : Nil
        raise "Unsupported"
      end

      def render(outputs : Pointer(Int32)) : UInt32
        @tickCount.iup
        clocks : UInt32 = (@tickCount.value &- @tickLast) & 0xFF
        tick(clocks)
        @tickLast = @tickCount.value

        v : Int32 = @fout * MAINS.get!(@mainVol) >> 8

        # Lowpass RC filter
        rcOut : Int32 = ((@rcAccum * @rcK) + (v * @rcL)) >> RC_BITS
        @rcAccum = rcOut
        v = rcOut

        # Output mix
        m : Int32 = @mask != 0 ? 0 : v
        outputs[0] = (m * @smL) >> (7 - 2)
        outputs[1] = (m * @smR) >> (7 - 2)
        2u32
      end

      def write(addr : UInt32, val : UInt32) : Bool
        # $4023 main I/O enable/disable
        if addr == 0x4023
          @mainIO = Yuno.bitflag?(val, 2)
          return true
        end

        return false unless @mainIO
        return false if addr < 0x4040 || addr > 0x408A

        if addr < 0x4080 # $4040-407F wave table write
          if @wavWrite
            @wave.get!(TWAV).put!(addr - 0x4040, (val & 0x3F).to_i32!)
          end
          return true
        end

        case addr & 0xFF
        when 0x80 # $4080 volume envelope
          @envDisable.put!(EVOL, Yuno.bitflag?(val, 0x80))
          @envMode.put!(EVOL, Yuno.bitflag?(val, 0x40))
          @envTimer.put!(EVOL, 0u32)
          @envSpeed.put!(EVOL, val & 0x3F)
          if @envDisable.get!(EVOL)
            @envOut.put!(EVOL, @envSpeed.get!(EVOL))
          end

        when 0x81
          return false

        when 0x82 # $4082 wave frequency low
          @freq.put!(TWAV, (@freq.get!(TWAV) & 0xF00) | val)

        when 0x83 # $4083 wave frequency high / enables
          @freq.put!(TWAV, (@freq.get!(TWAV) & 0x0FF) | ((val & 0x0F) << 8))
          @wavHalt = Yuno.bitflag?(val, 0x80)
          @envHalt = Yuno.bitflag?(val, 0x40)
          @phase.put!(TWAV, 0u32) if @wavHalt
          if @envHalt
            @envTimer.put!(EMOD, 0u32)
            @envTimer.put!(EVOL, 0u32)
          end

        when 0x84 # $4084 mod envelope
          @envDisable.put!(EMOD, Yuno.bitflag?(val, 0x80))
          @envMode.put!(EMOD, Yuno.bitflag?(val, 0x40))
          @envTimer.put!(EMOD, 0u32)
          @envSpeed.put!(EMOD, val & 0x3F)
          @envOut.put!(EMOD, @envSpeed.get!(EMOD)) if @envDisable.get!(EMOD)

        when 0x85 # $4085 mod position
          @modPos = val & 0x7F
          # Not hardware accurate, but prevents detune due to cycle inaccuracies
          # (notably in Bio Miracle Bokutte Upa).
          @phase.put!(TMOD, @modWritePos << 16) if @option.get!(OPT_4085_RESET) != 0

        when 0x86 # $4086 mod frequency low
          @freq.put!(TMOD, (@freq.get!(TMOD) & 0xF00) | val)

        when 0x87 # $4087 mod frequency high / enable
          @freq.put!(TMOD, (@freq.get!(TMOD) & 0x0FF) | ((val & 0x0F) << 8))
          @modHalt = Yuno.bitflag?(val, 0x80)
          if @modHalt
            @phase.put!(TMOD, @phase.get!(TMOD) & 0x3F0000) # Reset accumulator phase
          end

        when 0x88 # $4088 mod table write
          if @modHalt
            # Writes to current playback position (there is no direct way to set
            # phase)
            @wave.get!(TMOD).put!((@phase.get!(TMOD) >> 16) & 0x3F, (val & 0x7F).to_i32!)
            @phase.put!(TMOD, (@phase.get!(TMOD) &+ 0x010000) & 0x3FFFFF)
            @wave.get!(TMOD).put!((@phase.get!(TMOD) >> 16) & 0x3F, (val & 0x7F).to_i32!)
            @phase.put!(TMOD, (@phase.get!(TMOD) &+ 0x010000) & 0x3FFFFF)
            @modWritePos = @phase.get!(TMOD) >> 16 # used by OPT_4085_RESET
          end

        when 0x89 # $4089 wave write enable, master volume
          @wavWrite = Yuno.bitflag?(val, 0x80)
          @mainVol = (val & 0x03).to_u8!

        when 0x8A # $408A envelope speed
          @mainEnvSpeed = val
          # Haven't tested whether this register resets phase on hardware, but
          # this ensures my inplementation won't spam envelope clocks if this
          # value suddenly goes low.
          @envTimer.put!(EMOD, 0u32)
          @envTimer.put!(EVOL, 0u32)

        else return false
        end

        true
      end

      def read(addr : UInt32, val : UInt32) : Tuple(UInt32, Bool)
        case
        when addr >= 0x4040 && addr < 0x407F
          # TODO: If @wavWrite is not enabled, the read address may not be
          # reliable?  Need to test this on hardware.
          {@wave.get!(TWAV)[addr - 0x4040].to_u32!, true}

        when addr == 4090 # $4090 read volume envelope
          {@envOut.get!(EVOL) | 0x40, true}

        when 0x4092 # $4092 read mod envelope
          {@envOut.get!(EMOD) | 0x40, true}

        else
          {val, false}
        end
      end
    end
  end
end
