#### NES 2A03
####
#### Ported from NSFPlay 2.2 to VGMPlay (including C++ -> C conversion)
#### by Valley Bell on 25 September 2013
#### Updated to NSFPlay 2.3 on 26 September 2013
####
#### Ported to Crystal by Remilia Scarlet
####
require "./apu"
require "./counter"

module Yuno::Chips
  class Nes < Yuno::AbstractChip
    # Actual implementation of the NES's DMC emulator based on NFSPlay's
    # implementation
    private class NesDmcNsfPlay < Yuno::AbstractEmulator
      DEFAULT_CLOCK = 1789772.0   # 21477272 (NTSC) / 12 = APU clock
      DEFAULT_CLOCK_PAL = 1662607
      DEFAULT_RATE = 44100u32

      private OFFSET_MAX = (1 << 30) - (4 << 16) # 1073479680

      # volume adjusted by 0.75 based on empirical measurements
      MAIN_VOL = 8192.0 * 0.75

      OPT_UNMUTE_ON_RESET = 1u8
      OPT_NONLINEAR_MIXER = 2u8
      OPT_ENABLE_4011     = 4u8
      OPT_ENABLE_PNOISE   = 8u8
      OPT_DPCM_ANTI_CLICK = 16u8
      OPT_RANDOMIZE_NOISE = 32u8
      OPT_TRI_MUTE        = 64u8
      OPT_TRI_NULL        = 128u8

      OPTID_UNMUTE_ON_RESET = 0
      OPTID_NONLINEAR_MIXER = 1
      OPTID_ENABLE_4011     = 2
      OPTID_ENABLE_PNOISE   = 3
      OPTID_DPCM_ANTI_CLICK = 4
      OPTID_RANDOMIZE_NOISE = 5
      OPTID_TRI_MUTE        = 6
      OPTID_TRI_NULL        = 7
      OPTID_END             = 8

      GETA_BITS = 20

      WAVELEN_TABLE = [
        # NTSC
        [ 4, 8, 16, 32, 64, 96, 128, 160, 202, 254, 380, 508, 762, 1016, 2034, 4068 ] of UInt32,
        # PAL
        [ 4, 8, 14, 30, 60, 88, 118, 148, 188, 236, 354, 472, 708,  944, 1890, 3778 ] of UInt32
      ]

      FREQ_TABLE = [
        # NTSC
        [ 428, 380, 340, 320, 286, 254, 226, 214, 190, 160, 142, 128, 106,  84,  72,  54 ] of UInt32,
        # PAL
        [ 398, 354, 316, 298, 276, 236, 210, 198, 176, 148, 132, 118,  98,  78,  66,  50 ] of UInt32
      ]

      TRI_TABLE = [
        0, 1, 2, 3, 4, 5, 6, 7,
        8, 9, 10, 11, 12, 13, 14, 15,
        15, 14, 13, 12, 11, 10, 9, 8,
        7, 6, 5, 4, 3, 2, 1, 0
      ] of UInt32

      LENGTH_TABLE = [
        0x0A, 0xFE,
        0x14, 0x02,
        0x28, 0x04,
        0x50, 0x06,
        0xA0, 0x08,
        0x3C, 0x0A,
        0x0E, 0x0C,
        0x1A, 0x0E,
        0x0C, 0x10,
        0x18, 0x12,
        0x30, 0x14,
        0x60, 0x16,
        0xC0, 0x18,
        0x48, 0x1A,
        0x10, 0x1C,
        0x20, 0x1E
      ] of UInt8

      @tndTable : Array(Array(Array(UInt32))) = [[[0u32]]]
      @tndTableNL : Array(Array(Array(UInt32))) = [[[0u32]]]
      @option : UInt8 = 0u8
      @mask : Int32 = 0
      @smL : Array(Int32) = [128, 128, 128]
      @smR : Array(Int32) = [128, 128, 128]
      @reg : Array(UInt8) = Array(UInt8).new(0x10, 0u8)
      @lenReg : UInt32 = 0u32
      @addrReg : UInt32 = 0u32
      @memory : Array(UInt8) = [] of UInt8
      @renderMT : Int32 = 0
      @renderMN : Int32 = 0
      @renderMD : Int32 = 0
      @outputT : UInt32 = 0u32
      @outputN : UInt32 = 0u32
      @outputD : UInt32 = 0u32
      @daddress : UInt32 = 0u32
      @length : UInt32 = 0u32
      @data : UInt32 = 0u32
      @damp : Int16 = 0i16
      @dacLsb : Int32 = 0
      @dmcPop : Bool = false
      @dmcPopOffset : Int32 = 0
      @dmcPopFollow : Int32 = 0
      @clock : UInt32 = 0u32
      @rate : UInt32 = 0u32
      @pal : Int32 = 0
      @mode : Int32 = 0
      @irq : Bool = false
      @active : Bool = false

      @counter : Array(UInt32) = [0, 0, 0] of UInt32 # Frequency dividers
      @tphase : Int32 = 0 # Triangle phase
      @nfreq : UInt32 = 0u32 # Noise frequency
      @dfreq : UInt32 = 0u32 # DPCM frequency

      @triFreq : UInt32 = 0u32
      @linearCounter : Int32 = 0
      @linearCounterReload : Int32 = 0
      @linearCounterHalt : Bool = false
      @linearCounterControl : Bool = false

      @noiseVolume : Int32 = 0
      @noise : UInt32 = 0u32
      @noiseTap : UInt32 = 0u32

      ##
      ## Noise envelope
      ##
      @envelopeLoop : Bool = false
      @envelopeDisable : Bool = false
      @envelopeWrite : Bool = false
      @envelopeDivPeriod : Int32 = 0
      @envelopeDiv : Int32 = 0
      @envelopeCounter : Int32 = 0

      @enable : Array(Bool) = [false, false, false]
      @lengthCounter : Array(Int32) = [0, 0] # 0 = tri, 1 = noise

      ##
      ## Frame sequencer
      ##
      property! apu : NesApuNsfPlay? # APU is clocked by DMC's frame sequencer
      @frameSequenceCount : Int32 = 0 # Current cycle count
      @frameSequenceLength : Int32 = 7458 # CPU cycles per frame sequence
      @frameSequenceStep : Int32 = 0 # Current step of frame sequence
      @frameSequenceSteps : Int32 = 4 # 4/5 step per frame
      @frameIrq : Bool = false
      @frameIrqEnable : Bool = false

      @tickCount : Counter = Counter.new
      @tickLast : UInt32 = 0u32

      def initialize(clock : Int, rate : Int)
        self.clock = clock.to_f64!
        self.rate = rate.to_f64!
        @option |= OPT_ENABLE_4011
        @option |= OPT_ENABLE_PNOISE
        @option |= OPT_UNMUTE_ON_RESET
        @option |= OPT_NONLINEAR_MIXER
        @option |= OPT_RANDOMIZE_NOISE
        @option |= OPT_TRI_MUTE

        @tndTable = Array(Array(Array(UInt32))).new(16) do |_|
          Array(Array(UInt32)).new(16) do |_|
            Array(UInt32).new(128, 0u32)
          end
        end

        @tndTableNL = Array(Array(Array(UInt32))).new(16) do |_|
          Array(Array(UInt32)).new(16) do |_|
            Array(UInt32).new(128, 0u32)
          end
        end
      end

      def clock=(value : Float64) : Nil
        @clock = value.to_u32!

        # Check for approximately DEFAULT_CLOCK_PAL
        self.pal = (@clock - DEFAULT_CLOCK_PAL).abs <= 1000
      end

      def rate=(value : Float64) : Nil
        @rate = value != 0 ? value.to_u32! : DEFAULT_RATE
        @tickCount.initValues(@clock, @rate)
        @tickLast = 0
      end

      def pal=(value : Bool) : Nil
        @pal = value ? 1 : 0
        @frameSequenceLength = value ? 8314 : 7458
      end

      def damp : Int32
        (@damp.to_i32! << 1) | @dacLsb
      end

      def mask=(@mask : Int32) : Nil
      end

      def setStereoMix(trk : Int, mixL : Int16, mixR : Int16) : Nil
        @smL[trk] = mixL.to_i32!
        @smR[trk] = mixR.to_i32!
      end

      def frameSequence(s : Int) : Nil
        return if s > 3 # No operation on step 4
        @apu.try &.frameSequence(s)
        @frameIrq = (s == 0 && @frameSequenceSteps == 4)

        ##
        ## 240Hz clock
        ##
        divider : Bool = false
        if @linearCounterHalt
          @linearCounter = @linearCounterReload
        elsif @linearCounter > 0
          @linearCounter -= 1
        end

        unless @linearCounterControl
          @linearCounterHalt = false
        end

        ## Noise envelope
        if @envelopeWrite
          @envelopeWrite = false
          @envelopeCounter = 15
          @envelopeDiv = 0
        else
          if (@envelopeDiv += 1) > @envelopeDivPeriod
            divider = true
            @envelopeDiv = 0
          end
        end

        if divider
          if @envelopeLoop && @envelopeCounter == 0
            @envelopeCounter = 15
          elsif @envelopeCounter > 0
            @envelopeCounter -= 1 # TODO: Make this work.
          end
        end

        # 120Hz clock
        if (s & 1) == 0
          # Triangle length counter
          if !@linearCounterControl && @lengthCounter.get!(0) > 0
            @lengthCounter.decf!(0)
          end

          # Noise length counter
          if !@envelopeLoop && @lengthCounter.get!(1) > 0
            @lengthCounter.decf!(1)
          end
        end
      end

      # 三角波チャンネルの計算 戻り値は0-15
      @[AlwaysInline]
      def calcTri(clocks : UInt32) : UInt32
        if @linearCounter != 0 && @lengthCounter.get!(0) > 0 &&
           (!Yuno.bitflag?(@option, OPT_TRI_MUTE) || @triFreq > 0)
          @counter.incf!(0, clocks)
          while @counter.get!(0) > @triFreq
            @tphase = (@tphase &+ 1) & 31
            @counter.decf!(0, @triFreq + 1)
          end

        # Note: else-block added by VB
        elsif Yuno.bitflag?(@option, OPT_TRI_NULL)
          if @tphase != 0 && @tphase < 31
            # Finish the Triangle wave to prevent clicks.
            @counter.incf!(0, clocks)
            while @counter.get!(0) > @triFreq
              @tphase = (@tphase &+ 1) & 31
              @counter.decf!(0, @triFreq + 1)
            end
          end
        end

        TRI_TABLE.get!(@tphase)
      end

      # ノイズチャンネルの計算 戻り値は0-127
      # 低サンプリングレートで合成するとエイリアスノイズが激しいのでノイズだけは
      # この関数内で高クロック合成し、簡易なサンプリングレート変換を行っている。
      @[AlwaysInline]
      def calcNoise(clocks : UInt32) : UInt32
        env : UInt32 = (@envelopeDisable ? @noiseVolume : @envelopeCounter).to_u32!
        env = 0u32 if @lengthCounter.get!(1) < 1

        last : UInt32 = Yuno.bitflag?(@noise, 0x4000) ? env : 0u32
        return last if clocks < 1

        # Simple anti-aliasing (noise requires it, even when oversampling is off)
        count : UInt32 = 0u32
        accum : UInt32 = 0u32

        @counter.incf!(1, clocks)
        return last if @nfreq <= 0 # Prevent infinite loop -VB

        feedback : UInt32 = 0u32
        while @counter.get!(1) >= @nfreq
          # Tick the noise generator
          feedback = (@noise & 1) ^ (Yuno.bitflag?(@noise, @noiseTap) ? 1 : 0)
          @noise = (@noise >> 1) | (feedback << 14)
          count += 1
          accum += last
          last = Yuno.bitflag?(@noise, 0x4000) ? env : 0u32
          @counter.decf!(1, @nfreq)
        end

        return last if count < 1 # No change over interval, don't anti-alias

        clocksAccum : UInt32 = clocks &- @counter.get!(1)
        accum = (accum &* clocksAccum) &+ (last &* @counter.get!(1) &* count)
        accum.tdiv(clocks &* count)
      end

      # DMCチャンネルの計算 戻り値は0-127
      @[AlwaysInline]
      def calcDmc(clocks : UInt32) : UInt32
        @counter.incf!(2, clocks)
        return (@damp.to_u32! << 1) &+ @dacLsb if @dfreq <= 0 # Prevent infinite loop -VB

        while @counter.get!(2) >= @dfreq
          if @data != 0x100 # data = 0x100 は EMPTY を意味する。
            if Yuno.bitflag?(@data, 1) && @damp < 63
              @damp += 1
            elsif !Yuno.bitflag?(@data, 1) && 0 < @damp
              @damp -= 1
            end
            @data >>= 1
          end

          if @data == 0x100 && @active
            @data = @memory.get!(@daddress - 0x8000).to_u32!
            @data |= (@data & 0xFF) | 0x10000 # 8bitシフトで 0x100 になる
            if @length > 0
              @daddress = ((@daddress + 1) & 0xFFFF) | 0x8000
              @length -= 1
            end
          end

          if @length == 0 # 最後のフェッチが終了したら(再生完了より前に)即座に終端処理
            if Yuno.bitflag?(@mode, 1)
              @daddress = (@addrReg << 6) | 0xC000
              @length = (@lenReg << 4) &+ 1
            else
              @irq = @mode == 2 && @active # 直前がactiveだったときはIRQ発行
              @active = false
            end
          end

          @counter.decf!(2, @dfreq)
        end

        (@damp.to_u32! << 1) &+ @dacLsb
      end

      def tickFrameSequence(clocks : UInt32) : Nil
        @frameSequenceCount += clocks
        while @frameSequenceCount > @frameSequenceLength
          frameSequence(@frameSequenceStep)
          @frameSequenceCount -= @frameSequenceLength
          @frameSequenceStep += 1
          if @frameSequenceStep >= @frameSequenceSteps
            @frameSequenceStep = 0
          end
        end
      end

      @[AlwaysInline]
      def tick(clocks : UInt32) : Nil
        @outputT = calcTri(clocks)
        @outputN = calcNoise(clocks)
        @outputD = calcDmc(clocks)
      end

      def update(outputs : OutputBuffers, samples : UInt32) : Nil
        raise "Unsupported"
      end

      def render(outputs : Pointer(Int32)) : UInt32
        @tickCount.iup # increase counter (overflows after 255)
        clocks : UInt32 = (@tickCount.value &- @tickLast) & 0xFF
        tickFrameSequence(clocks)
        tick(clocks)
        @tickLast = @tickCount.value

        @outputT = Yuno.bitflag?(@mask, 1) ? 0u32 : @outputT
        @outputN = Yuno.bitflag?(@mask, 2) ? 0u32 : @outputN
        @outputD = Yuno.bitflag?(@mask, 4) ? 0u32 : @outputD

        @renderMT = @tndTable.get!(@outputT).get!(0).get!(0).to_i32!
        @renderMN = @tndTable.get!(0).get!(@outputN).get!(0).to_i32!
        @renderMD = @tndTable.get!(0).get!(0).get!(@outputD).to_i32!

        if Yuno.bitflag?(@option, OPT_NONLINEAR_MIXER)
          ref : Int32 = @renderMT &+ @renderMN &+ @renderMD
          voltage : Int32 = @tndTableNL.get!(@outputT).get!(@outputN).get!(@outputD).to_i32!
          if ref != 0
            # Remi: manually unrolled this loop
            @renderMT = (@renderMT &* voltage).tdiv(ref)
            @renderMN = (@renderMN &* voltage).tdiv(ref)
            @renderMD = (@renderMD &* voltage).tdiv(ref)
          else
            @renderMT = @renderMN = @renderMD = voltage
          end
        end

        # anti-click nullifies any $4011 write but preserves nonlinearity
        if Yuno.bitflag?(@option, OPT_DPCM_ANTI_CLICK)
          if @dmcPop # $4011 will cause pop this frame
            # adjust offset to counteract pop
            @dmcPopOffset += @dmcPopFollow - @renderMD
            @dmcPop = false

            # prevent overflow, keep headspace at edges
            @dmcPopOffset = @dmcPopOffset.clamp(-OFFSET_MAX, OFFSET_MAX)
          end

          @dmcPopFollow = @renderMD # remember previous position
          @renderMD = @renderMD &+ @dmcPopOffset # apply offset

          # TODO implement this in a better way
          # roll off offset (not ideal, but prevents overflow)
          if @dmcPopOffset > 0
            @dmcPopOffset -= 1
          elsif @dmcPopOffset < 0
            @dmcPopOffset += 1
          end
        end

        outputs[0]  = @renderMT &* @smL.get!(0)
        outputs[0] += @renderMN &* @smL.get!(1)
        outputs[0] += @renderMD &* @smL.get!(2)
        outputs[0] >>= (7 - 2)

        outputs[1]  = @renderMT &* @smR.get!(0)
        outputs[1] += @renderMN &* @smR.get!(1)
        outputs[1] += @renderMD &* @smR.get!(2)
        outputs[1] >>= (7 - 2)

        2u32
      end

      def initializeTNDTable(wt : Float64, wn : Float64, wd : Float64) : Nil
        # Linear mixer
        16.times do |t|
          16.times do |n|
            128.times do |d|
              @tndTable[t][n][d] = (MAIN_VOL * (3.0 * t + 2.0 * n + d) / 208.0).to_u32!
            end
          end
        end

        # Non-linear mixer
        @tndTableNL[0][0][0] = 0
        16.times do |t|
          16.times do |n|
            128.times do |d|
              if t != 0 || n != 0 || d != 0
                @tndTableNL[t][n][d] = ((MAIN_VOL * 159.79) / (100.0 + (1.0 / ((t / wt) + (n / wn) + (d / wd))))).to_u32!
              end
            end
          end
        end
      end

      def reset : Nil
        @mask = 0
        initializeTNDTable(8227, 12241, 22638) # Remi TODO: Make these constants
        @counter.fill(0u32)
        @tphase = 0
        @nfreq = WAVELEN_TABLE[0][0]
        @dfreq = FREQ_TABLE[0][0]

        @envelopeDiv = 0
        @lengthCounter.fill(0)
        @linearCounter = 0
        @envelopeCounter = 0

        @frameIrq = false
        @frameIrqEnable = false
        @frameSequenceCount = 0
        @frameSequenceSteps = 4
        @frameSequenceStep = 0

        0x10.times { |i| write(0x4008_u32 + i, 0u32) }

        @irq = false
        write(0x4015, 0)
        write(0x4015, 0x0F) if Yuno.bitflag?(@option, OPT_UNMUTE_ON_RESET)
        @outputT = 0u32
        @outputN = 0u32
        @outputD = 0u32
        @triFreq = 0
        @damp = 0
        @dmcPop = false
        @dmcPopOffset = 0
        @dmcPopFollow = 0
        @dacLsb = 0
        @data = 0x100
        @addrReg = 0
        @active = false
        @length = 0
        @lenReg = 0
        @daddress = 0
        @noise = 1
        @noiseTap = 1u32 << 1
        @noise |= Random.rand(UInt32) if Yuno.bitflag?(@option, OPT_RANDOMIZE_NOISE)
        self.rate = @rate.to_f64!
      end

      def memory=(@memory : Array(UInt8)) : Nil
      end

      def setOption(id : Int, val : Int)
        case id
        when OPTID_DPCM_ANTI_CLICK
          val != 0 ? (@option |= OPT_DPCM_ANTI_CLICK) : (@option &= ~OPT_DPCM_ANTI_CLICK)

        when OPTID_ENABLE_4011
          val != 0 ? (@option |= OPT_ENABLE_4011) : (@option &= ~OPT_ENABLE_4011)

        when OPTID_ENABLE_PNOISE
          val != 0 ? (@option |= OPT_ENABLE_PNOISE) : (@option &= ~OPT_ENABLE_PNOISE)

        when OPTID_NONLINEAR_MIXER
          val != 0 ? (@option |= OPT_NONLINEAR_MIXER) : (@option &= ~OPT_NONLINEAR_MIXER)
          initializeTNDTable(8227, 12241, 22638) # Remi TODO: Make these constantw

        when OPTID_RANDOMIZE_NOISE
          val != 0 ? (@option |= OPT_RANDOMIZE_NOISE) : (@option &= ~OPT_RANDOMIZE_NOISE)

        when OPTID_TRI_MUTE
          val != 0 ? (@option |= OPT_TRI_MUTE) : (@option &= ~OPT_TRI_MUTE)

        when OPTID_TRI_NULL
          val != 0 ? (@option |= OPT_TRI_NULL) : (@option &= ~OPT_TRI_NULL)

        when OPTID_UNMUTE_ON_RESET
          val != 0 ? (@option |= OPT_UNMUTE_ON_RESET) : (@option &= ~OPT_UNMUTE_ON_RESET)
        end
      end

      def write(addr : UInt32, val : UInt32) : Bool
        if addr == 0x4015
          @enable.put!(0, Yuno.bitflag?(val, 4))
          @enable.put!(1, Yuno.bitflag?(val, 8))
          @lengthCounter.put!(0, 0) unless @enable.get!(0)
          @lengthCounter.put!(1, 0) unless @enable.get!(1)

          if Yuno.bitflag?(val, 16) && !@active
            @active = true
            @enable.put!(2, true)
            @daddress = (0xC000_u32 | (@addrReg << 6))
            @length = (@lenReg << 4) &+ 1
            @irq = false
          elsif !Yuno.bitflag?(val, 16)
            @active = false
            @enable.put!(2, false)
          end

          @reg.put!(13, val.to_u8!) # addr is known to be 0x4015, addr - 0x4008 = 13
          return true
        end

        if addr == 0x4017
          @frameIrqEnable = ((val & 0x40) == 0x40)
          @frameIrq = @frameIrqEnable ? @frameIrq : false
          @frameSequenceCount = 0
          if Yuno.bitflag?(val, 0x80)
            @frameSequenceSteps = 5
            @frameSequenceStep = 0
            frameSequence(@frameSequenceStep)
            @frameSequenceStep += 1
          else
            @frameSequenceSteps = 4
            @frameSequenceStep = 1
          end
        end

        return false if addr < 0x4008 || 0x4013 < addr
        @reg.put!(addr - 0x4008, (val & 0xFF).to_u8!) # Safe due to the check above

        case addr
        ##
        ## Tri
        ##
        when 0x4008
          @linearCounterControl = ((val >> 7) & 1) != 0
          @linearCounterReload = (val & 0x7F).to_i32!

        when 0x4009
          nil

        when 0x400A
          @triFreq = val | (@triFreq & 0x700)
          @counter.put!(0, @triFreq) if @counter.get!(0) > @triFreq

        when 0x400B
          @triFreq = (@triFreq & 0xFF) | ((val & 0x7) << 8)
          @counter.put!(0, @triFreq) if @counter.get!(0) > @triFreq
          @linearCounterHalt = true
          if @enable.get!(0)
            # Safe due to the `& 0x1F`
            @lengthCounter.put!(0, LENGTH_TABLE.get!((val >> 3) & 0x1F).to_i32!)
          end

        ##
        ## Noise
        ##
        when 0x400C
          @noiseVolume = val.to_i32! & 15
          @envelopeDivPeriod = val.to_i32! & 15
          @envelopeDisable = ((val >> 4) & 1) != 0
          @envelopeLoop = ((val >> 5) & 1) != 0

        when 0x400D
          nil

        when 0x400E
          if Yuno.bitflag?(@option, OPT_ENABLE_PNOISE)
            @noiseTap = Yuno.bitflag?(val, 0x80) ? (1u32 << 6) : (1u32 << 1)
          else
            @noiseTap = 1u32 << 1
          end
          @nfreq = WAVELEN_TABLE.get!(@pal).get!(val & 15)
          @counter.put!(1, @nfreq) if @counter.get!(1) > @nfreq

        when 0x400F
          if @enable.get!(1)
            @lengthCounter.put!(1, LENGTH_TABLE.get!((val >> 3) & 0x1F).to_i32!)
          end
          @envelopeWrite = true

        ##
        ## DMC
        ##
        when 0x4010
          @mode = ((val >> 6) & 3).to_i32!
          @dfreq = FREQ_TABLE.get!(@pal).get!(val & 15)
          @counter.put!(2, @dfreq) if @counter.get!(2) > @dfreq

        when 0x4011
          if Yuno.bitflag?(@option, OPT_ENABLE_4011)
            @damp = ((val >> 1) & 0x3F).to_i16!
            @dacLsb = val.to_i32! & 1
            @dmcPop = true
          end

        when 0x4012
          @addrReg = val & 0xFF
          # ここでdaddressは更新されない

        when 0x4013
          @lenReg = val & 0xFF
          # ここでlengthは更新されない

        else return false
        end

        true
      end

      def read(addr : UInt32, val : UInt32) : Tuple(UInt32, Bool)
        ret : UInt32 = val
        if addr == 0x4015
          ret |= (!@irq ? 128 : 0) |
                 (@frameIrq ? 0x40 : 0) |
                 (@active ? 16 : 0) |
                 (@lengthCounter.get!(1) != 0 ? 8 : 0) |
                 (@lengthCounter.get!(0) != 0 ? 4 : 0)
          @frameIrq = false
          {ret, true}
        elsif 0x4008 <= addr && addr <= 0x4014
          ret |= @reg.get!(addr - 0x4008) # Safe due to the check
          {ret, true}
        else
          {ret, false}
        end
      end
    end
  end
end
