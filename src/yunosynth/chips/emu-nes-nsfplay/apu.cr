#### NES 2A03
####
#### Ported from NSFPlay 2.2 to VGMPlay (including C++ -> C conversion)
#### by Valley Bell on 24 September 2013
#### Updated to NSFPlay 2.3 on 26 September 2013
####
#### Ported to Crystal by Remilia Scarlet
####
require "./counter"

module Yuno::Chips
  class Nes < Yuno::AbstractChip
    # Actual implementation of the NES's APU emulator based on NFSPlay's
    # implementation
    private class NesApuNsfPlay < Yuno::AbstractEmulator
      # Master Clock: 21477272 (NTSC)
      # APU Clock = Master Clock / 12
      DEFAULT_CLOCK = 1789772.0 # Should this be 1789772.6666... ?
      DEFAULT_RATE = 44100.0

      ##
      ## Upper half of APU
      ##
      OPT_UNMUTE_ON_RESET = 1u8
      OPT_NONLINEAR_MIXER = 2u8
      OPT_PHASE_REFRESH   = 4u8
      OPT_DUTY_SWAP       = 8u8
      OPTID_UNMUTE_ON_RESET = 0
      OPTID_NONLINEAR_MIXER = 1
      OPTID_PHASE_REFRESH   = 2
      OPTID_DUTY_SWAP       = 3
      OPTID_END             = 4

      SQR0_MASK = 1
      SQR1_MASK = 2

      SQR_TBL = [
        [0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0] of Int16,
        [0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0] of Int16,
        [0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0] of Int16,
        [1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1] of Int16
      ] of Array(Int16)

      LENGTH_TABLE = [
        0x0A, 0xFE,
        0x14, 0x02,
        0x28, 0x04,
        0x50, 0x06,
        0xA0, 0x08,
        0x3C, 0x0A,
        0x0E, 0x0C,
        0x1A, 0x0E,
        0x0C, 0x10,
        0x18, 0x12,
        0x30, 0x14,
        0x60, 0x16,
        0xC0, 0x18,
        0x48, 0x1A,
        0x10, 0x1C,
        0x20, 0x1E
      ] of UInt8

      @option : UInt8 = 0u8 # Remi: changed into a bitfield to reduce array lookups
      @mask : Int32 = 0
      @smL : Array(Int32) = [128, 128]
      @smR : Array(Int32) = [128, 128]

      @gclock : UInt32 = 0
      @reg : Array(UInt8) = Array(UInt8).new(0x20, 0u8)
      @outL : Int32 = 0
      @outR : Int32 = 0
      @rate : Float64 = 0.0
      @clock : Float64 = 0.0

      @squareTable : Array(Int32) = Array(Int32).new(32, 0) # Nonlinear mixer

      @scounter : Array(Int32) = [0, 0] # Frequency divider
      @sphase : Array(Int32) = [0, 0] # Phase counter

      @duty : Array(Int32) = [0, 0]
      @volume : Array(Int32) = [0, 0]
      @freq : Array(Int32) = [0, 0]
      @sfreq : Array(Int32) = [0, 0]

      @sweepEnable : Array(Bool) = [false, false]
      @sweepMode : Array(Bool) = [false, false]
      @sweepWrite : Array(Bool) = [false, false]
      @sweepDivPeriod : Array(Int32) = [0, 0]
      @sweepDiv : Array(Int32) = [0, 0]
      @sweepAmount : Array(Int32) = [0, 0]

      @envelopeDisable : Array(Bool) = [false, false]
      @envelopeLoop : Array(Bool) = [false, false]
      @envelopeWrite : Array(Bool) = [false, false]
      @envelopeDivPeriod : Array(Int32) = [0, 0]
      @envelopeDiv : Array(Int32) = [0, 0]
      @envelopeCounter : Array(Int32) = [0, 0]

      @lengthCounter : Array(Int32) = [0, 0]

      @enable : Array(Bool) = [false, false]

      @tickCount : Counter = Counter.new
      @tickLast : UInt32 = 0u32

      @mem : Array(Int32) = [0, 0]

      def initialize(clock : Int, rate : Int)
        self.clock = clock.to_f64!
        self.rate = rate.to_f64!
        @option |= OPT_UNMUTE_ON_RESET
        @option |= OPT_PHASE_REFRESH
        @option |= OPT_NONLINEAR_MIXER
        @option &= ~OPT_DUTY_SWAP

        @squareTable[0] = 0
        (1...32).each do |i|
          @squareTable[i] = ((8192.0 * 95.88) / (8128.0 / i + 100)).to_i32!
        end
      end

      def setOption(id : Int, val : Int) : Nil
        case id
        when OPTID_DUTY_SWAP then @option |= OPT_DUTY_SWAP
        when OPTID_NONLINEAR_MIXER then @option |= OPT_NONLINEAR_MIXER
        when OPTID_PHASE_REFRESH then @option |= OPT_PHASE_REFRESH
        when OPTID_UNMUTE_ON_RESET then @option |= OPT_UNMUTE_ON_RESET
        end
      end

      def clock=(@clock : Float64) : Nil
      end

      def rate=(value : Float64) : Nil
        @rate = value != 0 ? value : DEFAULT_RATE
        @tickCount.initValues(@clock, @rate)
      end

      def mask=(value : Int) : Nil
        @mask = value.to_i32!
      end

      def setStereoMix(trk : Int, mixL : Int16, mixR : Int16) : Nil
        return if trk < 0 || trk > 1
        @smL[trk] = mixL.to_i32!
        @smR[trk] = mixR.to_i32!
      end

      def frameSequence(s : Int) : Nil
        return if s > 3 # No operation in step 4

        divider : Bool = false

        # 240 hz clock
        2.times do |i|
          divider = false
          if @envelopeWrite.get!(i)
            @envelopeWrite.put!(i, false)
            @envelopeCounter.put!(i, 15)
            @envelopeDiv.put!(i, 0)
          else
            @envelopeDiv.incf!(i)
            if @envelopeDiv.get!(i) > @envelopeDivPeriod.get!(i)
              divider = true
              @envelopeDiv.put!(i, 0)
            end
          end

          if divider
            if @envelopeLoop.get!(i) && @envelopeCounter.get!(i) == 0
              @envelopeCounter.put!(i, 15)
            elsif @envelopeCounter.get!(i) > 0
              @envelopeCounter.decf!(i)
            end
          end
        end

        # 120Hz clock
        unless Yuno.bitflag?(s, 1)
          2.times do |i|
            if !@envelopeLoop.get!(i) && @lengthCounter.get!(i) > 0
              @lengthCounter.decf!(i)
            end

            if @sweepEnable.get!(i)
              @sweepDiv.decf!(i)
              if @sweepDiv.get!(i) <= 0
                sweepSqr(i) # Calculate new sweep target

                if @freq.get!(i) >= 8 && @sfreq.get!(i) < 0x800 && @sweepAmount.get!(i) > 0
                  @freq.put!(i, @sfreq.get!(i) < 0 ? 0 : @sfreq.get!(i))
                  if @scounter.get!(i) > @freq.get!(i)
                    @scounter.put!(i, @freq.get!(i))
                  end
                end

                @sweepDiv.put!(i, @sweepDivPeriod.get!(i) + 1)
              end

              if @sweepWrite.get!(i)
                @sweepDiv.put!(i, @sweepDivPeriod.get!(i) + 1)
                @sweepWrite.put!(i, false)
              end
            end
          end
        end
      end

      def update(outputs : OutputBuffers, samples : UInt32) : Nil
        raise "Unsupported"
      end

      # 生成される波形の振幅は0-8191
      def render(outputs : Pointer(Int32)) : UInt32
        @tickCount.iup
        tick((@tickCount.value &- @tickLast) & 0xFF)
        @tickLast = @tickCount.value

        @outL = Yuno.bitflag?(@mask, 1) ? 0 : @outL
        @outR = Yuno.bitflag?(@mask, 2) ? 0 : @outR

        {% begin %}
          if Yuno.bitflag?(@option, OPT_NONLINEAR_MIXER)
            {% if flag?(:yunosynth_wd40) %}
              voltage : Int32 = @squareTable.get!(@outL &+ @outR)
            {% else %}
              voltage : Int32 = @squareTable[@outL &+ @outR]
            {% end %}
            @mem.put!(0, @outL << 6)
            @mem.put!(1, @outR << 6)

            ref : Int32 = @mem.get!(0) + @mem.get!(1)
            if ref > 0
              @mem.put!(0, (@mem.get!(0) * voltage).tdiv(ref))
              @mem.put!(1, (@mem.get!(1) * voltage).tdiv(ref))
            else
              @mem.put!(0, voltage)
              @mem.put!(1, voltage)
            end
          else
            @mem.put!(0, @outL << 6)
            @mem.put!(1, @outR << 6)
          end
        {% end %}

        # Shifting is (x-2) to match the volume of MAME's NES APU sound core
        outputs[0] = @mem.get!(0) * @smL.get!(0)
        outputs[0] += @mem.get!(1) * @smL.get!(1)
        outputs[0] = outputs[0] >> (7 - 2) # Was 7, but is now 8 for bipolar square.
        # Remi: TIL 5 == 8 is true.

        outputs[1] = @mem.get!(0) * @smR.get!(0)
        outputs[1] += @mem.get!(1) * @smR.get!(1)
        outputs[1] = outputs[1] >> (7 - 2) # See above

        2u32
      end

      def reset : Nil
        @gclock = 0
        @mask = 0

        @scounter.fill(0)
        @sphase.fill(0)
        @sweepDiv.fill(1)
        @envelopeDiv.fill(0)
        @lengthCounter.fill(0)
        @envelopeCounter.fill(0)

        (0x4000_u32...0x4008_u32).each { |i| write(i, 0u32) }
        write(0x4015, 0)
        write(0x4015, 0x0F) if Yuno.bitflag?(@option, OPT_UNMUTE_ON_RESET)

        @outL = 0
        @outR = 0

        self.rate = @rate # Remi: not a typo
      end

      def write(addr : UInt32, val : UInt32) : Bool
        if 0x4000 <= addr && addr < 0x4008
          addr &= 0xF # addr is now 0..15

          # Remi: Changed to UInt8 to save three bytes.
          ch : UInt8 = (addr >> 2).to_u8! # ch is now 0..3
          case addr
          when 0x0, 0x4
            @volume[ch] = (val & 15).to_i32! # After this, we know ch is 0..1
            @envelopeDisable.put!(ch, ((val >> 4) & 1) != 0)
            @envelopeLoop.put!(ch, ((val >> 5) & 1) != 0)
            @envelopeDivPeriod.put!(ch, (val & 15).to_i32!)
            @duty.put!(ch, ((val >> 6) & 3).to_i32!)
            if Yuno.bitflag?(@option, OPT_DUTY_SWAP)
              @duty[ch] = case @duty.get!(ch)
                          when 1 then 2
                          when 2 then 1
                          else @duty.get!(ch)
                          end
            end

          when 0x1, 0x5
            @sweepEnable[ch] = ((val >> 7) & 1) != 0 # After this, we know ch is 0..1
            @sweepDivPeriod.put!(ch, ((val >> 4) & 7).to_i32!)
            @sweepMode.put!(ch, ((val >> 3) & 1) != 0)
            @sweepAmount.put!(ch, (val & 7).to_i32!)
            @sweepWrite.put!(ch, true)
            sweepSqr(ch)

          when 0x2, 0x6
            @freq.put!(ch, val.to_i32! | (@freq[ch] & 0x700)) # After this, we know ch is 0..1
            sweepSqr(ch)
            @scounter.put!(ch, @freq.get!(ch)) if @scounter.get!(ch) > @freq.get!(ch)

          when 0x3, 0x7
            @freq.put!(ch, (@freq[ch] & 0xFF) | ((val & 0x7) << 8)) # After this, we know ch is 0..1
            @sphase.put!(ch, 0) if Yuno.bitflag?(@option, OPT_PHASE_REFRESH)
            @envelopeWrite.put!(ch, true)
            if @enable.get!(ch)
              # LENGTH_TABLE.get! is safe here because of the `& 0x1F`
              @lengthCounter.put!(ch, (LENGTH_TABLE.get!((val >> 3) & 0x1F)).to_i32!)
            end
            sweepSqr(ch)
            @scounter.put!(ch, @freq.get!(ch)) if @scounter.get!(ch) > @freq.get!(ch)

          else return false
          end

          @reg.put!(addr, val.to_u8!)
          true
        elsif addr == 0x4015
          @enable.put!(0, (val & 1) != 0)
          @enable.put!(1, (val & 2) != 0)
          @lengthCounter.put!(0, 0) unless @enable.get!(0)
          @lengthCounter.put!(1, 0) unless @enable.get!(1)
          @reg.put!(21, val.to_u8!) # 21 = addr - 0x4000, addr is known to be 0x4015
          true
        end # 4017 is handled in dmc.cr

        false
      end

      def read(addr : UInt32, val : UInt32) : Tuple(UInt32, Bool)
        if 0x4000 <= addr && addr < 0x4008
          return {val | @reg.get!(addr & 0x7), true}
        elsif addr == 0x4015
          x = @lengthCounter.get!(1) != 0 ? 2 : 0
          x |= @lengthCounter.get!(0) != 0 ? 1 : 0
          return { val | x, true }
        else
          return {0u32, false}
        end
      end

      @[AlwaysInline]
      private def sweepSqr(i : Int) : Nil
        {% begin %}
          {%if flag?(:yunosynth_wd40) %}
            shifted : Int32 = @freq.get!(i) >> @sweepAmount.get!(i)
          {% else %}
            # We know i is within bounds after this first @freq[i].
            shifted : Int32 = @freq[i] >> @sweepAmount.get!(i)
          {% end %}

          if i == 0 && @sweepMode.get!(i)
            shifted += 1
          end
          @sfreq.put!(i, @freq.get!(i) + (@sweepMode.get!(i) ? -shifted : shifted))
        {% end %}
      end

      @[AlwaysInline]
      private def calcSqr(i : Int, clocks : UInt32) : Int32
        ret : Int32 = 0

        {% if flag?(:yunosynth_wd40) %}
          @scounter.incf!(i, clocks)
        {% else %}
          @scounter[i] += clocks
        {% end %}
        while @scounter.get!(i) > @freq.get!(i)
          @sphase.put!(i, (@sphase.get!(i) + 1) & 15)
          @scounter.decf!(i, @freq.get!(i) + 1)
        end

        if @lengthCounter.get!(i) > 0 && @freq.get!(i) >= 8 && @sfreq.get!(i) < 0x800
          v = if @envelopeDisable.get!(i)
                @volume.get!(i)
              else
                @envelopeCounter.get!(i)
              end

          # The first .get! is safe because #write does an `& 3` when setting
          # @duty.  The second is because @sphase is set using an `& 15` above.
          ret = SQR_TBL.get!(@duty.get!(i)).get!(@sphase.get!(i)) != 0 ? v : 0
        end

        ret
      end

      @[AlwaysInline]
      private def tick(clocks : UInt32) : Nil
        @outL = calcSqr(0, clocks)
        @outR = calcSqr(1, clocks)
      end
    end
  end
end
