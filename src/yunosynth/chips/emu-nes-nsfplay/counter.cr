#### Ported from NSFPlay 2.2 to VGMPlay (including C++ -> C conversion)
#### by Valley Bell on 24 September 2013
#### Updated to NSFPlay 2.3 on 26 September 2013
####
#### Ported to Crystal by Remilia Scarlet
####

module Yuno::Chips
  class Nes < Yuno::AbstractChip
    # Common counter class used by other NES sound chips
    class Counter
      private SHIFT = 24

      property ratio : Float64 = 0.0
      property val : UInt32 = 0u32
      property step : UInt32 = 0u32

      def initialize
      end

      @[AlwaysInline]
      def initValues(clock, rate) : Nil
        @ratio = (1 << SHIFT) * (1.0 * clock / rate)
        @step = (@ratio + 0.5).to_u32!
        @val = 0u32
      end

      @[AlwaysInline]
      def cycle=(val)
        @step = (@ratio / (s + 1)).to_u32!
      end

      @[AlwaysInline]
      def iup
        @val = @val &+ @step
      end

      @[AlwaysInline]
      def value : UInt32
        @val >> SHIFT
      end
    end
  end
end
