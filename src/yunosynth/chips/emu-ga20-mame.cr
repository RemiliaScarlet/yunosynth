#### Irem GA20 PCM Sound Chip
#### Copyright (C) 2002-2018 Acho A. Tang,R. Belmont, Valley Bell
#### Copyright (C) 2023-2024 Remilia Scarlet
#### BSD 3-Clause License
####
#### It's not currently known whether this chip is stereo.
####
####
#### Revisions:
####
#### 04-15-2002 Acho A. Tang
#### - rewrote channel mixing
#### - added prelimenary volume and sample rate emulation
####
#### 05-30-2002 Acho A. Tang
#### - applied hyperbolic gain control to volume and used
####   a musical-note style progression in sample rate
####   calculation(still very inaccurate)
####
#### 02-18-2004 R. Belmont
#### - sample rate calculation reverse-engineered.
####   Thanks to Fujix, Yasuhiro Ogawa, the Guru, and Tormod
####   for real PCB samples that made this possible.
####
#### 02-03-2007 R. Belmont
#### - Cleaned up faux x86 assembly.
####
#### 09-25-2018 Valley Bell & co
#### - rewrote channel update to make data 0 act as sample terminator
####
#### 16 March 2023 Remilia Scarlet
#### - Ported to Crystal

module Yuno::Chips
  class GA20 < Yuno::AbstractChip
    private class GA20Mame < Yuno::AbstractEmulator
      MAX_VOL = 256_u32

      class Channel
        property rate   : UInt32 = 0
        property start  : UInt32 = 0
        property pos    : UInt32 = 0
        property frac   : UInt32 = 0
        property finish : UInt32 = 0
        property volume : UInt32 = 0
        property pan    : UInt32 = 0
        property? play  : Bool = false
        property muted  : UInt8 = 0

        def initialize
        end
      end

      @rom : Bytes = Bytes.new(0)
      @regs : Array(UInt16) = Array(UInt16).new(0x40, 0i16)
      @channels : Array(Channel)
      protected getter rate : UInt32

      def initialize(clock : UInt32)
        @channels = Array(Channel).new(4) { |_| Channel.new }
        @rate = clock.tdiv(4)
      end

      @[AlwaysInline]
      def write(offset : Int32, data : UInt8) : Nil
        @regs[offset] = data.to_u16!

        chan = @channels[offset >> 3]
        case offset & 0x07
        when 0 # Start address low
          chan.start = (chan.start & 0xFF000) | (data.to_u32! << 4)

        when 1 # Start address high
          chan.start = (chan.start & 0x00FF0) | (data.to_u32! << 12)

        when 2 # End address low
          chan.finish = (chan.finish & 0xFF000) | (data.to_u32! << 4)

        when 3 # End address high
          chan.finish = (chan.finish & 0x00FF0) | (data.to_u32! << 12)

        when 4
          chan.rate = 0x1000000_u32.tdiv(256_u32 - data.to_u32!)

        when 5 # AT: Gain control
          chan.volume = (data.to_u32! * MAX_VOL).tdiv(data.to_u32! + 10)

        when 6 # AT: this is always written 2 (enabling both channels?)
          chan.play = (data == 0 ? false : true)
          chan.pos = chan.start
          chan.frac = 0
        end
      end

      @[AlwaysInline]
      def read(offset : Int32) : UInt8
        channel = offset >> 3
        case offset & 0x07
        when 7 # Voice status.  Bit 0 is 1 if active (routine around 0xCCC in R-Type Leo)
          @channels[channel].play? ? 1u8 : 0u8
        else
          Yuno.warn("GA20: Read from unkown register #{offset & 0xF}, channel #{channel}")
          0u8
        end
      end

      def reset : Nil
        @channels.each do |chan|
          chan.rate = 0
          chan.start = 0
          chan.pos = 0
          chan.frac = 0
          chan.finish = 0
          chan.volume = 0
          chan.pan = 0
          chan.play = false
        end

        @regs.fill(0u8)
      end

      def update(outputs : OutputBuffers, samples : UInt32) : Nil
        outL : Slice(Int32) = outputs[0]
        outR : Slice(Int32) = outputs[1]

        rate : StaticArray(UInt32, 4) = StaticArray(UInt32, 4).new(0u32)
        pos : StaticArray(UInt32, 4) = StaticArray(UInt32, 4).new(0u32)
        frac : StaticArray(UInt32, 4) = StaticArray(UInt32, 4).new(0u32)
        finish : StaticArray(UInt32, 4) = StaticArray(UInt32, 4).new(0u32)
        vol : StaticArray(UInt32, 4) = StaticArray(UInt32, 4).new(0u32)
        play? : StaticArray(Bool, 4) = StaticArray(Bool, 4).new(false)
        sampleOut : Int32 = 0

        # Check once, then we can avoid bounds checks below.
        if outL.size < samples || outR.size < samples
          raise "GA20 update: Bad number of samples (expected #{samples}, " \
                "but buffers have #{outL.size} and #{outR.size})"
        end

        # Pre-cache some values
        4.times do |i|
          rate[i] = @channels[i].rate
          pos[i] = @channels[i].pos
          frac[i] = @channels[i].frac
          finish[i] = @channels[i].finish &- 0x20
          vol[i] = @channels[i].volume
          play?[i] = (@channels[i].muted == 0 ? @channels[i].play? : false)
        end

        samples.times do |i|
          sampleOut = 0

          # Update the four channels inline
          {% begin %}
            {% for i in 0..3 %}
              if play?[{{i}}]
                sampleOut += (@rom[pos[{{i}}]].to_i32! - 0x80) * vol[{{i}}].to_i32!
                frac[{{i}}] += rate[{{i}}]
                pos[{{i}}] += (frac[{{i}}] >> 24)
                frac[{{i}}] &= 0xFFFFFF
                play?[{{i}}] = pos[{{i}}] < finish[{{i}}]
              end
            {% end %}
          {% end %}

          sampleOut >>= 2
          outL.unsafe_put(i, sampleOut)
          outR.unsafe_put(i, sampleOut)
        end

        # Update the registers
        4.times do |i|
          @channels[i].pos = pos[i]
          @channels[i].frac = frac[i]
          @channels[i].play = play?[i] if @channels[i].muted == 0
        end
      end

      def writeRom(romSize : Int32, dataStart : Int32, dataLength : Int32, romData : Slice(UInt8)) : Nil
        if @rom.size != romSize
          @rom = Bytes.new(romSize, 0xFF_u8)
        end

        return if dataStart > romSize
        dataLength = romSize - dataStart if dataStart + dataLength > romSize

        # Copy the memory
        dataLength.times do |i|
          # This can be guaranteed by the checks above
          @rom.unsafe_put(i + dataStart, romData.unsafe_fetch(i))
        end
      end

      def muteMask=(mask : UInt32) : Nil
        @channels.each_with_index do |chan, idx|
          chan.muted = ((mask >> idx) & 0x01).to_u8!
        end
      end
    end
  end
end
