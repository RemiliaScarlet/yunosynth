#### OPL2/OPL3 emulation library
#### Copyright (C) 2002-2010  The DOSBox Team
#### Crystal port Copyright (C) 2023-2024 Remilia Scarlet
####
#### This library is free software; you can redistribute it and/or modify it
#### under the terms of the GNU Lesser General Public License as published by
#### the Free Software Foundation; either version 2.1 of the License, or (at
#### your option) any later version.
####
#### This library is distributed in the hope that it will be useful, but WITHOUT
#### ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
#### License for more details.
####
#### You should have received a copy of the GNU Lesser General Public License
#### along with this library; if not, write to the Free Software Foundation,
#### Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
require "./opl-dosbox-common"

####
#### OPL3 Emulation (Dosbox)
####

module Yuno::Chips
  # :nodoc:
  class OPL3Dosbox < Yuno::AbstractEmulator
    include OPLDosbox
    NUM_CHANNELS = 18u32
    MAX_OPERATORS = NUM_CHANNELS * 2

    # Map a register base to a modulator operator number
    REGBASE_TO_MODOP = [
      # First set
      0, 1, 2, 0, 1, 2, 0, 0, 3, 4, 5, 3, 4, 5, 0, 0, 6, 7, 8, 6, 7, 8,

      # Second set
      18, 19, 20, 18, 19, 20, 0, 0, 21, 22, 23, 21, 22, 23, 0, 0, 24, 25, 26, 24, 25, 26
    ] of UInt8

    # Map a register base to an operator number
    REGBASE_TO_OP = [
      # First set
      0, 1, 2, 9, 10, 11, 0, 0, 3, 4, 5, 12, 13, 14, 0, 0, 6, 7, 8, 15, 16, 17,

      # Second set
      18, 19, 20, 27, 28, 29, 0, 0, 21, 22, 23, 30, 31, 32, 0, 0, 24, 25, 26, 33, 34, 35
    ] of UInt8

    class Operator < OPLDosbox::Operator # Reopen and extend for OPL3
      property? is4Op : Bool = false # Base of a 4-OP channel
      property? is4OpAttached : Bool = false # Part of a 4-OP channel
      property leftPan : Int32 = 1
      property rightPan : Int32 = 1
    end

    @op : Array(Operator)
    @adlibReg : Array(UInt8) = Array(UInt8).new(512, 0u8) # Adlib register set (including second set)
    @waveSel : Array(UInt8) = Array(UInt8).new(44, 0u8) # Waveform selection

    def initialize(clock : UInt32, sampleRate : UInt32, updateHandler : AdlUpdateHandler?, param : AbstractChip?)
      @opFuncs = [
        ->operatorAttack(OPLDosbox::Operator),
        ->operatorDecay(OPLDosbox::Operator),
        ->operatorRelease(OPLDosbox::Operator),
        ->operatorSustain(OPLDosbox::Operator), # Sustain phase (keeping level)
        ->operatorRelease(OPLDosbox::Operator), # Sustain_nokeep phase (release-style)
        ->operatorOff(OPLDosbox::Operator)
      ]

      @chipClock = clock
      @intSampleRate = sampleRate.to_i32!
      @updateHandler = updateHandler
      @updateParam = param

      @intFrequ = @chipClock / 288.0 # Clocking of the chip.
      @generatorAdd = (@intFrequ * FIXEDPT / @intSampleRate).to_u32!
      @invSamp = 1.0 / @intSampleRate

      @op = Array(Operator).new(MAX_OPERATORS) { |_| Operator.new }
      @muteChan = Array(UInt8).new(NUM_CHANNELS + 5, 0u8)

      15.downto(0) do |i|
        @frqMul[i] = FRQMUL_TAB[i] * @intFrequ  / WAVEPREC * FIXEDPT * @invSamp
      end

      @vibTable[0] = 8
      @vibTable[1] = 4
      @vibTable[2] = 0
      @vibTable[3] = -4
      4.upto(VIBTAB_SIZE - 1) { |i| @vibTable[i] = @vibTable[i - 4] * -1 }

      # Vibrato at ~6.1 ?? (opl3 docs say 6.1, opl4 docs say 6.0, y8950 docs say 6.4)
      @vibTabAdd = (VIBTAB_SIZE.to_f64! * FIXEDPT_LFO / 8192 * @intFrequ / @intSampleRate).to_u32!

      tremTableInt = Array(Int32).new(TREMTAB_SIZE, 0i32)

      # Create tremolo table
      14.times { |i| tremTableInt[i] = i - 13 } # upwards (13 to 26 -> -0.5/6 to 0)
      14.upto(40) { |i| tremTableInt[i] = (-i) + 14 } # downwards (26 to 0 -> 0 to -1/6)
      41.upto(52) { |i| tremTableInt[i] = i - 40 - 26 } # upwards (1 to 12 -> -1/6 to -0.5/6)

      TREMTAB_SIZE.times do |i|
        tremVal1 = tremTableInt[i] * 4.8 / 26.0 / 6.0 # 4.8dB
        tremVal2 = tremTableInt[i].tdiv(4) * 1.2 / 6.0 / 6.0 # 1.2dB (larger stepping)
        @tremTable[i] = ((FL2 ** tremVal1) * FIXEDPT).to_i32!
        @tremTable[TREMTAB_SIZE + i] = ((FL2 ** tremVal2) * FIXEDPT).to_i32!
      end

      # Tremolo at 3.7 Hz
      @tremTabAdd = (TREMTAB_SIZE * TREM_FREQ * FIXEDPT_LFO / @intSampleRate).to_u32

      BLOCKBUF_SIZE.times { |i| @tremValConst[i] = FIXEDPT }

      # Create waveform tables
      (WAVEPREC >> 1).times do |i|
        @wavTable[(i << 1) +     WAVEPREC] = (16384 * Math.sin((i << 1)       * Math::PI * 2 / WAVEPREC)).to_i16!
        @wavTable[(i << 1) + 1 + WAVEPREC] = (16384 * Math.sin(((i << 1) + 1) * Math::PI * 2 / WAVEPREC)).to_i16!
        @wavTable[i] = @wavTable[(i << 1) + WAVEPREC]
      end

      (WAVEPREC >> 3).times do |i|
        @wavTable[i + (WAVEPREC << 1)]        = @wavTable[i + (WAVEPREC >> 3)] &- 16384
        @wavTable[i + ((WAVEPREC * 17) >> 3)] = @wavTable[i + (WAVEPREC >> 2)] &+ 16384
      end

      # Key scale level table verified ([table in book] * 8 / 3)
      @ksLev[7][0] = 0
      @ksLev[7][1] = 24
      @ksLev[7][2] = 32
      @ksLev[7][3] = 37
      @ksLev[7][4] = 40
      @ksLev[7][5] = 43
      @ksLev[7][6] = 45
      @ksLev[7][7] = 47
      @ksLev[7][8] = 48

      9.upto(15) { |i| @ksLev[7][i] = (i + 41).to_u8! }
      6.downto(0) do |j|
        16.times do |i|
          oct = @ksLev[j + 1][i].to_i32! - 8
          oct = 0 if oct < 0
          @ksLev[j][i] = oct.to_u8!
        end
      end
    end

    protected def changeAttackRate(regBase : UInt32, op : Operator) : Nil
      attackRate : Int32 = (@adlibReg[ARC_ATTR_DECR + regBase] >> 4).to_i32!
      if attackRate != 0
        stepSkipMask = StaticArray[0xFF_u8, 0xFE_u8, 0xEE_u8, 0xBA_u8, 0xAA_u8]
        f = (FL2 ** (attackRate.to_f64! + (op.toff >> 2) - 1)) * ATTACK_CONST[op.toff & 3] * @invSamp

        # Attack rate coefficients
        op.a0 = 0.0377 * f
        op.a1 = 10.73 * f + 1
        op.a2 = -17.57 * f
        op.a3 = 7.42 * f

        stepSkip : UInt32 = (attackRate * 4 + op.toff).to_u32!
        steps : UInt32 = stepSkip >> 2
        op.envStepA = (1 << (steps <= 12 ? 12 - steps : 0)) - 1

        stepNum : UInt32 = if stepSkip <= 48
                             4u32 - (stepSkip & 3)
                           else
                             0u32
                           end
        op.envStepSkipA = stepSkipMask.unsafe_fetch(stepNum).to_i32!

        if stepSkip >= 60 # OPL3-specific line
          op.a0 = 2.0 # Something that triggers an immediate transition to amp = 1.0
          op.a1 = 0.0
          op.a2 = 0.0
          op.a3 = 0.0
        end
      else
        # Attack disabled
        op.a0 = 0.0
        op.a1 = 1.0
        op.a2 = 0.0
        op.a3 = 0.0
        op.envStepA = 0
        op.envStepSkipA = 0
      end
    end

    protected def changeWaveform(regBase : UInt32, op : Operator) : Nil
      if regBase >= ARC_SECONDSET
        super(regBase - (ARC_SECONDSET - 22), op) # Second set starts at 22
      else
        super(regBase, op)
      end
    end

    def reset : Nil
      @adlibReg.fill(0)
      @op = Array(Operator).new(MAX_OPERATORS) do |_|
        Operator.new(Slice(Int16).new(@wavTable.to_unsafe + WAVEFORM_START[0],
                                      @wavTable.size - WAVEFORM_START[0]))
      end

      @status = 0
      @oplIndex = 0
      @oplAddr = 0
    end

    @[AlwaysInline]
    def writeIO(addr : UInt32, val : UInt8) : Nil
      if Yuno.bitflag?(addr, 1)
        adlibWrite(@oplAddr.to_u32!, val)
      else
        @oplAddr = val.to_i32! | ((addr & 2) << 7)
      end
    end

    def adlibWrite(idx : UInt32, val : UInt8) : Nil
      {% begin %}
        secondSet : UInt32 = idx & 0x100
        @adlibReg[idx] = val

        case idx & 0xF0
        when ARC_CONTROL
          # Here we check for the second set registers, too.
          case idx
          when 0x02, 0x03 # timer1 and timer2 counter
            return

          when 0x04 # IRQ reset, timer mask/start
            if Yuno.bitflag?(val, 0x80)
              # Clear IRQ bits in status register
              @status &= ~0x60
            else
              @status = 0
            end

          when 0x04 | ARC_SECONDSET
            # 4op enable/disable switches for each possible channel.
            {% for pair in [[0, 3, 1], [1, 4, 2], [2, 5, 4], [18, 21, 8], [19, 22, 16], [20, 23, 32]] %}
              @op.get!({{pair[0]}}).is4Op = (val & {{pair[2]}}) > 0
              @op.get!({{pair[1]}}).is4OpAttached = @op.get!({{pair[0]}}).is4Op?
            {% end %}

          when 0x05 | ARC_SECONDSET
            return

          when 0x08
            # CSW, note select
            return
          end

        when ARC_TVS_KSR_MUL, ARC_TVS_KSR_MUL + 0x10
          # Tremolo/vibrato/sustain keeping enabled; key scale rate; frequency
          # multiplication
          num : Int32 = (idx & 7).to_i32!
          base : UInt32 = (idx - ARC_TVS_KSR_MUL) & 0xFF
          if num < 6 && base < 22
            modop : UInt32 = REGBASE_TO_MODOP[secondSet != 0 ? base + 22 : base].to_u32!
            regBase : UInt32 = base + secondSet
            chanBase : UInt32 = if secondSet != 0
                                  modop - 18 + ARC_SECONDSET
                                else
                                  modop
                                end

            # Change tremolo/vibrato and sustain keeping of this operator.
            op : Operator = @op[modop + (num < 3 ? 0 : 9)]
            changeKeepSustain(regBase, op)
            changeVibrato(regBase, op)

            # Change frequency calculations of this operator as key scale rate
            # and frequency multiplier can be changed.
            if Yuno.bitflag?(@adlibReg.get!(0x105), 1) && @op.get!(modop).is4OpAttached?
              # Operator uses frequency of channel
              changeFrequency(chanBase - 3, regBase, op)
            else
              changeFrequency(chanBase, regBase, op)
            end
          end

        when ARC_KSL_OUTLEV, ARC_KSL_OUTLEV + 0x10
          # Key scale level; output rate
          num = (idx & 7).to_i32!
          base = (idx - ARC_KSL_OUTLEV) & 0xFF
          if num < 6 && base < 22
            modop = REGBASE_TO_MODOP[secondSet != 0 ? base + 22 : base].to_u32!
            chanBase = if secondSet != 0
                         modop - 18 + ARC_SECONDSET
                       else
                         modop
                       end

            # Change frequency calculations of this operator as key scale level
            # and output rate can be changed.
            op = @op[modop + (num < 3 ? 0 : 9)]
            regBase = base + secondSet
            if Yuno.bitflag?(@adlibReg.get!(0x105), 1) && @op.get!(modop).is4OpAttached?
              # Operator uses frequency of the channel.
              changeFrequency(chanBase - 3, regBase, op)
            else
              changeFrequency(chanBase, regBase, op)
            end
          end

        when ARC_ATTR_DECR, ARC_ATTR_DECR + 0x10
          # Attack/decay rates
          num = (idx & 7).to_i32!
          base = (idx - ARC_ATTR_DECR) & 0xFF
          if num < 6 && base < 22
            regBase = base + secondSet

            # Change attack rate and decay rate of this operator.
            op = @op[REGBASE_TO_OP[secondSet != 0 ? base + 22 : base]]
            changeAttackRate(regBase, op)
            changeDecayRate(regBase, op)
          end

        when ARC_SUSL_RELR, ARC_SUSL_RELR + 0x10
          # Sustain level, release rate
          num = (idx & 7).to_i32!
          base = (idx - ARC_SUSL_RELR) & 0xFF

          if num < 6 && base < 22
            regBase = base + secondSet

            # Change sustain level and release rate of this operator.
            op = @op[REGBASE_TO_OP[secondSet != 0 ? base + 22 : base]]
            changeReleaseRate(regBase, op)
            changeSustainLevel(regBase, op)
          end

        when ARC_FREQ_NUM
          # 0xA0 - 0xA8 low8 frequency
          base = (idx - ARC_FREQ_NUM) & 0xFF
          if base < 9
            opBase : Int32 = if secondSet != 0
                               (base + 18).to_i32!
                             else
                               base.to_i32!
                             end

            if Yuno.bitflag?(@adlibReg.get!(0x105), 1) && @op[opBase].is4OpAttached?
              return
            end

            modBase : Int32 = MODULATOR_BASE[base].to_i32! + secondSet
            chanBase = base + secondSet

            changeFrequency(chanBase, modBase.to_u32!, @op[opBase])
            changeFrequency(chanBase, (modBase + 3).to_u32!, @op[opBase + 9])

            # For 4op channels all four operators are modified to the
            # frequency of the channel.
            if Yuno.bitflag?(@adlibReg.get!(0x105), 1) && @op[secondSet != 0 ? base + 18 : base].is4Op?
              changeFrequency(chanBase, (modBase + 8).to_u32!, @op[opBase + 3])
              changeFrequency(chanBase, (modBase + (3 + 8)).to_u32!, @op[opBase + (3 + 9)])
            end
          end

        when ARC_KON_BNUM
          # Hack for DOSBox logs
          @updateHandler.try { |fn| fn.call(@updateParam.not_nil!) }

          if idx == ARC_PERC_MODE
            return if secondSet != 0

            if (val & 0x30) == 0x30
              # Base drum active
              enableOperator(16, @op.get!(6), OP_ACT_PERC)
              changeFrequency(6, 16, @op.get!(6))
              enableOperator(16u32 + 3, @op.get!(6 + 9), OP_ACT_PERC)
              changeFrequency(6u32, 16u32 + 3, @op.get!(6 + 9))
            else
              disableOperator(@op.get!(6), OP_ACT_PERC)
              disableOperator(@op.get!(6 + 9), OP_ACT_PERC)
            end

            if (val & 0x28) == 0x28
              # Snare active
              enableOperator(17u32 + 3, @op.get!(16), OP_ACT_PERC)
              changeFrequency(7, 17u32 + 3, @op.get!(16))
            else
              disableOperator(@op.get!(16), OP_ACT_PERC)
            end

            if (val & 0x24) == 0x24
              # Tom tom active
              enableOperator(18, @op.get!(8), OP_ACT_PERC)
              changeFrequency(8, 18, @op.get!(8))
            else
              disableOperator(@op.get!(8), OP_ACT_PERC)
            end

            if (val & 0x22) == 0x22
              # Cymbal active
              enableOperator(18u32 + 3, @op.get!(8 + 9), OP_ACT_PERC)
              changeFrequency(8, 18u32 + 3, @op.get!(8 + 9))
            else
              disableOperator(@op.get!(8 + 9), OP_ACT_PERC)
            end

            if (val & 0x21) == 0x21
              # Hihat active
              enableOperator(17, @op.get!(7), OP_ACT_PERC)
              changeFrequency(7, 17, @op.get!(7))
            else
              disableOperator(@op.get!(7), OP_ACT_PERC)
            end

            return
          end

          # Regular 0xB0 - 0xB8
          base = (idx - ARC_KON_BNUM) & 0xFF
          if base < 9
            opBase = if secondSet != 0
                       (base + 18).to_i32!
                     else
                       base.to_i32!
                     end

            if Yuno.bitflag?(@adlibReg.get!(0x105), 1) && @op[opBase].is4OpAttached?
              return
            end

            # Regbase of modulator
            modBase = MODULATOR_BASE[base].to_i32! + secondSet
            if Yuno.bitflag?(val, 32)
              # Operator switched on
              enableOperator(modBase.to_u32!, @op.get!(opBase), OP_ACT_NORMAL) # modulator (if 2op)
              enableOperator((modBase + 3).to_u32!, @op[opBase + 9], OP_ACT_NORMAL) # carrier (if 2op)

              # For 4op channels all four operators are switched on.
              if Yuno.bitflag?(@adlibReg.get!(0x105), 1) && @op.get!(opBase).is4Op?
                # Turn on chan + 3 operators as well
                enableOperator((modBase + 8).to_u32!, @op.get!(opBase + 3), OP_ACT_NORMAL)
                enableOperator((modBase + (3 + 8)).to_u32!, @op[opBase + (3 + 9)], OP_ACT_NORMAL)
              end
            else
              # Operator switched off.
              disableOperator(@op.get!(opBase), OP_ACT_NORMAL)
              disableOperator(@op[opBase + 9], OP_ACT_NORMAL)

              # For 4op channels all four operators are switched off.
              if Yuno.bitflag?(@adlibReg.get!(0x105), 1) && @op.get!(opBase).is4Op?
                # Turn off chan + 3 operators as well
                disableOperator(@op.get!(opBase + 3), OP_ACT_NORMAL)
                disableOperator(@op[opBase + (3 + 9)], OP_ACT_NORMAL)
              end
            end

            chanBase = base + secondSet

            # Change frequency calculations of modulator and carrier (2op) as
            # the frequency of the channel has changed.
            changeFrequency(chanBase, modBase.to_u32!, @op.get!(opBase))
            changeFrequency(chanBase, (modBase + 3).to_u32!, @op[opBase + 9])

            # for 4op channels all four operators are modified to the frequency of the channel
            if Yuno.bitflag?(@adlibReg.get!(0x105), 1) && @op[secondSet != 0 ? base + 18 : base].is4Op?
              # Change frequency calculations of chan + 3 operators as well.
              changeFrequency(chanBase, (modBase + 8).to_u32!, @op.get!(opBase + 3))
              changeFrequency(chanBase, (modBase + (3 + 8)).to_u32!, @op[opBase + (3 + 9)])
            end
          end

        when ARC_FEEDBACK
          # 0xC0 - 0xC8 feedback/modulation type (AM/FM)
          base = (idx - ARC_FEEDBACK) & 0xFF
          if base < 9
            opBase = if secondSet != 0
                       (base + 18).to_i32!
                     else
                       base.to_i32!
                     end
            chanBase = base + secondSet
            changeFeedback(chanBase, @op.get!(opBase))

            @op.get!(opBase).leftPan   = (val & 0x10).to_i32! >> 4
            @op.get!(opBase).rightPan  = (val & 0x20).to_i32! >> 5
            @op.get!(opBase).leftPan  += (val & 0x40).to_i32! >> 6
            @op.get!(opBase).rightPan += (val & 0x80).to_i32! >> 7
          end

        when ARC_WAVE_SEL, ARC_WAVE_SEL + 0x10
          num = (idx & 7).to_i32!
          base = (idx - ARC_WAVE_SEL) & 0xFF
          if num < 6 && base < 22
            wselBase : Int32 = if secondSet != 0
                                 (base + 22).to_i32!
                               else
                                 base.to_i32!
                               end

            # Change waveform
            if Yuno.bitflag?(@adlibReg.get!(0x105), 1)
              # OPL3 mode enabled, all waveforms accessible
              @waveSel[wselBase] = val & 7
            else
              @waveSel[wselBase] = val & 3
            end

            op = @op[REGBASE_TO_MODOP.get!(wselBase) + (num < 3 ? 0 : 9)]
            changeWaveform(wselBase.to_u32!, op)
          end
        end
      {% end %}
    end

    @[AlwaysInline]
    def regRead(port : UInt32) : UInt32
      # opl2-detection routines require ret & 6 to be zero
      unless Yuno.bitflag?(port, 1)
        @status.to_u32!
      else
        0x00_u32
      end
    end

    @[AlwaysInline]
    def writeIndex(port : UInt32, val : UInt8) : Nil
      @oplIndex = val
      if Yuno.bitflag?(port, 3)
        # possibly second set
        if (@adlibReg.get!(0x105) & 1) != 0 || @oplIndex == 5
          @oplIndex |= ARC_SECONDSET
        end
      end
    end

    # Careful with this... it's purposely unhygenic and uses cptr and chanVal.
    # This outputs into outL and outR.
    #
    # Changes by Valley Bell:
    #  - Changed to always output to both channels
    #  - added parameter "chn" to fix panning for 4-op channels and the Rhythm Cymbal
    macro chanValOut(chan)
      if Yuno.bitflag?(@adlibReg.get!(0x105), 1)
        outL.put!(i, outL.get!(i) + (chanVal * cptr[{{chan}}].leftPan))
        outR.put!(i, outR.get!(i) + (chanVal * cptr[{{chan}}].rightPan))
      else
        outL.put!(i, outL.get!(i) + chanVal)
        outR.put!(i, outR.get!(i) + chanVal)
      end
    end

    def update(outputs : OutputBuffers, samples : UInt32) : Nil
      finishSamples : Int32 = 0
      outL : Slice(Int32) = outputs[0]
      outR : Slice(Int32) = outputs[1]
      samplesToProcess : Int32 = samples.to_i32!
      curSamp : Int32 = 0
      vibTShift : Int32 = 0
      maxChannel : UInt32 = NUM_CHANNELS
      cptr : Pointer(Operator) = Pointer(Operator).null
      chanVal : Int32 = 0
      k : UInt32 = 0

      # Remi: Left these as pointers to simplify things and live dangerously 😎
      vibVal1 : Pointer(Int32) = Pointer(Int32).null
      vibVal2 : Pointer(Int32) = Pointer(Int32).null
      vibVal3 : Pointer(Int32) = Pointer(Int32).null
      vibVal4 : Pointer(Int32) = Pointer(Int32).null
      tremVal1 : Pointer(Int32) = Pointer(Int32).null
      tremVal2 : Pointer(Int32) = Pointer(Int32).null
      tremVal3 : Pointer(Int32) = Pointer(Int32).null
      tremVal4 : Pointer(Int32) = Pointer(Int32).null

      # Check once, then we can avoid bounds checks below.
      if outL.size < samples || outR.size < samples
        raise "OPL3 update: Bad number of samples (expected #{samples}, " \
              "but buffers have #{outL.size} and #{outR.size})"
      end

      unless Yuno.bitflag?(@adlibReg.get!(0x105), 1)
        maxChannel = NUM_CHANNELS >> 1 #.tdiv(2)
      end

      if samplesToProcess == 0
        maxChannel.times do |curCh|
          if Yuno.bitflag?(@adlibReg.get!(ARC_PERC_MODE), 0x20) && (curCh >= 6 && curCh < 9)
            next
          end

          cptr = if curCh < 9
                   @op.to_unsafe + curCh
                 else
                   @op.to_unsafe + (curCh + 9)
                 end
          next if cptr[0].is4OpAttached?

          operatorEgAttackCheck(cptr[0]) if cptr[0].opState == OF_TYPE_ATT
          operatorEgAttackCheck(cptr[9]) if cptr[9].opState == OF_TYPE_ATT
        end

        return
      end

      while curSamp < samplesToProcess
        finishSamples = samplesToProcess - curSamp

        outL.fill(0, 0, finishSamples)
        outR.fill(0, 0, finishSamples)

        # Calculate vibrato/tremolo lookup tables
        vibTShift = unless Yuno.bitflag?(@adlibReg.get!(ARC_PERC_MODE), 0x40)
                      1 # 14 cents
                    else
                      0 # 7 cents
                    end

        finishSamples.times do |i|
          # Cycle through vibrato table.
          @vibTabPos += @vibTabAdd
          if @vibTabPos.tdiv(FIXEDPT_LFO) >= VIBTAB_SIZE
            @vibTabPos -= VIBTAB_SIZE * FIXEDPT_LFO
          end

          # 14 cents (14/100 of a semitone) or 7 cents
          @vibLut.put!(i, @vibTable.get!(@vibTabPos.tdiv(FIXEDPT_LFO)) >> vibTShift)

          # Cycle through tremolo table
          @tremTabPos += @tremTabAdd
          if @tremTabPos.tdiv(FIXEDPT_LFO) >= TREMTAB_SIZE
            @tremTabPos -= TREMTAB_SIZE * FIXEDPT_LFO
          end

          if Yuno.bitflag?(@adlibReg[ARC_PERC_MODE], 0x80)
            @tremLut.put!(i, @tremTable[@tremTabPos.tdiv(FIXEDPT_LFO)])
          else
            @tremLut.put!(i, @tremTable[TREMTAB_SIZE + @tremTabPos.tdiv(FIXEDPT_LFO)])
          end
        end

        if Yuno.bitflag?(@adlibReg.get!(ARC_PERC_MODE), 0x20)
          # Bass Drum
          if @muteChan.get!(NUM_CHANNELS) != 0
            cptr = @op.to_unsafe + 6

            if Yuno.bitflag?(@adlibReg.get!(ARC_FEEDBACK + 6), 1)
              # Additive synthesis
              if cptr[9].opState != OF_TYPE_OFF
                if cptr[9].vibrato?
                  vibVal1 = @vibValVar1.to_unsafe
                  finishSamples.times do |i|
                    vibVal1[i] = ((@vibLut[i].to_f64! * cptr[9].freqHigh / 8) * FIXEDPT * VIBFAC).to_i32!
                  end
                else
                  vibVal1 = @vibValConst.to_unsafe
                end

                if cptr[9].tremolo?
                  tremVal1 = @tremLut.to_unsafe # Tremolo enabled, use table
                else
                  tremVal1 = @tremValConst.to_unsafe
                end

                # Calculate channel output
                finishSamples.times do |i|
                  operatorAdvance(cptr[9], vibVal1[i])
                  @opFuncs.get!(cptr[9].opState).not_nil!.call(cptr[9])
                  operatorOutput(cptr[9], 0, tremVal1[i])

                  chanVal = cptr[9].cval * 2
                  chanValOut(0)
                end
              end
            else
              # Frequency modulation
              if cptr[9].opState != OF_TYPE_OFF || cptr[9].opState != OF_TYPE_OFF
                if cptr[0].vibrato? && cptr[0].opState != OF_TYPE_OFF
                  vibVal1 = @vibValVar1.to_unsafe
                  finishSamples.times do |i|
                    vibVal1[i] = ((@vibLut.get!(i) * cptr[0].freqHigh / 8) * FIXEDPT * VIBFAC).to_i32!
                  end
                else
                  vibVal1 = @vibValConst.to_unsafe
                end

                if cptr[9].vibrato? && cptr[9].opState != OF_TYPE_OFF
                  vibVal2 = @vibValVar2.to_unsafe
                  finishSamples.times do |i|
                    vibVal2[i] = ((@vibLut.get!(i) * cptr[9].freqHigh / 8) * FIXEDPT * VIBFAC).to_i32!
                  end
                else
                  vibVal2 = @vibValConst.to_unsafe
                end

                if cptr[0].tremolo?
                  tremVal1 = @tremLut.to_unsafe # Tremolo enabled, use table
                else
                  tremVal1 = @tremValConst.to_unsafe
                end

                if cptr[9].tremolo?
                  tremVal2 = @tremLut.to_unsafe # Tremolo enabled, use table
                else
                  tremVal2 = @tremValConst.to_unsafe
                end

                # Calculate channel output
                finishSamples.times do |i|
                  operatorAdvance(cptr[0], vibVal1[i])
                  @opFuncs.get!(cptr[0].opState).not_nil!.call(cptr[0])
                  operatorOutput(cptr[0],
                                 ((cptr[0].lastCval + cptr[0].cval) * cptr[0].mfbi).tdiv(2),
                                 tremVal1[i])

                  operatorAdvance(cptr[9], vibVal2[i])
                  @opFuncs.get!(cptr[9].opState).not_nil!.call(cptr[9])
                  operatorOutput(cptr[9], cptr[0].cval * FIXEDPT, tremVal2[i])

                  chanVal = cptr[9].cval * 2
                  chanValOut(0)
                end
              end
            end
          end # if @muteChan[NUM_CHANNELS] != 0

          # Tom tom (j = 8)
          if @muteChan[NUM_CHANNELS + 2] != 0 && @op.get!(8).opState != OF_TYPE_OFF
            cptr = @op.to_unsafe + 8
            if cptr[0].vibrato?
              vibVal3 = @vibValVar1.to_unsafe
              finishSamples.times do |i|
                vibVal3[i] = ((@vibLut.get!(i) * cptr[0].freqHigh / 8) * FIXEDPT * VIBFAC).to_i32!
              end
            else
              vibVal3 = @vibValConst.to_unsafe
            end

            if cptr[0].tremolo?
              tremVal3 = @tremLut.to_unsafe # Tremolo enabled, use table
            else
              tremVal3 = @tremValConst.to_unsafe
            end

            # Calculate channel output
            finishSamples.times do |i|
              operatorAdvance(cptr[0], vibVal3[i])
              @opFuncs.get!(cptr[0].opState).not_nil!.call(cptr[0])
              operatorOutput(cptr[0], 0, tremVal3[i])
              chanVal = cptr[0].cval * 2
              chanValOut(0)
            end
          end

          # Snare/hihat (j = 7), Cymbal (j = 8)
          if @op.get!(7).opState != OF_TYPE_OFF || @op.get!(16).opState != OF_TYPE_OFF ||
             @op.get!(17).opState != OF_TYPE_OFF
            # Snare/hihat
            cptr = @op.to_unsafe + 7

            if cptr[0].vibrato? && cptr[0].opState != OF_TYPE_OFF
              vibVal1 = @vibValVar1.to_unsafe
              finishSamples.times do |i|
                vibVal1[i] = ((@vibLut.get!(i) * cptr[0].freqHigh / 8) * FIXEDPT * VIBFAC).to_i32!
              end
            else
              vibVal1 = @vibValConst.to_unsafe
            end

            if cptr[9].vibrato? && cptr[9].opState == OF_TYPE_OFF
              vibVal2 = @vibValVar1.to_unsafe
              finishSamples.times do |i|
                vibVal2[i] = ((@vibLut.get!(i) * cptr[9].freqHigh / 8) * FIXEDPT * VIBFAC).to_i32!
              end
            else
              vibVal2 = @vibValConst.to_unsafe
            end

            tremVal1 = if cptr[0].tremolo?
                         @tremLut.to_unsafe # Tremolo enabled, use table
                       else
                         @tremValConst.to_unsafe
                       end

            tremVal2 = if cptr[9].tremolo?
                         @tremLut.to_unsafe # Tremolo enabled, use table
                       else
                         @tremValConst.to_unsafe
                       end

            # Cymbal
            cptr = @op.to_unsafe + 8
            if cptr[9].vibrato? && cptr[9].opState == OF_TYPE_OFF
              vibVal4 = @vibValVar2.to_unsafe
              finishSamples.times do |i|
                vibVal4[i] = ((@vibLut.get!(i) * cptr[9].freqHigh / 8) * FIXEDPT * VIBFAC).to_i32!
              end
            else
              vibVal4 = @vibValConst.to_unsafe
            end

            tremVal4 = if cptr[9].tremolo?
                         @tremLut.to_unsafe # Tremolo enabled, use table
                       else
                         @tremValConst.to_unsafe
                       end

            # Calculate channel output
            cptr = @op.to_unsafe # Set cptr to something useful (else it stays at @op[8])
            finishSamples.times do |i|
              operatorAdvanceDrums(@op.get!(7    ), vibVal1[i],
                                   @op.get!(7 + 9), vibVal2[i],
                                   @op.get!(8 + 9), vibVal4[i])

              # Hihat
              if @muteChan.get!(NUM_CHANNELS + 4) != 0
                @opFuncs.get!(@op.get!(7).opState).not_nil!.call(@op.get!(7))
                operatorOutput(@op.get!(7), 0, tremVal1[i])
              else
                @op.get!(7).cval = 0
              end

              # Snare
              if @muteChan.get!(NUM_CHANNELS + 1) != 0
                @opFuncs.get!(@op.get!(7 + 9).opState).not_nil!.call(@op.get!(7 + 9))
                operatorOutput(@op.get!(7 + 9), 0, tremVal2[i])
              else
                @op.get!(7 + 9).cval = 0
              end

              # Cymbal
              if @muteChan.get!(NUM_CHANNELS + 3) != 0
                @opFuncs.get!(@op.get!(8 + 9).opState).not_nil!.call(@op.get!(8 + 9))
                operatorOutput(@op.get!(8 + 9), 0, tremVal4[i])
              else
                @op.get!(8 + 9).cval = 0
              end

              chanVal = (@op.get!(7).cval + @op.get!(7 + 9).cval) * 2
              chanValOut(7)
              chanVal = @op.get!(8 + 9).cval * 2
              chanValOut(8)
            end
          end
        end # if Yuno.bitflag?(@adlibReg[ARC_PERC_MODE], 0x20)

        (maxChannel - 1).downto(0) do |curCh|
          next if @muteChan.get!(curCh) != 0

          # Skip drum/percussion operators
          next if Yuno.bitflag?(@adlibReg.get!(ARC_PERC_MODE), 0x20) && curCh >= 6 && curCh < 9

          k = curCh

          if curCh < 9
            cptr = @op.to_unsafe + curCh
          else
            cptr = @op.to_unsafe + (curCh + 9) # Second set is operator 18 to operator 35
            k += (-9 + 256) # second set uses registers 0x100 onwards
          end

          # Check if this operator is part of a 4-op
          next if cptr[0].is4OpAttached?

          # Check for FM/AM
          if Yuno.bitflag?(@adlibReg.get!(ARC_FEEDBACK + k), 1)
            if cptr[0].is4Op?
              if Yuno.bitflag?(@adlibReg.get!(ARC_FEEDBACK + k + 3), 1)
                # AM-AM style synthesis (op1[fb] + (op2 * op3) + op4)
                if cptr[0].opState != OF_TYPE_OFF
                  if cptr[0].vibrato?
                    vibVal1 = @vibValVar1.to_unsafe
                    finishSamples.times do |i|
                      vibVal1[i] = ((@vibLut.get!(i) * cptr[0].freqHigh / 8) * FIXEDPT * VIBFAC).to_i32!
                    end
                  else
                    vibVal1 = @vibValConst.to_unsafe
                  end

                  tremVal1 = if cptr[0].tremolo?
                               @tremLut.to_unsafe # Tremolo enabled, use table
                             else
                               @tremValConst.to_unsafe
                             end

                  # Calculate channel output
                  finishSamples.times do |i|
                    operatorAdvance(cptr[0], vibVal1[i])
                    @opFuncs.get!(cptr[0].opState).not_nil!.call(cptr[0])
                    operatorOutput(cptr[0],
                                   ((cptr[0].lastCval + cptr[0].cval) * cptr[0].mfbi).tdiv(2),
                                   tremVal1[i])

                    chanVal = cptr[0].cval
                    chanValOut(3) # Note: Op 1 of 4, so it needs to use the panning bits of Op 4 (Ch + 3)
                  end
                end # if cptr[0].opState != OF_TYPE_OFF

                if cptr[3].opState != OF_TYPE_OFF || cptr[9].opState != OF_TYPE_OFF
                  if cptr[9].vibrato? && cptr[9].opState != OF_TYPE_OFF
                    vibVal1 = @vibValVar1.to_unsafe
                    finishSamples.times do |i|
                      vibVal1[i] = ((@vibLut.get!(i) * cptr[9].freqHigh / 8) * FIXEDPT * VIBFAC).to_i32!
                    end
                  else
                    vibVal1 = @vibValConst.to_unsafe
                  end

                  tremVal1 = if cptr[9].tremolo?
                               @tremLut.to_unsafe # Tremolo enabled, use table
                             else
                               @tremValConst.to_unsafe
                             end

                  tremVal2 = if cptr[3].tremolo?
                               @tremLut.to_unsafe # Tremolo enabled, use table
                             else
                               @tremValConst.to_unsafe
                             end

                  # Calculate channel output
                  finishSamples.times do |i|
                    operatorAdvance(cptr[9], vibVal1[i])
                    @opFuncs.get!(cptr[9].opState).not_nil!.call(cptr[9])
                    operatorOutput(cptr[9], 0, tremVal1[i])

                    operatorAdvance(cptr[3], 0)
                    @opFuncs.get!(cptr[3].opState).not_nil!.call(cptr[3])
                    operatorOutput(cptr[3], cptr[9].cval * FIXEDPT, tremVal2[i])

                    chanVal = cptr[3].cval
                    chanValOut(3)
                  end
                end # if cptr[3].opState != OF_TYPE_OFF || cptr[9].opState != OF_TYPE_OFF

                if cptr[3 + 9].opState != OF_TYPE_OFF
                  tremVal1 = if cptr[3 + 9].tremolo?
                               @tremLut.to_unsafe # Tremolo enabled, use table
                             else
                               @tremValConst.to_unsafe
                             end

                  # Calculate channel output
                  finishSamples.times do |i|
                    operatorAdvance(cptr[3 + 9], 0)
                    @opFuncs.get!(cptr[3 + 9].opState).not_nil!.call(cptr[3 + 9])
                    operatorOutput(cptr[3 + 9], 0, tremVal1[i])

                    chanVal = cptr[3 + 9].cval
                    chanValOut(3)
                  end
                end
              else # if Yuno.bitflag?(@adlibReg[ARC_FEEDBACK + k + 3], 1)
                # AM-FM style synthesis (op1[fb] + (op2 * op3 * op4))
                if cptr[0].opState != OF_TYPE_OFF
                  if cptr[0].vibrato?
                    vibVal1 = @vibValVar1.to_unsafe
                    finishSamples.times do |i|
                      vibVal1[i] = ((@vibLut.get!(i) * cptr[0].freqHigh / 8) * FIXEDPT * VIBFAC).to_i32!
                    end
                  else
                    vibVal1 = @vibValConst.to_unsafe
                  end

                  tremVal1 = if cptr[0].tremolo?
                               @tremLut.to_unsafe # Tremolo enabled, use table
                             else
                               @tremValConst.to_unsafe
                             end

                  # Calculate channel output
                  finishSamples.times do |i|
                    operatorAdvance(cptr[0], vibVal1[i])
                    @opFuncs.get!(cptr[0].opState).not_nil!.call(cptr[0])
                    operatorOutput(cptr[0],
                                   ((cptr[0].lastCval + cptr[0].cval) * cptr[0].mfbi).tdiv(2),
                                   tremVal1[i])

                    chanVal = cptr[0].cval
                    chanValOut(3)
                  end
                end

                if cptr[9].opState != OF_TYPE_OFF || cptr[3].opState != OF_TYPE_OFF ||
                   cptr[3 + 9].opState != OF_TYPE_OFF

                  if cptr[9].vibrato? && cptr[9].opState != OF_TYPE_OFF
                    vibVal1 = @vibValVar1.to_unsafe
                    finishSamples.times do |i|
                      vibVal1[i] = ((@vibLut.get!(i) * cptr[9].freqHigh / 8) * FIXEDPT * VIBFAC).to_i32!
                    end
                  else
                    vibVal1 = @vibValConst.to_unsafe
                  end

                  tremVal1 = if cptr[9].tremolo?
                               @tremLut.to_unsafe # Tremolo enabled, use table
                             else
                               @tremValConst.to_unsafe
                             end

                  tremVal2 = if cptr[3].tremolo?
                               @tremLut.to_unsafe # Tremolo enabled, use table
                             else
                               @tremValConst.to_unsafe
                             end

                  tremVal3 = if cptr[3 + 9].tremolo?
                               @tremLut.to_unsafe # Tremolo enabled, use table
                             else
                               @tremValConst.to_unsafe
                             end

                  # Calculate channel output
                  finishSamples.times do |i|
                    operatorAdvance(cptr[9], vibVal1[i])
                    @opFuncs.get!(cptr[9].opState).not_nil!.call(cptr[9])
                    operatorOutput(cptr[9], 0, tremVal1[i])

                    operatorAdvance(cptr[3], 0)
                    @opFuncs.get!(cptr[3].opState).not_nil!.call(cptr[3])
                    operatorOutput(cptr[3], cptr[9].cval * FIXEDPT, tremVal2[i])

                    operatorAdvance(cptr[3 + 9], 0)
                    @opFuncs.get!(cptr[3 + 9].opState).not_nil!.call(cptr[3 + 9])
                    operatorOutput(cptr[3 + 9], cptr[3].cval * FIXEDPT, tremVal3[i])

                    chanVal = cptr[3 + 9].cval
                    chanValOut(3)
                  end
                end
              end # if Yuno.bitflag?(@adlibReg[ARC_FEEDBACK + k + 3], 1)

              next
            end #if cptr.value.is4Op?

            # 2op additive synthesis
            next if cptr[9].opState == OF_TYPE_OFF && cptr[0].opState == OF_TYPE_OFF

            if cptr[0].vibrato? && cptr[0].opState != OF_TYPE_OFF
              vibVal1 = @vibValVar1.to_unsafe
              finishSamples.times do |i|
                vibVal1[i] = ((@vibLut.get!(i) * cptr[0].freqHigh / 8) * FIXEDPT * VIBFAC).to_i32!
              end
            else
              vibVal1 = @vibValConst.to_unsafe
            end

            if cptr[9].vibrato? && cptr[9].opState != OF_TYPE_OFF
              vibVal2 = @vibValVar2.to_unsafe
              finishSamples.times do |i|
                vibVal2[i] = ((@vibLut.get!(i) * cptr[9].freqHigh / 8) * FIXEDPT * VIBFAC).to_i32!
              end
            else
              vibVal2 = @vibValConst.to_unsafe
            end

            tremVal1 = if cptr[0].tremolo?
                         @tremLut.to_unsafe # Tremolo enabled, use table
                       else
                         @tremValConst.to_unsafe # Tremolo enabled, use table
                       end

            tremVal2 = if cptr[9].tremolo?
                         @tremLut.to_unsafe # Tremolo enabled, use table
                       else
                         @tremValConst.to_unsafe # Tremolo enabled, use table
                       end

            # Calculate channel output
            finishSamples.times do |i|
              # Carrier 1
              operatorAdvance(cptr[0], vibVal1[i])
              @opFuncs.get!(cptr[0].opState).not_nil!.call(cptr[0])
              operatorOutput(cptr[0],
                             ((cptr[0].lastCval + cptr[0].cval) * cptr[0].mfbi).tdiv(2),
                             tremVal1[i])

              # Carrier 2
              operatorAdvance(cptr[9], vibVal2[i])
              @opFuncs.get!(cptr[9].opState).not_nil!.call(cptr[9])
              operatorOutput(cptr[9], 0, tremVal2[i])

              chanVal = cptr[9].cval + cptr[0].cval
              chanValOut(0)
            end
          else #if Yuno.bitflag?(@adlibReg[ARC_FEEDBACK + k], 1)
            if cptr.value.is4Op?
              if Yuno.bitflag?(@adlibReg.get!(ARC_FEEDBACK + k + 3), 1)
                # FM-AM style synthesis ((op1[fb] * op2) + (op3 * op4))
                if cptr[0].opState != OF_TYPE_OFF || cptr[9].opState != OF_TYPE_OFF
                  if cptr[0].vibrato? && cptr[0].opState != OF_TYPE_OFF
                    vibVal1 = @vibValVar1.to_unsafe
                    finishSamples.times do |i|
                      vibVal1[i] = ((@vibLut.get!(i) * cptr[0].freqHigh / 8) * FIXEDPT * VIBFAC).to_i32!
                    end
                  else
                    vibVal1 = @vibValConst.to_unsafe
                  end

                  if cptr[9].vibrato? && cptr[9].opState != OF_TYPE_OFF
                    vibVal2 = @vibValVar2.to_unsafe
                    finishSamples.times do |i|
                      vibVal2[i] = ((@vibLut.get!(i) * cptr[9].freqHigh / 8) * FIXEDPT * VIBFAC).to_i32!
                    end
                  else
                    vibVal2 = @vibValConst.to_unsafe
                  end

                  tremVal1 = if cptr[0].tremolo?
                               @tremLut.to_unsafe # Tremolo enabled, use table
                             else
                               @tremValConst.to_unsafe
                             end

                  tremVal2 = if cptr[9].tremolo?
                               @tremLut.to_unsafe # Tremolo enabled, use table
                             else
                               @tremValConst.to_unsafe
                             end

                  # Calculate channel output
                  finishSamples.times do |i|
                    operatorAdvance(cptr[0], vibVal1[i])
                    @opFuncs.get!(cptr[0].opState).not_nil!.call(cptr[0])
                    operatorOutput(cptr[0],
                                   ((cptr[0].lastCval + cptr[0].cval) * cptr[0].mfbi).tdiv(2),
                                   tremVal1[i])

                    operatorAdvance(cptr[9], vibVal2[i])
                    @opFuncs.get!(cptr[9].opState).not_nil!.call(cptr[9])
                    operatorOutput(cptr[9], cptr[9].cval * FIXEDPT, tremVal2[i])

                    chanVal = cptr[9].cval
                    chanValOut(3)
                  end
                end

                if cptr[3].opState != OF_TYPE_OFF || cptr[3 + 9].opState != OF_TYPE_OFF
                  tremVal1 = if cptr[3].tremolo?
                               @tremLut.to_unsafe # Tremolo enabled, use table
                             else
                               @tremValConst.to_unsafe
                             end

                  tremVal2 = if cptr[3 + 9].tremolo?
                               @tremLut.to_unsafe # Tremolo enabled, use table
                             else
                               @tremValConst.to_unsafe
                             end

                  # Calculate channel output
                  finishSamples.times do |i|
                    operatorAdvance(cptr[3], 0)
                    @opFuncs.get!(cptr[3].opState).not_nil!.call(cptr[3])
                    operatorOutput(cptr[3], 0, tremVal1[i])

                    operatorAdvance(cptr[3 + 9], 0)
                    @opFuncs.get!(cptr[3 + 9].opState).not_nil!.call(cptr[3 + 9])
                    operatorOutput(cptr[3 + 9], cptr[3].cval * FIXEDPT, tremVal2[i])

                    chanVal = cptr[3 + 9].cval
                    chanValOut(3)
                  end
                end
              else # if Yuno.bitflag?(@adlibReg[ARC_FEEDBACK + k + 3], 1)
                # FM-FM style synthesis (op1[fb] * op2 * op3 * op4)
                if cptr[0].opState != OF_TYPE_OFF || cptr[9].opState != OF_TYPE_OFF ||
                   cptr[3].opState != OF_TYPE_OFF || cptr[3 + 9].opState != OF_TYPE_OFF
                  if cptr[0].vibrato? && cptr[0].opState != OF_TYPE_OFF
                    vibVal1 = @vibValVar1.to_unsafe
                    finishSamples.times do |i|
                      vibVal1[i] = ((@vibLut.get!(i) * cptr[0].freqHigh / 8) * FIXEDPT * VIBFAC).to_i32!
                    end
                  else
                    vibVal1 = @vibValConst.to_unsafe
                  end

                  if cptr[9].vibrato? && cptr[9].opState != OF_TYPE_OFF
                    vibVal2 = @vibValVar2.to_unsafe
                    finishSamples.times do |i|
                      vibVal2[i] = ((@vibLut.get!(i) * cptr[9].freqHigh / 8) * FIXEDPT * VIBFAC).to_i32!
                    end
                  else
                    vibVal2 = @vibValConst.to_unsafe
                  end

                  tremVal1 = if cptr[0].tremolo?
                               @tremLut.to_unsafe # Tremolo enabled, use table
                             else
                               @tremValConst.to_unsafe
                             end

                  tremVal2 = if cptr[9].tremolo?
                               @tremLut.to_unsafe # Tremolo enabled, use table
                             else
                               @tremValConst.to_unsafe
                             end

                  tremVal3 = if cptr[3].tremolo?
                               @tremLut.to_unsafe # Tremolo enabled, use table
                             else
                               @tremValConst.to_unsafe
                             end

                  tremVal4 = if cptr[3 + 9].tremolo?
                               @tremLut.to_unsafe # Tremolo enabled, use table
                             else
                               @tremValConst.to_unsafe
                             end

                  # Calculate channel output
                  finishSamples.times do |i|
                    operatorAdvance(cptr[0], vibVal1[i])
                    @opFuncs.get!(cptr[0].opState).not_nil!.call(cptr[0])
                    operatorOutput(cptr[0],
                                   ((cptr[0].lastCval + cptr[0].cval) * cptr[0].mfbi).tdiv(2),
                                   tremVal1[i])

                    operatorAdvance(cptr[9], vibVal2[i])
                    @opFuncs.get!(cptr[9].opState).not_nil!.call(cptr[9])
                    operatorOutput(cptr[9], cptr[0].cval * FIXEDPT, tremVal2[i])

                    operatorAdvance(cptr[3], 0)
                    @opFuncs.get!(cptr[3].opState).not_nil!.call(cptr[3])
                    operatorOutput(cptr[3], cptr[9].cval * FIXEDPT, tremVal3[i])

                    operatorAdvance(cptr[3 + 9], 0)
                    @opFuncs.get!(cptr[3 + 9].opState).not_nil!.call(cptr[3 + 9])
                    operatorOutput(cptr[3 + 9], cptr[3].cval * FIXEDPT, tremVal4[i])

                    chanVal = cptr[3 + 9].cval
                    chanValOut(3)
                  end
                end
              end # if Yuno.bitflag?(@adlibReg[ARC_FEEDBACK + k + 3], 1)

              next
            end # if cptr.value.is4Op?

            # 2op frequency modulation
            next if cptr[9].opState == OF_TYPE_OFF && cptr[0].opState == OF_TYPE_OFF

            if cptr[0].vibrato? && cptr[0].opState != OF_TYPE_OFF
              vibVal1 = @vibValVar1.to_unsafe
              finishSamples.times do |i|
                vibVal1[i] = ((@vibLut.get!(i) * cptr[0].freqHigh / 8) * FIXEDPT * VIBFAC).to_i32!
              end
            else
              vibVal1 = @vibValConst.to_unsafe
            end

            if cptr[9].vibrato? && cptr[9].opState != OF_TYPE_OFF
              vibVal2 = @vibValVar2.to_unsafe
              finishSamples.times do |i|
                vibVal2[i] = ((@vibLut.get!(i) * cptr[9].freqHigh / 8) * FIXEDPT * VIBFAC).to_i32!
              end
            else
              vibVal2 = @vibValConst.to_unsafe
            end

            tremVal1 = if cptr[0].tremolo?
                         @tremLut.to_unsafe # Tremolo enabled, use table
                       else
                         @tremValConst.to_unsafe
                       end

            tremVal2 = if cptr[9].tremolo?
                         @tremLut.to_unsafe # Tremolo enabled, use table
                       else
                         @tremValConst.to_unsafe
                       end

            # Calculate channel output
            finishSamples.times do |i|
              # Modulator
              operatorAdvance(cptr[0], vibVal1[i])
              @opFuncs.get!(cptr[0].opState).not_nil!.call(cptr[0])
              operatorOutput(cptr[0],
                             ((cptr[0].lastCval + cptr[0].cval) * cptr[0].mfbi).tdiv(2),
                             tremVal1[i])

              # Carrier
              operatorAdvance(cptr[9], vibVal2[i])
              @opFuncs.get!(cptr[9].opState).not_nil!.call(cptr[9])
              operatorOutput(cptr[9], cptr[0].cval * FIXEDPT, tremVal2[i])

              chanVal = cptr[9].cval
              chanValOut(0)
            end
          end
        end

        curSamp += finishSamples
      end # while curSamp < samplesToProcess
    end # def update()

    def muteMask=(mask : UInt32) : Nil
      (NUM_CHANNELS + 5).times do |i|
        @muteChan[i] = (mask >> i) & 0x01
      end
    end
  end
end
