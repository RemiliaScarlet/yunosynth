#### OPL2/OPL3 emulation library
#### Copyright (C) 2002-2010  The DOSBox Team
#### Crystal port Copyright (C) 2023-2024 Remilia Scarlet
####
#### This library is free software; you can redistribute it and/or modify it
#### under the terms of the GNU Lesser General Public License as published by
#### the Free Software Foundation; either version 2.1 of the License, or (at
#### your option) any later version.
####
#### This library is distributed in the hope that it will be useful, but WITHOUT
#### ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
#### License for more details.
####
#### You should have received a copy of the GNU Lesser General Public License
#### along with this library; if not, write to the Free Software Foundation,
#### Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
require "../../common.cr"

####
#### Common Yamaha OPL Dosbox Routines
####

module Yuno::Chips
  ###
  ### Note: Do NOT include OPLDosbox.  Use the OPL2Dosbox or OPL3Dosbox classes.
  ###

  # :nodoc:
  module OPLDosbox
    ###
    ### Aliases
    ###

    alias AdlUpdateHandler = Proc(AbstractChip, Nil)
    alias OpFunc = Proc(Operator, Nil)

    ###
    ### Constants
    ###

    FL05 = 0.5
    FL2  = 2.0

    FIXEDPT     = 0x10000 # Fixed-point calculations using 16+16
    FIXEDPT_LFO = 0x1000000 # Fixed-point calculations using 8+24

    WAVEPREC = 1024u32 # Waveform precision (10 bits)

    # TODO change these to an enum
    OF_TYPE_ATT        = 0u32
    OF_TYPE_DEC        = 1u32
    OF_TYPE_REL        = 2u32
    OF_TYPE_SUS        = 3u32
    OF_TYPE_SUS_NOKEEP = 4u32
    OF_TYPE_OFF        = 5u32

    ARC_CONTROL     = 0x00
    ARC_TVS_KSR_MUL = 0x20
    ARC_KSL_OUTLEV  = 0x40
    ARC_ATTR_DECR   = 0x60
    ARC_SUSL_RELR   = 0x80
    ARC_FREQ_NUM    = 0xA0
    ARC_KON_BNUM    = 0xB0
    ARC_PERC_MODE   = 0xBD
    ARC_FEEDBACK    = 0xC0
    ARC_WAVE_SEL    = 0xE0

    ARC_SECONDSET = 0x100 # Second operator set for OPL3

    OP_ACT_OFF    = 0x00_u32
    OP_ACT_NORMAL = 0x01_u32 # Regular channel activated (bitmasked)
    OP_ACT_PERC   = 0x02_u32 # Percussion channel activated (bitmasked)

    BLOCKBUF_SIZE = 512

    # Vibrato constants
    VIBTAB_SIZE = 8
    VIBFAC      = 70.0 / 50000.0 # Remi: Changed to Float64

    # Tremolo constants and table
    TREMTAB_SIZE = 53
    TREM_FREQ    = 3.7 # Tremolo at 3.7hz

    # Key scale level lookup table
    KSLMUL = [0.0, 0.5, 0.25, 1.0] # 0, 3, 1.5, 6 dB/oct

    # Frequency multiplier lookup table
    FRQMUL_TAB = [
      0.5, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0 ,10.0, 12.0, 12.0, 15.0, 15.0
    ]

    # Map a channel number to the register offset of the modulator (=register base)
    MODULATOR_BASE = [
      0,   1,  2,
      8,   9, 10,
      16, 17, 18
    ] of UInt8

    # Start of the waveform
    WAVEFORM_START = [
      WAVEPREC,
      WAVEPREC >> 1,
      WAVEPREC,
      (WAVEPREC * 3) >> 2,
      0,
      0,
      (WAVEPREC * 5) >> 2,
      WAVEPREC << 1
    ] of UInt32

    # Length of the waveform as mask
    WAVEMASK = [
      WAVEPREC - 1,
      WAVEPREC - 1,
      (WAVEPREC >> 1) - 1,
      (WAVEPREC >> 1) - 1,
      WAVEPREC - 1,
      ((WAVEPREC * 3) >> 2) - 1,
      WAVEPREC >> 1,
      WAVEPREC - 1
    ] of UInt32

    # Where the first entry resides
    WAVESTART = [
      0,
      WAVEPREC >> 1,
      0,
      WAVEPREC >> 2,
      0,
      0,
      0,
      WAVEPREC >> 3
    ] of UInt32

    ##
    ## Envelope generator function constants
    ##

    ATTACK_CONST = [
      1 / 2.82624,
      1 / 2.25280,
      1 / 1.88416,
      1 / 1.59744
    ]

    DECREL_CONST = [
      1 / 39.28064,
      1 / 31.41608,
      1 / 26.17344,
      1 / 22.44608
    ]

    ###
    ### Classes
    ###

    class Operator
      property cval : Int32 = 0 # Current output
      property lastCval : Int32 = 0 # Last output (used for feedback)
      property tCount : UInt32 = 0
      property wfPos : UInt32 = 0
      property tInc : UInt32 = 0

      property amp : Float64 = 0.0
      property stepAmp : Float64 = 0.0
      property vol : Float64 = 0.0
      property sustainLevel : Float64 = 0.0
      property mfbi : Int32 = 0 # Feedback amount

      ##
      ## attack rate function coefficients
      ##

      property a0 : Float64 = 0.0
      property a1 : Float64 = 0.0
      property a2 : Float64 = 0.0
      property a3 : Float64 = 0.0

      property decayMul : Float64 = 0.0 # Decay rate function
      property releaseMul : Float64 = 0.0 # Release rate function
      property opState : UInt32 = OF_TYPE_OFF # Current state of operator
                                              # (attack/decay/sustain/release/off)
      property toff : UInt32 = 0
      property freqHigh : Int32 = 0 # Highest three bits of the frequency, used for
                                    # vibrato calculations
      property curWform : Slice(Int16) = Slice(Int16).new(0, 0i16) # Selected waveform
      property curWmask : UInt32 = WAVEMASK[0] # Mask for selected waveform
      property actState : UInt32 = OP_ACT_OFF # Activity state (regular, percussion)
      property? susKeep : Bool = false # Keep sustain level when decay finished
      property? vibrato : Bool = false # Vibrato enable
      property? tremolo : Bool = false # Tremolo enable

      ##
      ## Variables used to provide non-continuous envelopes
      ##

      property generatorPos : UInt32 = 0 # For non-standard sample rates we need
                                         # to determine how many samples have
                                         # passed
      property curEnvStep : Int32 = 0 # Current (standardized) sample position
      property envStepA : Int32 = 0
      property envStepD : Int32 = 0
      property envStepR : Int32 = 0
      property stepSkipPosA : UInt8 = 0 # Position of 8-cyclic step skipping
                                        # (always 2^x to check against mask)
      property envStepSkipA : Int32 = 0 # Bitmask that determines if a step is
                                       # skipped (respective bit is zero then)

      def initialize
      end

      def initialize(@curWform : Slice(Int16))
      end
    end

    ###
    ### Fields
    ###

    @muteChan : Array(UInt8)
    @chipClock : UInt32

    @intSampleRate : Int32

    @status : UInt8 = 0u8
    @oplIndex : UInt32 = 0u32
    @oplAddr : Int32 = 0

    ##
    ## Vibrato/Tremolo tables
    ##

    @vibValVar1 : Array(Int32) = Array(Int32).new(BLOCKBUF_SIZE, 0i32) # Per-operator
    @vibValVar2 : Array(Int32) = Array(Int32).new(BLOCKBUF_SIZE, 0i32) # Per-operator

    ##
    ## Vibrato/Tremolo increments/counters
    ##

    @vibTabPos : UInt32 = 0u32
    @vibTabAdd : UInt32 = 0u32
    @tremTabPos : UInt32 = 0u32
    @tremTabAdd : UInt32 = 0u32

    @generatorAdd : UInt32 = 0u32 # Should be a chip parameter

    @invSamp : Float64 = 0.0 # Inverse of sampling rate
    @frqMul : Array(Float64) = Array(Float64).new(16, 0.0)

    @updateHandler : AdlUpdateHandler? = nil # Stream update handler
    @updateParam : AbstractChip? = nil # Stream update parameter

    @rand : Random = Random.new(Time.utc.to_unix_ms)

    @intFrequ : Float64 = 0.0

    # Waveform table
    @wavTable : Array(Int16) = Array(Int16).new(WAVEPREC * 3, 0i16)

    # Vibrato table
    @vibTable : Array(Int32) = Array(Int32).new(VIBTAB_SIZE, 0i32)

    # Tremolo table
    @tremTable : Array(Int32) = Array(Int32).new(TREMTAB_SIZE * 2, 0i32)

    # Additional Vibrato/Tremolo tables
    @vibValConst : Array(Int32) = Array(Int32).new(BLOCKBUF_SIZE, 0i32)
    @tremValConst : Array(Int32) = Array(Int32).new(BLOCKBUF_SIZE, 0i32)

    # Key scale levels
    @ksLev : Array(Array(UInt8)) = Array(Array(UInt8)).new(8) { |_| Array(UInt8).new(16, 0u8) }

    # Vibrato/tremolo lookup tables (global, to possibly be used by all
    # operators).  Used only in the #update method.
    @vibLut : Array(Int32) = Array(Int32).new(BLOCKBUF_SIZE, 0i32)
    @tremLut : Array(Int32) = Array(Int32).new(BLOCKBUF_SIZE, 0i32)


    @opFuncs : Array(OpFunc)


    ###
    ### Methods
    ###

    @[AlwaysInline]
    protected def operatorAdvance(op : Operator, vib : Int32) : Nil
      op.wfPos = op.tCount

      # Advance waveform time
      op.tCount = op.tCount &+ op.tInc
      op.tCount = op.tCount &+ (op.tInc.to_i32! &* vib).tdiv(FIXEDPT)

      op.generatorPos = op.generatorPos &+ @generatorAdd
    end

    @[AlwaysInline]
    protected def operatorAdvanceDrums(op1 : Operator, vib1 : Int32, op2 : Operator, vib2 : Int32,
                                       op3 : Operator, vib3 : Int32) : Nil
      c1 : UInt32 = op1.tCount.tdiv(FIXEDPT)
      c3 : UInt32 = op3.tCount.tdiv(FIXEDPT)
      phasebit : UInt32 = if (((c1 & 0x88) ^ ((c1 << 5) & 0x80)) | ((c3 ^ (c3 << 2)) & 0x20)) != 0
                            0x02_u32
                          else
                            0x00u32
                          end
      noisebit : UInt32 = @rand.rand(UInt32) & 1
      snarePhaseBit : UInt32 = op1.tCount.tdiv(FIXEDPT).tdiv(0x100) & 1

      # Hihat
      inttm : UInt32 = (phasebit << 8) | (0x34 << (phasebit ^ (noisebit << 1)))
      op1.wfPos = inttm * FIXEDPT # Waveform position

      # Advance waveform time
      op1.tCount = op1.tCount &+ op1.tInc
      op1.tCount = op1.tCount &+ (op1.tInc.to_i32! * vib1).tdiv(FIXEDPT)
      op1.generatorPos = op1.generatorPos &+ @generatorAdd

      # Snare
      inttm = (((1 + snarePhaseBit) ^ noisebit) << 8).to_u32!
      op2.wfPos = inttm * FIXEDPT # Waveform position

      # Advance waveform time
      op2.tCount = op2.tCount &+ op2.tInc
      op2.tCount = op2.tCount &+ (op2.tInc.to_i32! * vib2).tdiv(FIXEDPT)
      op2.generatorPos = op2.generatorPos &+ @generatorAdd

      # Cymbal
      inttm = (1u32 + phasebit) << 8
      op3.wfPos = inttm * FIXEDPT # Waveform position

      # Advance waveform time
      op3.tCount = op3.tCount &+ op3.tInc
      op3.tCount = op3.tCount &+ (op3.tInc.to_i32! * vib3).tdiv(FIXEDPT)
      op3.generatorPos = op3.generatorPos &+ @generatorAdd
    end

    @[AlwaysInline]
    protected def operatorOutput(op : Operator, modulator : Int32, trem : Int32) : Nil
      if op.opState != OF_TYPE_OFF
        op.lastCval = op.cval
        i : UInt32 = (op.wfPos &+ modulator).tdiv(FIXEDPT)

        # wform: -16384 to 16383 (0x4000)
        # trem :  32768 to 65535 (0x10000)
        # step_amp: 0.0 to 1.0
        # vol  : 1/2^14 to 1/2^29 (/0x4000; /1../0x8000)

        op.cval = (op.stepAmp * op.vol * op.curWform[i & op.curWmask] * trem / 16.0).to_i32!
      end
    end

    protected def operatorOff(op : Operator) : Nil
      # no action, operator is off
    end

    # Output level is sustained, mode changes only when operator is turned off
    # (->release) or when the keep-sustained bit is turned off
    # (->sustain_nokeep)
    protected def operatorSustain(op : Operator) : Nil
      numStepsAdd : UInt32 = op.generatorPos.tdiv(FIXEDPT) # Number of (standardized) samples
      numStepsAdd.times { |_| op.curEnvStep += 1 }
      op.generatorPos = op.generatorPos &- (numStepsAdd * FIXEDPT)
    end

    # Operator in release mode, if output level reaches zero the operator is
    # turned off.
    protected def operatorRelease(op : Operator) : Nil
      # ??? boundary?
      if op.amp > 0.00000001
        # Release phase
        op.amp *= op.releaseMul
      end

      numStepsAdd : UInt32 = op.generatorPos.tdiv(FIXEDPT) # Number of (standardized) samples
      numStepsAdd.times do |_|
        op.curEnvStep += 1 # Sample counter
        unless Yuno.bitflag?(op.curEnvStep, op.envStepR)
          if op.amp <= 0.00000001
            # Release phase finished, turn off this operator.
            op.amp = 0.0
            op.opState = OF_TYPE_OFF if op.opState == OF_TYPE_REL
          end

          op.stepAmp = op.amp
        end
      end

      op.generatorPos = op.generatorPos &- (numStepsAdd * FIXEDPT)
    end

    # Operator in decay mode, if sustain level is reached the output level is
    # either kept (sustain level keep enabled) or the operator is switched into
    # release mode
    protected def operatorDecay(op : Operator) : Nil
      if op.amp > op.sustainLevel
        # Decay phase
        op.amp *= op.decayMul
      end

      numStepsAdd : UInt32 = op.generatorPos.tdiv(FIXEDPT) # Number of (standardized) samples
      numStepsAdd.times do |_|
        op.curEnvStep += 1
        unless Yuno.bitflag?(op.curEnvStep, op.envStepD)
          if op.amp <= op.sustainLevel
            # Decay phase finished, sustain level reached.
            if op.susKeep?
              # Keep sustain level until turned off
              op.opState = OF_TYPE_SUS
              op.amp = op.sustainLevel
            else
              # Next: release phase
              op.opState = OF_TYPE_SUS_NOKEEP
            end
          end

          op.stepAmp = op.amp
        end
      end

      op.generatorPos = op.generatorPos &- (numStepsAdd * FIXEDPT)
    end

    # Operator in attack mode, if full output level is reached, the operator is
    # switched into decay mode
    protected def operatorAttack(op : Operator) : Nil
      op.amp = ((op.a3 * op.amp + op.a2) * op.amp + op.a1) * op.amp + op.a0

      numStepsAdd : UInt32 = op.generatorPos.tdiv(FIXEDPT) # Number of (standardized) samples
      numStepsAdd.times do |_|
        op.curEnvStep += 1 # Next sample

        unless Yuno.bitflag?(op.curEnvStep, op.envStepA)
          # Check if the next step has already been reached
          if op.amp > 1.0
            # Attack phase finished.  Next: decay.
            op.opState = OF_TYPE_DEC
            op.amp = 1.0
            op.stepAmp = 1.0
          end

          op.stepSkipPosA <<= 1
          op.stepSkipPosA = 1 if op.stepSkipPosA == 0
          if Yuno.bitflag?(op.stepSkipPosA, op.envStepSkipA)
            # Check if we need to skip the next step.
            op.stepAmp = op.amp
          end
        end
      end

      op.generatorPos = op.generatorPos &- (numStepsAdd * FIXEDPT)
    end

    protected def operatorEgAttackCheck(op : Operator) : Nil
      unless Yuno.bitflag?(op.curEnvStep + 1, op.envStepA)
        # Check if the next step has already been reached
        if op.a0 >= 1.0
          # Attack phase finished, next: Decay
          op.opState = OF_TYPE_DEC
          op.amp = 1.0
          op.stepAmp = 1.0
        end
      end
    end

    protected def changeDecayRate(regBase : UInt32, op : Operator) : Nil
      decayRate : Int32 = (@adlibReg[ARC_ATTR_DECR + regBase] & 15).to_i32!

      # decayMul should be 1.0 when decayRate == 0
      if decayRate != 0
        f : Float64 = -7.4493 * DECREL_CONST[op.toff & 3] * @invSamp
        op.decayMul = FL2 ** (f * (FL2 ** (decayRate.to_f64! + (op.toff >> 2))))
        steps : Int32 = (decayRate * 4 + op.toff) >> 2
        op.envStepD = (1 << (steps <= 12 ? 12 - steps : 0)) - 1
      else
        op.decayMul = 1.0
        op.envStepD = 0
      end

      # printf("%f, %i, %i, %i\n", op.decayMul, decayRate, op.envStepD, op.toff)
    end

    protected def changeReleaseRate(regBase : UInt32, op : Operator) : Nil
      releaseRate : Int32 = (@adlibReg[ARC_SUSL_RELR + regBase] & 15).to_i32!

      # releaseMul should be 1.0 when releaseRate == 0
      if releaseRate != 0
        f : Float64 = -7.4493 * DECREL_CONST[op.toff & 3] * @invSamp
        op.releaseMul = FL2 ** (f * (FL2 ** (releaseRate.to_f64! + (op.toff >> 2))))
        steps : Int32 = (releaseRate * 4 + op.toff) >> 2
        op.envStepR = (1 << (steps <= 12 ? 12 - steps : 0)) - 1
      else
        op.releaseMul = 1.0
        op.envStepR = 0
      end
    end

    protected def changeSustainLevel(regBase : UInt32, op : Operator) : Nil
      sustainLevel : Int32 = @adlibReg[ARC_SUSL_RELR + regBase].to_i32! >> 4

      # Sustain level should be 0.0 when sustainLevel == 15 (max)
      op.sustainLevel = if sustainLevel < 15
                          FL2 ** (sustainLevel * -0.5)
                        else
                          0.0
                        end
    end

    protected def changeWaveform(regBase : UInt32, op : Operator) : Nil
      # Waveform selection
      op.curWmask = WAVEMASK[@waveSel[regBase]]
      wavIdx = WAVEFORM_START[@waveSel[regBase]]
      op.curWform = Slice(Int16).new(@wavTable.to_unsafe + wavIdx, @wavTable.size - wavIdx)
      # (might need to be adapted to waveform type here...)
    end

    protected def changeKeepSustain(regBase : UInt32, op : Operator) : Nil
      op.susKeep = (@adlibReg[ARC_TVS_KSR_MUL + regBase] & 0x20) > 0
      if op.opState == OF_TYPE_SUS
        op.opState = OF_TYPE_SUS_NOKEEP unless op.susKeep?
      elsif op.opState == OF_TYPE_SUS_NOKEEP
        op.opState = OF_TYPE_SUS if op.susKeep?
      end
    end

    # Enable/disable vibrato/tremolo LFO effects
    protected def changeVibrato(regBase : UInt32, op : Operator) : Nil
      op.vibrato = (@adlibReg[ARC_TVS_KSR_MUL + regBase] & 0x40) != 0
      op.tremolo = (@adlibReg[ARC_TVS_KSR_MUL + regBase] & 0x80) != 0
    end

    # Change amount of self-feedback
    protected def changeFeedback(chanBase : UInt32, op : Operator) : Nil
      feedback : Int32 = (@adlibReg[ARC_FEEDBACK + chanBase] & 14).to_i32!
      op.mfbi = if feedback != 0
                  (FL2 ** ((feedback >> 1) + 8.0)).to_i32!
                else
                  0
                end
    end

    protected def changeFrequency(chanBase : UInt32, regBase : UInt32, op : Operator) : Nil
      # Frequency
      frn : UInt32 = ((@adlibReg[ARC_KON_BNUM + chanBase].to_u32! & 3) << 8) +
                     @adlibReg[ARC_FREQ_NUM + chanBase].to_u32!

      # Block number/octave
      oct : UInt32 = (@adlibReg[ARC_KON_BNUM + chanBase].to_u32! >> 2) & 7
      op.freqHigh = ((frn >> 7) & 7).to_i32!

      # Keysplit
      noteSel : UInt32 = (@adlibReg[8].to_u32! >> 6) & 1
      op.toff = ((frn >> 9) & (noteSel ^ 1)) | ((frn >> 8) & noteSel)
      op.toff = op.toff &+ oct << 1

      # Envelope scaling
      unless Yuno.bitflag?(@adlibReg[ARC_TVS_KSR_MUL + regBase], 0x10)
        # puts "No key scaling for #{chanBase}, #{regBase}"
        op.toff >>= 2
      end

      # 20 + a0 + b0
      op.tInc = ((frn << oct).to_f64! * @frqMul[@adlibReg[ARC_TVS_KSR_MUL + regBase] & 15]).to_u32!

      # 40 + a0 + b0
      volIn : Float64 = (@adlibReg[ARC_KSL_OUTLEV + regBase] & 63).to_f64! +
                        KSLMUL[@adlibReg[ARC_KSL_OUTLEV + regBase] >> 6].to_f64! *
                        @ksLev[oct][frn >> 6]
      op.vol = FL2 ** (volIn * -0.125 - 14.0)

      # Operator frequency changed, care about features that depend on it.
      changeAttackRate(regBase, op)
      changeDecayRate(regBase, op)
      changeReleaseRate(regBase, op)
    end

    @[AlwaysInline]
    protected def enableOperator(regBase : UInt32, op : Operator, actType : UInt32) : Nil
      # Check if this is really an off-on transition
      if op.actState == OP_ACT_OFF
        wselBase : Int32 = regBase.to_i32!
        wselBase -= (ARC_SECONDSET - 22) if wselBase >= ARC_SECONDSET # Second set starts at 22
        op.tCount = WAVESTART[@waveSel[wselBase]] * FIXEDPT

        # Start with attack mode
        op.opState = OF_TYPE_ATT
        op.actState |= actType
      end
    end

    @[AlwaysInline]
    protected def disableOperator(op : Operator, actType : UInt32) : Nil
      # Check if this is really an on-off transition
      if op.actState != OP_ACT_OFF
        op.actState &= (~actType)
        if op.actState == OP_ACT_OFF
          op.opState = OF_TYPE_REL if op.opState != OF_TYPE_OFF
        end
      end
    end

    macro callOpFunc(opNum)
      @opFuncs.get!(@op.get!({{opNum}}).opState).not_nil!.call(@op.get!({{opNum}}))
    end

    abstract def muteMask=(mask : UInt32) : Nil
  end # module OPLDosbox
end
