#### YunoSynth
#### Copyright (C) 2023-2024 Remilia Scarlet <remilia@posteo.jp>
#### Portions based on VGMPlay, Copyright (C) Valley Bell
####
#### This program is free software: you can redistribute it and/or
#### modify it under the terms of the GNU Affero General Public
#### License as published by the Free Software Foundation, either
#### version 3 of the License, or (at your option) any later version.
####
#### This program is distributed in the hope that it will be useful,
#### but WITHOUT ANY WARRANTY; without even the implied warranty of
#### MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#### Affero General Public License for more details.
####
#### You should have received a copy of the GNU Affero General Public License
#### along with this program.  If not, see <https://www.gnu.org/licenses/>.
require "./emu-es5503-mame"

####
#### Ensoniq ES5503 "DOC" sound chip emulator interface
####

module Yuno::Chips
  # ES5503 sound chip emulator.
  class ES5503 < Yuno::AbstractChip
    CHIP_ID = 0x24_u32

    @clockFromHeader : UInt32 = 0
    @samplingMode : UInt8 = 0
    @chipSampleRate : UInt32 = 0
    @chip : ES5503Mame? = nil
    @numChannels : UInt8 = 0

    class ES5503Flags < ChipFlags
      property flags : UInt8 = 0x01u8 # Disable rear outputs is the default.
      property clockDiv : UInt32 = 1
      def initialize
      end
    end

    def initialize(chipNum : Int32, absChipCount : Int32, vgm : VgmFile, playbackSampleRate : UInt32,
                   newSamplingMode : UInt8, newPlayerSampleRate : UInt32, *, emuCore : Symbol? = nil,
                   flags : ChipFlags? = nil)
      @clockFromHeader = vgm.getAlternateChipClock(chipNum, Yuno::ChipType::Es5503) || vgm.header.es5503Clock
      @clockFromHeader &= 0x3FFFFFFF
      @playerSampleRate = newPlayerSampleRate
      @numChannels = vgm.header.es5503NumChannels
      chipCount = (vgm.header.es5503Clock & 0x40000000) != 0 ? 2 : 1
      initFields(vgm, emuCore, playbackSampleRate, newSamplingMode, chipCount, flags)
    end

    protected def initialize
    end

    protected def initFields(vgm : VgmFile, emuCore : Symbol?, playbackSampleRate : UInt32, newSamplingMode : UInt8,
                             chipCount : Int32, flags : ChipFlags?) : Nil
      @volume = vgm.getChipVolume(self, ChipType::Es5503, chipCount)
      @core = emuCore || ES5503.defaultEmuCore
      case @core
      when :mame
        @samplingMode = newSamplingMode
        @chipSampleRate = @clockFromHeader <= 0 ? playbackSampleRate : @clockFromHeader
      else raise YunoError.new("Unsupported emulation core for ES5503: #{@core}")
      end
    end

    def type : ChipType
      ChipType::Es5503
    end

    def name : String
      "Ensoniq ES5503"
    end

    def shortName : String
      "ES5503"
    end

    def id : UInt32
      CHIP_ID
    end

    def emuCore : Symbol
      @core
    end

    def self.defaultEmuCore : Symbol
      :mame
    end

    def start(chipIndex : UInt8, clock : UInt32, flags : ChipFlags? = nil, previousChip : AbstractChip? = nil) : UInt32
      case @core
      when :mame
        @chip = ES5503Mame.new(clock, @numChannels.to_i32!)
        @chip.not_nil!.setSampleRateChangeCB(->AbstractChip.changeChipSampleRate(AbstractChip, UInt32) , self)
        @chip.not_nil!.unmuteAll
        @sampleRate = @chip.not_nil!.rate # Update sample rate
        @sampleRate

      else raise YunoError.new("Unsupported emulation core for ES5503: #{@core}")
      end
    end

    def getClock : UInt32
      @clockFromHeader
    end

    def getStartFlags(vgm : VgmFile) : ChipFlags?
      nil
    end

    def update(outputs : OutputBuffers, samples : Int) : Nil
      @chip.not_nil!.update(outputs, samples.to_u32!)
    end

    def reset(chipIndex : UInt8) : Nil
      @chip.not_nil!.reset
    end

    def read(chipIndex : UInt8, offset : Int) : UInt8|UInt16|UInt32
      @chip.not_nil!.read(offset)
    end

    def write(chipIndex : UInt8, offset : Int, data : Int, port : UInt8 = 0) : Nil
      @chip.not_nil!.write(offset, data.to_u8!)
    end

    def writeDac(port : Int, command : Int, data : Int) : Nil
      Yuno.warn("ES5503 does not yet support DAC write commands")
    end

    def setMuteMask(chipIndex : UInt8, mask : UInt32) : Nil
      @chip.not_nil!.muteMask = mask
    end

    def getVolModifier : UInt32
      @volume.to_u32 * 8
    end

    def baseVolume : UInt16
      0x40_u16
    end

    @[AlwaysInline]
    def writeRam(dataStart : Int32, dataLength : Int32, data : Slice(UInt8)) : Nil
      @chip.not_nil!.writeRam(dataStart, dataLength, data)
    end

    # The internal interface for the chip.
    @[AlwaysInline]
    def chip : AbstractEmulator
      @chip.not_nil!
    end
  end
end
