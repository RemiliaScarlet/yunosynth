#### SegaPCM Emulator
#### Copyright (C) Hiromitsu Shioya, Olivier Galibert
#### Copyright (C) 2023-2024 Remilia Scarlet
#### BSD 3-Clause License

module Yuno::Chips
  class SegaPCM < Yuno::AbstractChip
    private class SegaPCMMame < Yuno::AbstractEmulator
      BANK_256 = 11
      BANK_512 = 12
      BANK_12M = 13
      BANK_MASK7  = 0x70 << 16
      BANK_MASKF  = 0xF0 << 16
      BANK_MASKF8 = 0xF8 << 16
      NUM_CHANNELS = 16

      STD_ROM_SIZE = 0x80000

      @bank : UInt32 = 0

      @ram : Bytes = Bytes.new(0x800, 0xFF_u8)
      @low : Array(UInt8) = Array(UInt8).new(NUM_CHANNELS, 0u8)
      @rom : Array(UInt8) = Array(UInt8).new(STD_ROM_SIZE, 0x80_u8)
      @bankShift : UInt8 = 0
      @bankMask : UInt32 = 0
      @rgnMask : UInt32 = 0
      @muted : Array(UInt8) = Array(UInt8).new(NUM_CHANNELS, 0u8)
      @rate : UInt32 = 0

      def initialize(clock : UInt32, @bank : UInt32)
        @bankShift = @bank.to_u8!
        mask = @bank >> 16
        mask = BANK_MASK7 >> 16 if mask == 0

        len = STD_ROM_SIZE
        @rgnMask = (len - 1).to_u32
        romMask : UInt32 = 1u32
        while romMask < len
          romMask *= 2
        end
        romMask -= 1

        @bankMask = (mask & (romMask >> @bankShift)).to_u32!

        NUM_CHANNELS.times do |msk|
          @muted[msk] = 0
        end

        @rate = clock.tdiv(128)
      end

      protected def rate
        @rate
      end

      @[AlwaysInline]
      def reset : Nil
        @ram.fill(0xFF_u8)
      end

      @[AlwaysInline]
      def write(offset : Int32, data : UInt8)
        @ram.put!(offset & 0x07FF, data)
      end

      @[AlwaysInline]
      def read(offset : Int) : UInt8
        @ram.get!(offset & 0x07FF)
      end

      def writeRom(romSize : Int32, dataStart : Int32, dataLength : Int32, romData : Slice(UInt8)) : Nil
        if @rom.size != romSize
          @rom = Array(UInt8).new(romSize, 0x80_u8)

          # Recalculate bank mask
          mask : UInt32 = @bank >> 16
          mask = BANK_MASK7.to_u32! >> 16 if mask == 0

          romMask : UInt32 = 1u32
          while romMask < romSize
            romMask *= 2
          end
          romMask -= 1

          @rgnMask = romMask
          @bankMask = mask & (romMask >> @bankShift)
        end

        return if dataStart > romSize
        dataLength = romSize - dataStart if dataStart + dataLength > romSize

        # Copy the memory
        dataLength.times do |i|
          # This can be guaranteed by the checks above
          @rom.unsafe_put(i + dataStart, romData.unsafe_fetch(i))
        end
      end

      def muteMask=(mask : UInt32) : Nil
        NUM_CHANNELS.times do |i|
          @muted = ((mask >> i) & 0x01).to_u8!
        end
      end

      @[AlwaysInline]
      def update(outputs : OutputBuffers, samples : UInt32) : Nil
        outL : Slice(Int32) = outputs[0]
        outR : Slice(Int32) = outputs[1]

        rom : Pointer(UInt8) = Pointer(UInt8).null
        rgnMask : UInt32 = @rgnMask
        addr : UInt32 = 0
        loopPos : UInt32 = 0
        finish : UInt8 = 0
        v : Int8 = 0

        # Check once, then we can avoid bounds checks below.
        if outL.size < samples || outR.size < samples
          raise "SegaPCM update: Bad number of samples (expected #{samples}, " \
                "but buffers have #{outL.size} and #{outR.size})"
        end

        outL.fill(0, 0, samples)
        outR.fill(0, 0, samples)

        # reg      function
        # ------------------------------------------------
        # 0x00     ?
        # 0x01     ?
        # 0x02     volume left
        # 0x03     volume right
        # 0x04     loop address (08-15)
        # 0x05     loop address (16-23)
        # 0x06     end address
        # 0x07     address delta
        # 0x80     ?
        # 0x81     ?
        # 0x82     ?
        # 0x83     ?
        # 0x84     current address (08-15), 00-07 is internal?
        # 0x85     current address (16-23)
        # 0x86     bit 0: channel disable?
        #          bit 1: loop disable
        #          other bits: bank
        # 0x87     ?

        # Loop over channels
        regs : Pointer(UInt8) = Pointer(UInt8).null
        NUM_CHANNELS.times do |ch|
          regs = @ram.to_unsafe + (8 * ch)

          # Only process active channels
          if !Yuno.bitflag?(regs[0x86], 1) && @muted.get!(ch) == 0
            rom = @rom.to_unsafe + ((regs[0x86].to_u32! & @bankMask) << @bankShift)
            addr = (regs[0x85].to_u32! << 16) | (regs[0x84].to_u32! << 8) | @low.get!(ch)
            loopPos = (regs[0x05].to_u32! << 16) | (regs[0x04].to_u32! << 8)
            finish = regs[6] &+ 1

            # Loop over samples on this channel
            samples.times do |i|
              v = 0i8

              # Handle looping if we've hit the end
              if (addr >> 16) == finish
                if Yuno.bitflag?(regs[0x86], 2)
                  regs[0x86] |= 1
                  break
                else
                  addr = loopPos
                end
              end

              # Fetch the sample
              v = (rom.[(addr >> 8) & rgnMask].to_i16! - 0x80).to_i8!

              # Apply panning and advance.
              # fixed Bitmask for volume multiplication, thanks to ctr -Valley Bell
              outL.put!(i, outL.get!(i) + (v.to_i32! * (regs[2] & 0x7F)))
              outR.put!(i, outR.get!(i) + (v.to_i32! * (regs[3] & 0x7F)))
              addr = (addr + regs[7].to_u32!) & 0xFFFFFF
            end

            # Store back the updated address
            regs[0x84] = (addr >> 8).to_u8!
            regs[0x85] = (addr >> 16).to_u8!
            @low.put!(ch, (Yuno.bitflag?(regs[0x86], 1) ? 0u8 : addr.to_u8!))
          end
        end
      end
    end
  end
end
