#### ************************************************************
####
####     NEC UPD7759 ADPCM Speech Processor
####     by: Juergen Buchmueller, Mike Balfour, Howie Cohen,
####         Olivier Galibert, and Aaron Giles
####     Crystal port Copyright (C) 2023-2024 Remilia Scarlet
####     BSD 3-Clause License
####
#### *************************************************************
####
####     Description:
####
####     The UPD7759 is a speech processing LSI that utilizes ADPCM to produce
####     speech or other sampled sounds.  It can directly address up to 1Mbit
####     (128k) of external data ROM, or the host CPU can control the speech
####     data transfer.  The UPD7759 is usually hooked up to a 640 kHz clock and
####     has one 8-bit input port, a start pin, a busy pin, and a clock output.
####
####     The chip is composed of 3 parts:
####     - a clock divider
####     - a rom-reading engine
####     - an adpcm engine
####     - a 4-to-9 bit adpcm converter
####
####     The clock divider takes the base 640KHz clock and divides it first
####     by a fixed divisor of 4 and then by a value between 9 and 32.  The
####     result gives a clock between 5KHz and 17.78KHz.  It's probably
####     possible, but not recommended and certainly out-of-spec, to push the
####     chip harder by reducing the divider.
####
####     The rom-reading engine reads one byte every two divided clock cycles.
####     The factor two comes from the fact that a byte has two nibbles, i.e.
####     two samples.
####
####     The apdcm engine takes bytes and interprets them as commands:
####
####         00000000                    sample end
####         00dddddd                    silence
####         01ffffff                    send the 256 following nibbles to the converter
####         10ffffff nnnnnnnn           send the n+1 following nibbles to the converter
####         11---rrr --ffffff nnnnnnnn  send the n+1 following nibbles to the converter, and repeat r+1 times
####
####     "ffffff" is sent to the clock divider to be the base clock for the
####     adpcm converter, i.e., it's the sampling rate.  If the number of
####     nibbles to send is odd the last nibble is ignored.  The commands
####     are always 8-bit aligned.
####
####     "dddddd" is the duration of the silence.  The base speed is unknown,
####     1ms sounds reasonably.  It does not seem linked to the adpcm clock
####     speed because there often is a silence before any 01 or 10 command.
####
####     The adpcm converter converts nibbles into 9-bit DAC values.  It has
####     an internal state of 4 bits that's used in conjunction with the
####     nibble to lookup which of the 256 possible steps is used.  Then
####     the state is changed according to the nibble value.  Essentially, the
####     higher the state, the bigger the steps are, and using big steps
####     increase the state.  Conversely, using small steps reduces the state.
####     This allows the engine to be a little more adaptative than a
####     classical ADPCM algorithm.
####
####     The UPD7759 can run in two modes, master (also known as standalone)
####     and slave.  The mode is selected through the "md" pin.  No known
####     game changes modes on the fly, and it's unsure if that's even
####     possible to do.
####
####
####     Master mode:
####
####     The output of the rom reader is directly connected to the adpcm
####     converter.  The controlling cpu only sends a sample number and the
####     7759 plays it.
####
####     The sample rom has a header at the beginning of the form
####
####         nn 5a a5 69 55
####
####     where nn is the number of the last sample.  This is then followed by
####     a vector of 2-bytes msb-first values, one per sample.  Multiplying
####     them by two gives the sample start offset in the rom.  A 0x00 marks
####     the end of each sample.
####
####     It seems that the UPD7759 reads at least part of the rom header at
####     startup.  Games doing rom banking are careful to reset the chip after
####     each change.
####
####
####     Slave mode:
####
####     The rom reader is completely disconnected.  The input port is
####     connected directly to the adpcm engine.  The first write to the input
####     port activates the engine (the value itself is ignored).  The engine
####     activates the clock output and waits for commands.  The clock speed
####     is unknown, but its probably a divider of 640KHz.  We use 40KHz here
####     because 80KHz crashes altbeast.  The chip probably has an internal
####     fifo to the converter and suspends the clock when the fifo is full.
####     The first command is always 0xFF.  A second 0xFF marks the end of the
####     sample and the engine stops.  OTOH, there is a 0x00 at the end too.
####     Go figure.
####
#### *************************************************************
####

module Yuno::Chips
  class Upd7759 < Yuno::AbstractChip
    # Actual implementation of the C140 emulator based on Mame's implementation
    private class Upd7759Mame < Yuno::AbstractEmulator
      FRAC_BITS = 20
      FRAC_ONE = 1u32 << FRAC_BITS
      FRAC_MASK = FRAC_ONE - 1u32

      STEP = [
        [ 0,  0,  1,  2,  3,   5,   7,  10,  0,   0,  -1,  -2,  -3,   -5,   -7,  -10 ],
        [ 0,  1,  2,  3,  4,   6,   8,  13,  0,  -1,  -2,  -3,  -4,   -6,   -8,  -13 ],
        [ 0,  1,  2,  4,  5,   7,  10,  15,  0,  -1,  -2,  -4,  -5,   -7,  -10,  -15 ],
        [ 0,  1,  3,  4,  6,   9,  13,  19,  0,  -1,  -3,  -4,  -6,   -9,  -13,  -19 ],
        [ 0,  2,  3,  5,  8,  11,  15,  23,  0,  -2,  -3,  -5,  -8,  -11,  -15,  -23 ],
        [ 0,  2,  4,  7, 10,  14,  19,  29,  0,  -2,  -4,  -7, -10,  -14,  -19,  -29 ],
        [ 0,  3,  5,  8, 12,  16,  22,  33,  0,  -3,  -5,  -8, -12,  -16,  -22,  -33 ],
        [ 1,  4,  7, 10, 15,  20,  29,  43, -1,  -4,  -7, -10, -15,  -20,  -29,  -43 ],
        [ 1,  4,  8, 13, 18,  25,  35,  53, -1,  -4,  -8, -13, -18,  -25,  -35,  -53 ],
        [ 1,  6, 10, 16, 22,  31,  43,  64, -1,  -6, -10, -16, -22,  -31,  -43,  -64 ],
        [ 2,  7, 12, 19, 27,  37,  51,  76, -2,  -7, -12, -19, -27,  -37,  -51,  -76 ],
        [ 2,  9, 16, 24, 34,  46,  64,  96, -2,  -9, -16, -24, -34,  -46,  -64,  -96 ],
        [ 3, 11, 19, 29, 41,  57,  79, 117, -3, -11, -19, -29, -41,  -57,  -79, -117 ],
        [ 4, 13, 24, 36, 50,  69,  96, 143, -4, -13, -24, -36, -50,  -69,  -96, -143 ],
        [ 4, 16, 29, 44, 62,  85, 118, 175, -4, -16, -29, -44, -62,  -85, -118, -175 ],
        [ 6, 20, 36, 54, 76, 104, 144, 214, -6, -20, -36, -54, -76, -104, -144, -214 ]
      ]

      STATE_TABLE = [ -1, -1, 0, 0, 1, 2, 2, 3, -1, -1, 0, 0, 1, 2, 2, 3 ]

      # Chip states
      alias State = UInt8
      STATE_IDLE         = 1u8
      STATE_DROP_DRQ     = 2u8
      STATE_START        = 3u8
      STATE_FIRST_REQ    = 4u8
      STATE_LAST_SAMPLE  = 5u8
      STATE_FAKE_1       = 6u8
      STATE_ADDR_MSB     = 7u8
      STATE_ADDR_LSB     = 8u8
      STATE_FAKE_2       = 9u8
      STATE_BLOCK_HEADER = 10u8
      STATE_NIBBLE_COUNT = 11u8
      STATE_NIBBLE_MSN   = 12u8
      STATE_NIBBLE_LSN   = 13u8

      ###
      ### Fields
      ###

      ##
      ## Internal clock to output sample rate mapping
      ##

      @pos  : UInt32 = 0 # Current output sample position
      @step : UInt32 = 0 # Step value per output sample

      ##
      ## I/O lines
      ##

      @fifoIn : UInt8 = 0 # Last data written to the sound chip
      @reset  : UInt8 = 0 # Current state of the RESET line
      @start  : UInt8 = 0 # Current state of the START line
      @drq    : UInt8 = 0 # Current state of the DRQ line

      ##
      ## Internal state machine
      ##

      @state            : State  = STATE_IDLE  # Current overall chip state.
      @clocksLeft       : Int32  = 0           # Number of clocks left in this state
      @nibblesLeft      : UInt16 = 0           # Number of ADPCM nibbles left to process
      @repeatCount      : UInt8  = 0           # Number of repeats remaining in current repeat block
      @postDrqState     : State  = STATE_IDLE  # State we will be in after the DRQ line is dropped
      @postDrqClocks    : Int32  = 0           # Clocks that will be left after the DRQ line is dropped
      @reqSample        : UInt8  = 0           # Requested sample number
      @lastSample       : UInt8  = 0           # Last sample number available
      @blockHeader      : UInt8  = 0           # Header byte
      @sampleRate       : UInt8  = 0           # Number of UPD clocks per ADPCM nibble
      @firstValidHeader : UInt8  = 0           # Did we get our first valid header yet?
      @offset           : UInt32 = 0           # Current ROM offset
      @repeatOffset     : UInt32 = 0           # Current ROM repeat offset

      ##
      ## ADPCM processing
      ##

      @adpcmState : UInt8 = 0 # ADPCM state index
      @adpcmData  : UInt8 = 0 # Current byte of ADPCM data
      @sample     : Int16 = 0 # Current sample value

      ##
      ## ROM access
      ##

      @rom       : Bytes = Bytes.new(0) # Pointer to ROM data; empty for secondary data
      @romBase   : Bytes = Bytes.new(0) # Pointer to ROM data; empty for secondary data
      @romOffset : UInt32 = 0   # ROM offset to make save/restore easier
      @chipMode  : UInt8  = 0   # 0 - Primary, 1 - Secondary

      # Valley Bell: Added a FIFO buffer based on Sega Pico
      @dataBuf      : Bytes = Bytes.new(0x40)
      @dbufPosRead  : UInt8 = 0
      @dbufPosWrite : UInt8 = 0

      # Remi: Adjusted clock rate
      getter rate : UInt32 = 0

      def initialize(clock : UInt32)
        @chipMode = ((clock & 0x80000000) >> 31).to_u8
        clock &= 0x7FFFFFFF

        # Compute the stepping rate based on the chip's clock speed.
        @step = 4u32 * FRAC_ONE

        # Assume /RESET and /START are both high.
        @reset = 1
        @start = 1

        reset
        @rate = clock.tdiv(4)
      end

      def reset : Nil
        @pos = 0
        @fifoIn = 0
        @drq = 0
        @state = STATE_IDLE
        @clocksLeft = 0
        @nibblesLeft = 0
        @repeatCount = 0
        @postDrqState = STATE_IDLE
        @postDrqClocks = 0
        @reqSample = 0
        @lastSample = 0
        @blockHeader = 0
        @sampleRate = 0
        @firstValidHeader = 0
        @offset = 0
        @repeatOffset = 0
        @adpcmState = 0
        @sample = 0

        # Valley Bell: reset buffer
        @dataBuf[0] = 0
        @dataBuf[1] = 0
        @dbufPosRead = 0
        @dbufPosWrite = 0

        @clocksLeft = -1 if @chipMode != 0
      end

      @[AlwaysInline]
      def write(port : UInt8, data : UInt8) : Nil
        case port
        when 0x00 then resetWrite(data)
        when 0x01 then startWrite(data)
        when 0x02 then portWrite(0, data)
        when 0x03 then setBankBase(data.to_u32! * 0x20000_u32)
        end
      end

      def writeRom(romSize : Int32, dataStart : Int32, dataLength : Int32, romData : Slice(UInt8)) : Nil
        if @rom.empty? || @rom.size != romSize
          @romBase = Bytes.new(romSize)
          @romBase.fill(0xFF_u8)
          @rom = @romBase[@romOffset..]
        end

        return if dataStart > romSize
        dataLength = romSize - dataStart if dataStart + dataLength > romSize

        # Copy the memory
        dataLength.times do |i|
          # This can be guaranteed by the checks above
          @romBase.unsafe_put(i + dataStart, romData.unsafe_fetch(i))
        end
      end

      ###
      ### Protected/Private Methods
      ###

      @[AlwaysInline]
      protected def resetWrite(data : UInt8) : Nil
        oldReset = @reset
        @reset = data != 0 ? 1u8 : 0u8
        reset if oldReset != 0 && @reset == 0
      end

      @[AlwaysInline]
      protected def startWrite(data : UInt8) : Nil
        oldStart = @start
        @start = data != 0 ? 1u8 : 0u8

        # On the rising edge, if we're idle, start going, but not if we're held in reset.
        if @state == STATE_IDLE && oldStart == 0 && @start != 0 && @reset != 0
          @state = STATE_START
          @clocksLeft = 0
        end
      end

      @[AlwaysInline]
      protected def portWrite(offset : Int, data : UInt8) : Nil
        if @chipMode == 0
          @fifoIn = data
        else
          # Valley Bell: Added FIFO buffer for secondary mode.
          @dataBuf.put!(@dbufPosWrite, data)
          @dbufPosWrite += 1
          @dbufPosWrite &= 0x3F
        end
      end

      @[AlwaysInline]
      protected def setBankBase(base : UInt32) : Nil
        @rom = @romBase[base..]
        @romOffset = base
      end

      # ADPCM sample updater.
      @[AlwaysInline]
      private def updateAdpcm(data : Int) : Nil
        @sample = @sample &+ STEP.get!(@adpcmState)[data]
        @adpcmState = (@adpcmState.to_i32! + STATE_TABLE[data]).clamp(0, 15).to_u8!
      end

      @[AlwaysInline]
      private def getFifoData : Nil
        if @dbufPosRead == @dbufPosWrite
          {% if flag?(:yunosynth_debug) %}
            Yuno.warn("uPD7759: reading empty FIFO")
          {% end %}
          return
        end

        @fifoIn = @dataBuf.get!(@dbufPosRead)
        @dbufPosRead += 1
        @dbufPosRead &= 0x3F
      end

      # Main chip state machine.
      private def advanceState : Nil
        case @state
        # Idle state: we can stick around here while there's nothing to do.
        when STATE_IDLE
          @clocksLeft = 4

        # Drop DRQ state: update to the intended state.
        when STATE_DROP_DRQ
          @drq = 0

          getFifoData if @chipMode != 0 # Secondary mode only
          @clocksLeft = @postDrqClocks
          @state = @postDrqState

        # Start state: we begin here as soon as a sample is triggered.
        when STATE_START
          @reqSample = !@rom.empty? ? @fifoIn : 0x10_u8

          # 35+ cycles after we get here, the /DRQ goes low (first byte (number
          # of samples in ROM) should be sent in response)
          #
          # (35 is the minimum number of cycles I found during heavy tests.
          # Depending on the state the chip was in just before the /MD was set
          # to 0 (reset, standby or just-finished-playing-previous-sample) this
          # number can range from 35 up to ~24000).  It also varies slightly
          # from test to test, but not much - a few cycles at most.) */
          @clocksLeft = 70 # 35 breaks Cotton
          @state = STATE_FIRST_REQ

        # First request state: issue a request for the first byte.  The expected
        # response will be the index of the last sample
        when STATE_FIRST_REQ
          @drq = 1

          # 44 cycles later, we will latch this value and request another byte
          @clocksLeft = 44
          @state = STATE_LAST_SAMPLE

        # Last sample state: latch the last sample value and issue a request for
        # the second byte.  The second byte read will be fake.
        when STATE_LAST_SAMPLE
          @lastSample = @rom.empty? ? @fifoIn : @rom.get!(0)
          @drq = 1

          # 28 cycles later, we will latch this value and request another byte
          @clocksLeft = 28 # 28 - Breaks Cotton
          @state = @reqSample > @lastSample ? STATE_IDLE : STATE_FAKE_1

        # First fake state: ignore any data here and issue a request for the
        # third byte.  The expected response will be the MSB of the sample
        # address.
        when STATE_FAKE_1
          @drq = 1

          # 32 cycles later, we will latch this value and request another byte
          @clocksLeft = 32
          @state = STATE_ADDR_MSB

        # Address MSB state: latch the MSB of the sample address and issue a
        # request for the fourth byte.  The expected response will be the LSB of
        # the sample address.
        when STATE_ADDR_MSB
          @offset = (@rom.empty? ? @fifoIn : @rom[@reqSample * 2 + 5]).to_u32! << 9
          @drq = 1

          # 44 cycles later, we will latch this value and request another byte
          @clocksLeft = 44
          @state = STATE_ADDR_LSB

        # Address LSB state: latch the LSB of the sample address and issue a
        # request for the fifth byte.  The expected response will be fake.
        when STATE_ADDR_LSB
          @offset |= (@rom.empty? ? @fifoIn : @rom[@reqSample * 2 + 6]).to_u32! << 1
          @drq = 1

          # 36 cycles later, we will latch this value and request another byte
          @clocksLeft = 36
          @state = STATE_FAKE_2

        # Second fake state: ignore any data here and issue a request for the
        # the sixth byte.  The expected response will be the first block header.
        when STATE_FAKE_2
          @offset += 1
          @firstValidHeader = 0
          @drq = 1

          # 36?? cycles later, we will latch this value and request another byte.
          @clocksLeft = 36
          @state = STATE_BLOCK_HEADER

        # Block header state: latch the header and issue a request for the first
        # byte afterwards
        when STATE_BLOCK_HEADER
          if @repeatCount != 0
            @repeatCount -= 1
            @offset = @repeatOffset
          end

          if @rom.empty?
            @blockHeader = @fifoIn
          else
            @blockHeader = @rom[@offset & 0x1FFFF]
            @offset += 1
          end

          @drq = 1

          # Our next step depends on the top two bits.
          case @blockHeader & 0xC0
          when 0 # Silence
            @clocksLeft = 1024 * ((@blockHeader & 0x3F) + 1)
            @state = (@blockHeader == 0 && @firstValidHeader != 0) ? STATE_IDLE : STATE_BLOCK_HEADER
            @sample = 0
            @adpcmState = 0

          when 0x40 # 256 nibbles
            @sampleRate = (@blockHeader & 0x3F) + 1
            @nibblesLeft = 256_u16
            @clocksLeft = 36 # Just a guess
            @state = STATE_NIBBLE_MSN

          when 0x80 # n nibbles
            @sampleRate = (@blockHeader & 0x3F) + 1
            @clocksLeft = 36 # Just a guess
            @state = STATE_NIBBLE_COUNT

          when 0xC0 # Repeat loop
            @repeatCount = (@blockHeader & 7) + 1
            @repeatOffset = @offset
            @clocksLeft = 36 # Just a guess
            @state = STATE_BLOCK_HEADER
          end

          # Set a flag when we get the first non-zero header
          @firstValidHeader = 1 if @blockHeader != 0

        # Nibble count state: latch the number of nibbles to play and request
        # another byte.  The expected response will be the first data byte.
        when STATE_NIBBLE_COUNT
          if @rom.empty?
            @nibblesLeft = @fifoIn.to_u16!
          else
            @nibblesLeft = @rom[@offset & 0x1FFFF].to_u16!
            @offset += 1
          end
          @nibblesLeft += 1
          @drq = 1

          # 36?? cycles later, we will latch this value and request another byte.
          @clocksLeft = 36 # Just a guess
          @state = STATE_NIBBLE_MSN

        # MSN state: latch the data for this pair of samples and request another
        # byte.  The expected response will be the next sample data or another
        # header.
        when STATE_NIBBLE_MSN
          if @rom.empty?
            @adpcmData = @fifoIn
          else
            @adpcmData = @rom[@offset & 0x1FFFF]
            @offset += 1
          end

          updateAdpcm(@adpcmData >> 4)
          @drq = 1

          # We stay in this state until the time for this sample is complete.
          @clocksLeft = @sampleRate.to_i32! * 4
          @nibblesLeft -= 1
          if @nibblesLeft == 0
            @state = STATE_BLOCK_HEADER
          else
            @state = STATE_NIBBLE_LSN
          end

        # LSN state: process the lower nibble.
        when STATE_NIBBLE_LSN
          updateAdpcm(@adpcmData & 15)

          # We stay in this state until the time for this sample is complete.
          @clocksLeft = @sampleRate.to_i32! * 4
          @nibblesLeft -= 1
          if @nibblesLeft == 0
            @state = STATE_BLOCK_HEADER
          else
            @state = STATE_NIBBLE_MSN
          end

        else
          raise YunoError.new("Invalid uPD7759 state: #{@state}")
        end

        # If there's a DRQ, fudge the state.
        if @drq != 0
          @postDrqState = @state
          @postDrqClocks = @clocksLeft - 21
          @state = STATE_DROP_DRQ
          @clocksLeft = 21
        end
      end

      def update(outputs : OutputBuffers, samples : UInt32) : Nil
        clocksLeft : Int32 = @clocksLeft
        sample : Int16 = @sample
        step : UInt32 = @step
        pos : UInt32 = @pos
        clocksThisTime : UInt32 = 0
        outL : Slice(Int32) = outputs[0]
        outR : Slice(Int32) = outputs[1]
        outLPos = 0
        outRPos = 0

        # Check once, then we can avoid bounds checks below.
        if outL.size < samples || outR.size < samples
          raise "uPD7759 update: Bad number of samples (expected #{samples}, " \
                "but buffers have #{outL.size} and #{outR.size})"
        end

        # Loop until done
        unless @state == STATE_IDLE
          until samples == 0
            # Store the current sample
            outL.unsafe_put(outLPos, sample.to_i32! << 7)
            outR.unsafe_put(outRPos, sample.to_i32! << 7)
            outLPos += 1
            outRPos += 1
            samples -= 1

            # Advance the number of clocks/output sample
            pos = pos &+ step

            # Handle clocks, but only in standalone mode.
            if @chipMode == 0
              while !@rom.empty? && pos >= FRAC_ONE
                clocksThisTime = pos >> FRAC_BITS
                clocksThisTime = clocksLeft.to_u32! if clocksThisTime > clocksLeft

                # Clock once
                pos -= clocksThisTime * FRAC_ONE
                clocksLeft -= clocksThisTime

                # If we're out of clocks, time to handle the next state
                if clocksLeft == 0
                  # Advance one state; if we hit Idle, bail.
                  advanceState
                  break if @state == STATE_IDLE

                  # Reimport the variables that we cached
                  clocksLeft = @clocksLeft
                  sample = @sample
                end
              end
            else
              if clocksLeft == 0
                advanceState
                clocksLeft = @clocksLeft
              end

              # Advance the state (34x because of Clock Divider / 4)
              4.times do |_|
                clocksLeft -= 1
                if clocksLeft == 0
                  advanceState
                  clocksLeft = @clocksLeft
                end
              end
            end
          end
        end

        # If we got out early, just zap the rest of the buffer.
        if samples != 0
          outL.fill(0, outLPos, samples)
          outR.fill(0, outRPos, samples)
        end

        # Flush the state back
        @clocksLeft = clocksLeft
        @pos = pos
      end
    end
  end
end
