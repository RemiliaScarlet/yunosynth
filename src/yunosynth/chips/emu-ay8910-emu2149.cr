#### emu2149
#### https://github.com/digital-sound-antiques/emu2149
#### Copyright (C) 2001-2022 Mitsutaka Okazaki
#### Copyright (C) 2023-2024 Remilia Scarlet
####
#### Based on emu2149.c by Mitsutaka Okazaki, ported to Crystal by Remilia
#### Scarlet.  MIT License.
####
#### This source refers to the following documents. The author would like to thank all the authors who have
#### contributed to the writing of them.
#### - psg.vhd        -- 2000 written by Kazuhiro Tsujikawa.
#### - s_fme7.c       -- 1999,2000 written by Mamiya (NEZplug).
#### - ay8910.c       -- 1998-2001 Author unknown (MAME).
#### - MSX-Datapack   -- 1991 ASCII Corp.
#### - AY-3-8910 data sheet

module Yuno::Chips
  # Ay8910 sound chip emulator.
  class Ay8910 < Yuno::AbstractChip
    # :nodoc:
    class Ay8910Emu2149 < Yuno::AbstractEmulator
      ###
      ### Actual implementation of the Ay8910 emulator based on emu2149's implementation.
      ###

      VOL_DEFAULT = 1
      VOL_YM2149 = 0
      VOL_AY_3_8910 = 1
      ZX_STEREO = 0x80
      GETA_BITS = 24
      NUM_REGISTERS = 32

      VOL_TABLE = [
        # YM2149 - 32 steps
        [0x00, 0x01, 0x01, 0x02, 0x02, 0x03, 0x03, 0x04, 0x05, 0x06, 0x07, 0x09,
         0x0B, 0x0D, 0x0F, 0x12, 0x16, 0x1A, 0x1F, 0x25, 0x2D, 0x35, 0x3F, 0x4C,
         0x5A, 0x6A, 0x7F, 0x97, 0xB4, 0xD6, 0xEB, 0xFF],

        # AY-3-8910 - 16 steps
        [0x00, 0x00, 0x01, 0x01, 0x02, 0x02, 0x03, 0x03, 0x05, 0x05, 0x07, 0x07,
         0x0B, 0x0B, 0x0F, 0x0F, 0x16, 0x16, 0x1F, 0x1F, 0x2D, 0x2D, 0x3F, 0x3F,
         0x5A, 0x5A, 0x7F, 0x7F, 0xB4, 0xB4, 0xFF, 0xFF]
      ]

      # Volume Table
      @volumeTable : Array(Int32) = VOL_TABLE[VOL_DEFAULT]

      @registers : Array(UInt8) = Array(UInt8).new(NUM_REGISTERS, 0u8)
      @output : Int32 = 0

      @clock : UInt32 = 0
      getter rate : UInt32 = 0
      @baseIncr : UInt32 = 0
      @quality : UInt32 = 0

      @count : Array(UInt32) = Array(UInt32).new(3, 0u32)
      @volume : Array(UInt32) = Array(UInt32).new(3, 0u32)
      @freq : Array(UInt32) = Array(UInt32).new(3, 0u32)
      @edge : Array(UInt32) = Array(UInt32).new(3, 0u32)
      @tmask : Array(UInt32) = Array(UInt32).new(3, 0u32)
      @nmask : Array(UInt32) = Array(UInt32).new(3, 0u32)
      @mask : UInt32 = 0
      @stereoMask : Array(UInt32) = Array(UInt32).new(3, 0u32)

      ##
      ## TODO Remi: I get the feeling that a lot of these can be converted to
      ## UInt8.  They were UInt8s in earlier revisions of my ported code, but
      ## were changed to UInt32 while attempting to debug this emulator.
      ## Further testing once all the bugs are ironed out of this should tell us
      ## if we can go back to UInt8s and save some memory.
      ##

      @baseCount : UInt32 = 0

      @envPtr : UInt32 = 0
      @envFace : UInt32 = 0

      @envContinue : UInt32 = 0
      @envAttack : UInt32 = 0
      @envAlternate : UInt32 = 0
      @envHold : UInt32 = 0
      @envPause : UInt32 = 0

      @envFreq : UInt32 = 0
      @envCount : UInt32 = 0

      @noiseSeed : UInt32 = 0
      @noiseCount : UInt32 = 0
      @noiseFreq : UInt32 = 0

      # rate converter
      @realStep : UInt32 = 0
      @psgTime : UInt32 = 0
      @psgStep : UInt32 = 0

      # I/O Ctrl
      @addr : UInt32 = 0

      # output of channels
      @chOut : Array(Int32) = Array(Int32).new(3, 0i32)

      # Internal buffers for output
      @prev : Int32 = 0
      @next : Int32 = 0
      @updateBuffer : Slice(Int32) = Slice(Int32).new(2, 0)
      @sprev : Slice(Int32) = Slice(Int32).new(2, 0)
      @snext : Slice(Int32) = Slice(Int32).new(2, 0)

      property sampleRateFunc : SampleRateCallback?
      property sampleRateData : AbstractChip?

      def initialize(newClock : UInt32, newRate : UInt32) : Nil
        self.volumeMode = VOL_DEFAULT
        @clock = newClock
        @rate = (newRate != 0 ? newRate : 44100_u32)
        self.quality = 0
        self.mask = 0x00
        @stereoMask[0] = 0x03_u32
        @stereoMask[1] = 0x03_u32
        @stereoMask[2] = 0x03_u32
      end

      @[AlwaysInline]
      def flags=(value : UInt8) : Nil
        if Yuno.bitflag?(value, ZX_STEREO)
          @stereoMask[0] = 0x01_u32
          @stereoMask[1] = 0x03_u32
          @stereoMask[2] = 0x02_u32
        else
          @stereoMask[0] = 0x03_u32
          @stereoMask[1] = 0x03_u32
          @stereoMask[2] = 0x03_u32
        end
      end

      private macro psgMaskCh(x)
        (1 << {{x}})
      end

      @[AlwaysInline]
      private def internalRefresh : Nil
        if @quality != 0
          @baseIncr = 1u32 << GETA_BITS
          @realStep = ((1u32 << 31) / @rate).to_u32!
          @psgStep = ((1u32 << 31) / (@clock / 8)).to_u32!
          @psgTime = 0
        else
          @baseIncr = (@clock.to_f64! * (1 << GETA_BITS) / (8.0 * @rate)).to_u32!
        end
      end

      @[AlwaysInline]
      def clock=(value : UInt32) : Nil
        @clock = value
        internalRefresh
      end

      @[AlwaysInline]
      def rate=(value : UInt32) : Nil
        newRate = (value != 0 ? value : 44100_u32)
        @rate = newRate
        internalRefresh
      end

      @[AlwaysInline]
      def quality=(value : UInt8) : Nil
        @quality = value.to_u32!
        internalRefresh
      end

      @[AlwaysInline]
      def volumeMode=(typ : Int) : Nil
        case typ
        when 1 then @volumeTable = VOL_TABLE[VOL_YM2149]
        when 2 then @volumeTable = VOL_TABLE[VOL_AY_3_8910]
        else @volumeTable = VOL_TABLE[VOL_DEFAULT]
        end
      end

      @[AlwaysInline]
      def mask=(value : UInt32) : UInt32
        ret = @mask
        @mask = value
        ret
      end

      @[AlwaysInline]
      def stereoMask=(value : UInt32) : Nil
        @stereoMask[0] =  value       & 3
        @stereoMask[1] = (value >> 2) & 3
        @stereoMask[2] = (value >> 4) & 3
      end

      def reset : Nil
        @baseCount = 0

        3.times do |i|
          @count[i] = 0x1000
          @freq[i] = 0
          @edge[i] = 0
          @volume[i] = 0
          @chOut[i] = 0
        end

        @mask = 0

        16.times { |i| @registers[i] = 0 }
        @addr = 0

        @noiseSeed = 0xFFFF_u32
        @noiseCount = 0x40
        @noiseFreq = 0

        @envPtr = 0
        @envFreq = 0
        @envCount = 0
        @envPause = 1

        @output = 0
      end

      @[AlwaysInline]
      def readIO : UInt8
        @registers[@addr]
      end

      @[AlwaysInline]
      def readReg(regNum : UInt32) : UInt8
        @registers[regNum & 0x1F]
      end

      @[AlwaysInline]
      def writeReg(regNum : UInt32, value : UInt32) : Nil
        return if regNum > 15

        @registers[regNum] = (value & 0xFF).to_u8!

        case regNum
        when 0, 2, 4, 1, 3, 5
          c : UInt32 = regNum >> 1
          @freq[c] = ((@registers[c * 2 + 1] & 15).to_u32! << 8) + @registers[c * 2].to_u32!

        when 6
          @noiseFreq = (value == 0 ? 1_u32 : ((value & 31) << 1))

        when 7
          @tmask[0] = (value & 1)
          @tmask[1] = (value & 2)
          @tmask[2] = (value & 4)
          @nmask[0] = (value & 8)
          @nmask[1] = (value & 16)
          @nmask[2] = (value & 32)

        when 8, 9, 10
          @volume[regNum - 8] = value << 1

        when 11, 12
          @envFreq = (@registers[12].to_u32! << 8) + @registers[11].to_u32!

        when 13
          @envContinue = (value >> 3) & 1
          @envAttack = (value >> 2) & 1
          @envAlternate = (value >> 1) & 1
          @envHold = value & 1
          @envFace = @envAttack
          @envPause = 0
          @envCount = 0x10000_u32 - @envFreq
          @envPtr = (@envFace != 0 ? 0_u32 : 0x1F_u32)
        end
      end

      @[AlwaysInline]
      def writeIO(addr : UInt32, value : UInt32) : Nil
        if Yuno.bitflag?(addr, 1)
          writeReg(@addr, value)
        else
          @addr = value & 0x1F
        end
      end

      @[AlwaysInline]
      private def updateStereo(outputs : Slice(Int32)) : Nil
        noise : UInt32 = 0
        incr : UInt32 = 0
        l : Int32 = 0
        r : Int32 = 0

        {% if flag?(:yunosynth_debug) %}
          unless outputs.size == 2
            raise "Bad size of outputs for AY-1-8910"
          end
        {% end %}

        @baseCount += @baseIncr
        incr = @baseCount >> GETA_BITS
        @baseCount &= (1u32 << GETA_BITS) - 1u32

        # Envelope
        @envCount += incr
        while @envCount >= 0x10000 && @envFreq != 0
          if @envPause == 0
            if @envFace != 0
              @envPtr = (@envPtr + 1) & 0x3F
            else
              @envPtr = (@envPtr + 0x3F) & 0x3F
            end
          end

          if Yuno.bitflag?(@envPtr, 0x20) # If carry or borrow
            if @envContinue != 0
              @envFace ^= 1 if (@envAlternate ^ @envHold) != 0
              @envPause = 1 if @envHold != 0
              @envPtr = (@envFace != 0 ? 0_u32 : 0x1F_u32)
            else
              @envPause = 1
              @envPtr = 0
            end
          end

          @envCount -= @envFreq
        end

        # Noise
        @noiseCount = @noiseCount &+ incr
        if Yuno.bitflag?(@noiseCount, 0x40)
          @noiseSeed ^= 0x24000 if Yuno.bitflag?(@noiseSeed, 1)
          @noiseSeed >>= 1
          @noiseCount = @noiseCount &- @noiseFreq
        end
        noise = @noiseSeed & 1

        #Tone
        3.times do |i|
          @count.unsafe_put(i, @count.unsafe_fetch(i) &+ incr)

          if Yuno.bitflag?(@count.unsafe_fetch(i), 0x1000)
            if @freq.unsafe_fetch(i) > 1
              @edge.unsafe_put(i, (@edge.unsafe_fetch(i) == 0 ? 1_u32 : 0_u32))
              @count.unsafe_put(i, @count.unsafe_fetch(i) - @freq.unsafe_fetch(i))
            else
              @edge.unsafe_put(i, 1)
            end
          end

          @chOut.unsafe_put(i, 0)
          next if Yuno.bitflag?(@mask, psgMaskCh(i))

          if (@tmask.unsafe_fetch(i) != 0 || @edge.unsafe_fetch(i) != 0) && (@nmask.unsafe_fetch(i) != 0 || noise != 0)
            unless Yuno.bitflag?(@volume.unsafe_fetch(i), 32)
              @chOut.unsafe_put(i, @volumeTable[@volume.unsafe_fetch(i) & 31])
            else
              @chOut.unsafe_put(i, @volumeTable[@envPtr])
            end

            l += @chOut.unsafe_fetch(i) if Yuno.bitflag?(@stereoMask.unsafe_fetch(i), 0x01)
            r += @chOut.unsafe_fetch(i) if Yuno.bitflag?(@stereoMask.unsafe_fetch(i), 0x02)
          end
        end

        outputs.unsafe_put(0, l << 5)
        outputs.unsafe_put(1, r << 5)
      end

      @[AlwaysInline]
      def update(outputs : OutputBuffers, samples : UInt32) : Nil
        outL : Slice(Int32) = outputs[0]
        outR : Slice(Int32) = outputs[1]
        @updateBuffer.fill(0)

        # Check once, then we can avoid bounds checks below.
        if outL.size < samples || outR.size < samples
          raise "AY-1-8910 update: Bad number of samples (expected #{samples}, " \
                "but buffers have #{outL.size} and #{outR.size})"
        end

        if @quality == 0
          samples.times do |i|
            updateStereo(@updateBuffer)
            outL.unsafe_put(i, @updateBuffer.unsafe_fetch(0))
            outR.unsafe_put(i, @updateBuffer.unsafe_fetch(1))
          end
        else
          samples.times do |i|
            while @realStep > @psgTime
              @psgTime += @psgStep
              @sprev.unsafe_put(0, @snext.unsafe_fetch(0))
              @sprev.unsafe_put(1, @snext.unsafe_fetch(1))
              updateStereo(@snext)
            end

            @psgTime -= @realStep
            stepTimeDiff = @psgStep - @psgTime
            outL.unsafe_put(i, ((@snext.unsafe_fetch(0).to_f64! * stepTimeDiff +
                                 @sprev.unsafe_fetch(0).to_f64! * @psgTime) /
                                @psgStep).to_i32!)
            outR.unsafe_put(i, ((@snext.unsafe_fetch(1).to_f64! * stepTimeDiff +
                                 @sprev.unsafe_fetch(1).to_f64! * @psgTime) /
                                @psgStep).to_i32!)
          end
        end
      end
    end
  end
end
