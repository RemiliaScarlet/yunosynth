#### YunoSynth
#### Copyright (C) 2023-2024 Remilia Scarlet <remilia@posteo.jp>
#### Portions based on VGMPlay, Copyright (C) Valley Bell
####
#### This program is free software: you can redistribute it and/or
#### modify it under the terms of the GNU Affero General Public
#### License as published by the Free Software Foundation, either
#### version 3 of the License, or (at your option) any later version.
####
#### This program is distributed in the hope that it will be useful,
#### but WITHOUT ANY WARRANTY; without even the implied warranty of
#### MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#### Affero General Public License for more details.
####
#### You should have received a copy of the GNU Affero General Public License
#### along with this program.  If not, see <https://www.gnu.org/licenses/>.
require "./emu-dmg-mame"

####
#### Nintendo GameBoy sound chip emulator interface
####

module Yuno::Chips
  # Nintendo GameBoy sound chip emulator interface
  class DMG < Yuno::AbstractChip
    CHIP_ID = 0x13_u32

    class DMGFlags < ChipFlags
      property flags : UInt8 = 3u8 # Set to 0x0003u8 to boost the wave channel

      def initialize
      end
    end

    @clockFromHeader : UInt32 = 0
    @samplingMode : UInt8 = 0
    @chipSampleRate : UInt32 = 0
    @chip : DMGMame? = nil

    def initialize(chipNum : Int32, absChipCount : Int32, vgm : VgmFile, playbackSampleRate : UInt32,
                   newSamplingMode : UInt8, newPlayerSampleRate : UInt32, *, emuCore : Symbol? = nil,
                   flags : ChipFlags? = nil)
      @clockFromHeader = vgm.getAlternateChipClock(chipNum, Yuno::ChipType::Dmg) || vgm.header.dmgClock
      @clockFromHeader &= 0x3FFFFFFF
      @playerSampleRate = newPlayerSampleRate
      chipCount = (vgm.header.dmgClock & 0x40000000) != 0 ? 2 : 1
      initFields(vgm, emuCore, playbackSampleRate, newSamplingMode, chipCount, flags)
    end

    protected def initialize
    end

    protected def initFields(vgm : VgmFile, emuCore : Symbol?, playbackSampleRate : UInt32, newSamplingMode : UInt8,
                             chipCount : Int32, flags : ChipFlags?) : Nil
      @volume = vgm.getChipVolume(self, ChipType::Dmg, chipCount)
      @core = emuCore || DMG.defaultEmuCore
      case @core
      when :mame
        @samplingMode = newSamplingMode
        @chipSampleRate = @clockFromHeader <= 0 ? playbackSampleRate : @clockFromHeader
      else raise YunoError.new("Unsupported emulation core for DMG: #{@core}")
      end
    end

    def type : ChipType
      ChipType::Dmg
    end

    def name : String
      "Nintendo GameBoy"
    end

    def shortName : String
      "GameBoy"
    end

    def id : UInt32
      CHIP_ID
    end

    def emuCore : Symbol
      @core
    end

    def self.defaultEmuCore : Symbol
      :mame
    end

    def start(chipIndex : UInt8, clock : UInt32, flags : ChipFlags? = nil, previousChip : AbstractChip? = nil) : UInt32
      case @core
      when :mame
        raise "DMG requires flags to start" unless flags && flags.is_a?(DMGFlags)

        rate = (clock & 0x7FFFFFFF).tdiv(64)
        if (Yuno.bitflag?(@samplingMode, 1) && rate < @chipSampleRate) || @samplingMode == 2
          rate = @chipSampleRate
        end

        mode = Yuno.bitflag?(clock, 0x80000000) ? DMGMame::Mode::GameBoyColor : DMGMame::Mode::GameBoy

        @chip = DMGMame.new(clock, rate, mode)
        @chip.not_nil!.options = flags.flags
        @sampleRate = rate # Update sample rate
        @sampleRate

      else raise YunoError.new("Unsupported emulation core for DMG: #{@core}")
      end
    end

    def getClock : UInt32
      @clockFromHeader
    end

    def getStartFlags(vgm : VgmFile) : ChipFlags?
      DMGFlags.new
    end

    def update(outputs : OutputBuffers, samples : Int) : Nil
      @chip.not_nil!.update(outputs, samples.to_u32!)
    end

    def reset(chipIndex : UInt8) : Nil
      @chip.not_nil!.reset
    end

    def read(chipIndex : UInt8, offset : Int) : UInt8|UInt16|UInt32
      raise "Cannot do a straight read on DMG"
    end

    def write(chipIndex : UInt8, offset : Int, data : Int, port : UInt8 = 0) : Nil
      case @core
      when :mame
        @chip.not_nil!.soundWrite(offset, data.to_u8!)
      end
    end

    def setMuteMask(chipIndex : UInt8, mask : UInt32) : Nil
      @chip.not_nil!.muteMask = mask
    end

    def getVolModifier : UInt32
      @volume.to_u32 * 2
    end

    def baseVolume : UInt16
      0xC0_u16
    end

    # The internal interface for the chip.
    @[AlwaysInline]
    def chip : AbstractEmulator
      @chip.not_nil!
    end
  end
end
