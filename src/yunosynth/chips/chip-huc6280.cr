#### YunoSynth
#### Copyright (C) 2023-2024 Remilia Scarlet <remilia@posteo.jp>
#### Portions based on VGMPlay, Copyright (C) Valley Bell
####
#### This program is free software: you can redistribute it and/or
#### modify it under the terms of the GNU Affero General Public
#### License as published by the Free Software Foundation, either
#### version 3 of the License, or (at your option) any later version.
####
#### This program is distributed in the hope that it will be useful,
#### but WITHOUT ANY WARRANTY; without even the implied warranty of
#### MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#### Affero General Public License for more details.
####
#### You should have received a copy of the GNU Affero General Public License
#### along with this program.  If not, see <https://www.gnu.org/licenses/>.
require "./emu-huc6280-mame"

####
#### Hudson C6280 sound chip emulator interface
####

module Yuno::Chips
  # HuC6280 sound chip emulator.
  class HuC6280 < Yuno::AbstractChip
    CHIP_ID = 0x1B_u32

    enum Core
      Mame
      Ootake

      @[AlwaysInline]
      def symbol : Symbol
        case self
        in .mame? then :mame
        in .ootake? then :ootake
        end
      end
    end

    @clockFromHeader : UInt32 = 0
    @samplingMode : UInt8 = 0
    @chipSampleRate : UInt32 = 0
    @chip : HuC6280Mame|HuC6280Ootake|Nil = nil

    def initialize(chipNum : Int32, absChipCount : Int32, vgm : VgmFile, playbackSampleRate : UInt32,
                   newSamplingMode : UInt8, newPlayerSampleRate : UInt32, *, emuCore : Core|Symbol|Nil = nil,
                   flags : ChipFlags? = nil)
      @clockFromHeader = vgm.getAlternateChipClock(chipNum, Yuno::ChipType::Huc6280) || vgm.header.huc6280Clock
      @clockFromHeader &= 0x3FFFFFFF
      @playerSampleRate = newPlayerSampleRate
      chipCount = (vgm.header.huc6280Clock & 0x40000000) != 0 ? 2 : 1
      initFields(vgm, emuCore, playbackSampleRate, newSamplingMode, chipCount)
    end

    protected def initialize
    end

    protected def initFields(vgm : VgmFile, emuCore : Core|Symbol|Nil, playbackSampleRate : UInt32,
                             newSamplingMode : UInt8, chipCount : Int32) : Nil
      @volume = vgm.getChipVolume(self, ChipType::Huc6280, chipCount)
      @core = case emuCore
              when Core then emuCore.symbol
              when Symbol then emuCore
              else HuC6280.defaultEmuCore
              end
      case @core
      when :mame, :ootake
        @samplingMode = newSamplingMode
        @chipSampleRate = @clockFromHeader <= 0 ? playbackSampleRate : @clockFromHeader
      else raise YunoError.new("Unsupported emulation core for HuC6280: #{@core}")
      end
    end

    def type : ChipType
      ChipType::Huc6280
    end

    def name : String
      "Hudson HuC6280"
    end

    def shortName : String
      "HuC6280"
    end

    def id : UInt32
      CHIP_ID
    end

    def emuCore : Symbol
      @core
    end

    def self.defaultEmuCore : Symbol
      :ootake
    end

    def start(chipIndex : UInt8, clock : UInt32, flags : ChipFlags? = nil, previousChip : AbstractChip? = nil) : UInt32
      case @core
      when :ootake
        @sampleRate = @playerSampleRate
        @chip = HuC6280Ootake.new(clock.to_i32, @sampleRate.to_i32)

      when :mame
        # Update sample rate
        @sampleRate = (clock & 0x7FFFFFFF).tdiv(16)
        if (Yuno.bitflag?(@samplingMode, 0x01) && @sampleRate < @chipSampleRate) || @samplingMode == 0x02
          @sampleRate = @chipSampleRate
        end

        @chip = HuC6280Mame.new((clock & 0x7FFFFFFF).to_f64!, @sampleRate.to_f64!)
        @chip.as(HuC6280Mame).unmuteAll

      else raise YunoError.new("Unsupported emulation core for HuC6280: #{@core}")
      end

      @sampleRate
    end

    def getClock : UInt32
      @clockFromHeader
    end

    def getStartFlags(vgm : VgmFile) : ChipFlags?
      nil
    end

    def update(outputs : OutputBuffers, samples : Int) : Nil
      @chip.not_nil!.update(outputs, samples.to_u32!)
    end

    def reset(chipIndex : UInt8) : Nil
      case ch = @chip
      when HuC6280Ootake, HuC6280Mame then ch.reset
      else raise "Unexpected emulator type"
      end
    end

    def read(chipIndex : UInt8, offset : Int) : UInt8|UInt16|UInt32
      case ch = @chip
      when HuC6280Ootake, HuC6280Mame then ch.read(offset)
      else raise "Unexpected emulator type"
      end
    end

    def write(chipIndex : UInt8, offset : Int, data : Int, port : UInt8 = 0) : Nil
      case ch = @chip
      when HuC6280Ootake, HuC6280Mame then ch.write(offset, data.to_u8!)
      else raise "Unexpected emulator type"
      end
    end

    def writeDac(port : Int, command : Int, data : Int) : Nil
      write(command, data, port.to_u8!)
    end

    def setMuteMask(chipIndex : UInt8, mask : UInt32) : Nil
      @chip.not_nil!.muteMask = mask
    end

    def getVolModifier : UInt32
      @volume.to_u32!
    end

    def baseVolume : UInt16
      256_u16
    end

    # The internal interface for the chip.
    @[AlwaysInline]
    def chip : AbstractEmulator
      @chip.not_nil!
    end
  end
end
