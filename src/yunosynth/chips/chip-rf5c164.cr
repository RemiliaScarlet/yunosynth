#### YunoSynth
#### Copyright (C) 2023-2024 Remilia Scarlet <remilia@posteo.jp>
#### Portions based on VGMPlay, Copyright (C) Valley Bell
####
#### This program is free software: you can redistribute it and/or
#### modify it under the terms of the GNU Affero General Public
#### License as published by the Free Software Foundation, either
#### version 3 of the License, or (at your option) any later version.
####
#### This program is distributed in the hope that it will be useful,
#### but WITHOUT ANY WARRANTY; without even the implied warranty of
#### MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#### Affero General Public License for more details.
####
#### You should have received a copy of the GNU Affero General Public License
#### along with this program.  If not, see <https://www.gnu.org/licenses/>.
require "./emu-rf5c164-gens"

####
#### Ricoh RF5C164 sound chip emulator interface
####

module Yuno::Chips
  # RF5C164 sound chip emulator.
  class Rf5c164 < Yuno::AbstractChip
    CHIP_ID = 0x10_u32

    @clockFromHeader : UInt32 = 0
    @samplingMode : UInt8 = 0
    @chipSampleRate : UInt32 = 0
    @chip : Rf5c164Gens? = nil

    def initialize(chipNum : Int32, absChipCount : Int32, vgm : VgmFile, playbackSampleRate : UInt32,
                   newSamplingMode : UInt8, newPlayerSampleRate : UInt32, *, emuCore : Symbol? = nil,
                   flags : ChipFlags? = nil)
      @clockFromHeader = vgm.getAlternateChipClock(chipNum, Yuno::ChipType::Rf5c164) || vgm.header.rf5c164Clock
      @clockFromHeader &= 0xBFFFFFFF # Hack for Cosmic Fantasy Stories
      @playerSampleRate = newPlayerSampleRate
      chipCount = (vgm.header.rf5c164Clock & 0x40000000) != 0 ? 2 : 1
      initFields(vgm, emuCore, playbackSampleRate, newSamplingMode, chipCount, flags)
    end

    protected def initialize
    end

    protected def initFields(vgm : VgmFile, emuCore : Symbol?, playbackSampleRate : UInt32, newSamplingMode : UInt8,
                             chipCount : Int32, flags : ChipFlags?) : Nil
      @volume = vgm.getChipVolume(self, ChipType::Rf5c164, chipCount)
      @core = emuCore || Rf5c164.defaultEmuCore
      case @core
      when :gens
        @samplingMode = newSamplingMode
        @chipSampleRate = @clockFromHeader <= 0 ? playbackSampleRate : @clockFromHeader
      else raise YunoError.new("Unsupported emulation core for RF5C164: #{@core}")
      end
    end

    def type : ChipType
      ChipType::Rf5c164
    end

    def name : String
      "Richo RF5C164"
    end

    def shortName : String
      "RF5C164"
    end

    def id : UInt32
      CHIP_ID
    end

    def emuCore : Symbol
      @core
    end

    def self.defaultEmuCore : Symbol
      :gens
    end

    def start(chipIndex : UInt8, clock : UInt32, flags : ChipFlags? = nil, previousChip : AbstractChip? = nil) : UInt32
      case @core
      when :gens
        @sampleRate = (clock & 0x7FFFFFFF).tdiv(384)
        if (Yuno.bitflag?(@samplingMode, 0x01) && @sampleRate < @chipSampleRate) || @samplingMode == 0x02
          @sampleRate = @chipSampleRate
        end

        @chip = Rf5c164Gens.new(@sampleRate)
        @chip.not_nil!.smpl0Patch = ((clock & 0x80000000) >> 31).to_i32!
        @sampleRate

      else raise YunoError.new("Unsupported emulation core for RF5C164: #{@core}")
      end
    end

    def getClock : UInt32
      @clockFromHeader
    end

    def getStartFlags(vgm : VgmFile) : ChipFlags?
    end

    def update(outputs : OutputBuffers, samples : Int) : Nil
      @chip.not_nil!.update(outputs, samples.to_u32!)
    end

    def reset(chipIndex : UInt8) : Nil
      @chip.not_nil!.reset
    end

    def read(chipIndex : UInt8, offset : Int) : UInt8|UInt16|UInt32
      Yuno.warn("RF5C164 does not support reads")
      0u8
    end

    def write(chipIndex : UInt8, offset : Int, data : Int, port : UInt8 = 0) : Nil
      @chip.not_nil!.write(offset.to_u32!, data.to_u32!)
    end

    def memWrite(offset : Int16, data : UInt8) : Nil
      @chip.not_nil!.memWrite(offset, data)
    end

    def writeDac(port : Int, command : Int, data : Int) : Nil
      write(command, data, port.to_u8!)
    end

    def setMuteMask(chipIndex : UInt8, mask : UInt32) : Nil
      @chip.not_nil!.muteMask = mask
    end

    def getVolModifier : UInt32
      @volume.to_u32 * 2
    end

    def baseVolume : UInt16
      0x100_u16
    end

    @[AlwaysInline]
    def writeRam(dataStart : Int32, dataLength : Int32, ramData : Slice(UInt8)) : Nil
      @chip.not_nil!.writeRam(dataStart, dataLength, ramData)
    end

    # The internal interface for the chip.
    @[AlwaysInline]
    def chip : AbstractEmulator
      @chip.not_nil!
    end
  end
end
