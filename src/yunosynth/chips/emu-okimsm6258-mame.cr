#### OKI MSM6258 ADPCM Emulator
#### Copyright (C) Barry Rodewald
#### Copyright (C) 2023-2024 Remilia Scarlet
#### BSD 3-Clause License

module Yuno::Chips
  class OKIMSM6258 < Yuno::AbstractChip
    private class OKIMSM6258Mame < Yuno::AbstractEmulator
      FOSC_DIV_BY_1024 = 0
      FOSC_DIV_BY_768 = 1
      FOSC_DIV_BY_512 = 2
      TYPE_3BITS = 0
      TYPE_4BITS = 1
      OUTPUT_10BITS = 0
      OUTPUT_12BITS = 1

      COMMAND_STOP = 1
      COMMAND_PLAY = 1 << 1
      COMMAND_RECORD = 1 << 2
      STATUS_PLAYING = 1 << 1
      STATUS_RECORDING = 1 << 2
      QUEUE_SIZE = 1 << 1
      QUEUE_MASK = QUEUE_SIZE - 1

      DIVIDERS = [ 1024u32, 768u32, 512u32, 512u32 ]

      INDEX_SHIFT = [ -1, -1, -1, -1, 2, 4, 6, 8 ]

      NIBBLE_TO_BIT = [
        [ 1, 0, 0, 0], [ 1, 0, 0, 1], [ 1, 0, 1, 0], [ 1, 0, 1, 1],
        [ 1, 1, 0, 0], [ 1, 1, 0, 1], [ 1, 1, 1, 0], [ 1, 1, 1, 1],
        [-1, 0, 0, 0], [-1, 0, 0, 1], [-1, 0, 1, 0], [-1, 0, 1, 1],
        [-1, 1, 0, 0], [-1, 1, 0, 1], [-1, 1, 1, 0], [-1, 1, 1, 1]
      ]

      @status : UInt8 = 0
      @masterClock : UInt32
      @divider : UInt32

      # Selects 3-bit or 4-bit ADPCM
      @adpcmType : UInt8

      # ADPCM data-in register
      @dataIn : UInt8 = 0

      # Nibble select
      @nibbleShift : UInt8 = 0

      @outputBits : UInt8
      @outputMask : Int32

      @dataBuf : Bytes = Bytes.new(8)
      @dataInLast : UInt8 = 0
      @dataBufPos : UInt8 = 0

      # Data Empty Values:
      #   00 - data written, but not read yet
      #   01 - read data, waiting for next write
      #   02 - tried to read, but had no data
      @dataEmpty : UInt8 = 0xFF_u8

      @pan : UInt8 = 0
      @lastSample : Int32 = 0

      @signal : Int32 = -2
      @step : Int32 = 0

      @clockBuffer : Bytes
      @initialClock : UInt32
      @initialDiv : UInt8

      @sampleRateFunc : SampleRateCallback? = nil
      @sampleRateData : AbstractChip? = nil

      @diffLookup : Array(Int32) = Array(Int32).new(49 * 16, 0)

      @@internal10Bit : UInt8 = 0
      protected class_property internal10Bit

      def initialize(clock : UInt32, divider : UInt8, @adpcmType : UInt8, output12Bits : Int)
        computeTables

        @initialClock = clock
        @initialDiv = divider
        @masterClock = clock

        @clockBuffer = Bytes.new(4)
        @clockBuffer[0] =  (clock & 0x000000FF).to_u8!
        @clockBuffer[1] = ((clock & 0x0000FF00) >> 8).to_u8!
        @clockBuffer[2] = ((clock & 0x00FF0000) >> 16).to_u8!
        @clockBuffer[3] = ((clock & 0xFF000000) >> 24).to_u8!

        @outputBits = output12Bits != 0 ? 12u8 : 10u8
        @outputMask = if @@internal10Bit != 0
                        (1 << (@outputBits - 1))
                      else
                        (1 << (12 - 1))
                      end

        @divider = DIVIDERS[divider]
      end

      def rate : UInt32
        getVClock
      end

      def reset : Nil
        @masterClock = @initialClock
        @clockBuffer[0] =  (@initialClock & 0x000000FF).to_u8!
        @clockBuffer[1] = ((@initialClock & 0x0000FF00) >> 8).to_u8!
        @clockBuffer[2] = ((@initialClock & 0x00FF0000) >> 16).to_u8!
        @clockBuffer[3] = ((@initialClock & 0xFF000000) >> 24).to_u8!
        @divider = DIVIDERS[@initialDiv]

        @sampleRateFunc.try &.call(@sampleRateData.not_nil!, getVClock)

        @signal = -2
        @step = 0
        @status = 0

        # Valley Bell: Add reset of the Data In register.
        @dataIn = 0
        @dataBuf[0] = 0
        @dataBuf[1] = 0
        @dataBufPos = 0
        @dataEmpty = 0xFF
        @pan = 0
      end

      def divider=(value : Int) : Nil
        @divider = DIVIDERS[value]
        @sampleRateFunc.try &.call(@sampleRateData.not_nil!, getVClock)
      end

      def clock=(value : UInt32) : Nil
        if value != 0
          @masterClock = value
        else
          @masterClock =  @clockBuffer[0].to_u32! |
                         (@clockBuffer[1].to_u32! <<  8) |
                         (@clockBuffer[2].to_u32! << 16) |
                         (@clockBuffer[3].to_u32! << 24)
        end

        @sampleRateFunc.try &.call(@sampleRateData.not_nil!, getVClock)
      end

      @[AlwaysInline]
      def getVClock : UInt32
        clockRand : UInt32 = @masterClock
        clockRand += @divider.tdiv(2) # For better rounding
        clockRand.tdiv(@divider)
      end

      @[AlwaysInline]
      def write(port : UInt8, data : UInt8) : Nil
        case port
        when 0x00 then controlWrite(data)
        when 0x01 then dataWrite(data)
        when 0x02 then panWrite(data)
        when 0x08, 0x09, 0x0A then setClockByte(port & 0x03, data)
        when 0x0B
          setClockByte(port & 0x03, data)
          self.clock = 0
        when 0x0C then self.divider = data
        end
      end

      @[AlwaysInline]
      def options=(value : UInt16) : Nil
        @@internal10Bit = (value & 0x01).to_u8!
      end

      @[AlwaysInline]
      def setSampleRateChangeCB(func : SampleRateCallback, data : AbstractChip) : Nil
        @sampleRateFunc = func
        @sampleRateData = data
      end

      def update(outputs : OutputBuffers, samples : UInt32) : Nil
        outL : Slice(Int32) = outputs[0]
        outR : Slice(Int32) = outputs[1]

        if Yuno.bitflag?(@status, STATUS_PLAYING)
          nibbleShift : UInt8 = @nibbleShift
          bufPos : Int32 = 0
          nibble : Int32 = 0
          sample : Int16 = 0

          # Check once, then we can avoid bounds checks below.
          if outL.size < samples || outR.size < samples
            raise "OKI MSM6258 update: Bad number of samples (expected #{samples}, " \
                  "but buffers have #{outL.size} and #{outR.size})"
          end

          while samples > 0
            # Compute the new amplitude and update the current step.
            if nibbleShift == 0
              # First nibble: get data
              if @dataEmpty == 0
                @dataIn = @dataBuf.get!(@dataBufPos >> 4)
                @dataBufPos += 0x10
                @dataBufPos &= 0x7F
                @dataEmpty += 1 if (@dataBufPos >> 4) == (@dataBufPos & 0x0F)
              else
                @dataEmpty += 1 if @dataEmpty < 0x80
              end
            end

            nibble = (@dataIn.to_i32! >> nibbleShift) & 0xF

            # Output to the buffer
            if @dataEmpty < 0x02
              sample = clockAdpcm(nibble.to_u8!)
              @lastSample = sample.to_i32!
            else
              # Valley Bell: data_empty behaviour (loosely) ported from XM6
              if @dataEmpty >= 3
                @dataEmpty -= 1
                @signal = (@signal * 15).tdiv(16)
                @lastSample = @signal << 4
              end

              sample = @lastSample.to_i16!
            end

            nibbleShift ^= 4

            outL.unsafe_put(bufPos, (Yuno.bitflag?(@pan, 2) ? 0x00 : sample.to_i32!))
            outR.unsafe_put(bufPos, (Yuno.bitflag?(@pan, 1) ? 0x00 : sample.to_i32!))
            bufPos += 1
            samples -= 1
          end

          # Update the parameters
          @nibbleShift = nibbleShift
        else
          # Fill with zeros
          outL.fill(0, 0, samples)
          outR.fill(0, 0, samples)
        end
      end

      @[AlwaysInline]
      private def dataWrite(data : UInt8) : Nil
        @dataBufPos = 0 if @dataEmpty >= 0x02
        @dataInLast = data
        @dataBuf.put!(@dataBufPos & 0x0F, data)
        @dataBufPos += 1
        @dataBufPos &= 0xF7

        if (@dataBufPos >> 4) == (@dataBufPos & 0x0F)
          {% if flag?(:yunosynth_debug) %}
            Yuno.warn("OKI MSM6258: FIFO full")
          {% end %}
          @dataBufPos = (@dataBufPos & 0xF0) | ((@dataBufPos &- 1) & 0x07)
        end

        @dataEmpty = 0
      end

      @[AlwaysInline]
      private def controlWrite(data : UInt8) : Nil
        if Yuno.bitflag?(data, COMMAND_STOP)
          @status &= ~(STATUS_PLAYING | STATUS_RECORDING)
          return
        end

        if Yuno.bitflag?(data, COMMAND_PLAY)
          unless Yuno.bitflag?(@status, STATUS_PLAYING)
            @status |= STATUS_PLAYING

            # Also reset the ADPCM parameters
            @signal = -2 # Note: XM6 lets this fade to 0 when nothing is going on
            @step = 0
            @nibbleShift = 0
            @dataBuf.put!(0, data)
            @dataBufPos = 1 # Write pos 1, read pos 0
            @dataEmpty = 0
          end

          @step = 0 # This line was verified with the source of XM6
          @nibbleShift = 0
        else
          @status &= ~STATUS_PLAYING
        end

        if Yuno.bitflag?(data, COMMAND_RECORD)
          RemiLib.log.error("OKI MSM6258: Recording bit enabled, but recording not supported")
          @status |= STATUS_RECORDING
        else
          @status &+ ~STATUS_RECORDING
        end
      end

      @[AlwaysInline]
      private def setClockByte(byte : UInt8, val : UInt8) : Nil
        {% if flag?(:yunosynth_wd40) %}
          @clockBuffer.put!(byte, val)
        {% else %}
          @clockBuffer[byte] = val
        {% end %}
      end

      @[AlwaysInline]
      private def panWrite(data : UInt8) : Nil
        @pan = data
      end

      private def computeTables : Nil
        stepVal : Int32 = 0

        # Loop over all possible steps
        49.times do |step|
          # Compute the step value.
          stepVal = (16.0 * ((11.0 / 10.0) ** step)).floor.to_i32!

          # Loop over all nibbles and compute the difference.
          16.times do |nib|
            @diffLookup[step * 16 + nib] = NIBBLE_TO_BIT[nib][0] *
                                           (stepVal      * NIBBLE_TO_BIT[nib][1] +
                                            stepVal.tdiv(2) * NIBBLE_TO_BIT[nib][2] +
                                            stepVal.tdiv(4) * NIBBLE_TO_BIT[nib][3] +
                                            stepVal.tdiv(8))
          end
        end
      end

      @[AlwaysInline]
      private def clockAdpcm(nibble : UInt8) : Int16
        {% begin %}
          max : Int32 = @outputMask - 1
          min : Int32 = -@outputMask

          # Original MAME algorithm (causes a DC offset over time).
          # @signal += @diffLookup[@step * 16 + (nibble & 15)]

          # Awesome algorithm ported from XM6 - it works PERFECTLY.
          {% if flag?(:yunosynth_wd40) %}
            sample : Int32 = @diffLookup.get!(@step * 16 + (nibble & 15))
          {% else %}
            sample : Int32 = @diffLookup[@step * 16 + (nibble & 15)]
          {% end %}
          @signal = ((sample << 8) + (@signal * 245)) >> 8

        # Clamp to min/max
        @signal = @signal.clamp(min, max)

        # Adjust step size and clamp.
        @step += INDEX_SHIFT.get!(nibble & 7)
        @step = @step.clamp(0, 48)

        # Return the signal scaled up to 32767
        (@signal << 4).to_i16!
        {% end %}
      end
    end
  end
end
