#### Mednafen - Multi-system Emulator
#### Crystal port Copyright (C) 2023-2024 Remilia Scarlet
####
#### This program is free software; you can redistribute it and/or modify it
#### under the terms of the GNU General Public License as published by the Free
#### Software Foundation; either version 2 of the License, or (at your option)
#### any later version.
####
#### This program is distributed in the hope that it will be useful, but WITHOUT
#### ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
#### more details.
####
#### You should have received a copy of the GNU General Public License along
#### with this program; if not, write to the Free Software Foundation, Inc., 59
#### Temple Place, Suite 330, Boston, MA 02111-1307 USA
####

module Yuno::Chips
  class VsuVue < Yuno::AbstractChip
    # Actual implementation of the VsuVue emulator based on Mednafen's
    # implementation
    private class VsuVueMednafen < Yuno::AbstractEmulator
      NUM_CHANNELS = 6

      TAP_LUT = [ 15 - 1, 11 - 1, 14 - 1, 5 - 1, 9 - 1, 7 - 1, 10 - 1, 12 - 1 ]

      @intlControl : Array(UInt8) = Array(UInt8).new(NUM_CHANNELS, 0u8)
      @leftLevel : Array(UInt8) = Array(UInt8).new(NUM_CHANNELS, 0u8)
      @rightLevel : Array(UInt8) = Array(UInt8).new(NUM_CHANNELS, 0u8)
      @frequency : Array(UInt16) = Array(UInt16).new(NUM_CHANNELS, 0u16)
      @envControl : Array(UInt16) = Array(UInt16).new(NUM_CHANNELS, 0u16) # Channel 5/67 extra functionality tacked
                                                                          # on, too

      @ramAddress : Array(UInt8) = Array(UInt8).new(NUM_CHANNELS, 0u8)
      @sweepControl : UInt8 = 0u8
      @waveData : Array(Array(UInt8))
      @modData : Array(UInt8) = Array(UInt8).new(0x20, 0u8)

      @effFreq : Array(Int32) = Array(Int32).new(NUM_CHANNELS, 0i32)
      @envelope : Array(Int32) = Array(Int32).new(NUM_CHANNELS, 0i32)

      @wavePos : Array(Int32) = Array(Int32).new(NUM_CHANNELS, 0i32)
      @modWavePos : Int32 = 0

      @latcherClockDivider : Array(Int32) = Array(Int32).new(NUM_CHANNELS, 120i32)

      @freqCounter : Array(Int32) = Array(Int32).new(NUM_CHANNELS, 0i32)
      @intervalCounter : Array(Int32) = Array(Int32).new(NUM_CHANNELS, 0i32)
      @envelopeCounter : Array(Int32) = Array(Int32).new(NUM_CHANNELS, 0i32)
      @sweepModCounter : Int32 = 0

      @effectsClockDivider : Array(Int32) = Array(Int32).new(NUM_CHANNELS, 4800i32)
      @intervalClockDivider : Array(Int32) = Array(Int32).new(NUM_CHANNELS, 4i32)
      @envelopeClockDivider : Array(Int32) = Array(Int32).new(NUM_CHANNELS, 4i32)
      @sweepModClockDivider : Int32 = 1

      @noiseLatcherClockDivider : Int32 = 120
      @noiseLatcher : UInt32 = 0u32

      @lfsr : UInt32 = 0u32

      @lastTs : Int32 = 0

      @clock : UInt32 = 0
      getter sampleRate : UInt32 = 0
      @muted : Array(UInt8) = Array(UInt8).new(NUM_CHANNELS, 0u8)
      @tmSmpl : Int32 = 0
      @tmClock : Int32 = 0

      def initialize(@clock : UInt32, @sampleRate : UInt32)
        @waveData = Array(Array(UInt8)).new(5) do |_|
          Array(UInt8).new(0x20, 0u8)
        end
      end

      def reset : Nil
        Yuno.dlog!("Resetting")
        @sweepControl = 0
        @sweepModCounter = 0
        @sweepModClockDivider = 1

        NUM_CHANNELS.times do |i|
          @intlControl[i] = 0
          @leftLevel[i] = 0
          @rightLevel[i] = 0
          @frequency[i] = 0
          @envControl[i] = 0
          @ramAddress[i] = 0

          @effFreq[i] = 0
          @envelope[i] = 0
          @wavePos[i] = 0
          @freqCounter[i] = 0
          @intervalCounter[i] = 0
          @envelopeCounter[i] = 0

          @effectsClockDivider[i] = 4800
          @intervalClockDivider[i] = 4
          @envelopeClockDivider[i] = 4

          @latcherClockDivider[i] = 120
        end

        @noiseLatcherClockDivider = 120
        @noiseLatcher = 0

        @waveData.each(&.fill(0u8))
        @modData.fill(0u8)

        @lastTs = 0
        @tmSmpl = 0
        @tmClock = 0
      end

      def muteMask=(mask : UInt32) : Nil
        NUM_CHANNELS.times do |ch|
          @muted[ch] = ((mask >> ch) & 0x01).to_u8!
        end
      end

      def unmuteAll : Nil
        @muted.fill(0u8)
      end

      @[AlwaysInline]
      def write(addr : UInt32, val : UInt8) : Nil
        addr <<= 2
        addr &= 0x7FF
        #Yuno.dlog!(sprintf("VSU Write: %08x = %02x", addr, val))

        case
        when addr < 0x280
          @waveData[addr >> 7][(addr >> 2) & 0x1F] = val & 0x3F

        when addr < 0x400
          @modData[(addr >> 2) & 0x1F] = val

        when addr < 0x600
          ch : UInt32 = (addr >> 6) & 0xF

          if ch > 5
            if addr == 0x580 && Yuno.bitflag?(val, 1)
              Yuno.dlog!("STOP, HAMMER TIME")
              NUM_CHANNELS.times { |i| @intlControl.put!(i, @intlControl.get!(i) & ~0x80) }
            end
          else
            # At this point, we know ch is <= 4, which is always within the number of channels.
            case (addr >> 2) & 0xF
            when 0
              @intlControl.put!(ch, val & ~0x40)
              #Yuno.dlog!("@intlControl[ch] = val & ~0x40: #{@intlControl[ch]} = #{val & ~0x40} (ch = #{ch})")

              if Yuno.bitflag?(val, 0x80)
                @effFreq.put!(ch, @frequency.get!(ch).to_i32!)
                if ch == 5
                  @freqCounter.put!(ch, 10 * (2048 - @effFreq.get!(ch)))
                else
                  @freqCounter.put!(ch, 2048 - @effFreq.get!(ch))
                end
                @intervalCounter.put!(ch, ((val & 0x1F) + 1).to_i32!)
                @envelopeCounter.put!(ch, ((@envControl.get!(ch) & 0x7) + 1).to_i32!)

                if ch == 4
                  @sweepModCounter = ((@sweepControl >> 4) & 7).to_i32!
                  @sweepModClockDivider = Yuno.bitflag?(@sweepControl, 0x80) ? 8 : 1
                  @modWavePos = 0
                end

                @wavePos.put!(ch, 0)
                @lfsr = 1 if ch == 5

                @effectsClockDivider.put!(ch, 4800)
                @intervalClockDivider.put!(ch, 4)
                @envelopeClockDivider.put!(ch, 4)
              end

            when 1
              @leftLevel.put!(ch, (val >> 4) & 0xF)
              @rightLevel.put!(ch, val & 0xF)

            when 2
              @frequency.put!(ch, @frequency.get!(ch) & 0xFF00)
              @frequency.put!(ch, @frequency.get!(ch) | val)
              @effFreq.put!(ch, @effFreq.get!(ch) & 0xFF00)
              @effFreq.put!(ch, @effFreq.get!(ch) | val)

            when 3
              @frequency.put!(ch, @frequency.get!(ch) & 0x00FF)
              @frequency.put!(ch, @frequency.get!(ch) | (val & 0x7).to_u16! << 8)
              @effFreq.put!(ch, @effFreq.get!(ch) & 0x00FF)
              @effFreq.put!(ch, @effFreq.get!(ch) | (val & 0x7).to_i32! << 8)

            when 4
              @envControl.put!(ch, @envControl.get!(ch) & 0xFF00)
              @envControl.put!(ch, @envControl.get!(ch) | val)
              @envelope.put!(ch, ((val >> 4) & 0xF).to_i32!)

            when 5
               @envControl.put!(ch, @envControl.get!(ch) & 0x00FF)
              case ch
              when 4 then @envControl.put!(ch, @envControl.get!(ch) | (val & 0x73).to_u16! << 8)
              when 5 then @envControl.put!(ch, @envControl.get!(ch) | (val & 0x73).to_u16! << 8)
              else @envControl.put!(ch, @envControl.get!(ch) | (val & 0x03).to_u16! << 8)
              end

            when 6
              @ramAddress.put!(ch, val & 0xF)

            when 7
              @sweepControl = val if ch == 4
            end
          end
        end
      end

      @[AlwaysInline]
      private def calcCurrentOutput(ch : Int, left : Pointer(Int32), right : Pointer(Int32)) : Nil
        if !Yuno.bitflag?(@intlControl[ch], 0x80) || @muted.get!(ch) != 0
          left.value = 0
          right.value = 0
          return
        end

        wd : Int32 = if ch == 5
                       @noiseLatcher.to_i32!
                     else
                       if @ramAddress.get!(ch) > 4
                         0x20
                       else
                         @waveData[@ramAddress.get!(ch)][@wavePos.get!(ch)].to_i32!
                       end
                     end

        leftOL : Int32 = @envelope.get!(ch) * @leftLevel.get!(ch)
        if leftOL != 0
          leftOL >>= 3
          leftOL += 1
        end

        rightOL : Int32 = @envelope.get!(ch) * @rightLevel.get!(ch)
        if rightOL != 0
          rightOL >>= 3
          rightOL += 1
        end

        wd -= 0x20
        left.value += wd * leftOL
        right.value += wd * rightOL
      end

      private def updateChip(timestamp : Int32, outL : Pointer(Int32), outR : Pointer(Int32)) : Nil
        clocks : Int32 = 0
        chunkClocks : Int32 = 0
        feedback : UInt32 = 0
        delta : Int32 = 0
        newFreq : Int32 = 0

        outL.value = 0
        outR.value = 0

        NUM_CHANNELS.times do |ch|
          clocks = timestamp - @lastTs

          # Skip muted channels
          next if !Yuno.bitflag?(@intlControl.get!(ch), 0x80) || @muted.get!(ch) != 0

          while clocks > 0
            chunkClocks = clocks

            if chunkClocks > @effectsClockDivider.get!(ch)
              chunkClocks = @effectsClockDivider.get!(ch)
            end

            if ch == 5
              if chunkClocks > @noiseLatcherClockDivider
                chunkClocks = @noiseLatcherClockDivider
              end
            else
              if @effFreq.get!(ch) >= 2040
                if chunkClocks > @latcherClockDivider.get!(ch)
                  chunkClocks = @latcherClockDivider.get!(ch)
                end
              else
                if chunkClocks > @freqCounter.get!(ch)
                  chunkClocks = @freqCounter.get!(ch)
                end
              end
            end

            if ch == 5 && chunkClocks > @noiseLatcherClockDivider
              chunkClocks = @noiseLatcherClockDivider
            end

            @freqCounter.decf!(ch, chunkClocks)
            while @freqCounter.get!(ch) <= 0
              if ch == 5
                feedback = ((@lfsr >> 7) & 1) ^ ((@lfsr >> TAP_LUT[(@envControl.get!(5) >> 12) & 0x7]) & 1)
                @lfsr = ((@lfsr << 1) & 0x7FFF) | feedback
                @freqCounter.incf!(ch, 10 * (2048 - @effFreq.get!(ch)))
              else
                @freqCounter.incf!(ch, 2048 - @effFreq.get!(ch))
                @wavePos.put!(ch, (@wavePos.get!(ch) + 1) & 0x1F)
              end
            end

            @latcherClockDivider.decf!(ch, chunkClocks)
            while @latcherClockDivider.get!(ch) <= 0
              @latcherClockDivider.incf!(ch, 120)
            end

            if ch == 5
              @noiseLatcherClockDivider -= chunkClocks
              if @noiseLatcherClockDivider == 0
                @noiseLatcherClockDivider = 120
                @noiseLatcher = ((@lfsr & 1) << 6) - (@lfsr & 1)
              end
            end

            @effectsClockDivider.decf!(ch, chunkClocks)
            while @effectsClockDivider.get!(ch) <= 0
              @effectsClockDivider.incf!(ch, 4800)

              @intervalClockDivider.decf!(ch)
              while @intervalClockDivider.get!(ch) <= 0
                @intervalClockDivider.incf!(ch, 4)

                if Yuno.bitflag?(@intlControl.get!(ch), 0x20)
                  @intervalCounter.decf!(ch)
                  if @intervalCounter.get!(ch) == 0
                    @intlControl.put!(ch, @intlControl.get!(ch) & ~0x80)
                  end
                end

                @envelopeClockDivider.decf!(ch)
                while @envelopeClockDivider.get!(ch) <= 0
                  @envelopeClockDivider.incf!(ch, 4)

                  if Yuno.bitflag?(@envControl.get!(ch), 0x0100) # Envelope enabled?
                    @envelopeCounter.decf!(ch)
                    if @envelopeCounter.get!(ch) == 0
                      @envelopeCounter.put!(ch, (@envControl.get!(ch) & 0x7).to_i32! + 1)

                      if Yuno.bitflag?(@envControl.get!(ch), 0x0008)
                        # Grow
                        if @envelope.get!(ch) < 0xF || Yuno.bitflag?(@envControl.get!(ch), 0x200)
                          @envelope.put!(ch, (@envelope.get!(ch) + 1) & 0xF)
                        end
                      else
                        # Decay
                        if @envelope.get!(ch) > 0 || Yuno.bitflag?(@envControl.get!(ch), 0x200)
                          @envelope.put!(ch, (@envelope.get!(ch) - 1) & 0xF)
                        end
                      end
                    end
                  end
                end # while @envelopeClockDivider.get!(ch) <= 0
              end # while @intervalClockDivider.get!(ch) <= 0

              if ch == 4
                @sweepModClockDivider -= 1
                while @sweepModClockDivider <= 0
                  @sweepModClockDivider += (Yuno.bitflag?(@sweepControl, 0x80) ? 8 : 1)

                  if Yuno.bitflag?(@sweepControl >> 4, 0x7) && Yuno.bitflag?(@envControl.get!(ch), 0x4000)
                    @sweepModCounter -= 1 if @sweepModCounter != 0

                    if @sweepModCounter == 0
                      @sweepModCounter = (@sweepControl >> 4).to_i32! & 0x7

                      if Yuno.bitflag?(@envControl.get!(ch), 0x1000)
                        # Modulation
                        if @modWavePos < 32 || Yuno.bitflag?(@envControl.get!(ch), 0x2000)
                          @modWavePos &= 0x1F
                          @effFreq.put!(ch, @effFreq.get!(ch) + @modData.get!(@modWavePos).to_i8!)
                          @effFreq.put!(ch, @effFreq.get!(ch).clamp(0, 0x7FF))
                          @modWavePos += 1
                        end
                      else
                        # Sweep
                        delta = @effFreq.get!(ch) >> (@sweepControl & 0x7)
                        newFreq = @effFreq.get!(ch) + (Yuno.bitflag?(@sweepControl, 0x8) ? delta : -delta)

                        if newFreq < 0
                          @effFreq.put!(ch, 0)
                        elsif newFreq > 0x7FF
                          @intlControl.put!(ch, @intlControl.get!(ch) & ~0x80)
                        else
                          @effFreq.put!(ch, newFreq)
                        end
                      end
                    end
                  end
                end # while @sweepModClockDivider <= 0
              end # if ch == 4
            end # while @effectsClockDivider.get!(ch) <= 0

            clocks -= chunkClocks
          end # while clocks > 0

          calcCurrentOutput(ch, outL, outR)
        end # NUM_CHANNELS.times

        @lastTs = timestamp
      end

      def update(outputs : OutputBuffers, samples : UInt32) : Nil
        outL : Slice(Int32) = outputs[0]
        outR : Slice(Int32) = outputs[1]

        # Check once, then we can avoid bounds checks below.
        if outL.size < samples || outR.size < samples
          raise "VSU-VUE update: Bad number of samples (expected #{samples}, " \
                "but buffers have #{outL.size} and #{outR.size})"
        end

        samples.times do |curSmp|
          @tmSmpl += 1
          @tmClock = (@tmSmpl.to_i64! * @clock).tdiv(@sampleRate).to_i32!

          updateChip(@tmClock, outL.to_unsafe + curSmp, outR.to_unsafe + curSmp)
          if @lastTs >= @clock
            @lastTs -= @clock
            @tmClock -= @clock
            @tmSmpl -= @sampleRate
          end

          # Volume per channel: 0x1F (envelope/volume) * 0x3F (unsigned sample) = 0x7A1 (~0x800)
          # I turned the samples into signed format (-0x20..0x1F), so the used range is +-0x400.
          # 16-bit values are up to 0x8000
          # 0x8000 / 0x400 / 6 = 5.33 (possible boost without clipping)
          # Because music usually doesn't use the maximum volume (SFX do), I boost by 2^3 = 8.
          outL.put!(curSmp, outL.get!(curSmp) << 3)
          outR.put!(curSmp, outR.get!(curSmp) << 3)
        end
      end
    end
  end
end
