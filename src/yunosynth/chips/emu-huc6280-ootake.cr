#### Ootake PSG
####   ＰＳＧを記述するのに必要な定義および関数のプロトタイプ宣言を行ないます．
#### Copyright (C) 2004 Ki
#### Copyright(C) 2006-2012 Kitao Nakamura
#### Copyright (C) 2023-2024 Remilia Scarlet
####
#### This program is free software; you can redistribute it and/or modify it
#### under the terms of the GNU General Public License as published by the Free
#### Software Foundation; either version 2 of the License, or (at your option)
#### any later version.
####
#### This program is distributed in the hope that it will be useful, but WITHOUT
#### ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
#### more details.
####
#### You should have received a copy of the GNU General Public License along
#### with this program; if not, write to the Free Software Foundation, Inc., 59
#### Temple Place, Suite 330, Boston, MA 02111-1307 USA
####
#### ・キューの参照処理をシンプルにした。テンポの安定性および音質の向上。
#### ・オーバーサンプリングしないようにした。（筆者の主観もあるが、PSGの場合、響きの
####   美しさが損なわれてしまうケースが多いため。速度的にもアップ）
#### ・ノイズの音質・音量を実機並みに調整した。v0.72
#### ・ノイズの周波数に0x1Fが書き込まれたときは、0x1Eと同じ周波数で音量を半分にして
####   鳴らすようにした。v0.68
#### ・現状は再生サンプルレートは44.1KHz固定とした。(CD-DA再生時の速度アップのため)
#### ・DDA音の発声が終了したときにいきなり波形を0にせず、フェードアウトさせるように
####   し、ノイズを軽減した。v0.57
#### ・DDAモード(サンプリング発声)のときの波形データのノイズが多く含まれている部分
####   をカットしして、音質を上げた。音量も調節した。v0.59
#### ・ノイズ音の音質・音量を調整して、実機の雰囲気に近づけた。v0.68
#### ・waveIndexの初期化とDDAモード時の動作を見直して実機の動作に近づけた。v0.63
#### ・waveIndexの初期化時にwaveテーブルも初期化するようにした。ファイヤープロレス
####   リング、Ｆ１トリプルバトルなどの音が実機に近づいた。v0.65
#### ・waveの波形の正負を実機同様にした。v0.74
#### ・waveの最小値が-14になるようにし音質を整えた。v0.74
#### ・クリティカルセクションは必要ない(書き込みが同時に行われるわけではない)ような
####   ので、省略し高速化した。v1.09
#### ・キュー処理(ApuQueue.c)をここに統合して高速化した。v1.10
#### ・低音領域のボリュームを上げて実機並みの聞こえやすさに近づけた。v1.46
#### ・LFO処理のの実装。"はにいいんざすかい"のOPや、フラッシュハイダースの効果音が
####   実機の音に近づいた。v1.59

####
#### Remi: I've purposely left the comments in Japanese.
####

module Yuno::Chips
  class HuC6280 < Yuno::AbstractChip
    # Actual implementation of the HuC6280 emulator based on Ootake's implementation
    private class HuC6280Ootake < Yuno::AbstractEmulator
      N_CHANNEL = 6

      # Kitao更新。PSGはオーバーサンプリングすると響きの美しさが損なわれてしまう
      # のでオーバーサンプリングしないようにした。速度的にもアップ。
      OVERSAMPLE_RATE = 1.0

      # 21.8500。Kitao追加。PSG音量の減少値。*6.0は各チャンネル足したぶんを割る
      # 意味。大きいほど音は減る。CDDAが100%のときにちょうど良いぐらいの音量に合
      # わせよう。v2.19,v2.37,v2.39,v2.62更新
      PSG_DECLINE = 21.8500 * 6.0

      # -1.05809999010で雀探物語２OK。Kitao追加。音量テーブルの減少値。マイナス
      # -が大きいほど小さい音が聞こえづらくなる。マイナスが小さすぎると平面的な
      # -音になる。v2.19,v2.37,v2.39,v2.40,v2.62,v2.65更新
      VOL_TABLE_DECLINE = -1.05809999010

      # 0.30599899951。Kitao追加。サンプリング音の消音時の音の減退量。ソルジャー
      # ブレイド,将棋初心者無用の音声で調整。基本的にこの値が小さいほうがノイズ
      # が減る(逆のケースもある)。v2.40
      SAMPLE_FADE_DECLINE = 0.305998999951

      ####            [DEV NOTE]
      ####
      ####    MAL                     --- 0 - 15 (15 で -0[dB], １減るごとに -3.0 [dB])
      ####    AL                      --- 0 - 31 (31 で -0[dB], １減るごとに -1.5 [dB])
      ####    LAL/RAL         --- 0 - 15 (15 で -0[dB], １減るごとに -3.0 [dB])
      ####
      ####    次のように解釈しなおす。
      ####
      ####    MAL*2           --- 0 - 30 (30 で -0[dB], １減るごとに -1.5 [dB])
      ####    AL                      --- 0 - 31 (31 で -0[dB], １減るごとに -1.5 [dB])
      ####    LAL/RAL*2       --- 0 - 30 (30 で -0[dB], １減るごとに -1.5 [dB])
      ####
      ####
      ####    dB = 20 * log10(OUT/IN)
      ####
      ####    dB / 20 = log10(OUT/IN)
      ####
      ####    OUT/IN = 10^(dB/20)
      ####
      ####    IN(最大出力) を 1.0 とすると、
      ####
      ####    OUT = 10^(dB/20)
      ####
      ####                                    -91 <= -(MAL*2 + AL + LAL(RAL)*2) <= 0
      ####
      ####    だから、最も小さい音は、
      ####
      ####            -91 * 1.5 [dB] = -136.5 [dB] = 10^(-136.5/20) ~= 1.496236e-7 [倍]
      ####
      ####    となる。
      ####
      ####      1e-7 オーダーの値は、固定小数点で表現しようとすると、小数部だけで
      ####    24 ビット以上必要で、なおかつ１６ビットの音声を扱うためには +16ビット
      ####    だから 24+16 = 40ビット以上必要になる。よって、32 ビットの処理系で
      ####    ＰＣＥの音声を固定小数点で表現するのはつらい。そこで、波形の計算は
      ####    float で行なうことにする。
      ####
      ####      float から出力形式に変換するのはＡＰＵの仕事とする。
      ####
      ####    [2004.4.28] やっぱり Sint32 で実装することにした(微小な値は無視する)。
      ####
      ####      ＣＰＵとＰＳＧは同じＩＣにパッケージしてあるのだが、
      ####    実際にはＰＳＧはＣＰＵの１／２のクロックで動作すると考えて良いようだ。
      ####    よって、ＰＳＧの動作周波数 Fpsg は、
      ####
      ####            Fpsg = 21.47727 [MHz] / 3 / 2 = 3.579545 [MHz]
      ####
      ####    となる。
      ####
      ####    たとえば３２サンプルを１周期とする波形が再生されるとき、
      ####    この周波数の周期でサンプルを１つずつ拾い出すと、
      ####
      ####            M = 3579545 / 32 = 111860.78125 [Hz]
      ####
      ####    というマジックナンバーが得られる（ファミコンと同じ）。
      ####    ただし、再生周波数が固定では曲の演奏ができないので、
      ####    FRQ なる周波数パラメータを用いて再生周波数を変化させる。
      ####    FRQ はＰＳＧのレジスタに書き込まれる１２ビット長のパラメータで、
      ####    ↑で得られたマジックナンバーの「割る数」になっている。
      ####
      ####    上の３２サンプルを１周期とする波形が再生されるとき、
      ####    この波形の周波数 F は、FRQ を用いて、
      ####
      ####            F = M / FRQ [Hz]  (FRQ != 0)
      ####
      ####    となる。
      ####
      ####      ＰＣの再生サンプリング周波数が Fpc [Hz] だとすると、
      ####    １周期３２サンプルの波形の再生周波数 F2 は  F2 = Fpc / 32 [Hz]。
      ####    よって、ＰＣの１サンプルに対して、ＰＣＥの１サンプルを拾い出す
      ####    カウンタの進み幅 I は
      ####
      ####            I = F / F2 = 32 * F / Fpc = Fpsg / FRQ / Fpc [単位なし]
      ####
      ####    となる。
      ####
      ####    [NOISE CHANNEL]
      ####
      ####      擬似ノイズの生成にはＭ系列(maximum length sequence)が用いられる。
      ####    Ｍ系列のビット長は未調査につき不明。
      ####    ここでは仮に１５ビットとして実装を行なう。
      ####    出力は１ビットで、D0 がゼロのときは負の値、１のときは正の値とする。
      ####
      ####    ＰＣの１サンプルに対して、ＰＣＥの１サンプルを拾い出す
      ####    カウンタの進み幅 I は、
      ####
      ####            I = Fpsg / 64 / FRQ / Fpc  (FRQ != 0)
      ####
      ####    となる。
      ####
      ####    [再生クオリティ向上について] 2004.6.22
      ####
      ####      エミュレータでは、ＰＳＧのレジスタにデータが書き込まれるまで、
      ####    次に発声すべき音がわからない。レジスタにデータが書き込まれたときに、
      ####    サウンドバッファを更新したいのだけど、あいにく現在の実装では、
      ####    サウンドバッファの更新は別スレッドで行なわれていて、
      ####    エミュレーションスレッドから任意の時間に更新することができない。
      ####
      ####      これまでの再生では、サウンドバッファの更新時のレジスタ設定のみが
      ####    有効だったが、これだと例えばサウンドバッファ更新の合間に一瞬だけ
      ####    出力された音などが無視されてしまう。これは特にＤＤＡモードやノイズが
      ####    リズムパートとして使用される上で問題になる。
      ####
      ####      レジスタに書き込まれた値をきちんと音声出力に反映させるには、
      ####    過去に書き込まれたレジスタの値(いつ、どのレジスタに、何が書き込まれたか)
      ####    を保存しておいて、サウンドバッファ更新時にこれを参照する方法が
      ####    考えられる。どのくらい過去までレジスタの値を保存しておくかは、
      ####    サウンドバッファの長さにもよると思われるが、とりあえずは試行錯誤で
      ####    決めることにする。
      ####
      ####      ＰＳＧレジスタへの書き込み動作はエミュレーションスレッドで
      ####    行なわれ、サウンドバッファ更新はその専用スレッドで行なわれる。
      ####    これだと、エミュレーションスレッドがレジスタのキューに書き込みを
      ####    行なっている最中に、サウンドバッファ更新スレッドがキューから
      ####    読み出しを行なってしまい、アクセスが衝突する。この問題を解決するには、
      ####
      ####            １．サウンドバッファの更新を別スレッドで行なわない
      ####            ２．キューのアクセス部分を排他処理にする
      ####
      ####    の２とおりが考えられる。とりあえず２の方法をとることにする。

      class Psg
        property frq : UInt32 = 0
        property? on : Bool = false
        property? dda : Bool = false
        property volume : UInt32 = 0
        property volumeL : UInt32 = 0
        property volumeR : UInt32 = 0
        property outVolumeL : Int32 = 0
        property outVolumeR : Int32 = 0
        property wave : Array(Int32) = Array(Int32).new(32, 0i32)
        property waveIndex : UInt32 = 0
        property ddaSample : Int32 = 0
        property phase : UInt32 = 0
        property deltaPhase : UInt32 = 0
        property? noiseOn : Bool = false
        property noiseFrq : UInt32 = 0
        property deltaNoisePhase : UInt32 = 0

        def initialize
        end
      end

      @sampleRate : Float64 = 0.0
      @psgFrq : Float64 = 0.0
      @resampleRate : Float64 = 0.0

      @psg : Array(Psg)
      @ddaFadeOutL : Array(Int32) = Array(Int32).new(8, 0) # Kitao追加
      @ddaFadeOutR : Array(Int32) = Array(Int32).new(8, 0) # Kitao追加
      @channel : UInt32 = 0 # 0 - 5
      @mainVolumeL : UInt32 = 0 # 0 - 15
      @mainVolumeR : UInt32 = 0 # 0 - 15
      @lfoFrq : UInt32 = 0
      @lfoOn : Bool = false # v1.59から非使用。過去verのステートロードのために残してある。
      @lfoCtrl : UInt32 = 0
      @lfoShift : UInt32 = 0 # v1.59から非使用。過去verのステートロードのために残してある。
      @psgVolumeEffect : Int32 = 0 # Kitao追加
      @volume : Float64 = 0.0 # Kitao追加
      @vol : Float64 = 0.0 # Kitao追加。v1.08
      @psgMute : Array(Bool) = Array(Bool).new(8, false)
      @port : Array(UInt8) = Array(UInt8).new(16, 0u8) # for debug purpose
      @waveCrash : Bool = false # Kitao追加。DDA再生中にWaveデータが書き換えられたらTRUE
      property? honeyInTheSky : Bool = false # はにいいんざすかいパッチ用。v2.60

      @volumeTable : Array(Int32) = Array(Int32).new(92, 0)
      @noiseTable : Array(Int32) = Array(Int32).new(32768, 0)

      def initialize(clock : Int32, sampleRate : Int32)
        @psg = Array(Psg).new(8) { |_| Psg.new }
        @psgFrq = (clock & 0x7FFFFFFF).to_f64!
        self.honeyInTheSky = (clock >> 31) & 0x01 != 0
        setVolume
        reset
        createVolumeTable
        createNoiseTable
        @sampleRate = sampleRate.to_f64!
        @resampleRate = @psgFrq / OVERSAMPLE_RATE / @sampleRate
      end

      @[AlwaysInline]
      def read(reg : Int) : UInt8
        @port[reg & 15]
      end

      def write(reg : Int, data : UInt8) : Nil
        writeReg(reg.to_u8!, data)
      end

      private def setVolume : Nil
        @volume = 1.0 / PSG_DECLINE
        setVol
      end

      private def resetVolumeReg : Nil
        @mainVolumeL = 0
        @mainVolumeR = 0
        N_CHANNEL.times do |i|
          @psg[i].volume = 0
          @psg[i].outVolumeL = 0
          @psg[i].outVolumeR = 0
          @ddaFadeOutL[i] = 0
          @ddaFadeOutR[i] = 0
        end
      end

      @[AlwaysInline]
      private def setMutePsgChannel(num : Int32, mute? : Bool) : Nil
        @psgMute[num] = mute?
        if mute?
          @ddaFadeOutL[num] = 0
          @ddaFadeOutR[num] = 0
        end
      end

      def muteMask=(mask : UInt32) : Nil
        N_CHANNEL.times do |i|
          setMutePsgChannel(i, (muteMask >> i) & 0x01 != 0)
        end
      end

      @[AlwaysInline]
      private def getMutePsgChannel(num : Int32) : Bool
        @psgMute[num]
      end

      # ボリュームテーブルの作成
      # Kitao更新。低音量の音が実機より聞こえづらいので、減退率を
      # VOL_TABLE_DECLINE[db](試行錯誤したベスト値)とし、ノーマライズ処理をする
      # ようにした。v1.46おそらく、実機もアンプを通って出力される際にノーマライ
      # ズ処理されている。
      private def createVolumeTable : Nil
        v : Float64 = 0.0

        @volumeTable[0] = 0
        (1..91).each do |i|
          v = 91.0 - i
          @volumeTable[i] = (32768.0 * (10.0 ** (v * VOL_TABLE_DECLINE / 20.0))).to_i32!
        end
      end

      # ノイズテーブルの作成
      private def createNoiseTable : Nil
        bit0 : UInt32 = 0
        bit1 : UInt32 = 0
        bit14 : UInt32 = 0
        reg : UInt32 = 0x100

        32768_u32.times do |i|
          bit0 = reg & 1
          bit1 = (reg & 2) >> 1
          bit14 = (bit0 ^ bit1)
          reg >>= 1
          reg |= (bit14 << 14)
          @noiseTable[i] = bit0 != 0 ? -18 : -1
        end
      end

      # ＰＳＧポートの書き込みに対する動作を記述します。
      @[AlwaysInline]
      def writeReg(reg : UInt8, data : UInt8) : Nil
        frq : UInt32 = 0

        @port[reg & 15] = data

        case reg & 15
        when 0 # Register select
          @channel = (data & 7).to_u32!

        when 1 # Main volume
          @mainVolumeL = ((data >> 4) & 0x0F).to_u32!
          @mainVolumeR = (data & 0x0F).to_u32!

          # LMAL, RMAL は全チャネルの音量に影響する
          N_CHANNEL.times do |i|
            psgChan = @psg.unsafe_fetch(i)
            psgChan.outVolumeL = @volumeTable[psgChan.volume + (@mainVolumeL + psgChan.volumeL) * 2]
            psgChan.outVolumeR = @volumeTable[psgChan.volume + (@mainVolumeR + psgChan.volumeR) * 2]
          end

        when 2 # Frequency low
          psgChan = @psg.unsafe_fetch(@channel)
          psgChan.frq &= ~0xFF
          psgChan.frq |= data

          # Kitao更新。update_frequencyは、速度アップのためサブルーチンにせず直接実行するようにした。
          frq = ((psgChan.frq.to_i64! - 1) & 0xFFF).to_u32!
          if frq != 0
            # Kitao更新。速度アップのためfrq以外は定数計算にした。精度向上のため、
            # 先に値の小さいOVERSAMPLE_RATEのほうで割るようにした。+0.5は四捨五
            # 入で精度アップ。プチノイズ軽減のため。
            psgChan.deltaPhase = ((65536.0 * 256.0 * 8.0 * @resampleRate) / frq.to_f64! + 0.5).to_u32!
          else
            psgChan.deltaPhase = 0
          end

        when 3 # Frequency high
          psgChan = @psg.unsafe_fetch(@channel)
          psgChan.frq &= ~0xF00
          psgChan.frq |= (data & 0x0F).to_u32! << 8

          # Kitao更新。update_frequencyは、速度アップのためサブルーチンにせず直接実行するようにした。
          frq = ((psgChan.frq.to_i64! - 1) & 0xFFF).to_u32!

          if frq != 0
            # Kitao更新。速度アップのためfrq以外は定数計算にした。精度向上のため、
            # 先に値の小さいOVERSAMPLE_RATEのほうで割るようにした。+0.5は四捨五
            # 入で精度アップ。プチノイズ軽減のため。
            psgChan.deltaPhase = ((65536.0 * 256.0 * 8.0 * @resampleRate) / frq.to_f64! + 0.5).to_u32!
          else
            psgChan.deltaPhase = 0
          end

        when 4 # ON, DDA, AL
          psgChan = @psg.unsafe_fetch(@channel)

          # はにいいんざすかいのポーズ時に、微妙なボリューム調整タイミングの問題
          # でプチノイズが載ってしまうので、現状はパッチ処理で対応。v2.60更新
          if @honeyInTheSky
            # 発声中にdataが0の場合、LRボリュームも0にリセット。はにいいんざすか
            # いのポーズ時のノイズが解消。(data & 0x1F)だけが0のときにリセットす
            # ると、サイレントデバッガーズ等でNG。発声してない時にリセットすると
            # アトミックロボでNG。ｖ2.55
            if psgChan.on? && data == 0
              # メインボリュームのbit0が0のときだけ処理(はにいいんざすかいでイレ
              # ギュラーな0xE。他のゲームは0xF。※ヘビーユニットも0xEだった)。こ
              # れがないとミズバク大冒険で音が出ない。実機の仕組みと同じかどうか
              # は未確認。v2.53追加
              unless Yuno.bitflag?(@mainVolumeL, 1)
                psgChan.volumeL = 0
              end

              unless Yuno.bitflag?(@mainVolumeR, 1)
                psgChan.volumeR = 0
              end
            end
          end

          psgChan.on = (data & 0x80) != 0
          # DDAからWAVEへ切り替わるとき or DDAから消音するとき
          if psgChan.dda? && !Yuno.bitflag?(data, 0x40)
            # Kitao追加。DDAはいきなり消音すると目立つノイズが載るのでフェードアウトする。
            #
            # Remi: This has been refactored to be cleaner.  The commented-out
            # code is the original code.
            # @ddaFadeOutL[@channel] = ((psgChan.ddaSample * psgChan.outVolumeL) *
            #                           ((1 + (1 >> 3) + (1 >> 4) + (1 >> 5) + (1 >> 7) + (1 >> 12) + (1 >> 14) + (1 >> 15)) *
            #                            SAMPLE_FADE_DECLINE)).to_i32!
            # @ddaFadeOutR[@channel] = ((psgChan.ddaSample * psgChan.outVolumeR) *
            #                           ((1 + (1 >> 3) + (1 >> 4) + (1 >> 5) + (1 >> 7) + (1 >> 12) + (1 >> 14) + (1 >> 15)) *
            #                            SAMPLE_FADE_DECLINE)).to_i32!

            @ddaFadeOutL.unsafe_put(@channel, ((psgChan.ddaSample * psgChan.outVolumeL) * SAMPLE_FADE_DECLINE).to_i32!)
            @ddaFadeOutR.unsafe_put(@channel, ((psgChan.ddaSample * psgChan.outVolumeR) * SAMPLE_FADE_DECLINE).to_i32!)
          end

          psgChan.dda = (data & 0x40) != 0

          # Kitao追加。dataのbit7,6が01のときにWaveインデックスをリセットする。
          # DDAモード時にWaveデータを書き込んでいた場合はここでWaveデータを修復
          # （初期化）する。ファイヤープロレスリング。
          if (data & 0xC0) == 0x40
            psgChan.waveIndex = 0
            if @waveCrash
              32.times { |i| psgChan.wave.unsafe_put(i, -14) } # Waveデータを最小値で初期化
              @waveCrash = false
            end
          end

          psgChan.volume = (data & 0x1F).to_u32!
          psgChan.outVolumeL = @volumeTable[psgChan.volume + (@mainVolumeL + psgChan.volumeL) * 2]
          psgChan.outVolumeR = @volumeTable[psgChan.volume + (@mainVolumeR + psgChan.volumeR) * 2]

        when 5 # LAL, RAL
          psgChan = @psg.unsafe_fetch(@channel)
          psgChan.volumeL = ((data >> 4) & 0x0F).to_u32!
          psgChan.volumeR = (data & 0x0F).to_u32!
          psgChan.outVolumeL = @volumeTable[psgChan.volume + (@mainVolumeL + psgChan.volumeL) * 2]
          psgChan.outVolumeR = @volumeTable[psgChan.volume + (@mainVolumeR + psgChan.volumeR) * 2]

        when 6 # wave data.  Kitao更新。DDAモードのときもWaveデータを更新するよ
               # うにした。v0.63。ファイヤープロレスリング
          psgChan = @psg.unsafe_fetch(@channel)
          data &= 0x1F
          @waveCrash = false # Kitao追加

          # Kitao追加。音を鳴らしていないときだけWaveデータを更新する。v0.65。F1
          # トリプルバトルのエンジン音。
          unless psgChan.on?
            # 17。Kitao更新。一番心地よく響く値に。ミズバク大冒険，モトローダー，
            # ドラゴンスピリット等で調整。
            psgChan.wave.unsafe_put(psgChan.waveIndex, 17 - data)
            psgChan.waveIndex += 1
            psgChan.waveIndex &= 0x1F
          end

          if psgChan.dda?
            # Kitao更新。ノイズ軽減のため6より下側の値はカットするようにした。v0.59
            if data < 6 # サイバーナイトで6に決定
              data = 6 # ノイズが多いので小さな値はカット
            end

            # サイバーナイトで11に決定。ドラムの音色が最適。v0.74
            psgChan.ddaSample = 11 - data

            # DDAモード時にWaveデータを書き換えた場合
            @waveCrash = true unless psgChan.on?
          end

        when 7 # Noise on, noise frequency
          if @channel >= 4
            psgChan = @psg.unsafe_fetch(@channel)
            psgChan.noiseOn = (data & 0x80) != 0
            psgChan.noiseFrq = (0x1F - (data & 0x1F)).to_u32!
            if psgChan.noiseFrq == 0
              psgChan.deltaNoisePhase = ((2048.0 * @resampleRate) + 0.5).to_u32! # Kitao更新
            else
              psgChan.deltaNoisePhase = ((2048.0 * @resampleRate) / (psgChan.noiseFrq.to_f64! + 0.5)).to_u32! # Kitao更新
            end
          end

        when 8 # LFO frequency
          @lfoFrq = data.to_u32!

        when 9 # LFO control. Kitao更新。シンプルに実装してみた。実機で同じ動作
               # かは未確認。はにいいんざすかいの音が似るように実装。v1.59
          if Yuno.bitflag?(data, 0x80) # bit7を立てて呼ぶと恐らくリセット
            @psg.unsafe_fetch(1).phase = 0 # LfoFrqは初期化しない。はにいいんざすかい。
          end

          @lfoCtrl = (data & 7).to_u32! # ドロップロックほらホラで5が使われる。v1.61更新
          if Yuno.bitflag?(@lfoCtrl, 4)
            @lfoCtrl = 0 # ドロップロックほらホラ。実機で聴いた感じはLFOオフと同
                         # じ音のようなのでbit2が立っていた(負の数扱い？)ら0と同
                         # じこととする。
          end
        end
      end

      private def setVol : Nil
        if @psgVolumeEffect == 0
          @vol = 1.0 / 128.0
        elsif @psgVolumeEffect == 3
          @vol = @volume / (OVERSAMPLE_RATE * 4.0 / 3.0) # 3/4。v1.29追加
        else
          @vol = @volume / (OVERSAMPLE_RATE * @psgVolumeEffect) # Kitao追加。_PsgVolumeEffect=ボリューム調節効果。
        end
      end

      # ＰＳＧの出力をミックスします。
      def update(outputs : OutputBuffers, samples : UInt32) : Nil
        sample : Int32 = 0
        lfo : Int32 = 0
        sampleAllL : Int32 = 0 # Kitao追加。6chぶんのサンプルを足していくための
                               # バッファ。精度を維持するために必要。6chぶん合計
                               # が終わった後に、これをSint16に変換して書き込む
                               # ようにした。
        sampleAllR : Int32 = 0 # Kitao追加。上のＲチャンネル用
        smp : Int32 = 0 # Kitao追加。DDA音量,ノイズ音量計算用
        outL : Slice(Int32) = outputs[0]
        outR : Slice(Int32) = outputs[1]

        # Check once, then we can avoid bounds checks below.
        if outL.size < samples || outR.size < samples
          raise "HuC6280 update: Bad number of samples (expected #{samples}, " \
                "but buffers have #{outL.size} and #{outR.size})"
        end

        samples.times do |j|
          sampleAllL = 0
          sampleAllR = 0

          N_CHANNEL.times do |i|
            psgChan = @psg.unsafe_fetch(i)

            if psgChan.on? && (i != 1 || @lfoCtrl == 0) && !@psgMute.unsafe_fetch(i) # Kitao更新
              if psgChan.dda?
                # Kitao更新。サンプリング音の音量を実機並みに調整。v2.39,v2.40,v2.62,v2.65再調整した。
                smp = psgChan.ddaSample * psgChan.outVolumeL
                sampleAllL += smp + (smp >> 3) + (smp >> 4) + (smp >> 5) + (smp >> 7) + (smp >> 12) + (smp >> 14) + (smp >> 15)
                smp = psgChan.ddaSample * psgChan.outVolumeR
                sampleAllR += smp + (smp >> 3) + (smp >> 4) + (smp >> 5) + (smp >> 7) + (smp >> 12) + (smp >> 14) + (smp >> 15)

              elsif psgChan.noiseOn?
                sample = @noiseTable[psgChan.phase >> 17]

                # Kitao追加。noiseFrq=0(dataに0x1Fが書き込まれた)のときは音量が
                # 通常の半分とした。（ファイヤープロレスリング３、パックランド、
                # 桃太郎活劇、がんばれゴルフボーイズなど）
                if psgChan.noiseFrq == 0
                  smp = sample * psgChan.outVolumeL
                  sampleAllL += (smp >> 1) + (smp >> 12) + (smp >> 14) # (1/2 + 1/4096 + (1/32768 + 1/32768))
                  smp = sample * psgChan.outVolumeR
                  sampleAllR += (smp >> 1) + (smp >> 12) + (smp >> 14)
                else
                  # 通常

                  # Kitao更新。ノイズの音量を実機並みに調整(1 + 1/2048 + 1/16384
                  # + 1/32768)。この"+1/32768"で絶妙(主観。大魔界村,ソルジャーブ
                  # レイドなど)になる。v2.62更新
                  smp = sample * psgChan.outVolumeL
                  sampleAllL += smp + (smp >> 11) + (smp >> 14) + (smp >> 15)

                  # Kitao更新。ノイズの音量を実機並みに調整
                  smp = sample * psgChan.outVolumeR
                  sampleAllR += smp + (smp >> 11) + (smp >> 14) + (smp >> 15)
                end

                psgChan.phase = psgChan.phase &+ psgChan.deltaNoisePhase # Kitao更新

              elsif psgChan.deltaPhase != 0
                # Kitao更新。オーバーサンプリングしないようにした。
                sample = psgChan.wave[psgChan.phase >> 27]
                if psgChan.frq < 128
                  # 低周波域の音量を制限。ブラッドギアのスタート時などで実機と同
                  # 様の音に。ソルジャーブレイドなども実機に近くなった。v2.03
                  sample -= sample >> 2
                end

                sampleAllL += sample * psgChan.outVolumeL # Kitao更新
                sampleAllR += sample * psgChan.outVolumeR # Kitao更新

                # Kitao更新。Lfoオンが有効になるようにし、Lfoの掛かり具合を実機に近づけた。v1.59
                if i == 0 && @lfoCtrl > 0
                  # _LfoCtrlが1のときに0回シフト(そのまま)で、はにいいんざすかいが実機の音に近い。
                  # _LfoCtrlが3のときに4回シフトで、フラッシュハイダースが実機の音に近い。
                  lfo = @psg.unsafe_fetch(1).wave[@psg.unsafe_fetch(1).phase >> 27] << ((@lfoCtrl - 1) << 1)
                  @psg.unsafe_fetch(0).phase = @psg.unsafe_fetch(0).phase &+
                                               ((65536.0 * 256.0 * 8.0 * @resampleRate) /
                                                (@psg.unsafe_fetch(0).frq + lfo).to_f64! + 0.5).to_u32!
                  @psg.unsafe_fetch(1).phase = @psg.unsafe_fetch(1).phase &+
                                               ((65536.0 * 256.0 * 8.0 * @resampleRate) /
                                                (@psg.unsafe_fetch(1).frq + lfo).to_f64! + 0.5).to_u32!
                else
                  psgChan.phase = psgChan.phase &+ psgChan.deltaPhase
                end
              end
            end

            if @ddaFadeOutL.unsafe_fetch(i) > 0
              @ddaFadeOutL.unsafe_put(i, @ddaFadeOutL.unsafe_fetch(i) - 1)
            elsif @ddaFadeOutL.unsafe_fetch(i) < 0
              @ddaFadeOutL.unsafe_put(i, @ddaFadeOutL.unsafe_fetch(i) + 1)
            end

            if @ddaFadeOutR.unsafe_fetch(i) > 0
              @ddaFadeOutR.unsafe_put(i, @ddaFadeOutR.unsafe_fetch(i) - 1)
            elsif @ddaFadeOutR.unsafe_fetch(i) < 0
              @ddaFadeOutR.unsafe_put(i, @ddaFadeOutR.unsafe_fetch(i) + 1)
            end

            sampleAllL = sampleAllL &+ @ddaFadeOutL.unsafe_fetch(i)
            sampleAllR = sampleAllR &+ @ddaFadeOutR.unsafe_fetch(i)
          end

          # Kitao更新。6ch合わさったところで、ボリューム調整してバッファに書き込む。
          sampleAllL = (sampleAllL.to_f64! * @vol).to_i32!
          sampleAllR = (sampleAllR.to_f64! * @vol).to_i32!
          outL.unsafe_put(j, sampleAllL)
          outR.unsafe_put(j, sampleAllR)
        end
      end

      def reset : Nil
        @psg = Array(Psg).new(8) { |_| Psg.new }
        @ddaFadeOutL.fill(0) # Kitao追加
        @ddaFadeOutR.fill(0) # Kitao追加
        @mainVolumeL = 0
        @mainVolumeR = 0
        @lfoFrq = 0
        @lfoCtrl = 0
        @channel = 0 # Kitao追加。v2.65
        @waveCrash = false # Kitao追加

        # Kitao更新。v0.65．waveデータを初期化。
        N_CHANNEL.times do |i|
          32.times do |j|
            # 最小値で初期化。ファイプロ，フォーメーションサッカー'90，F1トリプルバトルで必要。
            @psg[i].wave[j] = -14
          end
        end

        32.times do |j|
          @psg[3].wave[j] = 17 # ch3は最大値で初期化。F1トリプルバトル。v2.65
        end
      end

      def unmuteAll : Nil
        @psgMute.fill(false)
      end
    end
  end
end
