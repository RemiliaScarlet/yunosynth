#### YunoSynth
#### Copyright (C) 2023-2024 Remilia Scarlet <remilia@posteo.jp>
#### Portions based on VGMPlay, Copyright (C) Valley Bell
####
#### This program is free software: you can redistribute it and/or
#### modify it under the terms of the GNU Affero General Public
#### License as published by the Free Software Foundation, either
#### version 3 of the License, or (at your option) any later version.
####
#### This program is distributed in the hope that it will be useful,
#### but WITHOUT ANY WARRANTY; without even the implied warranty of
#### MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#### Affero General Public License for more details.
####
#### You should have received a copy of the GNU Affero General Public License
#### along with this program.  If not, see <https://www.gnu.org/licenses/>.
require "./emu-sn76496-mame"

####
#### SN76489 sound chip emulator interface
####

module Yuno::Chips
  # SN76496 sound chip emulator.
  class SN76489 < Yuno::AbstractChip
    CHIP_ID = 0x00_u32

    @clockFromHeader : UInt32 = 0
    @samplingMode : UInt8 = 0
    @chipSampleRate : UInt32 = 0
    @chip : SN76489Mame? = nil
    @subType : UInt8 = 0
    @shiftRegWidthFromHeader : UInt8 = 0
    @feedbackFromHeader : UInt16 = 0
    @flagsFromHeader : UInt8 = 0
    @playerSampleRate : UInt32
    getter? ngpMode : Bool = false
    protected setter ngpMode : Bool

    def initialize(chipNum : Int32, absChipCount : Int32, vgm : VgmFile, playbackSampleRate : UInt32,
                   newSamplingMode : UInt8, newPlayerSampleRate : UInt32, *, emuCore : Symbol? = nil,
                   flags : ChipFlags? = nil)
      raise "Use alternate #initialize with previousChip"
    end

    def initialize(chipNum : Int32, absChipCount : Int32, vgm : VgmFile, playbackSampleRate : UInt32,
                   newSamplingMode : UInt8, newPlayerSampleRate : UInt32,
                   previousChip : AbstractChip?, *, emuCore : Symbol? = nil, flags : ChipFlags? = nil)
      @playerSampleRate = newPlayerSampleRate

      header = vgm.header
      case header.sn76489ShiftRegisterWidth
      when 0x0F # 0x4000
        if Yuno.bitflag?(header.sn76489Flags, 0x08) # Clock divider == 1?
          @subType = 0x05_u8 # SN94624
        else
          @subType = 0x01_u8 # SN76489
        end

      when 0x10 # 0x8000
        if header.sn76489Feedback == 0x0009
          @subType = 0x06_u8 # Sega PSG
        elsif header.sn76489Feedback == 0x0022
          if Yuno.bitflag?(header.sn76489Flags, 0x10) # If Tandy noise mode enabled
            @subType = Yuno.bitflag?(header.sn76489Flags, 0x02) ? 0x07_u8 : 0x08_u8 # NCR8496 / PSSJ-3
          else
            @subType = 0x07_u8 # NCR8496
          end
        end

      when 0x11 # 0x10000
        if Yuno.bitflag?(header.sn76489Flags, 0x08) # Clock divider == 1?
          @subType = 0x03_u8 # SN76494
        else
          @subType = 0x02_u8 # SN76489A/SN76496
        end
      end

      @clockFromHeader = vgm.getAlternateChipClock(chipNum, Yuno::ChipType::Sn76489) || header.sn76489Clock
      @clockFromHeader &= 0xBFFFFFFF
      @clockFromHeader &= ~0x80000000
      @clockFromHeader |= header.sn76489Clock & (chipNum << 31)

      chipCount = (header.sn76489Clock & 0x40000000) != 0 ? 2 : 1
      initFields(vgm, emuCore, playbackSampleRate, newSamplingMode, chipCount, flags)

      @shiftRegWidthFromHeader = header.sn76489ShiftRegisterWidth
      @feedbackFromHeader = header.sn76489Feedback
      @flagsFromHeader = header.sn76489Flags
      @ngpMode = Yuno.bitflag?(@clockFromHeader, 0x80000000)

      if previousChip.is_a?(SN76489)
        previousChip.ngpMode = @ngpMode
      end
    end

    protected def initialize
    end

    protected def initFields(vgm : VgmFile, emuCore : Symbol?, playbackSampleRate : UInt32, newSamplingMode : UInt8,
                             chipCount : Int32, flags : ChipFlags?) : Nil
      @volume = vgm.getChipVolume(self, ChipType::Sn76489, chipCount)
      @core = emuCore || SN76489.defaultEmuCore
      case @core
      when :mame
        @samplingMode = newSamplingMode
        @chipSampleRate = @clockFromHeader <= 0 ? playbackSampleRate : @clockFromHeader
      else raise YunoError.new("Unsupported emulation core for SN76489: #{@core}")
      end
    end

    def type : ChipType
      ChipType::Sn76489
    end

    def name : String
      if @ngpMode
        "Toshiba T6W28"
      else
        case @subType
        when 0x01 then "Texas Instruments SN76489"
        when 0x02 then "Texas Instruments SN76489A"
        when 0x03 then "Texas Instruments SN76494"
        when 0x04 then "Texas Instruments SN76496"
        when 0x05 then "Texas Instruments SN94624"
        when 0x06 then "Sega PSG"
        when 0x07 then "NCR 8496"
        when 0x08 then "Tandy PSSJ-3"
        else "Texas Instruments SN76496"
        end
      end
    end

    def shortName : String
      if @ngpMode
        "T6W28"
      else
        case @subType
        when 0x01 then "SN76489"
        when 0x02 then "SN76489A"
        when 0x03 then "SN76494"
        when 0x04 then "SN76496"
        when 0x05 then "SN94624"
        when 0x06 then "Sega PSG"
        when 0x07 then "NCR 8496"
        when 0x08 then "PSSJ-3"
        else "SN76496"
        end
      end
    end

    def id : UInt32
      CHIP_ID
    end

    def emuCore : Symbol
      @core
    end

    def self.defaultEmuCore : Symbol
      :mame
    end

    def start(chipIndex : UInt8, clock : UInt32, flags : ChipFlags? = nil, previousChip : AbstractChip? = nil) : UInt32
      case @core
      when :mame
        parentEmu : SN76489Mame? = nil

        if @ngpMode
          Yuno.dlog!("NeoGeo Pocket Color mode detected")
          if previousChip.nil?
            Yuno.dlog!("  ...but no parent chip found?")
          else
            parentEmu = previousChip.chip.as(SN76489Mame)
            previousChip.as(SN76489).ngpMode = true
            Yuno.dlog!("  Parent emulation core found")
          end
        else
          Yuno.dlog!("Standard SN76489 derivative mode detected")
        end

        @chip, rate = SN76489Mame.sn76496Start(clock, @shiftRegWidthFromHeader.to_i32!,
                                               @feedbackFromHeader.to_i32!,
                                               ((@flagsFromHeader & 0x02) >> 1).to_i32!,
                                               ((@flagsFromHeader & 0x04) >> 2).to_i32!,
                                               ((@flagsFromHeader & 0x08) >> 3).to_i32!,
                                               (@flagsFromHeader & 0x01).to_i32!,
                                               parentEmu)
        @chip.not_nil!.freqLimiter(clock & 0x3FFFFFFF, ((@flagsFromHeader & 0x08) >> 3).to_i32!, @playerSampleRate)
        if (Yuno.bitflag?(@samplingMode, 1) && rate < @chipSampleRate) || @samplingMode == 2
          rate = @chipSampleRate
        end
        @sampleRate = rate # Update sample rate
        @sampleRate

      else raise YunoError.new("Unsupported emulation core for SN76489: #{@core}")
      end
    end

    def getClock : UInt32
      @clockFromHeader
    end

    def update(outputs : OutputBuffers, samples : Int) : Nil
      @chip.not_nil!.update(outputs, samples.to_u32!)
    end

    def reset(chipIndex : UInt8) : Nil
      @chip.not_nil!.reset
    end

    def read(chipIndex : UInt8, offset : Int) : UInt8|UInt16|UInt32
      raise "Cannot do a straight read on SN76489"
    end

    def write(chipIndex : UInt8, offset : Int, data : Int, port : UInt8 = 0) : Nil
      case @core
      when :mame
        case offset
        when 0x00
          @chip.not_nil!.writeReg(data.to_u8!)
        when 0x01
          @chip.not_nil!.stereoWrite(data.to_u8!)
        end
      end
    end

    def setMuteMask(chipIndex : UInt8, mask : UInt32) : Nil
      @chip.not_nil!.muteMask = mask
    end

    def getVolModifier : UInt32
      @volume.to_u32
    end

    def baseVolume : UInt16
      0x80_u16
    end

    # The internal interface for the chip.
    @[AlwaysInline]
    def chip : AbstractEmulator
      @chip.not_nil!
    end
  end
end
