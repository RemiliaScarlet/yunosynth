#### Wonderswan Emulator
#### Based on in_wsr
#### Crystal port Copyright (C) 2024 Remilia Scarlet
#### BSD 3-Clause License

module Yuno::Chips
  class Wonderswan < Yuno::AbstractChip
    # Actual implementation of the Wonderswan emulator based on in_wsr's implementation
    private class WonderswanInwsr < Yuno::AbstractEmulator
      INITIAL_IO_VALUE = [
        0x00, 0x00, 0x9d, 0xbb, 0x00, 0x00, 0x00, 0x26, 0xfe, 0xde, 0xf9, 0xfb,
        0xdb, 0xd7, 0x7f, 0xf5, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x9e, 0x9b,
        0x00, 0x00, 0x00, 0x00, 0x99, 0xfd, 0xb7, 0xdf, 0x30, 0x57, 0x75, 0x76,
        0x15, 0x73, 0x77, 0x77, 0x20, 0x75, 0x50, 0x36, 0x70, 0x67, 0x50, 0x77,
        0x57, 0x54, 0x75, 0x77, 0x75, 0x17, 0x37, 0x73, 0x50, 0x57, 0x60, 0x77,
        0x70, 0x77, 0x10, 0x73, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x0a, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0f,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFF, 0x07, 0xFF, 0x07,
        0xFF, 0x07, 0xFF, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x1f, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x03, 0x00, 0x85, 0x00, 0x00, 0x00, 0x0, 0x0, 0x4f, 0xff,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xdb, 0x00, 0x00,
        0x00, 0x40, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x42, 0x00, 0x83, 0x00,
        0x2f, 0x3f, 0xff, 0xff, 0x00, 0x00, 0x00, 0x00, 0xd1, 0xd1, 0xd1, 0xd1,
        0xd1, 0xd1, 0xd1, 0xd1, 0xd1, 0xd1, 0xd1, 0xd1, 0xd1, 0xd1, 0xd1, 0xd1,
        0xd1, 0xd1, 0xd1, 0xd1, 0xd1, 0xd1, 0xd1, 0xd1, 0xd1, 0xd1, 0xd1, 0xd1,
        0xd1, 0xd1, 0xd1, 0xd1, 0xd1, 0xd1, 0xd1, 0xd1, 0xd1, 0xd1, 0xd1, 0xd1,
        0xd1, 0xd1, 0xd1, 0xd1, 0xd1, 0xd1, 0xd1, 0xd1, 0xd1, 0xd1, 0xd1, 0xd1,
        0xd1, 0xd1, 0xd1, 0xd1
      ] of UInt8

      # SoundDMA の転送間隔
      # 実際の数値が分からないので、予想です
      # サンプリング周期から考えてみて以下のようにした
      # 12KHz = 1.00HBlank = 256cycles間隔
      # 16KHz = 0.75HBlank = 192cycles間隔
      # 20KHz = 0.60HBlank = 154cycles間隔
      # 24KHz = 0.50HBlank = 128cycles間隔
      DMA_CYCLES = [ 256, 192, 154, 128 ]

      DEFAULT_CLOCK = 3072000

      NOISE_MASK = [
        1 << 0 | 1 << 1,
        1 << 0 | 1 << 1 | 1 << 4 | 1 << 5,
        1 << 0 | 1 << 1 | 1 << 3 | 1 << 4,
        1 << 0 | 1 << 1 | 1 << 4 | 1 << 6,
        1 << 0 | 1 << 2,
        1 << 0 | 1 << 3,
        1 << 0 | 1 << 4,
        1 << 0 | 1 << 2 | 1 << 3 | 1 << 4
      ]

      NOISE_BIT = [
        1 << 15,
        1 << 14,
        1 << 13,
        1 << 12,
        1 << 11,
        1 << 10,
        1 << 9,
        1 << 8
      ]

      class Audio
        property wave : Int32 = 0
        property lvol : Int32 = 0
        property rvol : Int32 = 0
        property offset : Int32 = 0
        property delta : Int32 = 0
        property pos : Int32 = 0
        property? muted : Bool = false

        def initialize
        end

        def reset : Nil
          @wave = 0
          @lvol = 0
          @rvol = 0
          @offset = 0
          @delta = 0
          @pos = 0
          @muted = false
        end
      end

      @audio : Array(Audio)
      @sweepDelta : Int32 = 0
      @sweepOffset : Int32 = 0
      @sweepTime : Int32 = 0
      @sweepStep : Int32 = 0
      @sweepCount : Int32 = 0
      @sweepFreq : Int32 = 0
      @noiseType : Int32 = 0
      @noiseRng : Int32 = 1
      @mainVolume : Int32 = 0x02
      @pcmVolumeLeft : Int32 = 0
      @pcmVolumeRight : Int32 = 0

      @ioRam : Array(UInt8) = Array(UInt8).new(0x100, 0u8)
      @internalRam : Array(UInt8) = Array(UInt8).new(0x4000, 0u8) # Actual size is 64 KB, but the audio chip can
                                                                  # only access 16 KB.

      @clock : UInt32 = 0u32
      @sampleRate : UInt32 = 0u32

      def initialize(@clock : UInt32, @sampleRate : UInt32)
        @audio = Array(Audio).new(4) { |_| Audio.new }
        reset
      end

      def reset : Nil
        @audio.each &.reset
        @sweepTime = 0
        @sweepStep = 0
        @noiseType = 0
        @noiseRng = 1
        @mainVolume = 0x02 # 0x04
        @pcmVolumeLeft = 0
        @pcmVolumeRight = 0

        @sweepDelta = (@clock.to_u64! * 256).tdiv(@sampleRate).to_i32!
        @sweepOffset = 0
        (0x80...0xC9).each { |i| write(i.to_u8!, INITIAL_IO_VALUE[i]) }
      end

      def write(port : UInt8, value : UInt8) : Nil
        @ioRam.put!(port, value)

        case port
        # 0x80-0x87の周波数レジスタについて
        # - ロックマン&フォルテの0x0fの曲では、周波数=0xFFFF の音が不要
        # - デジモンディープロジェクトの0x0dの曲のノイズは 周波数=0x07FF で音を出す
        # →つまり、0xFFFF の時だけ音を出さないってことだろうか。
        #   でも、0x07FF の時も音を出さないけど、ノイズだけ音を出すのかも。
        when 0x80, 0x81
          i : UInt32 = (@ioRam.get!(0x81).to_u32! << 8) + @ioRam.get!(0x80).to_u32!
          freq : Float64 = if i == 0xFFFF
                             0.0
                           else
                             @clock / (2048 - (i & 0x7FF))
                           end
          @audio.get!(0).delta = (freq * 65536.0 / @sampleRate).to_i32!

        when 0x82, 0x83
          i = (@ioRam.get!(0x83).to_u32! << 8) + @ioRam.get!(0x82).to_u32!
          freq = if i == 0xFFFF
                   0.0
                 else
                   @clock / (2048 - (i & 0x7FF))
                 end
          @audio.get!(1).delta = (freq * 65536.0 / @sampleRate).to_i32!

        when 0x84, 0x85
          i = (@ioRam.get!(0x85).to_u32! << 8) + @ioRam.get!(0x84).to_u32!
          freq = if i == 0xFFFF
                   0.0
                 else
                   @clock / (2048 - (i & 0x7FF))
                 end
          @audio.get!(2).delta = (freq * 65536.0 / @sampleRate).to_i32!

        when 0x86, 0x87
          i = (@ioRam.get!(0x87).to_u32! << 8) + @ioRam.get!(0x86).to_u32!
          freq = if i == 0xFFFF
                   0.0
                 else
                   @clock / (2048 - (i & 0x7FF))
                 end
          @audio.get!(3).delta = (freq * 65536.0 / @sampleRate).to_i32!

        when 0x88
          @audio.get!(0).lvol = ((value >> 4) & 0xF).to_i32!
          @audio.get!(0).rvol = (value & 0xF).to_i32!

        when 0x89
          @audio.get!(1).lvol = ((value >> 4) & 0xF).to_i32!
          @audio.get!(1).rvol = (value & 0xF).to_i32!

        when 0x8A
          @audio.get!(2).lvol = ((value >> 4) & 0xF).to_i32!
          @audio.get!(2).rvol = (value & 0xF).to_i32!

        when 0x8B
          @audio.get!(3).lvol = ((value >> 4) & 0xF).to_i32!
          @audio.get!(3).rvol = (value & 0xF).to_i32!

        when 0x8C
          @sweepStep = value.to_i32!

        when 0x8D
          #Sweepの間隔は 1/375[s] = 2.666..[ms]
          #CPU Clockで言うと 3072000/375 = 8192[cycles]
          #ここの設定値をnとすると、8192[cycles]*(n+1) 間隔でSweepすることになる
          #
          #これを HBlank (256cycles) の間隔で言うと、
          #　8192/256 = 32
          #なので、32[HBlank]*(n+1) 間隔となる
          @sweepTime = (value.to_i32! + 1) << 5
          @sweepCount = @sweepTime

        when 0x8E
          @noiseType = (value & 7).to_i32!
          @noiseRng = 1 if Yuno.bitflag?(@noiseRng, 8) # ノイズカウンターリセット

        when 0x8F
          @audio.get!(0).wave = value.to_i32! << 6
          @audio.get!(1).wave = @audio.get!(0).wave &+ 0x10
          @audio.get!(2).wave = @audio.get!(1).wave &+ 0x10
          @audio.get!(3).wave = @audio.get!(2).wave &+ 0x10

        #when 0x90

        when 0x91
          # ここでのボリューム調整は、内蔵Speakerに対しての調整だけらしいので、
          # ヘッドフォン接続されていると認識させれば問題無いらしい。
          @ioRam.put!(port, @ioRam.get!(port) | 0x80)

        #when 0x92, 0x93

        when 0x94
          @pcmVolumeLeft = (value.to_i32! & 0xC) * 2
          @pcmVolumeRight = ((value.to_i32! << 2) & 0xC) * 2
        end
      end

      @[AlwaysInline]
      def writeRam(offset : UInt16, value : UInt8) : Nil
        # RAM - 16 KB (WS) / 64 KB (WSC) internal RAM
        if offset < 0x4000 # We only allocated 16 KB
          @internalRam.put!(offset, value)
        end
      end

      @[AlwaysInline]
      def read(port : UInt8) : UInt8
        @ioRam[port]
      end

      @[AlwaysInline]
      def process : Nil
        if @sweepStep != 0 && Yuno.bitflag?(sndMod, 0x40)
          if @sweepCount < 0
            @sweepCount = @sweepTime
            @sweepFreq = @sweepFreq &+ @sweepStep
            @sweepFreq &= 0x7FF
            freq : Float64 = @clock / (2048 - @sweepFreq)
            @audio.get!(2).delta = (freq * 65536.0 / @sampleRate).to_i32!
          end
          @sweepCount = @sweepCount &- 1
        end
      end

      @[AlwaysInline]
      private def sndMod
        @ioRam.get!(0x90)
      end

      @[AlwaysInline]
      def update(outputs : OutputBuffers, samples : UInt32) : Nil
        outL : Slice(Int32) = outputs[0]
        outR : Slice(Int32) = outputs[1]
        count : Int32 = 0
        w : Int32 = 0
        l : Int32 = 0
        r : Int32 = 0
        masked : Int32 = 0
        xorReg : Int32 = 0

        # Check once, then we can avoid bounds checks below.
        if outL.size < samples || outR.size < samples
          raise "Wonderswan update: Bad number of samples (expected #{samples}, " \
                "but buffers have #{outL.size} and #{outR.size})"
        end

        samples.times do |i|
          @sweepOffset = @sweepOffset &+ @sweepDelta
          while @sweepOffset >= 0x10000
            @sweepOffset -= 0x10000
            process
          end

          l = 0
          r = 0

          @audio.each_with_index do |aud, ch|
            next if aud.muted?

            if ch == 1 && Yuno.bitflag?(sndMod, 0x20)
              # Voice出力
              w = @ioRam.get!(0x89).to_i32!
              w = w &- 0x80
              l = l &+ (@pcmVolumeLeft * w)
              r = r &+ (@pcmVolumeRight * w)
            elsif Yuno.bitflag?(sndMod, 1 << ch)
              if ch == 3 && Yuno.bitflag?(sndMod, 0x80)
                # Noise

                # OSWANの擬似乱数の処理と同等のつもり
                aud.offset = aud.offset &+ aud.delta
                count = aud.offset >> 16
                aud.offset &= 0xFFFF
                while count > 0
                  count -= 1

                  @noiseRng &= NOISE_BIT[@noiseType] - 1
                  if @noiseRng == 0
                    @noiseRng = NOISE_BIT[@noiseType] - 1
                  end

                  masked = @noiseRng & NOISE_MASK[@noiseType]
                  xorReg = 0
                  until masked == 0
                    xorReg ^= masked & 1
                    masked >>= 1
                  end

                  if xorReg != 0
                    @noiseRng |= NOISE_BIT[@noiseType]
                  end
                  @noiseRng >>= 1
                end

                @ioRam.put!(0x92, (@noiseRng & 0xFF).to_u8!)
                @ioRam.put!(0x93, ((@noiseRng >> 8) & 0x7F).to_u8!)

                w = Yuno.bitflag?(@noiseRng, 1) ? 0x7F : -0x80
                l = l &+ (aud.lvol * w)
                r = r &+ (aud.rvol * w)
              else # if ch == 3 && Yuno.bitflag?(sndMod, 0x80)
                aud.offset = aud.offset &+ aud.delta
                count = aud.offset >> 16
                aud.offset &= 0xFFFF
                aud.pos = aud.pos &+ count
                aud.pos &= 0x1F
                w = @internalRam[(aud.wave & 0xFFF0) + (aud.pos >> 1)].to_i32!
                if !Yuno.bitflag?(aud.pos, 1)
                  w = (w << 4) & 0xF0 #下位ニブル
                else
                  w = w & 0xF0 # 上位ニブル
                end
                w = w &- 0x80
                l = l &+ (aud.lvol * w)
                r = r &+ (aud.rvol * w)
              end
            end
          end

          outL.put!(i, l * @mainVolume)
          outR.put!(i, r * @mainVolume)
        end
      end

      def muteMask=(value : UInt32) : Nil
        @audio.each_with_index do |aud, idx|
          aud.muted = Yuno.bitflag?(value >> idx, 0x01)
        end
      end
    end
  end
end
