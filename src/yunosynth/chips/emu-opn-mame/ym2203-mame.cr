#### Software implementation of Yamaha FM sound generator
####
#### Copyright Jarek Burczynski (bujar at mame dot net)
#### Copyright Tatsuyuki Satoh , MultiArcadeMachineEmulator development
#### Copyright (C) 2023-2024 Remilia Scarlet
#### BSD 3-Clause License
require "./opn-mame-common"

####
#### Common Yamaha OPN Mame Routines
####

module Yuno::Chips
  # YM2203 sound chip emulator.
  class YM2203 < Yuno::AbstractChip
    private class YM2203Mame < Yuno::AbstractEmulator
      include Yuno::Chips::OPNMame
      @regs : Bytes = Bytes.new(256)
      @opn : FmOpn
      @updateReq : Proc(Nil)
      @ch : Array(FmCh)

      def initialize(@updateReq : Proc(Nil), parent : YM2203, clock : UInt32, rate : UInt32, ssg : SsgCallbacks)
        initTables

        @ch = Array(FmCh).new(3) { |_| FmCh.new }
        @opn = FmOpn.new(@ch)
        @opn.st.param = parent
        @opn.type = TYPE_YM2203.to_u8!
        @opn.st.clock = clock
        @opn.st.rate = rate
        @opn.st.ssg = ssg
      end

      def reset : Nil
        # Reset prescaler
        opnPrescalerWrite(@opn, 0, 1)

        # Reset SSG section
        @opn.st.ssg.reset.call

        # Status clear
        irqMaskSet(@opn.st, 0x03)
        opnWriteMode(@opn, 0x27, 0x30) # Mode 0, timer reset

        @opn.egTimer = 0
        @opn.egCount = 0

        statusReset(@opn.st, 0xFF)

        resetChannels(@opn.st, @opn.pch, 3)

        # Reset operator parameters
        0xB2.downto(0x30) { |i| opnWriteReg(@opn, i, 0)}
        0x26.downto(0x20) { |i| opnWriteReg(@opn, i, 0)}
      end

      @[AlwaysInline]
      def write(a : Int, v : UInt8) : UInt8
        if (a & 1) == 0
          # Address port
          @opn.st.address = v

          # Write register to SSG emulator
          if v < 16
            @opn.st.ssg.write.call(0, v.to_i32!)
          end

          # Prescaler select : 2F, 2E, 2F
          if v >= 0x2D && v <= 0x2F
            opnPrescalerWrite(@opn, v, 1)
          end
        else
          # Data port
          addr = @opn.st.address
          @regs.put!(addr, v)

          case addr & 0xF0
          when 0x00 # 0x00 - 0x0F # SSG section
            @opn.st.ssg.write.call(a.to_i32!, v.to_i32!)
          when 0x20 # 0x20 - 0x2F : Mode section
            @updateReq.call
            opnWriteMode(@opn, addr.to_i32!, v.to_i32!)
          else # 0x30 - 0xFF : OPN section
            @updateReq.call
            opnWriteReg(@opn, addr.to_i32!, v.to_i32!)
          end
        end

        @opn.st.irq
      end

      @[AlwaysInline]
      def read(a : Int) : UInt8
        addr = @opn.st.address
        if (a & 1) == 0
          # Status port
          @opn.st.status
        else
          # Data port (only SSG)
          if addr < 16
            @opn.st.ssg.read.call
          else
            0u8
          end
        end
      end

      @[AlwaysInline]
      def timerOver(c : Int) : UInt8
        if c != 0
          # Timer B
          timerBOver(@opn.st)
        else
          @updateReq.call

          # Timer A
          timerAOver(@opn.st)

          # CSM mode key, TL control
          if Yuno.bitflag?(@opn.st.mode, 0x80)
            csmKeyControl(@opn.pch.get!(2))
          end
        end

        @opn.st.irq
      end

      @[AlwaysInline]
      def muteMask=(mask : UInt32) : Nil
        @opn.pch.each_with_index do |ch, idx|
          ch.muted = ((mask >> idx) & 1).to_u8!
        end
      end

      @[AlwaysInline]
      def update(outputs : OutputBuffers, samples : UInt32) : Nil
        outL : Slice(Int32) = outputs[0]
        outR : Slice(Int32) = outputs[1]
        sample : Int32 = 0

        # Keeps the code below a bit less verbose.
        cch0 = @ch.unsafe_fetch(0)
        cch1 = @ch.unsafe_fetch(1)
        cch2 = @ch.unsafe_fetch(2)
        st = @opn.st
        sl3fc = @opn.sl3.fc
        sl3kc = @opn.sl3.kcode
        outFm = @opn.outFm

        # Check once, then we can avoid bounds checks below.
        if outL.size < samples || outR.size < samples
          raise "YM2203 update: Bad number of samples (expected #{samples}, " \
                "but buffers have #{outL.size} and #{outR.size})"
        end

        # Refresh PG and EG
        refreshFcEgChan(@opn, cch0)
        refreshFcEgChan(@opn, cch1)
        if Yuno.bitflag?(st.mode, 0xC0)
          # 3-slot mode
          if cch2.slot1.incr == -1
            refreshFcEgSlot(@opn, cch2.slot1, sl3fc.unsafe_fetch(1).to_i32!, sl3kc.unsafe_fetch(1))
            refreshFcEgSlot(@opn, cch2.slot2, sl3fc.unsafe_fetch(2).to_i32!, sl3kc.unsafe_fetch(2))
            refreshFcEgSlot(@opn, cch2.slot3, sl3fc.unsafe_fetch(0).to_i32!, sl3kc.unsafe_fetch(0))
            refreshFcEgSlot(@opn, cch2.slot4, cch2.fc.to_i32!, cch2.kcode)
          end
        else
          refreshFcEgChan(@opn, cch2)
        end

        # The YM2203 doesn't have an LFO, so we must keep these globals at 0
        # level.
        @opn.lfoAm = 0
        @opn.lfoPm = 0

        # Rendering
        samples.times do |i|
          outFm.unsafe_put(0, 0)
          outFm.unsafe_put(1, 0)
          outFm.unsafe_put(2, 0)

          # Advance envelope generator
          @opn.egTimer += @opn.egTimerAdd
          while @opn.egTimer >= @opn.egTimerOverflow
            @opn.egTimer -= @opn.egTimerOverflow
            @opn.egCount += 1

            advanceEgChannel(@opn, cch0.slot)
            advanceEgChannel(@opn, cch1.slot)
            advanceEgChannel(@opn, cch2.slot)
          end

          # Calculate FM
          chanCalc(@opn, cch0, 0)
          chanCalc(@opn, cch1, 1)
          chanCalc(@opn, cch2, 2)

          sample = outFm.unsafe_fetch(0) &+ outFm.unsafe_fetch(1) &+ outFm.unsafe_fetch(2)
          outL.unsafe_put(i, sample)
          outR.unsafe_put(i, sample)

          # Timer A control
          internalTimerA(st, cch2)
        end

        internalTimerB(st, samples)
      end

      def setSampleRateChangeCB(sampleRateFunc : SampleRateCallback, sampleRateData : AbstractChip) : Nil
        # Effectively does nothing since this isn't needed for the Emu2149 core.
      end
    end
  end
end
