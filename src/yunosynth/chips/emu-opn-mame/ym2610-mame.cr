#### Software implementation of Yamaha FM sound generator
####
#### Copyright Jarek Burczynski (bujar at mame dot net)
#### Copyright Tatsuyuki Satoh , MultiArcadeMachineEmulator development
#### Copyright (C) 2023-2024 Remilia Scarlet
#### BSD 3-Clause License
require "./ym2608-mame"

module Yuno::Chips
  # YM2610 sound chip emulator.
  class YM2610 < Yuno::AbstractChip
    private class YM2610Mame < YM2608::YM2608Mame
      include Yuno::Chips::OPNMame

      def initialize(@updateReq : Proc(Nil), param : AbstractChip?, clock : UInt32, rate : UInt32, ssg : SsgCallbacks)
        @adpcm = Array(Channel).new(6) { |_| Channel.new }
        @pcmSize = 0

        initTables

        @ch = Array(FmCh).new(6) { |_| FmCh.new }
        @opn = FmOpn.new(@ch)
        @opn.st.param = param
        @opn.type = TYPE_YM2610.to_u8
        @opn.st.clock = clock
        @opn.st.rate = rate

        # External handlers
        @opn.st.ssg = ssg

        # Delta-T
        @deltaT.memory = Bytes.new(0)
        @deltaT.memoryMask = 0

        @deltaT.statusSetHandler = ->deltaTStatusSet(Yuno::AbstractEmulator, UInt8)
        @deltaT.statusResetHandler = ->deltaTStatusReset(Yuno::AbstractEmulator, UInt8)
        @deltaT.statusChangeWhichChip = self
        @deltaT.statusChangeEOSBit = 0x80 # Status flag: set bit 2 on End of Sample

        initAdpcmATable
      end

      # Generate samples for the YM2610.
      @[AlwaysInline]
      def update(outputs : OutputBuffers, samples : UInt32) : Nil
        outFm = @opn.outFm
        outL : Slice(Int32) = outputs[0]
        outR : Slice(Int32) = outputs[1]
        lt : Int32 = 0
        rt : Int32 = 0

        # Remi: "1, 2, 4, 5" is correct.
        cch0 : FmCh = {% if flag?(:yunosynth_wd40) %} @ch.unsafe_fetch(1) {% else %} @ch[1] {% end %}
        cch1 : FmCh = {% if flag?(:yunosynth_wd40) %} @ch.unsafe_fetch(2) {% else %} @ch[2] {% end %}
        cch2 : FmCh = {% if flag?(:yunosynth_wd40) %} @ch.unsafe_fetch(4) {% else %} @ch[4] {% end %}
        cch3 : FmCh = {% if flag?(:yunosynth_wd40) %} @ch.unsafe_fetch(5) {% else %} @ch[5] {% end %}

        # Check once, then we can avoid bounds checks below.
        if outL.size < samples || outR.size < samples
          raise "YM210 update: Bad number of samples (expected #{samples}, " \
                "but buffers have #{outL.size} and #{outR.size})"
        end

        # Refresh PG and EG
        refreshFcEgChan(@opn, cch0)
        if Yuno.bitflag?(@opn.st.mode, 0xC0)
          # 3-slot mode
          if cch1.slot1.incr == -1
            refreshFcEgSlot(@opn, cch1.slot1, @opn.sl3.fc.get!(1).to_i32!, @opn.sl3.kcode.get!(1))
            refreshFcEgSlot(@opn, cch1.slot2, @opn.sl3.fc.get!(2).to_i32!, @opn.sl3.kcode.get!(2))
            refreshFcEgSlot(@opn, cch1.slot3, @opn.sl3.fc.get!(0).to_i32!, @opn.sl3.kcode.get!(0))
            refreshFcEgSlot(@opn, cch1.slot4, cch1.fc.to_i32!, cch1.kcode)
          end
        else
          refreshFcEgChan(@opn, cch1)
        end
        refreshFcEgChan(@opn, cch2)
        refreshFcEgChan(@opn, cch3)

        # Render audio
        samples.times do |i|
          advanceLfo(@opn)

          # Clear output acc.
          @opn.outAdpcm.put!(OUTD_LEFT, 0)
          @opn.outAdpcm.put!(OUTD_RIGHT, 0)
          @opn.outAdpcm.put!(OUTD_CENTER, 0)
          @opn.outDelta.put!(OUTD_LEFT, 0)
          @opn.outDelta.put!(OUTD_RIGHT, 0)
          @opn.outDelta.put!(OUTD_CENTER, 0)

          # Clear outputs
          outFm.put!(1, 0)
          outFm.put!(2, 0)
          outFm.put!(4, 0)
          outFm.put!(5, 0)

          # Advance envelope generator
          @opn.egTimer += @opn.egTimerAdd
          while @opn.egTimer >= @opn.egTimerOverflow
            @opn.egTimer -= @opn.egTimerOverflow
            @opn.egCount += 1
            {% for i in 0..3 %}
              advanceEgChannel(@opn, cch{{i}}.slot)
            {% end %}
          end

          # Calculate FM
          chanCalc(@opn, cch0, 1u32) # Remapped to 1
          chanCalc(@opn, cch1, 2u32) # Remapped to 2
          chanCalc(@opn, cch2, 4u32) # Remapped to 4
          chanCalc(@opn, cch3, 5u32) # Remapped to 5

          # DeltaT ADPCM
          if Yuno.bitflag?(@deltaT.portState, 0x80) && @muteDeltaT == 0
            @deltaT.adpcmCalc
          end

          # ADPCM A
          6.times do |j|
            {% if flag?(:yunosynth_wd40) %}
              if @adpcm.unsafe_fetch(j).flag != 0
                adpcmaCalcChan(@adpcm.unsafe_fetch(j))
              end
            {% else %}
              if @adpcm.get!(j).flag != 0
                adpcmaCalcChan(@adpcm.get!(j))
              end
            {% end %}
          end

          # Write audio data
          lt =  (@opn.outAdpcm.get!(OUTD_LEFT)  + @opn.outAdpcm.get!(OUTD_CENTER)) << 1
          rt =  (@opn.outAdpcm.get!(OUTD_RIGHT) + @opn.outAdpcm.get!(OUTD_CENTER)) << 1
          lt += (@opn.outDelta.get!(OUTD_LEFT)  + @opn.outDelta.get!(OUTD_CENTER)) >> 8
          rt += (@opn.outDelta.get!(OUTD_RIGHT) + @opn.outDelta.get!(OUTD_CENTER)) >> 8

          lt += (outFm.get!(1) & @opn.pan.get!(2))
          rt += (outFm.get!(1) & @opn.pan.get!(3))
          lt += (outFm.get!(2) & @opn.pan.get!(4))
          rt += (outFm.get!(2) & @opn.pan.get!(5))

          lt += (outFm.get!(4) & @opn.pan.get!(8))
          rt += (outFm.get!(4) & @opn.pan.get!(9))
          lt += (outFm.get!(5) & @opn.pan.get!(10))
          rt += (outFm.get!(5) & @opn.pan.get!(11))

          outL.unsafe_put(i, lt)
          outR.unsafe_put(i, rt)

          # Timer A Control
          internalTimerA(@opn.st, cch1)
        end

        internalTimerB(@opn.st, samples)
      end

      # Generate samples for the YM2610B.
      @[AlwaysInline]
      def update2610B(outputs : OutputBuffers, samples : UInt32) : Nil
        outFm = @opn.outFm
        outL : Slice(Int32) = outputs[0]
        outR : Slice(Int32) = outputs[1]
        lt : Int32 = 0
        rt : Int32 = 0

        cch0 : FmCh = {% if flag?(:yunosynth_wd40) %} @ch.unsafe_fetch(0) {% else %} @ch[0] {% end %}
        cch1 : FmCh = {% if flag?(:yunosynth_wd40) %} @ch.unsafe_fetch(1) {% else %} @ch[1] {% end %}
        cch2 : FmCh = {% if flag?(:yunosynth_wd40) %} @ch.unsafe_fetch(2) {% else %} @ch[2] {% end %}
        cch3 : FmCh = {% if flag?(:yunosynth_wd40) %} @ch.unsafe_fetch(3) {% else %} @ch[3] {% end %}
        cch4 : FmCh = {% if flag?(:yunosynth_wd40) %} @ch.unsafe_fetch(4) {% else %} @ch[4] {% end %}
        cch5 : FmCh = {% if flag?(:yunosynth_wd40) %} @ch.unsafe_fetch(5) {% else %} @ch[5] {% end %}

        # Check once, then we can avoid bounds checks below.
        if outL.size < samples || outR.size < samples
          raise "YM210 update: Bad number of samples (expected #{samples}, " \
                "but buffers have #{outL.size} and #{outR.size})"
        end

        # Refresh PG and EG
        refreshFcEgChan(@opn, cch0)
        refreshFcEgChan(@opn, cch1)
        if Yuno.bitflag?(@opn.st.mode, 0xC0)
          # 3-slot mode
          if cch2.slot1.incr == -1
            refreshFcEgSlot(@opn, cch2.slot1, @opn.sl3.fc.get!(1).to_i32!, @opn.sl3.kcode.get!(1))
            refreshFcEgSlot(@opn, cch2.slot2, @opn.sl3.fc.get!(2).to_i32!, @opn.sl3.kcode.get!(2))
            refreshFcEgSlot(@opn, cch2.slot3, @opn.sl3.fc.get!(0).to_i32!, @opn.sl3.kcode.get!(0))
            refreshFcEgSlot(@opn, cch2.slot4, cch2.fc.to_i32!, cch2.kcode)
          end
        else
          refreshFcEgChan(@opn, cch2)
        end
        refreshFcEgChan(@opn, cch3)
        refreshFcEgChan(@opn, cch4)
        refreshFcEgChan(@opn, cch5)

        # Render audio
        samples.times do |i|
          advanceLfo(@opn)

          # Clear output acc.
          @opn.outAdpcm.put!(OUTD_LEFT, 0)
          @opn.outAdpcm.put!(OUTD_RIGHT, 0)
          @opn.outAdpcm.put!(OUTD_CENTER, 0)
          @opn.outDelta.put!(OUTD_LEFT, 0)
          @opn.outDelta.put!(OUTD_RIGHT, 0)
          @opn.outDelta.put!(OUTD_CENTER, 0)

          # Clear outputs
          outFm.fill(0, 0, 6)

          # Advance envelope generator
          @opn.egTimer += @opn.egTimerAdd
          while @opn.egTimer >= @opn.egTimerOverflow
            @opn.egTimer -= @opn.egTimerOverflow
            @opn.egCount += 1
            {% for i in 0..5 %}
              advanceEgChannel(@opn, cch{{i}}.slot)
            {% end %}
          end

          # Calculate FM
          {% for i in 0..5 %}
            chanCalc(@opn, cch{{i}}, {{i}}u32)
          {% end %}

          # DeltaT ADPCM
          if Yuno.bitflag?(@deltaT.portState, 0x80) && @muteDeltaT == 0
            @deltaT.adpcmCalc
          end

          # ADPCM A
          6.times do |j|
            {% if flag?(:yunosynth_wd40) %}
              if @adpcm.unsafe_fetch(j).flag != 0
                adpcmaCalcChan(@adpcm.unsafe_fetch(j))
              end
            {% else %}
              if @adpcm.get!(j).flag != 0
                adpcmaCalcChan(@adpcm.get!(j))
              end
            {% end %}
          end

          # Write audio data
          lt =  (@opn.outAdpcm.get!(OUTD_LEFT)  + @opn.outAdpcm.get!(OUTD_CENTER)) << 1
          rt =  (@opn.outAdpcm.get!(OUTD_RIGHT) + @opn.outAdpcm.get!(OUTD_CENTER)) << 1
          lt += (@opn.outDelta.get!(OUTD_LEFT)  + @opn.outDelta.get!(OUTD_CENTER)) >> 8
          rt += (@opn.outDelta.get!(OUTD_RIGHT) + @opn.outDelta.get!(OUTD_CENTER)) >> 8

          lt += (outFm.get!(0) & @opn.pan.get!(0))
          rt += (outFm.get!(0) & @opn.pan.get!(1))
          lt += (outFm.get!(1) & @opn.pan.get!(2))
          rt += (outFm.get!(1) & @opn.pan.get!(3))
          lt += (outFm.get!(2) & @opn.pan.get!(4))
          rt += (outFm.get!(2) & @opn.pan.get!(5))
          lt += (outFm.get!(3) & @opn.pan.get!(6))
          rt += (outFm.get!(3) & @opn.pan.get!(7))
          lt += (outFm.get!(4) & @opn.pan.get!(8))
          rt += (outFm.get!(4) & @opn.pan.get!(9))
          lt += (outFm.get!(5) & @opn.pan.get!(10))
          rt += (outFm.get!(5) & @opn.pan.get!(11))

          outL.unsafe_put(i, lt)
          outR.unsafe_put(i, rt)

          # Timer A Control
          internalTimerA(@opn.st, cch2)
        end

        internalTimerB(@opn.st, samples)
      end

      def deltaTStatusSet(chip : Yuno::AbstractEmulator, changeBits : UInt8) : Nil
        @adpcmArrivedEndAddress |= changeBits
      end

      def deltaTStatusReset(chip : Yuno::AbstractEmulator, changeBits : UInt8) : Nil
        @adpcmArrivedEndAddress &= (~changeBits)
      end

      def reset : Nil
        opnSetPres(@opn, 6 * 24, 6 * 24, 4 * 2) # OPN 1/6, SSG 1/4

        # Reset SSG section
        @opn.st.ssg.reset.call

        # Status clear
        irqMaskSet(@opn.st, 0x03)
        opnWriteMode(@opn, 0x27, 0x30) # Mode 0, timer reset

        @opn.egTimer = 0
        @opn.egCount = 0

        statusReset(@opn.st, 0xFF)

        resetChannels(@opn.st, @opn.pch, 6)

        # Reset operator parameters
        0xB6.downto(0xB4) do |i|
          opnWriteReg(@opn, i,         0xC0)
          opnWriteReg(@opn, i | 0x100, 0xC0)
        end

        0xB2.downto(0x30) do |i|
          opnWriteReg(@opn, i,         0)
          opnWriteReg(@opn, i | 0x100, 0)
        end

        0x26.downto(0x20) do |i|
          opnWriteReg(@opn, i, 0)
        end

        # ADPCM work initial
        6.times do |i|
          @adpcm[i].step = ((1 << ADPCM_SHIFT).to_f64! * @opn.st.freqBase.to_f64! / 3.0).to_u32!
          @adpcm[i].nowAddr = 0
          @adpcm[i].nowStep = 0
          @adpcm[i].start = 0
          @adpcm[i].finish = 0
          @adpcm[i].volMul = 0i8
          @adpcm[i].pan = @opn.outAdpcm.to_unsafe + OUTD_CENTER # Default center
          @adpcm[i].flagMask = 1u8 << i
          @adpcm[i].flag = 0
          @adpcm[i].adpcmAcc = 0
          @adpcm[i].adpcmStep = 0
          @adpcm[i].adpcmOut = 0
        end

        @adpcmTl = 0x3F_u8
        @adpcmArrivedEndAddress = 0

        # Delta-T unit
        @deltaT.freqBase = @opn.st.freqBase
        @deltaT.outputPointer = Slice.new(@opn.outDelta.to_unsafe, @opn.outDelta.size)
        @deltaT.portShift = 8u8 # Always 8-bit shift
        @deltaT.outputRange = 1 << 23
        @deltaT.adpcmReset(OUTD_CENTER, EMULATION_MODE_YM2610)
      end

      @[AlwaysInline]
      def write(a : Int, v : UInt8) : Int32
        addr : Int32 = 0

        case a & 3
        when 0 # Address port 0
          @opn.st.address = v
          @addrA1 = 0

          # Write registers to SSG emulator
          if v < 16
            @opn.st.ssg.write.call(0, v.to_i32!)
          end

        when 1 # Data port 0
          if @addrA1 == 0 # Verified on real YM2608
            addr = @opn.st.address.to_i32!
            @regs.put!(addr, v)

            case addr & 0xF0
            when 0x00 # SSG section
              # Write data to SSG emulator
              @opn.st.ssg.write.call(a.to_i32!, v.to_i32!)

            when 0x10 # DeltaT ADPCM
              @updateReq.call

              case addr
              when 0x10, # Control 1
                   0x11, # Control 2
                   0x12, # Start address low
                   0x13, # Start address high
                   0x14, # Stop address low
                   0x15, # Stop address high
                   0x19, # Delta-n low
                   0x1A, # Delta-n high
                   0x1B  # Volume
                @deltaT.adpcmWrite(addr - 0x10, v)

              when 0x1C # Flag Control: Extend status clear/mask
                statusMask : UInt8 = ~v
                # Set arrived flag mask
                6.times do |i|
                  @adpcm.get!(i).flagMask = statusMask & (1u8 << i)
                end

                @deltaT.statusChangeEOSBit = statusMask & 0x80 # Status flag: bit 7 on End of Sample

                # Clear arrived flag
                @adpcmArrivedEndAddress &= statusMask

              else
                {% if flag?(:yunosynth_debug) %}
                  Yuno.warn(sprintf("YM2610: Write to unknown Delta-T register: %02x$ = $02x$", addr, v))
                {% end %}
              end

            when 0x20 # Mode register
              @updateReq.call
              opnWriteMode(@opn, addr, v.to_i32!)

            else # OPN section
              @updateReq.call
              opnWriteReg(@opn, addr, v.to_i32!)
            end
          end

        when 2 # Address port 1
          @opn.st.address = v
          @addrA1 = 1

        when 3 # Data port 1
          if @addrA1 == 1 # Verified on real YM2608
            @updateReq.call
            addr = @opn.st.address.to_i32!
            @regs.put!(addr | 0x100, v)

            if addr < 0x30
              # 100-12F: ADPCM A section
              adpcmAWrite(addr, v)
            else
              opnWriteReg(@opn, addr.to_i32! | 0x100, v.to_i32!)
            end
          end
        end

        @opn.st.irq.to_i32!
      end

      @[AlwaysInline]
      def read(a : Int) : UInt8
        ret : UInt8 = 0
        addr : UInt8 = @opn.st.address

        case a & 3
        when 0 # Status 0: YM2203 compatible
          ret = @opn.st.status & 0x83

        when 1 # Data 0
          if addr < 16
            ret = @opn.st.ssg.read.call
          elsif addr == 0xFF
            ret = 0x01 # ID code
          end

        when 2 # Status 1: ADPCM status
          # ADPCM STATUS (arrived End Address)
          # B,--,A5,A4,A3,A2,A1,A0
          # B     = ADPCM-B(DELTA-T) arrived end address
          # A0-A5 = ADPCM-A          arrived end address
          ret = @adpcmArrivedEndAddress

        when 3
          ret = 0u8
        end

        ret
      end

      def timerOver(c : Int) : Int32
        if c != 0
          # Timer B
          timerBOver(@opn.st)

        else
          # Timer A
          @updateReq.call

          # Timer update
          timerAOver(@opn.st)

          # CSM mode key, TL control
          if Yuno.bitflag?(@opn.st.mode, 0x80)
            # CSM mode total level latch and auto key on
            csmKeyControl(@ch.get!(2))
          end
        end

        @opn.st.irq
      end

      def writePcmRom(romID : UInt8, romSize : Int32, dataStart : Int32, dataLength : Int32,
                      romData : Slice(UInt8)) : Nil
        case romID
        when 0x01 # ADPCM
          if @pcmSize != romSize
            @pcmBuf = Array(UInt8).new(romSize, 0xFF)
            @pcmSize = romSize.to_u32!
          end

          return if dataStart > romSize
          dataLength = romSize - dataStart if (dataStart + dataLength) > romSize

          # Copy the memory
          dataLength.times do |i|
            # This can be guaranteed by the checks above
            @pcmBuf.unsafe_put(i + dataStart, romData.unsafe_fetch(i))
          end

        when 0x02 # Delta-T
          if @deltaT.memory.size != romSize
            @deltaT.memory = Bytes.new(romSize, 0xFFu8)
            @deltaT.calcMemMask
          end

          return if dataStart > romSize
          dataLength = romSize - dataStart if (dataStart + dataLength) > romSize

          # Copy the memory
          dataLength.times do |i|
            # This can be guaranteed by the checks above
            @deltaT.memory.unsafe_put(i + dataStart, romData.unsafe_fetch(i))
          end
        end
      end

      def muteMask=(mask : UInt32) : Nil
        6.times do |ch|
          @ch[ch].muted = ((mask >> ch) & 0x01).to_u8!
        end

        6.times do |ch|
          @adpcm[ch].muted = ((mask >> (ch + 6)) & 0x01).to_u8!
        end

        @muteDeltaT = ((mask >> 12) & 0x01).to_u8!
      end
    end
  end
end
