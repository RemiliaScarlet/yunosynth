#### Software implementation of Yamaha FM sound generator
####
#### Copyright Jarek Burczynski (bujar at mame dot net)
#### Copyright Tatsuyuki Satoh , MultiArcadeMachineEmulator development
#### Copyright (C) 2023-2024 Remilia Scarlet
#### BSD 3-Clause License
####
#### Version 1.5.1 (Genesis Plus GX ym2612.c rev. 368)
####
#### History:
####
#### 2006~2012  Eke-Eke (Genesis Plus GX):
#### Huge thanks to Nemesis, lot of those fixes came from his tests on Sega Genesis hardware
#### More informations at http://gendev.spritesmind.net/forum/viewtopic.php?t=386
####
####  TODO:
####
####  - core documentation
####  - BUSY flag support
####
####  CHANGELOG:
####
####  - fixed LFO implementation:
####      .added support for CH3 special mode: fixes various sound effects (birds in Warlock, bug sound in Aladdin...)
####      .inverted LFO AM waveform: fixes Spider-Man & Venom : Separation Anxiety (intro), California Games (surfing event)
####      .improved LFO timing accuracy: now updated AFTER sample output, like EG/PG updates, and without any precision loss anymore.
####  - improved internal timers emulation
####  - adjusted lowest EG rates increment values
####  - fixed Attack Rate not being updated in some specific cases (Batman & Robin intro)
####  - fixed EG behavior when Attack Rate is maximal
####  - fixed EG behavior when SL=0 (Mega Turrican tracks 03,09...) or/and Key ON occurs at minimal attenuation
####  - implemented EG output immediate changes on register writes
####  - fixed YM2612 initial values (after the reset): fixes missing intro in B.O.B
####  - implemented Detune overflow (Ariel, Comix Zone, Shaq Fu, Spiderman & many other games using GEMS sound engine)
####  - implemented accurate CSM mode emulation
####  - implemented accurate SSG-EG emulation (Asterix, Beavis&Butthead, Bubba'n Stix & many other games)
####  - implemented accurate address/data ports behavior
####
#### 06-23-2007 Zsolt Vasvari:
####  - changed the timing not to require the use of floating point calculations
####
#### 03-08-2003 Jarek Burczynski:
####  - fixed YM2608 initial values (after the reset)
####  - fixed flag and irqmask handling (YM2608)
####  - fixed BUFRDY flag handling (YM2608)
####
#### 14-06-2003 Jarek Burczynski:
####  - implemented all of the YM2608 status register flags
####  - implemented support for external memory read/write via YM2608
####  - implemented support for deltat memory limit register in YM2608 emulation
####
#### 22-05-2003 Jarek Burczynski:
####  - fixed LFO PM calculations (copy&paste bugfix)
####
#### 08-05-2003 Jarek Burczynski:
####  - fixed SSG support
####
#### 22-04-2003 Jarek Burczynski:
####  - implemented 100% correct LFO generator (verified on real YM2610 and YM2608)
####
#### 15-04-2003 Jarek Burczynski:
####  - added support for YM2608's register 0x110 - status mask
####
#### 01-12-2002 Jarek Burczynski:
####  - fixed register addressing in YM2608, YM2610, YM2610B chips. (verified on real YM2608)
####    The addressing patch used for early Neo-Geo games can be removed now.
####
#### 26-11-2002 Jarek Burczynski, Nicola Salmoria:
####  - recreated YM2608 ADPCM ROM using data from real YM2608's output which leads to:
####  - added emulation of YM2608 drums.
####  - output of YM2608 is two times lower now - same as YM2610 (verified on real YM2608)
####
#### 16-08-2002 Jarek Burczynski:
####  - binary exact Envelope Generator (verified on real YM2203);
####    identical to YM2151
####  - corrected 'off by one' error in feedback calculations (when feedback is off)
####  - corrected connection (algorithm) calculation (verified on real YM2203 and YM2610)
####
#### 18-12-2001 Jarek Burczynski:
####  - added SSG-EG support (verified on real YM2203)
####
#### 12-08-2001 Jarek Burczynski:
####  - corrected sin_tab and tl_tab data (verified on real chip)
####  - corrected feedback calculations (verified on real chip)
####  - corrected phase generator calculations (verified on real chip)
####  - corrected envelope generator calculations (verified on real chip)
####  - corrected FM volume level (YM2610 and YM2610B).
####  - changed YMxxxUpdateOne() functions (YM2203, YM2608, YM2610, YM2610B, YM2612) :
####    this was needed to calculate YM2610 FM channels output correctly.
####    (Each FM channel is calculated as in other chips, but the output of the channel
####    gets shifted right by one *before* sending to accumulator. That was impossible to do
####    with previous implementation).
####
#### 23-07-2001 Jarek Burczynski, Nicola Salmoria:
####  - corrected YM2610 ADPCM type A algorithm and tables (verified on real chip)
####
#### 11-06-2001 Jarek Burczynski:
####  - corrected end of sample bug in ADPCMA_calc_cha().
####    Real YM2610 checks for equality between current and end addresses (only 20 LSB bits).
####
#### 08-12-98 hiro-shi:
#### rename ADPCMA -> ADPCMB, ADPCMB -> ADPCMA
#### move ROM limit check.(CALC_CH? -> 2610Write1/2)
#### test program (ADPCMB_TEST)
#### move ADPCM A/B end check.
#### ADPCMB repeat flag(no check)
#### change ADPCM volume rate (8->16) (32->48).
####
#### 09-12-98 hiro-shi:
#### change ADPCM volume. (8->16, 48->64)
#### replace ym2610 ch0/3 (YM-2610B)
#### change ADPCM_SHIFT (10->8) missing bank change 0x4000-0xffff.
#### add ADPCM_SHIFT_MASK
#### change ADPCMA_DECODE_MIN/MAX.

require "./opn-mame-common"

module Yuno::Chips
  # YM2612 sound chip emulator.
  class YM2612 < Yuno::AbstractChip
    private class YM2612Mame < Yuno::AbstractEmulator
      include Yuno::Chips::OPNMame

      # Envelope Generator rates (32 + 64 rates + 32 RKS).
      #
      # note that there is no O(17) in this table - it's directly in the code.
      EG_RATE_SELECT_2612 = [
        # 32 infinite time rates (same as Rate 0)
        (18 * RATE_STEPS).to_u8!, (18 * RATE_STEPS).to_u8!, (18 * RATE_STEPS).to_u8!, (18 * RATE_STEPS).to_u8!,
        (18 * RATE_STEPS).to_u8!, (18 * RATE_STEPS).to_u8!, (18 * RATE_STEPS).to_u8!, (18 * RATE_STEPS).to_u8!,
        (18 * RATE_STEPS).to_u8!, (18 * RATE_STEPS).to_u8!, (18 * RATE_STEPS).to_u8!, (18 * RATE_STEPS).to_u8!,
        (18 * RATE_STEPS).to_u8!, (18 * RATE_STEPS).to_u8!, (18 * RATE_STEPS).to_u8!, (18 * RATE_STEPS).to_u8!,
        (18 * RATE_STEPS).to_u8!, (18 * RATE_STEPS).to_u8!, (18 * RATE_STEPS).to_u8!, (18 * RATE_STEPS).to_u8!,
        (18 * RATE_STEPS).to_u8!, (18 * RATE_STEPS).to_u8!, (18 * RATE_STEPS).to_u8!, (18 * RATE_STEPS).to_u8!,
        (18 * RATE_STEPS).to_u8!, (18 * RATE_STEPS).to_u8!, (18 * RATE_STEPS).to_u8!, (18 * RATE_STEPS).to_u8!,
        (18 * RATE_STEPS).to_u8!, (18 * RATE_STEPS).to_u8!, (18 * RATE_STEPS).to_u8!, (18 * RATE_STEPS).to_u8!,

        # rates 00-11
        #(0 * RATE_STEPS).to_u8!, (1 * RATE_STEPS).to_u8!, (2 * RATE_STEPS).to_u8!, (3 * RATE_STEPS).to_u8!,
        #(0 * RATE_STEPS).to_u8!, (1 * RATE_STEPS).to_u8!, (2 * RATE_STEPS).to_u8!, (3 * RATE_STEPS).to_u8!,
        # Nemesis's tests
        (18 * RATE_STEPS).to_u8!, (18 * RATE_STEPS).to_u8!, (0 * RATE_STEPS).to_u8!, (0 * RATE_STEPS).to_u8!,
        (0 * RATE_STEPS).to_u8!, (0 * RATE_STEPS).to_u8!, (2 * RATE_STEPS).to_u8!, (2 * RATE_STEPS).to_u8!,

        (0 * RATE_STEPS).to_u8!, (1 * RATE_STEPS).to_u8!, (2 * RATE_STEPS).to_u8!, (3 * RATE_STEPS).to_u8!,
        (0 * RATE_STEPS).to_u8!, (1 * RATE_STEPS).to_u8!, (2 * RATE_STEPS).to_u8!, (3 * RATE_STEPS).to_u8!,
        (0 * RATE_STEPS).to_u8!, (1 * RATE_STEPS).to_u8!, (2 * RATE_STEPS).to_u8!, (3 * RATE_STEPS).to_u8!,
        (0 * RATE_STEPS).to_u8!, (1 * RATE_STEPS).to_u8!, (2 * RATE_STEPS).to_u8!, (3 * RATE_STEPS).to_u8!,
        (0 * RATE_STEPS).to_u8!, (1 * RATE_STEPS).to_u8!, (2 * RATE_STEPS).to_u8!, (3 * RATE_STEPS).to_u8!,
        (0 * RATE_STEPS).to_u8!, (1 * RATE_STEPS).to_u8!, (2 * RATE_STEPS).to_u8!, (3 * RATE_STEPS).to_u8!,
        (0 * RATE_STEPS).to_u8!, (1 * RATE_STEPS).to_u8!, (2 * RATE_STEPS).to_u8!, (3 * RATE_STEPS).to_u8!,
        (0 * RATE_STEPS).to_u8!, (1 * RATE_STEPS).to_u8!, (2 * RATE_STEPS).to_u8!, (3 * RATE_STEPS).to_u8!,
        (0 * RATE_STEPS).to_u8!, (1 * RATE_STEPS).to_u8!, (2 * RATE_STEPS).to_u8!, (3 * RATE_STEPS).to_u8!,
        (0 * RATE_STEPS).to_u8!, (1 * RATE_STEPS).to_u8!, (2 * RATE_STEPS).to_u8!, (3 * RATE_STEPS).to_u8!,

        # rate 12
        (4 * RATE_STEPS).to_u8!, (5 * RATE_STEPS).to_u8!, (6 * RATE_STEPS).to_u8!, (7 * RATE_STEPS).to_u8!,

        # rate 13
        (8 * RATE_STEPS).to_u8!, (9 * RATE_STEPS).to_u8!, (10 * RATE_STEPS).to_u8!, (11 * RATE_STEPS).to_u8!,

        # rate 14
        (12 * RATE_STEPS).to_u8!, (13 * RATE_STEPS).to_u8!, (14 * RATE_STEPS).to_u8!, (15 * RATE_STEPS).to_u8!,

        # rate 15
        (16 * RATE_STEPS).to_u8!, (16 * RATE_STEPS).to_u8!, (16 * RATE_STEPS).to_u8!, (16 * RATE_STEPS).to_u8!,

        # 32 fake rates (same as 15 3)
        (16 * RATE_STEPS).to_u8!, (16 * RATE_STEPS).to_u8!, (16 * RATE_STEPS).to_u8!, (16 * RATE_STEPS).to_u8!,
        (16 * RATE_STEPS).to_u8!, (16 * RATE_STEPS).to_u8!, (16 * RATE_STEPS).to_u8!, (16 * RATE_STEPS).to_u8!,
        (16 * RATE_STEPS).to_u8!, (16 * RATE_STEPS).to_u8!, (16 * RATE_STEPS).to_u8!, (16 * RATE_STEPS).to_u8!,
        (16 * RATE_STEPS).to_u8!, (16 * RATE_STEPS).to_u8!, (16 * RATE_STEPS).to_u8!, (16 * RATE_STEPS).to_u8!,
        (16 * RATE_STEPS).to_u8!, (16 * RATE_STEPS).to_u8!, (16 * RATE_STEPS).to_u8!, (16 * RATE_STEPS).to_u8!,
        (16 * RATE_STEPS).to_u8!, (16 * RATE_STEPS).to_u8!, (16 * RATE_STEPS).to_u8!, (16 * RATE_STEPS).to_u8!,
        (16 * RATE_STEPS).to_u8!, (16 * RATE_STEPS).to_u8!, (16 * RATE_STEPS).to_u8!, (16 * RATE_STEPS).to_u8!,
        (16 * RATE_STEPS).to_u8!, (16 * RATE_STEPS).to_u8!, (16 * RATE_STEPS).to_u8!, (16 * RATE_STEPS).to_u8!
      ] of UInt8

      # Envelope Generator counter shifts (32 + 64 rates + 32 RKS).
      EG_RATE_SHIFT_2612 = [
        # Note: These are different for 2612 compared to normal OPN.
        11u8, 11u8, 11u8, 11u8, 11u8, 11u8, 11u8, 11u8,
        11u8, 11u8, 11u8, 11u8, 11u8, 11u8, 11u8, 11u8,
        11u8, 11u8, 11u8, 11u8, 11u8, 11u8, 11u8, 11u8,
        11u8, 11u8, 11u8, 11u8, 11u8, 11u8, 11u8, 11u8,

        # rates 00-11
        11u8, 11u8, 11u8, 11u8,
        10u8, 10u8, 10u8, 10u8,
        9u8, 9u8, 9u8, 9u8,
        8u8, 8u8, 8u8, 8u8,
        7u8, 7u8, 7u8, 7u8,
        6u8, 6u8, 6u8, 6u8,
        5u8, 5u8, 5u8, 5u8,
        4u8, 4u8, 4u8, 4u8,
        3u8, 3u8, 3u8, 3u8,
        2u8, 2u8, 2u8, 2u8,
        1u8, 1u8, 1u8, 1u8,
        0u8, 0u8, 0u8, 0u8,

        # rate 12
        0u8, 0u8, 0u8, 0u8,

        # rate 13
        0u8, 0u8, 0u8, 0u8,

        # rate 14
        0u8, 0u8, 0u8, 0u8,

        # rate 15
        0u8, 0u8, 0u8, 0u8,

        # 32 fake rates (same as 15 3)
        0u8, 0u8, 0u8, 0u8, 0u8, 0u8, 0u8, 0u8,
        0u8, 0u8, 0u8, 0u8, 0u8, 0u8, 0u8, 0u8,
        0u8, 0u8, 0u8, 0u8, 0u8, 0u8, 0u8, 0u8,
        0u8, 0u8, 0u8, 0u8, 0u8, 0u8, 0u8, 0u8
      ] of UInt8

      @regs : Bytes = Bytes.new(512)  # Registers
      @opn : FmOpn                    # OPN state
      @ch : Array(FmCh)               # Channel state
      @addrA1 : UInt8 = 0             # Address line A1
      @updateReq : Proc(Nil)

      @waveOutMode : UInt8
      @waveL : Int32 = 0
      @waveR : Int32 = 0

      ##
      ## DAC Output
      ##
      @dacen : UInt8 = 0
      @dacTest : UInt8 = 0
      @dacOut : Int32 = 0
      @muteDac : UInt8 = 0

      protected property? isVgmInit : Bool = false

      def initialize(@updateReq : Proc(Nil), parent : YM2612, clock : UInt32, rate : UInt32, flags : UInt8)
        initTables

        @ch = Array(FmCh).new(6) { |_| FmCh.new }
        @opn = FmOpn.new(@ch)
        @opn.st.param = parent
        @opn.type = TYPE_YM2612.to_u8!
        @opn.st.clock = clock
        @opn.st.rate = rate

        @waveOutMode = if Yuno.bitflag?(flags >> 2, 0x01) # pseudo stereo
                         0x01u8
                       else
                         0x03u8
                       end
      end

      def reset : Nil
        opnSetPres(@opn, 6 * 24, 6 * 24, 0)

        # Status clear
        irqMaskSet(@opn.st, 0x03)

        @opn.egTimer = 0
        @opn.egCount = 0

        @opn.lfoTimer = 0
        @opn.lfoCount = 0
        @opn.lfoAm = 126
        @opn.lfoPm = 0

        @opn.st.tac = 0
        @opn.st.tbc = 0

        @opn.sl3.keyCsm = 0

        @opn.st.status = 0
        @opn.st.mode = 0

        @regs.fill(0u8)

        opnWriteMode(@opn, 0x22, 0x00)

        opnWriteMode(@opn, 0x27, 0x30)
        opnWriteMode(@opn, 0x26, 0x00)
        opnWriteMode(@opn, 0x25, 0x00)
        opnWriteMode(@opn, 0x24, 0x00)

        resetChannels(@ch.to_unsafe, 6)

        0xB6.downto(0xB4) do |i|
          opnWriteReg(@opn, i, 0xC0)
          opnWriteReg(@opn, i | 0x100, 0xC0)
        end

        0xB2.downto(0x30) do |i|
          opnWriteReg(@opn, i, 0)
          opnWriteReg(@opn, i | 0x100, 0)
        end

        # DAC mode clear
        @dacen = 0
        @dacTest = 0
        @dacOut = 0

        @waveOutMode >>= 1 if @waveOutMode == 0x02
      end

      @[AlwaysInline]
      def write(a : Int, v : UInt8) : UInt8
        addr : Int32 = 0

        case a & 3
        when 0 # Address port 0
          @opn.st.address = v
          @addrA1 = 0

        when 1 # Data port 0
          unless @addrA1 != 0 # Verified on real YM2608
            addr = @opn.st.address.to_i32!
            @regs.put!(addr, v) # Safe because @opn.st.address is a UInt8
            case addr & 0xF0
            when 0x20 # 0x20-0x2f Mode
              case addr
              when 0x2A # DAC data
                @updateReq.not_nil!.call
                @dacOut = (v.to_i32! - 0x80) << 6 # Level unknown

              when 0x2B # DAC sel
                # B7 = dac enable
                @dacen = v & 0x80

              when 0x2C # Undocumented: DAC test reg
                # B5 = volume enable
                @dacTest = v & 0x20

              else # OPN section
                @updateReq.not_nil!.call
                opnWriteMode(@opn, addr, v.to_i32!)
              end

            else # 0x30-0xFF OPN section
              @updateReq.not_nil!.call
              # Write register
              opnWriteReg(@opn, addr, v.to_i32!)
            end
          end

        when 2 # Address port 1
          @opn.st.address = v
          @addrA1 = 1

        when 3 # Data port 1
          if @addrA1 == 1 # Verified on real YM2608
            addr = @opn.st.address.to_i32!
            @regs.put!(addr | 0x100, v) # Safe because @opn.st.address is a UInt8, @regs
                                        # has 512 elements, and ORing this won't go above
                                        # 512.
            @updateReq.not_nil!.call
            opnWriteReg(@opn, addr | 0x100, v.to_i32!)
          end
        end

        @opn.st.irq
      end

      @[AlwaysInline]
      def read(a : Int) : UInt8
        case a & 3
        when 0, 1, 2, 3 then @opn.st.status
        else 0u8
        end
      end

      @[AlwaysInline]
      def timerOver(c : Int) : UInt8
        if c != 0
          # Timer B
          timerBOver(@opn.st)
        else
          # Timer A
          @updateReq.not_nil!.call

          # Timer update
          timerAOver(@opn.st)

          # CSM mode key, TL control
          if (@opn.st.mode & 0xC0) == 0x80
            csmKeyControl(@opn, @ch.get!(2))
          end
        end

        @opn.st.irq
      end

      @[AlwaysInline]
      def muteMask=(mask : UInt32) : Nil
        6.times do |idx|
          @ch[idx].muted = ((mask >> idx) & 1).to_u8!
        end
        @muteDac = ((mask >> 6) & 0x01).to_u8!
      end

      @[AlwaysInline]
      def update(outputs : OutputBuffers, samples : UInt32) : Nil
        outFm = @opn.outFm
        outL : Slice(Int32) = outputs[0]
        outR : Slice(Int32) = outputs[1]
        lt : Int32 = 0
        rt : Int32 = 0

        cch0 : FmCh = {% if flag?(:yunosynth_wd40) %} @ch.get!(0) {% else %} @ch[0] {% end %}
        cch1 : FmCh = {% if flag?(:yunosynth_wd40) %} @ch.get!(1) {% else %} @ch[1] {% end %}
        cch2 : FmCh = {% if flag?(:yunosynth_wd40) %} @ch.get!(2) {% else %} @ch[2] {% end %}
        cch3 : FmCh = {% if flag?(:yunosynth_wd40) %} @ch.get!(3) {% else %} @ch[3] {% end %}
        cch4 : FmCh = {% if flag?(:yunosynth_wd40) %} @ch.get!(4) {% else %} @ch[4] {% end %}
        cch5 : FmCh = {% if flag?(:yunosynth_wd40) %} @ch.get!(5) {% else %} @ch[5] {% end %}

        # Check once, then we can avoid bounds checks below.
        if outL.size < samples || outR.size < samples
          raise "YM208 update: Bad number of samples (expected #{samples}, " \
                "but buffers have #{outL.size} and #{outR.size})"
        end

        dacOut : Int32 = if @muteDac == 0
                           @dacOut
                         else
                           0
                         end

        # Refresh PG and EG
        refreshFcEgChan(@opn, cch0)
        refreshFcEgChan(@opn, cch1)
        if Yuno.bitflag?(@opn.st.mode, 0xC0)
          # 3-slot mode
          if cch2.slot1.incr == -1
            refreshFcEgSlot(@opn, cch2.slot1, @opn.sl3.fc.get!(1).to_i32!, @opn.sl3.kcode.get!(1))
            refreshFcEgSlot(@opn, cch2.slot2, @opn.sl3.fc.get!(2).to_i32!, @opn.sl3.kcode.get!(2))
            refreshFcEgSlot(@opn, cch2.slot3, @opn.sl3.fc.get!(0).to_i32!, @opn.sl3.kcode.get!(0))
            refreshFcEgSlot(@opn, cch2.slot4, cch2.fc.to_i32!, cch2.kcode)
          end
        else
          refreshFcEgChan(@opn, cch2)
        end

        refreshFcEgChan(@opn, cch3)
        refreshFcEgChan(@opn, cch4)
        refreshFcEgChan(@opn, cch5)

        if samples == 0
          {% for i in 0..5 %}
            updateSsgEgChannel(cch{{i}}.slot.to_unsafe + SLOT1)
          {% end %}
        end

        # Buffering
        samples.times do |i|
          # Clear outputs
          outFm.fill(0, 0, 6)

          # Update SSG-EG output
          {% for i in 0..5 %}
            updateSsgEgChannel(cch{{i}}.slot.to_unsafe + SLOT1)
          {% end %}

          # Calculate FM
          if @dacTest == 0
            # Calculate FM
            {% for i in 0..4 %}
              chanCalc(@opn, cch{{i}}, {{i}}u32)
            {% end %}

            if @dacen != 0
              cch5.connect4.value += dacOut
            else
              chanCalc(@opn, cch5, 5)
            end
          else
            outFm.put!(0, dacOut)
            outFm.put!(1, dacOut)
            outFm.put!(2, dacOut)
            outFm.put!(3, dacOut)
            # no 4
            outFm.put!(5, dacOut)
          end

          # Advance LFO
          advanceLfo(@opn)

          # Advance envelope generator
          @opn.egTimer = @opn.egTimer &+ @opn.egTimerAdd
          while @opn.egTimer >= @opn.egTimerOverflow
            @opn.egTimer = @opn.egTimer &- @opn.egTimerOverflow
            @opn.egCount = @opn.egCount &+ 1

            {% for i in 0..5 %}
              advanceEgChannel(@opn, cch{{i}}.slot.to_unsafe + SLOT1)
            {% end %}
          end

          {% for i in 0..5 %}
            outFm.put!({{i}}, outFm.get!({{i}}).clamp(-8192, 8192))
          {% end %}

          # 6-channel mixing
          lt  = outFm.get!(0) & @opn.pan.get!(0)
          rt  = outFm.get!(0) & @opn.pan.get!(1)
          lt += outFm.get!(1) & @opn.pan.get!(2)
          rt += outFm.get!(1) & @opn.pan.get!(3)
          lt += outFm.get!(2) & @opn.pan.get!(4)
          rt += outFm.get!(2) & @opn.pan.get!(5)
          lt += outFm.get!(3) & @opn.pan.get!(6)
          rt += outFm.get!(3) & @opn.pan.get!(7)

          if @dacTest == 0
            lt += outFm.get!(4) & @opn.pan.get!(8)
            rt += outFm.get!(4) & @opn.pan.get!(9)
          else
            lt += dacOut
            lt += dacOut # Remi: both lt?
          end

          lt += outFm.get!(5) & @opn.pan.get!(10)
          rt += outFm.get!(5) & @opn.pan.get!(11)

          # Buffering
          @waveL = lt if Yuno.bitflag?(@waveOutMode, 0x01)
          @waveR = rt if Yuno.bitflag?(@waveOutMode, 0x02)
          @waveOutMode ^= 0x03 if (@waveOutMode ^ 0x03) != 0

          outL.put!(i, @waveL)
          outR.put!(i, @waveR)

          # CSM mode: if CSM Key ON has occured, CSM Key OFF need to be sent
          # only if Timer A does not overflow again (i.e CSM Key ON not set
          # again)
          @opn.sl3.keyCsm <<= 1

          # Timer A control
          if @opn.st.tac != 0
            if (@opn.st.tac -= (@opn.st.freqBase * 4096).to_i32!) <= 0
              timerAOver(@opn.st)
              # CSM mode total level latch and auto key on
              csmKeyControl(cch2) if Yuno.bitflag?(@opn.st.mode, 0x80)
            end
          end

          # CSM mode key on still disabled
          if Yuno.bitflag?(@opn.sl3.keyCsm, 2)
            # CSM Mode Key OFF (verified by Nemesis on real hardware)
            keyOffCsm(cch2, SLOT1)
            keyOffCsm(cch2, SLOT2)
            keyOffCsm(cch2, SLOT3)
            keyOffCsm(cch2, SLOT4)
            @opn.sl3.keyCsm = 0
          end
        end
      end

      ###
      ### YM2612-specific stuff
      ###

      @[AlwaysInline]
      protected def keyOnCsm(ch : FmCh, s : Int) : Nil
        slot = ch.slot[s]

        if slot.key == 0 && @opn.sl3.keyCsm == 0
          slot.phase = 0 # Restart phase generator
          slot.ssgn = 0 # Reset SSG-EG inversion flag

          if slot.ar + slot.ksr3 < 94 # 32 + 62
            slot.state = if slot.volume <= MIN_ATT_INDEX
                           if slot.sl == MIN_ATT_INDEX
                             EG_SUS
                           else
                             EG_DEC
                           end
                         else
                           EG_ATT
                         end
          else
            # Force attenuation level to 0
            slot.volume = MIN_ATT_INDEX

            # Directly switch to decay (or sustain)
            slot.state = slot.sl == MIN_ATT_INDEX ? EG_SUS : EG_DEC
          end

          # Recalculate EG output
          if Yuno.bitflag?(slot.ssg, 0x08) && (slot.ssgn ^ (slot.ssg & 0x04)) != 0
            slot.volOut = ((0x200u32 &- slot.volume) & MAX_ATT_INDEX) + slot.tl
          else
            slot.volOut = slot.volume.to_u32! &+ slot.tl
          end
        end
      end

      @[AlwaysInline]
      protected def keyOffCsm(ch : FmCh, s : Int) : Nil
        slot = ch.slot[s]
        if slot.key == 0
          if @isVgmInit
            slot.state = EG_OFF
            slot.volume = MAX_ATT_INDEX
            slot.volOut = MAX_ATT_INDEX.to_u32!
          elsif slot.state > EG_REL
            slot.state = EG_REL # Release phase

            # SSG-EG specific update
            if Yuno.bitflag?(slot.ssg, 0x08)
              # Convert EG attenuation level
              if (slot.ssgn ^ (slot.ssg & 0x04)) != 0
                slot.volume = 0x200 &- slot.volume
              end

              # Force EG attenuation level
              if slot.volume >= 0x200
                slot.volume = MAX_ATT_INDEX
                slot.state = EG_OFF
              end

              # Recalculate EG output
              slot.volOut = slot.volume.to_u32! + slot.tl
            end
          end
        end
      end

      # SSG-EG update process
      #
      # The behavior is based upon Nemesis tests on real hardware.  This is
      # executed before each sample.
      @[AlwaysInline]
      private def updateSsgEgChannel(slots : Pointer(FmSlot)) : Nil
        i : UInt32 = 4 # Four operators per channel
        slotPos : Int32 = 0

        loop do
          slot : FmSlot = slots[slotPos]

          # Detect SSG-EG transition.
          #
          # This is not required during release phase as the attenuation has
          # been forced to max and output invert flag is not used.  If an attack
          # phase is programmed, inversion can occur on each sample.
          if Yuno.bitflag?(slot.ssg, 0x08) && slot.volume >= 0x200 && slot.state > EG_REL
            if Yuno.bitflag?(slot.ssg, 0x01) # bit 0 = hold SSG-EG
              # Set inversion flag
              slot.ssgn = 4 if Yuno.bitflag?(slot.ssg, 0x02)

              # Force attenuation level during decay phases
              if slot.state != EG_ATT && (slot.ssgn ^ (slot.ssg & 0x04)) == 0
                slot.volume = MAX_ATT_INDEX
              end
            else
              # Toggle output inversion flag or reset Phase Generator
              if Yuno.bitflag?(slot.ssg, 0x02)
                slot.ssgn ^= 4
              else
                slot.phase = 0
              end

              # Same as key on
              unless slot.state == EG_ATT
                if slot.ar + slot.ksr3 < 94 # 32 + 62
                  slot.state = if slot.volume <= MIN_ATT_INDEX
                                 if slot.sl == MIN_ATT_INDEX
                                   EG_SUS
                                 else
                                   EG_DEC
                                 end
                               else
                                 EG_ATT
                               end
                else
                  # Attack rate is maximal, directly switch to decay or sustain.
                  slot.volume = MIN_ATT_INDEX
                  slot.state = slot.sl == MIN_ATT_INDEX ? EG_SUS : EG_DEC
                end
              end
            end

            # Recalculate EG output
            if (slot.ssgn ^ (slot.ssg & 0x04)) != 0
              slot.volOut = ((0x200 &- slot.volume).to_u32! & MAX_ATT_INDEX) + slot.tl
            else
              slot.volOut = slot.volume.to_u32! + slot.tl
            end
          end

          slotPos += 1
          i -= 1
          break if i == 0
        end # loop do
      end

      ###
      ### OPNMame Overrides
      ###

      @[AlwaysInline]
      protected def timerAOver(st : FmSt) : Nil
        # Set status (if enabled)
        statusSet(st, 0x01) if Yuno.bitflag?(st.mode, 0x04)

        # Clear and reload the counter
        st.tac = 4096 * (1024 - st.ta)
      end

      @[AlwaysInline]
      protected def keyOn(ch : FmCh, s : Int) : Nil
        {% begin %}
          {% if flag?(:yunosynth_wd40) %}
            slot = ch.slot.get!(s)
          {% else %}
            slot = ch.slot[s]
          {% end %}

            # Note by Valley Bell:
            #  I assume that the CSM mode shouldn't affect channels
            #  other than FM3, so I added a check for it here.
            if slot.key == 0 && (@opn.sl3.keyCsm == 0 || ch.same?(@opn.pch.get!(3)))
              slot.phase = 0 # Restart phase generator
              slot.ssgn = 0 # Reset SSG-EG inversion flag

              if slot.ar + slot.ksr3 < 94 # 32 + 62
                slot.state = if slot.volume <= MIN_ATT_INDEX
                               if slot.sl == MIN_ATT_INDEX
                                 EG_SUS
                               else
                                 EG_DEC
                               end
                             else
                               EG_ATT
                             end
              else
                # Force attenuation level to 0
                slot.volume = MIN_ATT_INDEX

                # Directly switch to decay (or sustain)
                slot.state = slot.sl == MIN_ATT_INDEX ? EG_SUS : EG_DEC
              end

              # Recalculate EG output
              if Yuno.bitflag?(slot.ssg, 0x08) && (slot.ssgn ^ (slot.ssg & 0x04)) != 0
                slot.volOut = ((0x200 &- slot.volume).to_u32! & MAX_ATT_INDEX) + slot.tl
              else
                slot.volOut = slot.volume.to_u32! + slot.tl
              end
            end

          slot.key = 1
        {% end %}
      end

      @[AlwaysInline]
      protected def keyOff(ch : FmCh, s : Int) : Nil
        {% begin %}
          {% if flag?(:yunosynth_wd40) %}
            slot = ch.slot.get!(s)
          {% else %}
            slot = ch.slot[s]
          {% end %}

            if slot.key != 0 && (@opn.sl3.keyCsm == 0 || ch.same?(@opn.pch.get!(3)))
              if @isVgmInit # Workaround for VGMs trimmed with VGMTool
                slot.state = EG_OFF
                slot.volume = MAX_ATT_INDEX
                slot.volOut = MAX_ATT_INDEX.to_u32!
              elsif slot.state > EG_REL
                slot.state = EG_REL # Release phase

                # SSG-EG specific update
                if Yuno.bitflag?(slot.ssg, 0x08)
                  # Convert EG attenuation level
                  if (slot.ssgn ^ (slot.ssg & 0x04)) != 0
                    slot.volume = 0x200 - slot.volume
                  end

                  # Force EG attenuation level
                  if slot.volume >= 0x200
                    slot.volume = MAX_ATT_INDEX
                    slot.state = EG_OFF
                  end

                  # Recalculate EG output
                  slot.volOut = slot.volume.to_u32! + slot.tl
                end
              end
            end

          slot.key = 0
        {% end %}
      end

      @[AlwaysInline]
      protected def setTimers(st : FmSt, n : AbstractChip?, v : Int) : Nil
        # b7 = CSM MODE
        # b6 = 3 slot mode
        # b5 = reset b
        # b4 = reset a
        # b3 = timer enable b
        # b2 = timer enable a
        # b1 = load b
        # b0 = load a

        if Yuno.bitflag?(@opn.st.mode ^ v, 0xC0)
          # Phase increment needs to be recalculated
          @opn.pch.get!(2).slot1.incr = -1

          # CSM mode disabled and CSM key on active
          if (v & 0xC0) != 0x80 && @opn.sl3.keyCsm != 0
            # CSM mode key off (verified by Nemesis on real hardware)
            keyOffCsm(@opn.pch.get!(2), SLOT1)
            keyOffCsm(@opn.pch.get!(2), SLOT2)
            keyOffCsm(@opn.pch.get!(2), SLOT3)
            keyOffCsm(@opn.pch.get!(2), SLOT4)
            @opn.sl3.keyCsm = 0
          end
        end

        # Reset timer B flag
        statusReset(st, 0x02) if Yuno.bitflag?(v, 0x20)

        # Reset timer A flag
        statusReset(st, 0x01) if Yuno.bitflag?(v, 0x10)

        # Load B
        if Yuno.bitflag?(v, 0x02) && !Yuno.bitflag?(st.mode, 2)
          st.tbc = (256 - st.tb.to_i32!) << 4
        end

        # Load A
        if Yuno.bitflag?(v, 0x01) && !Yuno.bitflag?(st.mode, 1)
          st.tac = 1024 - st.ta
          st.tac *= 4096
        end

        st.mode = v.to_u32!
      end

      # Sets the detune amount and multiple.
      @[AlwaysInline]
      protected def setDetMul(st : FmSt, ch : FmCh, slot : FmSlot, v : Int) : Nil
        slot.mul = if Yuno.bitflag?(v, 0x0F)
                     (v & 0x0F).to_u32! * 2u32
                   else
                     1u32
                   end
        slot.dt = st.dtTab.get!((v >> 4) & 7) # Save because dtTab has 8 elements
        ch.slot1.incr = -1
      end

      # Sets the total level.
      @[AlwaysInline]
      protected def setTl(ch : FmCh, slot : FmSlot, v : Int) : Nil
        slot.tl = (v.to_u32! & 0x7F) << (ENV_BITS - 7) # 7-bit TL

        # Recalculate EG output
        if Yuno.bitflag?(slot.ssg, 0x08) && (slot.ssgn ^ (slot.ssg & 0x04)) != 0 && slot.state > EG_REL
          slot.volOut = ((0x200 &- slot.volume).to_u32! & MAX_ATT_INDEX) + slot.tl
        else
          slot.volOut = slot.volume.to_u32! + slot.tl
        end
      end

      # Sets the attack rate and key scaling.
      @[AlwaysInline]
      protected def setArKsr(ch : FmCh, slot : FmSlot, v : Int) : Nil
        oldKsr : UInt8 = slot.ksr

        slot.ar = if Yuno.bitflag?(v, 0x1F)
                    32u32 + ((v.to_u32! & 0x1F) << 1)
                  else
                    0u32
                  end

        slot.ksr = (3 - (v >> 6)).to_u8!
        if slot.ksr != oldKsr
          ch.slot1.incr = -1
        end

        # Even if it seems unnecessary, in some odd case, KSR and KC are both modified
        # and could result in SLOT->kc remaining unchanged.
        # In such case, AR values would not be recalculated despite SLOT->ar has changed
        # This fixes the introduction music of Batman & Robin    (Eke-Eke)
        if (slot.ar + slot.ksr3.to_u32!) < (32 + 62)
          slot.egShAr = EG_RATE_SHIFT_2612[slot.ar + slot.ksr3]
          slot.egSelAr = EG_RATE_SELECT_2612.get!(slot.ar + slot.ksr3)
        else
          slot.egShAr = 0
          slot.egSelAr = (18 * RATE_STEPS).to_u8! # Verified by Nemesis on real hardware
        end
      end

      # Sets the decay rate
      @[AlwaysInline]
      protected def setDr(slot : FmSlot, v : Int) : Nil
        slot.d1r = if Yuno.bitflag?(v, 0x1F)
                     32u32 + ((v.to_u32! & 0x1F) << 1)
                   else
                     0u32
                   end
        slot.egShD1r = EG_RATE_SHIFT_2612[slot.d1r + slot.ksr3]
        slot.egSelD1r = EG_RATE_SELECT_2612.get!(slot.d1r + slot.ksr3)
      end

      # Sets the sustain "rate".
      @[AlwaysInline]
      protected def setSr(slot : FmSlot, v : Int) : Nil
        slot.d2r = if Yuno.bitflag?(v, 0x1F)
                     32u32 + ((v.to_u32! & 0x1F) << 1)
                   else
                     0u32
                   end
        slot.egShD2r = EG_RATE_SHIFT_2612[slot.d2r + slot.ksr3]
        slot.egSelD2r = EG_RATE_SELECT_2612.get!(slot.d2r + slot.ksr3)
      end

      # Sets the release rate.
      @[AlwaysInline]
      protected def setSlRr(slot : FmSlot, v : Int) : Nil
        slot.sl = SL_TABLE[v >> 4]

        # Check EG state changes
        if slot.state == EG_DEC && slot.volume >= slot.sl.to_i32!
          slot.state = EG_SUS
        end

        slot.rr = 34u32 + ((v.to_u32! & 0x0F) << 2)
        slot.egShRr = EG_RATE_SHIFT_2612[slot.rr + slot.ksr3]
        slot.egSelRr = EG_RATE_SELECT_2612.get!(slot.rr + slot.ksr3)
      end

      # Advances the LFO to the next sample.
      @[AlwaysInline]
      protected def advanceLfo(opn : FmOpn) : Nil
        # Is the LFO enabled?
        if opn.lfoTimerOverflow != 0
          # LFO is enabled.

          opn.lfoTimer = opn.lfoTimer &+ opn.lfoTimerAdd

          # When LFO is enabled, one level will last for 108, 77, 71, 67, 62, 44,
          # 8 or 5 samples.
          while opn.lfoTimer >= opn.lfoTimerOverflow
            opn.lfoTimer = opn.lfoTimer &- opn.lfoTimerOverflow

            # There are 128 LFO steps
            opn.lfoCount = (opn.lfoCount &+ 1) & 127

            # Valley Bell: Replaced old code (non-inverted triangle) with
            # the one from Genesis Plus GX 1.71.
            # triangle (inverted)
            # AM: from 126 to 0 step -2, 0 to 126 step +2
            if opn.lfoCount < 64
              opn.lfoAm = (opn.lfoCount ^ 63) << 1
            else
              opn.lfoAm = (opn.lfoCount & 63) << 1
            end

            # PM works with 4 times slower clock.
            opn.lfoPm = (opn.lfoCount >> 2).to_i32!
          end
        end
      end

      #@[AlwaysInline]
      protected def advanceEgChannel(opn : FmOpn, slots : Pointer(FmSlot)) : Nil
        slotPos : Int32 = 0

        # Four operators per channel
        i : UInt8 = 4u8
        loop do
          slot : FmSlot = slots[slotPos]

          case slot.state
          when EG_ATT # Attack phase
            unless Yuno.bitflag?(opn.egCount, (1 << slot.egShAr) - 1)
              # Update attenuation level
              {% if flag?(:yunosynth_wd40) %}
                slot.volume += (~(slot.volume) * (EG_INC.get!(slot.egSelAr + ((opn.egCount >> slot.egShAr) & 7)))) >> 4
              {% else %}
                slot.volume += (~(slot.volume) * (EG_INC[slot.egSelAr + ((opn.egCount >> slot.egShAr) & 7)])) >> 4
              {% end %}

              # Check phase transition
              if slot.volume <= MIN_ATT_INDEX
                slot.volume = MIN_ATT_INDEX
                slot.state = (slot.sl == MIN_ATT_INDEX ? EG_SUS : EG_DEC) # Special case where SL=0
              end

              # Recalculate EG output
              if Yuno.bitflag?(slot.ssg, 0x08) && (slot.ssgn ^ (slot.ssg & 0x04)) != 0 # SSG-EG output inversion
                slot.volOut = ((0x200 &- slot.volume).to_u32! & MAX_ATT_INDEX) + slot.tl
              else
                slot.volOut = slot.volume.to_u32! + slot.tl
              end
            end

          when EG_DEC # Decay phase
            unless Yuno.bitflag?(opn.egCount, (1 << slot.egShD1r) - 1)
              if Yuno.bitflag?(slot.ssg, 0x08) # SSG EG type
                # Update attenuation level
                if slot.volume < 0x200
                  {% if flag?(:yunosynth_wd40) %}
                    slot.volume += 4 * EG_INC.get!(slot.egSelD1r + ((opn.egCount >> slot.egShD1r) & 7))
                  {% else %}
                    slot.volume += 4 * EG_INC[slot.egSelD1r + ((opn.egCount >> slot.egShD1r) & 7)]
                  {% end %}

                  # Recalculate EG output
                  if (slot.ssgn ^ (slot.ssg & 0x04)) != 0 # SSG-EG output inversion
                    slot.volOut = ((0x200 &- slot.volume).to_u32! & MAX_ATT_INDEX) + slot.tl
                  else
                    slot.volOut = slot.volume.to_u32! + slot.tl
                  end
                end
              else
                # Update attenuation level
                {% if flag?(:yunosynth_wd40) %}
                  slot.volume += EG_INC.get!(slot.egSelD1r + ((opn.egCount >> slot.egShD1r) & 7))
                {% else %}
                  slot.volume += EG_INC[slot.egSelD1r + ((opn.egCount >> slot.egShD1r) & 7)]
                {% end %}

                # Recalculate EG output
                slot.volOut = slot.volume.to_u32! + slot.tl
              end

              # Check phase transition
              if slot.volume >= slot.sl.to_i32!
                slot.state = EG_SUS
              end
            end

          when EG_SUS # Sustain phase
            unless Yuno.bitflag?(opn.egCount, (1 << slot.egShD2r) - 1)
              if Yuno.bitflag?(slot.ssg, 0x08) # SSG EG type
                # Update attenuation level
                if slot.volume < 0x200
                  {% if flag?(:yunosynth_wd40) %}
                    slot.volume += 4 * EG_INC.get!(slot.egSelD2r + ((opn.egCount >> slot.egShD2r) & 7))
                  {% else %}
                    slot.volume += 4 * EG_INC[slot.egSelD2r + ((opn.egCount >> slot.egShD2r) & 7)]
                  {% end %}

                  # Recalculate EG output
                  if (slot.ssgn ^ (slot.ssg & 0x04)) != 0 # SSG-EG output inversion
                    slot.volOut = ((0x200 &- slot.volume).to_u32! & MAX_ATT_INDEX) + slot.tl
                  else
                    slot.volOut = slot.volume.to_u32! + slot.tl
                  end
                end
              else
                # Update attenuation level
                {% if flag?(:yunosynth_wd40) %}
                  slot.volume += EG_INC.get!(slot.egSelD2r + ((opn.egCount >> slot.egShD2r) & 7))
                {% else %}
                  slot.volume += EG_INC[slot.egSelD2r + ((opn.egCount >> slot.egShD2r) & 7)]
                {% end %}

                # Check phase transition
                if slot.volume >= MAX_ATT_INDEX
                  slot.volume = MAX_ATT_INDEX
                end
                # do not change SLOT->state (verified on real chip)

                # Recalculate EG output
                slot.volOut = slot.volume.to_u32! + slot.tl
              end
            end

          when EG_REL # Release phase
            unless Yuno.bitflag?(opn.egCount, (1 << slot.egShRr) - 1)
              # SSG EG type
              if Yuno.bitflag?(slot.ssg, 0x08)
                # Update attenuation level
                if slot.volume < 0x200
                  {% if flag?(:yunosynth_wd40) %}
                    slot.volume += 4 * EG_INC.get!(slot.egSelRr + ((opn.egCount >> slot.egShRr) & 7))
                  {% else %}
                    slot.volume += 4 * EG_INC[slot.egSelRr + ((opn.egCount >> slot.egShRr) & 7)]
                  {% end %}
                end

                # Check phase transition
                if slot.volume >= 0x200
                  slot.volume = MAX_ATT_INDEX
                  slot.state = EG_OFF
                end
              else
                # Update attenuation level
                {% if flag?(:yunosynth_wd40) %}
                  slot.volume += EG_INC.get!(slot.egSelRr + ((opn.egCount >> slot.egShRr) & 7))
                {% else %}
                  slot.volume += EG_INC[slot.egSelRr + ((opn.egCount >> slot.egShRr) & 7)]
                {% end %}

                # Check phase transition
                if slot.volume >= MAX_ATT_INDEX
                  slot.volume = MAX_ATT_INDEX
                  slot.state = EG_OFF
                end
              end

              # Recalculate EG output
              slot.volOut = slot.volume.to_u32! + slot.tl
            end
          end # case slot.state

          slotPos += 1
          i -= 1
          break if i == 0
        end # loop do
      end

      # Updates the phase increment and envelope generator.
      @[AlwaysInline]
      protected def refreshFcEgSlot(opn : FmOpn, slot : FmSlot, fc : Int32, kc : Int32) : Nil
        ksr : Int32 = kc >> slot.ksr
        {% if flag?(:yunosynth_wd40) %}
          fc += slot.dt.get!(kc)
        {% else %}
          fc += slot.dt[kc]
        {% end %}

        # Detects frequency overflow (credits to Nemesis)
        fc += opn.fnMax if fc < 0

        # (frequency) phase increment counter
        slot.incr = (fc.to_i32! &* slot.mul) >> 1

        if slot.ksr3 != ksr
          slot.ksr3 = ksr.to_u8!

          # Calculate envelope generator rates
          if (slot.ar + slot.ksr3.to_u32!) < (32 + 62)
            slot.egShAr = EG_RATE_SHIFT_2612[slot.ar + slot.ksr3]
            slot.egSelAr = EG_RATE_SELECT_2612.get!(slot.ar + slot.ksr3)
          else
            slot.egShAr = 0
            slot.egSelAr = (18 * RATE_STEPS).to_u8! # Verified by Nemesis on real hardware (attack phase is blocked)
          end

          {% if flag?(:yunosynth_wd40) %}
            slot.egShD1r = EG_RATE_SHIFT_2612.get!(slot.d1r + slot.ksr3.to_u32!).to_u8!
            slot.egShD2r = EG_RATE_SHIFT_2612.get!(slot.d2r + slot.ksr3.to_u32!).to_u8!
            slot.egShRr  = EG_RATE_SHIFT_2612.get!(slot.rr  + slot.ksr3.to_u32!).to_u8!
          {% else %}
            slot.egShD1r = EG_RATE_SHIFT_2612[slot.d1r + slot.ksr3.to_u32!].to_u8!
            slot.egShD2r = EG_RATE_SHIFT_2612[slot.d2r + slot.ksr3.to_u32!].to_u8!
            slot.egShRr  = EG_RATE_SHIFT_2612[slot.rr  + slot.ksr3.to_u32!].to_u8!
          {% end %}

          slot.egSelD1r = EG_RATE_SELECT_2612.get!(slot.d1r + slot.ksr3)
          slot.egSelD2r = EG_RATE_SELECT_2612.get!(slot.d2r + slot.ksr3)
          slot.egSelRr  = EG_RATE_SELECT_2612.get!(slot.rr  + slot.ksr3)
        end
      end

      # Updates phase increment counters.
      @[AlwaysInline]
      protected def refreshFcEgChan(opn : FmOpn, chan : FmCh) : Nil
        if chan.slot1.incr == -1
          fc : Int32 = chan.fc.to_i32!
          kc : Int32 = chan.kcode.to_i32!
          refreshFcEgSlot(opn, chan.slot1, fc, kc)
          refreshFcEgSlot(opn, chan.slot2, fc, kc)
          refreshFcEgSlot(opn, chan.slot3, fc, kc)
          refreshFcEgSlot(opn, chan.slot4, fc, kc)
        end
      end

      @[AlwaysInline]
      private def chanCalc(opn : FmOpn, ch : FmCh, chNum : UInt32) : Nil
        return if ch.muted != 0

        am : UInt32 = opn.lfoAm >> ch.ams
        opn.m2 = 0
        opn.c1 = 0
        opn.c2 = 0
        opn.mem = 0
        ch.memConnect.value = ch.memValue # Restored delayed sample (MEM) value to m2 or c2

        egOut : UInt32 = volumeCalc(ch.slot1, am)
        output : Int32 = ch.op1Out.get!(0) + ch.op1Out.get!(1)
        ch.op1Out.put!(0, ch.op1Out.get!(1))

        if ch.connect1.null?
          # Algorithm 5
          opn.mem = ch.op1Out.get!(0)
          opn.c1 = ch.op1Out.get!(0)
          opn.c2 = ch.op1Out.get!(0)
        else
          # Other algorithms
          ch.connect1.value += ch.op1Out.get!(0)
        end

        ch.op1Out.put!(1, 0)

        # Slot 1
        if egOut < ENV_QUIET
          output = 0 if ch.fb == 0
          ch.op1Out.put!(1, opCalc1(ch.slot1.phase, egOut, (output << ch.fb)))
        end

        # Slot 3
        egOut = volumeCalc(ch.slot3, am)
        if egOut < ENV_QUIET
          ch.connect3.value += opCalc(ch.slot3.phase, egOut, opn.m2)
        end

        # Slot 2
        egOut = volumeCalc(ch.slot2, am)
        if egOut < ENV_QUIET
          ch.connect2.value += opCalc(ch.slot2.phase, egOut, opn.c1)
        end

        # Slot 4
        egOut = volumeCalc(ch.slot4, am)
        if egOut < ENV_QUIET
          ch.connect4.value += opCalc(ch.slot4.phase, egOut, opn.c2)
        end

        # Store current MEM
        ch.memValue = opn.mem

        # Update phase counters *after* output calculations.
        if ch.pms != 0
          # Add support for 3-slot mode.
          if Yuno.bitflag?(opn.st.mode, 0xC0) && ch.same?(@ch.get!(2))
            updatePhaseLfoSlot(opn, ch.slot1, ch.pms, opn.sl3.blockFnum.get!(1))
            updatePhaseLfoSlot(opn, ch.slot2, ch.pms, opn.sl3.blockFnum.get!(2))
            updatePhaseLfoSlot(opn, ch.slot3, ch.pms, opn.sl3.blockFnum.get!(0))
            updatePhaseLfoSlot(opn, ch.slot4, ch.pms, ch.blockFnum)
          else
            updatePhaseLfoChannel(opn, ch)
          end
        else
          # No LFO phase modulation.
          {% begin %}
            {% for i in 1..4 %}
              ch.slot{{i}}.phase = ch.slot{{i}}.phase &+ ch.slot{{i}}.incr
            {% end %}
          {% end %}
        end
      end

      # CSM key control
      @[AlwaysInline]
      protected def csmKeyControl(ch : FmCh) : Nil
        # All key on (verified by Nemesis on real hardware)
        keyOnCsm(ch, SLOT1)
        keyOnCsm(ch, SLOT2)
        keyOnCsm(ch, SLOT3)
        keyOnCsm(ch, SLOT4)
        @opn.sl3.keyCsm = 1
      end

      # Write an OPN mode register (0x20 - 0x2F).
      @[AlwaysInline]
      protected def opnWriteMode(opn : FmOpn, r : Int32, v : Int32) : Nil
        case r
        #when 0x21 # Test
        #  nil

        when 0x22 # LFO Freq (YM2608 / YM2610 / YM2610B / YM2612)
          if Yuno.bitflag?(v, 8) # LFO enabled?
            # Safe because LFO_SAMPLES_PER_STEP has 8 elements
            opn.lfoTimerOverflow = LFO_SAMPLES_PER_STEP.get!(v & 7) << LFO_SH
          else
            # Valley Bell: Ported from Genesis Plus GX 1.71
            # hold LFO waveform in reset state
            opn.lfoTimerOverflow = 0
            opn.lfoTimer = 0
            opn.lfoCount = 0

            opn.lfoPm = 0
            opn.lfoAm = 126
          end

        when 0x24 # Timer A high 8
          opn.st.ta = (opn.st.ta & 0x03) | (v << 2)

        when 0x25 # Timer A low 2
          opn.st.ta = (opn.st.ta & 0x3FC) | (v & 3)

        when 0x26 # Timer B
          opn.st.tb = v.to_u8

        when 0x27 # Mode, timer control
          setTimers(opn.st, opn.st.param, v)

        when 0x28 # Key on/off
          c : UInt8 = (v & 0x03).to_u8!
          return if c == 3
          c += 3 if Yuno.bitflag?(v, 0x04) && Yuno.bitflag?(opn.type, TYPE_6CH)
          ch = opn.pch.get!(c) # Safe because we've ANDed with 3 and only add up to 3.
          Yuno.bitflag?(v, 0x10) ? keyOn(ch, SLOT1) : keyOff(ch, SLOT1)
          Yuno.bitflag?(v, 0x20) ? keyOn(ch, SLOT2) : keyOff(ch, SLOT2)
          Yuno.bitflag?(v, 0x40) ? keyOn(ch, SLOT3) : keyOff(ch, SLOT3)
          Yuno.bitflag?(v, 0x80) ? keyOn(ch, SLOT4) : keyOff(ch, SLOT4)
        end
      end

      # Writes to an OPN register (0x30 - 0xFF)
      @[AlwaysInline]
      protected def opnWriteReg(opn : FmOpn, r : Int32, v : Int32) : Nil
        c : UInt8 = opnChan(r).to_u8!
        return if c == 3 # 0xX3, 0xX7, 0xXB, 0xXF
        c += 3 if r >= 0x100

        ch = opn.pch.get!(c)
        slot = ch.slot.get!(opnSlot(r))

        case r & 0xF0
        when 0x30 # DET, MUL
          setDetMul(opn.st, ch, slot, v)

        when 0x40 # TL
          setTl(ch, slot, v)

        when 0x50 # KS, AR
          setArKsr(ch, slot, v)

        when 0x60 # bit 7 = AM ENABLE, DR
          setDr(slot, v)

          if Yuno.bitflag?(opn.type, TYPE_LFOPAN) # YM2608/YM2610/YM2610B/YM2612
            slot.amMask = if Yuno.bitflag?(v, 0x80)
                            UInt32::MAX
                          else
                            0u32
                          end
          end

        when 0x70 # SR
          setSr(slot, v.to_i32!)

        when 0x80 # SL, RR
          setSlRr(slot, v.to_i32!)

        when 0x90 # SSG-EG
          slot.ssg = (v & 0x0F).to_u8!

          # Recalculate EG output
          if slot.state > EG_REL
            if Yuno.bitflag?(slot.ssg, 0x08) && (slot.ssgn ^ (slot.ssg & 0x04)) != 0
              slot.volOut = ((0x200 &- slot.volume).to_u32! & MAX_ATT_INDEX) + slot.tl
            else
              slot.volOut = slot.volume.to_u32! + slot.tl
            end
          end

          # SSG-EG envelope wshapes:
          #
          # E AtAlH
          # 1 0 0 0  \\\\
          #
          # 1 0 0 1  \___
          #
          # 1 0 1 0  \/\/
          #         ___
          # 1 0 1 1  \
          #
          # 1 1 0 0  ////
          #                 ___
          # 1 1 0 1  /
          #
          # 1 1 1 0  /\/\
          #
          # 1 1 1 1  /___
          #
          #
          # E = SSG-EG enable
          #
          #
          # The shapes are generated using Attack, Decay and Sustain phases.
          #
          # Each single character in the diagrams above represents this whole
          # sequence:
          #
          # - when KEY-ON = 1, normal Attack phase is generated (*without* any
          #   difference when compared to normal mode),
          #
          # - later, when envelope level reaches minimum level (max volume),
          #   the EG switches to Decay phase (which works with bigger steps
          #   when compared to normal mode - see below),
          #
          # - later when envelope level passes the SL level,
          #   the EG swithes to Sustain phase (which works with bigger steps
          #   when compared to normal mode - see below),
          #
          # - finally when envelope level reaches maximum level (min volume),
          #   the EG switches to Attack phase again (depends on actual waveform).
          #
          # Important is that when switch to Attack phase occurs, the phase counter
          # of that operator will be zeroed-out (as in normal KEY-ON) but not always.
          # (I havent found the rule for that - perhaps only when the output level is low)
          #
          # The difference (when compared to normal Envelope Generator mode) is
          # that the resolution in Decay and Sustain phases is 4 times lower;
          # this results in only 256 steps instead of normal 1024.
          # In other words:
          # when SSG-EG is disabled, the step inside of the EG is one,
          # when SSG-EG is enabled, the step is four (in Decay and Sustain phases).
          #
          # Times between the level changes are the same in both modes.
          #
          #
          # Important:
          # Decay 1 Level (so called SL) is compared to actual SSG-EG output, so
          # it is the same in both SSG and no-SSG modes, with this exception:
          #
          # when the SSG-EG is enabled and is generating raising levels
          # (when the EG output is inverted) the SL will be found at wrong level !!!
          # For example, when SL=02:
          #     0 -6 = -6dB in non-inverted EG output
          #     96-6 = -90dB in inverted EG output
          # Which means that EG compares its level to SL as usual, and that the
          # output is simply inverted afterall.
          #
          #
          # The Yamaha's manuals say that AR should be set to 0x1f (max speed).
          # That is not necessary, but then EG will be generating Attack phase.

        when 0xA0
          case opnSlot(r)
          when 0 # 0xA0 - 0xA2 : FNUM1
            if @isVgmInit
              opn.st.fnh = (ch.blockFnum >> 8).to_u8!
            end

            fn : UInt32 = ((opn.st.fnh & 7).to_u32! << 8) + v
            blk : UInt8 = opn.st.fnh >> 3

            # Keyscale code
            ch.kcode = (blk << 2) | OPN_FK_TABLE.get!(fn >> 7)

            # Phase increment counter
            ch.fc = opn.fnTable.get!(fn * 2) >> (7 - blk)

            # Store fnum in clear form for LFO PM calculations
            ch.blockFnum = (blk.to_u32! << 11) | fn

            ch.slot1.incr = -1

          when 1 # 0xA4 - 0xA6 : FNUM2, BLK
            opn.st.fnh = (v & 0x3F).to_u8!
            if @isVgmInit
              ch.blockFnum = (opn.st.fnh.to_u32! << 8) | (ch.blockFnum & 0xFF)
            end

          when 2 # 0xA8 - 0xAA : 3CH FNUM1

            # The VGMs for Rocket Knight Adventures seem to like to go out of
            # bounds.  Do a manual size check to be safe.
            if @isVgmInit && c < opn.sl3.blockFnum.size
              opn.sl3.fnh = (opn.sl3.blockFnum.get!(c) >> 8).to_u8!
            end

            if r < 0x100
              fn = ((opn.sl3.fnh & 7).to_u32! << 8) + v
              blk = opn.sl3.fnh >> 3

              # Keyscale code
              opn.sl3.kcode.put!(c, (blk << 2) | OPN_FK_TABLE.get!(fn >> 7))

              # Phase increment counter
              opn.sl3.fc.put!(c, opn.fnTable.get!(fn * 2) >> (7 - blk))

              # Store fnum in clear form for LFO PM calculations
              opn.sl3.blockFnum.put!(c, (blk.to_u32! << 11) | fn)

              opn.pch.get!(2).slot1.incr = -1
            end

          when 3 # 0xAC - 0xAE : #3CH FNUM2, BLK
            if r < 0x100
              opn.sl3.fnh = (v & 0x3F).to_u8!
              if @isVgmInit
                # The put! is safe because we use [] within the value with the same index
                opn.sl3.blockFnum.put!(c, (opn.sl3.fnh.to_u32! << 8) | (opn.sl3.blockFnum[c] & 0xFF))
              end
            end
          end

        when 0xB0
          case opnSlot(r)
          when 0 # 0xB0 - 0xB2 : FB, ALGO
            feedback : UInt8 = ((v >> 3) & 7).to_u8!
            ch.algo = (v & 7).to_u8!
            ch.fb = if feedback != 0
                      feedback &+ 6
                    else
                      0u8
                    end
            setupConnection(opn, ch, c)

          when 1 # 0xB4 - 0xB6 : L, R, AMS, PMS (YM2612/YM2610B/YM2610/YM2608)
            if Yuno.bitflag?(opn.type, TYPE_LFOPAN)
              # b0-2 PMS
              ch.pms = (v & 7) * 32

              # b4-5 Ams
              ch.ams = LFO_AMS_DEPTH_SHIFT.get!((v >> 4) & 3) # Safe because LFO_AMS_DEPTH_SHIFT only has 4 elements

              # PAN : b7 = L, B6 = R
              opn.pan.put!(c * 2    , (Yuno.bitflag?(v, 0x80) ? UInt32::MAX : 0u32))
              opn.pan.put!(c * 2 + 1, (Yuno.bitflag?(v, 0x40) ? UInt32::MAX : 0u32))
            end
          end
        end
      end

      # Initialize time tables
      protected def initTimeTables(st : FmSt, freqBase : Float64) : Nil
        rate : Float64 = 0.0

        # Detune table
        4.times do |d|
          32.times do |i|
            # -10 because chip works with 10.10 fixed point, while we use 16.16
            rate = DT_TAB[d * 32 + i].to_f64! * freqBase * (1 << (FREQ_SH - 10))
            st.dtTab[d][i] = rate.to_i32!
            st.dtTab[d + 4][i] = -(st.dtTab[d][i])
          end
        end

        # There are 2048 FNUMs that can be generated using FNUM/BLK registers
        # but LFO works with one more bit of a precision so we really need 4096
        # elements.
        #
        # Calculate fnumber -> increment counter table
        4096.times do |i|
          # freq table for octave 7
          # OPN phase increment counter = 20bit
          # the correct formula is : F-Number = (144 * fnote * 2^20 / M) / 2^(B-1)
          # where sample clock is  M/144
          # this means the increment value for one clock sample is FNUM * 2^(B-1) = FNUM * 64 for octave 7
          # we also need to handle the ratio between the chip frequency and the emulated frequency (can be 1.0)
          #
          # -10 because chip works with 10.10 fixed point, while we use 16.16
          @opn.fnTable[i] = (i.to_f64! * 32 * freqBase * (1 << (FREQ_SH - 10))).to_u32!
        end

        # Maximal frequency is required for Phase overflow calculation, register size is 17 bits (Nemesis)
        @opn.fnMax = (0x20000.to_f64! * freqBase * (1 << (FREQ_SH - 10))).to_u32!
      end

      # Prescaler set (and make time tables)
      protected def opnSetPres(opn : FmOpn, pres : Int, timerPrescaler : Int32, ssgPres : Int) : Nil
        # Frequency base
        opn.st.freqBase = if opn.st.rate != 0
                            (opn.st.clock.to_f64! / opn.st.rate) / pres
                          else
                            0.0
                          end

        opn.egTimerAdd = ((1u32 << EG_SH) * opn.st.freqBase).to_u32!
        opn.egTimerOverflow = 3u32 * (1u32 << EG_SH)

        # LFO timer increment (every sample)
        opn.lfoTimerAdd = ((1 << LFO_SH) * opn.st.freqBase).to_u32!

        # Timer base time
        opn.st.timerPrescaler = timerPrescaler

        # SSG part prescaler set
        if ssgPres != 0
          opn.st.ssg.setClock.call((opn.st.clock * 2 / ssgPres).to_u32!)
        end

        # Make time tables
        initTimeTables(opn.st, opn.st.freqBase)
      end

      protected def resetChannels(ch : Pointer(FmCh), num : Int) : Nil
        num.times do |c|
          chan = ch[c]
          chan.memValue = 0
          chan.op1Out[0] = 0
          chan.op1Out[1] = 0
          chan.fc = 0

          4.times do |s|
            slot = chan.slot[s]
            slot.incr = -1
            slot.key = 0
            slot.phase = 0
            slot.ssg = 0
            slot.ssgn = 0
            slot.state = EG_OFF
            slot.volume = MAX_ATT_INDEX
            slot.volOut = MAX_ATT_INDEX.to_u32!
          end
        end
      end

      # Initializes generic tables.
      protected def initTables : Nil
        n : Int32 = 0
        o : Float64 = 0.0
        m : Float64 = 0.0

        TL_RES_LEN.times do |x|
          m = (1 << 16) / (2 ** ((x + 1) * (ENV_STEP / 4.0) / 8.0))
          m = m.floor

          # We never reach (1<<16) here due to the (x+1).  Result fits within 16
          # bits at maximum.

          n = m.to_i32! # 16 bits here
          n >>= 4       # 12 bits here
          n = if Yuno.bitflag?(n, 1) # Round to nearest
                (n >> 1) + 1
              else
                n >> 1
              end
          # 11 bits here
          n <<= 2       # 13 bits here

          # 14 bits (with sign bit)
          @tlTab[x * 2 + 0] = n
          @tlTab[x * 2 + 1] = -(@tlTab[x * 2 + 0])

          # one entry in the 'Power' table use the following format, xxxxxyyyyyyyys with:
          #        s = sign bit
          # yyyyyyyy = 8-bits decimal part (0-TL_RES_LEN)
          # xxxxx    = 5-bits integer 'shift' value (0-31) but, since Power table output is 13 bits,
          #            any value above 13 (included) would be discarded.
          (1...13).each do |i|
            @tlTab[x * 2 + 0 + i * 2 * TL_RES_LEN] =   @tlTab[x * 2 + 0] >> i
            @tlTab[x * 2 + 1 + i * 2 * TL_RES_LEN] = -(@tlTab[x * 2 + 0 + i * 2 * TL_RES_LEN])
          end
        end

        # Build logarithmic sinus table
        SIN_LEN.times do |i|
          # Non-standard sinus
          m = Math.sin(((i * 2) + 1) * Math::PI / SIN_LEN) # Checked against real chip

          # We never reach zero here due to ((i * 2) + 1)

          # Conver to decibels.
          o = if m > 0.0
                8 * Math.log(1.0 / m) / Math.log(2.0)
              else
                8 * Math.log(-1.0 / m) / Math.log(2.0)
              end

          o = o / (ENV_STEP / 4)

          n = (2.0 * o).to_i32!

          # Round to nearest
          n = if Yuno.bitflag?(n, 1)
                (n >> 1) + 1
              else
                n >> 1
              end

          # 13-bits (8.5) value is formatted for above 'Power' table
          @sinTab[i] = (n * 2 + (m >= 0.0 ? 0 : 1)).to_u32!
        end

        # Build LFO PM modulation table.
        8u32.times do |i|
          128u32.times do |fnum| # 7-bits meaningful fnum
            value : UInt8 = 0
            offsetDepth : UInt32 = i
            offsetFnumBit : UInt32 = 0

            8.times do |step|
              value = 0
              7u32.times do |bitTmp| # 7 bits
                if Yuno.bitflag?(fnum, (1u32 << bitTmp)) # Only if bit "bitTmp" is set.
                  offsetFnumBit = bitTmp * 8
                  value += LFO_PM_OUTPUT[offsetFnumBit + offsetDepth][step]
                end
              end

              @lfoPmTable[(fnum * 32 * 8) + (i * 32) +  step      +  0] = value.to_i32!
              @lfoPmTable[(fnum * 32 * 8) + (i * 32) + (step ^ 7) +  8] = value.to_i32!
              @lfoPmTable[(fnum * 32 * 8) + (i * 32) +  step      + 16] = -(value.to_i32!)
              @lfoPmTable[(fnum * 32 * 8) + (i * 32) + (step ^ 7) + 24] = -(value.to_i32!)
            end
          end
        end
      end
    end
  end
end
