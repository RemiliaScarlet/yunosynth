#### Software implementation of Yamaha FM sound generator
####
#### Copyright Jarek Burczynski (bujar at mame dot net)
#### Copyright Tatsuyuki Satoh , MultiArcadeMachineEmulator development
#### Copyright (C) 2023-2024 Remilia Scarlet
#### BSD 3-Clause License

####
#### Yamaha OPN Mame tables and constants.
####

module Yuno::Chips
  module OPNMame
    ###
    ### Constants and Tables
    ###

    TYPE_SSG    =  1 # SSG support
    TYPE_LFOPAN =  2 # OPN type LFO and PAN
    TYPE_6CH    =  4 # FM 6 channel / 3 channel
    TYPE_DAC    =  8 # YM2612's DAC device
    TYPE_ADPCM  = 16 # Two ADPCM units
    TYPE_2610   = 32 # Bogus flag to differentiate between the 2608 and 2610.

    TYPE_YM2203 = TYPE_SSG
    TYPE_YM2608 = TYPE_SSG | TYPE_LFOPAN | TYPE_6CH | TYPE_ADPCM
    TYPE_YM2610 = TYPE_SSG | TYPE_LFOPAN | TYPE_6CH | TYPE_ADPCM | TYPE_2610
    TYPE_YM2612 = TYPE_DAC | TYPE_LFOPAN | TYPE_6CH

    FREQ_SH  = 16 # 16.16 fixed point (frequency calculations)
    EG_SH    = 16 # 16.16 fixed point (envelope generator timing)
    LFO_SH   = 24 # 8.24 fixed point (LFO calculations)
    TIMER_SH = 16 # 16.16 fixed point (timer calculations)

    FREQ_MASK = (1 << FREQ_SH) - 1
    INV_FREQ_MASK = ~(FREQ_MASK.to_u32!)

    ENV_BITS = 10
    ENV_LEN = 1 << ENV_BITS
    ENV_STEP = 128.0 / ENV_LEN

    MAX_ATT_INDEX = ENV_LEN - 1
    MIN_ATT_INDEX = 0

    EG_ATT = 4_u8
    EG_DEC = 3_u8
    EG_SUS = 2_u8
    EG_REL = 1_u8
    EG_OFF = 0_u8

    SIN_BITS = 10
    SIN_LEN = 1 << SIN_BITS
    SIN_MASK = SIN_LEN - 1

    TL_RES_LEN = 256

    # TL_TAB_LEN is calculated as:
    #   13 - sinus amplitude bits     (Y axis)
    #   2  - sinus sign bit           (Y axis)
    #   TL_RES_LEN - sinus resolution (X axis)
    TL_TAB_LEN = 13 * 2 * TL_RES_LEN

    ENV_QUIET = TL_TAB_LEN >> 3

    # Sustain level table (3dB per step)
    #
    # bit0, bit1, bit2, bit3, bit4, bit5, bit6
    # 1,    2,    4,    8,    16,   32,   64   (value)
    # 0.75, 1.5,  3,    6,    12,   24,   48   (dB)

    SL_TABLE = [
      (0 * (4.0 / ENV_STEP)).to_u32!, (1 * (4.0 / ENV_STEP)).to_u32!, (2 * (4.0 / ENV_STEP)).to_u32!,
      (3 * (4.0 / ENV_STEP)).to_u32!, (4 * (4.0 / ENV_STEP)).to_u32!, (5 * (4.0 / ENV_STEP)).to_u32!,
      (6 * (4.0 / ENV_STEP)).to_u32!, (7 * (4.0 / ENV_STEP)).to_u32!,

      (8 * (4.0 / ENV_STEP)).to_u32!,  (9 * (4.0 / ENV_STEP)).to_u32!, (10 * (4.0 / ENV_STEP)).to_u32!,
      (11 * (4.0 / ENV_STEP)).to_u32!, (12 * (4.0 / ENV_STEP)).to_u32!, (13 * (4.0 / ENV_STEP)).to_u32!,
      (14 * (4.0 / ENV_STEP)).to_u32!, (31 * (4.0 / ENV_STEP)).to_u32!
    ] of UInt32

    RATE_STEPS = 8
    EG_INC = [
      # cycle:0 1  2 3  4 5  6 7

      0,1, 0,1, 0,1, 0,1, # rates 00..11 0 (increment by 0 or 1)
      0,1, 0,1, 1,1, 0,1, # rates 00..11 1
      0,1, 1,1, 0,1, 1,1, # rates 00..11 2
      0,1, 1,1, 1,1, 1,1, # rates 00..11 3

      1,1, 1,1, 1,1, 1,1, # rate 12 0 (increment by 1)
      1,1, 1,2, 1,1, 1,2, # rate 12 1
      1,2, 1,2, 1,2, 1,2, # rate 12 2
      1,2, 2,2, 1,2, 2,2, # rate 12 3

      2,2, 2,2, 2,2, 2,2, # rate 13 0 (increment by 2)
      2,2, 2,4, 2,2, 2,4, # rate 13 1
      2,4, 2,4, 2,4, 2,4, # rate 13 2
      2,4, 4,4, 2,4, 4,4, # rate 13 3

      4,4, 4,4, 4,4, 4,4, # rate 14 0 (increment by 4)
      4,4, 4,8, 4,4, 4,8, # rate 14 1
      4,8, 4,8, 4,8, 4,8, # rate 14 2
      4,8, 8,8, 4,8, 8,8, # rate 14 3

      8,8, 8,8, 8,8, 8,8, # rates 15 0, 15 1, 15 2, 15 3 (increment by 8)
      16,16,16,16,16,16,16,16, # rates 15 2, 15 3 for attack
      0,0, 0,0, 0,0, 0,0, # infinity rates for attack and decay(s)
    ] of UInt8

    # Envelope Generator rates (32 + 64 rates + 32 RKS).
    #
    # note that there is no O(17) in this table - it's directly in the code.
    EG_RATE_SELECT = [
      # 32 infinite time rates
      (18 * RATE_STEPS).to_u8!, (18 * RATE_STEPS).to_u8!, (18 * RATE_STEPS).to_u8!, (18 * RATE_STEPS).to_u8!,
      (18 * RATE_STEPS).to_u8!, (18 * RATE_STEPS).to_u8!, (18 * RATE_STEPS).to_u8!, (18 * RATE_STEPS).to_u8!,
      (18 * RATE_STEPS).to_u8!, (18 * RATE_STEPS).to_u8!, (18 * RATE_STEPS).to_u8!, (18 * RATE_STEPS).to_u8!,
      (18 * RATE_STEPS).to_u8!, (18 * RATE_STEPS).to_u8!, (18 * RATE_STEPS).to_u8!, (18 * RATE_STEPS).to_u8!,
      (18 * RATE_STEPS).to_u8!, (18 * RATE_STEPS).to_u8!, (18 * RATE_STEPS).to_u8!, (18 * RATE_STEPS).to_u8!,
      (18 * RATE_STEPS).to_u8!, (18 * RATE_STEPS).to_u8!, (18 * RATE_STEPS).to_u8!, (18 * RATE_STEPS).to_u8!,
      (18 * RATE_STEPS).to_u8!, (18 * RATE_STEPS).to_u8!, (18 * RATE_STEPS).to_u8!, (18 * RATE_STEPS).to_u8!,
      (18 * RATE_STEPS).to_u8!, (18 * RATE_STEPS).to_u8!, (18 * RATE_STEPS).to_u8!, (18 * RATE_STEPS).to_u8!,

      # rates 00-11
      (0 * RATE_STEPS).to_u8!, (1 * RATE_STEPS).to_u8!, (2 * RATE_STEPS).to_u8!, (3 * RATE_STEPS).to_u8!,
      (0 * RATE_STEPS).to_u8!, (1 * RATE_STEPS).to_u8!, (2 * RATE_STEPS).to_u8!, (3 * RATE_STEPS).to_u8!,
      (0 * RATE_STEPS).to_u8!, (1 * RATE_STEPS).to_u8!, (2 * RATE_STEPS).to_u8!, (3 * RATE_STEPS).to_u8!,
      (0 * RATE_STEPS).to_u8!, (1 * RATE_STEPS).to_u8!, (2 * RATE_STEPS).to_u8!, (3 * RATE_STEPS).to_u8!,
      (0 * RATE_STEPS).to_u8!, (1 * RATE_STEPS).to_u8!, (2 * RATE_STEPS).to_u8!, (3 * RATE_STEPS).to_u8!,
      (0 * RATE_STEPS).to_u8!, (1 * RATE_STEPS).to_u8!, (2 * RATE_STEPS).to_u8!, (3 * RATE_STEPS).to_u8!,
      (0 * RATE_STEPS).to_u8!, (1 * RATE_STEPS).to_u8!, (2 * RATE_STEPS).to_u8!, (3 * RATE_STEPS).to_u8!,
      (0 * RATE_STEPS).to_u8!, (1 * RATE_STEPS).to_u8!, (2 * RATE_STEPS).to_u8!, (3 * RATE_STEPS).to_u8!,
      (0 * RATE_STEPS).to_u8!, (1 * RATE_STEPS).to_u8!, (2 * RATE_STEPS).to_u8!, (3 * RATE_STEPS).to_u8!,
      (0 * RATE_STEPS).to_u8!, (1 * RATE_STEPS).to_u8!, (2 * RATE_STEPS).to_u8!, (3 * RATE_STEPS).to_u8!,
      (0 * RATE_STEPS).to_u8!, (1 * RATE_STEPS).to_u8!, (2 * RATE_STEPS).to_u8!, (3 * RATE_STEPS).to_u8!,
      (0 * RATE_STEPS).to_u8!, (1 * RATE_STEPS).to_u8!, (2 * RATE_STEPS).to_u8!, (3 * RATE_STEPS).to_u8!,

      # rate 12
      (4 * RATE_STEPS).to_u8!, (5 * RATE_STEPS).to_u8!, (6 * RATE_STEPS).to_u8!, (7 * RATE_STEPS).to_u8!,

      # rate 13
      (8 * RATE_STEPS).to_u8!, (9 * RATE_STEPS).to_u8!, (10 * RATE_STEPS).to_u8!, (11 * RATE_STEPS).to_u8!,

      # rate 14
      (12 * RATE_STEPS).to_u8!, (13 * RATE_STEPS).to_u8!, (14 * RATE_STEPS).to_u8!, (15 * RATE_STEPS).to_u8!,

      # rate 15
      (16 * RATE_STEPS).to_u8!, (16 * RATE_STEPS).to_u8!, (16 * RATE_STEPS).to_u8!, (16 * RATE_STEPS).to_u8!,

      # 32 placeholder rates (same as 15 3)
      (16 * RATE_STEPS).to_u8!, (16 * RATE_STEPS).to_u8!, (16 * RATE_STEPS).to_u8!, (16 * RATE_STEPS).to_u8!,
      (16 * RATE_STEPS).to_u8!, (16 * RATE_STEPS).to_u8!, (16 * RATE_STEPS).to_u8!, (16 * RATE_STEPS).to_u8!,
      (16 * RATE_STEPS).to_u8!, (16 * RATE_STEPS).to_u8!, (16 * RATE_STEPS).to_u8!, (16 * RATE_STEPS).to_u8!,
      (16 * RATE_STEPS).to_u8!, (16 * RATE_STEPS).to_u8!, (16 * RATE_STEPS).to_u8!, (16 * RATE_STEPS).to_u8!,
      (16 * RATE_STEPS).to_u8!, (16 * RATE_STEPS).to_u8!, (16 * RATE_STEPS).to_u8!, (16 * RATE_STEPS).to_u8!,
      (16 * RATE_STEPS).to_u8!, (16 * RATE_STEPS).to_u8!, (16 * RATE_STEPS).to_u8!, (16 * RATE_STEPS).to_u8!,
      (16 * RATE_STEPS).to_u8!, (16 * RATE_STEPS).to_u8!, (16 * RATE_STEPS).to_u8!, (16 * RATE_STEPS).to_u8!,
      (16 * RATE_STEPS).to_u8!, (16 * RATE_STEPS).to_u8!, (16 * RATE_STEPS).to_u8!, (16 * RATE_STEPS).to_u8!
    ] of UInt8

    # Envelope Generator counter shifts (32 + 64 rates + 32 RKS).
    EG_RATE_SHIFT = [
      # 32 infinite time rates
      0u8, 0u8, 0u8, 0u8, 0u8, 0u8, 0u8, 0u8,
      0u8, 0u8, 0u8, 0u8, 0u8, 0u8, 0u8, 0u8,
      0u8, 0u8, 0u8, 0u8, 0u8, 0u8, 0u8, 0u8,
      0u8, 0u8, 0u8, 0u8, 0u8, 0u8, 0u8, 0u8,

      # rates 00-11
      11u8, 11u8, 11u8, 11u8,
      10u8, 10u8, 10u8, 10u8,
      9u8, 9u8, 9u8, 9u8,
      8u8, 8u8, 8u8, 8u8,
      7u8, 7u8, 7u8, 7u8,
      6u8, 6u8, 6u8, 6u8,
      5u8, 5u8, 5u8, 5u8,
      4u8, 4u8, 4u8, 4u8,
      3u8, 3u8, 3u8, 3u8,
      2u8, 2u8, 2u8, 2u8,
      1u8, 1u8, 1u8, 1u8,
      0u8, 0u8, 0u8, 0u8,

      # rate 12
      0u8, 0u8, 0u8, 0u8,

      # rate 13
      0u8, 0u8, 0u8, 0u8,

      # rate 14
      0u8, 0u8, 0u8, 0u8,

      # rate 15
      0u8, 0u8, 0u8, 0u8,

      # 32 dummy rates (same as 15 3)
      0u8, 0u8, 0u8, 0u8, 0u8, 0u8, 0u8, 0u8,
      0u8, 0u8, 0u8, 0u8, 0u8, 0u8, 0u8, 0u8,
      0u8, 0u8, 0u8, 0u8, 0u8, 0u8, 0u8, 0u8,
      0u8, 0u8, 0u8, 0u8, 0u8, 0u8, 0u8, 0u8
    ] of UInt8

    DT_TAB = [
      # This is YM2151 and YM2612 phase increment data (in 10.10 fixed point
      # format)

      # FD = 0
      0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
      0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,

      # FD = 1
      0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2,
      2, 3, 3, 3, 4, 4, 4, 5, 5, 6, 6, 7, 8, 8, 8, 8,

      # FD = 2
      1, 1, 1, 1, 2, 2, 2,  2,  2,  3,  3,  3,  4,  4,  4,  5,
      5, 6, 6, 7, 8, 8, 9, 10, 11, 12, 13, 14, 16, 16, 16, 16,

      # FD = 3
      2, 2, 2,  2,  2,  3,  3,  3,  4,  4,  4,  5,  5,  6,  6,  7,
      8, 8, 9, 10, 11, 12, 13, 14, 16, 17, 19, 20, 22, 22, 22, 22
    ] of UInt8

    # OPN key frequency number -> key code follow table.  Fnum higher 4bit ->
    # keycode lower 2bit.
    OPN_FK_TABLE = [ 0, 0, 0, 0, 0, 0, 0, 1, 2, 3, 3, 3, 3, 3, 3, 3 ] of UInt8

    # 8 LFO speed parameters.  Each value represents number of samples that
    # one LFO level will last
    LFO_SAMPLES_PER_STEP = [ 108, 77, 71, 67, 62, 44, 8, 5 ] of UInt32

    # There are 4 different LFO AM depths available, they are:
    #   0 dB, 1.4 dB, 5.9 dB, 11.8 dB
    #
    # Here is how it is generated (in EG steps):
    #
    #   11.8 dB = 0, 2, 4, 6, 8, 10,12,14,16...126,126,124,122,120,118,....4,2,0
    #    5.9 dB = 0, 1, 2, 3, 4, 5, 6, 7, 8....63, 63, 62, 61, 60, 59,.....2,1,0
    #    1.4 dB = 0, 0, 0, 0, 1, 1, 1, 1, 2,...15, 15, 15, 15, 14, 14,.....0,0,0
    #
    # (1.4 dB is losing precision as you can see)
    #
    # It's implemented as generator from 0..126 with step 2 then a shift right
    # N times, where N is:
    #
    #   8 for 0 dB
    #   3 for 1.4 dB
    #   1 for 5.9 dB
    #   0 for 11.8 dB
    LFO_AMS_DEPTH_SHIFT = [8, 3, 1, 0] of UInt8

    # There are 8 different LFO PM depths available, they are:
    #   0, 3.4, 6.7, 10, 14, 20, 40, 80 (cents)
    #
    # Modulation level at each depth depends on F-NUMBER bits: 4,5,6,7,8,9,10
    # (bits 8,9,10 = FNUM MSB from OCT/FNUM register).
    #
    # Here we store only first quarter (positive one) of full waveform.  Full
    # table (lfo_pm_table) containing all 128 waveforms is build at run (init)
    # time.
    #
    # One value in table below represents 4 (four) basic LFO steps (1 PM step
    # = 4 AM steps).
    #
    # For example:
    #   at `LFO SPEED = 0` (which is 108 samples per basic LFO step)
    #   one value from `LFO_PM_OUTPUT` table lasts for 432 consecutive
    #   samples (4*108=432) and one full LFO waveform cycle lasts for 13824
    #   samples (32*432=13824; 32 because we store only a quarter of whole
    #   waveform in the table below)
    LFO_PM_OUTPUT = [
      # 7 bits meaningful (of F-NUMBER), 8 LFO output levels per one depth
      # (out of 32), 8 LFO depths.

      # FNUM BIT 4: 000 0001xxxx
      [ 0,   0,   0,   0,   0,   0,   0,   0 ] of UInt8, # DEPTH 0
      [ 0,   0,   0,   0,   0,   0,   0,   0 ] of UInt8, # DEPTH 1
      [ 0,   0,   0,   0,   0,   0,   0,   0 ] of UInt8, # DEPTH 2
      [ 0,   0,   0,   0,   0,   0,   0,   0 ] of UInt8, # DEPTH 3
      [ 0,   0,   0,   0,   0,   0,   0,   0 ] of UInt8, # DEPTH 4
      [ 0,   0,   0,   0,   0,   0,   0,   0 ] of UInt8, # DEPTH 5
      [ 0,   0,   0,   0,   0,   0,   0,   0 ] of UInt8, # DEPTH 6
      [ 0,   0,   0,   0,   1,   1,   1,   1 ] of UInt8, # DEPTH 7

      # FNUM BIT 5: 000 0010xxxx
      [ 0,   0,   0,   0,   0,   0,   0,   0 ] of UInt8, # DEPTH 0
      [ 0,   0,   0,   0,   0,   0,   0,   0 ] of UInt8, # DEPTH 1
      [ 0,   0,   0,   0,   0,   0,   0,   0 ] of UInt8, # DEPTH 2
      [ 0,   0,   0,   0,   0,   0,   0,   0 ] of UInt8, # DEPTH 3
      [ 0,   0,   0,   0,   0,   0,   0,   0 ] of UInt8, # DEPTH 4
      [ 0,   0,   0,   0,   0,   0,   0,   0 ] of UInt8, # DEPTH 5
      [ 0,   0,   0,   0,   1,   1,   1,   1 ] of UInt8, # DEPTH 6
      [ 0,   0,   1,   1,   2,   2,   2,   3 ] of UInt8, # DEPTH 7

      # FNUM BIT 6: 000 0100xxxx
      [ 0,   0,   0,   0,   0,   0,   0,   0 ] of UInt8, # DEPTH 0
      [ 0,   0,   0,   0,   0,   0,   0,   0 ] of UInt8, # DEPTH 1
      [ 0,   0,   0,   0,   0,   0,   0,   0 ] of UInt8, # DEPTH 2
      [ 0,   0,   0,   0,   0,   0,   0,   0 ] of UInt8, # DEPTH 3
      [ 0,   0,   0,   0,   0,   0,   0,   1 ] of UInt8, # DEPTH 4
      [ 0,   0,   0,   0,   1,   1,   1,   1 ] of UInt8, # DEPTH 5
      [ 0,   0,   1,   1,   2,   2,   2,   3 ] of UInt8, # DEPTH 6
      [ 0,   0,   2,   3,   4,   4,   5,   6 ] of UInt8, # DEPTH 7

      # FNUM BIT 7: 000 1000xxxx
      [ 0,   0,   0,   0,   0,   0,   0,   0 ] of UInt8, # DEPTH 0
      [ 0,   0,   0,   0,   0,   0,   0,   0 ] of UInt8, # DEPTH 1
      [ 0,   0,   0,   0,   0,   0,   1,   1 ] of UInt8, # DEPTH 2
      [ 0,   0,   0,   0,   1,   1,   1,   1 ] of UInt8, # DEPTH 3
      [ 0,   0,   0,   1,   1,   1,   1,   2 ] of UInt8, # DEPTH 4
      [ 0,   0,   1,   1,   2,   2,   2,   3 ] of UInt8, # DEPTH 5
      [ 0,   0,   2,   3,   4,   4,   5,   6 ] of UInt8, # DEPTH 6
      [ 0,   0,   4,   6,   8,   8, 0xA, 0xC ] of UInt8, # DEPTH 7

      # FNUM BIT 8: 001 0000xxxx
      [ 0,   0,   0,   0,   0,   0,   0,   0 ] of UInt8, # DEPTH 0
      [ 0,   0,   0,   0,   1,   1,   1,   1 ] of UInt8, # DEPTH 1
      [ 0,   0,   0,   1,   1,   1,   2,   2 ] of UInt8, # DEPTH 2
      [ 0,   0,   1,   1,   2,   2,   3,   3 ] of UInt8, # DEPTH 3
      [ 0,   0,   1,   2,   2,   2,   3,   4 ] of UInt8, # DEPTH 4
      [ 0,   0,   2,   3,   4,   4,   5,   6 ] of UInt8, # DEPTH 5
      [ 0,   0,   4,   6,   8,   8, 0xA, 0xC ] of UInt8, # DEPTH 6
      [ 0,   0,   8, 0xC,0x10,0x10,0x14,0x18 ] of UInt8, # DEPTH 7

      # FNUM BIT 9: 010 0000xxxx
      [ 0,   0,   0,   0,   0,   0,   0,   0 ] of UInt8, # DEPTH 0
      [ 0,   0,   0,   0,   2,   2,   2,   2 ] of UInt8, # DEPTH 1
      [ 0,   0,   0,   2,   2,   2,   4,   4 ] of UInt8, # DEPTH 2
      [ 0,   0,   2,   2,   4,   4,   6,   6 ] of UInt8, # DEPTH 3
      [ 0,   0,   2,   4,   4,   4,   6,   8 ] of UInt8, # DEPTH 4
      [ 0,   0,   4,   6,   8,   8, 0xa, 0xC ] of UInt8, # DEPTH 5
      [ 0,   0,   8, 0xC,0x10,0x10,0x14,0x18 ] of UInt8, # DEPTH 6
      [ 0,   0,0x10,0x18,0x20,0x20,0x28,0x30 ] of UInt8, # DEPTH 7

      # FNUM BIT10: 100 0000xxxx
      [ 0,   0,   0,   0,   0,   0,   0,   0 ] of UInt8, # DEPTH 0
      [ 0,   0,   0,   0,   4,   4,   4,   4 ] of UInt8, # DEPTH 1
      [ 0,   0,   0,   4,   4,   4,   8,   8 ] of UInt8, # DEPTH 2
      [ 0,   0,   4,   4,   8,   8, 0xC, 0xC ] of UInt8, # DEPTH 3
      [ 0,   0,   4,   8,   8,   8, 0xC,0x10 ] of UInt8, # DEPTH 4
      [ 0,   0,   8, 0xC,0x10,0x10,0x14,0x18 ] of UInt8, # DEPTH 5
      [ 0,   0,0x10,0x18,0x20,0x20,0x28,0x30 ] of UInt8, # DEPTH 6
      [ 0,   0,0x20,0x30,0x40,0x40,0x50,0x60 ] of UInt8  # DEPTH 7
    ] of Array(UInt8)

    SLOT1 = 0
    SLOT2 = 2
    SLOT3 = 1
    SLOT4 = 3

    OUTD_RIGHT  = 1
    OUTD_LEFT   = 2
    OUTD_CENTER = 3

    OPN_PRES = [
      2 * 12,
      2 * 12,
      6 * 12,
      3 * 12
    ]

    SSG_PRES = [1, 1, 4, 2]
  end
end
