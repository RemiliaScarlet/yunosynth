#### Software implementation of Yamaha FM sound generator
####
#### Copyright Jarek Burczynski (bujar at mame dot net)
#### Copyright Tatsuyuki Satoh , MultiArcadeMachineEmulator development
#### Copyright (C) 2023-2024 Remilia Scarlet
#### BSD 3-Clause License
require "./deltat"
require "./ym2608-mame-constants"

module Yuno::Chips
  # YM2203 sound chip emulator.
  class YM2608 < Yuno::AbstractChip
    # :nodoc:
    class YM2608Mame < Yuno::AbstractEmulator
      include Yuno::Chips::OPNMame

      class Channel
        property flag      : UInt8  = 0 # Port state
        property flagMask  : UInt8  = 0 # Arrived flag mask
        property nowData   : UInt8  = 0 # Current ROM data
        property nowAddr   : UInt32 = 0 # Current ROM address
        property nowStep   : UInt32 = 0
        property step      : UInt32 = 0
        property start     : UInt32 = 0 # Sample data start address
        property finish    : UInt32 = 0 # Sample data end address
        property il        : UInt8  = 0 # Instrument level
        property adpcmAcc  : Int32  = 0 # Accumulator
        property adpcmStep : Int32  = 0 # Step
        property adpcmOut  : Int32  = 0 # Speedup - Hiro-shi!!
        property volMul    : Int8   = 0 # Volume in 0.75dB steps
        property volShift  : UInt8  = 0 # Volume in -6dB steps
        property pan       : Pointer(Int32) = Pointer(Int32).null # Pointer to @outAdpcm
        property muted     : UInt8 = 0

        def initialize
        end
      end

      @regs : Bytes = Bytes.new(512)
      protected getter opn : FmOpn
      @ch : Array(FmCh)
      @addrA1 : UInt8 = 0

      @pcmBuf : Array(UInt8) = ADPCM_ROM
      @pcmSize : UInt32 = ADPCM_ROM.size.to_u32!
      @adpcmTl : UInt8 = 0
      @adpcm : Array(Channel)
      @adpcmReg : Array(UInt32) = Array(UInt32).new(0x30, 0u32)
      @adpcmArrivedEndAddress : UInt8 = 0
      @deltaT : DeltaT = DeltaT.new # Delta-T ADPCM unit
      @muteDeltaT : UInt8 = 0

      @flagMask : UInt8 = 0 # YM2608 only
      @irqMask : UInt8 = 0 # YM2608 only

      # These are not the bytes you are looking for...
      # Here for speedup purposes.
      @jediTable : Array(Int32) = Array(Int32).new(49 * 16, 0i32)

      @updateReq : Proc(Nil)

      def initialize(@updateReq : Proc(Nil), param : AbstractChip?, clock : UInt32, rate : UInt32, ssg : SsgCallbacks)
        @adpcm = Array(Channel).new(6) { |_| Channel.new }

        initTables

        @ch = Array(FmCh).new(6) { |_| FmCh.new }
        @opn = FmOpn.new(@ch)
        @opn.st.param = param
        @opn.type = TYPE_YM2608.to_u8
        @opn.st.clock = clock
        @opn.st.rate = rate

        # External handlers
        @opn.st.ssg = ssg

        # Delta-T
        @deltaT.memory = Bytes.new(0)
        @deltaT.memoryMask = 0

        @deltaT.statusSetHandler = ->deltaTStatusSet(Yuno::AbstractEmulator, UInt8)
        @deltaT.statusResetHandler = ->deltaTStatusReset(Yuno::AbstractEmulator, UInt8)
        @deltaT.statusChangeWhichChip = self
        @deltaT.statusChangeEOSBit  = 0x04 # Status flag: set bit 2 on End of Sample
        @deltaT.statusChangeBRDYBit = 0x08 # Status flag: set bit 3 on BRDY
        @deltaT.statusChangeZeroBit = 0x10 # Status flag: set bit 4 if silence
                                           # continues for more than 290
                                           # miliseconds while recording the
                                           # ADPCM.

        # ADPCM Rhythm
        @pcmSize = 0x2000_u32
        if @pcmBuf.size != @pcmSize
          Yuno.warn(sprintf("YM2608: ADPCM ROM size unexpected: $%04x != $04x", @pcmSize, @pcmBuf.size))
        end

        initAdpcmATable
      end

      protected def initAdpcmATable : Nil
        value : Int32 = 0

        49.times do |step|
          # Loop over all nibbles and compute the difference
          16.times do |nib|
            value = ((2 * (nib & 0x07) + 1) * STEPS[step]).tdiv(8)
            @jediTable[step * 16 + nib] = (Yuno.bitflag?(nib, 0x08) ? -value : value)
          end
        end
      end

      # ADPCM A (non-control type): Calculate one channel output.
      @[AlwaysInline]
      def adpcmaCalcChan(ch : Channel) : Nil
        return if ch.muted != 0

        ch.nowStep += ch.step
        if ch.nowStep >= (1u32 << ADPCM_SHIFT)
          step : UInt32 = ch.nowStep >> ADPCM_SHIFT
          ch.nowStep &= (1u32 << ADPCM_SHIFT) - 1

          data : UInt8 = 0

          loop do
            # End check
            #
            # 11-06-2001 JB: corrected comparison. Was > instead of ==
            # YM2610 checks lower 20 bits only, the 4 MSB bits are sample bank
            # Here we use 1<<21 to compensate for nibble calculations
            if (ch.nowAddr & ((1u32 << 21) - 1)) == ((ch.finish << 1) & ((1u32 << 21) - 1))
              ch.flag = 0
              @adpcmArrivedEndAddress |= ch.flagMask
              return
            end

            if Yuno.bitflag?(ch.nowAddr, 1)
              data = ch.nowData & 0x0F
            else
              ch.nowData = @pcmBuf[ch.nowAddr >> 1]
              data = (ch.nowData >> 4) & 0x0F
            end

            ch.nowAddr += 1
            ch.adpcmAcc += @jediTable[ch.adpcmStep + data]

            # Extend 12-bit signed int
            if Yuno.bitflag?(ch.adpcmAcc, ~0x7FF)
              ch.adpcmAcc |= ~0xFFF
            else
              ch.adpcmAcc &= 0xFFF
            end

            ch.adpcmStep = (ch.adpcmStep.to_i64! + STEP_INC.get!(data & 7)).clamp(0, 48 * 16).to_i32!

            step -= 1
            break if step == 0
          end

          # Calc pcm * volume data.
          # multiply, shift and mask out 2 LSB bits.
          ch.adpcmOut = ((ch.adpcmAcc * ch.volMul.to_i32!) >> ch.volShift) & ~3_i32
        end

        ch.pan[0] += ch.adpcmOut
      end

      # ADPCM type A write
      @[AlwaysInline]
      def adpcmAWrite(r : Int, v : UInt8) : Nil
        @adpcmReg[r] = v.to_u32!
        case r
        when 0x00 # DM,--,C5,C4,C3,C2,C1,C0
          unless Yuno.bitflag?(v, 0x80)
            # Key on
            6.times do |i|
              if Yuno.bitflag?(v >> i, 1)
                # *** start adpcm ***
                # The .step variable is already set and for the YM2608 it is different on channels 4 and 5.
                @adpcm.get!(i).nowAddr = @adpcm.get!(i).start << 1
                @adpcm.get!(i).nowStep = 0u32
                @adpcm.get!(i).adpcmAcc = 0
                @adpcm.get!(i).adpcmStep = 0
                @adpcm.get!(i).adpcmOut = 0
                @adpcm.get!(i).flag = 1u8

                # Remi: I'm pretty sure this could never happen since we always
                # have it set to a default.
                if @pcmBuf.empty?
                  {% if flag?(:yunosynth_debug) %}
                    RemiLib.log.error("YM2608/YM2610: ADPCM-A ROM not mapped")
                  {% end %}
                  @adpcm.get!(i).flag = 0u8
                else
                  # Check end in range
                  if @adpcm.get!(i).finish >= @pcmSize
                    {% if flag?(:yunosynth_debug) %}
                      Yuno.warn(sprintf("YM2608/YM2610: ADPCM-A end out of range: $%08x", @adpcm.get!(i).finish))
                    {% end %}
                  end

                  # Check start in range
                  if @adpcm.get!(i).start >= @pcmSize
                    {% if flag?(:yunosynth_debug) %}
                      Yuno.warn(sprintf("YM2608/YM2610: ADPCM-A start out of range: $%08x", @adpcm.get!(i).start))
                    {% end %}
                    @adpcm.get!(i).flag = 0u8
                  end
                end
              end
            end
          else # unless Yuno.bitflag?(v, 0x80)
            # Key off
            6.times do |i|
              @adpcm.get!(i).flag = 0u8 if Yuno.bitflag?(v >> i, 1)
            end
          end

        when 0x01 # B0-5 = TL
          @adpcmTl = (v & 0x3F_u8) ^ 0x3F_u8
          6.times do |i|
            ch : Channel = @adpcm.get!(i)
            volume : UInt16 = @adpcmTl.to_u16! + ch.il.to_u16!
            if volume >= 63 # This is correct, 63 = quiet
              ch.volMul = 0i8
              ch.volShift = 0u8
            else
              ch.volMul = (15 - (volume & 7)).to_i8! # So called 0.75dB

              # Yamaha engineers used the approximation: each -6 dB is close to
              # divide by two (shift right).
              ch.volShift = (1 + (volume >> 3)).to_u8!
            end

            # Calc pcm * volume data.
            # Multiply, shift and mask out low 2 bits.
            ch.adpcmOut = ((ch.adpcmAcc * ch.volMul.to_i32!) >> ch.volShift) & ~3_i32
          end

        else
          c : UInt8 = (r & 0x07).to_u8!
          return if c >= 0x06
          ch : Channel = @adpcm.get!(c)
          case r & 0x38
          when 0x08 # B7=L,B6=R, B4-0=IL
            ch.il = (v & 0x1F) ^ 0x1F_u8
            volume = @adpcmTl.to_u16! + ch.il.to_u16!

            if volume >= 63 # This is correct, 63 = quiet
              ch.volMul = 0i8
              ch.volShift = 0u8
            else
              ch.volMul = (15 - (volume & 7)).to_i8! # So called 0.75dB

              # Yamaha engineers used the approximation: each -6 dB is close to
              # divide by two (shift right).
              ch.volShift = (1 + (volume >> 3)).to_u8!
            end

            ch.pan = @opn.outAdpcm.to_unsafe + ((v >> 6) & 0x03)

            # Calc pcm * volume data.
            # Multiply, shift and mask out low 2 bits.
            ch.adpcmOut = ((ch.adpcmAcc * ch.volMul.to_i32!) >> ch.volShift) & ~3_i32

          when 0x10, 0x18
            @adpcm.get!(c).start   = ((@adpcmReg.get!(0x18 + c) * 0x0100_u32) | @adpcmReg.get!(0x10 + c)) << ADPCMA_ADDRESS_SHIFT

          when 0x20, 0x28
            ch = @adpcm.get!(c)
            ch.finish  = ((@adpcmReg.get!(0x28 + c) * 0x0100_u32) | @adpcmReg.get!(0x20 + c)) << ADPCMA_ADDRESS_SHIFT
            ch.finish += (1u32 << ADPCMA_ADDRESS_SHIFT) - 1
          end
        end
      end

      @[AlwaysInline]
      def irqFlagWrite(v : Int) : Nil
        if Yuno.bitflag?(v, 0x80)
          # don't touch BUFRDY flag otherwise we'd have to call ymdeltat module to set the flag back
          statusReset(@opn.st, 0xF7)
        else
          # Set status flag mask
          @flagMask = ~((v & 0x1F).to_u8!)
          irqMaskSet(@opn.st, @irqMask & @flagMask)
        end
      end

      # Compatible mode & IRQ enable control 0x29
      @[AlwaysInline]
      def irqMaskWrite(v : Int) : Nil
        # SCH,xx,xxx,EN_ZERO,EN_BRDY,EN_EOS,EN_TB,EN_TA

        # extend 3ch. enable/disable
        if Yuno.bitflag?(v, 0x80)
          @opn.type |= TYPE_6CH # OPNA mode - 6 FM channels
        else
          @opn.type &= (~TYPE_6CH) # OPN mode - 3 FM channels
        end

        # IRQ MASK store and set
        @irqMask = (v & 0x1F).to_u8!
        irqMaskSet(@opn.st, @irqMask & @flagMask)
      end

      # Generate samples for the YM2608.
      @[AlwaysInline]
      def update(outputs : OutputBuffers, samples : UInt32) : Nil
        outFm = @opn.outFm
        outL : Slice(Int32) = outputs[0]
        outR : Slice(Int32) = outputs[1]
        lt : Int32 = 0
        rt : Int32 = 0

        cch0 : FmCh = {% if flag?(:yunosynth_wd40) %} @ch.get!(0) {% else %} @ch[0] {% end %}
        cch1 : FmCh = {% if flag?(:yunosynth_wd40) %} @ch.get!(1) {% else %} @ch[1] {% end %}
        cch2 : FmCh = {% if flag?(:yunosynth_wd40) %} @ch.get!(2) {% else %} @ch[2] {% end %}
        cch3 : FmCh = {% if flag?(:yunosynth_wd40) %} @ch.get!(3) {% else %} @ch[3] {% end %}
        cch4 : FmCh = {% if flag?(:yunosynth_wd40) %} @ch.get!(4) {% else %} @ch[4] {% end %}
        cch5 : FmCh = {% if flag?(:yunosynth_wd40) %} @ch.get!(5) {% else %} @ch[5] {% end %}

        # Check once, then we can avoid bounds checks below.
        if outL.size < samples || outR.size < samples
          raise "YM208 update: Bad number of samples (expected #{samples}, " \
                "but buffers have #{outL.size} and #{outR.size})"
        end

        # Refresh PG and EG
        refreshFcEgChan(@opn, cch0)
        refreshFcEgChan(@opn, cch1)
        if Yuno.bitflag?(@opn.st.mode, 0xC0)
          # 3-slot mode
          if cch2.slot1.incr == -1
            refreshFcEgSlot(@opn, cch2.slot1, @opn.sl3.fc.get!(1).to_i32!, @opn.sl3.kcode.get!(1))
            refreshFcEgSlot(@opn, cch2.slot2, @opn.sl3.fc.get!(2).to_i32!, @opn.sl3.kcode.get!(2))
            refreshFcEgSlot(@opn, cch2.slot3, @opn.sl3.fc.get!(0).to_i32!, @opn.sl3.kcode.get!(0))
            refreshFcEgSlot(@opn, cch2.slot4, cch2.fc.to_i32!, cch2.kcode)
          end
        else
          refreshFcEgChan(@opn, cch2)
        end
        refreshFcEgChan(@opn, cch3)
        refreshFcEgChan(@opn, cch4)
        refreshFcEgChan(@opn, cch5)

        # Render audio
        samples.times do |i|
          advanceLfo(@opn)

          # Clear output acc.
          @opn.outAdpcm.put!(OUTD_LEFT, 0)
          @opn.outAdpcm.put!(OUTD_RIGHT, 0)
          @opn.outAdpcm.put!(OUTD_CENTER, 0)
          @opn.outDelta.put!(OUTD_LEFT, 0)
          @opn.outDelta.put!(OUTD_RIGHT, 0)
          @opn.outDelta.put!(OUTD_CENTER, 0)

          # Clear outputs
          outFm.fill(0, 0, 6)

          # Calculate FM
          {% for i in 0..5 %}
            chanCalc(@opn, cch{{i}}, {{i}}u32)
          {% end %}

          # DeltaT ADPCM
          if Yuno.bitflag?(@deltaT.portState, 0x80) && @muteDeltaT == 0
            @deltaT.adpcmCalc
          end

          # ADPCM A
          6.times do |j|
            {% if flag?(:yunosynth_wd40) %}
              if @adpcm.unsafe_fetch(j).flag != 0
                adpcmaCalcChan(@adpcm.unsafe_fetch(j))
              end
            {% else %}
              if @adpcm.get!(j).flag != 0
                adpcmaCalcChan(@adpcm.get!(j))
              end
            {% end %}
          end

          # Advance envelope generator
          @opn.egTimer += @opn.egTimerAdd
          while @opn.egTimer >= @opn.egTimerOverflow
            @opn.egTimer -= @opn.egTimerOverflow
            @opn.egCount += 1
            {% for i in 0..5 %}
              advanceEgChannel(@opn, cch{{i}}.slot)
            {% end %}
          end

          # Write audio data
          lt =  (@opn.outAdpcm.get!(OUTD_LEFT)  + @opn.outAdpcm.get!(OUTD_CENTER)) << 1
          rt =  (@opn.outAdpcm.get!(OUTD_RIGHT) + @opn.outAdpcm.get!(OUTD_CENTER)) << 1
          lt += (@opn.outDelta.get!(OUTD_LEFT)  + @opn.outDelta.get!(OUTD_CENTER)) >> 8
          rt += (@opn.outDelta.get!(OUTD_RIGHT) + @opn.outDelta.get!(OUTD_CENTER)) >> 8

          lt += (outFm.get!(0) & @opn.pan.get!(0))
          rt += (outFm.get!(0) & @opn.pan.get!(1))
          lt += (outFm.get!(1) & @opn.pan.get!(2))
          rt += (outFm.get!(1) & @opn.pan.get!(3))
          lt += (outFm.get!(2) & @opn.pan.get!(4))
          rt += (outFm.get!(2) & @opn.pan.get!(5))
          lt += (outFm.get!(3) & @opn.pan.get!(6))
          rt += (outFm.get!(3) & @opn.pan.get!(7))
          lt += (outFm.get!(4) & @opn.pan.get!(8))
          rt += (outFm.get!(4) & @opn.pan.get!(9))
          lt += (outFm.get!(5) & @opn.pan.get!(10))
          rt += (outFm.get!(5) & @opn.pan.get!(11))

          outL.unsafe_put(i, lt)
          outR.unsafe_put(i, rt)

          # Timer A Control
          internalTimerA(@opn.st, cch2)
        end

        internalTimerB(@opn.st, samples)

        # Check IRQ for DeltaT EOS
        statusSet(@opn.st, 0)
      end

      @[AlwaysInline]
      def deltaTStatusSet(chip : Yuno::AbstractEmulator, changeBits : UInt8) : Nil
        ch = chip.as(YM2608Mame)
        statusSet(ch.opn.st, changeBits)
      end

      @[AlwaysInline]
      def deltaTStatusReset(chip : Yuno::AbstractEmulator, changeBits : UInt8) : Nil
        ch = chip.as(YM2608Mame)
        statusReset(ch.opn.st, changeBits)
      end

      def reset : Nil
        # Reset prescaler
        opnPrescalerWrite(@opn, 0, 2)
        @deltaT.freqBase = @opn.st.freqBase

        # Reset SSG section
        @opn.st.ssg.reset.call

        # Register 0x29: Default value after reset is: enable only 3 FM channels
        # and enable all the status flags.
        irqMaskWrite(0x1F)

        # Register 0x10, A1=1: Default value is 1 for D4, D3, D2, and 0 for the
        # rest.
        irqFlagWrite(0x1C)

        opnWriteMode(@opn, 0x27, 0x30) # Mode 0, timer reset

        @opn.egTimer = 0
        @opn.egCount = 0

        statusReset(@opn.st, 0xFF)

        resetChannels(@opn.st, @opn.pch, 6)

        # Reset operator parameters
        0xB6.downto(0xB4) do |i|
          opnWriteReg(@opn, i,         0xC0)
          opnWriteReg(@opn, i | 0x100, 0xC0)
        end

        0xB2.downto(0x30) do |i|
          opnWriteReg(@opn, i,         0)
          opnWriteReg(@opn, i | 0x100, 0)
        end

        0x26.downto(0x20) do |i|
          opnWriteReg(@opn, i, 0)
        end

        # ADPCM: percussion sounds
        6.times do |i|
          if i <= 3
            # Channels 0 through 3
            @adpcm[i].step = ((1 << ADPCM_SHIFT).to_f32! * @opn.st.freqBase.to_f32! / 3.0_f32).to_u32!
          else
            # Channels 4 and 5 work with a slower clock
            @adpcm[i].step = ((1 << ADPCM_SHIFT).to_f32! * @opn.st.freqBase.to_f32! / 6.0_f32).to_u32!
          end

          @adpcm[i].start  = ADPCM_ROM_ADDR[i * 2]
          @adpcm[i].finish = ADPCM_ROM_ADDR[i * 2 + 1]

          @adpcm[i].nowAddr = 0
          @adpcm[i].nowStep = 0
          @adpcm[i].volMul = 0i8
          @adpcm[i].pan = @opn.outAdpcm.to_unsafe + OUTD_CENTER # Default center
          @adpcm[i].flagMask = 0
          @adpcm[i].flag = 0
          @adpcm[i].adpcmAcc = 0
          @adpcm[i].adpcmStep = 0
          @adpcm[i].adpcmOut = 0
        end

        @adpcmTl = 0x3F_u8
        @adpcmArrivedEndAddress = 0 # Not used for YM2608

        # Delta-T unit
        @deltaT.freqBase = @opn.st.freqBase
        @deltaT.outputPointer = Slice.new(@opn.outDelta.to_unsafe, @opn.outDelta.size)
        @deltaT.portShift = 5
        @deltaT.outputRange = 1 << 23
        @deltaT.adpcmReset(OUTD_CENTER, EMULATION_MODE_NORMAL)
      end

      @[AlwaysInline]
      def write(a : Int, v : UInt8) : Int32
        addr : Int32 = 0

        case a & 3
        when 0 # Address port 0
          @opn.st.address = v
          @addrA1 = 0

          # Write registers to SSG emulator
          if v < 16
            @opn.st.ssg.write.call(0, v.to_i32!)
          end

          # Prescaler selector: 2d, 2e, 2f
          if v >= 0x2D && v <= 0x2F
            opnPrescalerWrite(@opn, v, 2)
            # TODO: set ADPCM[c].step
            @deltaT.freqBase = @opn.st.freqBase
          end

        when 1 # Data port 0
          if @addrA1 == 0 # Verified on real YM2608
            addr = @opn.st.address.to_i32!
            @regs.put!(addr, v)

            case addr & 0xF0
            when 0x00 # SSG section
              # Write data to SSG emulator
              @opn.st.ssg.write.call(a.to_i32!, v.to_i32!)

            when 0x10 # 0x10-0x1F: Rhythm section
              @updateReq.call
              adpcmAWrite(addr - 0x10, v)

            when 0x20 # Mode register
              case addr
              when 0x29 # SCH,xx,xxx,EN_ZERO,EN_BRDY,EN_EOS,EN_TB,EN_TA
                irqMaskWrite(v)
              else
                @updateReq.call
                opnWriteMode(@opn, addr, v.to_i32!)
              end

            else # OPN section
              @updateReq.call
              opnWriteReg(@opn, addr, v.to_i32!)
            end
          end

        when 2 # Address port 1
          @opn.st.address = v
          @addrA1 = 1

        when 3 # Data port 1
          if @addrA1 == 1 # Verified on real YM2608
            addr = @opn.st.address.to_i32!
            @regs.put!(addr | 0x100, v)
            @updateReq.call

            case addr & 0xF0
            when 0x00 # Delta-T Port
              case addr
              when 0x0E # DAC data
                {% if flag?(:yunosynth_debug) %}
                  Yuno.warn(sprintf("YM2608/YM2610: write to DAC data (unimplemented), value = $%02x", v))
                {% end %}
                nil
              else
                @deltaT.adpcmWrite(addr, v)
              end

            when 0x10 # IRQ flag control
              irqFlagWrite(v) if addr == 0x10

            else
              opnWriteReg(@opn, addr.to_i32! | 0x100, v.to_i32!)
            end
          end
        end

        @opn.st.irq.to_i32!
      end

      @[AlwaysInline]
      def read(a : Int) : UInt8
        ret : UInt8 = 0
        addr : UInt8 = @opn.st.address

        case a & 3
        when 0 # Status 0: YM2203 compatible
          # BUSY:x:x:x:x:x:FLAGB:FLAGA
          ret = @opn.st.status & 0x83

        when 1 # Status 0, ID
          if addr < 16
            ret = @opn.st.ssg.read.call
          elsif addr == 0xFF
            ret = 0x01 # ID code
          end

        when 2 # Status 1: status 0 + ADPCM status
          # BUSY : x : PCMBUSY : ZERO : BRDY : EOS : FLAGB : FLAGA
          ret = (@opn.st.status & (@flagMask | 0x80)) | ((@deltaT.pcmBsy & 1) << 5)

        when 3
          if addr == 0x08
            ret = @deltaT.adpcmRead
          elsif addr == 0x0F
            {% if flag?(:yunosynth_debug) %}
              Yuno.warn("YM2608/YM2610: A/D conversion is accessed, but not implemented")
            {% end %}
            ret = 0x80 # 2's complement PCM data - result from A/D conversion
          end
        end

        ret
      end

      @[AlwaysInline]
      def timerOver(c : Int) : Int32
        case c
        when 1
          # Timer B
          timerBOver(@opn.st)

        when 0
          # Timer A
          @updateReq.call

          # Timer update
          timerAOver(@opn.st)

          # CSM mode key, TL control
          if Yuno.bitflag?(@opn.st.mode, 0x80)
            # CSM mode total level latch and auto key on
            csmKeyControl(@ch.get!(2))
          end
        end

        @opn.st.irq
      end

      def writePcmRom(romID : UInt8, romSize : Int32, dataStart : Int32, dataLength : Int32,
                      romData : Slice(UInt8)) : Nil
        case romID
        when 0x01 # ADPCM
          # Unused, it's constant
          nil

        when 0x02 # Delta-T
          if @deltaT.memory.size != romSize
            @deltaT.memory = Bytes.new(romSize, 0xFFu8)
            @deltaT.calcMemMask
          end

          return if dataStart > romSize
          dataLength = romSize - dataStart if (dataStart + dataLength) > romSize

          # Copy the memory
          dataLength.times do |i|
            # This can be guaranteed by the checks above
            @deltaT.memory.unsafe_put(i + dataStart, romData.unsafe_fetch(i))
          end
        end
      end

      def muteMask=(mask : UInt32) : Nil
        6.times do |ch|
          @opn.pch[ch].muted = ((mask >> ch) & 0x01).to_u8!
        end

        6.times do |ch|
          @adpcm[ch].muted = ((mask >> ch) & 0x01).to_u8!
        end

        @muteDeltaT = ((mask >> 12) & 0x01).to_u8!
      end
    end
  end
end
