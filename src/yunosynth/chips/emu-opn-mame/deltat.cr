#### YAMAHA DELTA-T adpcm sound emulation subroutine
#### used by fmopl.c (Y8950) and fm.c (YM2608 and YM2610/B)
####
#### Base program is YM2610 emulator by Hiromitsu Shioya.
#### Written by Tatsuyuki Satoh
#### Improvements by Jarek Burczynski (bujar at mame dot net)
#### Crystal port Copyright (C) 2023-2024 Remilia Scarlet
#### BSD 3-Clause License
####
####
#### History:
####
#### 03-08-2003 Jarek Burczynski:
####  - fixed BRDY flag implementation.
####
#### 24-07-2003 Jarek Burczynski, Frits Hilderink:
####  - fixed delault value for control2 in YM_DELTAT_ADPCM_Reset
####
#### 22-07-2003 Jarek Burczynski, Frits Hilderink:
####  - fixed external memory support
####
#### 15-06-2003 Jarek Burczynski:
####  - implemented CPU -> AUDIO ADPCM synthesis (via writes to the ADPCM data reg $08)
####  - implemented support for the Limit address register
####  - supported two bits from the control register 2 ($01): RAM TYPE (x1 bit/x8 bit), ROM/RAM
####  - implemented external memory access (read/write) via the ADPCM data reg reads/writes
####    Thanks go to Frits Hilderink for the example code.
####
#### 14-06-2003 Jarek Burczynski:
####  - various fixes to enable proper support for status register flags: BSRDY, PCM BSY, ZERO
####  - modified EOS handling
####
#### 05-04-2003 Jarek Burczynski:
####  - implemented partial support for external/processor memory on sample replay
####
#### 01-12-2002 Jarek Burczynski:
####  - fixed first missing sound in gigandes thanks to previous fix (interpolator) by ElSemi
####  - renamed/removed some YM_DELTAT struct fields
####
#### 28-12-2001 Acho A. Tang
####  - added EOS status report on ADPCM playback.
####
#### 05-08-2001 Jarek Burczynski:
####  - now_step is initialized with 0 at the start of play.
####
#### 12-06-2001 Jarek Burczynski:
####  - corrected end of sample bug in YM_DELTAT_ADPCM_CALC.
####    Checked on real YM2610 chip - address register is 24 bits wide.
####    Thanks go to Stefan Jokisch (stefan.jokisch@gmx.de) for tracking down the problem.
####
#### TO DO:
####      Check size of the address register on the other chips....
####
#### Version 0.72
####
#### sound chips that have this unit:
#### YM2608   OPNA
#### YM2610/B OPNB
#### Y8950    MSX AUDIO

module Yuno::Chips
  module OPNMame
    SHIFT = 16
    EMULATION_MODE_NORMAL = 0
    EMULATION_MODE_YM2610 = 1

    DELTA_MAX = 24576
    DELTA_MIN = 127
    DELTA_DEF = 127

    DECODE_RANGE = 32768
    DECODE_MIN = -DECODE_RANGE
    DECODE_MAX = DECODE_RANGE - 1

    # Forecast to next Forecast (rate = *8)
    DECODE_TABLEB1 = [
      # 1/8 , 3/8 , 5/8 , 7/8 , 9/8 , 11/8 , 13/8 , 15/8
       1,   3,   5,   7,   9,  11,  13,  15,
      -1,  -3,  -5,  -7,  -9, -11, -13, -15,
    ]

    # Delta to next delta (rate = *64)
    DECODE_TABLEB2 = [
      # 0.9 , 0.9 , 0.9 , 0.9 , 1.2 , 1.6 , 2.0 , 2.4
      57,  57,  57,  57, 77, 102, 128, 153,
      57,  57,  57,  57, 77, 102, 128, 153
    ]

    # 0-DRAM x1, 1-ROM, 2-DRAM x8, 3-ROM (3 is bad setting - not allowed by the manual).
    DRAM_RIGHT_SHIFT = [ 3, 0, 0, 0 ] of UInt8

    alias StatusChangeHandler = Proc(Yuno::AbstractEmulator, UInt8, Nil)

    # DELTA-T (adpcm type B) record.
    class DeltaT
      property memory : Bytes = Bytes.new(0)
      property outputPointer : Slice(Int32) = Slice(Int32).new(0, 0i32)
      property freqBase : Float64 = 0.0
      property memoryMask : UInt32 = 0
      property outputRange : Int32  = 0
      property portShift : UInt8  = 0 # Address bits shift-left (8 for YM2610, 6 for Y8950 and YM2608)
      getter portState : UInt8  = 0 # Port status

      @pan           : Pointer(Int32) = Pointer(Int32).null
      @nowAddr       : UInt32 = 0 # Current address
      @nowStep       : UInt32 = 0 # Current step
      @step          : UInt32 = 0 # Step
      @start         : UInt32 = 0 # Step address
      @limit         : UInt32 = 0 # Limit address
      @finish        : UInt32 = 0 # End address
      @delta         : UInt32 = 0 # Delta scale
      @volume        : Int32  = 0 # Current volume
      @acc           : Int32  = 0 # Shift measurement value
      @adpcmd        : Int32  = 0 # Next forecast
      @adpcml        : Int32  = 0 # Current value
      @prevAcc       : Int32  = 0 # Leveling value
      @nowData       : UInt8  = 0 # Current ROM data
      @cpuData       : UInt8  = 0 # Current data from register 08
      @control2      : UInt8  = 0 # Control register: SAMPLE, DA/AD, RAM TYPE (x8bit/x1bit), ROM/RAM
      @dramPortShift : UInt8  = 0 # Address bits shift right (0 for ROM and x8bit DRAMs, 3 for x1 DRAMs)
      @memRead       : UInt8  = 0 # Needed for reading/writing external memory

      ###
      ### Handlers and parameters for  the status flags support
      ###

      property statusSetHandler : StatusChangeHandler?
      property statusResetHandler : StatusChangeHandler?

      ###
      ### Note that different chips have these flags on different bits of the
      ### status register.
      ###

      property! statusChangeWhichChip : Yuno::AbstractEmulator? # this chip id
      property statusChangeEOSBit : UInt8 = 0 # 1 on End Of Sample (record/playback/cycle time of AD/DA converting
                                              # has passed)
      property statusChangeBRDYBit : UInt8 = 0 # 1 after recording 2 datas (2x4bits) or after reading/writing 1 data
      property statusChangeZeroBit : UInt8 = 0 # 1 if silence lasts for more than 290 miliseconds on ADPCM recording

      ###
      ### Neither Y8950 nor YM2608 can generate IRQ when PCMBSY bit changes, so
      ### instead of above, the statusflag gets ORed with PCM_BSY (below) (on
      ### each read of statusflag of Y8950 and YM2608).
      ###

      getter pcmBsy : UInt8 = 0 # 1 when ADPCM is playing (Y8950/YM2608 only)

      @reg : Bytes = Bytes.new(16) # ADPCM registers
      @emulationMode : UInt8 = 0

      def initialize
      end

      def adpcmRead : UInt8
        v : UInt8 = 0

        # External memory read
        if (@portState & 0xE0) == 0x20
          # Two fake reads
          if @memRead != 0
            @nowAddr = @start << 1
            @memRead -= 1
            return 0u8
          end

          if @nowAddr != (@finish << 1)
            v = @memory.get!(@nowAddr >> 1)
            @nowAddr += 2 # Two nibbles at a time

            # reset BRDY bit in status register, which means we are reading the
            # memory now.
            @statusResetHandler.try do |fn|
              if @statusChangeBRDYBit != 0
                fn.call(self.statusChangeWhichChip, @statusChangeBRDYBit)
              end
            end

            # Setup a timer that will callback us in 10 master clock cycles for
            # Y8950 in the callback set the BRDY flag to 1 , which means we have
            # another data ready.  For now, we don't really do this; we simply
            # reset and set the flag in zero time, so that the IRQ will work.

            # Set BRDY bit in status register.
            @statusSetHandler.try do |fn|
              if @statusChangeBRDYBit != 0
                fn.call(self.statusChangeWhichChip, @statusChangeBRDYBit)
              end
            end
          else
            # Set EOS bit in status register.
            @statusSetHandler.try do |fn|
              if @statusChangeEOSBit != 0
                fn.call(self.statusChangeWhichChip, @statusChangeEOSBit)
              end
            end
          end
        end

        v
      end

      def adpcmWrite(r : Int, v : Int) : Nil
        return if  r >= 0x10
        @reg.put!(r, v.to_u8!) # Stock data

        case r
        when 0x00
          # START:
          # Accessing *external* memory is started when START bit (D7) is set to "1", so
          # you must set all conditions needed for recording/playback before starting.
          # If you access *CPU-managed* memory, recording/playback starts after
          # read/write of ADPCM data register $08.

          # REC:
          #   0 = ADPCM synthesis (playback)
          #   1 = ADPCM analysis (record)
          #
          # MEMDATA:
          #   0 = processor (*CPU-managed*) memory (means: using register $08)
          #   1 = external memory (using start/end/limit registers to access memory: RAM or ROM)
          #
          # SPOFF:
          #   controls output pin that should disable the speaker while ADPCM analysis
          #
          # RESET and REPEAT only work with external memory.
          #
          # some examples:
          #   value:   START, REC, MEMDAT, REPEAT, SPOFF, x,x,RESET   meaning:
          #     C8     1      1    0       0       1      0 0 0       Analysis (recording) from AUDIO to CPU (to reg $08), sample rate in PRESCALER register
          #     E8     1      1    1       0       1      0 0 0       Analysis (recording) from AUDIO to EXT.MEMORY,       sample rate in PRESCALER register
          #     80     1      0    0       0       0      0 0 0       Synthesis (playing) from CPU (from reg $08) to AUDIO,sample rate in DELTA-N register
          #     a0     1      0    1       0       0      0 0 0       Synthesis (playing) from EXT.MEMORY to AUDIO,        sample rate in DELTA-N register
          #
          #     60     0      1    1       0       0      0 0 0       External memory write via ADPCM data register $08
          #     20     0      0    1       0       0      0 0 0       External memory read via ADPCM data register $08

          # Handle emulation mode.
          if @emulationMode == EMULATION_MODE_YM2610
            v |= 0x20 # YM2610 always uses external memory and doesn't even have memory flag bit.
          end

          # start, rec, memory mode, repeat flag copy, reset(bit0)
          @portState = (v & (0x80|0x40|0x20|0x10|0x01)).to_u8!

          if Yuno.bitflag?(@portState, 0x80) # START,REC,MEMDATA,REPEAT,SPOFF,--,--,RESET
            # Set PCM BUSY bit.
            @pcmBsy = 1

            # Start ADPCM
            @nowStep = 0
            @acc = 0
            @prevAcc = 0
            @adpcml = 0
            @adpcmd = DELTA_DEF
            @nowData = 0
            Yuno.dlog!("DeltaT: Start #{@start} > #{@finish}") if @start > @finish
          end

          if Yuno.bitflag?(@portState, 0x20) # Do we access external memory?
            @nowAddr = @start << 1
            @memRead = 2 # Two fake reads needed before accessing external memory via register 08$

            # If yes, then let's check if ADPCM memory is mapped and big
            # enough.
            if @memory.empty?
              {% if flag?(:yunosynth_debug) %}
                RemiLib.log.error("YM Delta-T ADPCM ROM not mapped")
              {% end %}
              @portState = 0
              @pcmBsy = 0
            else
              # Check end in range.
              if @finish >= @memory.size
                {% if flag?(:yunosynth_debug) %}
                  RemiLib.log.error(sprintf("YM Delta-T ADPCM end out of range: $%08x", @finish))
                {% end %}
                @finish = (@memory.size - 1).to_u32!
              end

              # Check start in range
              if @start >= @memory.size
                {% if flag?(:yunosynth_debug) %}
                  RemiLib.log.error(sprintf("YM Delta-T ADPCM start out of range: $%08x", @start))
                {% end %}
                @portState = 0
                @pcmBsy = 0
              end
            end
          else
            # We access CPU memory (ADPCM data register 08$) so we only reset
            # @nowAddr here.
            @nowAddr = 0
          end

          if Yuno.bitflag?(@portState, 0x01)
            @portState = 0

            # Clear PCM BUSY bit (in status register)
            @pcmBsy = 0

            # Set BRDY flag.
            @statusSetHandler.try do |fn|
              if @statusChangeBRDYBit != 0
                fn.call(self.statusChangeWhichChip, @statusChangeBRDYBit)
              end
            end
          end

        when 0x01 # L,R,-,-,SAMPLE,DA/AD,RAMTYPE,ROM
          # Handle emulation mode
          if @emulationMode == EMULATION_MODE_YM2610
            v |= 0x01 # YM2610 always uses ROM as an external memory and doesn't have ROM/RAM memory flag bit
          end

          @pan = @outputPointer.to_unsafe + ((v >> 6) & 0x03)
          if (@control2 & 3) != (v & 3)
            # 0-DRAM x1, 1-ROM, 2-DRAM x8, 3-ROM (3 is bad setting - not allowed by the manual).
            if @dramPortShift != DRAM_RIGHT_SHIFT.get!(v & 3)
              @dramPortShift = DRAM_RIGHT_SHIFT.get!(v & 3)

              # Final shift value depends on chip type and memory type selected:
              #   8 for YM2610 (ROM only),
              #   5 for ROM for Y8950 and YM2608,
              #   5 for x8bit DRAMs for Y8950 and YM2608,
              #   2 for x1bit DRAMs for Y8950 and YM2608.

              # Refresh addresses
              @start   = ((@reg.get!(0x3).to_u32! * 0x100) | @reg.get!(0x2)) << (@portShift - @dramPortShift)
              @finish  = ((@reg.get!(0x5).to_u32! * 0x100) | @reg.get!(0x4)) << (@portShift - @dramPortShift)
              @finish += (1u32 << (@portShift - @dramPortShift)) &- 1
              @limit   = ((@reg.get!(0xD).to_u32! * 0x100) | @reg.get!(0xC)) << (@portShift - @dramPortShift)
            end
          end

          @control2 = v.to_u8!

        when 0x02, 0x03 # Start address low and high
          @start = ((@reg.get!(0x3).to_u32! * 0x100) | @reg.get!(0x2)) << (@portShift - @dramPortShift)

        when 0x04, 0x05 # Stop address low and high
          @finish  = ((@reg.get!(0x5).to_u32! * 0x100) | @reg.get!(0x4)) << (@portShift - @dramPortShift)
          @finish += (1u32 << (@portShift - @dramPortShift)) &- 1

        #when 0x06, 0x07 # Prescale low (ADPCM and record frq) and high
        #  nil

        when 0x08 # ADPCM data
          # some examples:
          # value:   START, REC, MEMDAT, REPEAT, SPOFF, x,x,RESET   meaning:
          #  C8     1      1    0       0       1      0 0 0       Analysis (recording) from AUDIO to CPU (to reg $08), sample rate in PRESCALER register
          #  E8     1      1    1       0       1      0 0 0       Analysis (recording) from AUDIO to EXT.MEMORY,       sample rate in PRESCALER register
          #  80     1      0    0       0       0      0 0 0       Synthesis (playing) from CPU (from reg $08) to AUDIO,sample rate in DELTA-N register
          #  a0     1      0    1       0       0      0 0 0       Synthesis (playing) from EXT.MEMORY to AUDIO,        sample rate in DELTA-N register
          #  60     0      1    1       0       0      0 0 0       External memory write via ADPCM data register $08
          #  20     0      0    1       0       0      0 0 0       External memory read via ADPCM data register $08

          # External memory write
          if (@portState & 0xE0) == 0x60
            if @memRead != 0
              @nowAddr = @start << 1
              @memRead = 0
            end

            if @nowAddr != (@finish << 1)
              @memory.put!(@nowAddr >> 1, v.to_u8!)
              @nowAddr += 2 # Two nibbles at a time.

              # Reset BRDY bit in status register, which means we are
              # processing the write.
              @statusResetHandler.try do |fn|
                if @statusChangeBRDYBit != 0
                  fn.call(self.statusChangeWhichChip, @statusChangeBRDYBit)
                end
              end

              # Setup a timer that will callback us in 10 master clock cycles for
              # Y8950 in the callback set the BRDY flag to 1 , which means we have
              # another data ready.  For now, we don't really do this; we simply
              # reset and set the flag in zero time, so that the IRQ will work.

              # Set BRDY bit in status register
              @statusSetHandler.try do |fn|
                if @statusChangeBRDYBit != 0
                  fn.call(self.statusChangeWhichChip, @statusChangeBRDYBit)
                end
              end
            else
              # Set EOS bit in status register.
              @statusSetHandler.try do |fn|
                if @statusChangeEOSBit != 0
                  fn.call(self.statusChangeWhichChip, @statusChangeEOSBit)
                end
              end
            end

            return
          end

          # ADPCM synthesis from CPU
          if (@portState & 0xE0) == 0x80
            @cpuData = v.to_u8!

            # Reset BRDY bit in status register, which means we are
            # full of data.
            @statusResetHandler.try do |fn|
              if @statusChangeBRDYBit != 0
                fn.call(self.statusChangeWhichChip, @statusChangeBRDYBit)
              end
            end

            return
          end

        when 0x09, 0x0A # DELTA-N low (ADPCM playback prescaler) and high
          @delta = ((@reg.get!(0xA).to_u32! * 0x100) | @reg.get!(0x9)).to_u32!
          @step = (@delta.to_f64! * @freqBase).to_u32!

        when 0x0B # Output level control (volume, linear)
          oldVol : Int32 = @volume
          @volume = ((v.to_i32! & 0xFF) * (@outputRange / 256) / DECODE_RANGE).to_i32!
          if oldVol != 0
            @adpcml = (@adpcml.to_f64! / oldVol.to_f64! * @volume.to_f64!).to_i32!
          end

        when 0x0C, 0x0D # Limit address low and high
          @limit = ((@reg.get!(0xD).to_u32! * 0x0100) | @reg.get!(0xC).to_u32!) << (@portShift - @dramPortShift)
        end
      end

      def adpcmReset(pan : Int, emulationMode : Int) : Nil
        @nowAddr = 0
        @nowStep = 0
        @step = 0
        @start = 0
        @finish = 0
        @limit = UInt32::MAX # This way YM2610 and Y8950 (both of which don't have limit address reg) will still work.
        @volume = 0
        @pan = @outputPointer.to_unsafe + pan
        @acc = 0
        @prevAcc = 0
        @adpcmd = DELTA_DEF
        @adpcml = 0
        @emulationMode = emulationMode.to_u8!
        @portState = (emulationMode == EMULATION_MODE_YM2610 ? 0x20_u8 : 0u8)

        # Default setting depends on the emulation mode. MSX demo called
        # "facdemo_4" doesn't setup control2 register at all and still works.
        @control2 = (emulationMode == EMULATION_MODE_YM2610 ? 0x01_u8 : 0u8)
        @dramPortShift = DRAM_RIGHT_SHIFT.get!(@control2 & 3)

        # The flag mask register disables the BRDY after the reset, however as
        # soon as the mask is enabled the flag needs to be set.

        # Set BRDY bit in status register
        @statusSetHandler.try do |fn|
          if @statusChangeBRDYBit != 0
            fn.call(self.statusChangeWhichChip, @statusChangeBRDYBit)
          end
        end
      end

      @[AlwaysInline]
      def synthesisFromExternalMemory : Nil
        step : UInt32 = 0
        data : UInt8 = 0

        @nowStep += @step
        if @nowStep >= (1u32 << SHIFT)
          step = @nowStep >> SHIFT
          @nowStep &= (1u32 << SHIFT) - 1

          loop do
            @nowAddr = 0 if @nowAddr == (@limit << 1)

            if @nowAddr == (@finish << 1) # 12-06-2001 JB: corrected comparison. Was > instead of ==
              if Yuno.bitflag?(@portState, 0x10)
                # Repeat start
                @nowAddr = @start << 1
                @acc = 0
                @adpcmd = DELTA_DEF
                @prevAcc = 0
              else
                # Set EOS bit in status register.
                @statusSetHandler.try do |fn|
                  if @statusChangeEOSBit != 0
                    fn.call(self.statusChangeWhichChip, @statusChangeEOSBit)
                  end
                end

                # Clear PCM BUSY bit (reflected in status register).
                @pcmBsy = 0

                @portState = 0
                @adpcml = 0
                @prevAcc = 0
                return
              end
            end

            if Yuno.bitflag?(@nowAddr, 1)
              data = @nowData & 0x0F
            else
              @nowData = @memory.get!(@nowAddr >> 1)
              data = @nowData >> 4
            end

            @nowAddr += 1
            @nowAddr &= @memoryMask

            # Store accumulator value
            @prevAcc = @acc

            # Forecast to next Forecast
            @acc = (@acc + (DECODE_TABLEB1.get!(data) * @adpcmd / 8)).to_i32!.clamp(DECODE_MIN, DECODE_MAX)

            # Delta to next delta
            @adpcmd = ((@adpcmd * DECODE_TABLEB2.get!(data)) / 64).to_i32!.clamp(DELTA_MIN, DELTA_MAX)

            step -= 1
            break if step == 0
          end # loop do
        end # if @nowStep >= (1u32 << SHIFT)

        # ElSemi: Fix interpolator.
        @adpcml  =  @prevAcc * ((1 << SHIFT) - @nowStep).to_i32!
        @adpcml += (@acc.to_i64! * @nowStep.to_i64!).to_i32!
        @adpcml  = (@adpcml >> SHIFT) &* @volume.to_i32!

        # Output for work of output channels
        @pan[0] = @pan[0] &+ @adpcml
      end

      @[AlwaysInline]
      def synthesisFromCPUMemory : Nil
        step : UInt32 = 0
        data : UInt8 = 0

        @nowStep += @step
        if @nowStep >= (1u32 << SHIFT)
          step = @nowStep >> SHIFT
          @nowStep &= (1u32 << SHIFT) - 1

          loop do
            if Yuno.bitflag?(@nowAddr, 1)
              data = @nowData & 0x0F
              @nowData = @cpuData

              # After we use @cpuData, we set BRDY bit in status register, which
              # means we are ready to accept another byte of data.
              @statusSetHandler.try do |fn|
                if @statusChangeBRDYBit != 0
                  fn.call(self.statusChangeWhichChip, @statusChangeBRDYBit)
                end
              end
            else
              data = @nowData >> 4
            end

            @nowAddr += 1

            # Store accumulator value
            @prevAcc = @acc

            # Forecast to next Forecast
            @acc += (DECODE_TABLEB1.get!(data) * @adpcmd / 8).to_i32!
            @acc = @acc.clamp(DECODE_MIN, DECODE_MAX)

            # Delta to next delta
            @adpcmd = ((@adpcmd * DECODE_TABLEB2.get!(data)) / 64).to_i32!.clamp(DELTA_MIN, DELTA_MAX)

            step -= 1
            break if step == 0
          end
        end

        # ElSemi: Fix interpolator.
        @adpcml  =  @prevAcc * ((1u32 << SHIFT) - @nowStep).to_i32!
        @adpcml += (@acc * @nowStep.to_i32!)
        @adpcml  = (@adpcml >> SHIFT) &* @volume.to_i32!

        # Output for work of output channels
        @pan[0] += @adpcml
      end

      @[AlwaysInline]
      def adpcmCalc : Nil
        case @portState & 0xE0
        when 0xA0 then synthesisFromExternalMemory
        when 0x80 then synthesisFromCPUMemory
        end
      end

      @[AlwaysInline]
      def calcMemMask : Nil
        maskSize = 0x01_u32
        while maskSize < @memory.size
          maskSize <<= 1
        end
        @memoryMask = (maskSize << 1) - 1 # It's mask<<1 because of the nibbles.
      end
    end
  end
end
