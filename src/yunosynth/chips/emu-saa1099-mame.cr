####    Philips SAA1099 Sound driver
####
####    By Juergen Buchmueller and Manuel Abadia
####    Crystal port Copyright (C) 2024 Remilia Scarlet
####    BSD 3-Clause License
####
####    SAA1099 register layout:
####    ========================
####
####    offs | 7654 3210 | description
####    -----+-----------+---------------------------
####    0x00 | ---- xxxx | Amplitude channel 0 (left)
####    0x00 | xxxx ---- | Amplitude channel 0 (right)
####    0x01 | ---- xxxx | Amplitude channel 1 (left)
####    0x01 | xxxx ---- | Amplitude channel 1 (right)
####    0x02 | ---- xxxx | Amplitude channel 2 (left)
####    0x02 | xxxx ---- | Amplitude channel 2 (right)
####    0x03 | ---- xxxx | Amplitude channel 3 (left)
####    0x03 | xxxx ---- | Amplitude channel 3 (right)
####    0x04 | ---- xxxx | Amplitude channel 4 (left)
####    0x04 | xxxx ---- | Amplitude channel 4 (right)
####    0x05 | ---- xxxx | Amplitude channel 5 (left)
####    0x05 | xxxx ---- | Amplitude channel 5 (right)
####         |           |
####    0x08 | xxxx xxxx | Frequency channel 0
####    0x09 | xxxx xxxx | Frequency channel 1
####    0x0a | xxxx xxxx | Frequency channel 2
####    0x0b | xxxx xxxx | Frequency channel 3
####    0x0c | xxxx xxxx | Frequency channel 4
####    0x0d | xxxx xxxx | Frequency channel 5
####         |           |
####    0x10 | ---- -xxx | Channel 0 octave select
####    0x10 | -xxx ---- | Channel 1 octave select
####    0x11 | ---- -xxx | Channel 2 octave select
####    0x11 | -xxx ---- | Channel 3 octave select
####    0x12 | ---- -xxx | Channel 4 octave select
####    0x12 | -xxx ---- | Channel 5 octave select
####         |           |
####    0x14 | ---- ---x | Channel 0 frequency enable (0 = off, 1 = on)
####    0x14 | ---- --x- | Channel 1 frequency enable (0 = off, 1 = on)
####    0x14 | ---- -x-- | Channel 2 frequency enable (0 = off, 1 = on)
####    0x14 | ---- x--- | Channel 3 frequency enable (0 = off, 1 = on)
####    0x14 | ---x ---- | Channel 4 frequency enable (0 = off, 1 = on)
####    0x14 | --x- ---- | Channel 5 frequency enable (0 = off, 1 = on)
####         |           |
####    0x15 | ---- ---x | Channel 0 noise enable (0 = off, 1 = on)
####    0x15 | ---- --x- | Channel 1 noise enable (0 = off, 1 = on)
####    0x15 | ---- -x-- | Channel 2 noise enable (0 = off, 1 = on)
####    0x15 | ---- x--- | Channel 3 noise enable (0 = off, 1 = on)
####    0x15 | ---x ---- | Channel 4 noise enable (0 = off, 1 = on)
####    0x15 | --x- ---- | Channel 5 noise enable (0 = off, 1 = on)
####         |           |
####    0x16 | ---- --xx | Noise generator parameters 0
####    0x16 | --xx ---- | Noise generator parameters 1
####         |           |
####    0x18 | --xx xxxx | Envelope generator 0 parameters
####    0x18 | x--- ---- | Envelope generator 0 control enable (0 = off, 1 = on)
####    0x19 | --xx xxxx | Envelope generator 1 parameters
####    0x19 | x--- ---- | Envelope generator 1 control enable (0 = off, 1 = on)
####         |           |
####    0x1c | ---- ---x | All channels enable (0 = off, 1 = on)
####    0x1c | ---- --x- | Synch & Reset generators

module Yuno::Chips
  class Saa1099 < Yuno::AbstractChip
    # Actual implementation of the Saa1099 emulator based on Mame's implementation
    private class Saa1099Mame < Yuno::AbstractEmulator
      AMPLITUDE_LOOKUP = [
        (0 * 32767).tdiv(16),  (1 * 32767).tdiv(16),  (2 * 32767).tdiv(16),  (3 * 32767).tdiv(16),
        (4 * 32767).tdiv(16),  (5 * 32767).tdiv(16),  (6 * 32767).tdiv(16),  (7 * 32767).tdiv(16),
        (8 * 32767).tdiv(16),  (9 * 32767).tdiv(16),  (10 * 32767).tdiv(16), (11 * 32767).tdiv(16),
        (12 * 32767).tdiv(16), (13 * 32767).tdiv(16), (14 * 32767).tdiv(16), (15 * 32767).tdiv(16)
      ]

      ENVELOPE = [
        # Zero amplitude
        [
          0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
          0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
          0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
          0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
        ] of UInt8,

        # Maximum amplitude
        [
          15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15,
          15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15,
          15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15,
          15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15,
        ] of UInt8,

        # Single decay
        [
          15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0,
          0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
          0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
          0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
        ] of UInt8,

        # Repetitive decay
        [
          15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0,
          15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0,
          15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0,
          15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0
        ] of UInt8,

        # Single triangular
        [
          0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15,
          15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0,
          0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
          0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
        ] of UInt8,

        # Repetitive triangular
        [
          0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15,
          15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0,
          0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15,
          15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0
        ] of UInt8,

        # Single attack
        [
          0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15,
          0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
          0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
          0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
        ] of UInt8,

        # Repetitive attack
        [
          0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15,
          0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15,
          0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15,
          0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15
        ] of UInt8
      ]

      # Defines a sound channel.
      class Channel
        property frequency     : Int32 = 0     # Frequency (0x00..0xFF)
        property? freqEnabled  : Bool  = false # Frequency enabled?
        property? noiseEnabled : Bool  = false # Noise enabled?
        property octave        : Int32 = 0     # Octave (0x00..0x07)
        property amplitudeL    : Int32 = 0     # Amplitude left (0x00..0x0F)
        property amplitudeR    : Int32 = 0     # Amplitude right (0x00..0x0F)
        property envelopeL     : Int32 = 0     # Envelope left (0x00..0x0F, or 0x10 == off)
        property envelopeR     : Int32 = 0     # Envelope right (0x00..0x0F, or 0x10 == off)

        # Variables to similate the square wave
        property counter : Float64 = 0.0
        property freq    : Float64 = 0.0
        property level   : Int32 = 0
        property? muted  : Bool = false

        def initialize
        end

        def reset : Nil
          @frequency = 0u8
          @octave = 0u8
          @amplitudeL = 0
          @amplitudeR = 0
          @envelopeL = 0
          @envelopeR = 0
          @freqEnabled = false
          @noiseEnabled = false
          @counter = 0.0
          @freq = 0.0
          @level = 0
        end
      end

      # Defines a noise channel.
      class Noise
        # Variables to simulate the noise generator output
        property counter : Float64 = 0.0
        property freq : Float64 = 0.0
        property level : Int32 = 0 # Noise polynomal shifter

        def initialize
        end

        def reset : Nil
          @counter = 0.0
          @freq = 0.0
          @level = 0
        end
      end

      @noiseParams     : Array(UInt8) = [0u8, 0u8]    # Noise generator parameters
      @envEnable       : Array(Bool) = [false, false] # Envelope generators enabled?
      @envReverseRight : Array(Bool) = [false, false] # Envelope reversed for right channel?
      @envMode         : Array(Int32) = [0, 0]        # Envelope generator mode
      @envBits         : Array(Bool) = [false, false] # 3 bit resolution?
      @envClock        : Array(Bool) = [false, false] # Envelope clock mode (true == external)
      @envStep         : Array(Int32) = [0, 0]        # Current envelope step
      @allChEnable     : Bool = false                 # All channels enabled?
      @syncState       : Bool = false                 # Sync all channels?
      @selectedReg     : Int32 = 0                    # Selected register
      @channels        : Array(Channel)               # Audio channels
      @noise           : Array(Noise)                 # Noise generators
      @sampleRate      : Float64 = 0.0
      @mainClock       : UInt32 = 0u32
      @clk2Div512      : UInt32

      def initialize(@mainClock : UInt32)
        @sampleRate = @mainClock / 128.0 * 8

        # clock fix thanks to http://www.vogons.org/viewtopic.php?p=344227#p344227
        @clk2Div512 = (@mainClock + 128).tdiv(256)

        @channels = Array(Channel).new(6) { |_| Channel.new }
        @noise = Array(Noise).new(2) { |_| Noise.new }
      end

      def rate : UInt32
        (@sampleRate + 0.5).to_u32!
      end

      def reset : Nil
        @channels.each &.reset
        @noise.each &.reset
        @noiseParams.fill(0)
        @envReverseRight.fill(false)
        @envMode.fill(0)
        @envBits.fill(false)
        @envClock.fill(false)
        @envEnable.fill(false)
        @envStep.fill(0)
        @allChEnable = false
        @syncState = false
      end

      @[AlwaysInline]
      def controlWrite(data : UInt8) : Nil
        {% if flag?(:yunosynth_debug) %}
          if data > 0x1C
            RemiLib.log.warn("SAA1099: Unknown register selected")
          end
        {% end %}

        @selectedReg = (data & 0x1F).to_i32!
        if @selectedReg == 0x18 || @selectedReg == 0x19
          # Clock the envelope channels
          envelope(0) if @envClock.get!(0)
          envelope(1) if @envClock.get!(1)
        end
      end

      @[AlwaysInline]
      def dataWrite(data : UInt8) : Nil
        reg : Int32 = @selectedReg
        ch : Int32 = 0

        case reg
        when 0x00, 0x01, 0x02, 0x03, 0x04, 0x05 # Channel amplitude
          ch = reg & 7
          @channels.get!(ch).amplitudeL = AMPLITUDE_LOOKUP.get!(data & 0x0F)
          @channels.get!(ch).amplitudeR = AMPLITUDE_LOOKUP.get!((data >> 4) & 0x0F)

        when 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D # Channel frequency
          ch = reg & 7
          @channels.get!(ch).frequency = data.to_i32!

        when 0x10, 0x11, 0x12 # Channel octave
          ch = (reg - 0x10) << 1
          @channels.get!(ch).octave = (data & 0x07).to_i32!
          @channels.get!(ch + 1).octave = ((data >> 4) & 0x07).to_i32!

        when 0x14 # Channel frequency enable
          @channels.get!(0).freqEnabled = Yuno.bitflag?(data, 0x01)
          @channels.get!(1).freqEnabled = Yuno.bitflag?(data, 0x02)
          @channels.get!(2).freqEnabled = Yuno.bitflag?(data, 0x04)
          @channels.get!(3).freqEnabled = Yuno.bitflag?(data, 0x08)
          @channels.get!(4).freqEnabled = Yuno.bitflag?(data, 0x10)
          @channels.get!(5).freqEnabled = Yuno.bitflag?(data, 0x20)

        when 0x15 # Channel noise enable
          @channels.get!(0).noiseEnabled = Yuno.bitflag?(data, 0x01)
          @channels.get!(1).noiseEnabled = Yuno.bitflag?(data, 0x02)
          @channels.get!(2).noiseEnabled = Yuno.bitflag?(data, 0x04)
          @channels.get!(3).noiseEnabled = Yuno.bitflag?(data, 0x08)
          @channels.get!(4).noiseEnabled = Yuno.bitflag?(data, 0x10)
          @channels.get!(5).noiseEnabled = Yuno.bitflag?(data, 0x20)

        when 0x16 # Noise generator parameters
          @noiseParams.put!(0, data & 0x03)
          @noiseParams.put!(1, (data >> 4) & 0x03)

        when 0x18, 0x19 # Envelope generator parameter
          ch = reg - 0x18
          @envReverseRight.put!(ch, Yuno.bitflag?(data, 0x01))
          @envMode.put!(ch, ((data >> 1) & 0x07).to_i32!)
          @envBits.put!(ch, Yuno.bitflag?(data, 0x10))
          @envClock.put!(ch, Yuno.bitflag?(data, 0x20))
          @envEnable.put!(ch, Yuno.bitflag?(data, 0x80))
          @envStep.put!(ch, 0) # Reset the envelope

        when 0x1C # Channels enable and reset generators
          @allChEnable = Yuno.bitflag?(data, 0x01)
          @syncState = Yuno.bitflag?(data, 0x02)
          if Yuno.bitflag?(data, 0x02)
            # Sync and reset generators
            RemiLib.log.warn("SAA1099: Register 0x1C Chip reset")
            6.times do |i|
              @channels.get!(i).level = 0
              @channels.get!(i).counter = 0.0
            end
          end

        else
          {% if flag?(:yunosynth_debug) %}
            RemiLib.log.warn(sprintf("SAA1099: Unknown operation (reg: $%02x, data: $%02x)", reg, data))
          {% end %}
        end
      end

      def muteMask=(mask : UInt32) : Nil
        6.times do |i|
          @channels[i].muted = Yuno.bitflag?(mask >> i, 1)
        end
      end

      private def envelope(ch : Int) : Nil
        if @envEnable[ch]
          mode : Int32 = @envMode.get!(ch)
          step : Int32 = ((@envStep.get!(ch) + 1) & 0x3F) | (@envStep.get!(ch) & 0x20)
          @envStep.put!(ch, step)

          mask : Int32 = 15
          mask &= ~1 if @envBits.get!(ch) # 3-bit resolution, mask LSB

          val : UInt8 = ENVELOPE.get!(mode).get!(step) & mask
          @channels.get!(ch * 3    ).envelopeL = val.to_i32!
          @channels.get!(ch * 3 + 1).envelopeL = val.to_i32!
          @channels.get!(ch * 3 + 2).envelopeL = val.to_i32!

          if @envReverseRight.get!(ch)
            val = ((15 - ENVELOPE.get!(mode).get!(step)) & mask).to_u8!
          end
          @channels.get!(ch * 3    ).envelopeR = val.to_i32!
          @channels.get!(ch * 3 + 1).envelopeR = val.to_i32!
          @channels.get!(ch * 3 + 2).envelopeR = val.to_i32!
        else
          @channels.get!(ch * 3    ).envelopeL = 16u8
          @channels.get!(ch * 3 + 1).envelopeL = 16u8
          @channels.get!(ch * 3 + 2).envelopeL = 16u8
          @channels.get!(ch * 3    ).envelopeR = 16u8
          @channels.get!(ch * 3 + 1).envelopeR = 16u8
          @channels.get!(ch * 3 + 2).envelopeR = 16u8
        end
      end

      @[AlwaysInline]
      def update(outputs : OutputBuffers, samples : UInt32) : Nil
        outL : Slice(Int32) = outputs[0]
        outR : Slice(Int32) = outputs[1]

        # Check once, then we can avoid bounds checks below.
        if outL.size < samples || outR.size < samples
          raise "SAA1099 update: Bad number of samples (expected #{samples}, " \
                "but buffers have #{outL.size} and #{outR.size})"
        end

        # If the channels are disabled, we're done
        unless @allChEnable
          # Init output data
          outL.fill(0)
          outR.fill(0)
          return
        end

        2.times do |ch|
          case @noiseParams.get!(ch)
          when 0 then @noise.get!(ch).freq = @mainClock / 256.0 * 2
          when 1 then @noise.get!(ch).freq = @mainClock / 512.0 * 2
          when 2 then @noise.get!(ch).freq = @mainClock / 1024.0 * 2
          when 3 then @noise.get!(ch).freq = @channels.get!(ch * 3).freq
          end
        end

        # Generate samples
        outputL : Int32 = 0
        outputR : Int32 = 0
        samples.times do |j|
          outputL = 0
          outputR = 0

          @channels.each_with_index do |ch, idx|
            if ch.freq == 0
              ch.freq = (@clk2Div512 << ch.octave) / (511.0 - ch.frequency)
            end

            # Check the actual position in the square wave
            ch.counter -= ch.freq
            while ch.counter < 0
              # Calculate new frequency now after the half wave is updated
              ch.freq = (@clk2Div512 << ch.octave) / (511.0 - ch.frequency)
              ch.counter += @sampleRate
              ch.level ^= 1

              # Eventually clock the envelope counters
              envelope(0) if idx == 1 && !@envClock.get!(0)
              envelope(1) if idx == 4 && !@envClock.get!(1)
            end

            next if ch.muted? # Placed here to ensure that envelopes are updated

            # Now with bipolar output. -Valley Bell
            if ch.noiseEnabled?
              if Yuno.bitflag?(@noise.get!(idx.tdiv(3)).level, 1)
                outputL += (ch.amplitudeL * ch.envelopeL).tdiv(32)
                outputR += (ch.amplitudeR * ch.envelopeR).tdiv(32)
              else
                outputL -= (ch.amplitudeL * ch.envelopeL).tdiv(32)
                outputR -= (ch.amplitudeR * ch.envelopeR).tdiv(32)
              end
            end

            if ch.freqEnabled?
              if Yuno.bitflag?(ch.level, 1)
                outputL += (ch.amplitudeL * ch.envelopeL).tdiv(32)
                outputR += (ch.amplitudeR * ch.envelopeR).tdiv(32)
              else
                outputL -= (ch.amplitudeL * ch.envelopeL).tdiv(32)
                outputR -= (ch.amplitudeR * ch.envelopeR).tdiv(32)
              end
            end
          end

          @noise.each do |ch|
            # Check the actual position in the noise generator
            ch.counter -= ch.freq
            while ch.counter < 0
              ch.counter += @sampleRate
              if Yuno.bitflag?(ch.level, 0x4000) == Yuno.bitflag?(ch.level, 0x0040)
                ch.level = (ch.level << 1) | 1
              else
                ch.level <<= 1
              end
            end
          end

          # Write sound data to the buffer
          outL.put!(j, outputL.tdiv(6))
          outR.put!(j, outputR.tdiv(6))
        end
      end
    end
  end
end
