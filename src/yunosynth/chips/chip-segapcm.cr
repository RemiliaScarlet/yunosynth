#### YunoSynth
#### Copyright (C) 2023-2024 Remilia Scarlet <remilia@posteo.jp>
#### Portions based on VGMPlay, Copyright (C) Valley Bell
####
#### This program is free software: you can redistribute it and/or
#### modify it under the terms of the GNU Affero General Public
#### License as published by the Free Software Foundation, either
#### version 3 of the License, or (at your option) any later version.
####
#### This program is distributed in the hope that it will be useful,
#### but WITHOUT ANY WARRANTY; without even the implied warranty of
#### MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#### Affero General Public License for more details.
####
#### You should have received a copy of the GNU Affero General Public License
#### along with this program.  If not, see <https://www.gnu.org/licenses/>.
require "./emu-multipcm-mame"

####
#### Sega SegaPCM sound chip emulator interface
####

module Yuno::Chips
  # Sega SegaPCM sound chip emulator.
  class SegaPCM < Yuno::AbstractChip
    CHIP_ID = 0x04_u32

    @clockFromHeader : UInt32 = 0
    @samplingMode : UInt8 = 0
    @chipSampleRate : UInt32 = 0
    @chip : SegaPCMMame? = nil
    @interfaceReg : UInt32 = 0

    def initialize(chipNum : Int32, absChipCount : Int32, vgm : VgmFile, playbackSampleRate : UInt32,
                   newSamplingMode : UInt8, newPlayerSampleRate : UInt32, *, emuCore : Symbol? = nil,
                   flags : ChipFlags? = nil)
      @clockFromHeader = vgm.getAlternateChipClock(chipNum, Yuno::ChipType::SegaPcm) || vgm.header.segaPcmClock
      @clockFromHeader &= 0x3FFFFFFF
      @playerSampleRate = newPlayerSampleRate
      @interfaceReg = vgm.header.segaPcmInterfaceReg
      chipCount = (vgm.header.segaPcmClock & 0x40000000) != 0 ? 2 : 1
      initFields(vgm, emuCore, playbackSampleRate, newSamplingMode, chipCount)
    end

    protected def initialize
    end

    protected def initFields(vgm : VgmFile, emuCore : Symbol?, playbackSampleRate : UInt32, newSamplingMode : UInt8,
                             chipCount : Int32) : Nil
      @volume = vgm.getChipVolume(self, ChipType::SegaPcm, chipCount)
      @core = emuCore || SegaPCM.defaultEmuCore
      case @core
      when :mame
        @samplingMode = newSamplingMode
        @chipSampleRate = @clockFromHeader <= 0 ? playbackSampleRate : @clockFromHeader
      else raise YunoError.new("Unsupported emulation core for SegaPCM: #{@core}")
      end
    end

    def type : ChipType
      ChipType::SegaPcm
    end

    def name : String
      "Sega SegaPCM"
    end

    def shortName : String
      "SegaPCM"
    end

    def id : UInt32
      CHIP_ID
    end

    def emuCore : Symbol
      @core
    end

    def self.defaultEmuCore : Symbol
      :mame
    end

    def start(chipIndex : UInt8, clock : UInt32, flags : ChipFlags? = nil, previousChip : AbstractChip? = nil) : UInt32
      case @core
      when :mame
        @chip = SegaPCMMame.new(clock, @interfaceReg)
        @sampleRate = @chip.not_nil!.rate # Update sample rate
        @sampleRate
      else raise YunoError.new("Unsupported emulation core for SegaPCM: #{@core}")
      end
    end

    def getClock : UInt32
      @clockFromHeader
    end

    def getStartFlags(vgm : VgmFile) : ChipFlags?
      nil
    end

    def update(outputs : OutputBuffers, samples : Int) : Nil
      @chip.not_nil!.update(outputs, samples.to_u32!)
    end

    def reset(chipIndex : UInt8) : Nil
      @chip.not_nil!.reset
    end

    def read(chipIndex : UInt8, offset : Int) : UInt8|UInt16|UInt32
      @chip.not_nil!.read(offset)
    end

    def write(chipIndex : UInt8, offset : Int, data : Int, port : UInt8 = 0) : Nil
      @chip.not_nil!.write(offset.to_i32!, data.to_u8!)
    end

    def writeDac(port : Int, command : Int, data : Int) : Nil
      Yuno.warn("SegaPCM does not yet support DAC write commands")
    end

    def setMuteMask(chipIndex : UInt8, mask : UInt32) : Nil
      @chip.not_nil!.muteMask = mask
    end

    def getVolModifier : UInt32
      @volume.to_u32
    end

    def baseVolume : UInt16
      0x180_u16
    end

    @[AlwaysInline]
    def writeRom(romSize : Int32, dataStart : Int32, dataLength : Int32, romData : Slice(UInt8)) : Nil
      @chip.not_nil!.writeRom(romSize, dataStart, dataLength, romData)
    end

    # The internal interface for the chip.
    @[AlwaysInline]
    def chip : AbstractEmulator
      @chip.not_nil!
    end
  end
end
