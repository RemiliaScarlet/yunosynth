#### Copyright (c) 1999-2002 by Stéphane Dallongeville
#### Copyright (c) 2003-2004 by Stéphane Akhoun
#### Copyright (c) 2008-2009 by David Korth
#### Copyright (C) 2024 Remilia Scarlet <remilia@posteo.jp>
####
#### This program is free software; you can redistribute it and/or modify it
#### under the terms of the GNU General Public License as published by the Free
#### Software Foundation; either version 2 of the License, or (at your option)
#### any later version.
####
#### This program is distributed in the hope that it will be useful, but WITHOUT
#### ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
#### more details.
####
#### You should have received a copy of the GNU General Public License along
#### with this program; if not, write to the Free Software Foundation, Inc., 51
#### Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

module Yuno::Chips
  class Pwm < Yuno::AbstractChip
    # Actual implementation of the Pwm emulator based on Gens's implementation
    private class PwmGens < Yuno::AbstractEmulator
      # Remi: The original default was to use the PWM_BUF_SIZE equal to 4.  I've
      # changed this to a default of 8.
      {% if flag?(:yunosynth_pwm_bufsize_4) %}
        PWM_BUF_SIZE = 4
      {% else %}
        PWM_BUF_SIZE = 8
      {% end %}

      @cycle : UInt32 = 0u32
      @mode : UInt32 = 0u32
      @outputR : UInt32 = 0u32
      @outputL : UInt32 = 0u32
      @offset : Int32 = 0
      @scale : Int32 = 0
      @clock : UInt32

      def initialize(@clock : UInt32)
        reset
      end

      @[AlwaysInline]
      def update(outputs : OutputBuffers, samples : UInt32) : Nil
        outL : Slice(Int32) = outputs[0]
        outR : Slice(Int32) = outputs[1]

        # Check once, then we can avoid bounds checks below.
        if outL.size < samples || outR.size < samples
          raise "PWM update: Bad number of samples (expected #{samples}, " \
                "but buffers have #{outL.size} and #{outR.size})"
        end

        if @outputL == 0 && @outputR == 0
          outL.fill(0)
          outR.fill(0)
        else
          tempOutL = updateScale(@outputL.to_i32!)
          tempOutR = updateScale(@outputR.to_i32!)
          samples.times do |i|
            outL.put!(i, tempOutL)
            outR.put!(i, tempOutR)
          end
        end
      end

      def reset : Nil
        @mode = 0
        @outR = 0
        @outL = 0
        setCycle(0)
      end

      @[AlwaysInline]
      def write(channel : UInt8, data : UInt16) : Nil
        if @clock == 1
          # Old-style commands
          case channel
          when 0x00 then @outputL = data.to_u32!
          when 0x01 then @outputR = data.to_u32!
          when 0x02 then setCycle(data.to_u32!)
          when 0x03
            @outputL = data.to_u32!
            @outputR = data.to_u32!
          end
        else
          case channel
          when 0x01 # Cycle register
            setCycle(data.to_u32!)
          when 0x02 # Left channel
            @outputL = data.to_u32!
          when 0x03 # Right channel
            @outputR = data.to_u32!
            if @mode == 0
              if @outputL == @outputR
                # Fixes these terrible pops when starting/stopping/pausing the
                # song.
                @offset = data.to_i32!
                @mode = 1
              end
            end
          when 0x04 # Mono channel
            @outputL = data.to_u32!
            @outputR = data.to_u32!
            if @mode == 0
              @offset = data.to_i32!
              @mode = 1
            end
          end
        end
      end

      @[AlwaysInline]
      private def setCycle(data : UInt32) : Nil
        data = data &- 1
        @cycle = data & 0xFFF
        @offset = @cycle.tdiv(2).to_i32! &+ 1
        @scale = 0x7FFF00.tdiv(@offset)
      end

      @[AlwaysInline]
      private def updateScale(pwmIn : Int) : Int
        return 0 if pwmIn == 0

        # Knuckles' Chaotix: Tachy Touch uses the values 0xF?? for negative
        # values.  This small modification fixes the terrible pops.
        pwmIn &= 0xFFF
        pwmIn |= ~0xFFF if Yuno.bitflag?(pwmIn, 0x800)
        ((pwmIn &- @offset) &* @scale) >> 8
      end
    end
  end
end
