#### YunoSynth
#### Copyright (C) 2023-2024 Remilia Scarlet <remilia@posteo.jp>
#### Portions based on VGMPlay, Copyright (C) Valley Bell
####
#### This program is free software: you can redistribute it and/or
#### modify it under the terms of the GNU Affero General Public
#### License as published by the Free Software Foundation, either
#### version 3 of the License, or (at your option) any later version.
####
#### This program is distributed in the hope that it will be useful,
#### but WITHOUT ANY WARRANTY; without even the implied warranty of
#### MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#### Affero General Public License for more details.
####
#### You should have received a copy of the GNU Affero General Public License
#### along with this program.  If not, see <https://www.gnu.org/licenses/>.
require "./emu-opl-dosbox/*"

####
#### Yamaha YM3812 sound chip emulator interface
####

module Yuno::Chips
  # YM3812 sound chip emulator.
  class YM3812 < Yuno::AbstractChip
    CHIP_ID = 0x09_u32

    enum Core
      Dosbox

      @[AlwaysInline]
      def symbol : Symbol
        case self
        in .dosbox? then :dosbox
        end
      end
    end

    @clockFromHeader : UInt32 = 0
    @chip : OPL2Dosbox|Nil = nil
    @samplingMode : UInt8 = 0
    @chipSampleRate : UInt32 = 0

    def initialize(chipNum : Int32, absChipCount : Int32, vgm : VgmFile, playbackSampleRate : UInt32,
                   newSamplingMode : UInt8, newPlayerSampleRate : UInt32, *, emuCore : Core|Symbol|Nil = nil,
                   flags : ChipFlags? = nil)
      @clockFromHeader = vgm.getAlternateChipClock(chipNum, Yuno::ChipType::Ym3812) || vgm.header.ym3812Clock
      @clockFromHeader &= 0xBFFFFFFF
      @playerSampleRate = newPlayerSampleRate
      chipCount = (vgm.header.ym3812Clock & 0x40000000) != 0 ? 2 : 1
      initFields(vgm, emuCore, playbackSampleRate, newSamplingMode, chipCount)
    end

    protected def initialize
    end

    protected def initFields(vgm : VgmFile, emuCore : Core|Symbol|Nil, playbackSampleRate : UInt32,
                             newSamplingMode : UInt8, chipCount : Int32) : Nil
      @volume = vgm.getChipVolume(self, ChipType::Ym3812, chipCount)
      @samplingMode = newSamplingMode
      @core = case emuCore
              when Core then emuCore.symbol
              when Symbol then emuCore
              else YM3812.defaultEmuCore
              end
      case @core
      when :dosbox
        @chipSampleRate = @clockFromHeader <= 0 ? playbackSampleRate : @clockFromHeader
      else raise YunoError.new("Unsupported emulation core for YM3812: #{@core}")
      end
    end

    def type : ChipType
      ChipType::Ym3812
    end

    def name : String
      "Yamaha YM3812"
    end

    def shortName : String
      "YM3812"
    end

    def id : UInt32
      CHIP_ID
    end

    def emuCore : Symbol
      @core
    end

    def self.defaultEmuCore : Symbol
      :dosbox
    end

    def start(chipIndex : UInt8, clock : UInt32, flags : ChipFlags? = nil, previousChip : AbstractChip? = nil) : UInt32
      # Update sample rate
      @sampleRate = (clock & 0x7FFFFFFF).tdiv(72)
      if (@samplingMode == 0x01 && @sampleRate < @chipSampleRate) || @samplingMode == 0x02
        @sampleRate = @chipSampleRate
      end

      case @core
      when :dosbox
        @chip = OPL2Dosbox.new(clock & 0x7FFFFFFF, @sampleRate, ->self.streamUpdate(AbstractChip), self)
      else raise YunoError.new("Unsupported emulation core for YM3812: #{@core}")
      end

      @sampleRate
    end

    def getClock : UInt32
      @clockFromHeader
    end

    def getStartFlags(vgm : VgmFile) : ChipFlags?
      nil
    end

    def update(outputs : OutputBuffers, samples : Int) : Nil
      ch = @chip.not_nil!
      ch.update(outputs, samples.to_u32!)
    end

    protected def streamUpdate(param : AbstractChip) : Nil
      ch = @chip.not_nil!
      ch.update(Yuno::FAKE_BUF, 0)
    end

    def reset(chipIndex : UInt8) : Nil
      @chip.not_nil!.reset
    end

    def read(chipIndex : UInt8, offset : Int) : UInt8|UInt16|UInt32
      ch = @chip.not_nil!
      ch.regRead((offset & 0x01).to_u32!)
    end

    def write(chipIndex : UInt8, offset : Int, data : Int, port : UInt8 = 0) : Nil
      ch = @chip.not_nil!
      ch.writeIO((offset & 1).to_u32!, data.to_u8!)
    end

    @[AlwaysInline]
    def statusPortRead(offset : Int) : UInt8|UInt16|UInt32
      read(0)
    end

    @[AlwaysInline]
    def readPortRead(offset : Int) : UInt8|UInt16|UInt32
      read(1)
    end

    @[AlwaysInline]
    def controlPortWrite(offset : Int, data : Int) : UInt8|UInt16|UInt32
      write(0, data)
    end

    @[AlwaysInline]
    def writePortWrite(offset : Int, data : Int) : UInt8|UInt16|UInt32
      write(1, data)
    end

    def writeDac(port : Int, command : Int, data : Int) : Nil
      write(0, 0x00, command)
      write(0, 0x01, data)
    end

    def setMuteMask(chipIndex : UInt8, mask : UInt32) : Nil
      @chip.not_nil!.muteMask = mask
    end

    def getVolModifier : UInt32
      @volume.to_u32 * 2
    end

    def baseVolume : UInt16
      0x100_u16
    end

    # The internal interface for the chip.
    @[AlwaysInline]
    def chip : AbstractEmulator
      @chip.not_nil!
    end
  end
end
