#### Konami 054539 (TOP) PCM Sound Chip
#### Copyright (C) Olivier Galibert
#### Copyright (C) 2023-2024 Remilia Scarlet
#### BSD 3-Clause license
####
#### A lot of information comes from Amuse.
#### Big thanks to them.
require "math"

module Yuno::Chips
  class K054539 < Yuno::AbstractChip
    # Actual implementation of the K054539 emulator based on Mame's
    # implementation
    private class K054539Mame < Yuno::AbstractEmulator
      RESET_FLAGS = 0
      REVERSE_STEREO = 1
      DISABLE_REVERB = 2
      UPDATE_AT_KEY_ON = 4

      DPCM = [
          0i16 << 8,   1i16 << 8,   4i16 << 8,   9i16 << 8,  16i16 << 8, 25i16 << 8, 36i16 << 8, 49i16 << 8,
        -64i16 << 8, -49i16 << 8, -36i16 << 8, -25i16 << 8, -16i16 << 8, -9i16 << 8, -4i16 << 8, -1i16 << 8
      ]

      VOL_CAP = 1.8

      private class Channel
        property pos  : UInt32 = 0
        property pfrac : UInt32 = 0
        property val   : Int32 = 0
        property pval  : Int32 = 0

        def initialize
        end
      end

      @volTable : Array(Float64) = Array(Float64).new(256, 0.0)
      @panTable : Array(Float64) = Array(Float64).new(0xF, 0.0)

      @gain : Array(Float64) = Array(Float64).new(8, 0.0)
      @posRegLatch : Array(Array(UInt8))
      @flags : Int32 = 0

      @regs : Array(UInt8) = Array(UInt8).new(0x230, 0u8)
      @ram : Array(UInt8) = Array(UInt8).new(0x4000, 0u8)
      @reverbPos : Int32 = 0

      @curPtr : Int32 = 0
      @curLimit : Int32 = 0
      @curZone : Bytes = Bytes.new(0)
      @rom : Bytes = Bytes.new(0)
      @romSize : UInt32 = 0
      @romMask : UInt32 = 0

      @channels : Array(Channel)
      @muted : Array(UInt8) = Array(UInt8).new(8, 0u8)

      @clock : UInt32 = 0

      def initialize
        @posRegLatch = Array(Array(UInt8)).new(8) { |_| Array(UInt8).new(3, 0u8) }
        @channels = Array(Channel).new(8) { |_| Channel.new }
      end

      def start(clock : UInt32) : UInt32
        8.times do |i|
          @gain[i] = 1.0
        end

        # Factor the 1/4 for the number of channels in the volume (1/8 is too
        # harsh, 1/2 gives clipping) vol=0 -> no attenuation, vol=0x40 -> -36dB
        256.times do |i|
          @volTable[i] = (10.0 ** ((-36.0 * i / 0x40) / 20.0)) / 4.0
        end

        # Pan table for the left channel
        # Right channel is identical with inverted index
        # Formula is such that pan[i]**2+pan[0xe-i]**2 = 1 (constant output power)
        # and pan[0xe] = 1 (full panning)
        0xF.times do |i|
          @panTable[i] = Math.sqrt(i.to_f64!) / Math.sqrt(0xE)
        end

        8.times do |i|
          @muted[i] = 0
        end

        clock *= 384 if clock < 1000000 # if < 1 MHz, then it's the sample rate, not the clock.
        @clock = clock
        @flags |= UPDATE_AT_KEY_ON

        @clock.tdiv(384)
      end

      def flags=(newFlags : Int) : Nil
        @flags = newFlags.to_i32!
      end

      @[AlwaysInline]
      def setGain(channel : Int, gain : Float64) : Nil
        @gain[channel] = gain if gain >= 0
      end

      @[AlwaysInline]
      def regUpdate : Bool
        !Yuno.bitflag?(@regs[0x22F], 0x80)
      end

      @[AlwaysInline]
      def keyOn(channel : Int) : Nil
        if regUpdate
          @regs[0x22C] |= (1u8 << channel)
        end
      end

      @[AlwaysInline]
      def keyOff(channel : Int) : Nil
        if regUpdate
          @regs[0x22C] &= ~(1u8 << channel)
        end
      end

      def reset : Nil
        @regs.fill(0u8)
        @posRegLatch.each &.fill(0u8)
        @reverbPos = 0
        @curPtr = 0
        @ram.fill(0u8)
      end

      def writeRom(romSize : Int32, dataStart : Int32, dataLength : Int32, romData : Slice(UInt8)) : Nil
        if @rom.size != romSize
          @rom = Bytes.new(romSize, 0xFF_u8)
          @romSize = romSize.to_u32!

          @romMask = 0xFFFFFFFF
          32.times do |i|
            if (1u32 << i) >= @romSize
              @romMask = (1u32 << i) - 1
              break
            end
          end
        end

        return if dataStart > romSize
        dataLength = romSize - dataStart if dataStart + dataLength > romSize

        # Copy the memory
        dataLength.times do |i|
          # This can be guaranteed by the checks above
          @rom.unsafe_put(i + dataStart, romData.unsafe_fetch(i))
        end
      end

      def muteMask=(mask : UInt32) : Nil
        8.times do |i|
          @muted[i] = ((mask >> i) & 0x01).to_u8!
        end
      end

      def unmuteAll : Nil
        @muted.fill(0)
      end

      def write(offset : Int32, data : UInt8) : Nil
        offs : Int32 = 0
        #pan : Int32 = 0
        latch : Bool = Yuno.bitflag?(@flags, UPDATE_AT_KEY_ON) && Yuno.bitflag?(@regs[0x22F], 1)

        if latch && offset < 0x100
          offs = (offset & 0x1F) - 0xC
          ch : Int32 = offset >> 5

          if offs >= 0 && offs <= 2
            # Latch writes to the position index registers
            @posRegLatch[ch][offs] = data
            return
          end
        else
          case offset
          when 0x13F
            #   pan = if data >= 0x11 && data <= 0x1F
            #           data - 0x11
            #         else
            #           0x18 - 0x11
            #         end
            nil

          when 0x214
            if latch
              8.times do |chanNum|
                if Yuno.bitflag?(data, 1 << chanNum)
                  posPtr = @posRegLatch[chanNum].to_unsafe
                  regPtr = @regs.to_unsafe + ((chanNum << 5) + 0xC)

                  # Update the chip at key on
                  regPtr[0] = posPtr[0]
                  regPtr[1] = posPtr[1]
                  regPtr[2] = posPtr[2]

                  keyOn(chanNum)
                end
              end
            else
              8.times do |chanNum|
                keyOn(chanNum) if Yuno.bitflag?(data, 1 << chanNum)
              end
            end

          when 0x215
            8.times do |chanNum|
              keyOff(chanNum) if Yuno.bitflag?(data, 1 << chanNum)
            end

          when 0x22D
            @curZone[@curPtr] = data if @regs[0x22E] == 0x80
            @curPtr += 1
            @curPtr = 0 if @curPtr == @curLimit

          when 0x22E
            @curZone = if data == 0x80
                         @curLimit = 0x4000
                         Slice(UInt8).new(@ram.to_unsafe, @ram.size)
                       else
                         @curLimit = 0x20000
                         @rom[(0x20000 * data.to_i32!)..]
                       end
            @curPtr = 0
          end
        end

        @regs[offset] = data
      end

      def read(offset : Int) : UInt8
        case offset
        when 0x22D
          if Yuno.bitflag?(@regs[0x22F], 0x10)
            ret = @curZone[@curPtr]
            @curPtr += 1
            @curPtr = 0 if @curPtr == @curLimit
            return ret
          else
            return 0u8
          end

        when 0x22C
          nil
        else
          {% if flag?(:yunosynth_debug) %}
            Yuno.warn("K054539: unknown register read: #{offset}")
          {% end %}
        end

        @regs[offset]
      end

      def update(outputs : OutputBuffers, samples : UInt32) : Nil
        outL : Slice(Int32) = outputs[0]
        outR : Slice(Int32) = outputs[1]

        # Check once, then we can avoid bounds checks below.
        if outL.size < samples || outR.size < samples
          raise "K054539 update: Bad number of samples (expected #{samples}, " \
                "but buffers have #{outL.size} and #{outR.size})"
        end

        outL.fill(0, 0, samples)
        outR.fill(0, 0, samples)

        return unless Yuno.bitflag?(@regs[0x22F], 1)

        lval : Float64 = 0.0
        rval : Float64 = 0.0
        delta : Int32 = 0
        vol : Int32 = 0
        bval : Int32 = 0
        pan : Int32 = 0
        curGain : Float64 = 0.0
        lvol : Float64 = 0.0
        rvol : Float64 = 0.0
        rbvol : Float64 = 0.0
        rdelta : Int32 = 0
        curPos : UInt32 = 0
        fdelta : Int32 = 0
        pdelta : Int32 = 0
        curPFrac : Int32 = 0
        curVal : Int32 = 0
        curPVal : Int32 = 0
        base1 : Pointer(UInt8) = Pointer(UInt8).null
        base2 : Pointer(UInt8) = Pointer(UInt8).null
        rbase : Pointer(Int16) = Pointer(Int16).new(@ram.to_unsafe.address)

        samples.times do |i|
          unless Yuno.bitflag?(@flags, DISABLE_REVERB)
            lval = rval = rbase[@reverbPos].to_f64!
          else
            lval = rval = 0
          end

          rbase[@reverbPos] = 0

          8.times do |ch|
            if Yuno.bitflag?(@regs.unsafe_fetch(0x22C), 1u8 << ch) && @muted.unsafe_fetch(ch) == 0
              base1 = @regs.to_unsafe + (0x20 * ch)
              base2 = @regs.to_unsafe + (0x200 + 2 * ch)
              chan = @channels.unsafe_fetch(ch)

              delta = base1[0].to_i32! | (base1[1].to_i32! << 8) | (base1[2].to_i32! << 16)
              vol = base1[3].to_i32!

              bval = vol + base1[4].to_i32!
              bval = 255 if bval > 255

              pan = base1[5].to_i32!

              # DJ Main: 81-87 right, 88 middle, 89-8f left
              if pan >= 0x81 && pan <= 0x8F
                pan -= 0x81
              elsif pan >= 0x11 && pan <= 0x1F
                pan -= 0x11
              else
                pan = 0x18 - 0x11
              end

              curGain = @gain.unsafe_fetch(ch)

              lvol = @volTable[vol] * @panTable[pan] * curGain
              lvol = VOL_CAP if lvol > VOL_CAP

              rvol = @volTable[vol] * @panTable[0xE - pan] * curGain
              rvol = VOL_CAP if rvol > VOL_CAP

              rbvol = @volTable[bval] * curGain / 2
              rbvol = VOL_CAP if rbvol > VOL_CAP

              rdelta = (base1[6].to_i32! | (base1[7].to_i32! << 8)) >> 3
              rdelta = (rdelta + @reverbPos) & 0x3FFF

              curPos = base1[0x0C].to_u32! | (base1[0x0D].to_u32! << 8) | (base1[0x0E].to_u32! << 16)

              if Yuno.bitflag?(base2[0], 0x20)
                delta = -delta
                fdelta = 0x10000
                pdelta = -1
              else
                fdelta = -0x10000
                pdelta = 1
              end

              if curPos != chan.pos
                chan.pos = curPos
                curPFrac = 0
                curVal = 0
                curPVal = 0
              else
                curPFrac = chan.pfrac.to_i32!
                curVal = chan.val
                curPVal = chan.pval
              end

              case base2[0] & 0xC
              when 0 # 8-bit PCM
                curPFrac += delta
                while Yuno.bitflag?(curPFrac, ~0xFFFF)
                  curPFrac += fdelta
                  curPos += pdelta

                  curPVal = curVal
                  curVal = (@rom[curPos & @romMask].to_i32! << 8).to_i16!.to_i32!
                  if curVal == -32768 && Yuno.bitflag?(base2[1], 1)
                    curPos = base1[0x08].to_u32! | (base1[0x09].to_u32! << 8) | (base1[0x0A].to_u32! << 16)
                    curVal = (@rom[curPos & @romMask].to_i32! << 8).to_i16!.to_i32!
                  end

                  if curVal == -32768
                    keyOff(ch)
                    curVal = 0
                    break
                  end
                end

              when 0x04 # 16-bit PCM, LSB first
                pdelta <<= 1

                curPFrac += delta
                while Yuno.bitflag?(curPFrac, ~0xFFFF)
                  curPFrac += fdelta
                  curPos += pdelta

                  curPVal = curVal
                  curVal = (@rom[curPos & @romMask].to_i16! | (@rom[(curPos + 1) & @romMask].to_i16! << 8)).to_i32!
                  if curVal == -32768 && Yuno.bitflag?(base2[1], 1)
                    curPos = base1[0x08].to_u32! | (base1[0x09].to_u32! << 8) | (base1[0x0A].to_u32! << 16)
                    curVal = (@rom[curPos & @romMask].to_i16! | (@rom[(curPos + 1) & @romMask].to_i16! << 8)).to_i32!
                  end

                  if curVal == -32768
                    keyOff(ch)
                    curVal = 0
                    break
                  end
                end

              when 0x08 # 4-bit DPCM
                curPos <<= 1
                curPFrac <<= 1

                if Yuno.bitflag?(curPFrac, 0x10000)
                  curPFrac &= 0xFFFF
                  curPos |= 1
                end

                curPFrac += delta
                while Yuno.bitflag?(curPFrac, ~0xFFFF)
                  curPFrac += fdelta
                  curPos += pdelta

                  curPVal = curVal
                  curVal = @rom[(curPos >> 1) & @romMask].to_i32!
                  if curVal == 136 && Yuno.bitflag?(base2[1], 1)
                    curPos = (base1[0x08].to_u32! | (base1[0x09].to_u32! << 8) | (base1[0x0A].to_u32! << 16)) << 1
                    curVal = @rom[(curPos >> 1) & @romMask].to_i32!
                  end

                  if curVal == 136
                    keyOff(ch)
                    curVal = 0
                    break
                  end

                  if Yuno.bitflag?(curPos, 1)
                    curVal >>= 4
                  else
                    curVal &= 15
                  end

                  curVal = (curPVal + DPCM[curVal]).clamp(-32768, 32767)
                end

                curPFrac >>= 1
                curPFrac |= 0x8000 if Yuno.bitflag?(curPos, 1)
                curPos >>= 1

              else
                {% if flag?(:yunosynth_debug) %}
                  Yuno.warn("K054539: Unknown sample type #{base2[0] & 0xC} for channel #{ch}")
                {% end %}
              end

              lval += curVal * lvol
              rval += curVal * rvol
              rbase[(rdelta + @reverbPos) & 0x1FFF] += (curVal * rbvol).to_i16!

              chan.pos = curPos
              chan.pfrac = curPFrac.to_u32!
              chan.pval = curPVal
              chan.val = curVal

              if regUpdate
                base1[0x0C] = ( curPos        & 0xFF).to_u8!
                base1[0x0D] = ((curPos >>  8) & 0xFF).to_u8!
                base1[0x0E] = ((curPos >> 16) & 0xFF).to_u8!
              end
            end
          end # 8.times

          @reverbPos = (@reverbPos + 1) & 0x1FFF
          outL.unsafe_put(i, lval.to_i32!)
          outR.unsafe_put(i, rval.to_i32!)
        end
      end
    end
  end
end
