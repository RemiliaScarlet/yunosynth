#### Konami 053260 PCM Sound Chip Emulator
#### Copyright (C) 2004 The MAME Team
#### Copyright (C) 2023-2024 Remilia Scarlet
#### BSD-3-Clause License
require "math"

module Yuno::Chips
  class K053260 < Yuno::AbstractChip
    # Actual implementation of the K053260 emulator based on Mame's
    # implementation
    private class K053260Mame < Yuno::AbstractEmulator
      BASE_SHIFT = 16

      DPCM_CONV = [
        0i8, 1i8, 2i8, 4i8, 8i8, 16i8, 32i8, 64i8,
        -128i8, -64i8, -32i8, -16i8, -8i8, -4i8, -2i8, -1i8
      ]

      class Channel
        property rate     : UInt32 = 0
        property size     : UInt32 = 0
        property start    : UInt32 = 0
        property bank     : UInt32 = 0
        property volume   : UInt32 = 0
        property? play    : Bool = false
        property pan      : UInt32 = 0
        property pos      : UInt32 = 0
        property? hasLoop : Bool = false
        property? ppcm    : Bool = false # Packed PCM (4-bit signed)
        property ppcmData : Int8 = 0
        property muted    : UInt8 = 0

        def initialize
        end
      end

      @mode : Int32 = 0
      @regs : Array(Int32) = Array(Int32).new(0x30, 0i32)
      @rom : Bytes = Bytes.new(0)
      @deltaTable : Array(UInt32) = Array(UInt32).new(0x1000, 0u32)
      @channels : Array(Channel)
      @rate : UInt32

      def initialize(clock : UInt32)
        @channels = Array(Channel).new(4) { |_| Channel.new }
        @rate = clock.tdiv(32)
        initDeltaTable(@rate, clock)
        unmuteAll
      end

      protected def rate
        @rate
      end

      @[AlwaysInline]
      def unmuteAll : Nil
        4.times { |i| @channels[i].muted = 0 }
      end

      def reset
        @channels.each do |chan|
          chan.rate = 0
          chan.size = 0
          chan.start = 0
          chan.bank = 0
          chan.volume = 0
          chan.play = false
          chan.pan = 0
          chan.pos = 0
          chan.hasLoop = false
          chan.ppcm = false
          chan.ppcmData = 0
        end
      end

      def write(offset : Int32, data : UInt8) : Nil
        r = offset
        v = data.to_i32!

        if r > 0x2F
          Yuno.warn("K053260: Writing past registers")
          return
        end

        # Before we update the regs, we need to check for a latched reg.
        if r == 0x28
          t = @regs[r] ^ v

          4.times do |i|
            if Yuno.bitflag?(t, 1 << i)
              if Yuno.bitflag?(v, 1 << i)
                @channels[i].play = true
                @channels[i].pos = 0
                @channels[i].ppcmData = 0
                checkBounds(i)
              else
                @channels[i].play = false
              end
            end
          end

          @regs[r] = v.to_u8!.to_i32!
          return
        end

        # Update register
        @regs[r] = v.to_u8!.to_i32!

        # Communication registers
        return if r < 8

        # Channel setup
        if r < 0x28
          chan = @channels[(r - 8).tdiv(8)]
          case (r - 8) & 0x07
          when 0 # Sample rate low
            chan.rate &= 0x0F00
            chan.rate |= v

          when 1 # Sample rate high
            chan.rate &= 0x00FF
            chan.rate |= ((v & 0x0F) << 8)

          when 2 # Size low
            chan.size &= 0xFF00
            chan.size |= v

          when 3 # Size high
            chan.size &= 0x00FF
            chan.size |= (v << 8)

          when 4 # Start low
            chan.start &= 0xFF00
            chan.start |= v

          when 5 # Start high
            chan.start &= 0x00FF
            chan.start |= (v << 8)

          when 6 # Bank
            chan.bank = (v & 0xFF).to_u32!

          when 7 # volume is 7 bits. Convert to 8 bits now.
            chan.volume = (((v & 0x7F) << 1) | (v & 1)).to_u32!
          end

          return
        end

        case r
        when 0x2A # Loop, ppcm
          4.times do |i|
            @channels[i].hasLoop = Yuno.bitflag?(v, 1 << i)
          end

          (4...8).each do |i|
            @channels[i - 4].ppcm = Yuno.bitflag?(v, 1 << i)
          end

        when 0x2C # Pan
          @channels[0].pan = (v & 7).to_u32!
          @channels[1].pan = ((v >> 3) & 7).to_u32!

        when 0x2D # More pan
          @channels[2].pan = (v & 7).to_u32!
          @channels[3].pan = ((v >> 3) & 7).to_u32!

        when 0x2F # Control
          @mode = v & 7
        end
      end

      def read(offset : Int32) : UInt8|UInt32
        case offset
        when 0x29 # Channel status
          status : Int32 = 0
          4.times { |i| status |= ((@channels[i].play? ? 1 : 0) << i) }
          return status.to_u8!

        when 0x2E # Read rom
          if Yuno.bitflag?(@mode, 1)
            offs : UInt32 = @channels[0].start + (@channels[0].pos >> BASE_SHIFT) + (@channels[0].bank << 16)
            @channels[0].pos += (1 << 16)

            if offs > @rom.size
              Yuno.warn(sprintf("K053260: Attempting to read past ROM size in ROM Read Mode " \
                                "(offset: %06x$, size: %06x$)", offs, @rom.size))
              return 0u8
            end

            return @rom[offs]
          end
        end

        @regs[offset].to_u32!
      end

      def writeRom(romSize : Int32, dataStart : Int32, dataLength : Int32, romData : Slice(UInt8)) : Nil
        if @rom.size != romSize
          @rom = Bytes.new(romSize, 0xFF_u8)
        end

        return if dataStart > romSize
        dataLength = romSize - dataStart if dataStart + dataLength > romSize

        # Copy the memory
        dataLength.times do |i|
          # This can be guaranteed by the checks above
          @rom.unsafe_put(i + dataStart, romData.unsafe_fetch(i))
        end
      end

      def muteMask=(mask : UInt32) : Nil
        4.times do |i|
          @channels[i].muted = ((mask >> i) & 0x01).to_u8!
        end
      end

      def update(outputs : OutputBuffers, samples : UInt32) : Nil
        outL : Slice(Int32) = outputs[0]
        outR : Slice(Int32) = outputs[1]

        lvol  : StaticArray(UInt32, 4) = StaticArray(UInt32, 4).new(0u32)
        rvol  : StaticArray(UInt32, 4) = StaticArray(UInt32, 4).new(0u32)
        delta : StaticArray(UInt32, 4) = StaticArray(UInt32, 4).new(0u32)
        ends  : StaticArray(UInt32, 4) = StaticArray(UInt32, 4).new(0u32)
        pos   : StaticArray(UInt32, 4) = StaticArray(UInt32, 4).new(0u32)
        ppcmData : StaticArray(Int8, 4) = StaticArray(Int8, 4).new(0i8)
        play? : StaticArray(Bool, 4) = StaticArray(Bool, 4).new(false)
        loop? : StaticArray(Bool, 4) = StaticArray(Bool, 4).new(false)
        ppcm? : StaticArray(Bool, 4) = StaticArray(Bool, 4).new(false)
        rom   : StaticArray(UInt32, 4) = StaticArray(UInt32, 4).new(0u32)
        dataL : Int32 = 0
        dataR : Int32 = 0
        d : Int8 = 0


        # Check once, then we can avoid bounds checks below.
        if outL.size < samples || outR.size < samples
          raise "K053260 update: Bad number of samples (expected #{samples}, " \
                "but buffers have #{outL.size} and #{outR.size})"
        end

        # Pre-cache some values
        4.times do |i|
          if @channels[i].muted != 0
            play?[i] = false
            next
          end

          rom[i] = @channels[i].start + (@channels[i].bank << 16)
          delta[i] = @deltaTable[@channels[i].rate]
          lvol[i] = @channels[i].volume * @channels[i].pan
          rvol[i] = @channels[i].volume * (8 - @channels[i].pan)
          ends[i] = @channels[i].size
          pos[i] = @channels[i].pos
          play?[i] = @channels[i].play?
          loop?[i] = @channels[i].hasLoop?
          ppcm?[i] = @channels[i].ppcm?
          ppcmData[i] = @channels[i].ppcmData.to_i8!
          delta[i] = delta[i].tdiv(2) if ppcm?[i]
        end

        samples.times do |j|
          dataL = 0
          dataR = 0

          4.times do |i|
            # See if the voice is active
            if play?[i]
              # See if we're done
              if (pos[i] >> BASE_SHIFT) >= ends[i]
                ppcmData[i] = 0
                if loop?[i]
                  pos[i] = 0
                else
                  play?[i] = false
                  next
                end
              end

              if ppcm?[i]
                # Packed PCM
                #
                # We only update the signal if we're starting or a real sound
                # sample has gone by this is all due to the dynamic sample rate
                # conversion.
                if pos[i] == 0 || ((pos[i] ^ (pos[i] - delta[i])) & 0x8000) == 0x8000
                  newData = if Yuno.bitflag?(pos[i], 0x8000)
                              (@rom[rom[i] + (pos[i] >> BASE_SHIFT)].to_u16! >> 4) & 0x0F # High nibble
                            else
                              (@rom[rom[i] + (pos[i] >> BASE_SHIFT)].to_u16!     ) & 0x0F # Low nibble
                            end
                  ppcmData[i] = ppcmData[i] &+ DPCM_CONV[newData]
                end

                d = ppcmData[i]
                pos[i] += delta[i]
              else
                # PCM
                d = @rom[rom[i] + (pos[i] >> BASE_SHIFT)].to_i8!
                pos[i] += delta[i]
              end

              if Yuno.bitflag?(@mode, 2)
                dataL += (d.to_i32! * lvol[i].to_i32!) >> 2
                dataR += (d.to_i32! * rvol[i].to_i32!) >> 2
              end
            end
          end

          outL.unsafe_put(j, dataL.clamp(-32768, 32767))
          outR.unsafe_put(j, dataR.clamp(-32768, 32767))
        end

        # Update registers
        4.times do |i|
          next if @channels[i].muted != 0
          @channels[i].pos = pos[i]
          @channels[i].play = play?[i]
          @channels[i].ppcmData = ppcmData[i]
        end
      end

      private def initDeltaTable(rate : Int, clock : Int) : Nil
        base : Float64 = rate.to_f64!
        max : Float64 = clock.to_f64!
        val : UInt32 = 0
        v : Float64 = 0.0
        target : Float64 = 0.0
        fixed : Float64 = 0.0

        0x1000.times do |i|
          v = (0x1000 - i).to_f64!
          target = max / v
          fixed = (1 << BASE_SHIFT).to_f64!

          if target != 0 && base != 0
            target = fixed / (base / target)
            val = target.to_u32!
            val = 1u32 if val == 0
          else
            val = 1u32
          end

          @deltaTable[i] = val
        end
      end

      @[AlwaysInline]
      private def checkBounds(channel : Int) : Nil
        chanStart = (@channels[channel].bank << 16) + @channels[channel].start
        chanEnd = chanStart.to_i64! - @channels[channel].size.to_i64! - 1

        if chanStart > @rom.size
          Yuno.warn(sprintf("K053260: Attempting to start playing past the end of the ROM " \
                            "(start: %06x$, end: %06x$)", chanStart, chanEnd))
          @channels[channel].play = false
          return
        end

        if chanEnd > @rom.size
          Yuno.warn(sprintf("K053260: Attempting play past the end of the ROM " \
                            "(start: %06x$, end: %06x$)", chanStart, chanEnd))
          @channels[channel].size = (@rom.size - chanStart).to_u32!
        end
      end
    end
  end
end
