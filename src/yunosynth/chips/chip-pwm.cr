#### YunoSynth
#### Copyright (C) 2023-2024 Remilia Scarlet <remilia@posteo.jp>
#### Portions based on VGMPlay, Copyright (C) Valley Bell
####
#### This program is free software: you can redistribute it and/or
#### modify it under the terms of the GNU Affero General Public
#### License as published by the Free Software Foundation, either
#### version 3 of the License, or (at your option) any later version.
####
#### This program is distributed in the hope that it will be useful,
#### but WITHOUT ANY WARRANTY; without even the implied warranty of
#### MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#### Affero General Public License for more details.
####
#### You should have received a copy of the GNU Affero General Public License
#### along with this program.  If not, see <https://www.gnu.org/licenses/>.
require "./emu-c140-mame"

####
#### Sega PWM sound chip emulator interface
####

module Yuno::Chips
  # PWM sound chip emulator.
  class Pwm < Yuno::AbstractChip
    CHIP_ID = 0x11_u32

    @clockFromHeader : UInt32 = 0
    @samplingMode : UInt8 = 0
    @chipSampleRate : UInt32 = 0
    @chip : PwmGens? = nil

    def initialize(chipNum : Int32, absChipCount : Int32, vgm : VgmFile, playbackSampleRate : UInt32,
                   newSamplingMode : UInt8, newPlayerSampleRate : UInt32, *, emuCore : Symbol? = nil,
                   flags : ChipFlags? = nil)
      @clockFromHeader = vgm.getAlternateChipClock(chipNum, Yuno::ChipType::Pwm) ||  vgm.header.pwmClock
      @clockFromHeader &= 0x3FFFFFFF
      @playerSampleRate = newPlayerSampleRate
      chipCount = (vgm.header.pwmClock & 0x40000000) != 0 ? 2 : 1
      initFields(vgm, emuCore, playbackSampleRate, newSamplingMode, chipCount, flags)
    end

    protected def initialize
    end

    protected def initFields(vgm : VgmFile, emuCore : Symbol?, playbackSampleRate : UInt32, newSamplingMode : UInt8,
                             chipCount : Int32, flags : ChipFlags?) : Nil
      @volume = vgm.getChipVolume(self, ChipType::Pwm, chipCount)

      @core = emuCore || Pwm.defaultEmuCore
      case @core
      when :gens
        @samplingMode = newSamplingMode
        @chipSampleRate = @clockFromHeader <= 0 ? playbackSampleRate : @clockFromHeader
      else raise YunoError.new("Unsupported emulation core for PWM: #{@core}")
      end
    end

    def type : ChipType
      ChipType::Pwm
    end

    def name : String
      "Sega 32x PWM"
    end

    def shortName : String
      "32x PWM"
    end

    def id : UInt32
      CHIP_ID
    end

    def emuCore : Symbol
      @core
    end

    def self.defaultEmuCore : Symbol
      :gens
    end

    def start(chipIndex : UInt8, clock : UInt32, flags : ChipFlags? = nil, previousChip : AbstractChip? = nil) : UInt32
      case @core
      when :gens
        @sampleRate = 22020u32 # that's the rate the PWM is mostly used
        if (Yuno.bitflag?(@samplingMode, 0x01) && @sampleRate < @chipSampleRate) || @samplingMode == 0x02
          @sampleRate = @chipSampleRate
        end

        @chip = PwmGens.new(clock)
        @sampleRate
      else raise YunoError.new("Unsupported emulation core for PWM: #{@core}")
      end
    end

    def getClock : UInt32
      @clockFromHeader
    end

    def getStartFlags(vgm : VgmFile) : ChipFlags?
      nil
    end

    def update(outputs : OutputBuffers, samples : Int) : Nil
      @chip.not_nil!.update(outputs, samples.to_u32!)
    end

    def reset(chipIndex : UInt8) : Nil
      @chip.not_nil!.reset
    end

    def read(chipIndex : UInt8, offset : Int) : UInt8|UInt16|UInt32
      0u8
    end

    def write(chipIndex : UInt8, offset : Int, data : Int, port : UInt8 = 0) : Nil
      @chip.not_nil!.write(port, offset.to_u16! << 8 | data.to_u16!)
    end

    def writeDac(port : Int, command : Int, data : Int) : Nil
      write(0, command, data, port.to_u8!)
    end

    def setMuteMask(chipIndex : UInt8, mask : UInt32) : Nil
    end

    def getVolModifier : UInt32
      @volume.to_u32
    end

    def baseVolume : UInt16
      0xE0_u16
    end

    # The internal interface for the chip.
    @[AlwaysInline]
    def chip : AbstractEmulator
      @chip.not_nil!
    end
  end
end
