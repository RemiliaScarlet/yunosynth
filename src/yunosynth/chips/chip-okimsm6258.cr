#### YunoSynth
#### Copyright (C) 2023-2024 Remilia Scarlet <remilia@posteo.jp>
#### Portions based on VGMPlay, Copyright (C) Valley Bell
####
#### This program is free software: you can redistribute it and/or
#### modify it under the terms of the GNU Affero General Public
#### License as published by the Free Software Foundation, either
#### version 3 of the License, or (at your option) any later version.
####
#### This program is distributed in the hope that it will be useful,
#### but WITHOUT ANY WARRANTY; without even the implied warranty of
#### MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#### Affero General Public License for more details.
####
#### You should have received a copy of the GNU Affero General Public License
#### along with this program.  If not, see <https://www.gnu.org/licenses/>.
require "./emu-okimsm6258-mame"

####
#### OKI MSM6258 sound chip emulator interface
####

module Yuno::Chips
  # OKI MSM6258 sound chip emulator.
  class OKIMSM6258 < Yuno::AbstractChip
    CHIP_ID = 0x17_u32

    @clockFromHeader : UInt32 = 0
    @samplingMode : UInt8 = 0
    @chipSampleRate : UInt32 = 0
    @chip : OKIMSM6258Mame? = nil
    @specialFlags : UInt8 = 0

    class OKIMSM6258Flags < ChipFlags
      property flags : UInt8 = 0
      property? enable10Bit : Bool = false

      def initialize
      end
    end

    def initialize(chipNum : Int32, absChipCount : Int32, vgm : VgmFile, playbackSampleRate : UInt32,
                   newSamplingMode : UInt8, newPlayerSampleRate : UInt32, *, emuCore : Symbol? = nil,
                   flags : ChipFlags? = nil)
      @clockFromHeader = vgm.getAlternateChipClock(chipNum, Yuno::ChipType::Okim6258) || vgm.header.okim6258Clock
      @clockFromHeader &= 0x3FFFFFFF
      @playerSampleRate = newPlayerSampleRate
      chipCount = (vgm.header.okim6258Clock & 0x40000000) != 0 ? 2 : 1
      initFields(vgm, emuCore, playbackSampleRate, newSamplingMode, chipCount)
    end

    protected def initialize
    end

    protected def initFields(vgm : VgmFile, emuCore : Symbol?, playbackSampleRate : UInt32, newSamplingMode : UInt8,
                             chipCount : Int32) : Nil
      @volume = vgm.getChipVolume(self, ChipType::Okim6258, chipCount)
      @core = emuCore || OKIMSM6258.defaultEmuCore
      case @core
      when :mame
        @samplingMode = newSamplingMode
        @chipSampleRate = @clockFromHeader <= 0 ? playbackSampleRate : @clockFromHeader
      else raise YunoError.new("Unsupported emulation core for OKI MSM6258: #{@core}")
      end
    end

    def type : ChipType
      ChipType::Okim6258
    end

    def name : String
      "OKI MSM6258"
    end

    def shortName : String
      "MSM6258"
    end

    def id : UInt32
      CHIP_ID
    end

    def emuCore : Symbol
      @core
    end

    def self.defaultEmuCore : Symbol
      :mame
    end

    def start(chipIndex : UInt8, clock : UInt32, flags : ChipFlags? = nil, previousChip : AbstractChip? = nil) : UInt32
      raise YunoError.new("OKI MSM6258 requires start flags") unless flags

      case @core
      when :mame
        @chip = OKIMSM6258Mame.new(clock,
                                   (flags.as(OKIMSM6258Flags).flags & 0x03),
                                   ((flags.as(OKIMSM6258Flags).flags & 0x04) >> 2),
                                   ((flags.as(OKIMSM6258Flags).flags & 0x08) >> 3))

        flagData : UInt8 = flags.as(OKIMSM6258Flags).enable10Bit? ? 1u8 : 0u8
        @chip.not_nil!.options = flagData.to_u16!
        @chip.not_nil!.setSampleRateChangeCB(->AbstractChip.changeChipSampleRate(AbstractChip, UInt32) , self)
        @sampleRate = @chip.not_nil!.rate # Update sample rate
        @sampleRate
      else raise YunoError.new("Unsupported emulation core for OKI MSM6258: #{@core}")
      end
    end

    def getClock : UInt32
      @clockFromHeader
    end

    def getStartFlags(vgm : VgmFile) : ChipFlags?
      ret = OKIMSM6258Flags.new
      ret.flags = vgm.header.okim6258Flags
      ret
    end

    def update(outputs : OutputBuffers, samples : Int) : Nil
      @chip.not_nil!.update(outputs, samples.to_u32!)
    end

    def reset(chipIndex : UInt8) : Nil
      @chip.not_nil!.reset
    end

    def read(chipIndex : UInt8, offset : Int) : UInt8|UInt16|UInt32
      0u8
    end

    def write(chipIndex : UInt8, offset : Int, data : Int, port : UInt8 = 0) : Nil
      @chip.not_nil!.write(offset.to_u8!, data.to_u8!)
    end

    def writeDac(port : Int, command : Int, data : Int) : Nil
      write(0, command, data)
    end

    def setMuteMask(chipIndex : UInt8, mask : UInt32) : Nil
      @chip.not_nil!.muteMask = mask
    end

    def getVolModifier : UInt32
      @volume.to_u32 * 2
    end

    def baseVolume : UInt16
      0x1C0_u16
    end

    # The internal interface for the chip.
    @[AlwaysInline]
    def chip : AbstractEmulator
      @chip.not_nil!
    end
  end
end
