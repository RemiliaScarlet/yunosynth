#### ES5503 - Ensoniq ES5503 "DOC" emulator v2.1.1
#### By R. Belmont.
#### Crystal port Copyright (C) 2023-2024 Remilia Scarlet
####
#### Copyright R. Belmont.
####
#### This software is dual-licensed: it may be used in MAME and properly licensed
#### MAME derivatives under the terms of the MAME license.  For use outside of
#### MAME and properly licensed derivatives, it is available under the
#### terms of the GNU Lesser General Public License (LGPL), version 2.1.
#### You may read the LGPL at http://www.gnu.org/licenses/lgpl.html
####
#### History: the ES5503 was the next design after the famous C64 "SID" by Bob Yannes.
#### It powered the legendary Mirage sampler (the first affordable pro sampler) as well
#### as the ESQ-1 synth/sequencer.  The ES5505 (used in Taito's F3 System) and 5506
#### (used in the "Soundscape" series of ISA PC sound cards) followed on a fundamentally
#### similar architecture.
####
#### Bugs: On the real silicon, oscillators 30 and 31 have random volume fluctuations and are
#### unusable for playback.  We don't attempt to emulate that. :-)
####
#### Additionally, in "swap" mode, there's one cycle when the switch takes place where the
#### oscillator's output is 0x80 (centerline) regardless of the sample data.  This can
#### cause audible clicks and a general degradation of audio quality if the correct sample
#### data at that point isn't 0x80 or very near it.
####
#### Changes:
#### 0.2 (RB) - improved behavior for volumes > 127, fixes missing notes in Nucleus & missing voices in Thexder
#### 0.3 (RB) - fixed extraneous clicking, improved timing behavior for e.g. Music Construction Set & Music Studio
#### 0.4 (RB) - major fixes to IRQ semantics and end-of-sample handling.
#### 0.5 (RB) - more flexible wave memory hookup (incl. banking) and save state support.
#### 1.0 (RB) - properly respects the input clock
#### 2.0 (RB) - C++ conversion, more accurate oscillator IRQ timing
#### 2.1 (RB) - Corrected phase when looping; synthLAB, Arkanoid, and Arkanoid II no longer go out of tune
#### 2.1.1 (RB) - Fixed issue introduced in 2.0 where IRQs were delayed

module Yuno::Chips
  class ES5503 < Yuno::AbstractChip
    # Actual implementation of the ES5503 emulator based on Mame's implementation.
    private class ES5503Mame < Yuno::AbstractEmulator
      WAVE_SIZES = [
        256_u16, 512_u16, 1024_u16, 2048_u16, 4096_u16, 8192_u16, 16384_u16, 32768_u16
      ]

      WAVE_MASKS = [
        0x1FF00_u32, 0x1FE00_u32, 0x1FC00_u32, 0x1F800_u32, 0x1F000_u32, 0x1E000_u32, 0x1C000_u32, 0x18000_u32
      ]

      ACC_MASKS = [
        0xFF_u32, 0x1FF_u32, 0x3FF_u32, 0x7FF_u32, 0xFFF_u32, 0x1FFF_u32, 0x3FFF_u32, 0x7FFF_u32
      ]

      RES_SHIFTS = [ 9, 10, 11, 12, 13, 14, 15, 16 ]

      NUM_OSCS = 32_i8

      enum Mode
        Free = 0
        OneShot = 1
        SynCam = 2
        Swap = 3
      end

      class Oscillator
        property freq : UInt16 = 0
        property wtSize : UInt16 = 0
        property control : UInt8 = 0
        property vol : UInt8 = 0
        property data : UInt8 = 0
        property waveTablePointer : UInt32 = 0
        property waveTableSize : UInt8 = 0
        property resolution : UInt8 = 0
        property accumulator : UInt32 = 0
        property irqPending : UInt8 = 0
        property muted : UInt8 = 0

        def initialize
        end
      end

      @oscillators : Array(Oscillator)
      @dramSize : UInt32 = 0x20000_u32 # 128 KB
      @docRam : Slice(UInt8) = Slice(UInt8).new(0x20000, 0u8)
      @oscsEnabled : Int8 = 0 # Number of oscillators enabled
      @regE0 : Int32 = 0xFF # Contents of register 0xE0
      @channelStrobe : UInt8 = 0
      @clock : UInt32 = 0
      @outputChannels : Int32 = 0
      @outChanMask : Int32
      @outputRate : UInt32 = 0

      property sampleRateFunc : SampleRateCallback?
      property sampleRateData : AbstractChip?

      def initialize(@clock : UInt32, @outputChannels : Int32)
        @oscillators = Array(Oscillator).new(NUM_OSCS) { |_| Oscillator.new }

        @outChanMask = 1
        while @outChanMask < @outputChannels
          @outChanMask <<= 1
        end
        @outChanMask -= 1

        {% if flag?(:yunosynth_debug) %}
          Yuno.dlog!("ES5503 output channel mask: #{@outChanMask} (num channels: #{@outputChannels})")
        {% end %}

        @outputRate = @clock.tdiv(8).tdiv(34)
      end

      def rate : UInt32
        @outputRate
      end

      private def haltOsc(onum : Int, typ : Int, accumulator : Pointer(UInt32), resShift : Int) : Nil
        osc : Oscillator = @oscillators[onum]
        partner : Oscillator = @oscillators[onum ^ 1]
        mode : Mode = Mode.from_value((osc.control >> 1) & 3)
        omode : Mode = Mode.from_value((partner.control >> 1) & 3)

        # If 0 found in sample data or mode is not free-run, halt this oscillator.
        if !mode.free? || typ != 0
          osc.control |= 1
        else
          # Preserve the relative phase of the oscillator when looping.
          wtSize : UInt16 = osc.wtSize - 1
          altRam : UInt32 = accumulator.value >> resShift
          altRam = if altRam > wtSize
                     altRam - wtSize
                   else
                     0u32
                   end
          accumulator.value = altRam << resShift
        end

        # If swap mode, start the partner.
        # Note: The swap mode fix breaks Silpheed and other games.
        if mode.swap?
          # Clear the halt bit, then make sure it starts from the top (does this
          # also need phase preservation?)
          partner.control &= ~1_u8
          partner.accumulator = 0
        end

        # IRQ enabled for this voice?
        if Yuno.bitflag?(osc.control, 0x08)
          osc.irqPending = 1
        end
      end

      def update(outputs : OutputBuffers, samples : UInt32) : Nil
        ramPtr : UInt32 = 0
        chansStereo : Int32 =  @outputChannels & ~1i32
        wtPtr : UInt32 = 0
        altRam : UInt32 = 0
        acc : UInt32 = 0
        wtSize : UInt16 = 0
        freq : UInt16 = 0
        vol : Int16 = 0
        chanMask : UInt8 = 0
        resShift : Int32 = 0
        sizeMask : UInt32 = 0
        outData : Int32 = 0
        chan : Int32 = 0
        outL : Slice(Int32) = outputs[0]
        outR : Slice(Int32) = outputs[1]
        outOther = uninitialized Slice(Int32)

        # Check once, then we can avoid bounds checks below.
        if outL.size < samples || outR.size < samples
          raise "ES5503 update: Bad number of samples (expected #{samples}, " \
                "but buffers have #{outL.size} and #{outR.size})"
        end

        outL.fill(0, 0, samples)
        outR.fill(0, 0, samples)

        {% unless flag?(:yunosynth_wd40) %}
          raise "ES5503: @oscsEnabled is out of range" if @oscsEnabled > NUM_OSCS || @oscsEnabled < 0
        {% end %}

        @oscsEnabled.times do |oscNum|
          osc = {% if flag?(:yunosynth_wd40) %}
                  @oscillators.unsafe_fetch(oscNum)
                {% else %}
                  @oscillators[oscNum]
                {% end %}

          if !Yuno.bitflag?(osc.control, 1) && osc.muted == 0
            wtPtr = osc.waveTablePointer & WAVE_MASKS[osc.waveTableSize]
            acc = osc.accumulator
            wtSize = osc.wtSize - 1
            freq = osc.freq
            vol = osc.vol.to_i16!
            chanMask = (osc.control >> 4) & 0x0F
            resShift = RES_SHIFTS[osc.resolution] - osc.waveTableSize
            sizeMask = ACC_MASKS[osc.waveTableSize]
            outData = 0

            chanMask &= @outChanMask
            samples.times do |snum|
              altRam = acc >> resShift
              ramPtr = altRam & sizeMask
              acc += freq

              # Channel strobe is always valid when reading; this allows
              # potentially banking per voice.
              osc.data = @docRam[ramPtr + wtPtr]

              if osc.data == 0
                haltOsc(oscNum, 1, pointerof(acc), resShift)
              else
                outData = (osc.data.to_i32! - 0x80) * vol

                # Send groups of two channels to L or R
                chan = 0
                while chan < chansStereo
                  if chan == chanMask
                    outOther = outputs.unsafe_fetch(chan & 1)
                    outOther.unsafe_put(snum, outOther.unsafe_fetch(snum) + outData)
                  end
                  chan += 1
                end

                outData = (outData * 181) >> 8 # outData *= sqrt(2)

                # Send remaining channels to L+R
                while chan < @outputChannels
                  if chan == chanMask
                    outL.unsafe_put(snum, outL.unsafe_fetch(snum) + outData)
                    outR.unsafe_put(snum, outR.unsafe_fetch(snum) + outData)
                  end
                  chan += 1
                end

                if altRam >= wtSize
                  haltOsc(oscNum, 0, pointerof(acc), resShift)
                end
              end

              # If the oscillator is halted, then we have no more samples to
              # generate.
              break if Yuno.bitflag?(osc.control, 1)
            end

            osc.accumulator = acc
          end
        end # @oscsEnabled.times
      end # def update

      def reset : Nil
        @oscillators.each do |osc|
          osc.freq = 0
          osc.wtSize = 0
          osc.control = 0
          osc.vol = 0
          osc.data = 0x80
          osc.waveTablePointer = 0
          osc.waveTableSize = 0
          osc.resolution = 0
          osc.accumulator = 0
          osc.irqPending = 0
        end

        @oscsEnabled = 1
        @channelStrobe = 0
        @docRam.fill(0u8, 0, @dramSize)
        @outputRate = @clock.tdiv(8).tdiv(2 + @oscsEnabled)
        @sampleRateFunc.try do |fn|
          fn.call(@sampleRateData.not_nil!, @outputRate)
        end
      end

      def read(offset : Int) : UInt8
        if offset < 0xE0
          # Oscillator registers

          osc = offset & 0x1F
          case offset & 0xE0
          when 0 # Freq low
            return @oscillators.unsafe_fetch(osc).freq.to_u8!

          when 0x20 # Freq high
            return (@oscillators.unsafe_fetch(osc).freq >> 8).to_u8!

          when 0x40 # Volume
            return @oscillators.unsafe_fetch(osc).vol

          when 0x60 # Data
            return @oscillators.unsafe_fetch(osc).data

          when 0x80 # Wavetable pointer
            return (@oscillators.unsafe_fetch(osc).waveTablePointer >> 8).to_u8!

          when 0xA0 # Oscillator control
            return @oscillators.unsafe_fetch(osc).control

          when 0xC0 # Bank select / wavetable size / resolution
            retVal : UInt8 = 0
            oscInst = @oscillators.unsafe_fetch(osc)
            if Yuno.bitflag?(oscInst.waveTablePointer, 0x10000)
              retVal |= 0x40
            end

            retVal |= (oscInst.waveTableSize << 3)
            return retVal | oscInst.resolution
          end
        else
          # Global registers
          case offset
          when 0xE0 # Interrupt status
            retVal = @regE0.to_u8!

            # Scan all enabled oscillators
            @oscsEnabled.times do |i|
              if @oscillators[i].irqPending != 0
                # Signal this oscillator has an interrupt
                retVal = (i << 1).to_u8!
                @regE0 = (retVal | 0x80).to_i32!

                # And clear its flag
                @oscillators[i].irqPending = 0
                break
              end
            end

            return retVal

          when 0xE1 # Oscillator enable
            return ((@oscsEnabled - 1) << 1).to_u8!
          end
        end

        0u8
      end

      def write(offset : Int, data : UInt8) : Nil
        oscNum = offset & 0x1F
        osc = @oscillators.unsafe_fetch(oscNum)

        if offset < 0xE0
          # Oscillator registers
          case offset & 0xE0
          when 0 # Freq low
            osc.freq &= 0xFF00
            osc.freq |= data

          when 0x20 # Freq high
            osc.freq &= 0x00FF
            osc.freq |= (data.to_u16! << 8)

          when 0x40 # Volume
            osc.vol = data

          # ignore write commands
          # when 0x60 # data

          when 0x80 # Wavetable pointer
            osc.waveTablePointer = data.to_u32! << 8

          when 0xA0 # Oscillator control
            # If this is a fresh key-on event, reset the accumulator.
            if Yuno.bitflag?(osc.control, 1) && !Yuno.bitflag?(data, 1)
              osc.accumulator = 0
            end

            osc.control = data

          when 0xC0 # Bank select / wavetable size / resolution
            if Yuno.bitflag?(data, 0x40)
              # Bank select - not used on the Apple IIgs
              osc.waveTablePointer |= 0x10000
            else
              osc.waveTablePointer &= 0xffff
            end

            osc.waveTableSize = (data >> 3) & 7
            osc.wtSize = WAVE_SIZES[osc.waveTableSize]
            osc.resolution = data & 7
          end
        else
          # Global registers
          case offset
          # Ignored
          #when 0xE0 # Interrupt status

          when 0xE1 # Oscillator enable
            # Remi: Clamp this so we can use unsafe_fetch later on in the #update method..
            @oscsEnabled = (1 + ((data >> 1) & 0x1F)).to_i8!.clamp(0i8, NUM_OSCS)
            @outputRate = @clock.tdiv(8).tdiv(2 + @oscsEnabled)
            @sampleRateFunc.try do |fn|
              fn.call(@sampleRateData.not_nil!, @outputRate)
            end

          # Ignored
          #when 0xE2 # A/D converter
          end
        end
      end

      def writeRam(dataStart : Int32, dataLength : Int32, data : Slice(UInt8)) : Nil
        return if dataStart >= @dramSize
        dataLength = @dramSize - dataStart if dataStart + dataLength > @dramSize

        # Copy the memory
        dataLength.times do |i|
          # This can be guaranteed by the checks above
          @docRam.unsafe_put(i + dataStart, data.unsafe_fetch(i))
        end
      end

      @[AlwaysInline]
      def setSampleRateChangeCB(@sampleRateFunc : SampleRateCallback, @sampleRateData : AbstractChip) : Nil
      end

      @[AlwaysInline]
      def muteMask=(mask : UInt32) : Nil
        NUM_OSCS.times do |i|
          @oscillators.unsafe_fetch(i).muted = ((mask >> i) & 0x01).to_u8!
        end
      end

      @[AlwaysInline]
      def unmuteAll : Nil
        self.muteMask = 0
      end
    end
  end
end
