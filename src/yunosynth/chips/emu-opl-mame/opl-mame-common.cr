####
#### File: fmopl.c - software implementation of FM sound generator
####                                            types OPL and OPL2
####
#### Copyright Jarek Burczynski (bujar at mame dot net)
#### Copyright Tatsuyuki Satoh , MultiArcadeMachineEmulator development
#### Crystal port Copyright (C) 2023-2024 Remilia Scarlet
####
####  Version 0.72
####
####
#### Revision History:
####
#### 04-08-2003 Jarek Burczynski:
####  - removed BFRDY hack. BFRDY is busy flag, and it should be 0 only when the chip
####    handles memory read/write or during the adpcm synthesis when the chip
####    requests another byte of ADPCM data.
####
#### 24-07-2003 Jarek Burczynski:
####  - added a small hack for Y8950 status BFRDY flag (bit 3 should be set after
####    some (unknown) delay). Right now it's always set.
####
#### 14-06-2003 Jarek Burczynski:
####  - implemented all of the status register flags in Y8950 emulation
####  - renamed y8950_set_delta_t_memory() parameters from _rom_ to _mem_ since
####    they can be either RAM or ROM
####
#### 08-10-2002 Jarek Burczynski (thanks to Dox for the YM3526 chip)
####  - corrected ym3526_read() to always set bit 2 and bit 1
####    to HIGH state - identical to ym3812_read (verified on real YM3526)
####
#### 04-28-2002 Jarek Burczynski:
####  - binary exact Envelope Generator (verified on real YM3812);
####    compared to YM2151: the EG clock is equal to internal_clock,
####    rates are 2 times slower and volume resolution is one bit less
####  - modified interface functions (they no longer return pointer -
####    that's internal to the emulator now):
####     - new wrapper functions for OPLCreate: ym3526_init(), ym3812_init() and y8950_init()
####  - corrected 'off by one' error in feedback calculations (when feedback is off)
####  - enabled waveform usage (credit goes to Vlad Romascanu and zazzal22)
####  - speeded up noise generator calculations (Nicola Salmoria)
####
#### 03-24-2002 Jarek Burczynski (thanks to Dox for the YM3812 chip)
####  Complete rewrite (all verified on real YM3812):
####  - corrected sin_tab and tl_tab data
####  - corrected operator output calculations
####  - corrected waveform_select_enable register;
####    simply: ignore all writes to waveform_select register when
####    waveform_select_enable == 0 and do not change the waveform previously selected.
####  - corrected KSR handling
####  - corrected Envelope Generator: attack shape, Sustain mode and
####    Percussive/Non-percussive modes handling
####  - Envelope Generator rates are two times slower now
####  - LFO amplitude (tremolo) and phase modulation (vibrato)
####  - rhythm sounds phase generation
####  - white noise generator (big thanks to Olivier Galibert for mentioning Berlekamp-Massey algorithm)
####  - corrected key on/off handling (the 'key' signal is ORed from three sources: FM, rhythm and CSM)
####  - funky details (like ignoring output of operator 1 in BD rhythm sound when connect == 1)
####
#### 12-28-2001 Acho A. Tang
####  - reflected Delta-T EOS status on Y8950 status port.
####  - fixed subscription range of attack/decay tables
####
####
####     To do:
####         add delay before key off in CSM mode (see CSMKeyControll)
####         verify volume of the FM part on the Y8950
####
require "../../common.cr"

####
#### Common Yamaha OPL Mame Routines
####

module Yuno::Chips
  ###
  ### Note: Do NOT include OPLMame.  Use the Y8950 or YM3526 classes.
  ###

  # :nodoc:
  module OPLMame
    ###
    ### Aliases
    ###

    alias OPLIrqHandler       = Proc(Yuno::AbstractEmulator, Int32, Nil)
    alias OPLUpdateHandler    = Proc(Yuno::AbstractEmulator, Nil)

    ###
    ### Constants
    ###

    FREQ_SH  = 16  # 16.16 fixed point (frequency calculations)
    EG_SH    = 16  # 16.16 fixed point (EG timing)
    LFO_SH   = 24  # 8.24 fixed point (LFO calculations)
    TIMER_SH = 16  # 16.16 fixed point (timers calculations)

    FREQ_MASK = (1 << FREQ_SH) - 1

    # Envelope output entries
    ENV_BITS = 10
    ENV_LEN  = 1 << ENV_BITS
    ENV_STEP = 128.0 / ENV_LEN

    MAX_ATT_INDEX = (1 << (ENV_BITS - 1)) - 1 # 511
    MIN_ATT_INDEX = 0

    # Sinwave entries
    SIN_BITS = 10
    SIN_LEN  = 1 << SIN_BITS
    SIN_MASK = SIN_LEN - 1

    TL_RES_LEN = 256 # 8 bits addressing (real chip)

    # Register number to channel number , slot offset
    SLOT1 = 0
    SLOT2 = 1

    # Envelope Generator phases

    EG_ATT = 4u8
    EG_DEC = 3u8
    EG_SUS = 2u8
    EG_REL = 1u8
    EG_OFF = 0u8

    OPL_TYPE_WAVESEL  = 0x01_u8 # Waveform select
    OPL_TYPE_ADPCM    = 0x02_u8 # DELTA-T ADPCM unit
    OPL_TYPE_KEYBOARD = 0x04_u8 # Keyboard interface
    OPL_TYPE_IO       = 0x08_u8 # I/O port

    # Mapping of register number (offset) to slot number used by the emulator.
    SLOT_ARRAY = [
      0, 2, 4, 1, 3, 5, -1, -1,
      6, 8, 10, 7, 9, 11, -1, -1,
      12, 14, 16, 13, 15, 17, -1, -1,
      -1, -1, -1, -1, -1, -1, -1, -1
    ]

    # Key scale level.
    #
    # Table is 3dB/octave, kslDv then converts this into 6dB/octave.
    # 0.1875 is bit 0 weight of the envelope counter (volume) expressed in decibels.
    macro kslDv(val)
      ({{val}} / (0.1875 / 2)).to_u32!
    end
    KSL_TAB = [
      # OCT
      kslDv(0.000), kslDv(0.000), kslDv(0.000), kslDv(0.000),
      kslDv(0.000), kslDv(0.000), kslDv(0.000), kslDv(0.000),
      kslDv(0.000), kslDv(0.000), kslDv(0.000), kslDv(0.000),
      kslDv(0.000), kslDv(0.000), kslDv(0.000), kslDv(0.000),

      # OCT 1
      kslDv(0.000), kslDv(0.000), kslDv(0.000), kslDv(0.000),
      kslDv(0.000), kslDv(0.000), kslDv(0.000), kslDv(0.000),
      kslDv(0.000), kslDv(0.750), kslDv(1.125), kslDv(1.500),
      kslDv(1.875), kslDv(2.250), kslDv(2.625), kslDv(3.000),

      # OCT 2
      kslDv(0.000), kslDv(0.000), kslDv(0.000), kslDv(0.000),
      kslDv(0.000), kslDv(1.125), kslDv(1.875), kslDv(2.625),
      kslDv(3.000), kslDv(3.750), kslDv(4.125), kslDv(4.500),
      kslDv(4.875), kslDv(5.250), kslDv(5.625), kslDv(6.000),

      # OCT 3
      kslDv(0.000), kslDv(0.000), kslDv(0.000), kslDv(1.875),
      kslDv(3.000), kslDv(4.125), kslDv(4.875), kslDv(5.625),
      kslDv(6.000), kslDv(6.750), kslDv(7.125), kslDv(7.500),
      kslDv(7.875), kslDv(8.250), kslDv(8.625), kslDv(9.000),

      # OCT 4
      kslDv(0.000),  kslDv(0.000),  kslDv(3.000),  kslDv(4.875),
      kslDv(6.000),  kslDv(7.125),  kslDv(7.875),  kslDv(8.625),
      kslDv(9.000),  kslDv(9.750),  kslDv(10.125), kslDv(10.500),
      kslDv(10.875), kslDv(11.250), kslDv(11.625), kslDv(12.000),

      # OCT 5
      kslDv(0.000),  kslDv(3.000),  kslDv(6.000),  kslDv(7.875),
      kslDv(9.000),  kslDv(10.125), kslDv(10.875), kslDv(11.625),
      kslDv(12.000), kslDv(12.750), kslDv(13.125), kslDv(13.500),
      kslDv(13.875), kslDv(14.250), kslDv(14.625), kslDv(15.000),

      # OCT 6
      kslDv(0.000),  kslDv(6.000),  kslDv(9.000),  kslDv(10.875),
      kslDv(12.000), kslDv(13.125), kslDv(13.875), kslDv(14.625),
      kslDv(15.000), kslDv(15.750), kslDv(16.125), kslDv(16.500),
      kslDv(16.875), kslDv(17.250), kslDv(17.625), kslDv(18.000),

      # OCT 7
      kslDv(0.000),  kslDv(9.000),  kslDv(12.000), kslDv(13.875),
      kslDv(15.000), kslDv(16.125), kslDv(16.875), kslDv(17.625),
      kslDv(18.000), kslDv(18.750), kslDv(19.125), kslDv(19.500),
      kslDv(19.875), kslDv(20.250), kslDv(20.625), kslDv(21.000)
    ] of UInt32

    KSL_SHIFT = [31, 1, 2, 0] of UInt8

    # Sustain level table (3dB per step)
    # 0 - 15: 0, 3, 6, 9, 12, 15, 18, 21, 24, 27, 30, 33, 36, 39, 42, 93 (dB)
    SL_TAB = [
      (0 * (2.0 / ENV_STEP)).to_u32!, (1 * (2.0 / ENV_STEP)).to_u32!, (2 * (2.0 / ENV_STEP)).to_u32!,
      (3 * (2.0 / ENV_STEP)).to_u32!, (4 * (2.0 / ENV_STEP)).to_u32!, (5 * (2.0 / ENV_STEP)).to_u32!,
      (6 * (2.0 / ENV_STEP)).to_u32!, (7 * (2.0 / ENV_STEP)).to_u32!, (8 * (2.0 / ENV_STEP)).to_u32!,
      (9 * (2.0 / ENV_STEP)).to_u32!, (10 * (2.0 / ENV_STEP)).to_u32!, (11 * (2.0 / ENV_STEP)).to_u32!,
      (12 * (2.0 / ENV_STEP)).to_u32!, (13 * (2.0 / ENV_STEP)).to_u32!, (14 * (2.0 / ENV_STEP)).to_u32!,
      (31 * (2.0 / ENV_STEP)).to_u32!
    ] of UInt32

    RATE_STEPS = 8
    EG_INC = [
      # cycle: 0  1  2  3  4  5  6  7
               0, 1, 0, 1, 0, 1, 0, 1, # rates 00..12 0 (increment by 0 or 1)
               0, 1, 0, 1, 1, 1, 0, 1, # rates 00..12 1
               0, 1, 1, 1, 0, 1, 1, 1, # rates 00..12 2
               0, 1, 1, 1, 1, 1, 1, 1, # rates 00..12 3

               1, 1, 1, 1, 1, 1, 1, 1, # rate 13 0 (increment by 1)
               1, 1, 1, 2, 1, 1, 1, 2, # rate 13 1
               1, 2, 1, 2, 1, 2, 1, 2, # rate 13 2
               1, 2, 2, 2, 1, 2, 2, 2, # rate 13 3

               2, 2, 2, 2, 2, 2, 2, 2, # rate 14 0 (increment by 2)
               2, 2, 2, 4, 2, 2, 2, 4, # rate 14 1
               2, 4, 2, 4, 2, 4, 2, 4, # rate 14 2
               2, 4, 4, 4, 2, 4, 4, 4, # rate 14 3

               4, 4, 4, 4, 4, 4, 4, 4, # rates 15 0, 15 1, 15 2, 15 3 (increment by 4)
               8, 8, 8, 8, 8, 8, 8, 8, # rates 15 2, 15 3 for attack
               0, 0, 0, 0, 0, 0, 0, 0  # infinity rates for attack and decay(s)
    ] of UInt8

    EG_RATE_SELECT = [
      # 16 infinite time rates
      14u8 * 8, 14u8 * 8, 14u8 * 8, 14u8 * 8, 14u8 * 8, 14u8 * 8, 14u8 * 8, 14u8 * 8,
      14u8 * 8, 14u8 * 8, 14u8 * 8, 14u8 * 8, 14u8 * 8, 14u8 * 8, 14u8 * 8, 14u8 * 8,

      # Rates 00-12
      0u8 * 8, 1u8 * 8, 2u8 * 8, 3u8 * 8,
      0u8 * 8, 1u8 * 8, 2u8 * 8, 3u8 * 8,
      0u8 * 8, 1u8 * 8, 2u8 * 8, 3u8 * 8,
      0u8 * 8, 1u8 * 8, 2u8 * 8, 3u8 * 8,
      0u8 * 8, 1u8 * 8, 2u8 * 8, 3u8 * 8,
      0u8 * 8, 1u8 * 8, 2u8 * 8, 3u8 * 8,
      0u8 * 8, 1u8 * 8, 2u8 * 8, 3u8 * 8,
      0u8 * 8, 1u8 * 8, 2u8 * 8, 3u8 * 8,
      0u8 * 8, 1u8 * 8, 2u8 * 8, 3u8 * 8,
      0u8 * 8, 1u8 * 8, 2u8 * 8, 3u8 * 8,
      0u8 * 8, 1u8 * 8, 2u8 * 8, 3u8 * 8,
      0u8 * 8, 1u8 * 8, 2u8 * 8, 3u8 * 8,
      0u8 * 8, 1u8 * 8, 2u8 * 8, 3u8 * 8,

      # Rate 13
      4u8 * 8, 5u8 * 8, 6u8 * 8, 7u8 * 8,

      # Rate 14
      8u8 * 8, 9u8 * 8, 10u8 * 8, 11u8 * 8,

      # Rate 15
      12u8 * 8, 12u8 * 8, 12u8 * 8, 12u8 * 8,

      # 16 dummy rates (same as 15 3)
      12u8 * 8, 12u8 * 8, 12u8 * 8, 12u8 * 8, 12u8 * 8, 12u8 * 8, 12u8 * 8, 12u8 * 8,
      12u8 * 8, 12u8 * 8, 12u8 * 8, 12u8 * 8, 12u8 * 8, 12u8 * 8, 12u8 * 8, 12u8 * 8
    ] of UInt8

    # Rate  0,    1,    2,    3,   4,   5,   6,  7,  8,  9,  10, 11, 12, 13, 14, 15 */
    # Shift 12,   11,   10,   9,   8,   7,   6,  5,  4,  3,  2,  1,  0,  0,  0,  0  */
    # Mask  4095, 2047, 1023, 511, 255, 127, 63, 31, 15, 7,  3,  1,  0,  0,  0,  0  */
    EG_RATE_SHIFT = [
      # 16 infinite time rates
      0, 0, 0, 0, 0, 0, 0, 0,
      0, 0, 0, 0, 0, 0, 0, 0,

      # Rates 00-12
      12, 12, 12, 12,
      11, 11, 11, 11,
      10, 10, 10, 10,
      9, 9, 9, 9,
      8, 8, 8, 8,
      7, 7, 7, 7,
      6, 6, 6, 6,
      5, 5, 5, 5,
      4, 4, 4, 4,
      3, 3, 3, 3,
      2, 2, 2, 2,
      1, 1, 1, 1,
      0, 0, 0, 0,

      # Rate 13
      0, 0, 0, 0,

      # Rate 14
      0, 0, 0, 0,

      # Rate 15
      0, 0, 0, 0,

      # 16 fake rates (same as 15 3)
      0, 0, 0, 0, 0, 0, 0, 0,
      0, 0, 0, 0, 0, 0, 0, 0
    ] of UInt8

    # Multiple table
    MUL_TAB = [
      # 1/2, 1, 2, 3, 4, 5, 6, 7, 8, 9,10,10,12,12,15,15
      (0.50 * 2).to_u8!, (1.00 * 2).to_u8!, (2.00 * 2).to_u8!, (3.00 * 2).to_u8!, (4.00 * 2).to_u8!,
      (5.00 * 2).to_u8!, (6.00 * 2).to_u8!, (7.00 * 2).to_u8!, (8.00 * 2).to_u8!, (9.00 * 2).to_u8!,
      (10.00 * 2).to_u8!, (10.00 * 2).to_u8!, (12.00 * 2).to_u8!, (12.00 * 2).to_u8!, (15.00 * 2).to_u8!,
      (15.00 * 2).to_u8!
    ] of UInt8

    # TL_TAB_LEN is calculated as:
    #   12 - sinus amplitude bits     (Y axis)
    #   2  - sinus sign bit           (Y axis)
    #   TL_RES_LEN - sinus resolution (X axis)
    TL_TAB_LEN = 12 * 2 * TL_RES_LEN
    ENV_QUIET = TL_TAB_LEN >> 4

    # LFO Amplitude Modulation table (verified on real YM3812)
    #
    # 27 output levels (triangle waveform); 1 level takes one of: 192, 256 or 448 samples
    #
    # Length: 210 elements.
    #
    # Each of the elements has to be repeated exactly 64 times (on 64
    # consecutive samples).  The whole table takes: 64 * 210 = 13440 samples.
    #
    # When AM = 1 data is used directly
    # When AM = 0 data is divided by 4 before being used (losing precision is important)

    LFO_AM_TAB_ELEMENTS = 210
    LFO_AM_TABLE = [
      0, 0, 0, 0, 0, 0, 0,
      1, 1, 1, 1,
      2, 2, 2, 2,
      3, 3, 3, 3,
      4, 4, 4, 4,
      5, 5, 5, 5,
      6, 6, 6, 6,
      7, 7, 7, 7,
      8, 8, 8, 8,
      9, 9, 9, 9,
      10, 10, 10, 10,
      11, 11, 11, 11,
      12, 12, 12, 12,
      13, 13, 13, 13,
      14, 14, 14, 14,
      15, 15, 15, 15,
      16, 16, 16, 16,
      17, 17, 17, 17,
      18, 18, 18, 18,
      19, 19, 19, 19,
      20, 20, 20, 20,
      21, 21, 21, 21,
      22, 22, 22, 22,
      23, 23, 23, 23,
      24, 24, 24, 24,
      25, 25, 25, 25,
      26, 26, 26,
      25, 25, 25, 25,
      24, 24, 24, 24,
      23, 23, 23, 23,
      22, 22, 22, 22,
      21, 21, 21, 21,
      20, 20, 20, 20,
      19, 19, 19, 19,
      18, 18, 18, 18,
      17, 17, 17, 17,
      16, 16, 16, 16,
      15, 15, 15, 15,
      14, 14, 14, 14,
      13, 13, 13, 13,
      12, 12, 12, 12,
      11, 11, 11, 11,
      10, 10, 10, 10,
      9, 9, 9, 9,
      8, 8, 8, 8,
      7, 7, 7, 7,
      6, 6, 6, 6,
      5, 5, 5, 5,
      4, 4, 4, 4,
      3, 3, 3, 3,
      2, 2, 2, 2,
      1, 1, 1, 1
    ] of UInt8

    # LFO Phase Modulation table (verified on real YM3812)
    LFO_PM_TABLE = [
      # FNUM2/FNUM = 00 0xxxxxxx (0x0000)
      0, 0, 0, 0, 0, 0, 0, 0, # LFO PM depth = 0
      0, 0, 0, 0, 0, 0, 0, 0, # LFO PM depth = 1

      # FNUM2/FNUM = 00 1xxxxxxx (0x0080)
      0, 0, 0, 0, 0, 0, 0, 0, # LFO PM depth = 0
      1, 0, 0, 0, -1, 0, 0, 0, # LFO PM depth = 1

      # FNUM2/FNUM = 01 0xxxxxxx (0x0100)
      1, 0, 0, 0, -1, 0, 0, 0, # LFO PM depth = 0
      2, 1, 0, -1, -2, -1, 0, 1, # LFO PM depth = 1

      # FNUM2/FNUM = 01 1xxxxxxx (0x0180)
      1, 0, 0, 0, -1, 0, 0, 0, # LFO PM depth = 0
      3, 1, 0, -1, -3, -1, 0, 1, # LFO PM depth = 1

      # FNUM2/FNUM = 10 0xxxxxxx (0x0200)
      2, 1, 0, -1, -2, -1, 0, 1, # LFO PM depth = 0
      4, 2, 0, -2, -4, -2, 0, 2, # LFO PM depth = 1

      # FNUM2/FNUM = 10 1xxxxxxx (0x0280)
      2, 1, 0, -1, -2, -1, 0, 1, # LFO PM depth = 0
      5, 2, 0, -2, -5, -2, 0, 2, # LFO PM depth = 1

      # FNUM2/FNUM = 11 0xxxxxxx (0x0300)
      3, 1, 0, -1, -3, -1, 0, 1, # LFO PM depth = 0
      6, 3, 0, -3, -6, -3, 0, 3, # LFO PM depth = 1

      # FNUM2/FNUM = 11 1xxxxxxx (0x0380)
      3, 1, 0, -1, -3, -1, 0, 1, # LFO PM depth = 0
      7, 3, 0, -3, -7, -3, 0, 3 # LFO PM depth = 1
    ] of Int32

    ###
    ### Classes
    ###

    class OPLSlot
      property ar   : UInt32 = 0u32 # Attack rate: AR<<2
      property dr   : UInt32 = 0u32 # Decay rate:  DR<<2
      property rr   : UInt32 = 0u32 # Release rate:RR<<2
      property ksrC : UInt8  = 0u8  # Key scale rate (Remi: this was the capital KSR in the C code)
      property ksl  : UInt8  = 0u8  # Keyscale level
      property ksr  : UInt8  = 0u8  # Key scale rate: kcode>>KSR
      property mul  : UInt8  = 0u8  # Multiple: MUL_TAB[ml]

      # Phase Generator
      property count : UInt32 = 0u32 # Frequency counter
      property incr  : UInt32 = 0u32 # Frequency counter step
      property fb    : UInt8 = 0u8   # Feedback shift value
      property connect1 : Pointer(Int32) = Pointer(Int32).null # Slot1 output pointer
      property op1Out   : Array(Int32) = [0, 0] # Slot1 output for feedback
      property con   : UInt8 = 0u8   # Connection (algorithm) type

      # Envelope Generator
      property egType  : UInt8  = 0u8  # Percussive/non-percussive mode
      property state   : UInt8  = 0u8  # Phase type
      property tl      : UInt32 = 0u32 # Total level: TL << 2
      property tll     : Int32  = 0    # Adjusted now TL
      property volume  : Int32  = 0    # Envelope counter
      property sl      : UInt32 = 0u32 # Sustain level: sl_tab[SL]
      property egShAr  : UInt8  = 0u8  # Attack state
      property egSelAr : UInt8  = 0u8  # Attack state
      property egShDr  : UInt8  = 0u8  # Decay state
      property egSelDr : UInt8  = 0u8  # Decay state
      property egShRr  : UInt8  = 0u8  # Release state
      property egSelRr : UInt8  = 0u8  # Release state
      property key     : UInt32 = 0u32 # 0 = key off, >0 = key on

      # LFO
      property amMask : UInt32 = 0u32 # LFO Amplitude Modulation enable mask
      property vib    : UInt8  = 0u8  # LFO Phase Modulation enable flag (active high)

      # Waveform select
      property wavetable : UInt16 = 0u16

      def initialize
      end
    end

    class OPLCh
      property slot : Array(OPLSlot)

      # Phase generator state
      property blockFnum : UInt32 = 0u32 # Block + fnum
      property fc        : UInt32 = 0u32 # Frequency increment base
      property kslBase   : UInt32 = 0u32 # KeyScaleLevel base step
      property kcode     : UInt8  = 0u8  # Key code (for key scaling)
      property muted     : UInt8  = 0u8

      def initialize
        @slot = Array(OPLSlot).new(2) { |_| OPLSlot.new }
      end

      @[AlwaysInline]
      def slot1 : OPLSlot
        @slot.get!(SLOT1)
      end

      @[AlwaysInline]
      def slot2 : OPLSlot
        @slot.get!(SLOT2)
      end
    end

    ###
    ### Fields
    ###

    @pch : Array(OPLCh) # OPL/OPL2 chips have 9 channels
    @muteSpc : Array(UInt8) = Array(UInt8).new(6, 0u8) # Mute special: 5 rhythm + 1 DeltaT channel

    @egCount         : UInt32 = 0u32 # Global envelope generator counter
    @egTimer         : UInt32 = 0u32 # Global envelope generator counter works at frequency = chipclock/72
    @egTimerAdd      : UInt32 = 0u32 # Step of @egTimer
    @egTimerOverflow : UInt32 = 0u32 # Envelope generator timer overflows every 1 sample (on real chip)

    @rhythm : UInt8 = 0u8 # Rhythm mode

    @fnTab : Array(UInt32) = Array(UInt32).new(1024, 0u32) # fnumber->increment counter

    @tlTab : Array(Int32) = Array(Int32).new(TL_TAB_LEN, 0)
    @sinTab : Array(UInt32) = Array(UInt32).new(SIN_LEN * 4, 0u32)

    # LFO
    @lfoAm           : UInt32 = 0u32
    @lfoPm           : Int32  = 0
    @lfoAmDepth      : UInt8  = 0u8
    @lfoPmDepthRange : UInt8  = 0u8
    @lfoAmCount      : UInt32 = 0u32
    @lfoAmInc        : UInt32 = 0u32
    @lfoPmCount      : UInt32 = 0u32
    @lfoPmInc        : UInt32 = 0u32

    # Noise generation
    @noiseRng : UInt32 = 0u32 # 23 bit noise shift register
    @noiseP   : UInt32 = 0u32 # Current noise phase
    @noiseF   : UInt32 = 0u32 # Current noise period

    @waveSel : UInt8 = 0u8 # Waveform select enable flag

    @timer   : Array(UInt32) = [0u32, 0u32] # Timer counters
    @timerOn : Array(UInt32) = [0u32, 0u32] # Timer enable

    @irqHandler    : OPLIrqHandler?
    @irqParam      : Yuno::AbstractEmulator?
    @updateHandler : OPLUpdateHandler?
    @updateParam   : Yuno::AbstractEmulator?

    @type       : UInt8 = 0u8 # Chip type
    @address    : UInt8 = 0u8 # Address register
    @status     : UInt8 = 0u8 # Status flag
    @statusMask : UInt8 = 0u8 # Status mask
    @mode       : UInt8 = 0u8 # Register 8 : CSM, note select, etc.

    @clock    : UInt32  = 0u32 # Primary clock (Hz)
    @rate     : UInt32  = 0u32 # Sampling rate (Hz)
    @freqBase : Float64 = 0.0  # Timer base time (sampling time)

    @phaseModulation : Array(Int32) = [0] # Phase modulation input (slot 2)
    @output : Array(Int32) = [0]

    macro slot7_1
      (@pch.get!(7).slot1)
    end

    macro slot7_2
      (@pch.get!(7).slot2)
    end

    macro slot8_1
      (@pch.get!(8).slot1)
    end

    macro slot8_2
      (@pch.get!(8).slot2)
    end

    protected def withChannels(&) : Nil
      (9 * 2).times do |i|
        ch = @pch.get!(i >> 1)
        op = ch.slot.get!(i & 1)
        yield ch, op
      end
    end

    @[AlwaysInline]
    protected def spcMuted?(idx : Int) : Bool
      @muteSpc.get!(idx) != 0
    end

    @[AlwaysInline]
    protected def statusSet(flag : Int) : Nil
      # Set status flag
      @status |= flag
      unless Yuno.bitflag?(@status, 0x80)
        if Yuno.bitflag?(@status, @statusMask)
          # IRQ On
          @status |= 0x80

          # Callback user interrupt handler (IRQ is off => on)
          @irqHandler.try(&.call(@irqParam.not_nil!, 1))
        end
      end
    end

    @[AlwaysInline]
    protected def statusReset(flag : Int) : Nil
      # Reset status flag
      @status &= ~flag
      if Yuno.bitflag?(@status, 0x80)
        unless Yuno.bitflag?(@status, @statusMask)
          # IRQ On
          @status &= 0x7F

          # Callback user interrupt handler (IRQ is on => off)
          @irqHandler.try(&.call(@irqParam.not_nil!, 0))
        end
      end
    end

    @[AlwaysInline]
    protected def statusMaskSet(flag : Int) : Nil
      @statusMask = flag

      # IRQ handling check
      statusSet(0)
      statusReset(0)
    end

    @[AlwaysInline]
    protected def advanceLfo : Nil
      @lfoAmCount = @lfoAmCount &+ @lfoAmInc
      if @lfoAmCount >= (LFO_AM_TAB_ELEMENTS.to_u32! << LFO_SH)
        @lfoAmCount = @lfoAmCount &- (LFO_AM_TAB_ELEMENTS.to_u32! << LFO_SH)
      end

      tmp : UInt32 = LFO_AM_TABLE[@lfoAmCount >> LFO_SH].to_u32!

      @lfoAm = if @lfoAmDepth != 0
                 tmp
               else
                 tmp >> 2
               end
      @lfoPmCount = @lfoPmCount &+ @lfoPmInc
      @lfoPm = ((@lfoPmCount.to_i32! >> LFO_SH) & 7) | @lfoPmDepthRange
    end

    # @[AlwaysInline]
    # protected def refreshEg : Nil
    #   # Envelope generator
    #   withChannels do |ch, op|
    #     case op.state
    #     when EG_ATT # Attack phase
    #       unless Yuno.bitflag?(@egCount, (1 << op.egShAr) - 1)
    #         newVol : Int32 = op.volume + (((~op.volume) * EG_INC[op.egSelAr + ((@egCount >> op.egShAr) & 7)]) >> 3)
    #         if newVol <= MIN_ATT_INDEX
    #           op.volume = MIN_ATT_INDEX
    #           op.state = EG_DEC
    #         end
    #       end
    #     end
    #   end
    # end

    @[AlwaysInline]
    protected def advance : Nil
      @egTimer = @egTimer &+ @egTimerAdd

      while @egTimer >= @egTimerOverflow
        @egTimer = @egTimer &- @egTimerOverflow
        @egCount += 1

        withChannels do |ch, op|
          # Envelope generator
          case op.state
          when EG_ATT # Attack phase
            unless Yuno.bitflag?(@egCount, (1 << op.egShAr) - 1)
              op.volume += ((~op.volume) * EG_INC[op.egSelAr + ((@egCount >> op.egShAr) & 7)]) >> 3
              if op.volume <= MIN_ATT_INDEX
                op.volume = MIN_ATT_INDEX
                op.state = EG_DEC
              end
            end

          when EG_DEC # Decay phase
            unless Yuno.bitflag?(@egCount, (1 << op.egShDr) - 1)
              op.volume += EG_INC[op.egSelDr + ((@egCount >> op.egShDr) & 7)]
              op.state = EG_SUS if op.volume >= op.sl
            end

          when EG_SUS # Sustain phase
            # This is important behaviour: One can change
            # percussive/non-percussive modes on the fly and the chip will remain
            # in sustain phase - verified on real YM3812

            if op.egType == 0 # Don't do anything for non-percussive mode
              # During the sustain phase the chip adds release rate (in
              # percussive mode)
              unless Yuno.bitflag?(@egCount, (1 << op.egShRr) - 1)
                op.volume += EG_INC[op.egSelRr + ((@egCount >> op.egShRr) & 7)]
                op.volume = MAX_ATT_INDEX if op.volume >= MAX_ATT_INDEX
              end
            end

          when EG_REL # Release phase
            unless Yuno.bitflag?(@egCount, (1 << op.egShRr) - 1)
              op.volume += EG_INC[op.egSelRr + ((@egCount >> op.egShRr) & 7)]
              if op.volume >= MAX_ATT_INDEX
                op.volume = MAX_ATT_INDEX
                op.state = EG_OFF
              end
            end
          end # case op.state
        end # withChannels
      end # while @egTimer >= @egOverflow

      block : UInt8
      blockFnum : UInt32 = 0
      fnumLfo : UInt32 = 0
      lfoFnTableIndexOffset : Int32 = 0

      withChannels do |ch, op|
        # Phase generator
        if op.vib != 0
          blockFnum = ch.blockFnum
          fnumLfo = (blockFnum & 0x0380) >> 7
          lfoFnTableIndexOffset = LFO_PM_TABLE[@lfoPm + 16 * fnumLfo]

          if lfoFnTableIndexOffset != 0
            # LFO phase modulation is active
            blockFnum += lfoFnTableIndexOffset
            block = ((blockFnum & 0x1C00) >> 10).to_u8!
            op.count = op.count &+ ((@fnTab.get!(blockFnum & 0x03FF) >> (7 - block)) * op.mul)
          else
            # LFO phase modulation = zero
            op.count = op.count &+ op.incr
          end
        else
          # LFO phase modulation disabled for this operator
          op.count = op.count &+ op.incr
        end
      end

      # The Noise Generator of the YM3812 is 23-bit shift register.  Period is
      # equal to 2^23-2 samples.  Register works at sampling frequency of the
      # chip, so output can change on every sample.
      #
      # Output of the register and input to the bit 22 is:
      #   bit0 XOR bit14 XOR bit15 XOR bit22
      #
      # Simply use bit 22 as the noise output.
      @noiseP = @noiseP &+ @noiseF
      i : UInt32 = @noiseP >> FREQ_SH # number of events (shifts of the shift register)
      @noiseP &= FREQ_MASK

      until i == 0
        # We use a trick here (and use bit 0 as the noise output).  The
        # difference is only that the noise bit changes one step ahead. This
        # doesn't matter since we don't know what is real state of the noise_rng
        # after the reset.
        @noiseRng ^= 0x800302 if Yuno.bitflag?(@noiseRng, 1)
        @noiseRng >>= 1
        i -= 1
      end
    end

    @[AlwaysInline]
    protected def opCalc(phase : UInt32, env : UInt32, pm : Int32, waveTab : UInt32) : Int32
      idx = (env << 4) &+ @sinTab[waveTab + ((((phase & (~FREQ_MASK)) &+ (pm << 16)).to_i32! >> FREQ_SH) & SIN_MASK)]
      if idx >= TL_TAB_LEN
        0
      else
        @tlTab.get!(idx)
      end
    end

    @[AlwaysInline]
    protected def opCalc1(phase : UInt32, env : UInt32, pm : Int32, waveTab : UInt32) : Int32
      idx : UInt32 = (env << 4) &+ @sinTab[waveTab + ((((phase & (~FREQ_MASK)) &+ pm).to_i32! >> FREQ_SH) & SIN_MASK)]
      if idx >= TL_TAB_LEN
        0
      else
        @tlTab.get!(idx)
      end
    end

    @[AlwaysInline]
    protected def volumeCalc(op : OPLSlot) : UInt32
      (op.tll + op.volume.to_u32! + (@lfoAm & op.amMask)).to_u32!
    end

    @[AlwaysInline]
    protected def calcCh(ch : OPLCh) : Nil
      return if ch.muted != 0

      @phaseModulation.put!(0, 0)

      # Slot 1
      slot = ch.slot1
      env : UInt32 = volumeCalc(slot)
      output : Int32 = slot.op1Out.get!(0) + slot.op1Out.get!(1)
      slot.op1Out.put!(0, slot.op1Out.get!(1))
      slot.connect1.value += slot.op1Out.get!(0)
      slot.op1Out.put!(1, 0)
      if env < ENV_QUIET
        output = 0 if slot.fb == 0
        slot.op1Out.put!(1, opCalc1(slot.count, env, (output << slot.fb),
                                    slot.wavetable.to_u32!))
      end

      # Slot 2
      slot = ch.slot2
      env = volumeCalc(slot)
      if env < ENV_QUIET
        @output.incf!(0, opCalc(slot.count, env, @phaseModulation.get!(0),
                                slot.wavetable.to_u32!))
      end
    end

    ## Operators used in the rhythm sounds generation process:
    ##
    ## Envelope Generator:
    ##
    ## channel  operator  register number   Bass  High  Snare Tom  Top
    ## / slot   number    TL ARDR SLRR Wave Drum  Hat   Drum  Tom  Cymbal
    ##  6 / 0   12        50  70   90   f0  +
    ##  6 / 1   15        53  73   93   f3  +
    ##  7 / 0   13        51  71   91   f1        +
    ##  7 / 1   16        54  74   94   f4              +
    ##  8 / 0   14        52  72   92   f2                    +
    ##  8 / 1   17        55  75   95   f5                          +
    ##
    ## Phase Generator:
    ##
    ## channel  operator  register number   Bass  High  Snare Tom  Top
    ## / slot   number    MULTIPLE          Drum  Hat   Drum  Tom  Cymbal
    ##  6 / 0   12        30                +
    ##  6 / 1   15        33                +
    ##  7 / 0   13        31                      +     +           +
    ##  7 / 1   16        34                -----  n o t  u s e d -----
    ##  8 / 0   14        32                                  +
    ##  8 / 1   17        35                      +                 +
    ##
    ## channel  operator  register number   Bass  High  Snare Tom  Top
    ## number   number    BLK/FNUM2 FNUM    Drum  Hat   Drum  Tom  Cymbal
    ##    6     12,15     B6        A6      +
    ##
    ##    7     13,16     B7        A7            +     +           +
    ##
    ##    8     14,17     B8        A8            +           +     +

    # Calculates rhythm
    @[AlwaysInline]
    protected def calcRh(ch : OPLCh, noise : UInt32) : Nil
      # Remi: Rather than making ch a Slice(OPLCh) or a Pointer(OPLCh), I've
      # changed it to just a straight up OPLCh reference.  The only calls to
      # this use @pch[0], and then the original code always indexed ch[6] below.
      # To save two indexes, we can just get @pch[6] from whatever calls this.

      ##
      ## Bass Drum (verified on real YM3812):
      ##
      ## - Depends on the channel 6 'connect' register:
      ##   * When connect = 0 it works the same as in normal (non-rhythm) mode
      ##     (op1->op2->out)
      ##   * When connect = 1 _only_ operator 2 is present on output (op2->out),
      ##     operator 1 is ignored
      ## - Output sample always is multiplied by 2.
      ##

      @phaseModulation.put!(0, 0)

      # Slot 1
      slot : OPLSlot = ch.slot1
      env : UInt32 = volumeCalc(slot)
      output : Int32 = slot.op1Out.get!(0) + slot.op1Out.get!(1)
      slot.op1Out.put!(0, slot.op1Out.get!(1))

      @phaseModulation.put!(0, slot.op1Out.get!(0)) if slot.con == 0

      slot.op1Out.put!(1, 0)
      if env < ENV_QUIET
        output = 0 if slot.fb == 0
        slot.op1Out.put!(1, opCalc1(slot.count, env, output << slot.fb,
                                    slot.wavetable.to_u32!))
      end

      # Slot 2
      slot = ch.slot2
      env = volumeCalc(slot)
      if env < ENV_QUIET && !spcMuted?(0)
        @output.incf!(0, opCalc(slot.count, env, @phaseModulation.get!(0), slot.wavetable.to_u32!) * 2)
      end

      ##
      ## Phase generation is based on:
      ##
      ## * HH (13) channel 7->slot 1 combined with channel 8->slot 2 (same
      ##   combination as TOP CYMBAL but different output phases)
      ## * SD (16) channel 7->slot 1
      ## * TOM (14) channel 8->slot 1 */
      ## * TOP (17) channel 7->slot 1 combined with channel 8->slot 2 (same
      ##   combination as HIGH HAT but different output phases)
      ##
      ## Envelope generation based on:
      ##
      ## * HH  channel 7->slot1
      ## * SD  channel 7->slot2
      ## * TOM channel 8->slot1
      ## * TOP channel 8->slot2
      ##

      # The following formulas can be well optimized.  I leave them in direct
      # form for now (in case I've missed something).

      # High Hat (verified on real YM3812)
      env = volumeCalc(slot7_1)
      if env < ENV_QUIET && !spcMuted?(4)
        # High hat phase generation:
        #   Phase = d0 or 234 (based on frequency only)
        #   Phase = 34 or 2d0 (based on noise)

        # Base frequency derived from operator 1 in channel 7.
        bit7 : UInt8 = (((slot7_1.count >> FREQ_SH) >> 7) & 1).to_u8!
        bit3 : UInt8 = (((slot7_1.count >> FREQ_SH) >> 3) & 1).to_u8!
        bit2 : UInt8 = (((slot7_1.count >> FREQ_SH) >> 2) & 1).to_u8!
        res1 : UInt8 = (bit2 ^ bit7) | bit3

        # When res1 = 0 phase = 0x000 | 0xd0
        # When res1 = 1 phase = 0x200 | (0xd0>>2)
        phase : UInt32 = res1 != 0 ? (0x200_u32 | (0xD0_u32 >> 2)) : 0xD0_u32

        # Enable gate based on frequency of operator 2 in channel 8.
        bit5e : UInt8 = (((slot8_2.count >> FREQ_SH) >> 5) & 1).to_u8!
        bit3e : UInt8 = (((slot8_2.count >> FREQ_SH) >> 3) & 1).to_u8!
        res2 : UInt8 = bit3e ^ bit5e

        # When res2 = 0 pass the phase from calculation above (res1)
        # When res2 = 1 phase = 0x200 | (0xd0>>2)
        unless res2 == 0
          phase = (0x200_u32 | (0xD0_u32 >> 2))
        end

        if Yuno.bitflag?(phase, 0x200)
          # When phase & 0x200 is set and noise=1 then phase = 0x200|0xd0
          # When phase & 0x200 is set and noise=0 then phase = 0x200|(0xd0>>2), ie no change
          phase = 0x200_u32 | 0xD0 if noise != 0
        else
          # When phase & 0x200 is clear and noise=1 then phase = 0xd0>>2
          # When phase & 0x200 is clear and noise=0 then phase = 0xd0, ie no change
          phase = 0xD0_u32 >> 2 if noise != 0
        end

        @output.incf!(0, opCalc(phase << FREQ_SH, env, 0, slot7_1.wavetable) * 2)
      end # if env < ENV_QUIET && @muteSpc[4] == 0

      # Snare Drum (verified on real YM3812)
      env = volumeCalc(slot7_2)
      if env < ENV_QUIET && !spcMuted?(1)
        bit8 : UInt8 = (((slot7_1.count >> FREQ_SH) >> 8) & 1).to_u8!

        # When bit8 = 0 phase = 0x100
        # When bit8 = 1 phase = 0x200
        phase = bit8 != 0 ? 0x200_u32 : 0x100_u32

        # Noise bit XOR'es phase by 0x100
        # When noisebit = 0 pass the phase from calculation above
        # When noisebit = 1 phase ^= 0x100
        # In other words: phase ^= (noisebit<<8)
        phase ^= 0x100 if noise != 0

        @output.incf!(0, opCalc(phase << FREQ_SH, env, 0, slot7_2.wavetable) * 2)
      end

      # Tom Tom (verified on real YM3812)
      env = volumeCalc(slot8_1)
      if env < ENV_QUIET && !spcMuted?(2)
        @output.incf!(0, opCalc(slot8_1.count, env, 0, slot8_1.wavetable) * 2)
      end

      # Top Cymbal (verified on real YM3812)
      env = volumeCalc(slot8_2)
      if env < ENV_QUIET && !spcMuted?(3)
        # Base frequency derived from operator 1 in channel 7.
        bit7 = (((slot7_1.count >> FREQ_SH) >> 7) & 1).to_u8!
        bit3 = (((slot7_1.count >> FREQ_SH) >> 3) & 1).to_u8!
        bit2 = (((slot7_1.count >> FREQ_SH) >> 2) & 1).to_u8!
        res1 = (bit2 ^ bit7) | bit3

        # When res1 = 0 phase = 0x000 | 0x100
        # When res1 = 1 phase = 0x200 | 0x100
        phase = res1 != 0 ? 0x300_u32 : 0x100_u32

        # Enable gate based on frequency of operator 2 in channel 8.
        bit5e = (((slot8_2.count >> FREQ_SH) >> 5) & 1).to_u8!
        bit3e = (((slot8_2.count >> FREQ_SH) >> 3) & 1).to_u8!
        res2 = bit3e ^ bit5e

        # When res2 = 0 pass the phase from calculation above (res1)
        # When res2 = 1 phase = 0x200 | 0x100
        phase = 0x300_u32 if res2 != 0

        @output.incf!(0, opCalc(phase << FREQ_SH, env, 0, slot8_2.wavetable) * 2)
      end
    end

    # Generic table initialization
    protected def initTables : Nil
      # Nom nom :drool:
      n : Int32 = 0
      o : Float64 = 0.0
      m : Float64 = 0.0

      TL_RES_LEN.times do |x|
        m = ((1 << 16) / (2.0 ** ((x + 1) * (ENV_STEP / 4.0) / 8.0))).floor

        # We never reach (1<<16) here due to the (x+1).  Result fits within 16
        # bits at maximum.

        n = m.to_i32! # 16 bits here
        n >>= 4       # 12 bits here
        n = if Yuno.bitflag?(n, 1) # Round to nearest
              (n >> 1) + 1
            else
              n >> 1
            end
                      # 11 bits here (rounded)
        n <<= 1       # 12 bits here (as in real chip)
        @tlTab[x * 2    ] = n
        @tlTab[x * 2 + 1] = -@tlTab[x * 2]

        (1...12).each do |i|
          @tlTab[x * 2     + i * 2 * TL_RES_LEN] =  @tlTab[x * 2] >> i
          @tlTab[x * 2 + 1 + i * 2 * TL_RES_LEN] = -@tlTab[x * 2 + i * 2 * TL_RES_LEN]
        end
      end

      SIN_LEN.times do |i|
        # Non-standard sinus
        m = Math.sin(((i * 2) + 1) * Math::PI / SIN_LEN) # Checked against real chip

        # We never reach zero here due to ((i*2)+1)

        # Convert to decibels
        o = if m > 0
              8 * Math.log(1.0 / m) / Math.log(2.0)
            else
              8 * Math.log(-1.0 / m) / Math.log(2.0)
            end

        o = o / (ENV_STEP / 4)

        n = (2.0 * o).to_i32!
        n = if Yuno.bitflag?(n, 1) # Round to nearest
              (n >> 1) + 1
            else
              n >> 1
            end
        @sinTab[i] = (n * 2 + (m >= 0 ? 0 : 1)).to_u32!
      end

      SIN_LEN.times do |i|
        # waveform 1:  __      __
        #             /  \____/  \____
        # Output only first half of the sinus waveform (positive one)

        @sinTab[1 * SIN_LEN + i] = if Yuno.bitflag?(i, 1 << (SIN_BITS - 1))
                                     TL_TAB_LEN.to_u32!
                                   else
                                     @sinTab[i]
                                   end

        # waveform 2:  __  __  __  __
        #             /  \/  \/  \/  \
        # abs(sin)

        @sinTab[2 * SIN_LEN + i] = @sinTab[i & (SIN_MASK >> 1)]

        # waveform 3:  _   _   _   _
        #             / |_/ |_/ |_/ |_
        # abs(output only first quarter of the sinus waveform)

        @sinTab[3 * SIN_LEN + i] = if Yuno.bitflag?(i, 1 << (SIN_BITS - 2))
                                     TL_TAB_LEN.to_u32!
                                   else
                                     @sinTab[i & (SIN_MASK >> 2)]
                                   end

      end
    end

    protected def initOpl : Nil
      @freqBase = if @rate != 0
                    (@clock / 72.0) / @rate
                  else
                    0.0
                  end
      1024.times do |i|
        # -10 because chip works with 10.10 fixed point, while we use 16.16
        @fnTab[i] = (i.to_f64! * 64 * @freqBase * (1 << (FREQ_SH - 10))).to_u32!
      end

      @pch.each(&.muted = 0)
      6.times { |i| @muteSpc[i] = 0 }

      # Amplitude modulation: 27 output levels (triangle waveform); 1 level
      # takes one of: 192, 256 or 448 samples
      #
      # One entry from LFO_AM_TABLE lasts for 64 samples.
      @lfoAmInc = ((1.0 / 64.0) * (1 << LFO_SH) * @freqBase).to_u32!

      # Vibrato: 8 output levels (triangle waveform); 1 level takes 1024 samples
      @lfoPmInc = ((1.0 / 1024.0) * (1 << LFO_SH) * @freqBase).to_u32!

      # Noise generator: a step takes 1 sample
      @noiseF = ((1 << FREQ_SH) * @freqBase).to_u32!

      @egTimerAdd = ((1 << EG_SH) * @freqBase).to_u32!
      @egTimerOverflow = 1u32 << EG_SH
    end

    @[AlwaysInline]
    protected def keyOn(slot : OPLSlot, keySet : UInt32) : Nil
      if slot.key == 0
        slot.count = 0 # Restart phase generator
        slot.state = EG_ATT # Attack phase
      end
      slot.key |= keySet
    end

    @[AlwaysInline]
    protected def keyOff(slot : OPLSlot, keyClr : UInt32) : Nil
      unless slot.key == 0
        slot.key &= keyClr

        if slot.key == 0
          # Release phase
          slot.state = EG_REL if slot.state > EG_REL
        end
      end
    end

    # Updates phase increment counter of operator (also update the EG rates if
    # necessary).
    @[AlwaysInline]
    protected def calcFcSlot(ch : OPLCh, slot : OPLSlot) : Nil
      # (Frequency) phase increment counter
      slot.incr = ch.fc * slot.mul
      ksr : Int32 = ch.kcode.to_i32! >> slot.ksrC

      if slot.ksr != ksr.to_u8!
        slot.ksr = ksr.to_u8!

        # Calculate envelope generator rates
        if (slot.ar + slot.ksr) < (16 + 62)
          slot.egShAr  = EG_RATE_SHIFT[slot.ar + slot.ksr]
          slot.egSelAr = EG_RATE_SELECT[slot.ar + slot.ksr]
        else
          slot.egShAr  = 0
          slot.egSelAr = 13u8 * RATE_STEPS
        end

        slot.egShDr  = EG_RATE_SHIFT[slot.dr + slot.ksr]
        slot.egSelDr = EG_RATE_SELECT[slot.dr + slot.ksr]
        slot.egShRr  = EG_RATE_SHIFT[slot.rr + slot.ksr]
        slot.egSelRr = EG_RATE_SELECT[slot.rr + slot.ksr]
      end
    end

    # Sets multi, am,vib, EG-TYP, ksrC, mul
    @[AlwaysInline]
    protected def setMul(slot : Int, v : Int) : Nil
      ch = @pch[slot >> 1]
      chSlot = ch.slot.get!(slot & 1)
      chSlot.mul    = MUL_TAB[v & 0x0F]
      chSlot.ksrC   = Yuno.bitflag?(v, 0x10) ? 0u8 : 2u8
      chSlot.egType = (v & 0x20).to_u8!
      chSlot.vib    = (v & 0x40).to_u8!
      chSlot.amMask = Yuno.bitflag?(v, 0x80) ? ~0u32 : 0u32
      calcFcSlot(ch, chSlot)
    end

    # Sets ksl and tl
    @[AlwaysInline]
    protected def setKslTl(slot : Int, v : Int) : Nil
      ch = @pch[slot >> 1]
      chSlot = ch.slot.get!(slot & 1)
      chSlot.ksl = KSL_SHIFT[v >> 6]
      chSlot.tl = (v.to_u32! & 0x3f) << (ENV_BITS - 1 - 7) # 7 bits TL (bit 6 = always 0)
      chSlot.tll = (chSlot.tl + (ch.kslBase >> chSlot.ksl)).to_i32!
    end

    # Sets attack rate and decay rate
    @[AlwaysInline]
    protected def setArDr(slot : Int, v : Int) : Nil
      ch = @pch[slot >> 1]
      chSlot = ch.slot.get!(slot & 1)

      chSlot.ar = (v >> 4) != 0 ? 16u32 + ((v.to_u32! >> 4) << 2) : 0u32
      if (chSlot.ar + chSlot.ksr) < (16 + 62)
        chSlot.egShAr  = EG_RATE_SHIFT[chSlot.ar + chSlot.ksr]
        chSlot.egSelAr = EG_RATE_SELECT[chSlot.ar + chSlot.ksr]
      else
        chSlot.egShAr  = 0
        chSlot.egSelAr = 13u8 * RATE_STEPS
      end

      chSlot.dr = Yuno.bitflag?(v, 0x0F) ? 16u32 + ((v.to_u32! & 0x0F) << 2) : 0u32
      chSlot.egShDr  = EG_RATE_SHIFT[chSlot.dr + chSlot.ksr]
      chSlot.egSelDr = EG_RATE_SELECT[chSlot.dr + chSlot.ksr]
    end

    # Sets sustain level and release rate
    @[AlwaysInline]
    protected def setSlRr(slot : Int, v : Int) : Nil
      ch = @pch[slot >> 1]
      chSlot = ch.slot.get!(slot & 1)
      chSlot.sl = SL_TAB[v >> 4]
      chSlot.rr = Yuno.bitflag?(v, 0x0F) ? 16u32 + ((v.to_u32! & 0x0F) << 2) : 0u32
      chSlot.egShRr  = EG_RATE_SHIFT[chSlot.rr + chSlot.ksr]
      chSlot.egSelRr = EG_RATE_SELECT[chSlot.rr + chSlot.ksr]
    end

    # @[AlwaysInline]
    # protected def csmKeyControl(ch : OPLCh) : Nil
    #   keyOn(ch.slot1, 4u32)
    #   keyOn(ch.slot2, 4u32)

    #   # The key off should happen exactly one sample later - not implemented
    #   # correctly yet.
    #   keyOff(ch.slot1, ~4u32)
    #   keyOff(ch.slot2, ~4u32)
    # end

    # Write a value v to register r on OPL chip
    protected def writeReg(r : UInt8, v : UInt8) : Nil
      slot : Int32 = 0
      blockFnum : Int32 = 0

      case r & 0xE0
      when 0x00 # 00-1F: control
        case r & 0x1F
        when 0x01 # Waveform select enable
          if Yuno.bitflag?(@type, OPL_TYPE_WAVESEL)
            @waveSel = v & 0x20
            # Do not change the waveform previously selected
          end

        when 0x02 # Timer 1
          @timer.put!(0, (256u32 - v) * 4)

        when 0x03 # Timer 2
          @timer.put!(1, (256u32 - v) * 16)

        when 0x04 # IRQ clear / mask and Timer enable
          if Yuno.bitflag?(v, 0x80)
            # IRQ flag clear
            statusReset(0x7F - 0x08) # Don't reset BFRDY flag or we will have to call deltat module to set the flag
          else
            # Set IRQ mask ,timer enable
            st1 : UInt8 = v & 1
            st2 : UInt8 = (v >> 1) & 1

            # IRQRST, T1MSK, t2MSK, EOSMSK, BRMSK, x, ST2, ST1
            statusReset(v & (0x78 - 0x08))
            statusMaskSet((~v) & 0x78)

             # Timer 2
            @timerOn.put!(1, st2.to_u32!) if @timerOn.get!(1) != st2

             # Timer 1
            @timerOn.put!(0, st1.to_u32!) if @timerOn.get!(0) != st1
          end

        # when 0x06 # This is Y8950 only
        # when 0x07 # This is Y8950 only

        when 0x08
          # MODE, DELTA-T control 2 : CSM, NOTESEL, x, x, smpl, da/ad, 64k, rom
          @mode = v

        else
          Yuno.dlog!(sprintf("OPL: write to unknown register: %02x", r))
        end # case r & 0x1F

      # AM on, Vibrato on, ksr, egType, mul
      when 0x20
        slot = SLOT_ARRAY[r & 0x1F]
        return if slot < 0
        setMul(slot, v)
      when 0x40
        slot = SLOT_ARRAY[r & 0x1F]
        return if slot < 0
        setKslTl(slot, v)
      when 0x60
        slot = SLOT_ARRAY[r & 0x1F]
        return if slot < 0
        setArDr(slot, v)
      when 0x80
        slot = SLOT_ARRAY[r & 0x1F]
        return if slot < 0
        setSlRr(slot, v)
      when 0xA0
        if r == 0xBD # am depth, vibrato depth, r, bd, sd, tom, tc, hh
          @lfoAmDepth = v & 80
          @lfoPmDepthRange = Yuno.bitflag?(v, 0x40) ? 8u8 : 0u8
          @rhythm = v & 0x3F

          if Yuno.bitflag?(@rhythm, 0x20)
            # BD key on/off
            if Yuno.bitflag?(v, 0x10)
              keyOn(@pch.get!(6).slot1, 2u32)
              keyOn(@pch.get!(6).slot2, 2u32)
            else
              keyOff(@pch.get!(6).slot1, ~2u32)
              keyOff(@pch.get!(6).slot2, ~2u32)
            end

            # HH key on/off
            if Yuno.bitflag?(v, 0x01)
              keyOn(@pch.get!(7).slot1, 2u32)
            else
              keyOff(@pch.get!(7).slot1, ~2u32)
            end

            # SD key on/off
            if Yuno.bitflag?(v, 0x08)
              keyOn(@pch.get!(7).slot2, 2u32)
            else
              keyOff(@pch.get!(7).slot2, ~2u32)
            end

            # TOM key on/off
            if Yuno.bitflag?(v, 0x04)
              keyOn(@pch.get!(8).slot1, 2u32)
            else
              keyOff(@pch.get!(8).slot1, ~2u32)
            end

            # TOP-CY key on/off
            if Yuno.bitflag?(v, 0x02)
              keyOn(@pch.get!(8).slot2, 2u32)
            else
              keyOff(@pch.get!(8).slot2, ~2u32)
            end
          else
            # BD key off
            keyOff(@pch.get!(6).slot1, ~2u32)
            keyOff(@pch.get!(6).slot2, ~2u32)

            # HH key off
            keyOff(@pch.get!(7).slot1, ~2u32)

            # SD key off
            keyOff(@pch.get!(7).slot2, ~2u32)

            # TIM key off
            keyOff(@pch.get!(8).slot1, ~2u32)

            # TOP-CY key off
            keyOff(@pch.get!(8).slot2, ~2u32)
          end # Yuno.bitflag?(@rhythm, 0x20)
        end # if r == 0xBD

        # Key on, block, fnum
        return if (r & 0x0F) > 8
        ch = @pch[r & 0x0F]
        if !Yuno.bitflag?(r, 0x10)
          # A0-A8
          blockFnum = ((ch.blockFnum & 0x1F00) | v).to_i32!
        else
          # B0-B8
          blockFnum = ((v.to_i32! & 0x1F) << 8) | (ch.blockFnum & 0xFF)

          if Yuno.bitflag?(v, 0x20)
            keyOn(ch.slot1, 1u32)
            keyOn(ch.slot2, 1u32)
          else
            keyOff(ch.slot1, ~1u32)
            keyOff(ch.slot2, ~1u32)
          end
        end

        # Update
        if ch.blockFnum != blockFnum
          block : UInt8 = (blockFnum >> 10).to_u8!

          ch.blockFnum = blockFnum.to_u32!
          ch.kslBase = KSL_TAB[blockFnum >> 6]
          ch.fc = @fnTab.get!(blockFnum & 0x03FF) >> (7 - block)

          # BLK 2, 1, 0 bits -> bits 3, 2, 1 of kcode
          ch.kcode = ((ch.blockFnum & 0x1C00) >> 9).to_u8!

          # The info below is actually opposite to what is stated in the Manuals
          # (verifed on real YM3812):
          #
          # if notesel == 0 -> lsb of kcode is bit 10 (MSB) of fnum
          # if notesel == 1 -> lsb of kcode is bit 9 (MSB-1) of fnum
          if Yuno.bitflag?(@mode, 0x40)
            ch.kcode |= (ch.blockFnum & 0x100) >> 8 # notesel = 1
          else
            ch.kcode |= (ch.blockFnum & 0x200) >> 9 # notesel = 0
          end

          # Refresh Total Level in both SLOTs of this channel
          ch.slot1.tll = (ch.slot1.tl &+ (ch.kslBase >> ch.slot1.ksl)).to_i32!
          ch.slot2.tll = (ch.slot2.tl &+ (ch.kslBase >> ch.slot2.ksl)).to_i32!

          # Refresh frequency counter in both SLOTs of this channel
          calcFcSlot(ch, ch.slot1)
          calcFcSlot(ch, ch.slot2)
        end

      when 0xC0 # FB, C
        return if (r & 0x0F) > 8
        ch = @pch[r & 0x0F]
        ch.slot1.fb = Yuno.bitflag?(v >> 1, 7) ? ((v >> 1) & 7) + 7 : 0u8
        ch.slot1.con = v & 1
        ch.slot1.connect1 = if ch.slot1.con != 0
                              @output.to_unsafe
                            else
                              @phaseModulation.to_unsafe
                            end

      when 0xE0 # Waveform select
        # Simply ignore write to the waveform select register if selecting not
        # enabled in test register.
        if @waveSel != 0
          slot = SLOT_ARRAY[r & 0x1F]
          return if slot < 0
          ch = @pch[slot >> 1]
          ch.slot.get!(slot & 1).wavetable = (v.to_u16! & 0x03) * SIN_LEN
        end
      end # case r & 0xE0
    end

    def reset : Nil
      @egTimer = 0
      @egCount = 0
      @noiseRng = 1 # Noise shift register
      @mode = 0     # Normal mode
      statusReset(0x7F)

      # Reset these with some register writes
      writeReg(0x01_u8, 0u8) # wavesel disable
      writeReg(0x02_u8, 0u8) # Timer 1
      writeReg(0x03_u8, 0u8) # Timer 2
      writeReg(0x04_u8, 0u8) # IRQ mask clear
      0xFF_u8.downto(0x20u8) { |i| writeReg(i, 0u8) }

      # Reset operator parameters
      @pch.each do |ch|
        ch.slot.each do |sl|
          sl.wavetable = 0
          sl.state = EG_OFF
          sl.volume = MAX_ATT_INDEX
        end
      end
    end

    # def setIRQHandler(handler : OPLIrqHandler, param : AbstractEmulator) : Nil
    #   @irqHandler = handler
    #   @irqParam = param
    # end

    def setUpdateHandler(handler : OPLUpdateHandler, param : AbstractEmulator) : Nil
      @updateHandler = handler
      @updateParam = param
    end

    @[AlwaysInline]
    def write(a : Int32, v : Int32) : Int32
      if !Yuno.bitflag?(a, 1)
        # Address port
        @address = v.to_u8!
      else
        # Data port
        @updateHandler.try(&.call(@updateParam.not_nil!))
        writeReg(@address, v.to_u8!)
      end
      (@status >> 7).to_i32!
    end

    def read(a : Int32) : UInt8
      unless Yuno.bitflag?(a, 1)
        # Status port
        @status & (@statusMask | 0x80)
      else
        0xFF_u8
      end
    end

    def muteMask=(mask : UInt32) : Nil
      9.times do |ch|
        @pch[ch].muted = ((mask >> ch) & 0x01).to_u8!
      end

      6.times do |ch|
        @muteSpc = ((mask >> (9 + ch)) & 0x01).to_u8!
      end
    end
  end # module OPLMame
end
