####  Yamaha 3812 emulator interface - MAME VERSION
#### Created by Ernesto Corvi
#### Crystal port Copyright (C) 2023-2024 Remilia Scarlet
####
#### UPDATE LOG
####   JB  28-04-2002  Fixed simultaneous usage of all three different chip types.
####                       Used real sample rate when resample filter is active.
####       AAT 12-28-2001  Protected Y8950 from accessing unmapped port and keyboard handlers.
####   CHS 1999-01-09  Fixes new ym3812 emulation interface.
####   CHS 1998-10-23  Mame streaming sound chip update
####   EC  1998        Created Interface
####
require "./opl-mame-common"
require "../emu-opn-mame/deltat"

####
#### Yamaha YM3526 Emulator
####

module Yuno::Chips
  # :nodoc:
  class YM3526Mame < Yuno::AbstractEmulator
    include OPLMame

    ###
    ### Public Methods
    ###

    def initialize(@clock : UInt32, @rate : UInt32)
      @pch = Array(OPLCh).new(9) { |_| OPLCh.new }
      initOpl
      initTables
      reset
    end

    @[AlwaysInline]
    def read(a : Int32) : UInt8
      ret = super
      ret | 0x06
    end

    @[AlwaysInline]
    def update(outputs : OutputBuffers, samples : UInt32) : Nil
      outL : Slice(Int32) = outputs[0]
      outR : Slice(Int32) = outputs[1]

      # Check once, then we can avoid bounds checks below.
      if outL.size < samples || outR.size < samples
        raise "YM3526 update: Bad number of samples (expected #{samples}, " \
              "but buffers have #{outL.size} and #{outR.size})"
      end

      rhythm : UInt8 = @rhythm & 0x20
      lt : Int32 = 0

      samples.times do |i|
        @output.put!(0, 0)

        advanceLfo

        # FM part
        calcCh(@pch.get!(0))
        calcCh(@pch.get!(1))
        calcCh(@pch.get!(2))
        calcCh(@pch.get!(3))
        calcCh(@pch.get!(4))
        calcCh(@pch.get!(5))

        if rhythm == 0
          calcCh(@pch.get!(6))
          calcCh(@pch.get!(7))
          calcCh(@pch.get!(8))
        else
          # Rhythm part
          calcRh(@pch.get!(6), @noiseRng & 1) # Remi: Pass channel 6 here so we don't have to index @pch twice later
        end

        lt = @output.get!(0)
        outL.put!(i, lt)
        outR.put!(i, lt)

        advance
      end
    end
  end
end
