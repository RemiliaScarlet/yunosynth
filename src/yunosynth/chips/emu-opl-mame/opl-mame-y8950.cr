####
#### File: fmopl.c - software implementation of FM sound generator
####                                            types OPL and OPL2
####
#### Copyright Jarek Burczynski (bujar at mame dot net)
#### Copyright Tatsuyuki Satoh , MultiArcadeMachineEmulator development
#### Crystal port Copyright (C) 2023-2024 Remilia Scarlet
####
####  Version 0.72
####
####
#### Revision History:
####
#### 04-08-2003 Jarek Burczynski:
####  - removed BFRDY hack. BFRDY is busy flag, and it should be 0 only when the chip
####    handles memory read/write or during the adpcm synthesis when the chip
####    requests another byte of ADPCM data.
####
#### 24-07-2003 Jarek Burczynski:
####  - added a small hack for Y8950 status BFRDY flag (bit 3 should be set after
####    some (unknown) delay). Right now it's always set.
####
#### 14-06-2003 Jarek Burczynski:
####  - implemented all of the status register flags in Y8950 emulation
####  - renamed y8950_set_delta_t_memory() parameters from _rom_ to _mem_ since
####    they can be either RAM or ROM
####
#### 08-10-2002 Jarek Burczynski (thanks to Dox for the YM3526 chip)
####  - corrected ym3526_read() to always set bit 2 and bit 1
####    to HIGH state - identical to ym3812_read (verified on real YM3526)
####
#### 04-28-2002 Jarek Burczynski:
####  - binary exact Envelope Generator (verified on real YM3812);
####    compared to YM2151: the EG clock is equal to internal_clock,
####    rates are 2 times slower and volume resolution is one bit less
####  - modified interface functions (they no longer return pointer -
####    that's internal to the emulator now):
####     - new wrapper functions for OPLCreate: ym3526_init(), ym3812_init() and y8950_init()
####  - corrected 'off by one' error in feedback calculations (when feedback is off)
####  - enabled waveform usage (credit goes to Vlad Romascanu and zazzal22)
####  - speeded up noise generator calculations (Nicola Salmoria)
####
#### 03-24-2002 Jarek Burczynski (thanks to Dox for the YM3812 chip)
####  Complete rewrite (all verified on real YM3812):
####  - corrected sin_tab and tl_tab data
####  - corrected operator output calculations
####  - corrected waveform_select_enable register;
####    simply: ignore all writes to waveform_select register when
####    waveform_select_enable == 0 and do not change the waveform previously selected.
####  - corrected KSR handling
####  - corrected Envelope Generator: attack shape, Sustain mode and
####    Percussive/Non-percussive modes handling
####  - Envelope Generator rates are two times slower now
####  - LFO amplitude (tremolo) and phase modulation (vibrato)
####  - rhythm sounds phase generation
####  - white noise generator (big thanks to Olivier Galibert for mentioning Berlekamp-Massey algorithm)
####  - corrected key on/off handling (the 'key' signal is ORed from three sources: FM, rhythm and CSM)
####  - funky details (like ignoring output of operator 1 in BD rhythm sound when connect == 1)
####
#### 12-28-2001 Acho A. Tang
####  - reflected Delta-T EOS status on Y8950 status port.
####  - fixed subscription range of attack/decay tables
####
####
####     To do:
####         add delay before key off in CSM mode (see CSMKeyControll)
####         verify volume of the FM part on the Y8950
####
require "./opl-mame-common"
require "../emu-opn-mame/deltat"

####
#### Yamaha Y8950 Emulator
####

module Yuno::Chips
  # :nodoc:
  class Y8950Mame < Yuno::AbstractEmulator
    include OPLMame

    ###
    ### Aliases
    ###

    alias OPLPortHandlerWrite = Proc(AbstractEmulator, UInt8, Nil)
    alias OPLPortHandlerRead  = Proc(AbstractEmulator, UInt8)

    ###
    ### Fields
    ###

    @deltaT : OPNMame::DeltaT # Delta-T ADPCM unit

    # Keyboard and I/O ports interface
    @portDirection        : UInt8 = 0
    @portLatch            : UInt8 = 0
    @portHandlerRead      : OPLPortHandlerRead?
    @portHandlerWrite     : OPLPortHandlerWrite?
    @portParam            : Yuno::AbstractEmulator?
    @keyboardHandlerRead  : OPLPortHandlerRead?
    @keyboardHandlerWrite : OPLPortHandlerWrite?
    @keyboardParam        : Yuno::AbstractEmulator?

    @outputDeltaT : Array(Int32) = [0, 0, 0, 0] # The chip is mono, that 4 here is just for safety

    ###
    ### Public Methods
    ###

    def initialize(@clock : UInt32, @rate : UInt32)
      @type = OPL_TYPE_ADPCM | OPL_TYPE_KEYBOARD | OPL_TYPE_IO
      @pch = Array(OPLCh).new(9) { |_| OPLCh.new }

      @deltaT = OPNMame::DeltaT.new
      @deltaT.memory = Bytes.new(0)
      @deltaT.statusSetHandler   = ->deltaTStatusSet(Yuno::AbstractEmulator, UInt8)
      @deltaT.statusResetHandler = ->deltaTStatusReset(Yuno::AbstractEmulator, UInt8)
      @deltaT.statusChangeWhichChip = self

      # Status flag: set bit4 on End Of Sample
      @deltaT.statusChangeEOSBit = 0x10

      # Status flag: set bit3 on BRDY (End Of: ADPCM analysis/synthesis, memory reading/writing)
      @deltaT.statusChangeBRDYBit = 0x08

      initOpl
      initTables
      reset
    end

    def reset : Nil
      super

      # The Y8950 does a bit more because of its DeltaT ADPCM unit.
      @deltaT.freqBase = @freqBase
      @deltaT.outputPointer = Slice.new(@outputDeltaT.to_unsafe, @outputDeltaT.size)
      @deltaT.portShift = 5
      @deltaT.outputRange = 1 << 23
      @deltaT.adpcmReset(0, OPNMame::EMULATION_MODE_NORMAL)
    end

    def read(a : Int32) : UInt8
      unless Yuno.bitflag?(a, 1)
        # Status port
        @status & (@statusMask | 0x80)
      else
        # Data port
        case @address
        when 0x05 # Keyboard in
          if fn = @keyboardHandlerRead
            fn.call(@keyboardParam.not_nil!)
          else
            0u8
          end

        when 0x0F # ADPCM Data
          @deltaT.adpcmRead

        when 0x19 # I/O Data
          if fn = @portHandlerRead
            fn.call(@portParam.not_nil!)
          else
            0u8
          end

        when 0x1A # PCM Data
          0x80_u8 # 2's complement PCM data - result from A/D conversion

        else 0xFFu8
        end
      end
    end

    @[AlwaysInline]
    def update(outputs : OutputBuffers, samples : UInt32) : Nil
      outL : Slice(Int32) = outputs[0]
      outR : Slice(Int32) = outputs[1]

      # Check once, then we can avoid bounds checks below.
      if outL.size < samples || outR.size < samples
        raise "Y8950 update: Bad number of samples (expected #{samples}, " \
              "but buffers have #{outL.size} and #{outR.size})"
      end

      rhythm : UInt8 = @rhythm & 0x20
      lt : Int32 = 0

      samples.times do |i|
        @output.put!(0, 0)
        @outputDeltaT.put!(0, 0)

        advanceLfo

        # DeltaT ADPCM
        if Yuno.bitflag?(@deltaT.portState, 0x80) && !spcMuted?(5)
          @deltaT.adpcmCalc
        end

        # FM part
        calcCh(@pch.get!(0))
        calcCh(@pch.get!(1))
        calcCh(@pch.get!(2))
        calcCh(@pch.get!(3))
        calcCh(@pch.get!(4))
        calcCh(@pch.get!(5))

        if rhythm == 0
          calcCh(@pch.get!(6))
          calcCh(@pch.get!(7))
          calcCh(@pch.get!(8))
        else
          # Rhythm part
          calcRh(@pch.get!(6), @noiseRng & 1) # Remi: Pass channel 6 here so we don't have to index @pch twice later
        end

        lt = @output.get!(0) + (@outputDeltaT.get!(0) >> 11)
        outL.put!(i, lt)
        outR.put!(i, lt)

        advance
      end
    end

    @[AlwaysInline]
    def setDeltaTMemory(deltaTMem : Slice(UInt8)) : Nil
      @deltaT.memory = deltaTMem
    end

    def writeRom(romSize : Int32, dataStart : Int32, dataLength : Int32, romData : Slice(UInt8)) : Nil
      if @deltaT.memory.size != romSize
        @deltaT.memory = Bytes.new(romSize, 0xFF_u8)
        @deltaT.calcMemMask
      end

      return if dataStart > romSize
      dataLength = romSize - dataStart if dataStart + dataLength > romSize

      # Copy the memory
      dataLength.times do |i|
        # This can be guaranteed by the checks above
        @deltaT.memory.put!(i + dataStart, romData.unsafe_fetch(i))
      end
    end

    # def setPortHandler(writeHandler : OPLPortHandlerWrite, readHandler : OPLPortHandlerRead,
    #                    param : AbstractEmulator) : Nil
    #   @portHandlerWrite = writeHandler
    #   @portHandlerRead = readHandler
    #   @portParam = param
    # end

    # def setKeyboardHandler(writeHandler : OPLPortHandlerWrite, readHandler : OPLPortHandlerRead,
    #                        param : AbstractEmulator) : Nil
    #   @keyboardHandlerWrite = writeHandler
    #   @keyboardHandlerRead = readHandler
    #   @keyboardParam = param
    # end

    ###
    ### Protected Methods
    ###

    protected def writeReg(r : UInt8, v : UInt8) : Nil
      slot : Int32 = 0
      blockFnum : Int32 = 0

      case r & 0xE0
      when 0x00 # 00-1F: control
        case r & 0x1F
        when 0x01 # Waveform select enable
          nil # Y8950 doesn't use this

        when 0x02 # Timer 1
          @timer.put!(0, (256u32 - v) * 4)

        when 0x03 # Timer 2
          @timer.put!(1, (256u32 - v) * 16)

        when 0x04 # IRQ clear / mask and Timer enable
          if Yuno.bitflag?(v, 0x80)
            # IRQ flag clear
            statusReset(0x7F - 0x08) # Don't reset BFRDY flag or we will have to call deltat module to set the flag
          else
            # Set IRQ mask ,timer enable
            st1 : UInt8 = v & 1
            st2 : UInt8 = (v >> 1) & 1

            # IRQRST, T1MSK, t2MSK, EOSMSK, BRMSK, x, ST2, ST1
            statusReset(v & (0x78 - 0x08))
            statusMaskSet((~v) & 0x78)

             # Timer 2
            @timerOn.put!(1, st2.to_u32!) if @timerOn.get!(1) != st2

             # Timer 1
            @timerOn.put!(0, st1.to_u32!) if @timerOn.get!(0) != st1
          end

        when 0x06 # Keyboard out
          if fn = @keyboardHandlerWrite
            fn.call(@keyboardParam.not_nil!, v)
          else
            Yuno.dlog!("Y8950: Write to unmapped keyboard port")
          end

        when 0x07 # DELTA-T control 1: START, REC, MEMDATA, REPT, SPOFF, x, x, RST
          @deltaT.adpcmWrite(r - 0x07, v)

        when 0x08
          # MODE, DELTA-T control 2 : CSM, NOTESEL, x, x, smpl, da/ad, 64k, rom
          @mode = v
          # mask 4 LSBs in register 08 for DELTA-T unit
          @deltaT.adpcmWrite(r - 0x07, v & 0x0F)

        when 0x09, # Start add
             0x0A,
             0x0B, # Stop add
             0x0C,
             0x0D, # Prescale
             0x0E,
             0x0F, # ADPCM data write
             0x10, # Delta-N
             0x11, # Delta-N
             0x12  # ADPCM volume
          @deltaT.adpcmWrite(r - 0x07, v)

        when 0x15, # DAC data high 8 bits (F7, F6..F2)
             0x16, # DAC data low 2 bits (F1, F0 in bits 7, 6)
             0x17  # DAC data shift (S2, S1, S0 in bits 2, 1, 0)
          Yuno.dlog!(sprintf("Y8950: DAC data register written, but not implemented reg=%02x val=%02x", r, v))

        when 0x18 # I/O Control (direction)
          @portDirection = v & 0x0F

        when 0x19 # I/O Data
          @portLatch = v
          @portHandlerWrite.try(&.call(@portParam.not_nil!, v & @portDirection))

        else
          Yuno.dlog!(sprintf("Y8950: write to unknown register: %02x", r))
        end

      # AM on, Vibrato on, ksr, egType, mul
      when 0x20
        slot = SLOT_ARRAY[r & 0x1F]
        return if slot < 0
        setMul(slot, v)
      when 0x40
        slot = SLOT_ARRAY[r & 0x1F]
        return if slot < 0
        setKslTl(slot, v)
      when 0x60
        slot = SLOT_ARRAY[r & 0x1F]
        return if slot < 0
        setArDr(slot, v)
      when 0x80
        slot = SLOT_ARRAY[r & 0x1F]
        return if slot < 0
        setSlRr(slot, v)
      when 0xA0
        if r == 0xBD # am depth, vibrato depth, r, bd, sd, tom, tc, hh
          @lfoAmDepth = v & 0x80
          @lfoPmDepthRange = Yuno.bitflag?(v, 0x40) ? 8u8 : 0u8
          @rhythm = v & 0x3F

          if Yuno.bitflag?(@rhythm, 0x20)
            # BD key on/off
            if Yuno.bitflag?(v, 0x10)
              keyOn(@pch.get!(6).slot1, 2u32)
              keyOn(@pch.get!(6).slot2, 2u32)
            else
              keyOff(@pch.get!(6).slot1, ~2u32)
              keyOff(@pch.get!(6).slot2, ~2u32)
            end

            # HH key on/off
            if Yuno.bitflag?(v, 0x01)
              keyOn(@pch.get!(7).slot1, 2u32)
            else
              keyOff(@pch.get!(7).slot1, ~2u32)
            end

            # SD key on/off
            if Yuno.bitflag?(v, 0x08)
              keyOn(@pch.get!(7).slot2, 2u32)
            else
              keyOff(@pch.get!(7).slot2, ~2u32)
            end

            # TOM key on/off
            if Yuno.bitflag?(v, 0x04)
              keyOn(@pch.get!(8).slot1, 2u32)
            else
              keyOff(@pch.get!(8).slot1, ~2u32)
            end

            # TOP-CY key on/off
            if Yuno.bitflag?(v, 0x02)
              keyOn(@pch.get!(8).slot2, 2u32)
            else
              keyOff(@pch.get!(8).slot2, ~2u32)
            end
          else
            # BD key off
            keyOff(@pch.get!(6).slot1, ~2u32)
            keyOff(@pch.get!(6).slot2, ~2u32)

            # HH key off
            keyOff(@pch.get!(7).slot1, ~2u32)

            # SD key off
            keyOff(@pch.get!(7).slot2, ~2u32)

            # TIM key off
            keyOff(@pch.get!(8).slot1, ~2u32)

            # TOP-CY key off
            keyOff(@pch.get!(8).slot2, ~2u32)
          end # Yuno.bitflag?(@rhythm, 0x20)
        end # if r == 0xBD

        # Key on, block, fnum
        return if (r & 0x0F) > 8
        ch = @pch[r & 0x0F]
        if !Yuno.bitflag?(r, 0x10)
          # A0-A8
          blockFnum = ((ch.blockFnum & 0x1F00) | v).to_i32!
        else
          # B0-B8
          blockFnum = ((v.to_i32! & 0x1F) << 8) | (ch.blockFnum & 0xFF)

          if Yuno.bitflag?(v, 0x20)
            keyOn(ch.slot1, 1u32)
            keyOn(ch.slot2, 1u32)
          else
            keyOff(ch.slot1, ~1u32)
            keyOff(ch.slot2, ~1u32)
          end
        end

        # Update
        if ch.blockFnum != blockFnum
          block : UInt8 = (blockFnum >> 10).to_u8!

          ch.blockFnum = blockFnum.to_u32!
          ch.kslBase = KSL_TAB[blockFnum >> 6]
          ch.fc = @fnTab.get!(blockFnum & 0x03FF) >> (7 - block)

          # BLK 2, 1, 0 bits -> bits 3, 2, 1 of kcode
          ch.kcode = ((ch.blockFnum & 0x1C00) >> 9).to_u8!

          # The info below is actually opposite to what is stated in the Manuals
          # (verifed on real YM3812):
          #
          # if notesel == 0 -> lsb of kcode is bit 10 (MSB) of fnum
          # if notesel == 1 -> lsb of kcode is bit 9 (MSB-1) of fnum
          if Yuno.bitflag?(@mode, 0x40)
            ch.kcode |= (ch.blockFnum & 0x100) >> 8 # notesel = 1
          else
            ch.kcode |= (ch.blockFnum & 0x200) >> 9 # notesel = 0
          end

          # Refresh Total Level in both SLOTs of this channel
          ch.slot1.tll = (ch.slot1.tl &+ (ch.kslBase >> ch.slot1.ksl)).to_i32!
          ch.slot2.tll = (ch.slot2.tl &+ (ch.kslBase >> ch.slot2.ksl)).to_i32!

          # Refresh frequency counter in both SLOTs of this channel
          calcFcSlot(ch, ch.slot1)
          calcFcSlot(ch, ch.slot2)
        end

      when 0xC0 # FB, C
        return if (r & 0x0F) > 8
        ch = @pch[r & 0x0F]
        ch.slot1.fb = Yuno.bitflag?(v >> 1, 7) ? ((v >> 1) & 7) + 7 : 0u8
        ch.slot1.con = v & 1
        ch.slot1.connect1 = if ch.slot1.con != 0
                              @output.to_unsafe
                            else
                              @phaseModulation.to_unsafe
                            end

      when 0xE0 # Waveform select
        # Simply ignore write to the waveform select register if selecting not
        # enabled in test register.
        if @waveSel != 0
          slot = SLOT_ARRAY[r & 0x1F]
          return if slot < 0
          ch = @pch[slot >> 1]
          ch.slot.get!(slot & 1).wavetable = (v.to_u16! & 0x03) * SIN_LEN
        end
      end # case r & 0xE0
    end

    protected def deltaTStatusSet(chip : Yuno::AbstractEmulator, changeBits : UInt8) : Nil
      statusSet(changeBits)
    end

    protected def deltaTStatusReset(chip : Yuno::AbstractEmulator, changeBits : UInt8) : Nil
      statusReset(changeBits)
    end
  end
end
