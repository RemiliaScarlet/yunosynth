#### Game Boy sound emulation (c) Anthony Kruize (trandor@labyrinth.net.au)
#### thanks-to:Shay Green
#### Crystal port Copyright (C) 2023-2024 Remilia Scarlet
####
#### Anyways, sound on the Game Boy consists of 4 separate 'channels'
####   Sound1 = Quadrangular waves with SWEEP and ENVELOPE functions  (NR10,11,12,13,14)
####   Sound2 = Quadrangular waves with ENVELOPE functions (NR21,22,23,24)
####   Sound3 = Wave patterns from WaveRAM (NR30,31,32,33,34)
####   Sound4 = White noise with an envelope (NR41,42,43,44)
####
#### Each sound channel has 2 modes, namely ON and OFF...  whoa
####
#### These tend to be the two most important equations in
#### converting between Hertz and GB frequency registers:
#### (Sounds will have a 2.4% higher frequency on Super GB.)
####       gb = 2048 - (131072 / Hz)
####       Hz = 131072 / (2048 - gb)
####
#### Changes:
####
####   10/2/2002       AK - Preliminary sound code.
####   13/2/2002       AK - Added a hack for mode 4, other fixes.
####   23/2/2002       AK - Use lookup tables, added sweep to mode 1. Re-wrote the square
####                        wave generation.
####   13/3/2002       AK - Added mode 3, better lookup tables, other adjustments.
####   15/3/2002       AK - Mode 4 can now change frequencies.
####   31/3/2002       AK - Accidently forgot to handle counter/consecutive for mode 1.
####    3/4/2002       AK - Mode 1 sweep can still occur if shift is 0.  Don't let frequency
####                        go past the maximum allowed value. Fixed Mode 3 length table.
####                        Slight adjustment to Mode 4's period table generation.
####    5/4/2002       AK - Mode 4 is done correctly, using a polynomial counter instead
####                        of being a total hack.
####    6/4/2002       AK - Slight tweak to mode 3's frequency calculation.
####   13/4/2002       AK - Reset envelope value when sound is initialized.
####   21/4/2002       AK - Backed out the mode 3 frequency calculation change.
####                        Merged init functions into gameboy_sound_w().
####   14/5/2002       AK - Removed magic numbers in the fixed point math.
####   12/6/2002       AK - Merged SOUNDx structs into one SOUND struct.
####  26/10/2002       AK - Finally fixed channel 3!
#### xx/4-5/2016       WP - Rewrote sound core. Most of the code is not optimized yet.
####
#### TODO:
#### - Implement different behavior of CGB-02.
#### - Implement different behavior of CGB-05.
#### - Perform more tests on real hardware to figure out when the frequency counters are
####   reloaded.
#### - Perform more tests on real hardware to understand when changes to the noise divisor
####   and shift kick in.
#### - Optimize the channel update methods.

module Yuno::Chips
  class DMG < Yuno::AbstractChip
    private class DMGMame < Yuno::AbstractEmulator
      ###
      ### Constants
      ###

      NR10 = 0x00_u8
      NR11 = 0x01_u8
      NR12 = 0x02_u8
      NR13 = 0x03_u8
      NR14 = 0x04_u8
      # 0x05
      NR21 = 0x06_u8
      NR22 = 0x07_u8
      NR23 = 0x08_u8
      NR24 = 0x09_u8
      NR30 = 0x0A_u8
      NR31 = 0x0B_u8
      NR32 = 0x0C_u8
      NR33 = 0x0D_u8
      NR34 = 0x0E_u8
      # 0x0F
      NR41 = 0x10_u8
      NR42 = 0x11_u8
      NR43 = 0x12_u8
      NR44 = 0x13_u8
      NR50 = 0x14_u8
      NR51 = 0x15_u8
      NR52 = 0x16_u8
      # 0x17 - 0x1F
      AUD3W0 = 0x20_u8
      AUD3W1 = 0x21_u8
      AUD3W2 = 0x22_u8
      AUD3W3 = 0x23_u8
      AUD3W4 = 0x24_u8
      AUD3W5 = 0x25_u8
      AUD3W6 = 0x26_u8
      AUD3W7 = 0x27_u8
      AUD3W8 = 0x28_u8
      AUD3W9 = 0x29_u8
      AUD3WA = 0x2A_u8
      AUD3WB = 0x2B_u8
      AUD3WC = 0x2C_u8
      AUD3WD = 0x2D_u8
      AUD3WE = 0x2E_u8
      AUD3WF = 0x2F_u8

      FRAME_CYCLES = 8192_u32

      # Represents wave duties of 12.5%, 25%, 50% and 75%
      WAVE_DUTY_TABLE = [
        [ -1i8, -1i8, -1i8, -1i8, -1i8, -1i8, -1i8,  1i8],
        [  1i8, -1i8, -1i8, -1i8, -1i8, -1i8, -1i8,  1i8],
        [  1i8, -1i8, -1i8, -1i8, -1i8,  1i8,  1i8,  1i8],
        [ -1i8,  1i8,  1i8,  1i8,  1i8,  1i8,  1i8, -1i8]
      ]

      RC_SHIFT = 16_u8

      enum Mode
        GameBoy
        GameBoyColor
      end

      SOUND_READ_MASK = [
        0x80_u8, 0x3F_u8, 0x00_u8, 0xFF_u8, 0xBF_u8, 0xFF_u8, 0x3F_u8,
        0x00_u8, 0xFF_u8, 0xBF_u8, 0x7F_u8, 0xFF_u8, 0x9F_u8, 0xFF_u8,
        0xBF_u8, 0xFF_u8, 0xFF_u8, 0x00_u8, 0x00_u8, 0xBF_u8, 0x00_u8,
        0x00_u8, 0x70_u8, 0xFF_u8, 0xFF_u8, 0xFF_u8, 0xFF_u8, 0xFF_u8,
        0xFF_u8, 0xFF_u8, 0xFF_u8, 0xFF_u8, 0x00_u8, 0x00_u8, 0x00_u8,
        0x00_u8, 0x00_u8, 0x00_u8, 0x00_u8, 0x00_u8, 0x00_u8, 0x00_u8,
        0x00_u8, 0x00_u8, 0x00_u8, 0x00_u8, 0x00_u8, 0x00_u8, 0x00_u8,
        0x00_u8, 0x00_u8, 0x00_u8, 0x00_u8, 0x00_u8, 0x00_u8, 0x00_u8,
        0x00_u8, 0x00_u8, 0x00_u8, 0x00_u8, 0x00_u8, 0x00_u8, 0x00_u8,
        0x00_u8
      ]

      NOISE_PERIOD_CYCLES_DIVISOR = [ 8_u32, 16_u32, 32_u32, 48_u32, 64_u32, 80_u32, 96_u32, 112_u32 ]

      ###
      ### Internal Classes
      ###

      private class RatioCtrl
        property inc : UInt32 = 0 # Counter increment
        property val : UInt32 = 0 # Current value

        def initialize
        end

        @[AlwaysInline]
        def setRatio(mul : UInt32, div : UInt32) : Nil
          @inc = ((mul.to_u64! << RC_SHIFT) + div.to_u64!.tdiv(2)).tdiv(div.to_u64!).to_u32
        end
      end

      private class Sound
        ###
        ### Common Fields
        ###

        property reg : Bytes = Bytes.new(5)
        property? on : Bool = false
        property channel : UInt8 = 0
        property length : UInt8 = 0
        property lengthMask : UInt8 = 0
        property? lengthCounting : Bool = false
        property? lengthEnabled : Bool = false

        ###
        ### Modes 1, 2, and 3
        ###

        property cyclesLeft : UInt32 = 0
        property duty : Int8 = 0

        ###
        ### Modes 1, 2, and 4
        ###

        property? envelopeEnabled : Bool = false
        property envelopeValue : Int8 = 0
        property envelopeDirection : Int8 = 0
        property envelopeTime : UInt8 = 0
        property envelopeCount : UInt8 = 0
        property signal : Int8 = 0

        ###
        ### Mode 1
        ###

        property frequency : UInt16 = 0
        property frequencyCounter : UInt16 = 0
        property? sweepEnabled : Bool = false
        property? sweepNegModeUsed : Bool = false
        property sweepShift : UInt8 = 0
        property sweepDirection : Int8 = 0
        property sweepTime : UInt8 = 0
        property sweepCount : UInt8 = 0

        ###
        ### Mode 3
        ###

        property level : UInt8 = 0
        property offset : UInt8 = 0
        property dutyCount : UInt32 = 0
        property currentSample : Int8 = 0
        property? sampleReading : Bool = false

        ###
        ### Mode 4
        ###

        property? noiseShort : Bool = false
        property noiseLfsr : UInt16 = 0
        property muted : UInt8 = 0

        def initialize
        end

        @[AlwaysInline]
        def tickLength : Nil
          if @lengthEnabled
            @length = (@length &+ 1) & @lengthMask
            if @length == 0
              @on = false
              @lengthCounting = false
            end
          end
        end

        @[AlwaysInline]
        def calculateNextSweep : Int32
          @sweepNegModeUsed = @sweepDirection < 0
          newFreq : Int32 = @frequency.to_i32! + @sweepDirection.to_i32! * (@frequency.to_i32! >> @sweepShift)

          @on = false if newFreq > 0x7FF
          newFreq
        end

        @[AlwaysInline]
        def applyNextSweep : Nil
          newFreq : Int32 = calculateNextSweep

          if @on && @sweepShift > 0
            @frequency = newFreq.to_u16!
            @reg.unsafe_put(3, (@frequency & 0xFF).to_u8!)
          end
        end

        @[AlwaysInline]
        def tickSweep : Nil
          @sweepCount = (@sweepCount &- 1) & 7
          if @sweepCount == 0
            @sweepCount = @sweepTime

            if @sweepEnabled && @sweepTime > 0
              applyNextSweep
              calculateNextSweep
            end
          end
        end

        @[AlwaysInline]
        def tickEnvelope : Nil
          if @envelopeEnabled
            @envelopeCount = (@envelopeCount &- 1) & 7

            if @envelopeCount == 0
              @envelopeCount = @envelopeTime

              if @envelopeCount != 0
                newEnvVal : Int8 = @envelopeValue &+ @envelopeDirection

                if newEnvVal >= 0 && newEnvVal <= 15
                  @envelopeValue = newEnvVal
                else
                  @envelopeEnabled = false
                end
              end
            end
          end
        end

        @[AlwaysInline]
        def dacEnabled? : Bool
          if @channel != 3
            Yuno.bitflag?(@reg.unsafe_fetch(2), 0xF8)
          else
            Yuno.bitflag?(@reg.unsafe_fetch(0), 0x80)
          end
        end

        @[AlwaysInline]
        def updateSquareChannel(cycles : UInt32) : Nil
          if @on
            # Compensate for leftover cycles
            @cyclesLeft = @cyclesLeft &+ cycles
            return if @cyclesLeft <= 0

            cycles = @cyclesLeft >> 2
            @cyclesLeft &= 3
            distance : UInt16 = 0x800_u16 &- @frequencyCounter
            if cycles >= distance
              cycles -= distance
              distance = 0x800_u16 &- @frequency
              counter : UInt32 = 1u32 + cycles.tdiv(distance)

              @dutyCount = (@dutyCount &+ counter) & 0x07
              @signal = WAVE_DUTY_TABLE[@duty][@dutyCount]
              @frequencyCounter = @frequency &+ (cycles % distance)
            else
              @frequencyCounter += cycles
            end
          end
        end

        def updateWaveChannel(cycles : UInt32, mode : Mode, sndRegs : Bytes, boost : Bool) : Nil
          if @on
            @cyclesLeft = @cyclesLeft &+ cycles

            while @cyclesLeft >= 2
              @cyclesLeft = @cyclesLeft &- 2

              # Calculate next state
              @frequencyCounter = (@frequencyCounter &+ 1) & 0x7FF
              @sampleReading = false
              if mode.game_boy? && @frequencyCounter == 0x7FF
                @offset = (@offset &+ 1) & 0x1F
              end

              if @frequencyCounter == 0
                # Read next sample
                @sampleReading = true
                if mode.game_boy_color?
                  @offset = (@offset &+ 1) & 0x1F
                end
                @currentSample = sndRegs[AUD3W0 + @offset.tdiv(2)].to_i8!
                @currentSample >>= 4 unless Yuno.bitflag?(@offset, 1)
                @currentSample = (@currentSample & 0x0F) - 8
                @currentSample <<= 1 if boost

                @signal = if @level != 0
                            @currentSample.to_i16!.tdiv(1i16 << (@level &- 1)).to_i8!
                          else
                            0i8
                          end

                # Reload frequency counter
                @frequencyCounter = @frequency
              end
            end
          end
        end

        @[AlwaysInline]
        def updateNoiseChannel(cycles : UInt32, gb : DMGMame) : Nil
          period : UInt32 = gb.noisePeriodCycles
          @cyclesLeft = @cyclesLeft &+ cycles
          feedback : UInt16 = 0u16

          while @cyclesLeft >= period
            feedback = 0u16
            @cyclesLeft = @cyclesLeft &- period

            # Using a Polynomial Counter (aka Linear Feedback Shift Register)
            # Mode 4 has a 15 bit counter so we need to shift the bits around
            # accordingly.
            feedback = ((@noiseLfsr >> 1) ^ @noiseLfsr) & 1
            @noiseLfsr = (@noiseLfsr >> 1) | (feedback << 14)
            if @noiseShort
              @noiseLfsr = (@noiseLfsr & ~(1u16 << 6)) | (feedback << 6)
            end
            @signal = Yuno.bitflag?(@noiseLfsr, 1) ? -1i8 : 1i8
          end
        end
      end

      private class SoundCtrl
        property? on : Bool = false
        property volLeft    : UInt8 = 0
        property volRight   : UInt8 = 0
        property mode1Left  : UInt8 = 0
        property mode1Right : UInt8 = 0
        property mode2Left  : UInt8 = 0
        property mode2Right : UInt8 = 0
        property mode3Left  : UInt8 = 0
        property mode3Right : UInt8 = 0
        property mode4Left  : UInt8 = 0
        property mode4Right : UInt8 = 0
        property cycles : UInt32 = 0
        property? waveRamLocked : Bool = false

        def initialize
        end
      end

      ###
      ### Fields
      ###

      @rate : UInt32 = 0
      @snd1 : Sound = Sound.new
      @snd2 : Sound = Sound.new
      @snd3 : Sound = Sound.new
      @snd4 : Sound = Sound.new
      @sndControl : SoundCtrl = SoundCtrl.new

      @sndRegs : Bytes = Bytes.new(48)
      @cycleCtrl : RatioCtrl = RatioCtrl.new
      @mode : Mode

      @sampleRate : UInt32 = 0
      @boostWaveChn : UInt8 = 0

      ###
      ### Methods
      ###

      def initialize(clock : UInt32, @rate : UInt32, @mode : Mode)
        self.muteMask = 0
        @cycleCtrl.setRatio(clock & 0x7FFFFFFF, @rate)
      end

      @[AlwaysInline]
      def waveWrite(offset : Int, data : UInt8) : Nil
        if @snd3.on?
          case @mode
          in .game_boy?
            if @snd3.sampleReading?
              @sndRegs[AUD3W0 + @snd3.offset.tdiv(2)] = data
            end
          in .game_boy_color?
            @sndRegs[AUD3W0 + @snd3.offset.tdiv(2)] = data
          end
        else
          @sndRegs[AUD3W0 + offset] = data
        end
      end

      def soundWrite(offset : Int, data : UInt8) : Nil
        if offset < AUD3W0
          case @mode
          in .game_boy?
            # Only register NR52 is accessible if the sound controller is disabled.
            if !@sndControl.on? && offset != NR52 && offset != NR11 && offset != NR21 &&
               offset != NR31 && offset != NR41
              return
            end
          in .game_boy_color?
            # Only register NR52 is accessible if the sound controller is disabled.
            return if !@sndControl.on? && offset != NR52
          end

          soundWriteInternal(offset.to_u8!, data)
        elsif offset <= AUD3WF
          waveWrite(offset - AUD3W0, data)
        end
      end

      def reset : Nil
        mmask : UInt32 = self.muteMask

        @cycleCtrl.val = 0
        @snd1 = Sound.new
        @snd2 = Sound.new
        @snd3 = Sound.new
        @snd4 = Sound.new

        self.muteMask = mmask

        @snd1.channel = 1_u8
        @snd1.lengthMask = 0x3F_u8

        @snd2.channel = 2_u8
        @snd2.lengthMask = 0x3F_u8

        @snd3.channel = 3_u8
        @snd3.lengthMask = 0xFF_u8 # Note: different

        @snd4.channel = 4_u8
        @snd4.lengthMask = 0x3F_u8

        soundWriteInternal(NR52, 0u8)

        case @mode
        in .game_boy?
          @sndRegs[AUD3W0] = 0xAC_u8
          @sndRegs[AUD3W1] = 0xDD_u8
          @sndRegs[AUD3W2] = 0xDA_u8
          @sndRegs[AUD3W3] = 0x48_u8
          @sndRegs[AUD3W4] = 0x36_u8
          @sndRegs[AUD3W5] = 0x02_u8
          @sndRegs[AUD3W6] = 0xCF_u8
          @sndRegs[AUD3W7] = 0x16_u8
          @sndRegs[AUD3W8] = 0x2C_u8
          @sndRegs[AUD3W9] = 0x04_u8
          @sndRegs[AUD3WA] = 0xE5_u8
          @sndRegs[AUD3WB] = 0x2C_u8
          @sndRegs[AUD3WC] = 0xAC_u8
          @sndRegs[AUD3WD] = 0xDD_u8
          @sndRegs[AUD3WE] = 0xDA_u8
          @sndRegs[AUD3WF] = 0x48_u8
        in .game_boy_color?
          @sndRegs[AUD3W0] = 0x00_u8
          @sndRegs[AUD3W1] = 0xFF_u8
          @sndRegs[AUD3W2] = 0x00_u8
          @sndRegs[AUD3W3] = 0xFF_u8
          @sndRegs[AUD3W4] = 0x00_u8
          @sndRegs[AUD3W5] = 0xFF_u8
          @sndRegs[AUD3W6] = 0x00_u8
          @sndRegs[AUD3W7] = 0xFF_u8
          @sndRegs[AUD3W8] = 0x00_u8
          @sndRegs[AUD3W9] = 0xFF_u8
          @sndRegs[AUD3WA] = 0x00_u8
          @sndRegs[AUD3WB] = 0xFF_u8
          @sndRegs[AUD3WC] = 0x00_u8
          @sndRegs[AUD3WD] = 0xFF_u8
          @sndRegs[AUD3WE] = 0x00_u8
          @sndRegs[AUD3WF] = 0xFF_u8
        end
      end

      def update(outputs : OutputBuffers, samples : UInt32) : Nil
        outL : Slice(Int32) = outputs[0]
        outR : Slice(Int32) = outputs[1]
        sample : Int32 = 0
        left : Int32 = 0
        right : Int32 = 0
        cycles : UInt32 = 0

        # Check once, then we can avoid bounds checks below.
        if outL.size < samples || outR.size < samples
          raise "DMG update: Bad number of samples (expected #{samples}, " \
                "but buffers have #{outL.size} and #{outR.size})"
        end

        samples.times do |i|
          left = 0
          right = 0

          @cycleCtrl.val += @cycleCtrl.inc
          cycles = @cycleCtrl.val >> RC_SHIFT
          @cycleCtrl.val &= ((1u32 << RC_SHIFT) - 1)
          self.updateState(cycles)

          # Mode 1 - Wave with envelope and sweep
          if @snd1.on? && @snd1.muted == 0
            sample = @snd1.signal.to_i32! * @snd1.envelopeValue.to_i32!
            left = left &+ sample if @sndControl.mode1Left != 0
            right = right &+ sample if @sndControl.mode1Right != 0
          end

          # Mode 2 - Wave with envelope
          if @snd2.on? && @snd2.muted == 0
            sample = @snd2.signal.to_i32! * @snd2.envelopeValue.to_i32!
            left = left &+ sample if @sndControl.mode2Left != 0
            right = right &+ sample if @sndControl.mode2Right != 0
          end

          # Mode 3 - Wave patterns from WaveRAM
          if @snd3.on? && @snd3.muted == 0
            sample = @snd3.signal.to_i32!
            left = left &+ sample if @sndControl.mode3Left != 0
            right = right &+ sample if @sndControl.mode3Right != 0
          end

          # Mode 4 - Noise with envelope
          if @snd4.on? && @snd4.muted == 0
            sample = @snd4.signal.to_i32! * @snd4.envelopeValue.to_i32!
            left = left &+ sample if @sndControl.mode4Left != 0
            right = right &+ sample if @sndControl.mode4Right != 0
          end

          # Adjust for master volume
          left = left &* @sndControl.volLeft
          right = right &* @sndControl.volRight

          # Pump up the volume
          left <<= 6
          right <<= 6

          # Update the buffers
          outL.unsafe_put(i, left)
          outR.unsafe_put(i, right)
        end

        @sndRegs.unsafe_put(NR52, (@sndRegs.unsafe_fetch(NR52) & 0xF0) |
                                  (@snd1.on? ? 1 : 0) |
                                  ((@snd2.on? ? 1 : 0) << 1) |
                                  ((@snd3.on? ? 1 : 0) << 2) |
                                  ((@snd4.on? ? 1 : 0) << 3))
      end

      @[AlwaysInline]
      def muteMask=(mask : UInt32) : Nil
        @snd1.muted = (mask & 1).to_u8!
        @snd2.muted = ((mask >> 1) & 1).to_u8!
        @snd3.muted = ((mask >> 2) & 1).to_u8!
        @snd4.muted = ((mask >> 3) & 1).to_u8!
      end

      @[AlwaysInline]
      def muteMask : UInt32
        @snd1.muted.to_u32! |
          (@snd2.muted.to_u32! << 1) |
          (@snd3.muted.to_u32! << 2) |
          (@snd4.muted.to_u32! << 3)
      end

      def options=(flags : UInt8) : Nil
        @boostWaveChn = (flags & 1).to_u8!
      end

      ###
      ### Private Methods
      ###

      private def soundWriteInternal(offset : UInt8, data : UInt8) : Nil
        lengthWasEnabled : Bool = false

        # Store the current value.
        oldData : UInt8 = @sndRegs[offset]

        @sndRegs[offset] = data if @sndControl.on?

        case offset
        ###
        ### Mode 1
        ###

        when NR10 # Sweep (R/W)
          @snd1.reg.unsafe_put(0, data)
          @snd1.sweepShift = data & 7
          @snd1.sweepDirection = Yuno.bitflag?(data, 8) ? -1i8 : 1i8
          @snd1.sweepTime = (data & 0x70) >> 4

          if Yuno.bitflag?(oldData, 8) && !(Yuno.bitflag?(data, 8)) && @snd1.sweepNegModeUsed?
            @snd1.on = false
          end

        when NR11 # Sound length, Wave pattern duty (R/W)
          @snd1.reg.unsafe_put(1, data)
          @snd1.duty = ((data & 0xC0) >> 6).to_i8! if @sndControl.on?
          @snd1.length = data & 0x3F
          @snd1.lengthCounting = true

        when NR12 # Envelope (R/W)
          @snd1.reg.unsafe_put(2, data)
          @snd1.envelopeValue = (data >> 4).to_i8!
          @snd1.envelopeDirection = Yuno.bitflag?(data, 8) ? 1i8 : -1i8
          @snd1.envelopeTime = data & 7
          @snd1.on = false unless @snd1.dacEnabled?

        when NR13 # Frequency lo (R/W)
          @snd1.reg.unsafe_put(3, data)

          # Only enabling the frequency line breaks blarggs's sound
          # test #5.  This condition may not be correct.
          unless @snd1.sweepEnabled?
            @snd1.frequency = ((@snd1.reg.unsafe_fetch(4).to_u16! & 7) << 8) | @snd1.reg.unsafe_fetch(3)
          end

        when NR14 # Frequency hi, initialize (R/W)
          @snd1.reg.unsafe_put(4, data)

          lengthWasEnabled = @snd1.lengthEnabled?
          @snd1.lengthEnabled = Yuno.bitflag?(data, 0x40)
          @snd1.frequency = ((@sndRegs.unsafe_fetch(NR14).to_u16! & 7) << 8) | @snd1.reg.unsafe_fetch(3)

          if !lengthWasEnabled && !(Yuno.bitflag?(@sndControl.cycles, FRAME_CYCLES)) && @snd1.lengthCounting?
            @snd1.tickLength if @snd1.lengthEnabled?
          end

          if Yuno.bitflag?(data, 0x80)
            @snd1.on = true
            @snd1.envelopeEnabled = true
            @snd1.envelopeValue = (@snd1.reg.unsafe_fetch(2) >> 4).to_i8!
            @snd1.envelopeCount = @snd1.envelopeTime
            @snd1.sweepCount = @snd1.sweepTime
            @snd1.sweepNegModeUsed = false
            @snd1.signal = 0i8
            @snd1.length = @snd1.reg.unsafe_fetch(1) & 0x3F # Valley Bell: VGM log fix
            @snd1.lengthCounting = true
            @snd1.frequency = ((@snd1.reg.unsafe_fetch(4).to_u16! & 7) << 8) | @snd1.reg.unsafe_fetch(3)
            @snd1.frequencyCounter = @snd1.frequency
            @snd1.cyclesLeft = 0
            @snd1.dutyCount = 0
            @snd1.sweepEnabled = (@snd1.sweepShift != 0 || @snd1.sweepTime != 0)

            @snd1.on = false unless @snd1.dacEnabled?
            @snd1.calculateNextSweep if @snd1.sweepShift > 0

            if @snd1.length == 0 && @snd1.lengthEnabled? && !(Yuno.bitflag?(@sndControl.cycles, FRAME_CYCLES))
              @snd1.tickLength
            end
          else
            # This condition may not be correct.
            unless @snd1.sweepEnabled?
              @snd1.frequency = ((@snd1.reg.unsafe_fetch(4).to_u16! & 7) << 8) | @snd1.reg.unsafe_fetch(3)
            end
          end

        ###
        ### Mode 2
        ###

        when NR21 # Sound length, wave pattern duty (R/W)
          @snd2.reg.unsafe_put(1, data)
          @snd2.duty = ((data & 0xC0) >> 6).to_i8! if @sndControl.on?
          @snd2.length = data & 0x3F
          @snd2.lengthCounting = true

        when NR22 # Envelope (R/W)
          @snd2.reg.unsafe_put(2, data)
          @snd2.envelopeValue = (data >> 4).to_i8!
          @snd2.envelopeDirection = Yuno.bitflag?(data, 8) ? 1i8 : -1i8
          @snd2.envelopeTime = data & 7
          @snd2.on = false unless @snd2.dacEnabled?

        when NR23 # Frequency lo (R/W)
          @snd2.reg.unsafe_put(3, data)
          @snd2.frequency = ((@snd2.reg.unsafe_fetch(4).to_u16! & 7) << 8) | @snd2.reg.unsafe_fetch(3)

        when NR24 # Frequency hi, initialize (R/W)
          @snd2.reg.unsafe_put(4, data)

          lengthWasEnabled = @snd2.lengthEnabled?
          @snd2.lengthEnabled = Yuno.bitflag?(data, 0x40)

          if !lengthWasEnabled && !(Yuno.bitflag?(@sndControl.cycles, FRAME_CYCLES)) && @snd2.lengthCounting?
            @snd2.tickLength if @snd2.lengthEnabled?
          end

          if Yuno.bitflag?(data, 0x80)
            @snd2.on = true
            @snd2.envelopeEnabled = true
            @snd2.envelopeValue = (@snd2.reg.unsafe_fetch(2) >> 4).to_i8!
            @snd2.envelopeCount = @snd2.envelopeTime
            @snd2.frequency = ((@snd2.reg.unsafe_fetch(4).to_u16! & 7) << 8) | @snd2.reg.unsafe_fetch(3)
            @snd2.frequencyCounter = @snd2.frequency
            @snd2.cyclesLeft = 0
            @snd2.dutyCount = 0
            @snd2.signal = 0i8
            @snd2.length = @snd2.reg.unsafe_fetch(1) & 0x3F # Valley Bell: VGM log fix
            @snd2.lengthCounting = true

            @snd2.on = false unless @snd2.dacEnabled?

            if @snd2.length == 0 && @snd2.lengthEnabled? && !(Yuno.bitflag?(@sndControl.cycles, FRAME_CYCLES))
              @snd2.tickLength
            end
          else
            @snd2.frequency = ((@snd2.reg.unsafe_fetch(4).to_u16! & 7) << 8) | @snd2.reg.unsafe_fetch(3)
          end

        ###
        ### Mode 3
        ###

        when NR30 # Sound On/Off (R/W)
          @snd3.reg.unsafe_put(0, data)
          @snd3.on = false unless @snd3.dacEnabled?

        when NR31 # Sound length (R/W)
          @snd3.reg.unsafe_put(1, data)
          @snd3.length = data
          @snd3.lengthCounting = true

        when NR32 # Select output level
          @snd3.reg.unsafe_put(2, data)
          @snd3.level = (data & 0x60) >> 5

        when NR33 # Frequency lo (W)
          @snd3.reg.unsafe_put(3, data)
          @snd3.frequency = ((@snd3.reg.unsafe_fetch(4).to_u16! & 7) << 8) | @snd3.reg.unsafe_fetch(3)

        when NR34 # Frequency hi, initialize (W)
          @snd3.reg.unsafe_put(4, data)

          lengthWasEnabled = @snd3.lengthEnabled?
          @snd3.lengthEnabled = Yuno.bitflag?(data, 0x40)

          if !lengthWasEnabled && !(Yuno.bitflag?(@sndControl.cycles, FRAME_CYCLES)) && @snd3.lengthCounting?
            @snd3.tickLength if @snd3.lengthEnabled?
          end

          if Yuno.bitflag?(data, 0x80)
            self.corruptWaveRam if @snd3.on? && @snd3.frequencyCounter == 0x7FF
            @snd3.on = true
            @snd3.offset = 0
            @snd3.duty = 1
            @snd3.dutyCount = 0
            #@snd3.length = @snd3.reg.unsafe_fetch(1) # Valley Bell: VGM log fix
            @snd3.lengthCounting = true
            @snd3.frequency = ((@snd3.reg.unsafe_fetch(4).to_u16! & 7) << 8) | @snd3.reg.unsafe_fetch(3)
            @snd3.frequencyCounter = @snd3.frequency

            # There is a tiny bit of delay in starting up the wave channel
            @snd3.cyclesLeft = 4294967290u32 # -6
            @snd3.sampleReading = false

            @snd3.on = false unless @snd3.dacEnabled?

            if @snd3.length == 0 && @snd3.lengthEnabled? && !(Yuno.bitflag?(@sndControl.cycles, FRAME_CYCLES))
              @snd3.tickLength
            end
          else
            @snd3.frequency = ((@snd3.reg.unsafe_fetch(4).to_u16! & 7) << 8) | @snd3.reg.unsafe_fetch(3)
          end

        ###
        ### Mode 4
        ###

        when NR41 # Sound length (R/W)
          @snd4.reg.unsafe_put(1, data)
          @snd4.length = data & 0x3F
          @snd4.lengthCounting = true

        when NR42 # Envelope (R/W)
          @snd4.reg.unsafe_put(2, data)
          @snd4.envelopeValue = (data >> 4).to_i8!
          @snd4.envelopeDirection = Yuno.bitflag?(data, 8) ? 1i8 : -1i8
          @snd4.envelopeTime = data & 7
          @snd4.on = false unless @snd4.dacEnabled?

        when NR43 # Polynomial counter, frequency
          @snd4.reg.unsafe_put(3, data)
          @snd4.noiseShort = Yuno.bitflag?(data, 8)

        when NR44 # Counter, consecutive, initialize (R/W)
          @snd4.reg.unsafe_put(4, data)

          lengthWasEnabled = @snd4.lengthEnabled?
          @snd4.lengthEnabled = Yuno.bitflag?(data, 0x40)

          if !lengthWasEnabled && !(Yuno.bitflag?(@sndControl.cycles, FRAME_CYCLES)) && @snd4.lengthCounting?
            @snd4.tickLength if @snd4.lengthEnabled?
          end

          if Yuno.bitflag?(data, 0x80)
            @snd4.on = true
            @snd4.envelopeEnabled = true
            @snd4.envelopeValue = (@snd4.reg.unsafe_fetch(2) >> 4).to_i8!
            @snd4.envelopeCount = @snd4.envelopeTime
            @snd4.frequencyCounter = 0
            @snd4.cyclesLeft = self.noisePeriodCycles
            @snd4.signal = -1i8
            @snd4.noiseLfsr = 0x7FFF_u16
            @snd4.length = @snd4.reg.unsafe_fetch(1) & 0x3F # Valley Bell: VGM log fix
            @snd4.lengthCounting = true

            @snd4.on = false unless @snd4.dacEnabled?
            if @snd4.length == 0 && @snd4.lengthEnabled? && !(Yuno.bitflag?(@sndControl.cycles, FRAME_CYCLES))
              @snd4.tickLength
            end
          end

        ###
        ### Control
        ###

        when NR50 # Channel control, on/off, volume (R/W)
          @sndControl.volLeft = data & 7
          @sndControl.volRight = (data & 0x70) >> 4

        when NR51 # Selection of sound output terminal
          @sndControl.mode1Right = data & 1
          @sndControl.mode1Left  = (data & 0x10) >> 4
          @sndControl.mode2Right = (data & 2) >> 1
          @sndControl.mode2Left  = (data & 0x20) >> 5
          @sndControl.mode3Right = (data & 4) >> 2
          @sndControl.mode3Left  = (data & 0x40) >> 6
          @sndControl.mode4Right = (data & 8) >> 3
          @sndControl.mode4Left  = (data & 0x80) >> 7

        when NR52 # Sound on/off (R/W)
          # Only bit 7 is writable, writing to bits 0-3 does NOT
          # enable or disable sound. They are read-only.
          unless Yuno.bitflag?(data, 0x80)
            # On DMG, the length counters are not affected and not
            # clocked.  Powering off should actually clear all
            # registers.
            apuPowerOff
          else
            unless @sndControl.on?
              @sndControl.cycles |= 7u32 * FRAME_CYCLES
            end
          end

          @sndControl.on = Yuno.bitflag?(data, 0x80)
          @sndRegs.unsafe_put(NR52, data & 0x80)
        end
      end

      @[AlwaysInline]
      private def corruptWaveRam : Nil
        return unless @mode.game_boy?

        if @snd3.offset < 8
          @sndRegs.unsafe_put(AUD3W0, @sndRegs[AUD3W0 + @snd3.offset.tdiv(2)])
        else
          4.times do |i|
            @sndRegs.unsafe_put(AUD3W0 + i,
                                @sndRegs[AUD3W0 + (@snd3.offset.tdiv(2) & ~0x03) + i])
          end
        end
      end

      private def apuPowerOff : Nil
        case @mode
        in .game_boy?
          soundWriteInternal(NR10, 0)
          @snd1.duty = 0
          @sndRegs.unsafe_put(NR11, 0)
          soundWriteInternal(NR12, 0)
          soundWriteInternal(NR13, 0)
          soundWriteInternal(NR14, 0)
          @snd1.lengthCounting = false
          @snd1.sweepNegModeUsed = false

          @sndRegs.unsafe_put(NR21, 0)
          soundWriteInternal(NR22, 0)
          soundWriteInternal(NR23, 0)
          soundWriteInternal(NR24, 0)
          @snd2.lengthCounting = false

          soundWriteInternal(NR30, 0)
          soundWriteInternal(NR32, 0)
          soundWriteInternal(NR33, 0)
          soundWriteInternal(NR34, 0)
          @snd3.lengthCounting = false
          @snd3.currentSample = 0

          @sndRegs.unsafe_put(NR41, 0)
          soundWriteInternal(NR42, 0)
          soundWriteInternal(NR43, 0)
          soundWriteInternal(NR44, 0)
          @snd4.lengthCounting = false
          @snd4.cyclesLeft = self.noisePeriodCycles

        in .game_boy_color?
          soundWriteInternal(NR10, 0)
          @snd1.duty = 0
          soundWriteInternal(NR11, 0)
          soundWriteInternal(NR12, 0)
          soundWriteInternal(NR13, 0)
          soundWriteInternal(NR14, 0)
          @snd1.lengthCounting = false
          @snd1.sweepNegModeUsed = false

          soundWriteInternal(NR21, 0)
          soundWriteInternal(NR22, 0)
          soundWriteInternal(NR23, 0)
          soundWriteInternal(NR24, 0)
          @snd2.lengthCounting = false

          soundWriteInternal(NR30, 0)
          soundWriteInternal(NR31, 0)
          soundWriteInternal(NR32, 0)
          soundWriteInternal(NR33, 0)
          soundWriteInternal(NR34, 0)
          @snd3.lengthCounting = false
          @snd3.currentSample = 0

          soundWriteInternal(NR41, 0)
          soundWriteInternal(NR42, 0)
          soundWriteInternal(NR43, 0)
          soundWriteInternal(NR44, 0)
          @snd4.lengthCounting = false
          @snd4.cyclesLeft = self.noisePeriodCycles
        end

        @snd1.on = false
        @snd2.on = false
        @snd3.on = false
        @snd4.on = false

        @sndControl.waveRamLocked = false

        ((NR44 + 1)...NR52).each do |i|
          soundWriteInternal(i, 0)
        end
      end

      private def updateState(cycles : UInt32) : Nil
        return unless @sndControl.on?

        oldCycles : UInt32 = @sndControl.cycles
        @sndControl.cycles += cycles

        if oldCycles.tdiv(FRAME_CYCLES) != @sndControl.cycles.tdiv(FRAME_CYCLES)
          # Left over cycles in current frame.
          cyclesCurrentFrame : UInt32 = FRAME_CYCLES - (oldCycles & (FRAME_CYCLES - 1))

          @snd1.updateSquareChannel(cyclesCurrentFrame)
          @snd2.updateSquareChannel(cyclesCurrentFrame)
          @snd3.updateWaveChannel(cyclesCurrentFrame, @mode, @sndRegs, @boostWaveChn != 0)
          @snd4.updateNoiseChannel(cyclesCurrentFrame, self)

          cycles -= cyclesCurrentFrame

          # Switch to next frame
          case @sndControl.cycles.tdiv(FRAME_CYCLES) & 7
          when 0
            # Length
            @snd1.tickLength
            @snd2.tickLength
            @snd3.tickLength
            @snd4.tickLength

          when 2
            # Sweep
            @snd1.tickSweep
            # Length
            @snd1.tickLength
            @snd2.tickLength
            @snd3.tickLength
            @snd4.tickLength

          when 4
            # Length
            @snd1.tickLength
            @snd2.tickLength
            @snd3.tickLength
            @snd4.tickLength

          when 6
            # Sweep
            @snd1.tickSweep
            # Length
            @snd1.tickLength
            @snd2.tickLength
            @snd3.tickLength
            @snd4.tickLength

          when 7
            # Update envelope
            @snd1.tickEnvelope
            @snd2.tickEnvelope
            @snd4.tickEnvelope
          end
        end

        @snd1.updateSquareChannel(cycles)
        @snd2.updateSquareChannel(cycles)
        @snd3.updateWaveChannel(cycles, @mode, @sndRegs, @boostWaveChn != 0)
        @snd4.updateNoiseChannel(cycles, self)
      end

      @[AlwaysInline]
      protected def noisePeriodCycles : UInt32
        NOISE_PERIOD_CYCLES_DIVISOR[@snd4.reg.unsafe_fetch(3) & 7] << (@snd4.reg.unsafe_fetch(3) >> 4)
      end
    end
  end
end
