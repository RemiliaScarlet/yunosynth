#### YunoSynth
#### Copyright (C) 2023-2024 Remilia Scarlet <remilia@posteo.jp>
#### Portions based on VGMPlay, Copyright (C) Valley Bell
####
#### This program is free software: you can redistribute it and/or modify it
#### under the terms of the GNU Affero General Public License as published by
#### the Free Software Foundation, either version 3 of the License, or (at your
#### option) any later version.
####
#### This program is distributed in the hope that it will be useful, but WITHOUT
#### ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
#### License for more details.
####
#### You should have received a copy of the GNU Affero General Public License
#### along with this program.  If not, see <https://www.gnu.org/licenses/>.
require "./emu-ym2413-emu2413"

####
#### Yamaha YM2413 sound chip emulator interface
####

module Yuno::Chips
  # YM2413 sound chip emulator.
  class YM2413 < Yuno::AbstractChip
    CHIP_ID = 0x01_u32

    enum Core
      Emu2413

      @[AlwaysInline]
      def symbol : Symbol
        case self
        in .emu2413? then :emu2413
        end
      end
    end

    # :nodoc:
    VRC7_INST = [
      # VRC7 VOICE
      # Dumped via VRC7 debug mode by Nuke.YKT
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x03, 0x21, 0x05, 0x06, 0xE8, 0x81, 0x42, 0x27,
      0x13, 0x41, 0x14, 0x0D, 0xD8, 0xF6, 0x23, 0x12,
      0x11, 0x11, 0x08, 0x08, 0xFA, 0xB2, 0x20, 0x12,
      0x31, 0x61, 0x0C, 0x07, 0xA8, 0x64, 0x61, 0x27,
      0x32, 0x21, 0x1E, 0x06, 0xE1, 0x76, 0x01, 0x28,
      0x02, 0x01, 0x06, 0x00, 0xA3, 0xE2, 0xF4, 0xF4,
      0x21, 0x61, 0x1D, 0x07, 0x82, 0x81, 0x11, 0x07,
      0x23, 0x21, 0x22, 0x17, 0xA2, 0x72, 0x01, 0x17,
      0x35, 0x11, 0x25, 0x00, 0x40, 0x73, 0x72, 0x01,
      0xB5, 0x01, 0x0F, 0x0F, 0xA8, 0xA5, 0x51, 0x02,
      0x17, 0xC1, 0x24, 0x07, 0xF8, 0xF8, 0x22, 0x12,
      0x71, 0x23, 0x11, 0x06, 0x65, 0x74, 0x18, 0x16,
      0x01, 0x02, 0xD3, 0x05, 0xC9, 0x95, 0x03, 0x02,
      0x61, 0x63, 0x0C, 0x00, 0x94, 0xC0, 0x33, 0xF6,
      0x21, 0x72, 0x0D, 0x00, 0xC1, 0xD5, 0x56, 0x06,
      0x01, 0x01, 0x18, 0x0F, 0xDF, 0xF8, 0x6A, 0x6D,
      0x01, 0x01, 0x00, 0x00, 0xC8, 0xD8, 0xA7, 0x68,
      0x05, 0x01, 0x00, 0x00, 0xF8, 0xAA, 0x59, 0x55,
    ] of UInt8

    @clockFromHeader : UInt32 = 0
    @chip : YM2413Emu2413|Nil = nil
    @samplingMode : UInt8 = 0
    @chipSampleRate : UInt32 = 0
    @lastReg : UInt8 = 0
    @subType : UInt8 = 0

    def initialize(chipNum : Int32, absChipCount : Int32, vgm : VgmFile, playbackSampleRate : UInt32,
                   newSamplingMode : UInt8, newPlayerSampleRate : UInt32, *, emuCore : Core|Symbol|Nil = nil,
                   flags : ChipFlags? = nil)
      @clockFromHeader = vgm.getAlternateChipClock(chipNum, Yuno::ChipType::Ym2413) || vgm.header.ym2413Clock
      @clockFromHeader &= 0xBFFFFFFF
      @subType = Yuno.bitflag?(@clockFromHeader, 0x80000000) ? 0x01_u8 : 0x00_u8
      @playerSampleRate = newPlayerSampleRate
      chipCount = (vgm.header.ym2413Clock & 0x40000000) != 0 ? 2 : 1
      initFields(vgm, emuCore, playbackSampleRate, newSamplingMode, chipCount)
    end

    protected def initialize
    end

    protected def initFields(vgm : VgmFile, emuCore : Core|Symbol|Nil, playbackSampleRate : UInt32,
                             newSamplingMode : UInt8, chipCount : Int32) : Nil
      @volume = vgm.getChipVolume(self, ChipType::Ym2413, chipCount)
      @samplingMode = newSamplingMode
      @core = case emuCore
              when Core then emuCore.symbol
              when Symbol then emuCore
              else YM2413.defaultEmuCore
              end
      case @core
      when :emu2413
        @chipSampleRate = @clockFromHeader <= 0 ? playbackSampleRate : @clockFromHeader
      else raise YunoError.new("Unsupported emulation core for YM2413: #{@core}")
      end
    end

    def type : ChipType
      ChipType::Ym2413
    end

    def name : String
      case @subType
      when 1 then "Konami VRC7"
      else "Yamaha YM2413"
      end
    end

    def shortName : String
      case @subType
      when 1 then "VRC7"
      else "YM2413"
      end
    end

    def id : UInt32
      CHIP_ID
    end

    def emuCore : Symbol
      @core
    end

    def self.defaultEmuCore : Symbol
      :emu2413
    end

    def start(chipIndex : UInt8, clock : UInt32, flags : ChipFlags? = nil, previousChip : AbstractChip? = nil) : UInt32
      mode = (clock & 0x80000000) >> 31
      clock &= 0x7FFFFFFF

      # Update sample rate
      @sampleRate = clock.tdiv(72)
      if (@samplingMode == 0x01 && @sampleRate < @chipSampleRate) || @samplingMode == 0x02
        @sampleRate = @chipSampleRate
      end

      # Note: VRC7 instruments are set in #reset if necessary.
      case @core
      when :emu2413
        @chip = YM2413Emu2413.new(clock, @sampleRate)
        @chip.as(YM2413Emu2413).setChipType(mode)
        if mode != 0
          @chip.as(YM2413Emu2413).patch = VRC7_INST
        end
        @sampleRate
      else raise YunoError.new("Unsupported emulation core for YM2413: #{@core}")
      end
    end

    def getClock : UInt32
      @clockFromHeader
    end

    def getStartFlags(vgm : VgmFile) : ChipFlags?
      nil
    end

    def update(outputs : OutputBuffers, samples : Int) : Nil
      ch = @chip.not_nil!
      case ch
      when YM2413Emu2413 then ch.update(outputs, samples.to_u32!)
      end
    end

    def reset(chipIndex : UInt8) : Nil
      ch = @chip.not_nil!
      case ch
      when YM2413Emu2413 then ch.reset
      else raise "Unexpected YM2413 type in YM2413#reset"
      end
    end

    def read(chipIndex : UInt8, offset : Int) : UInt8|UInt16|UInt32
      0u8
    end

    def write(chipIndex : UInt8, offset : Int, data : Int, port : UInt8 = 0) : Nil
      ch = @chip.not_nil!
      case ch
      when YM2413Emu2413 then ch.writeIO((offset & 1).to_u32!, data.to_u8!)
      else raise "Unexpected YM2413 type in YM2413#write"
      end
    end

    def writeDac(port : Int, command : Int, data : Int) : Nil
      write(0, 0x00, command)
      write(0, 0x01, data)
    end

    def setMuteMask(chipIndex : UInt8, mask : UInt32) : Nil
      ch = @chip.not_nil!
      case ch
      when YM2413Emu2413 then ch.mask = mask
      else raise "Unexpected YM2413 type in YM2413#setMuteMask"
      end
    end

    def getVolModifier : UInt32
      @volume.to_u32!.tdiv(2)
    end

    def baseVolume : UInt16
      0x200_u16
    end

    # The internal interface for the chip.
    @[AlwaysInline]
    def chip : AbstractEmulator
      @chip.not_nil!
    end
  end
end
