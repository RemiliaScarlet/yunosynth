#### Yamaha YMZ280B driver
####  Copyright (C) Aaron Giles
####  Copyright (C) 2023-2024 Remilia Scarlet
####  BSD 3-Clause License
####
####  YMZ280B 8-Channel PCMD8 PCM/ADPCM Decoder
####
#### Features as listed in LSI-4MZ280B3 data sheet:
####  Voice data stored in external memory can be played back simultaneously for up to eight voices
####  Voice data format can be selected from 4-bit ADPCM, 8-bit PCM and 16-bit PCM
####  Control of voice data external memory
####   Up to 16M bytes of ROM or SRAM (x 8 bits, access time 150ms max) can be connected
####   Continuous access is possible
####   Loop playback between selective addresses is possible
####  Voice data playback frequency control
####   4-bit ADPCM ................ 0.172 to 44.1kHz in 256 steps
####   8-bit PCM, 16-bit PCM ...... 0.172 to 88.2kHz in 512 steps
####  256 steps total level and 16 steps panpot can be set
####  Voice signal is output in stereo 16-bit 2's complement MSB-first format
####
####  TODO:
####  - Is memory handling 100% correct? At the moment, Konami firebeat.c is the only
####    hardware currently emulated that uses external handlers.
####    It also happens to be the only one using 16-bit PCM.
####
####    Some other drivers (eg. bishi.c, bfm_sc4/5.c) also use ROM readback.

####
#### Yamaha YMZ280B sound chip emulator interface
####

module Yuno::Chips
  # Yamaha YMZ280B sound chip emulator.
  class YMZ280B < Yuno::AbstractChip
    private class YMZ280BMame < Yuno::AbstractEmulator
      MAX_SAMPLE_CHUNK = 0x10000_u32

      FRAC_BITS = 14_u32
      FRAC_ONE = (1u32 << FRAC_BITS)
      FRAC_MASK = (FRAC_ONE - 1)

      INTERNAL_BUFFER_SIZE = (1u32 << 15)

      INDEX_SCALE = [
        0x0E6, 0x0E6, 0x0E6, 0x0E6, 0x133, 0x199, 0x200, 0x266
      ]

      class Voice
        property playing : UInt8 = 0
        property keyOn : UInt8 = 0
        property looping : UInt8 = 0
        property mode : UInt8 = 0           # Playback mode
        property fnum : UInt16 = 0          # Frequency
        property level : UInt8 = 0          # Output level
        property pan : UInt8 = 0            # Panning

        property start : UInt32 = 0         # Start address, in nibbles
        property stop : UInt32 = 0          # Stop address, in nibbles

        property loopStart : UInt32 = 0     # loop start address, in nibbles
        property loopEnd : UInt32 = 0       # loop end address, in nibbles
        property position : UInt32 = 0      # current position, in nibbles

        property signal : Int32 = 0         # current ADPCM signal
        property step : Int32 = 0           # current ADPCM step

        property loopSignal : Int32 = 0     # signal at loop start
        property loopStep : Int32 = 0       # step at loop start
        property loopCount : UInt32 = 0     # number of loops so far

        property outputLeft : Int32 = 0      # output volume (left)
        property outputRight : Int32 = 0     # output volume (right)
        property outputStep : Int32 = 0      # step value for frequency conversion
        property outputPos : Int32 = 0       # current fractional position
        property lastSample : Int16 = 0      # last sample output
        property curSample : Int16 = 0       # current sample target
        property? irqSchedule : Bool = false # True if the IRQ state is updated by timer
        property muted : UInt8 = 0           # used for muting

        @masterClock : Float64
        @rate : Float64

        def initialize(@masterClock : Float64, @rate : Float64)
        end

        @[AlwaysInline]
        def updateStep : Nil
          frequency : Float64 = if @mode == 1
                                  @masterClock * ((@fnum & 0x0FF).to_u32! + 1).to_f64! * (1.0 / 256.0)
                                else
                                  @masterClock * ((@fnum & 0x1FF).to_u32! + 1).to_f64! * (1.0 / 256.0)
                                end
          @outputStep = (frequency * FRAC_ONE.to_f64! / @rate).to_i32!
        end

        @[AlwaysInline]
        def updateVolumes : Nil
          if @pan == 8
            @outputLeft = @level.to_i32!
            @outputRight = @level.to_i32!
          elsif @pan < 8
            @outputLeft = @level.to_i32!
            @outputRight = (@pan == 0 ? 0 : (@level.to_i32! * (@pan.to_i32! - 1)).tdiv(7))
          else
            @outputLeft = (@level.to_i32! * (15 - @pan.to_i32!)).tdiv(7)
            @outputRight = @level.to_i32!
          end
        end
      end

      @regionBase : Bytes = Bytes.new(0)
      @currentRegister : UInt8 = 0
      @statusRegister : UInt8 = 0
      @irqState : UInt8 = 0
      @irqMask : UInt8 = 0
      @irqEnable : UInt8 = 0
      @keyOnEnable : UInt8 = 0
      @extMemEnable : UInt8 = 0
      @extReadLatch : UInt8 = 0
      @extMemAddressHigh : UInt32 = 0
      @extMemAddressMid : UInt32 = 0
      @extMemAddress : UInt32 = 0

      @masterClock : Float64 = 0.0
      @rate : Float64 = 0.0
      @voices : Array(Voice)
      @scratch : Slice(Int16) = Slice(Int16).new(0, 0i16)

      @diffLookup : Array(Int32) = Array(Int32).new(16, 0)

      def initialize(clock : UInt32)
        computeTables

        @masterClock = clock / 384.0
        @rate = @masterClock * 2.0

        @voices = Array(Voice).new(8) { |_| Voice.new(@masterClock, @rate) }
        @scratch = Slice(Int16).new(MAX_SAMPLE_CHUNK.to_i32!, 0i16)

        unmuteAll
      end

      def update(outputs : OutputBuffers, samples : UInt32) : Nil
        outL : Slice(Int32) = outputs[0]
        outR : Slice(Int32) = outputs[1]

        # Check once, then we can avoid bounds checks below.
        if outL.size < samples || outR.size < samples
          raise "YMZ280B update: Bad number of samples (expected #{samples}, " \
                "but buffers have #{outL.size} and #{outR.size})"
        end

        outL.fill(0, 0, samples)
        outR.fill(0, 0, samples)

        prev : Int16 = 0
        cur : Int16 = 0
        newSamples : UInt32 = 0
        samplesLeft : UInt32 = 0
        finalPos : UInt32 = 0
        remaining : UInt32 = 0
        lvol : Int32 = 0
        rvol : Int32 = 0
        interpSample : Int64 = 0
        posL = 0
        posR = 0
        curDataPos = 0

        @voices.each do |voice|
          next if voice.muted != 0

          lvol = voice.outputLeft
          rvol = voice.outputRight
          prev = voice.lastSample
          cur = voice.curSample
          curDataPos = 0
          posL = 0
          posR = 0
          remaining = samples

          # Quick out if we're not playing and we're at 0
          if voice.playing == 0 && cur == 0 && prev == 0
            voice.outputPos = FRAC_ONE.to_i32!
            next
          end

          # Finish off the current sample.
          # Interpolate.
          while remaining > 0 && voice.outputPos < FRAC_ONE
            interpSample = ((prev.to_i64! * (FRAC_ONE.to_i64! - voice.outputPos.to_i64!)) +
                             (cur.to_i64! * voice.outputPos)) >> FRAC_BITS
            outL.unsafe_put(posL, outL.unsafe_fetch(posL) + (interpSample * lvol).to_i32!)
            outR.unsafe_put(posR, outR.unsafe_fetch(posR) + (interpSample * rvol).to_i32!)
            posL += 1
            posR += 1
            remaining -= 1
            voice.outputPos += voice.outputStep
          end

          # If we're over, continue.  Otherwise we're done.
          if voice.outputPos >= FRAC_ONE.to_i32!
            voice.outputPos -= FRAC_ONE.to_i32!
          else
            next
          end

          # Compute how many new samples we need.
          finalPos = (voice.outputPos + remaining * voice.outputStep).to_u32!
          newSamples = (finalPos + FRAC_ONE) >> FRAC_BITS
          newSamples = MAX_SAMPLE_CHUNK if newSamples > MAX_SAMPLE_CHUNK
          samplesLeft = newSamples

          # Generate them into our buffer
          samplesLeft = case voice.playing.to_u32! << 7 | voice.mode
                        when 0x81 then generateAdpcm(voice, newSamples)
                        when 0x82 then generatePcm8(voice, newSamples)
                        when 0x83 then generatePcm16(voice, newSamples)
                        else
                          @scratch.fill(0, 0, newSamples)
                          0u32
                        end

          # If there are leftovers, ramp back to 0
          if samplesLeft != 0
            # Note: samplesLeft bit 16 is set if the voice was finished at the
            # same time the function ended.
            samplesLeft &= 0xFFFF
            base : UInt32 = newSamples - samplesLeft
            t : Int32 = (base == 0 ? cur : @scratch[base - 1]).to_i32!

            samplesLeft.times do |i|
              if t < 0
                t = -((-t * 15) >> 4)
              elsif t > 0
                t = (t * 15) >> 4
              end
              @scratch[base + i] = t.to_i16!
            end

            # If we hit the end and IRQs are enabled, signal it.
            if base != 0
              voice.playing = 0
              voice.irqSchedule = true
            end
          end

          # Advance forward one sample
          prev = cur
          cur = @scratch[curDataPos]
          curDataPos += 1

          # Then sample-rate convert with linear interpolation
          while remaining > 0
            # Interpolate
            while remaining > 0 && voice.outputPos < FRAC_ONE
              interpSample = ((prev.to_i64! * (FRAC_ONE.to_i64! - voice.outputPos.to_i64!)) +
                              (cur.to_i64! * voice.outputPos)) >> FRAC_BITS
              outL.unsafe_put(posL, outL.unsafe_fetch(posL) + (interpSample * lvol).to_i32!)
              outR.unsafe_put(posR, outR.unsafe_fetch(posR) + (interpSample * rvol).to_i32!)
              posL += 1
              posR += 1
              remaining -= 1
              voice.outputPos += voice.outputStep
            end

            # If we're over, grab the next samples
            if voice.outputPos >= FRAC_ONE
              voice.outputPos -= FRAC_ONE.to_i32!
              prev = cur
              cur = @scratch[curDataPos]
              curDataPos += 1
            end
          end

          # Remember the last samples
          voice.lastSample = prev
          voice.curSample = cur
        end

        samples.times do |v|
          outL.unsafe_put(v, outL.unsafe_fetch(v) >> 8)
          outR.unsafe_put(v, outR.unsafe_fetch(v) >> 8)
        end

        @voices.size.times { |i| updateIrqStateTimerCommon(i) }
      end

      def start : UInt32
        @rate.to_u32!
      end

      def unmuteAll : Nil
        @voices.each &.muted=(0)
      end

      def reset : Nil
        0xFF.downto(0) do |i|
          next if i == 0x83 || (i >= 0x58 && i <= 0xFD)
          @currentRegister = i.to_u8!
          writeToRegister(0)
        end

        @currentRegister = 0
        @statusRegister = 0

        # Clear other voice parameters
        @voices.each do |voice|
          voice.curSample = 0
          voice.lastSample = 0
          voice.outputPos = FRAC_ONE.to_i32
          voice.playing = 0
        end
      end

      def writeToRegister(data : UInt32) : Nil
        # Lower registers follow a pattern
        if @currentRegister < 0x80
          voice = @voices[(@currentRegister >> 2) & 7]

          case @currentRegister & 0xE3
          when 0x00 # Pitch low 8 bits
            voice.fnum = (voice.fnum & 0x100) | (data & 0xFF)
            voice.updateStep

          when 0x01 # Pitch upper 1 bit, loop, key on, mode...
            voice.fnum = (voice.fnum & 0xFF) | ((data & 0x01) << 8)
            voice.looping = ((data & 0x10) >> 4).to_u8!

            if (data & 0x60) == 0
              data &= 0x7F # Ignore mode setting and set to same state as KON=0
            else
              voice.mode = ((data & 0x60) >> 5).to_u8!
            end

            if voice.keyOn == 0 && (data & 0x80) != 0 && @keyOnEnable != 0
              voice.playing = 1
              voice.position = voice.start
              voice.signal = 0
              voice.loopSignal = 0
              voice.step = 0x7F
              voice.loopStep = 0x7F
              voice.loopCount = 0
              voice.irqSchedule = false # If updateIrqStateTimer is set, cancel it
            elsif voice.keyOn != 0 && (data & 0x80) == 0
              voice.playing = 0
              voice.irqSchedule = false # If updateIrqStateTimer is set, cancel it
            end

            voice.keyOn = ((data & 0x80) >> 7).to_u8!
            voice.updateStep

          when 0x02 # Total level
            voice.level = data.to_u8!
            voice.updateVolumes

          when 0x03 # Pan
            voice.pan = (data & 0x0F).to_u8!
            voice.updateVolumes

          when 0x20 # Start address high
            voice.start = (voice.start & (0x00FFFF << 1)) | (data << 17)

          when 0x21 # Loop start address high
            voice.loopStart = (voice.loopStart & (0x00FFFF << 1)) | (data << 17)

          when 0x22 # Loop end address high
            voice.loopEnd = (voice.loopEnd & (0x00FFFF << 1)) | (data << 17)

          when 0x23 # Stop address high
            voice.stop = (voice.stop & (0x00FFFF << 1)) | (data << 17)

          when 0x40 # Start address middle
            voice.start = (voice.start & (0xFF00FF << 1)) | (data << 9)

          when 0x41 # Loop start address middle
            voice.loopStart = (voice.loopStart & (0xFF00FF << 1)) | (data << 9)

          when 0x42 # Loop end address middle
            voice.loopEnd = (voice.loopEnd & (0xFF00FF << 1)) | (data << 9)

          when 0x43 # Stop address middle
            voice.stop = (voice.stop & (0xFF00FF << 1)) | (data << 9)

          when 0x60 # Start address low
            voice.start = (voice.start & (0xFFFF00 << 1)) | (data << 1)

          when 0x61 # Loop start address low
            voice.loopStart = (voice.loopStart & (0xFFFF00 << 1)) | (data << 1)

          when 0x62 # Loop end address low
            voice.loopEnd = (voice.loopEnd & (0xFFFF00 << 1)) | (data << 1)

          when 0x63 # Stop address low
            voice.stop = (voice.stop & (0xFFFF00 << 1)) | (data << 1)

          else
            {% if flag?(:yunosynth_debug) %}
              Yuno.warn("YMZ280B: Unknown register write: #{@currentRegister} = #{data}")
            {% end %}
          end
        else # if @currentRegister < 0x80
          # Upper registers are special
          case @currentRegister
               ##
               ## DSP related (not implemented yet
               ##
          when 0x80, 0x81, 0x82
            {% if flag?(:yunosynth_debug) %}
              Yuno.warn("YMZ280B: DSP register write: #{@currentRegister} = #{data}")
            {% else %}
              nil
            {% end %}

          when 0x84 # ROM readback / RAM write (high)
            @extMemAddressHigh = data << 16

          when 0x85 # ROM readback / RAM write (middle)
            @extMemAddressMid = data << 8

          when 0x86 # ROM readback / RAM write (low) -> update latch
            @extMemAddress = @extMemAddressHigh | @extMemAddressMid | data
            if @extMemEnable != 0
              @extReadLatch = readMemory(@extMemAddress)
            end

          when 0x87 # RAM write
            if @extMemEnable != 0
              @extMemAddress = (@extMemAddress + 1) & 0xFFFFFF
            end

          when 0xFE # IRQ mask
            @irqMask = data.to_u8!
            updateIrqState

          when 0xFF # IRQ enable, test, etc.
            @extMemEnable = ((data & 0x40) >> 6).to_u8!
            @irqEnable = ((data & 0x10) >> 4).to_u8!
            updateIrqState

            if @keyOnEnable != 0 && (data & 0x80) == 0
              @voices.each do |v|
                v.playing = 0
                v.irqSchedule = false
              end
            elsif @keyOnEnable == 0 && (data & 0x80) != 0
              @voices.each do |v|
                v.playing = 1 if v.keyOn != 0 && v.looping != 0
              end
            end

            @keyOnEnable = ((data & 0x80) >> 7).to_u8!

          else
            {% if flag?(:yunosynth_debug) %}
              Yuno.warn("YMZ280B: Unknown register write: #{@currentRegister} = #{data}")
            {% end %}
          end
        end
      end

      @[AlwaysInline]
      def read(offset : Int) : UInt8
        if (offset & 1) == 0
          if @extMemEnable == 0
            0xFFu8
          else
            #ret = @extReadLatch
            ret = readMemory(@extMemAddress)
            @extMemAddress = (@extMemAddress + 1) & 0xFFFFFF
            ret
          end
        else
          computeStatus
        end
      end

      @[AlwaysInline]
      def write(offset : Int, data : UInt8) : Nil
        if (offset & 1) == 0
          @currentRegister = data
        else
          writeToRegister(data.to_u32!)
        end
      end

      def writeRom(romSize : Int32, dataStart : Int32, dataLength : Int32, romData : Slice(UInt8)) : Nil
        if @regionBase.size != romSize
          Yuno.dlog!("Resizing memory region (was #{@regionBase.size}, need #{romSize})")
          @regionBase = Bytes.new(romSize, 0xFF_u8)
        end

        return if dataStart > romSize
        dataLength = romSize - dataStart if dataStart + dataLength > romSize
        dataLength.times do |i|
          @regionBase[dataStart + i] = romData[i]
        end
      end

      def muteMask=(mask : UInt32)
        @voices.each_with_index do |v, idx|
          v.muted = ((muteMask >> idx) & 0x01).to_u8!
        end
      end

      @[AlwaysInline]
      private def computeStatus : UInt8
        result = @statusRegister
        @statusRegister = 0
        updateIrqState
        result
      end

      @[AlwaysInline]
      private def updateIrqState : Nil
        irqBits : Int32 = (@statusRegister & @irqMask).to_i32!

        # Always off if the enable is off.
        irqBits = 0 if @irqEnable == 0

        # Update the state if it changed
        if irqBits != 0 && @irqState == 0
          @irqState = 1u8
        elsif irqBits == 0 && @irqState != 0
          @irqState = 0
        end
      end

      @[AlwaysInline]
      private def readMemory(offset : UInt32) : UInt8
        offset &= 0xFFFFFF
        if offset < @regionBase.size
          @regionBase.unsafe_fetch(offset)
        else
          0u8
        end
      end

      @[AlwaysInline]
      private def updateIrqStateTimerCommon(voiceNum : Int32) : Nil
        voice = @voices[voiceNum]
        return unless voice.irqSchedule?
        voice.playing = 0
        @statusRegister |= (1u32 << voiceNum).to_u8!
        updateIrqState
        voice.irqSchedule = false
      end

      private def computeTables : Nil
        16.times do |nib|
          value = (nib & 0x07) * 2 + 1
          @diffLookup[nib] = ((nib & 0x08) != 0 ? -value : value)
        end
      end

      private def generateAdpcm(voice : Voice, samples : Int) : UInt32
        position : UInt32 = voice.position
        signal : Int32 = voice.signal
        step : Int32 = voice.step
        val : Int32 = 0
        bufferPos = 0

        # Two cases: first case is non-looping.
        if voice.looping == 0
          # Loop while we still have samples to generate
          while samples > 0
            # Compute the new amplitude and update the current step.
            val = readMemory(position.tdiv(2)).to_i32! >> ((~position & 1) << 2).to_i32!
            signal = (signal * 254).tdiv(256)
            signal += (step * @diffLookup[val & 15]).tdiv(8)

            # Clamp to maximum
            signal = signal.clamp(-32768, 32767)

            # Adjust the step size, then clamp it.
            step = ((step * INDEX_SCALE[val & 7]) >> 8).clamp(0x7F, 0x6000)

            # Output to the buffer, scaling by the volume
            @scratch[bufferPos] = signal.to_i16!
            bufferPos += 1
            samples -= 1

            # Advance
            position += 1
            if position >= voice.stop
              samples |= 0x10000 if samples == 0
              break
            end
          end
        else
          # Second case: looping
          while samples > 0
            # Compute the new amplitude and update the current step.
            val = readMemory(position.tdiv(2)).to_i32! >> ((~position & 1) << 2).to_i32!
            signal = (signal * 254).tdiv(256)
            signal += (step * @diffLookup[val & 15]).tdiv(8)

            # Clamp to maximum
            signal = signal.clamp(-32768, 32767)

            # Adjust the step size, then clamp it.
            step = ((step * INDEX_SCALE[val & 7]) >> 8).clamp(0x7F, 0x6000)

            # Output to the buffer, scaling by the volume
            @scratch[bufferPos] = signal.to_i16!
            bufferPos += 1
            samples -= 1

            # Advance
            position += 1
            if position == voice.loopStart && voice.loopCount == 0
              voice.loopSignal = signal
              voice.loopStep = step
            end

            if position >= voice.loopEnd
              if voice.keyOn != 0
                position = voice.loopStart
                signal = voice.loopSignal
                step = voice.loopStep
                voice.loopCount += 1
              end
            end

            if position >= voice.stop
              samples |= 0x10000 if samples == 0
              break
            end
          end
        end

        # Update parameters
        voice.position = position
        voice.signal = signal
        voice.step = step

        samples
      end

      private def generatePcm8(voice : Voice, samples : Int) : UInt32
        position : UInt32 = voice.position
        val : Int32 = 0
        bufferPos = 0

        # Two cases: first case is non-looping
        if voice.looping == 0
          # Loop while we still have samples to generate
          while samples > 0
            val = readMemory(position.tdiv(2)).to_i32!

            # Output to the buffer, scaling by the volume.
            #
            # To Int8 first to truncate it, then to Int16 for the
            # multiplication and storage.
            @scratch[bufferPos] = val.to_i8!.to_i16! * 256
            bufferPos += 1
            samples -= 1

            # Advance
            position += 2
            if position >= voice.stop
              samples |= 0x10000 if samples == 0
              break
            end
          end
        else
          # Second case: looping
          while samples > 0
            val = readMemory(position.tdiv(2)).to_i32!

            # Output to the buffer, scaling by the volume.
            #
            # To Int8 first to truncate it, then to Int16 for the
            # multiplication and storage.
            @scratch[bufferPos] = val.to_i8!.to_i16! * 256
            bufferPos += 1
            samples -= 1

            # Advance
            position += 2
            if position >= voice.loopEnd
              position = voice.loopStart if voice.keyOn != 0
            end

            if position >= voice.stop
              samples |= 0x10000 if samples == 0
              break
            end
          end
        end

        # Update the parameters
        voice.position = position

        samples
      end

      private def generatePcm16(voice : Voice, samples : Int) : UInt32
        position : UInt32 = voice.position
        val : Int16 = 0
        bufferPos = 0

        # Two cases: first case is non-looping
        if voice.looping == 0
          # Loop while we still have samples to generate
          while samples > 0
            # Fetch the current value.
            # The manual says "16-bit 2's complement MSB-first format".
            val = ((readMemory(position.tdiv(2)).to_i32! << 8) +
                   readMemory(position.tdiv(2) + 1).to_i32!).to_i16!

            # Output to the buffer, scaling by the volume
            @scratch[bufferPos] = val
            bufferPos += 1
            samples -= 1

            # Advance
            position += 4
            if position >= voice.stop
              samples |= 0x10000 if samples == 0
              break
            end
          end
        else
          # Second case: looping
          while samples > 0
            # Fetch the current value.
            # The manual says "16-bit 2's complement MSB-first format".
            val = ((readMemory(position.tdiv(2)).to_i32! << 8) +
                   readMemory(position.tdiv(2) + 1).to_i32!).to_i16!

            # Output to the buffer, scaling by the volume
            @scratch[bufferPos] = val
            bufferPos += 1
            samples -= 1

            # Advance
            position += 4
            if position >= voice.loopEnd
              position = voice.loopStart if voice.keyOn != 0
            end

            if position >= voice.stop
              samples |= 0x10000 if samples == 0
              break
            end
          end
        end

        # Update the parameters
        voice.position = position

        samples
      end
    end
  end
end
