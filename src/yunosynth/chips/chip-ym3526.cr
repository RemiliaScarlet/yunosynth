#### YunoSynth
#### Copyright (C) 2023-2024 Remilia Scarlet <remilia@posteo.jp>
#### Based on VGMPlay, Copyright (C) Valley Bell
####
#### This program is free software: you can redistribute it and/or
#### modify it under the terms of the GNU Affero General Public
#### License as published by the Free Software Foundation, either
#### version 3 of the License, or (at your option) any later version.
####
#### This program is distributed in the hope that it will be useful,
#### but WITHOUT ANY WARRANTY; without even the implied warranty of
#### MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#### Affero General Public License for more details.
####
#### You should have received a copy of the GNU Affero General Public License
#### along with this program.  If not, see <https://www.gnu.org/licenses/>.
require "./emu-opl-mame/opl-mame-ym3526"

####
#### Yamaha YM3526 sound chip emulator interface
####

module Yuno::Chips
  # YM3526 sound chip emulator.
  class YM3526 < Yuno::AbstractChip
    CHIP_ID = 0x0A_u32

    @clockFromHeader : UInt32 = 0
    @chip : YM3526Mame? = nil
    @samplingMode : UInt8 = 0
    @chipSampleRate : UInt32 = 0

    def initialize(chipNum : Int32, absChipCount : Int32, vgm : VgmFile, playbackSampleRate : UInt32,
                   newSamplingMode : UInt8, newPlayerSampleRate : UInt32, *, emuCore : Symbol? = nil,
                   flags : ChipFlags? = nil)
      @clockFromHeader = vgm.getAlternateChipClock(chipNum, Yuno::ChipType::Ym3526) || vgm.header.ym3526Clock
      @clockFromHeader &= 0x3FFFFFFF
      @playerSampleRate = newPlayerSampleRate
      chipCount = (vgm.header.ym3526Clock & 0x40000000) != 0 ? 2 : 1
      initFields(vgm, emuCore, playbackSampleRate, newSamplingMode, chipCount)
    end

    protected def initialize
    end

    protected def initFields(vgm : VgmFile, emuCore : Symbol?, playbackSampleRate : UInt32, newSamplingMode : UInt8,
                             chipCount : Int32) : Nil
      @volume = vgm.getChipVolume(self, ChipType::Ym3526, chipCount)
      @core = emuCore || YM3526.defaultEmuCore
      case @core
      when :mame
        @samplingMode = newSamplingMode
        @chipSampleRate = @clockFromHeader <= 0 ? playbackSampleRate : @clockFromHeader
      else raise YunoError.new("Unsupported emulation core for YM3526: #{@core}")
      end
    end

    def type : ChipType
      ChipType::Ym3526
    end

    def name : String
      "Yamaha YM3526"
    end

    def shortName : String
      "YM3526"
    end

    def id : UInt32
      CHIP_ID
    end

    def emuCore : Symbol
      @core
    end

    def self.defaultEmuCore : Symbol
      :mame
    end

    def start(chipIndex : UInt8, clock : UInt32, flags : ChipFlags? = nil, previousChip : AbstractChip? = nil) : UInt32
      # Update sample rate
      @sampleRate = clock.tdiv(72)
      if (@samplingMode == 0x01 && @sampleRate < @chipSampleRate) || @samplingMode == 0x02
        @sampleRate = @chipSampleRate
      end

      case @core
      when :mame
        newChip = YM3526Mame.new(clock, @sampleRate)
        # Remi: Don't set the IRQ handler, it isn't used.
        # TODO: Remove the IRQ handler from the emulation core.
        newChip.setUpdateHandler(->doStreamUpdate(AbstractEmulator), newChip)
        @chip = newChip

      else raise YunoError.new("Unsupported emulation core for YM3526: #{@core}")
      end
      @sampleRate
    end

    private def doStreamUpdate(chip : AbstractEmulator) : Nil
      @chip.as(YM3526Mame).update(Yuno::FAKE_BUF, 0)
    end

    def getClock : UInt32
      @clockFromHeader
    end

    def update(outputs : OutputBuffers, samples : Int) : Nil
      @chip.not_nil!.update(outputs, samples.to_u32!)
    end

    def reset(chipIndex : UInt8) : Nil
      @chip.not_nil!.reset
    end

    def read(chipIndex : UInt8, offset : Int) : UInt8|UInt16|UInt32
      @chip.not_nil!.read(offset & 1)
    end

    def write(chipIndex : UInt8, offset : Int, data : Int, port : UInt8 = 0) : Nil
      @chip.not_nil!.write(offset.to_i32! & 1, data.to_i32!)
    end

    def writeDac(port : Int, command : Int, data : Int) : Nil
      write(0, 0x00, command)
      write(0, 0x01, data)
    end

    @[AlwaysInline]
    def statusPortRead(chipIndex : UInt8, offset : Int) : UInt8
      read(chipIndex, 0).to_u8!
    end

    @[AlwaysInline]
    def readPortRead(chipIndex : UInt8, offset : Int) : UInt8
      read(chipIndex, 1).to_u8!
    end

    @[AlwaysInline]
    def controlPortWrite(chipIndex : UInt8, offset : Int, data : UInt8) : Nil
      write(chipIndex, 0, data)
    end

    @[AlwaysInline]
    def writePortWrite(chipIndex : UInt8, offset : Int, data : UInt8) : Nil
      write(chipIndex, 1, data)
    end

    def setMuteMask(chipIndex : UInt8, mask : UInt32) : Nil
      @chip.not_nil!.muteMask = mask
    end

    def getVolModifier : UInt32
      @volume.to_u32! * 2
    end

    def baseVolume : UInt16
      0x100_u16
    end

    @[AlwaysInline]
    def updateRequest : Nil
      @chip.not_nil!.update(Yuno::FAKE_BUF, 0)
    end

    # The internal interface for the chip.
    @[AlwaysInline]
    def chip : AbstractEmulator
      @chip.not_nil!
    end

    @[AlwaysInline]
    def writeRom(romSize : Int32, dataStart : Int32, dataLength : Int32, romData : Slice(UInt8)) : Nil
      @chip.not_nil!.writeRom(romSize, dataStart, dataLength, romData)
    end
  end
end
