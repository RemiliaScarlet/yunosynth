#### SN76496 Emulator
####  by Nicola Salmoria
####  with contributions by others
####  Crystal port Copyright (C) 2023-2024 Remilia Scarlet
####  BSD 3-Clause License
####
####  Routines to emulate the:
####  Texas Instruments SN76489, SN76489A, SN76494/SN76496
####  ( Also known as, or at least compatible with, the TMS9919 and SN94624.)
####  and the Sega 'PSG' used on the Master System, Game Gear, and Megadrive/Genesis
####  This chip is known as the Programmable Sound Generator, or PSG, and is a 4
####  channel sound generator, with three squarewave channels and a noise/arbitrary
####  duty cycle channel.
####
####  Noise emulation for all verified chips should be accurate:
####
####  ** SN76489 uses a 15-bit shift register with taps on bits D and E, output on E,
####  XOR function.
####  It uses a 15-bit ring buffer for periodic noise/arbitrary duty cycle.
####  Its output is inverted.
####
####  ** SN94624 is the same as SN76489 but lacks the /8 divider on its clock input.
####
####  ** SN76489A uses a 15-bit shift register with taps on bits D and E, output on F,
####  XOR function.
####  It uses a 15-bit ring buffer for periodic noise/arbitrary duty cycle.
####  Its output is not inverted.
####
####  ** SN76494 is the same as SN76489A but lacks the /8 divider on its clock input.
####
####  ** SN76496 is identical in operation to the SN76489A, but the audio input is
####  documented.
####
####  All the TI-made PSG chips have an audio input line which is mixed with the 4 channels
####  of output. (It is undocumented and may not function properly on the sn76489, 76489a
####  and 76494; the sn76489a input is mentioned in datasheets for the tms5200)
####  All the TI-made PSG chips act as if the frequency was set to 0x400 if 0 is
####  written to the frequency register.
####
####  ** Sega Master System III/MD/Genesis PSG uses a 16-bit shift register with taps
####  on bits C and F, output on F
####  It uses a 16-bit ring buffer for periodic noise/arbitrary duty cycle.
####  (whether it uses an XOR or XNOR needs to be verified, assumed XOR)
####  (whether output is inverted or not needs to be verified, assumed to be inverted)
####
####  ** Sega Game Gear PSG is identical to the SMS3/MD/Genesis one except it has an
####  extra register for mapping which channels go to which speaker.
####  The register, connected to a z80 port, means:
####  for bits 7  6  5  4  3  2  1  0
####           L3 L2 L1 L0 R3 R2 R1 R0
####
####  Noise is an XOR function, and audio output is negated before being output.
####  All the Sega-made PSG chips act as if the frequency was set to 0 if 0 is written
####  to the frequency register.
####
####  ** NCR7496 (as used on the Tandy 1000) is similar to the SN76489 but with a
####  different noise LFSR patttern: taps on bits A and E, output on E
####  It uses a 15-bit ring buffer for periodic noise/arbitrary duty cycle.
####  (all this chip's info needs to be verified)
####
####  28/03/2005 : Sebastien Chevalier
####  Update th SN76496Write func, according to SN76489 doc found on SMSPower.
####   - On write with 0x80 set to 0, when LastRegister is other then TONE,
####   the function is similar than update with 0x80 set to 1
####
####  23/04/2007 : Lord Nightmare
####  Major update, implement all three different noise generation algorithms and a
####  set_variant call to discern among them.
####
####  28/04/2009 : Lord Nightmare
####  Add READY line readback; cleaned up struct a bit. Cleaned up comments.
####  Add more TODOs. Fixed some unsaved savestate related stuff.
####
####  04/11/2009 : Lord Nightmare
####  Changed the way that the invert works (it now selects between XOR and XNOR
####  for the taps), and added R->OldNoise to simulate the extra 0 that is always
####  output before the noise LFSR contents are after an LFSR reset.
####  This fixes SN76489/A to match chips. Added SN94624.
####
####  14/11/2009 : Lord Nightmare
####  Removed STEP mess, vastly simplifying the code. Made output bipolar rather
####  than always above the 0 line, but disabled that code due to pending issues.
####
####  16/11/2009 : Lord Nightmare
####  Fix screeching in regulus: When summing together four equal channels, the
####  size of the max amplitude per channel should be 1/4 of the max range, not
####  1/3. Added NCR7496.
####
####  18/11/2009 : Lord Nightmare
####  Modify Init functions to support negating the audio output. The gamegear
####  psg does this. Change gamegear and sega psgs to use XOR rather than XNOR
####  based on testing. Got rid of R->OldNoise and fixed taps accordingly.
####  Added stereo support for game gear.
####
####  15/01/2010 : Lord Nightmare
####  Fix an issue with SN76489 and SN76489A having the wrong periodic noise periods.
####  Note that properly emulating the noise cycle bit timing accurately may require
####  extensive rewriting.
####
####  24/01/2010: Lord Nightmare
####  Implement periodic noise as forcing one of the XNOR or XOR taps to 1 or 0 respectively.
####  Thanks to PlgDavid for providing samples which helped immensely here.
####  Added true clock divider emulation, so sn94624 and sn76494 run 8x faster than
####  the others, as in real life.
####
####  15/02/2010: Lord Nightmare & Michael Zapf (additional testing by PlgDavid)
####  Fix noise period when set to mirror channel 3 and channel 3 period is set
####      to 0 (tested on hardware for noise, wave needs tests) - MZ
####  Fix phase of noise on sn94624 and sn76489; all chips use a standard XOR,
####      the only inversion is the output itself - LN, Plgdavid
####  Thanks to PlgDavid and Michael Zapf for providing samples which helped immensely here.
####
####  23/02/2011: Lord Nightmare & Enik
####  Made it so the Sega PSG chips have a frequency of 0 if 0 is written to the
####  frequency register, while the others have 0x400 as before. Should fix a bug
####  or two on sega games, particularly Vigilante on Sega Master System. Verified
####  on SMS hardware.
####
####  TODO: * Implement the TMS9919 - any difference to sn94624?
####        * Implement the T6W28; has registers in a weird order, needs writes
####          to be 'sanitized' first. Also is stereo, similar to game gear.
####        * Test the NCR7496; Smspower says the whitenoise taps are A and E,
####          but this needs verification on real hardware.
####        * Factor out common code so that the SAA1099 can share some code.
####        * Convert to modern device
require "weak_ref"

module Yuno::Chips
  # SN76496 sound chip emulator.
  class SN76489 < Yuno::AbstractChip
    # :nodoc:
    class SN76489Mame < Yuno::AbstractEmulator
      ###
      ### Actual implementation of the SN76496 emulator based on MAME's
      ### implementation.
      ###

      MAX_OUTPUT = 0x8000

      @volTable  : Array(Int32) = Array(Int32).new(16, 0i32) # volume table (for 4-bit to dB conversion)
      @registers : Array(Int32) = Array(Int32).new(8, 0i32)
      @lastRegister : Int32 = 0 # Last register written

      protected getter volume : Array(Int32) = Array(Int32).new(4, 0i32) # dB volume of voices 0-2 and noise
      protected property clockDivider   : Int32  = 8
      protected property currentClock   : Int32  = 0
      protected property feedbackMask   : Int32  = 0x10000
      protected property whitenoiseTap1 : Int32  = 0x04    # Mask for white noise tap 1 (higher one, usually bit 14)
      protected property whitenoiseTap2 : Int32  = 0x08    # Mask for white noise tap 2 (lower one, usually bit 13)
      protected property negate         : Int32  = 0       # Output negation flag
      protected property stereo         : Int32  = 0       # Whether we're dealing with stereo or not

      @rng        : UInt32 = 0       # Noise generator LFSR
      @stereoMask : Int32  = 0xFF

      @period : Array(Int32) = Array(Int32).new(4, 0i32) # Length of 1/2 of waveform
      @count  : Array(Int32) = Array(Int32).new(4, 0i32) # Position within the waveform
      @output : Array(Int32) = Array(Int32).new(4, 0i32) # 1-bit output of each channel, pre-volume

      @cyclesToReady : Int32 = 1 # Number of cycles until the READY line goes active

      # Flag for if frequency zero acts as if it is one more than max (0x3FF +
      # 1), or if it acts like 0.
      protected property freq0IsMax : Int32 = 1

      protected property muteMask : Array(UInt32) = Array(UInt32).new(4, 0i32)
      protected property ngpFlags : UInt8 = 0u8 # bit 7: NGP mode on/off.  bit 0: this is the second NGP chip
      protected property ngpChip2 : WeakRef(SN76489Mame)? = nil

      @fnumLimit : UInt16 = 0u16
      protected getter sampleRate : UInt32

      @volUpdate : Array(Int32) = Array(Int32).new(4, 0i32) # Used in the #update method.

      # Use the SN76489mame.genericStart or SN76489mame.sn76496Start class methods to
      # create and init an instance.
      protected def initialize(clock : UInt32, @stereo : Int32)
        @sampleRate = clock.tdiv(2)

        idx : Int32 = 0
        while idx < 8
          @registers[idx] = 0
          @registers[idx + 1] = 0x0F # Volume = 0
          idx += 2
        end

        4.times do |i|
          @muteMask[i] = ~0u32
        end

        # Default is SN76489A
        @rng = @feedbackMask.to_u32!
        @output[3] = (@rng & 1).to_i32!
      end

      def self.genericStart(clock : UInt32, feedbackMask : Int32, noiseTap1 : Int32, noiseTap2 : Int32,
                            negate : Int32, stereo : Int32, clockDivider : Int32,
                            freq0 : Int32, parentCore : SN76489Mame?) : Tuple(SN76489Mame, UInt32)
        chip : SN76489Mame = SN76489Mame.new(clock & 0x7FFFFFFF, stereo)
        sampleRate : UInt32 = chip.sampleRate

        if Yuno.bitflag?(clock, 0x80000000) && parentCore
          # Activate special NeoGeo Pocket mode
          parentCore.ngpFlags = 0x80u8 | 0x00u8
          chip.ngpFlags  = 0x80u8 | 0x01u8
          chip.ngpChip2 = WeakRef.new(parentCore)
          parentCore.ngpChip2 = WeakRef.new(chip)
        end

        chip.gain = 0
        chip.feedbackMask = feedbackMask
        chip.whitenoiseTap1 = noiseTap1
        chip.whitenoiseTap2 = noiseTap2
        chip.negate = negate
        chip.stereo = stereo
        chip.clockDivider = clockDivider if clockDivider != 0
        chip.currentClock = clockDivider - 1
        chip.freq0IsMax = freq0

        # Speed patch
        sampleRate = sampleRate.tdiv(chip.clockDivider)

        {chip, sampleRate}
      end

      def self.sn76496Start(clock : UInt32, shiftRegWidth : Int32, noiseTaps : Int32, negate : Int32, stereo : Int32,
                            clockDivider : Int32, freq0 : Int32,
                            previousChip : SN76489Mame?) : Tuple(SN76489Mame, UInt32)
        Yuno.dlog!(sprintf(%|SN76489: Clock: %i, Shift Reg: %i, Noise Taps: %i
                  Negate: %i, Stereo: %i, Clock Div: %i, Freq0: %i\n|,
                           clock, shiftRegWidth, noiseTaps, negate, stereo, clockDivider, freq0))
        # Extract single noise tap bits
        curTap : Int32 = 0
        ntap : Array(Int32) = [0, 0]
        16.times do |curBit|
          if Yuno.bitflag?(noiseTaps, 1 << curBit)
            ntap[curTap] = 1 << curBit
            curTap += 1
            break if curTap == 2
          end
        end

        genericStart(clock,
                     1 << (shiftRegWidth - 1),
                     ntap[0], ntap[1],
                     negate,
                     stereo == 0 ? 1 : 0,
                     clockDivider != 0 ? 1 : 8,
                     freq0,
                     previousChip)
      end

      def reset : Nil
        4.times { |i| @volume[i] = 0 }

        @lastRegister = 0
        i : Int32 = 0
        while i < 8
          @registers[i] = 0
          @registers[i + 1] = 0x0F # Volume = 0
          i += 2
        end

        4.times do |i|
          @output[i] = 0
          @period[i] = 0
          @count[i] = 0
        end

        @cyclesToReady = 1
        @stereoMask = 0xFF # All channels enabled

        @rng = @feedbackMask.to_u32!
        @output[3] = (@rng & 1).to_i32!
      end

      def freqLimiter(clock : UInt32, clockDiv : Int, sampleRate : UInt32) : Nil
        @fnumLimit = ((clock / (clockDiv != 0 ? 2.0 : 16.0)) / sampleRate).to_u16!
      end

      # @[AlwaysInline]
      # def readyRead(offset : Int) : UInt8
      #   @cyclesToReady != 0 ? 0u8 : 1u8
      # end

      @[AlwaysInline]
      def stereoWrite(data : UInt8) : Nil
        if @stereo != 0
          @stereoMask = data.to_i32!
        else
          Yuno.dlog!("SN76489: Call to stereo write with mono chip")
        end
      end

      @[AlwaysInline]
      def writeReg(data : UInt8) : Nil
        n : Int32 = 0
        r : Int32 = 0
        c : Int32 = 0

        # Set number of cycles until READY is active; this is always one
        # 'sample', i.e. it equals the clock divider exactly.  Until the clock
        # divider is fully supported, we delay until one sample has played.  The
        # fact that this below is '2' and not '1' is because of a ?race
        # condition? in the mess crvision driver, where after any sample is
        # played at all, no matter what, the cycles_to_ready ends up never being
        # not ready, unless this value is greater than 1.  Once the full clock
        # divider stuff is written, this should no longer be an issue.
        @cyclesToReady = 2

        if Yuno.bitflag?(data, 0x80)
          r = (data.to_i32! & 0x70) >> 4 # 0..7
          @lastRegister = r
          @registers.put!(r, (@registers.get!(r) & 0x3F0) | (data & 0x0F))
        else
          r = @lastRegister # This is only assigned in the code directly above
        end

        c = r.tdiv(2) # 0..3

        case r
        when 0, 2, 4 # Tone 0, 1, and 2 frequency
          unless Yuno.bitflag?(data, 0x80)
            @registers.put!(r, (@registers.get!(r) & 0x0F) | ((data.to_i32! & 0x3F) << 4))
          end

          if @registers.get!(r) != 0 || @freq0IsMax == 0
            @period.put!(c, @registers.get!(r))
          else
            @period.put!(c, 0x400)
          end

          if r == 4
            # Update noise shift frequency
            if (@registers.get!(6) & 0x03) == 0x03
              @period.put!(3, 2 * @period.get!(2))
            end
          end

        when 1, 3, 5, 7 # Tone 0, 1, 2, and noise volume
          @volume.put!(c, @volTable.get!(data & 0x0F))
          unless Yuno.bitflag?(data, 0x80)
            @registers.put!(r, (@registers.get!(r) & 0x3F0) | (data & 0x0F))
          end

        when 6 # Noise frequency mode
          unless Yuno.bitflag?(data, 0x80)
            @registers.put!(r, (@registers.get!(r) & 0x3F0) | (data & 0x0F))
          end

          n = @registers.get!(6)

          # N/512,N/1024,N/2048,Tone #3 output
          if (n & 3) == 3
            @period.put!(3, 2 * @period.get!(2))
          else
            @period.put!(3, (1 << (5 + (n & 3))))
          end

          @rng = @feedbackMask.to_u32!
        end
      end

      @[AlwaysInline]
      def update(outputs : OutputBuffers, samples : UInt32) : Nil
        outL : Slice(Int32) = outputs[0]
        outR : Slice(Int32) = outputs[1]
        outLPos : Int32 = 0
        outRPos : Int32 = 0
        i : Int32 = 0  # Can't use "X.times do |i|" below

        # Check once, then we can avoid bounds checks below.
        if outL.size < samples || outR.size < samples
          raise "SN76496 update: Bad number of samples (expected #{samples}, " \
                "but buffers have #{outL.size} and #{outR.size})"
        end

        out1 : Int32 = 0
        out2 : Int32 = 0
        ngpMode : UInt8 = (@ngpFlags >> 7) & 1
        @volUpdate.fill(0)

        if ngpMode == 0
          # Speed hack
          while i < 3
            if @period.get!(i) != 0 || @volume.get!(i) != 0
              out1 = 1
              break
            end
            i += 1
          end

          out1 = 1 if @volume.get!(3) != 0

          if out1 == 0
            outL.fill(0, 0, samples)
            outR.fill(0, 0, samples)
            return
          end
        end

        ggst : StaticArray(Int32, 2) = StaticArray(Int32, 2).new(0x01_i32)
        while samples > 0
          # Decrement Cycles to READY by one
          @cyclesToReady -= 1 if @cyclesToReady > 0

          # Handle channels, 0 through 2
          i = 0
          while i < 3
            @count.decf!(i)
            if @count.get!(i) <= 0
              @output.put!(i, @output.get!(i) ^ 1)
              @count.put!(i, @period.get!(i))
            end
            i += 1
          end

          # Handle channel 3
          @count.decf!(3)
          if @count.get!(3) <= 0
            # If noisemode is 1, both taps are enabled.
            # If noisemode is 0, the lower tap, whitenoisetap2, is held at 0.
            x : UInt8 = Yuno.bitflag?(@rng, @whitenoiseTap1) ? 1u8 : 0u8
            y : UInt8 = Yuno.bitflag?(@rng, @whitenoiseTap2) ? 1u8 : 0u8
            z : UInt8 = Yuno.bitflag?(@registers.get!(6), 4) ? 1u8 : 0u8
            if (x ^ (y * z)) != 0
              @rng >>= 1
              @rng |= @feedbackMask
            else
              @rng >>= 1
            end

            @output.put!(3, (@rng & 1).to_i32!)
            @count.put!(3, @period.get!(3))
          end

          out1 = out2 = 0
          if @ngpFlags == 0
            i = 0
            while i < 4
              ## --- Preparation Start ---
              # Bipolar output
              @volUpdate.put!(i, @output.get!(i) != 0 ? 1 : -1)

              # Disable high frequencies (> SampleRate / 2) for tone channels.
              # Freq. 0/1 isn't disabled becaus it would also disable PCM.
              if i != 3
                if @period.get!(i) <= @fnumLimit && @period.get!(i) > 1
                  @volUpdate.put!(i, 0)
                end
              end
              @volUpdate.put!(i, @volUpdate.get!(i) & @muteMask.get!(i))
              ## --- Preparation End ---

              if @stereo != 0
                ggst.unsafe_put(0, Yuno.bitflag?(@stereoMask, 0x10 << i) ? 1 : 0)
                ggst.unsafe_put(1, Yuno.bitflag?(@stereoMask, 0x01 << i) ? 1 : 0)
              end

              if @period.get!(i) > 1 || i == 3
                out1 += @volUpdate.get!(i) * @volume.get!(i) * ggst.unsafe_fetch(0)
                out2 += @volUpdate.get!(i) * @volume.get!(i) * ggst.unsafe_fetch(1)
              elsif @muteMask.get!(i) != 0
                # Make bipolar output with PCM possible
                out1 += @volume.get!(i) * ggst.unsafe_fetch(0)
                out2 += @volume.get!(i) * ggst.unsafe_fetch(1)
              end

              i += 1
            end
          else # if @ngpFlags == 0
            if !Yuno.bitflag?(@ngpFlags, 1)
              # Tone channels 1 through 3
              if @stereo != 0
                # This is why we have to use while loops with "i".
                ggst.unsafe_put(0, (@stereoMask & (0x10 << i)) != 0 ? 1 : 0)
                ggst.unsafe_put(1, (@stereoMask & (0x01 << i)) != 0 ? 1 : 0)
              end

              i = 0
              while i < 3
                ## --- Preparation Start ---
                # Bipolar output
                @volUpdate.put!(i, @output.get!(i) != 0 ? 1 : -1)

                # Disable high frequencies (> SampleRate / 2) for tone channels.
                # Freq. 0 isn't disabled becaus it would also disable PCM.
                if @period.get!(i) <= @fnumLimit && @period.get!(i) != 0
                  @volUpdate.put!(i, 0)
                end
                @volUpdate.put!(i, @volUpdate.get!(i) & @muteMask.get!(i))
                ## --- Preparation End ---

                if @period.get!(i) != 0
                  out1 += @volUpdate.get!(i) * @volume.get!(i) * ggst.unsafe_fetch(0)
                  out2 += @volUpdate.get!(i) * @ngpChip2.not_nil!.value.not_nil!.volume.get!(i) * ggst.unsafe_fetch(1)
                elsif @muteMask.get!(i) != 0
                  # Make bipolar output with PCM possible
                  out1 += @volume.get!(i) * ggst.unsafe_fetch(0)
                  out2 += @ngpChip2.not_nil!.value.not_nil!.volume.get!(i) * ggst.unsafe_fetch(1)
                end

                i += 1
              end
            else # if !Yuno.bitflag?(@ngpFlags, 1)
              # --- Preparation Start ---
              # Bipolar output
              @volUpdate.put!(i, @output.get!(i) != 0 ? 1 : -1)
              # Use muteMask from chip 0
              @volUpdate.put!(i, @volUpdate.get!(i) & @ngpChip2.not_nil!.value.not_nil!.muteMask.get!(i))
              # --- Preparation End ---

              # Noise channel
              if @stereo != 0
                ggst.unsafe_put(0, Yuno.bitflag?(@stereoMask, 0x80) ? 1 : 0)
                ggst.unsafe_put(1, Yuno.bitflag?(@stereoMask, 0x08) ? 1 : 0)
              else
                ggst.fill(1)
              end

              out1 += @volUpdate.get!(3) * @ngpChip2.not_nil!.value.not_nil!.volume.get!(3) * ggst.unsafe_fetch(0)
              out2 += @volUpdate.get!(3) * @volume.get!(3) * ggst.unsafe_fetch(1)
            end
          end # if @ngpFlags == 0

          if @negate != 0
            out1 = -out1
            out2 = -out2
          end

          outL.put!(outLPos, out1 >> 1)
          outR.put!(outRPos, out2 >> 1)
          outLPos += 1
          outRPos += 1
          samples -= 1
        end # while samples > 0
      end

      def gain=(gain : Int) : Nil
        # Remi: Changed the hard-coded literals to macro calls for increased
        # precision.  Moved MAX_OUTPUT / 4.0 to a variable.
        {% begin %}
          maxOutQuarter : Float64 = MAX_OUTPUT / 4.0
          output : Float64 = maxOutQuarter
          gain &= 0xFF # Ensure 8-bit

          # Increase max output basing on gain (0.2 dB per step)
          while gain > 0
            output *= {{10 ** (0.2 / 20)}}
            gain -= 1
          end

          # Build volume table (2dB per step)
          15.times do |i|
            # Limit volume to avoid clipping
            if output > maxOutQuarter
              @volTable.put!(i, maxOutQuarter.to_i32!)
            else
              @volTable.put!(i, (output + 0.5).to_i32!) # I like rounding
            end

            output /= {{10 ** (2 / 20)}}
          end

          @volTable.put!(15, 0)
        {% end %}
      end

      def muteMask=(mask : UInt32) : Nil
        4.times do |idx|
          @muteMask[idx] = Yuno.bitflag?(muteMask, 1 << idx) ? 0 : ~0u32
        end
      end
    end # class SN76489Mame
  end
end
