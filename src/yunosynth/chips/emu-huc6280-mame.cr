#### HuC6280 sound chip emulator
#### Copyright (C) Charles MacDonald
#### Copyright (C) 2023-2024 Remilia Scarlet
#### BSD 3-Clause License
####
#### Original C implementation by Charles MacDonald
#### E-mail: cgfm2@hotmail.com
#### WWW: http://cgfm2.emuviews.com
####
#### Common Lisp port done by Remilia Scarlet
####
#### Thanks to:
####
#### - Paul Clifford for his PSG documentation.
#### - Richard Bannister for the TGEmu-specific sound updating code.
#### - http://www.uspto.gov for the PSG patents.
#### - All contributors to the tghack-list.
####
#### Missing features / things to do:
####
####  - Add LFO support. But do any games actually use it?
####
####  - Add shared index for waveform playback and sample writes. Almost every
####    game will reset the index prior to playback so this isn't an issue.
####
####  - While the noise emulation is complete, the data for the pseudo-random
####    bitstream is calculated by machine.rand() and is not a representation of
####    what the actual hardware does.
####
#### For some background on Hudson Soft's C62 chipset:
####
####  - http://www.hudsonsoft.net/ww/about/about.html
####  - http://www.hudson.co.jp/corp/eng/coinfo/history.html

module Yuno::Chips
  # HuC6280 sound chip emulator.
  class HuC6280 < Yuno::AbstractChip
    # Actual implementation of the HuC6280 emulator based on Mame's implementation
    private class HuC6280Mame < Yuno::AbstractEmulator
      SCALE_TABLE = [
        0x00, 0x03, 0x05, 0x07, 0x09, 0x0B, 0x0D, 0x0F,
        0x10, 0x13, 0x15, 0x17, 0x19, 0x1B, 0x1D, 0x1F
      ]

      private class Channel
        property frequency : UInt16 = 0
        property control : UInt8 = 0
        property balance : UInt8 = 0
        property waveform : Array(UInt8) = Array(UInt8).new(32, 0)
        property index : UInt8 = 0
        property dda : Int16 = 0
        property noiseControl : UInt8 = 0
        property noiseCounter : UInt32 = 0
        property counter : UInt32 = 0
        property muted : UInt8 = 0

        def initialize
        end

        def reset : Nil
          @frequency = 0
          @control = 0
          @balance = 0
          @waveform.fill(0)
          @index = 0
          @dda = 0
          @noiseControl = 0
          @noiseCounter = 0
          @counter = 0
        end
      end

      @rand = Random.new(Time.local.to_unix_ms)
      @selector : UInt8 = 0
      @balance : UInt8 = 0
      @lfoFreq : UInt8 = 0
      @lfoControl : UInt8 = 0
      @channels : Array(Channel)
      @volumeTable : Array(Int16) = Array(Int16).new(32, 0)
      @noiseFreqTable : Array(UInt32) = Array(UInt32).new(32, 0)
      @waveFreqTable : Array(UInt32) = Array(UInt32).new(4096, 0)
      @noiseData : Int32 = 0

      def initialize(clock : Float64, rate : Float64)
        step : Float64 = 0.0
        level : Float64 = 65536.0 / 6.0 / 32.0

        # Initialise the channels
        @channels = Array(Channel).new(8) { |_| Channel.new }

        # Initialize the waveform frequency table
        4096.times do |i|
          step = ((clock / rate) * 4096) / (i + 1)
          @waveFreqTable[(i + 1) & 0xFFF] = step.to_u32!
        end

        # Initialize the noise frequency table
        32.times do |i|
          step = ((clock / rate) * 32) / (i + 1)
          @noiseFreqTable[i] = step.to_u32!
        end

        # Initialize the volume table.  The chip has a 48dB volume range spread
        # over 32 steps.
        step = 48.0 / 32.0
        31.times do |i|
          @volumeTable[i] = level.to_i16!
          level = level / (10.0 ** (step / 20.0))
        end
        @volumeTable[31] = 0
      end

      def reset : Nil
        @selector = 0
        @balance = 0
        @lfoFreq = 0
        @lfoControl = 0
        @channels.each &.reset
      end

      def read(offset : Int) : UInt8
        if offset == 0
          @selector
        else
          0u8
        end
      end

      def muteMask=(mask : UInt32) : Nil
        @channels.each_with_index do |ch, idx|
          ch.muted = ((mask >> idx) & 0x01).to_u8!
        end
      end

      def write(offset : Int, data : UInt8) : Nil
        chan = @channels[@selector]
        case offset & 0x0F
        when 0x00 # Channel select
          @selector = data & 0x07

        when 0x01 # Global balance
          @balance = data

        when 0x02 # Channel frequency (LSB)
          chan.frequency = (chan.frequency & 0x0F00) | data.to_u16!
          chan.frequency = chan.frequency & 0x0FFF

        when 0x03 # Channel frequency (MSB)
          chan.frequency = (chan.frequency & 0x00FF) | (data.to_u16! << 8)
          chan.frequency = chan.frequency & 0x0FFF

        when 0x04 # Channel control (key on, DDA mode, volume...)
          if Yuno.bitflag?(chan.control, 0x40) && !Yuno.bitflag?(data, 0x40)
            chan.index = 0
          end
          chan.control = data

        when 0x05 # Channel balance
          chan.balance = data

        when 0x06 # Channel waveform data
          case chan.control & 0xC0
          when 0x00
            chan.waveform[chan.index & 0x1F] = data & 0x1F
            chan.index = (chan.index + 1) & 0x1F

          when 0x80
            chan.waveform[chan.index & 0x1F] = data & 0x1F
            chan.index = (chan.index + 1) & 0x1F

          when 0xC0
            chan.dda = (data & 0x1F).to_i16!
          end

        when 0x07 # Noise control (enable/disable, frequency)
          chan.noiseControl = data

        when 0x08 # LFO frequency
          @lfoFreq = data

        when 0x09 # LFO control (enable/disable, mode)
          @lfoControl = data
        end
      end

      def update(outputs : OutputBuffers, samples : UInt32) : Nil
        lmal   : Int32 = (@balance.to_i32! >> 4) & 0x0F
        rmal   : Int32 = @balance.to_i32! & 0x0F
        vll    : Int32 = 0 # Left volume level
        vlr    : Int32 = 0 # Right volume level
        lal    : Int32 = 0
        ral    : Int32 = 0
        al     : Int32 = 0
        step   : Int32 = 0
        offset : Int32 = 0
        data   : Int32 = 0

        lmal = SCALE_TABLE[lmal]
        rmal = SCALE_TABLE[rmal]

        outL : Slice(Int32) = outputs[0]
        outR : Slice(Int32) = outputs[1]

        # Check once, then we can avoid bounds checks below.
        if outL.size < samples || outR.size < samples
          raise "HuC6280 update: Bad number of samples (expected #{samples}, " \
                "but buffers have #{outL.size} and #{outR.size})"
        end

        # Clear output buffers
        outL.fill(0, 0, samples)
        outR.fill(0, 0, samples)

        # Render
        @channels.each_with_index do |chan, chanNum|
          # Only look at channels that are enabled.
          if Yuno.bitflag?(chan.control, 0x80) && chan.muted == 0
            lal = (chan.balance.to_i32! >> 4) & 0x0F
            ral = chan.balance.to_i32! & 0x0F
            al = chan.control.to_i32! & 0x1F
            lal = SCALE_TABLE[lal]
            ral = SCALE_TABLE[ral]

            # Calculate volume just like the patent says.
            vll = Math.min(0x1F, (0x1F - lal) + (0x1F - al) + (0x1F - lmal)).to_i32!
            vlr = Math.min(0x1F, (0x1F - ral) + (0x1F - al) + (0x1F - rmal)).to_i32!
            vll = @volumeTable[vll].to_i32!
            vlr = @volumeTable[vlr].to_i32!

            # Check the channel mode
            case
            when chanNum >= 4 && Yuno.bitflag?(chan.noiseControl, 0x80)
              # Noise mode
              step = @noiseFreqTable[(chan.noiseControl & 0x1F) ^ 0x1F].to_i32!
              samples.times do |i|
                chan.noiseCounter += step
                if chan.noiseCounter >= 0x800
                  @noiseData = (@rand.rand(Int32) & 1) != 0 ? 0x1F : 0
                end
                chan.noiseCounter &= 0x7FF
                outL.unsafe_put(i, outL.unsafe_fetch(i) + (vll * (@noiseData - 16)))
                outR.unsafe_put(i, outR.unsafe_fetch(i) + (vlr * (@noiseData - 16)))
              end

            when Yuno.bitflag?(chan.control, 0x40)
              # DDA mode
              samples.times do |i|
                outL.unsafe_put(i, outL.unsafe_fetch(i) + (vll * (chan.dda - 16)))
                outR.unsafe_put(i, outR.unsafe_fetch(i) + (vlr * (chan.dda - 16)))
              end

            else
              # Normal waveform mode
              step = @waveFreqTable[chan.frequency].to_i32!
              samples.times do |i|
                offset = (chan.counter.to_i32! >> 12) & 0x1F
                chan.counter += step
                chan.counter = chan.counter & 0x1FFFF
                data = chan.waveform[offset].to_i32!

                outL.unsafe_put(i, outL.unsafe_fetch(i) + (vll * (data - 16)))
                outR.unsafe_put(i, outR.unsafe_fetch(i) + (vlr * (data - 16)))
              end
            end
          end
        end
      end

      def unmuteAll : Nil
        @channels.each &.muted=(0)
      end
    end
  end
end
