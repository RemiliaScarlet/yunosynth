#### YunoSynth
#### Copyright (C) 2023-2024 Remilia Scarlet <remilia@posteo.jp>
#### Portions based on VGMPlay, Copyright (C) Valley Bell
####
#### This program is free software: you can redistribute it and/or
#### modify it under the terms of the GNU Affero General Public
#### License as published by the Free Software Foundation, either
#### version 3 of the License, or (at your option) any later version.
####
#### This program is distributed in the hope that it will be useful,
#### but WITHOUT ANY WARRANTY; without even the implied warranty of
#### MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#### Affero General Public License for more details.
####
#### You should have received a copy of the GNU Affero General Public License
#### along with this program.  If not, see <https://www.gnu.org/licenses/>.
require "./emu-nes-nsfplay/*"

####
#### NES sound chip emulator interface
####

module Yuno::Chips
  # NES sound chip emulator.
  class Nes < Yuno::AbstractChip
    CHIP_ID = 0x14_u32

    class NesFlags < ChipFlags
      property flags : UInt16 = 0x8000_u16 | (0x3B << 4) | (0x01 << 2) | 0x03

      def initialize
      end
    end

    @clockFromHeader : UInt32 = 0
    @samplingMode : UInt8 = 0
    @chipSampleRate : UInt32 = 0
    @apu : NesApuNsfPlay?
    @dmc : NesDmcNsfPlay?
    @fds : NesFdsNsfPlay?
    @memory : Array(UInt8) = [] of UInt8
    @hasFds : Bool = false
    property options : UInt16 = 0x8000_u16
    @buffer : Array(Int32) = [0, 0, 0, 0]

    def initialize(chipNum : Int32, absChipCount : Int32, vgm : VgmFile, playbackSampleRate : UInt32,
                   newSamplingMode : UInt8, newPlayerSampleRate : UInt32, *, emuCore : Symbol? = nil,
                   flags : ChipFlags? = nil)
      @clockFromHeader = vgm.getAlternateChipClock(chipNum, Yuno::ChipType::NesApu) ||  vgm.header.nesApuClock
      @clockFromHeader &= 0xBFFFFFFF
      @playerSampleRate = newPlayerSampleRate
      @hasFds = Yuno.bitflag?(vgm.header.nesApuClock, 0x80000000)
      chipCount = (vgm.header.nesApuClock & 0x40000000) != 0 ? 2 : 1
      initFields(vgm, emuCore, playbackSampleRate, newSamplingMode, chipCount, flags)

      self.options = self.getStartFlags(vgm).as(NesFlags).flags
    end

    protected def initialize
    end

    protected def initFields(vgm : VgmFile, emuCore : Symbol?, playbackSampleRate : UInt32, newSamplingMode : UInt8,
                             chipCount : Int32, flags : ChipFlags?) : Nil
      @volume = vgm.getChipVolume(self, ChipType::NesApu, chipCount)

      @core = emuCore || Nes.defaultEmuCore
      case @core
      when :nsf_play
        @samplingMode = newSamplingMode
        @chipSampleRate = @clockFromHeader <= 0 ? playbackSampleRate : @clockFromHeader

      else raise YunoError.new("Unsupported emulation core for NES: #{@core}")
      end
    end

    def type : ChipType
      ChipType::NesApu
    end

    def name : String
      if @hasFds
        "Nintendo APU + Famicom Disk System"
      else
        "Nintendo APU"
      end
    end

    def shortName : String
      "NES APU+FDS"
    end

    def id : UInt32
      CHIP_ID
    end

    def emuCore : Symbol
      @core
    end

    def self.defaultEmuCore : Symbol
      :nsf_play
    end

    def start(chipIndex : UInt8, clock : UInt32, flags : ChipFlags? = nil, previousChip : AbstractChip? = nil) : UInt32
      enableFds : UInt8 = ((clock >> 31) & 0x01).to_u8!
      clock &= 0x7FFFFFFF
      rate = clock.tdiv(4)
      if (Yuno.bitflag?(@samplingMode, 0x01) && rate < @chipSampleRate) || @samplingMode == 0x02
        rate = @chipSampleRate
      end

      case @core
      when :nsf_play
        @apu = NesApuNsfPlay.new(clock, rate)
        @dmc = NesDmcNsfPlay.new(clock, rate)
        @dmc.not_nil!.apu = @apu

        @memory = Array(UInt8).new(0x8000, 0u8)
        @dmc.not_nil!.memory = @memory
      else raise YunoError.new("Unsupported emulation core for NES: #{@core}")
      end

      if enableFds != 0
        @fds = NesFdsNsfPlay.new(clock, rate)
      else
        @fds = nil
      end
      setChipOption

      @sampleRate = rate # Update sample rate
      @sampleRate
    end

    def getClock : UInt32
      @clockFromHeader
    end

    def getStartFlags(vgm : VgmFile) : ChipFlags?
      NesFlags.new
    end

    def update(outputs : OutputBuffers, samples : Int) : Nil
      outL : Slice(Int32) = outputs[0]
      outR : Slice(Int32) = outputs[1]

      # Check once, then we can avoid bounds checks below.
      if outL.size < samples || outR.size < samples
        raise "NES update: Bad number of samples (expected #{samples}, " \
              "but buffers have #{outL.size} and #{outR.size})"
      end

      @buffer.fill(0)
      case @core
      when :nsf_play
        samples.times do |i|
          @apu.not_nil!.render(@buffer.to_unsafe)
          @dmc.not_nil!.render(@buffer.to_unsafe + 2)
          outL.put!(i, @buffer.get!(0) &+ @buffer.get!(2))
          outR.put!(i, @buffer.get!(1) &+ @buffer.get!(3))
        end

        @fds.try do |fds|
          samples.times do |i|
            fds.render(@buffer.to_unsafe)
            outL.incf!(i, @buffer.get!(0))
            outR.incf!(i, @buffer.get!(1))
          end
        end
      end
    end

    def reset(chipIndex : UInt8) : Nil
      @apu.try &.reset
      @dmc.try &.reset
      @fds.try &.reset
    end

    def read(chipIndex : UInt8, offset : Int) : UInt8|UInt16|UInt32
      # Not implemented
      0u8
    end

    def write(chipIndex : UInt8, offset : Int, data : Int, port : UInt8 = 0) : Nil
      case offset & 0xE0
      when 0x00 # APU
        case @core
        when :nsf_play
          # APU handles the square waves, DMS the rest
          @apu.not_nil!.write(0x4000u32 | offset, data.to_u32!)
          @dmc.not_nil!.write(0x4000u32 | offset, data.to_u32!)
        end

      when 0x20 # FDS
        @fds.try do |fds|
          if offset == 0x3F
            fds.write(0x4023u32, data.to_u32!)
          else
            fds.write(0x4080u32 | (offset & 0x1F), data.to_u32!)
          end
        end
      when 0x40, 0x60 # 0x40 = FDS wave RAM
        @fds.try &.write(0x4000u32 | offset, data.to_u32!)
      end
    end

    def writeDac(port : Int, command : Int, data : Int) : Nil
      write(0, 0x00, command)
      write(0, 0x01, data)
    end

    def setMuteMask(chipIndex : UInt8, mask : UInt32) : Nil
      case @core
      when :nsf_play
        @apu.not_nil!.mask=(mask & 0x03)
        @dmc.not_nil!.mask=((mask & 0x1C) >> 2)
      end
      @fds.try &.mask=((mask & 0x20) >> 5)
    end

    def getVolModifier : UInt32
      @volume.to_u32 * 2
    end

    def baseVolume : UInt16
      0x100_u16
    end

    def writeRam(dataStart : Int32, dataLength : Int32, ramData : Slice(UInt8)) : Nil
      return if  dataStart >= 0x10000

      ramOffset : Int32 = 0
      if dataStart < 0x8000
        return if dataStart + dataLength <= 0x8000

        remainBytes : UInt32 = 0x8000u32 - dataStart
        dataStart = 0x8000
        ramOffset += remainBytes
        dataLength -= remainBytes
      end

      remainBytes = 0u32
      if dataStart + dataLength > 0x10000
        remainBytes = dataLength.to_u32!
        dataLength = 0x10000 - dataStart
        remainBytes -= dataLength
      end

      memOffset = dataStart - 0x8000
      dataLength.times do |i|
        @memory[memOffset + i] = ramData[ramOffset + i]
      end

      if remainBytes != 0
        remainBytes = 0x8000 if remainBytes > 0x8000
        remainBytes.times do |i|
          @memory[i] = ramData[ramOffset + dataLength + i]
        end
      end
    end

    # The internal interface for the chip.
    @[AlwaysInline]
    def chip : AbstractEmulator
      raise "Not implemented for NES"
    end

    def setChipOption : Nil
      return if Yuno.bitflag?(@options, 0x8000)

      case @core
      when :nsf_play
        2.times do |i|
          @apu.not_nil!.setOption(i, (@options >> i) & 0x01)
          @dmc.not_nil!.setOption(i, (@options >> i) & 0x01)
        end

        # APU only options
        (2...4).each do |i|
          @apu.not_nil!.setOption(i - 2 + 2, (@options >> i) & 0x01)
        end

        # DMC only options
        (4...10).each do |i|
          @dmc.not_nil!.setOption(i - 4 + 2, (@options >> i) & 0x01)
        end
      end

      # FDS Options
      @fds.try do |fds|
        # Skip the cutoff frequency here since it's not a boolean value.
        (12...14).each do |i|
          fds.setOption(i - 12 + 1, (@options >> i) & 0x01)
        end
      end
    end
  end
end
