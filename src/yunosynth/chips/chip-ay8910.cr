#### YunoSynth
#### Copyright (C) 2023-2024 Remilia Scarlet <remilia@posteo.jp>
#### Portions based on VGMPlay, Copyright (C) Valley Bell
####
#### This program is free software: you can redistribute it and/or
#### modify it under the terms of the GNU Affero General Public
#### License as published by the Free Software Foundation, either
#### version 3 of the License, or (at your option) any later version.
####
#### This program is distributed in the hope that it will be useful,
#### but WITHOUT ANY WARRANTY; without even the implied warranty of
#### MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#### Affero General Public License for more details.
####
#### You should have received a copy of the GNU Affero General Public License
#### along with this program.  If not, see <https://www.gnu.org/licenses/>.
require "./emu-ay8910-emu2149"

####
#### General Instruments AY-1-8910 sound chip emulator interface (and related
#### chips)
####

module Yuno::Chips
  # Ay8910 sound chip emulator.
  class Ay8910 < Yuno::AbstractChip
    CHIP_ID = 0x12_u32
    YM2149_PIN26_LOW = 0x10_u8
    YM2149_PIN26_HIGH = 0x00_u8

    class Ay8910Flags < ChipFlags
      property flags : UInt8
      def initialize(@flags : UInt8 = 0)
      end
    end

    @clockFromHeader : UInt32 = 0
    @samplingMode : UInt8 = 0
    @chipSampleRate : UInt32 = 0
    @subtype : UInt8 = 0 # Whether this is an actual AY-1-8910, YM2149, etc...
    @flags : Ay8910Flags = Ay8910Flags.new
    @chip : Ay8910Emu2149? = nil

    def initialize(chipNum : Int32, absChipCount : Int32, vgm : VgmFile, playbackSampleRate : UInt32,
                   newSamplingMode : UInt8, newPlayerSampleRate : UInt32, *, emuCore : Symbol? = nil,
                   flags : ChipFlags? = nil)
      @clockFromHeader = vgm.getAlternateChipClock(chipNum, Yuno::ChipType::Ay8910) || vgm.header.ay8910Clock
      @clockFromHeader &= 0x3FFFFFFF
      @playerSampleRate = newPlayerSampleRate
      chipCount = (vgm.header.ay8910Clock & 0x40000000) != 0 ? 2 : 1
      initFields(vgm, emuCore, playbackSampleRate, newSamplingMode, chipCount, flags)
    end

    protected def initialize
    end

    protected def initFields(vgm : VgmFile, emuCore : Symbol?, playbackSampleRate : UInt32, newSamplingMode : UInt8,
                             chipCount : Int32, flags : ChipFlags? ) : Nil
      @volume = vgm.getChipVolume(self, ChipType::Ay8910, chipCount)
      @subtype = vgm.header.ay8910ChipType

      if flags
        @flags = flags.as(Ay8910Flags)
      end

      @core = emuCore || Ay8910.defaultEmuCore
      case @core
      when :emu2149
        @samplingMode = newSamplingMode
        @chipSampleRate = @clockFromHeader <= 0 ? playbackSampleRate : @clockFromHeader

      else raise YunoError.new("Unsupported emulation core for AY-1-8910: #{@core}")
      end
    end

    def type : ChipType
      ChipType::Ay8910
    end

    def name : String
      case @subtype
      when 0x00 then "General Instruments AY-1-8910"
      when 0x01 then "General Instruments AY-1-8912"
      when 0x02 then "General Instruments AY-1-8913"
      when 0x03 then "General Instruments AY-1-8930"
      when 0x10 then "Yamaha YM2149"
      when 0x11 then "Yamaha YM3439"
      when 0x12 then "Yamaha YMZ284"
      when 0x13 then "Yamaha YMZ294"
      else "Unknown AY8910 chip"
      end
    end

    @[AlwaysInline]
    def shortName : String
      case @subtype
      when 0x00 then "AY-1-8910"
      when 0x01 then "AY-1-8912"
      when 0x02 then "AY-1-8913"
      when 0x03 then "AY-1-8930"
      when 0x10 then "YM2149"
      when 0x11 then "YM3439"
      when 0x12 then "YMZ284"
      when 0x13 then "YMZ294"
      else "AY-1-8910?"
      end
    end

    def id : UInt32
      CHIP_ID
    end

    def emuCore : Symbol
      @core
    end

    def self.defaultEmuCore : Symbol
      :emu2149
    end

    def start(chipIndex : UInt8, clock : UInt32, flags : ChipFlags? = nil, previousChip : AbstractChip? = nil) : UInt32
      raise YunoError.new("Ay8910 requires flags") unless flags
      @flags = flags.as(Ay8910Flags)

      rate : UInt32 = if Yuno.bitflag?(@flags.flags, YM2149_PIN26_LOW)
                        clock.tdiv(16)
                      else
                        clock.tdiv(8)
                      end

      if (Yuno.bitflag?(@samplingMode, 0x01) && rate < @chipSampleRate) || @samplingMode == 0x02
        rate = @chipSampleRate
      end

      case @core
      when :emu2149
        clock = clock.tdiv(2) if Yuno.bitflag?(@flags.flags, YM2149_PIN26_LOW)
        @chip = Ay8910Emu2149.new(clock, rate)
        @chip.not_nil!.volumeMode = (Yuno.bitflag?(@subtype, 0x10) ? 1 : 2)
        @chip.not_nil!.flags = (@flags.flags & ~YM2149_PIN26_LOW).to_u8!
        @sampleRate = @chip.not_nil!.rate
      else raise YunoError.new("Unsupported emulation core for AY-1-8910: #{@core}")
      end

      @sampleRate
    end

    def startYm(clock : UInt32, rate : UInt32, flags : Ay8910Flags?) : UInt32
      @flags = flags.as(Ay8910Flags) if flags
      case @core
      when :emu2149
        @chip = Ay8910Emu2149.new(clock, rate)
        @chip.not_nil!.volumeMode = Ay8910Emu2149::VOL_YM2149
        @sampleRate = @chip.not_nil!.rate
      else raise YunoError.new("Unsupported emulation core for paired YM2149: #{@core}")
      end

      @sampleRate
    end

    def getClock : UInt32
      @clockFromHeader
    end

    def clock=(clock : UInt32) : Nil
      if (@subtype & 0xF0) == 0x10 && Yuno.bitflag?(@flags.flags, YM2149_PIN26_LOW)
        clock = clock.tdiv(2)
      end

      @chip.not_nil!.sampleRateFunc.try &.call(@chip.not_nil!.sampleRateData.not_nil!, clock.tdiv(8))
    end

    def getStartFlags(vgm : VgmFile) : ChipFlags?
      Ay8910Flags.new(vgm.header.ay8910Flags)
    end

    def update(outputs : OutputBuffers, samples : Int) : Nil
      @chip.not_nil!.update(outputs, samples.to_u32!)
    end

    def reset(chipIndex : UInt8) : Nil
      @chip.not_nil!.reset
    end

    def read(chipIndex : UInt8, offset : Int) : UInt8|UInt16|UInt32
      @chip.not_nil!.readReg(offset.to_u32!)
    end

    def readIO : UInt8
      @chip.not_nil!.readIO
    end

    def write(chipIndex : UInt8, offset : Int, data : Int, port : UInt8 = 0) : Nil
      @chip.not_nil!.writeIO(offset.to_u32!, data.to_u32!)
    end

    def writeDac(port : Int, command : Int, data : Int) : Nil
      write(0, 0x00, command)
      write(0, 0x01, data)
    end

    def writeIO(offset : Int, data : Int) : Nil
      @chip.not_nil!.writeIO(offset.to_u32!, data.to_u32!)
    end

    def setMuteMask(chipIndex : UInt8, mask : UInt32) : Nil
      @chip.not_nil!.mask = mask
    end

    # Sets the stereo mask for this chip.
    def setStereoMask(chipIndex : UInt8, mask : UInt32) : Nil
      @chip.not_nil!.stereoMask = mask
    end

    def getVolModifier : UInt32
      @volume.to_u32! * 2
    end

    def baseVolume : UInt16
      0x100_u16
    end

    # The internal interface for the chip.
    @[AlwaysInline]
    def chip : AbstractEmulator
      @chip.not_nil!
    end
  end
end
