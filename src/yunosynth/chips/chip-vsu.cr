#### YunoSynth
#### Copyright (C) 2023-2024 Remilia Scarlet <remilia@posteo.jp>
#### Portions based on VGMPlay, Copyright (C) Valley Bell
####
#### This program is free software: you can redistribute it and/or
#### modify it under the terms of the GNU Affero General Public
#### License as published by the Free Software Foundation, either
#### version 3 of the License, or (at your option) any later version.
####
#### This program is distributed in the hope that it will be useful,
#### but WITHOUT ANY WARRANTY; without even the implied warranty of
#### MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#### Affero General Public License for more details.
####
#### You should have received a copy of the GNU Affero General Public License
#### along with this program.  If not, see <https://www.gnu.org/licenses/>.
require "./emu-vsu-mednafen"

####
#### Nintendo VSU-VUE sound chip emulator interface
####

module Yuno::Chips
  # VSU-VUE sound chip emulator.
  class VsuVue < Yuno::AbstractChip
    CHIP_ID = 0x22_u32

    @clockFromHeader : UInt32 = 0
    @samplingMode : UInt8 = 0
    @chipSampleRate : UInt32 = 0
    @chip : VsuVueMednafen? = nil

    enum Core
      Mednafen

      @[AlwaysInline]
      def symbol : Symbol
        case self
        in .mednafen? then :mednafen
        end
      end
    end

    def initialize(chipNum : Int32, absChipCount : Int32, vgm : VgmFile, playbackSampleRate : UInt32,
                   newSamplingMode : UInt8, newPlayerSampleRate : UInt32, *, emuCore : Symbol? = nil,
                   flags : ChipFlags? = nil)
      @clockFromHeader = vgm.getAlternateChipClock(chipNum, Yuno::ChipType::VsuVue) || vgm.header.vsuClock
      @clockFromHeader &= 0x3FFFFFFF
      @playerSampleRate = newPlayerSampleRate
      chipCount = (vgm.header.vsuClock & 0x40000000) != 0 ? 2 : 1
      initFields(vgm, emuCore, playbackSampleRate, newSamplingMode, chipCount, flags)
    end

    protected def initialize
    end

    protected def initFields(vgm : VgmFile, emuCore : Symbol?, playbackSampleRate : UInt32, newSamplingMode : UInt8,
                             chipCount : Int32, flags : ChipFlags?) : Nil
      @volume = vgm.getChipVolume(self, ChipType::VsuVue, chipCount)
      @core = emuCore || VsuVue.defaultEmuCore
      case @core
      when :mednafen
        @samplingMode = newSamplingMode
        @chipSampleRate = @clockFromHeader <= 0 ? playbackSampleRate : @clockFromHeader
      else raise YunoError.new("Unsupported emulation core for VSU-VUE: #{@core}")
      end
    end

    def type : ChipType
      ChipType::VsuVue
    end

    def name : String
      "Nintendo VSU-VUE"
    end

    def shortName : String
      "VSU-VUE"
    end

    def id : UInt32
      CHIP_ID
    end

    def emuCore : Symbol
      @core
    end

    def self.defaultEmuCore : Symbol
      :mednafen
    end

    def start(chipIndex : UInt8, clock : UInt32, flags : ChipFlags? = nil, previousChip : AbstractChip? = nil) : UInt32
      case @core
      when :mednafen
        @sampleRate = clock.tdiv(120) # Most effects run with a /120 divider
        if (@samplingMode == 0x01 && @sampleRate < @chipSampleRate) || @samplingMode == 0x02
          @sampleRate = @chipSampleRate
        end

        @chip = VsuVueMednafen.new(clock, @sampleRate)
        @chip.not_nil!.unmuteAll
        @sampleRate

      else raise YunoError.new("Unsupported emulation core for VSU-VUE: #{@core}")
      end
    end

    def getClock : UInt32
      @clockFromHeader
    end

    def update(outputs : OutputBuffers, samples : Int) : Nil
      @chip.not_nil!.update(outputs, samples.to_u32!)
    end

    def reset(chipIndex : UInt8) : Nil
      @chip.not_nil!.reset
    end

    def read(chipIndex : UInt8, offset : Int) : UInt8|UInt16|UInt32
      raise "Cannot read from VSU-VUE"
    end

    def write(chipIndex : UInt8, offset : Int, data : Int, port : UInt8 = 0) : Nil
      @chip.not_nil!.write(offset.to_u32!, data.to_u8!)
    end

    def writeDac(port : Int, command : Int, data : Int) : Nil
      Yuno.warn("VSU-VUE does not yet support DAC write commands")
    end

    def setMuteMask(chipIndex : UInt8, mask : UInt32) : Nil
      @chip.not_nil!.muteMask = mask
    end

    def getVolModifier : UInt32
      @volume.to_u32
    end

    def baseVolume : UInt16
      0x100_u16
    end

    # The internal interface for the chip.
    @[AlwaysInline]
    def chip : AbstractEmulator
      @chip.not_nil!
    end
  end
end
