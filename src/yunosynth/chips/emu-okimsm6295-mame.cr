#### streaming ADPCM driver
#### Copyright (C) Aaron Giles
#### Copyright (C) 2023-2024 Remilia Scarlet
#### BSD 3-Clause License
####
#### Library to transcode from an ADPCM source to raw PCM.
#### Written by Buffoni Mirko in 08/06/97
#### References: various sources and documents.
####
#### HJB 08/31/98
#### modified to use an automatically selected oversampling factor
#### for the current sample rate
####
#### Mish 21/7/99
#### Updated to allow multiple OKI chips with different sample rates
####
#### R. Belmont 31/10/2003
#### Updated to allow a driver to use both MSM6295s and "raw" ADPCM voices (gcpinbal)
#### Also added some error trapping for MAME_DEBUG builds
####
#### Remilia Scarlet March 2023
#### Ported by hand to Crystal

module Yuno::Chips
  class OKIMSM6295 < Yuno::AbstractChip
    private class OKIMSM6295Mame < Yuno::AbstractEmulator
      MAX_SAMPLE_CHUNK = 16_u32
      NUM_VOICES = 4

      INDEX_SHIFT = [ -1i8, -1i8, -1i8, -1i8, 2i8, 4i8, 6i8, 8i8 ]

      # Volume lookup table.  The manual lists only 9 steps, ~3dB per step.
      # Given the dB values, that seems to map to a 5-bit volume control.  Any
      # volume parameter beyond the 9th index results in silent playback.
      VOLUME_TABLE = [
        0x20_u32, #     0 dB
        0x16_u32, #  -3.2 dB
        0x10_u32, #  -6.0 dB
        0x0b_u32, #  -9.2 dB
        0x08_u32, # -12.0 dB
        0x06_u32, # -14.5 dB
        0x04_u32, # -18.0 dB
        0x03_u32, # -20.5 dB
        0x02_u32, # -24.0 dB
        0x00_u32,
        0x00_u32,
        0x00_u32,
        0x00_u32,
        0x00_u32,
        0x00_u32,
        0x00_u32,
      ]

      NIBBLE_TO_BIT = [
        [ 1i8, 0i8, 0i8, 0i8], [ 1i8, 0i8, 0i8, 1i8], [ 1i8, 0i8, 1i8, 0i8], [ 1i8, 0i8, 1i8, 1i8],
        [ 1i8, 1i8, 0i8, 0i8], [ 1i8, 1i8, 0i8, 1i8], [ 1i8, 1i8, 1i8, 0i8], [ 1i8, 1i8, 1i8, 1i8],
        [-1i8, 0i8, 0i8, 0i8], [-1i8, 0i8, 0i8, 1i8], [-1i8, 0i8, 1i8, 0i8], [-1i8, 0i8, 1i8, 1i8],
        [-1i8, 1i8, 0i8, 0i8], [-1i8, 1i8, 0i8, 1i8], [-1i8, 1i8, 1i8, 0i8], [-1i8, 1i8, 1i8, 1i8]
      ]

      NMK_BANK_TABLE_BITS = 8u32
      NMK_BANK_TABLE_SIZE = 1u32 << NMK_BANK_TABLE_BITS # 0x100
      NMK_TABLE_SIZE      = 4u32 * NMK_BANK_TABLE_SIZE  # 0x400
      NMK_TABLE_MASK      = NMK_TABLE_SIZE - 1u32       # 0x3FF
      NMK_BANK_BITS       = 16u32
      NMK_BANK_SIZE       = 1u32 << NMK_BANK_BITS       # 0x10000
      NMK_BANK_MASK       = NMK_BANK_SIZE - 1u32        # 0xFFFF
      NMK_ROM_BASE        = 4u32 * NMK_BANK_SIZE        # 0x40000

      ###
      ### Class Fields
      ###

      @@tablesComputed : Bool = false
      protected class_property? tablesComputed

      @@diffLookup : Array(Int32) = Array(Int32).new(49 * 16, 0)
      protected class_getter diffLookup

      ###
      ### Inner Classes
      ###

      class AdpcmState
        property signal : Int32 = -2
        property step : Int32 = 0

        def initialize
        end

        # Resets the ADPCM stream.
        @[AlwaysInline]
        def reset
          OKIMSM6295Mame.computeTables unless OKIMSM6295Mame.tablesComputed?
          @signal = -2
          @step = 0
        end

        # Clocks the next ADPCM byte.
        @[AlwaysInline]
        def clock(nibble : UInt8) : Int16
          {% if flag?(:yunosynth_wd40) %}
            @signal += OKIMSM6295Mame.diffLookup.get!(@step * 16 + (nibble & 15))
          {% else %}
            @signal += OKIMSM6295Mame.diffLookup[@step * 16 + (nibble & 15)]
          {% end %}

          # Clamp
          @signal = @signal.clamp(-2048, 2047)

          # Adjust the step size and clamp
          @step += INDEX_SHIFT.get!(nibble & 7)
          @step = @step.clamp(0, 48)

          # Return the signal
          @signal.to_i16!
        end
      end

      class AdpcmVoice
        property playing : UInt8 = 0 # 1 if we're actively playing.
        property baseOffset : UInt32 = 0 # Pointer to the base memory location.
        property sample : UInt32 = 0 # Current sample number.
        property count : UInt32 = 0 # Total samples to play.
        property adpcm : AdpcmState = AdpcmState.new # Current ADPCM state.
        property volume : UInt32 = 0 # Output volume.
        property muted : UInt8 = 0

        def initialize
        end
      end

      ###
      ### Fields
      ###

      @voices : Array(AdpcmVoice)
      @command : Int16 = -1
      @bankOffset : Int32 = 0
      @pin7State : UInt8 = 0
      @nmkMode : UInt8 = 0
      @nmkBank : Bytes = Bytes.new(4)
      @masterClock : UInt32 = 0
      @initialClock : UInt32 = 0
      @rom : Bytes = Bytes.new(0)

      @sampleRateFunc : SampleRateCallback? = nil
      @sampleRateData : AbstractChip? = nil

      @sampleDataBuf : Slice(Int16) = Slice(Int16).new(MAX_SAMPLE_CHUNK.to_i32!, 0i16)

      ###
      ### Methods
      ###

      def initialize(clock : UInt32)
        @voices = Array(AdpcmVoice).new(NUM_VOICES) { |_| AdpcmVoice.new }
        OKIMSM6295Mame.computeTables

        @initialClock = clock
        @masterClock = clock & 0x7FFFFFFF
        @pin7State = ((clock & 0x80000000) >> 31).to_u8!
      end

      def rate : UInt32
        divisor = (@pin7State != 0 ? 132_u32 : 165_u32)
        @masterClock.tdiv(divisor)
      end

      def reset : Nil
        @command = -1
        @bankOffset = 0
        @nmkMode = 0
        @nmkBank.fill(0)
        @masterClock = @initialClock & 0x7FFFFFFF
        @pin7State = ((@initialClock & 0x80000000) >> 31).to_u8!

        @voices.each do |voice|
          voice.volume = 0
          voice.adpcm.reset
          voice.playing = 0
        end
      end

      def read(offset : Int) : UInt8
        result : UInt8 = 0xF0 # naname expects bits 4-7 to be 1

        # Set the bit to 1 if something is playing on a given channel.
        @voices.each_with_index do |voice, idx|
          if voice.playing != 0
            result |= 1u8 << idx
          end
        end

        result
      end

      @[AlwaysInline]
      def write(offset : Int, data : UInt8) : Nil
        case offset
        when 0x00
          writeCommand(data)
        when 0x08
          @masterClock &= ~0x000000FF_u32
          @masterClock |= data
        when 0x09
          @masterClock &= ~0x0000FF00_u32
          @masterClock |= (data.to_u32! << 8)
        when 0x0A
          @masterClock &= ~0x00FF0000_u32
          @masterClock |= (data.to_u32! << 16)
        when 0x0B
          data &= 0x7F # Fix a bug in MAME VGM logs.
          @masterClock &= ~0xFF000000_u32
          @masterClock |= (data.to_u32! << 24)
          clockChanged
        when 0x0C
          setPin7(data)
        when 0x0E # NMK112 bank switch enable
          @nmkMode = data
        when 0x0F
          setBankBase(data.to_i32! << 18)
        when 0x10, 0x11, 0x12, 0x13
          @nmkBank.put!(offset & 0x03, data)
        end
      end

      def update(outputs : OutputBuffers, samples : UInt32) : Nil
        outL : Slice(Int32) = outputs[0]
        outR : Slice(Int32) = outputs[1]

        # Check once, then we can avoid bounds checks below.
        if outL.size < samples || outR.size < samples
          raise "OKI MSM6295 update: Bad number of samples (expected #{samples}, " \
                "but buffers have #{outL.size} and #{outR.size})"
        end

        outL.fill(0, 0, samples)
        outR.fill(0, 0, samples)
        bufferPos = 0
        remaining : UInt32 = 0
        numSamples : UInt32 = 0

        @voices.each do |voice|
          if voice.muted == 0
            @sampleDataBuf.fill(0)
            bufferPos = 0
            remaining = samples

            # While we still have samples remaining.
            while remaining != 0
              numSamples = (remaining > MAX_SAMPLE_CHUNK ? MAX_SAMPLE_CHUNK : remaining)

              generateAdpcm(voice, numSamples)
              numSamples.times do |smp|
                # Mono output
                outL.unsafe_put(bufferPos, outL.unsafe_fetch(bufferPos) + @sampleDataBuf.unsafe_fetch(smp))
                outR.unsafe_put(bufferPos, outL.unsafe_fetch(bufferPos))
                bufferPos += 1
              end

              remaining -= numSamples
            end
          end
        end
      end

      def writeRom(romSize : Int32, dataStart : Int32, dataLength : Int32, romData : Slice(UInt8)) : Nil
        if @rom.size != romSize
          @rom = Bytes.new(romSize, 0xFF_u8)
        end

        return if dataStart > romSize
        dataLength = romSize - dataStart if dataStart + dataLength > romSize

        # Copy the memory
        dataLength.times do |i|
          # This can be guaranteed by the checks above
          @rom.unsafe_put(i + dataStart, romData.unsafe_fetch(i))
        end
      end

      def muteMask=(mask : UInt32) : Nil
        @voices.each_with_index do |voice, idx|
          voice.muted = ((mask >> idx) & 1).to_u8!
        end
      end

      @[AlwaysInline]
      def setSampleRateChangeCB(func : SampleRateCallback, data : AbstractChip) : Nil
        @sampleRateFunc = func
        @sampleRateData = data
      end

      # Computes the difference tables.
      protected def self.computeTables : Nil
        stepVal : Int64 = 0

        # Loop over all possible steps.
        49.times do |step|
          # Compute the step value.
          stepVal = (16.0 * ((11.0 / 10.0) ** step)).floor.to_i64!

          # Loop over all nibbles and compute the difference.
          16.times do |nib|
            @@diffLookup[step * 16 + nib] = (NIBBLE_TO_BIT[nib][0].to_i64! *
                                             (stepVal      * NIBBLE_TO_BIT[nib][1].to_i64! +
                                              stepVal.tdiv(2) * NIBBLE_TO_BIT[nib][2].to_i64! +
                                              stepVal.tdiv(4) * NIBBLE_TO_BIT[nib][3].to_i64! +
                                              stepVal.tdiv(8))).to_i32!
          end
        end

        @@tablesComputed = true
      end

      ###
      ### Private Methods
      ###

      private def memoryRawReadByte(offset : UInt32) : UInt8
        curOff : Int32 = 0

        if @nmkMode == 0
          curOff = @bankOffset | offset
        else
          bankId : UInt8 = 0

          if offset < NMK_TABLE_SIZE && Yuno.bitflag?(@nmkMode, 0x80)
            # Pages sample table
            bankId = (offset >> NMK_BANK_TABLE_BITS).to_u8!
            curOff = (offset & NMK_TABLE_MASK).to_i32!
          else
            bankId = (offset >> NMK_BANK_BITS).to_u8!
            curOff = (offset & NMK_BANK_MASK).to_i32!
          end

          curOff |= (@nmkBank.get!(bankId & 0x03).to_i32! << NMK_BANK_BITS)
        end

        if curOff < @rom.size
          @rom.unsafe_fetch(curOff)
        else
          0u8
        end
      end

      private def generateAdpcm(voice : AdpcmVoice, samples : UInt32) : Nil
        bufferPos = 0

        # Only fill the buffer if the voice is active.
        if voice.playing != 0
          base : UInt32 = voice.baseOffset
          sample : UInt32 = voice.sample
          count : UInt32 = voice.count
          nibble : UInt8 = 0

          # Loop while we still have samples to generate.
          while samples != 0
            # Compute the new amplitude and update the current step.
            nibble = memoryRawReadByte(base + sample.tdiv(2)) >> (((sample & 1) << 2) ^ 4)

            {% if flag?(:yunosynth_wd40) %}
              @sampleDataBuf.put!(bufferPos,
                                  (voice.adpcm.clock(nibble).to_i32! * voice.volume.tdiv(2).to_i32!).to_i16!)
            {% else %}
              @sampleDataBuf[bufferPos] = (voice.adpcm.clock(nibble).to_i32! * voice.volume.tdiv(2).to_i32!).to_i16!
            {% end %}
            bufferPos += 1
            samples -= 1

            # Advance
            sample += 1
            if sample >= count
              voice.playing = 0
              break
            end
          end

          # Update the parameters.
          voice.sample = sample
        end

        # Fill the rest with silence
        while samples > 0
          {% if flag?(:yunosynth_wd40) %}
            @sampleDataBuf.put!(bufferPos, 0i16)
          {% else %}
            @sampleDataBuf[bufferPos] = 0i16
          {% end %}
          bufferPos += 1
          samples -= 1
        end
      end

      # Set the base of the bank for a given voice on a given chip.
      @[AlwaysInline]
      private def setBankBase(base : Int32) : Nil
        @bankOffset = base
      end

      @[AlwaysInline]
      private def clockChanged : Nil
        divisor : UInt32 = (@pin7State != 0 ? 132_u32 : 165_u32)
        @sampleRateFunc.try do |fn|
          fn.call(@sampleRateData.not_nil!, @masterClock.tdiv(divisor))
        end
      end

      @[AlwaysInline]
      private def setPin7(pin7 : Int) : Nil
        @pin7State = pin7.to_u8!
        clockChanged
      end

      # Write to the data port.
      private def writeCommand(data : UInt8) : Nil
        temp : UInt8 = 0
        start : UInt32 = 0
        stop : UInt32 = 0
        base : UInt32 = 0

        # If a command is pending, process the second half.
        if @command != -1
          temp = data >> 4

          # The manual explicitly says that it's not possible to start multiple
          # voices at the same time.
          if temp != 0 && temp != 1 && temp != 2 && temp != 4 && temp != 8
            RemiLib.log.error("OKI MSM6295: Starting a voice that should not be started: #{temp}")
          end

          # Determine which voice(s) (voice is set by a 1 bit in the upper 4
          # bits of the second byte).
          @voices.each_with_index do |voice, idx|
            if Yuno.bitflag?(temp, 1)
            # Determine the start/stop positions
              base = @command.to_u32! * 8

              start  = memoryRawReadByte(base).to_u32! << 16
              start |= memoryRawReadByte(base + 1).to_u32! << 8
              start |= memoryRawReadByte(base + 2).to_u32!
              start &= 0x3FFFF

              stop  = memoryRawReadByte(base + 3).to_u32! << 16
              stop |= memoryRawReadByte(base + 4).to_u32! << 8
              stop |= memoryRawReadByte(base + 5).to_u32!
              stop &= 0x3FFFF

              # Set up the voice to play this sample.
              if start < stop
                if voice.playing == 0 # Fixes Got-cha and Steel Force
                  voice.playing = 1
                  voice.baseOffset = start
                  voice.sample = 0
                  voice.count = 2u32 * (stop &- start &+ 1)

                  # Also reset the ADPCM parameters.
                  voice.adpcm.reset
                  voice.volume = VOLUME_TABLE.get!(data & 0x0F)
                end
              else
                # Invalid samples go here.
                Yuno.warn("OKI MSM6295: Voice #{idx} requested an invalid sample #{@command}")
                voice.playing = 0
              end
            end

            temp >>= 1
          end

          # Reset the command
          @command = -1

        elsif Yuno.bitflag?(data, 0x80)
          # If this is the start of a command, remember the sample number for
          # next time.
          @command = (data & 0x7F).to_i16!
        else
          # Otherwise, see if this is a silence command.
          temp = data >> 3

          @voices.each do |voice|
            voice.playing = 0 if Yuno.bitflag?(temp, 1)
            temp >>= 1
          end
        end
      end
    end
  end
end
