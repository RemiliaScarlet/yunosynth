#### C352 sound chip emulator
#### Copyright R. Belmont
#### Copyright superctr
#### Copyright (C) 2023-2024 Remilia Scarlet
####
#### BSD 3-Clause License
####
#### Original C implementation by R. Belmont and superctr.
####   Additional code in the original by cync and the hoot development team.
####
####   Thanks to Cap of VivaNonno for info and The_Author for preliminary reverse-engineering
####
#### Chip specs:
####  * 32 voices
####  * Supports 8-bit linear and 8-bit muLaw samples
####  * Output: digital, 16 bit, 4 channels
####  * Output sample rate is the input clock / (288 * 2).

module Yuno::Chips
  class C352 < Yuno::AbstractChip
    # Actual implementation of the C352 emulator based on Mame's implementation
    private class C352Mame < Yuno::AbstractEmulator
      ##
      ## Constants
      ##
      ## Note: Do not make the FLG_ constants an Enum, it slows down performance
      ##

      FLG_BUSY      = 0x8000 # Channel is busy
      FLG_KEY_ON    = 0x4000 # Key on
      FLG_KEY_OFF   = 0x2000 # Key off
      FLG_LOOP_TRIG = 0x1000 # Loop trigger
      FLG_LOOP_HIST = 0x0800 # Loop history
      FLG_FM        = 0x0400 # Frequency modulation
      FLG_PHASE_RL  = 0x0200 # Rear/Left invert phase 180 degrees
      FLG_PHASE_FL  = 0x0100 # Front/Left invert phase 180 degrees
      FLG_PHASE_FR  = 0x0080 # Invert phase 180 degrees (flip sign of sample)
      FLG_LDIR      = 0x0040 # Loop direction
      FLG_LINK      = 0x0020 # "long-format" sample (can't loop)
      FLG_NOISE     = 0x0010 # Play noise instead of the sample
      FLG_MULAW     = 0x0008 # Sample is µLaw encoded
      FLG_NO_FILTER = 0x0004 # Don't Apply Filter
      FLG_REVLOOP   = 0x0003 # Loop backward
      FLG_LOOP      = 0x0002 # Loop forward
      FLG_REVERSE   = 0x0001 # Play the sample backwards

      NUM_VOICES = 32

      ##
      ## Internal classes
      ##

      private class Voice
        property pos : UInt32 = 0
        property counter : UInt32 = 0
        property sample : Int16 = 0
        property lastSample : Int16 = 0
        property volF : UInt16 = 0
        property volR : UInt16 = 0
        property curVolume : Array(UInt8) = Array(UInt8).new(4, 0u8)
        property freq : UInt16 = 0
        property flags : UInt16 = 0
        property waveBank : UInt16 = 0
        property waveStart : UInt16 = 0
        property waveEnd : UInt16 = 0
        property waveLoop : UInt16 = 0
        property mute : UInt8 = 0

        def initialize
        end

        @[AlwaysInline]
        def rampVolume(channel : Int, val : UInt8) : Nil
          {% unless flag?(:yunosynth_wd40) %}
            raise "Bad channel nummber" unless channel < 4 && channel >= 0
          {% end %}
          if (volDelta = @curVolume.unsafe_fetch(channel).to_i16! - val) != 0
            @curVolume.unsafe_put(channel, @curVolume.unsafe_fetch(channel) + (volDelta > 0 ? -1 : 1))
          end
        end
      end

      ##
      ## Fields
      ##

      @sampleRateBase : UInt32 = 0
      @divider : UInt16 = 0
      @voices : Array(Voice)
      @random : UInt16 = 0
      @control : UInt16 = 0
      @wave : Bytes = Bytes.new(0)
      @waveMask : UInt32 = 0
      @muteRear : UInt8 = 0 # This is derived from a flag in the VGM header
      @mulawTable : Array(Int16) = Array(Int16).new(256, 0i16)
      @muteAllRear : UInt8 = 0u8

      ##
      ## Methods
      ##

      def initialize(clock : UInt32, clockDiv : UInt32)
        @divider = (clockDiv != 0 ? clockDiv.to_u16 : 288u16)
        @sampleRateBase = (clock & 0x7FFFFFFF).tdiv(@divider)
        @muteRear = ((clock & 0x80000000) >> 31).to_u8
        @voices = Array(Voice).new(NUM_VOICES) { |_| Voice.new }

        self.muteMask = 0

        j : UInt16 = 0
        128.times do |i|
          @mulawTable[i] = (j << 5).to_i16!
          case
          when i < 16 then j += 1
          when i < 24 then j += 2
          when i < 48 then j += 4
          when i < 100 then j += 8
          else j += 16
          end
        end

        (128...256).each do |i|
          @mulawTable[i] = (~@mulawTable[i - 128]) & 0xFFE0
        end
      end

      @[AlwaysInline]
      private def fetchSample(voice : Voice) : Nil
        voice.lastSample = voice.sample

        if Yuno.bitflag?(voice.flags, FLG_NOISE)
          @random = ((@random.to_i16! >> 1) ^ ((-(@random.to_i16! & 1)) & 0xFFF6)).to_u16!
          voice.sample = @random.to_i16!
        else
          smp : UInt8 = @wave.unsafe_fetch(voice.pos & @waveMask)

          voice.sample = smp.to_i16! << 8
          if Yuno.bitflag?(voice.flags, FLG_MULAW)
            # We can guarantee the index since it's a UInt8, and the size is always 256
            voice.sample = @mulawTable.unsafe_fetch(smp)
          end

          pos : UInt32 = voice.pos & 0xFFFF

          if Yuno.bitflag?(voice.flags, FLG_LOOP) && Yuno.bitflag?(voice.flags, FLG_REVERSE)
            # Backwards -> Forwards
            if Yuno.bitflag?(voice.flags, FLG_LDIR) && pos == voice.waveLoop
              voice.flags &= ~FLG_LDIR
            elsif !Yuno.bitflag?(voice.flags, FLG_LDIR) && pos == voice.waveEnd
              voice.flags |= FLG_LDIR
            end

            voice.pos += (Yuno.bitflag?(voice.flags, FLG_LDIR) ? -1 : 1)

          elsif pos == voice.waveEnd
            case
            when Yuno.bitflag?(voice.flags, FLG_LINK) && Yuno.bitflag?(voice.flags, FLG_LOOP)
              voice.pos = (voice.waveStart.to_u32! << 16) | voice.waveLoop
              voice.flags |= FLG_LOOP_HIST

            when Yuno.bitflag?(voice.flags, FLG_LOOP)
              voice.pos = (voice.pos & 0xFF0000) | voice.waveLoop
              voice.flags |= FLG_LOOP_HIST

            else
              voice.flags |= FLG_KEY_OFF
              voice.flags &= ~FLG_BUSY
              voice.sample = 0
            end
          else
            voice.pos += (Yuno.bitflag?(voice.flags, FLG_REVERSE) ? -1 : 1)
          end
        end
      end

      def update(outputs : OutputBuffers, samples : UInt32) : Nil
        smp : Int16 = 0
        nextCounter : UInt32 = 0
        outL : Slice(Int32) = outputs[0]
        outR : Slice(Int32) = outputs[1]

        # Check once, then we can avoid bounds checks below.
        if outL.size < samples || outR.size < samples
          raise "C352 update: Bad number of samples (expected #{samples}, " \
                "but buffers have #{outL.size} and #{outR.size})"
        end

        # We'll use this as a temporary internal buffer
        outbuf = StaticArray(StreamSample, 4).new(0)

        # Clear output buffers
        outL.fill(0, 0, samples)
        outR.fill(0, 0, samples)

        # Render
        samples.times do |i|
          # Clear temporary buffer
          outbuf.fill(0)

          # Render each voice
          @voices.each do |voice|
            smp = 0

            # Only look at voices that are doing something
            if Yuno.bitflag?(voice.flags, FLG_BUSY)
              nextCounter = voice.counter + voice.freq
              fetchSample(voice) if Yuno.bitflag?(nextCounter, 0x10000)

              if Yuno.bitflag?(nextCounter ^ voice.counter, 0x18000)
                voice.rampVolume(0, (voice.volF >> 8).to_u8!)
                voice.rampVolume(1, (voice.volF & 0xFF).to_u8!)
                voice.rampVolume(2, (voice.volR >> 8).to_u8!)
                voice.rampVolume(3, (voice.volR & 0xFF).to_u8!)
              end

              voice.counter = nextCounter & 0xFFFF
              smp = voice.sample

              # Interpolate samples
              unless Yuno.bitflag?(voice.flags, FLG_NO_FILTER)
                smp = (voice.lastSample.to_i64! +
                       (voice.counter.to_i64! * (voice.sample.to_i64! - voice.lastSample.to_i64!) >> 16)).to_i16!
              end
            end

            # Mix into temporary output buffer if the voice isn't muted.
            if voice.mute == 0
              # Left
              outbuf.unsafe_put(0, outbuf.unsafe_fetch(0) &+
                                   (((Yuno.bitflag?(voice.flags, FLG_PHASE_FL) ? -smp.to_i64! : smp.to_i64!) &*
                                     voice.curVolume.unsafe_fetch(0).to_i32!) >> 8).to_i16!)
              outbuf.unsafe_put(2, outbuf.unsafe_fetch(2) &+
                                   (((Yuno.bitflag?(voice.flags, FLG_PHASE_RL) ? -smp.to_i64! : smp.to_i64!) &*
                                     voice.curVolume.unsafe_fetch(2).to_i32!) >> 8).to_i16!)

              # Right
              outbuf.unsafe_put(1, outbuf.unsafe_fetch(1) &+
                                   (((Yuno.bitflag?(voice.flags, FLG_PHASE_FR) ? -smp.to_i64! : smp.to_i64!) &*
                                     voice.curVolume.unsafe_fetch(1).to_i32!) >> 8).to_i16!)
              outbuf.unsafe_put(3, outbuf.unsafe_fetch(3) &+
                                   (((Yuno.bitflag?(voice.flags, FLG_PHASE_FR) ? -smp.to_i64! : smp.to_i64!) &*
                                     voice.curVolume.unsafe_fetch(3).to_i32!) >> 8).to_i16!)
            end
          end

          # The unsafe_fetches on the outputs can be guaranteed
          # because we already called outputs[x] with these indicies
          # above.

          # Mix into output buffers
          outL.unsafe_put(i, outL.unsafe_fetch(i) + outbuf.unsafe_fetch(0))
          outR.unsafe_put(i, outR.unsafe_fetch(i) + outbuf.unsafe_fetch(1))

          # Also mix the quadraphonic sound in as stereo
          if @muteRear == 0 && @muteAllRear == 0
            outL.unsafe_put(i, outL.unsafe_fetch(i) + outbuf.unsafe_fetch(2))
            outR.unsafe_put(i, outR.unsafe_fetch(i) + outbuf.unsafe_fetch(3))
          end
        end
      end

      def start : UInt32
        @sampleRateBase
      end

      def reset : Nil
        mmask = self.getMuteMask

        # Clear channel states
        @voices = Array(Voice).new(NUM_VOICES) { |_| Voice.new }

        # Init noise generator
        @random = 0x1234
        @control = 0

        self.muteMask = mmask
      end

      @[AlwaysInline]
      def read(address : Int) : UInt16
        if address < 0x100 # Channel registers
          voice = @voices.unsafe_fetch(address.tdiv(8))
          case address % 8
          when 0 then voice.volF
          when 1 then voice.volR
          when 2 then voice.freq
          when 3 then voice.flags
          when 4 then voice.waveBank
          when 5 then voice.waveStart
          when 6 then voice.waveEnd
          when 7 then voice.waveLoop
          else 0u16
          end
        elsif address == 0x200
          @control
        else
          0u16
        end
      end

      @[AlwaysInline]
      def write(address : Int, val : UInt16) : Nil
        case
        when address < 0x100 # Channel registers
          voice = @voices.unsafe_fetch(address.tdiv(8))
          case address % 8
          when 0 then voice.volF = val
          when 1 then voice.volR = val
          when 2 then voice.freq = val
          when 3 then voice.flags = val
          when 4 then voice.waveBank = val
          when 5 then voice.waveStart = val
          when 6 then voice.waveEnd = val
          when 7 then voice.waveLoop = val
          end

        when address == 0x200
          @control = val

        when address == 0x202 # Key on/off
          @voices.each do |voc|
            if Yuno.bitflag?(voc.flags, FLG_KEY_ON)
              voc.pos = (voc.waveBank.to_u32! << 16) | voc.waveStart
              voc.sample = 0
              voc.lastSample = 0
              voc.counter = 0xFFFF
              voc.flags |= FLG_BUSY
              voc.flags &= ~(FLG_KEY_ON | FLG_LOOP_HIST)
              voc.curVolume.unsafe_put(0, 0)
              voc.curVolume.unsafe_put(1, 0)
              voc.curVolume.unsafe_put(2, 0)
              voc.curVolume.unsafe_put(3, 0)
            elsif Yuno.bitflag?(voc.flags, FLG_KEY_OFF)
              voc.flags &= ~(FLG_BUSY | FLG_KEY_OFF)
              voc.counter = 0xFFFF
            end
          end
        end
      end

      def writeRom(romSize : Int32, dataStart : Int32, dataLength : Int32, romData : Slice(UInt8)) : Nil
        if @wave.size != romSize
          @wave = Bytes.new(romSize, 0xFF_u8)
          @waveMask = 1
          while @waveMask < @wave.size
            @waveMask <<= 1
          end
          @waveMask -= 1
        end

        return if dataStart > romSize
        dataLength = romSize - dataStart if dataStart + dataLength > romSize

        # Copy the memory
        dataLength.times do |i|
          # This can be guaranteed by the checks above
          @wave.unsafe_put(i + dataStart, romData.unsafe_fetch(i))
        end
      end

      def muteMask=(mask : UInt32) : Nil
        @voices.each_with_index do |v, idx|
          v.mute = ((mask >> idx) & 0x01).to_u8!
        end
      end

      private def getMuteMask : UInt32
        mask : UInt32 = 0
        @voices.each_with_index do |v, idx|
          mask |= (v.mute.to_u32! << idx)
        end
        mask
      end

      def options=(flags : UInt8) : Nil
        @muteAllRear = flags & 0x01
      end

      def unmuteAll : Nil
        @voices.each &.mute=(0)
      end
    end
  end
end
