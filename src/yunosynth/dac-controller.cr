#### YunoSynth
#### Copyright (C) 2023-2024 Remilia Scarlet <remilia@posteo.jp>
#### Based on VGMPlay, Copyright (C) Valley Bell
####
#### This program is free software: you can redistribute it and/or
#### modify it under the terms of the GNU Affero General Public
#### License as published by the Free Software Foundation, either
#### version 3 of the License, or (at your option) any later version.
####
#### This program is distributed in the hope that it will be useful,
#### but WITHOUT ANY WARRANTY; without even the implied warranty of
#### MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#### Affero General Public License for more details.
####
#### You should have received a copy of the GNU Affero General Public License
#### along with this program.  If not, see <https://www.gnu.org/licenses/>.
require "./common"
require "./dac/*"

module Yuno
  # The `DacController` class implements the VGM specification for the "DAC
  # Stream Control Driver".  This is used to stream data from data blocks in a
  # VGM file to the chip emulators via chip writes.
  private class DacController
    # :nodoc:
    PCM_BANK_COUNT = 0x40

    # :nodoc:
    DCTRL_LMODE_IGNORE = 0x00

    # :nodoc:
    DCTRL_LMODE_CMDS = 0x01

    # :nodoc:
    DCTRL_LMODE_MSEC = 0x02

    # :nodoc:
    DCTRL_LMODE_TO_END = 0x03

    # :nodoc:
    DCTRL_LMODE_BYTES = 0x0F

    ############################################################################
    ###
    ### Fields
    ###

    protected property chipTable : ChipTable = ChipTable.new
    @pcmTable : BankTable = BankTable.new

    # Maps a bank type to a PcmBank instance.
    @banks : Array(PcmBank)

    # Maps a stream ID to a bank.
    @bankMap : Hash(UInt8, BankMapping) = {} of UInt8 => BankMapping

    @streams : Hash(UInt8, Control) = {} of UInt8 => Control
    @sampleRate : UInt32
    @bankTable : BankTable = BankTable.new

    ############################################################################
    ###
    ### Public Methods
    ###

    # Creates a new `DacController` instance.
    def initialize(@sampleRate : UInt32)
      @banks = Array(PcmBank).new(PCM_BANK_COUNT) do |_|
        PcmBank.new
      end
    end

    @[AlwaysInline]
    def seekToPcmBank(bankNum : Int, pos : UInt32) : Nil
      #Yuno.dlog!(sprintf("Seeking PCM Bank $%02x to $%08x", bankNum, pos))
      @banks[bankNum].dataPos = pos
    end

    # Writes data to the chips for `sampleCount` samples.
    def update(sampleCount : UInt32) : Nil
      @streams.each_value do |streamID|
        update(streamID, sampleCount)
      end
    end

    # # Resets the instance to the initial state.
    # def reset : Nil
    #   @banks.each do |bnk|
    #     if bnk
    #       bnk.dataPos = 0
    #       bnk.bankPos = 0
    #     end
    #   end

    #   @bankTable.entryCount = 0
    # end

    # Adds PCM data to the current instance.
    def addPcmData(dataType : UInt8, dataSize : UInt32, data : Bytes) : Nil
      Yuno.dlog!("Adding PCM data of type #{dataType}, size #{dataSize} (array size: #{data.size})")
      bnkType : UInt8 = dataType & 0x3F
      #return if bnkType > PCM_BANK_COUNT # Remi: Not possible because of the logand

      if dataType == 0x7F
        readPCMTable(dataSize, data)
        return
      end

      tempPcm : PcmBank = @banks[bnkType]
      tempPcm.bankPos += 1

      # Speed hack for restarting playback (skip already loaded data)
      return if tempPcm.bankPos <= tempPcm.banks.size

      curBank : Int32 = tempPcm.banks.size
      tempPcm.banks << PcmBank::PcmData.new

      bankSize : UInt32 = unless Yuno.bitflag?(dataType, 0x40)
                            dataSize
                          else
                            Yuno.readInt32LE(data[1..]).to_u32!
                          end
      tempPcm.data = tempPcm.data.realloc((tempPcm.dataSize + bankSize).to_i32, 0u8)
      tempBank : PcmBank::PcmData = tempPcm.banks[curBank]
      tempBank.dataStart = tempPcm.dataSize

      unless Yuno.bitflag?(dataType, 0x40)
        tempBank.dataSize = dataSize
        tempBank.data = tempPcm.data[tempBank.dataStart..]
        dataSize.times do |i|
          tempBank.data[i] = data[i]
        end
      else
        tempBank.data = tempPcm.data[tempBank.dataStart..]
        unless decompressDataBlock(tempBank, dataSize, data)
          Yuno.warn("Decompression of DAC PCM data failed")
          tempBank.data = Bytes.new(0)
          tempBank.dataSize = 0

          @streams.each_value do |chip|
            refreshData(chip, tempPcm.data)
          end

          return
        end
      end

      if bankSize != tempBank.dataSize
        raise YunoError.new("Error reading data block: data size conflict")
      end

      tempPcm.dataSize += bankSize

      @streams.each_value do |chip|
        refreshData(chip, tempPcm.data)
      end
    end

    def getPointerFromPcmBank(typ : UInt8, dataPos : UInt32) : Slice(UInt8)?
      Yuno.dlog!("Getting PCM bank pointer for type #{typ} at pos #{dataPos}")
      if typ >= PCM_BANK_COUNT || dataPos > @banks[typ].dataSize
        nil
      else
        @banks[typ].data[dataPos..]
      end
    end

    ############################################################################
    ###
    ### Protected Methods
    ###

    # :nodoc:
    @[AlwaysInline]
    protected def streamEnabled?(streamID : UInt8) : Bool
      @streams.has_key?(streamID)
    end

    # :nodoc:
    @[AlwaysInline]
    protected def reset(streamID : UInt8) : Nil
      @streams[streamID].try &.reset
    end

    # protected def enable(chip : ChipType) : Nil
    # end

    @[AlwaysInline]
    protected def getDacFromPcmBank : UInt8
      bank = @banks[0]
      dataPos = bank.dataPos
      if dataPos >= bank.dataSize
        0x80_u8
      else
        bank.dataPos += 1
        bank.data[dataPos]
      end
    end

    protected def setupChip(chipType : ChipType, chipIndex : UInt8, streamID : UInt8, command : UInt16) : Nil
      Yuno.dlog!("Setting up DAC for type #{chipType}, stream ID #{streamID}, with command #{command}")

      unless @bankMap.has_key?(streamID)
        Yuno.dlog!("Enabling DAC for chip #{chipType} index #{chipIndex} with stream ID #{streamID}")
        startChipStream(chipType, chipIndex, streamID)
        reset(streamID)
        @bankMap[streamID] = BankMapping.new(chipType)
      end

      chip = @streams[streamID]
      chip.destCommand = command
      case chip.destChipType
      when .sn76489?
        if Yuno.bitflag?(chip.destCommand, 0x10)
          chip.commandSize = 1 # Volume write
        else
          chip.commandSize = 2 # Frequency write
        end

      when .ym2612?
        chip.commandSize = 1

      when .pwm?, .qsound?
        chip.commandSize = 2

      else
        chip.commandSize = 1
      end

      chip.dataStep = chip.commandSize * chip.stepSize
    end

    protected def setFrequency(streamID : UInt8, frequency : UInt32) : Nil
      if chip = @streams[streamID]?
        return unless chip.running?

        if frequency != 0
          chip.step = (chip.step * chip.frequency).tdiv(frequency)
        end
        chip.frequency = frequency
      end
    end

    @[AlwaysInline]
    protected def bankCount(streamID : UInt8) : UInt32
      @bankMap[streamID].bank.banks.size.to_u32!
    end

    # Assigns PCM data from the given bank ID to the given stream ID.
    protected def assignPCMData(streamID : UInt8, bankID : UInt8, stepSize : UInt8, stepBase : UInt8) : Nil
      Yuno.dlog!("Assinging PCM data to bank #{bankID}, step size #{stepSize}, step base #{stepBase}")
      bank : PcmBank = @banks[bankID >= PCM_BANK_COUNT ? 0 : bankID]
      @bankMap[streamID].bank = bank
      setData(streamID, bank.data, stepSize, stepBase)
    end

    protected def refreshData(chip : Control, data : Bytes) : Nil
      Yuno.dlog!("Refreshing data for chip #{chip.destChipType} with #{data.size} bytes of data")
      return unless chip.running?

      if data.size > 0
        chip.dataLen = data.size.to_u32!
        chip.data = data
      else
        chip.dataLen = 0
        chip.data = Bytes.new(0)
      end
    end

    # Plays a block of compressed data on a DAC stream.  This essentially sets
    # up some stuff, then calls `#start`.
    @[AlwaysInline]
    protected def playBlock(streamID : UInt8, blockID : UInt16, flags : UInt8) : Nil
      pcm = @bankMap[streamID].bank
      bank = pcm.banks[blockID >= pcm.banks.size ? 0 : blockID]
      lenMode = DCTRL_LMODE_BYTES |
                (flags.to_u32! & 0x10) |      # Reverse mode
                ((flags.to_u32! & 0x01) << 7) # Looping
      start(streamID, bank.dataStart.to_u32!, lenMode.to_u8!, bank.dataSize)
    end

    # Starts playing data on a DAC stream.
    protected def start(streamID : UInt8, dataPos : UInt32, lenMode : UInt8, length : UInt32) : Nil
      chip = @streams[streamID]
      return unless chip.running?

      cmdStepBase : UInt16 = chip.commandSize.to_u16! * chip.stepBase.to_u16!

      # Skip setting dataStart if we're at -1
      if dataPos != 0xFFFFFFFF
        chip.dataStart = dataPos + cmdStepBase
        if chip.dataStart > chip.dataLen
          chip.dataStart = chip.dataLen
        end
      end

      case lenMode & 0x0F
      when DCTRL_LMODE_IGNORE
        nil # Length is already set - ignore
      when DCTRL_LMODE_CMDS
        chip.commandsToSend = length
      when DCTRL_LMODE_MSEC
        chip.commandsToSend = (1000u32 * length).tdiv(chip.frequency)
      when DCTRL_LMODE_TO_END
        chip.commandsToSend = (chip.dataLen - (chip.dataStart - cmdStepBase)).tdiv(chip.dataStep)
      when DCTRL_LMODE_BYTES
        chip.commandsToSend = length.tdiv(chip.dataStep)
      else
        chip.commandsToSend = 0
      end

      chip.reverse = (lenMode & 0x10) >> 4

      chip.remainingCommands = chip.commandsToSend
      chip.step = 0
      chip.pos = 0

      if chip.reverse == 0
        chip.realPos = 0
      else
        chip.realPos = (chip.commandsToSend - 1) * chip.dataStep
      end

      chip.running &= 0x04
      chip.running |= (Yuno.bitflag?(lenMode, 0x80) ? 4 : 0) # Set loop mode
      chip.running |= 1 # Start
      chip.running &= ~10u8 # Command isn't yet sent
    end

    # Stops the given DAC stream.
    @[AlwaysInline]
    protected def stop(streamID : UInt8) : Nil
      chip = @streams[streamID]
      return unless chip.running?
      chip.running &= ~1u8 # Stop
    end

    # Stops all DAC streams.
    @[AlwaysInline]
    protected def stopAll : Nil
      @streams.each_value do |chip|
        next unless chip.running?
        chip.running &= ~1u8 # Stop
      end
    end

    ############################################################################
    ###
    ### Private Methods
    ###

    private def readPCMTable(dataSize : UInt32, data : Bytes) : Nil
      Yuno.dlog!("Reading PCM table of size #{dataSize} (array size: #{data.size})")
      @bankTable.compressionType = data[0]
      @bankTable.compressionSubType = data[1]
      @bankTable.bitDec = data[2]
      @bankTable.bitCmp = data[3]
      @bankTable.entryCount = Yuno.readInt16LE(data[4..]).to_u16!

      valSize : UInt8 = (@bankTable.bitDec &+ 7).tdiv(8)
      tableSize : UInt64 = @bankTable.entryCount.to_u64! * valSize.to_u64!

      @bankTable.entries = data[6..(6 + tableSize)]

      if dataSize < 0x06 + tableSize
        Yuno.warn("Bad PCM table length")
      end
    end

    private def setData(streamID : UInt8, data : Bytes, stepSize : UInt8, stepBase : UInt8) : Nil
      Yuno.dlog!("Setting data for DAC stream #{streamID}, step size #{stepSize}, step base #{stepBase}, data size #{data.size}")
      chip = @streams[streamID]
      return unless chip.running?

      if data.size > 0
        chip.dataLen = data.size.to_u32!
        chip.data = data
      else
        chip.dataLen = 0
        chip.data = Bytes.new(0)
      end

      chip.stepSize = stepSize != 0 ? stepSize : 1u8
      chip.stepBase = stepBase
      chip.dataStep = chip.commandSize * chip.stepSize
    end

    # Instructs the chip to update itself by rendering data from the DAC.
    @[AlwaysInline]
    private def update(chip : Control, sampleCount : UInt32) : Nil
      # Chip is disabled?
      return unless chip.running?

      # Chip is stopped?
      return unless Yuno.bitflag?(chip.running, 0x01)

      newPos : UInt32 = 0
      realDataStep : Int16 = chip.reverse == 0 ? chip.dataStep.to_i16! : -(chip.dataStep.to_i16!)

      if sampleCount > 0x20
        # Very effective speed hack for fast seeking.
        newPos = chip.step + (sampleCount - 0x10)
        newPos = mulDiv64Round(newPos * chip.dataStep.to_u32!, chip.frequency, @sampleRate)
        while chip.remainingCommands != 0 && chip.pos < newPos
          chip.pos += chip.dataStep
          chip.realPos += realDataStep
          chip.remainingCommands -= 1
        end
      end

      chip.step += sampleCount

      # Formula: Step * Freq / SampleRate
      newPos = mulDiv64Round(chip.step * chip.dataStep.to_u32!, chip.frequency, @sampleRate)
      sendCommand(chip)

      while chip.remainingCommands != 0 && chip.pos < newPos
        sendCommand(chip)
        chip.pos += chip.dataStep
        chip.realPos = chip.realPos &+ realDataStep
        chip.running &= ~0x10_u8
        chip.remainingCommands -= 1
      end

      if chip.remainingCommands == 0 && Yuno.bitflag?(chip.running, 0x04)
        # Loop back to start
        chip.remainingCommands = chip.commandsToSend
        chip.step = 0
        chip.pos = 0
        if chip.reverse == 0
          chip.realPos = 0
        else
          chip.realPos = (chip.commandsToSend - 1) * chip.dataStep
        end
      end

      # Stop chip?
      chip.running &= ~0x01_u8 if chip.remainingCommands == 0
    end

    private def startChipStream(chipType : ChipType, chipIndex : UInt8, streamID : UInt8) : Nil
      ctrl = Control.new
      ctrl.destChipType = chipType
      ctrl.destChipIndex = chipIndex
      ctrl.destCommand = 0
      ctrl.running = 0xFF # Disable all actions for now
      @streams[streamID] = ctrl
    end

    private def sendCommand(chip : Control) : Nil
      port : UInt8 = 0
      command : UInt8 = 0
      data : UInt8 = 0

      # Command already sent?
      return if Yuno.bitflag?(chip.running, 0x10)

      return if chip.dataStart.to_u64! + chip.realPos.to_u64! >= chip.dataLen

      chipData : Bytes = chip.data[(chip.dataStart + chip.realPos)..]

      case chip.destChipType
      ##
      ## Main chips
      ##
      when .ym2612? # 16-bit Register (actually 9 Bit), 8-bit Data
        port = ((chip.destCommand & 0xFF00) >> 8).to_u8!
        command = (chip.destCommand & 0x00FF).to_u8!
        data = chipData[0]
        chipInstance : AbstractChip = @chipTable[chip.destChipType][chip.destChipIndex]
        chipInstance.writeDac(port, command, data)

      when .pwm?
        port = (chip.destCommand & 0x000F).to_u8!
        command = chipData[1] & 0x0F
        data = chipData[0]
        chipInstance = @chipTable[chip.destChipType][chip.destChipIndex]
        chipInstance.writeDac(port, command, data)

      ##
      ## Other chips (for completeness)
      ##

      # 8-bit register, 8-bit data
      when .ym2151?, .ym2203?, .ay8910?, .ymz280b?, .multi_pcm?, .dmg?, .okim6258?, .k053260?, .upd7759?, .y8950?,
           .ym3526?, .nes_apu?, .ym2413?
        command = (chip.destCommand & 0x00FF).to_u8!
        data = chipData[0]
        chipInstance = @chipTable[chip.destChipType][chip.destChipIndex]
        chipInstance.writeDac(0, command, data)

      # 16-bit register, 8-bit data
      when .k051649?, .k054539?, .c140?, .ym2608?, .ym2610?, .ym3812?, .ymf262?
        port = ((chip.destCommand & 0xFF00) >> 8).to_u8!
        command = (chip.destCommand & 0x00FF).to_u8!
        data = chipData[0]
        chipInstance = @chipTable[chip.destChipType][chip.destChipIndex]
        chipInstance.writeDac(port, command, data)

      # 8-bit register, 16-bit data
      when .qsound?
        command = (chip.destCommand & 0x00FF).to_u8!
        chipInstance = @chipTable[chip.destChipType][chip.destChipIndex]
        chipInstance.writeDac(chipData[0], chipData[1], command)

      # 8-bit Register with Channel Select, 8-bit Data
      when .rf5c68?
        port = ((chip.destCommand & 0xFF00) >> 8).to_u8!
        command = (chip.destCommand & 0x00FF).to_u8!
        data = chipData[0]
        chipInstance = @chipTable[chip.destChipType][chip.destChipIndex]
        chipInstance.writeDac(0, command, data)

        if port == 0xFF
          chipInstance.writeDac(0, command & 0x0F, data)
        else
          # Get current channel
          prevChn = port # TODO

          # Send channel select
          chipInstance.writeDac(0, command >> 4, port)

          # Send data
          chipInstance.writeDac(0, command & 0x0F, data)

          # Restore old channel
          unless prevChn == port
            chipInstance.writeDac(0, command >> 4, prevChn)
          end
        end

      # 8-bit Register with Channel Select, 8-bit Data
      when .huc6280?, .rf5c164?
        port = ((chip.destCommand & 0xFF00) >> 8).to_u8!
        command = (chip.destCommand & 0x00FF).to_u8!
        data = chipData[0]
        chipInstance = @chipTable[chip.destChipType][chip.destChipIndex]

        if port == 0xFF
          chipInstance.writeDac(0, command & 0x0F, data)
        else
          # Get current channel
          prevChn = chipInstance.read(chip.destChipIndex, 0).to_u8!

          # Send channel select
          chipInstance.writeDac(0, command >> 4, port)

          # Send data
          chipInstance.writeDac(0, command & 0x0F, data)

          # Restore old channel
          unless prevChn == port
            chipInstance.writeDac(0, command >> 4, prevChn)
          end
        end

      # OKI6295-specific
      when .okim6295?
        command = (chip.destCommand & 0x00FF).to_u8!
        data = chipData[0]
        chipInstance = @chipTable[chip.destChipType][chip.destChipIndex]

        if command == 0
          port = ((chip.destCommand & 0x0F00) >> 8).to_u8!
          if Yuno.bitflag?(data, 0x80)
            # Sample start.  Write sample ID first.
            chipInstance.writeDac(0, command, data)

            # Write channel(s) that should play the sample.
            chipInstance.writeDac(0, command, port << 4)
          else
            # Sample stop
            chipInstance.writeDac(0, command, port << 3)
          end
        else
          chipInstance.writeDac(0, command, data)
        end

      # Unknown/unsupported DAC chip
      when .unknown?
        raise "Attempted to send a DAC command to an unknown chip"
      end

      chip.running |= 0x10 # Command has been sent.
    end

    # Multiplies *multiplicand* and *multiplier* together, then rounds using *divisor*.
    @[AlwaysInline]
    private def mulDiv64Round(multiplicand : UInt32, multiplier : UInt32, divisor : UInt32) : UInt32
      # Yes, I'm correctly rounding the values.
      ((multiplicand.to_u64! * multiplier.to_u64!) + divisor.to_u64!.tdiv(2)).tdiv(divisor.to_u64!).to_u32!
    end

    # Decompresses a block of compressed PCM data.
    private def decompressDataBlock(bank : PcmBank::PcmData, dataSize : UInt32, data : Bytes) : Bool
      compressionType : UInt8 = data[0]
      bank.dataSize = Yuno.readInt32LE(data[1..]).to_u32!

      case compressionType
      when 0x00 # n-bit compression
        decompressNBit(bank, dataSize, data)

      when 0x01 # DPCM
        decompressDPCM(bank, dataSize, data)

      else
        Yuno.warn("Unknown data block compression type")
        false
      end
    end

    # Decompresses an n-bit compressed sample.
    private def decompressNBit(bank, dataSize, data) : Bool
      bitDec : UInt8 = data[5]
      bitCmp : UInt32 = data[6].to_u32!
      compressionSubType : UInt8 = data[7]
      addVal : UInt16 = Yuno.readInt16LE(data[8..]).to_u16!
      ent1B : Pointer(UInt8) = Pointer(UInt8).null
      ent2B : Pointer(UInt16) = Pointer(UInt16).null

      if compressionSubType == 0x02
        # Big Endian note: Those are stored in LE and converted when reading.
        ent1B = Pointer(UInt8).new(@bankTable.entries.to_unsafe.address)
        ent2B = Pointer(UInt16).new(@bankTable.entries.to_unsafe.address)

        if @bankTable.entryCount == 0
          bank.dataSize = 0
          Yuno.warn("Error loading table-compressed data block: no table loaded")
          return false
        elsif bitDec != @bankTable.bitDec || bitCmp != @bankTable.bitCmp
          bank.dataSize = 0
          Yuno.warn("Error loading table-compressed data block: data block and loaded table are incompatible")
          return false
        end
      end

      valSize : UInt32 =  (bitDec.to_u32! + 7).tdiv(8)
      inPos : UInt32 = 0x0A
      inDataEnd : UInt32 = dataSize
      inShift : UInt32 = 0
      outShift : UInt32 = bitDec.to_u32! - bitCmp.to_u32!
      outDataEnd : UInt32 = bank.dataSize
      outVal : UInt32 = 0
      outPos : UInt32 = 0
      outBit : UInt32 = 0
      inVal : UInt32 = 0
      bitsToRead : UInt32 = 0
      bitReadVal : UInt32 = 0
      inValB : UInt32 = 0
      bitMask : UInt32 = 0

      # Do the actual decompression.
      while outPos < outDataEnd && inPos < inDataEnd
        outBit = 0
        inVal = 0
        bitsToRead = bitCmp

        while bitsToRead != 0
          bitReadVal = (bitsToRead >= 8 ? 8u32 : bitsToRead)
          bitsToRead -= bitReadVal
          bitMask = (1u32 << bitReadVal) - 1

          inShift += bitReadVal
          inValB = (data[inPos].to_u32! << inShift >> 8) & bitMask
          if inShift >= 8
            inShift -= 8
            inPos += 1
            if inShift != 0
              inVal |= (data[inPos].to_u32! << inShift >> 8) & bitMask
            end
          end

          inVal |= inValB << outBit
          outBit += bitReadVal
        end

        case compressionSubType
        when 0x00 # Copy
          outVal = inVal + addVal

        when 0x01 # Shift left
          outVal = (inVal << outShift) + addVal

        when 0x02 # Table
          case valSize
          when 0x01 then outVal = ent1B[inVal].to_u32!
          when 0x02 then outVal = ent2B[inVal].to_u32! # NOTE: needs adjustment for big endian
          end
        end

        # NOTE: needs adjustment for big endian
        if valSize == 0x01
          bank.data[outPos] = outVal.to_u8!
        else
          bank.data[outPos] = (outVal & 0x00FF).to_u8!
          bank.data[outPos + 1] = ((outVal.to_u16! & 0xFF00) >> 8).to_u8!
        end

        outPos += valSize
      end

      true
    end

    # Decompresses a Delta-PCM encoded sample.
    private def decompressDPCM(bank, dataSize, data) : Bool
      bitDec : UInt8 = data[5]
      bitCmp : UInt32 = data[6].to_u32!
      outVal : UInt32 = Yuno.readInt16LE(data[8..]).to_u32!
      ent1B : Pointer(UInt8) = Pointer(UInt8).new(@bankTable.entries.to_unsafe.address)
      ent2B : Pointer(UInt16) = Pointer(UInt16).new(@bankTable.entries.to_unsafe.address)

      if @bankTable.entryCount == 0
        bank.dataSize = 0
        Yuno.warn("Error loading table-compressed data block: no table loaded")
        return false
      elsif bitDec != @bankTable.bitDec || bitCmp != @bankTable.bitCmp
        bank.dataSize = 0
        Yuno.warn("Error loading table-compressed data block: data block and loaded table are incompatible")
        return false
      end

      valSize : UInt32 =  (bitDec.to_u32! + 7).tdiv(8)
      outMask : UInt16 = ((1u32 << bitDec) - 1).to_u16!
      inPos : UInt32 = 0x0A
      inDataEnd : UInt32 = dataSize
      inShift : UInt32 = 0
      #outShift : UInt32 = bitDec.to_u32! - bitCmp.to_u32!
      outDataEnd : UInt32 = bank.dataSize
      addVal : UInt16 = 0
      outBit : UInt32 = 0
      inVal : UInt32 = 0
      bitsToRead : UInt32 = 0
      bitReadVal : UInt32 = 0
      inValB : UInt32 = 0
      bitMask : UInt32 = 0
      outPos : UInt32 = 0

      # Do the actual decompression.
      while outPos < outDataEnd && inPos < inDataEnd
        outBit = 0
        inVal = 0
        bitsToRead = bitCmp

        while bitsToRead != 0
          bitReadVal = (bitsToRead >= 8 ? 8u32 : bitsToRead)
          bitsToRead -= bitReadVal
          bitMask = (1u32 << bitReadVal) - 1

          inShift += bitReadVal
          inValB = ((data[inPos].to_u32! << inShift) >> 8) & bitMask

          if inShift >= 8
            inShift -= 8
            inPos += 1
            if inShift != 0
              inVal |= ((data[inPos].to_u32! << inShift) >> 8) & bitMask
            end
          end

          inVal |= inValB << outBit
          outBit += bitReadVal
        end

        case valSize
        when 0x01
          addVal = ent1B[inVal].to_u16!
          outVal += addVal
          outVal &= outMask
          bank.data[outPos] = outVal.to_u8!

        when 0x02
          addVal = ent2B[inVal]
          outVal += addVal
          outVal &= outMask
          bank.data[outPos] = (outVal & 0x00FF).to_u8!
          bank.data[outPos + 1] = ((outVal.to_i16! & 0xFF00) >> 8).to_u8!
        end

        outPos += valSize
      end

      true
    end
  end
end
