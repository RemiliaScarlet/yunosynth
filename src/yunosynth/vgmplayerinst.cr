#### YunoSynth
#### Copyright (C) 2023-2024 Remilia Scarlet <remilia@posteo.jp>
#### Portions based on VGMPlay, Copyright (C) Valley Bell
####
#### This program is free software: you can redistribute it and/or
#### modify it under the terms of the GNU Affero General Public
#### License as published by the Free Software Foundation, either
#### version 3 of the License, or (at your option) any later version.
####
#### This program is distributed in the hope that it will be useful,
#### but WITHOUT ANY WARRANTY; without even the implied warranty of
#### MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#### Affero General Public License for more details.
####
#### You should have received a copy of the GNU Affero General Public License
#### along with this program.  If not, see <https://www.gnu.org/licenses/>.

####
#### VGM Playback Command Implementations
####
#### Parts of this code were taken from VGMPlay.
####

module Yuno
  class VgmPlayer

    ###
    ### Chip Instructions
    ###
    ### Each instruction returns the number of instructions that the player
    ### should advance after calling these functions.
    ###

    # Gets a little-endian signed 16-bit integer from the data stream.
    @[AlwaysInline]
    private def getInt16LE(at : Int32) : Int32
      (@vgm.data[at + 1].to_i32! <<  8) |
       @vgm.data.get!(at).to_i32!
    end

    # Gets a big-endian signed 16-bit integer from the data stream.
    @[AlwaysInline]
    private def getInt16BE(at : Int32) : Int32
      (@vgm.data[at    ].to_i32! <<  8) |
       @vgm.data[at + 1].to_i32!
    end

    # Gets a little-endian signed 24-bit integer (as an Int32) from the data
    # stream.
    @[AlwaysInline]
    private def getInt24LE(at : Int32) : Int32
      (@vgm.data[at + 2].to_i32! <<  16) |
      (@vgm.data.get!(at + 1).to_i32! <<  8) |
       @vgm.data.get!(at).to_i32!
    end

    # Gets a little-endian signed 32-bit integer from the data stream.
    @[AlwaysInline]
    private def getInt32LE(at : Int32) : Int32
      (@vgm.data[at + 3].to_i32! << 24) |
      (@vgm.data.get!(at + 2).to_i32! << 16) |
      (@vgm.data.get!(at + 1).to_i32! <<  8) |
       @vgm.data.get!(at).to_i32!
    end

    # Writes to the HuC6280 core's registers.
    @[AlwaysInline]
    private def huc6280Write : Int32
      curChip : UInt8 = (@vgm.data[@ip + 1] & 0x80) >> 7
      chip = @chipTable[ChipType::Huc6280][curChip]
      chip.write(curChip, @vgm.data[@ip + 1] & 0x7F, @vgm.data[@ip + 2])
      3
    end

    # Writes to the K051649 core's registers.
    @[AlwaysInline]
    private def k051649Write(curChip : UInt8) : Int32
      curPort = @vgm.data[@ip + 1] & 0x7F
      chip = @chipTable[ChipType::K051649][curChip]
      chip.write(curChip, (curPort << 1) | 0x00, @vgm.data[@ip + 2])
      chip.write(curChip, (curPort << 1) | 0x01, @vgm.data[@ip + 3])
      4
    end

    # Writes to the AY-1-8910 core's registers.
    @[AlwaysInline]
    private def ay8910Write : Int32
      curChip : UInt8 = (@vgm.data[@ip + 1] & 0x80) >> 7
      chip = @chipTable[ChipType::Ay8910][curChip]
      chip.write(curChip, 0x00, @vgm.data[@ip + 1] & 0x7F)
      chip.write(curChip, 0x01, @vgm.data[@ip + 2])
      3
    end

    # Sets the stereo mask for the AY-1-8910 core.
    @[AlwaysInline]
    private def ay8910SetStereoMask : Int32
      tmpByte = @vgm.data[@ip + 1]
      tmpLng = tmpByte.to_u32! & 0x3F
      curChip : UInt8 = Yuno.bitflag?(tmpByte, 0x80) ? 1u8 : 0u8

      if (tmpByte & 0x40) != 0
        chip = @chipTable[ChipType::Ym2203][curChip].as(Yuno::Chips::YM2203)
        chip.psgStereoMask = tmpLng
      else
        chip = @chipTable[ChipType::Ay8910][curChip]
        chip.setStereoMask(curChip, tmpLng)
      end

      2
    end

    # Writes to the C352 core's registers.
    @[AlwaysInline]
    private def c352Write : Int32
      curChip : UInt8 = (@vgm.data[@ip + 1] & 0x80) >> 7
      chip = @chipTable[ChipType::C352][curChip]
      address = ((@vgm.data[@ip + 1].to_u32! & 0x7F) << 8) | @vgm.data[@ip + 2].to_u32!
      chip.write(curChip, address, ((@vgm.data[@ip + 3].to_u16! << 8) | @vgm.data[@ip + 4].to_u16!))
      5
    end

    # Writes to the YM2151 core's registers.
    @[AlwaysInline]
    private def ym2151Write(curChip) : Int32
      chip = @chipTable[ChipType::Ym2151][curChip]
      chip.write(curChip, 0x00, @vgm.data[@ip + 1])
      chip.write(curChip, 0x01, @vgm.data[@ip + 2])
      3
    end

    # Writes to the Sega MultiPCM core's registers.
    @[AlwaysInline]
    private def multiPCMWrite : Int32
      curChip : UInt8 = (@vgm.data[@ip + 1] & 0x80) >> 7
      chip = @chipTable[ChipType::MultiPcm][curChip]
      chip.write(curChip, @vgm.data[@ip + 1] & 0x7F, @vgm.data[@ip + 2])
      3
    end

    # Writes to the Sega MultiPCM core's internal memory.
    @[AlwaysInline]
    private def multiPCMWriteMemory : Int32
      curChip : UInt8 = (@vgm.data[@ip + 1] & 0x80) >> 7
      chip = @chipTable[ChipType::MultiPcm][curChip]
      tempShort = getInt16LE(@ip + 2)
      chip.as(Yuno::Chips::MultiPCM).writeBank(@vgm.data[@ip + 1] & 0x7F, tempShort.to_u16!)
      4
    end

    # Writes to the YMZ280B core's registers.
    @[AlwaysInline]
    private def ymz280bWrite(curChip) : Int32
      chip = @chipTable[ChipType::Ymz280b][curChip]
      chip.write(curChip, 0x00, @vgm.data[@ip + 1])
      chip.write(curChip, 0x01, @vgm.data[@ip + 2])
      3
    end

    # Writes to the OKI MSM6258 core's registers.
    @[AlwaysInline]
    private def okiMsm6258Write : Int32
      curChip : UInt8 = (@vgm.data[@ip + 1] & 0x80) >> 7
      chip = @chipTable[ChipType::Okim6258][curChip]
      chip.write(curChip, @vgm.data[@ip + 1] & 0x7F, @vgm.data[@ip + 2])
      3
    end

    # Writes to the QSound core's registers.
    @[AlwaysInline]
    private def qsoundWrite(curChip : UInt8) : Int32
      chip = @chipTable[ChipType::Qsound][curChip]
      chip.write(curChip, 0x00, @vgm.data[@ip + 1])
      chip.write(curChip, 0x01, @vgm.data[@ip + 2])
      chip.write(curChip, 0x02, @vgm.data[@ip + 3])
      4
    end

    # Writes to the OKI MSM6295 core's registers.
    @[AlwaysInline]
    private def okiMsm6295Write : Int32
      curChip : UInt8 = (@vgm.data[@ip + 1] & 0x80) >> 7
      chip = @chipTable[ChipType::Okim6295][curChip]
      chip.write(curChip, @vgm.data[@ip + 1] & 0x7F, @vgm.data[@ip + 2])
      3
    end

    # Writes to the DMG core's registers.
    @[AlwaysInline]
    private def dmgWrite : Int32
      curChip : UInt8 = (@vgm.data[@ip + 1] & 0x80) >> 7
      chip = @chipTable[ChipType::Dmg][curChip]
      chip.write(curChip, @vgm.data[@ip + 1] & 0x7F, @vgm.data[@ip + 2])
      3
    end

    # Writes to the YM2203 core's registers.
    @[AlwaysInline]
    private def ym2203Write(curChip : UInt8) : Int32
      chip = @chipTable[ChipType::Ym2203][curChip]
      chip.write(curChip, 0, @vgm.data[@ip + 1])
      chip.write(curChip, 1, @vgm.data[@ip + 2])
      3
    end

    # Writes to the K054539 core's registers.
    @[AlwaysInline]
    private def k054539Write : Int32
      curChip : UInt8 = (@vgm.data[@ip + 1] & 0x80) >> 7
      chip = @chipTable[ChipType::K054539][curChip].as(Yuno::Chips::K054539)
      offset = ((@vgm.data[@ip + 1] & 0x7F).to_i32! << 8) | @vgm.data[@ip + 2].to_i32!
      chip.write(curChip, offset, @vgm.data[@ip + 3])
      4
    end

    # Writes to the C140 core's registers.
    @[AlwaysInline]
    private def c140Write : Int32
      curChip : UInt8 = (@vgm.data[@ip + 1] & 0x80) >> 7
      offset = ((@vgm.data[@ip + 1] & 0x7F).to_i32! << 8) | @vgm.data[@ip + 2].to_i32!
      chip = @chipTable[ChipType::C140][curChip].as(Yuno::Chips::C140)
      chip.write(curChip, offset, @vgm.data[@ip + 3])
      4
    end

    # Writes to the K053260 core's registers.
    @[AlwaysInline]
    private def k053260Write : Int32
      curChip : UInt8 = (@vgm.data[@ip + 1] & 0x80) >> 7
      chip = @chipTable[ChipType::K053260][curChip].as(Yuno::Chips::K053260)
      chip.write(curChip, @vgm.data[@ip + 1] & 0x7F, @vgm.data[@ip + 2])
      3
    end

    # Writes to the SegaPCM core's registers.
    @[AlwaysInline]
    private def segaPCMWrite(curChip : UInt8) : Int32
      offset = getInt16LE(@ip + 1)
      chip = @chipTable[ChipType::SegaPcm][curChip].as(Yuno::Chips::SegaPCM)
      chip.write(((offset & 0x8000) >> 15).to_u8!, offset & 0x7FFF, @vgm.data[@ip + 3])
      4
    end

    # Writes to the GA20 core's registers.
    @[AlwaysInline]
    private def ga20Write : Int32
      curChip : UInt8 = (@vgm.data[@ip + 1] & 0x80) >> 7
      chip = @chipTable[ChipType::Ga20][curChip].as(Yuno::Chips::GA20)
      chip.write(curChip, @vgm.data[@ip + 1] & 0x7F, @vgm.data[@ip + 2])
      3
    end

    # Writes to the YM2608 core's registers.
    @[AlwaysInline]
    private def ym2608Write(curChip : UInt8, cmd : UInt8) : Int32
      chip = @chipTable[ChipType::Ym2608][curChip].as(Yuno::Chips::YM2608)
      port = (cmd & 0x01) << 1
      chip.write(curChip, port | 0x00, @vgm.data[@ip + 1])
      chip.write(curChip, port | 0x01, @vgm.data[@ip + 2])
      3
    end

    # Writes to the YM2610 core's registers.
    @[AlwaysInline]
    private def ym2610Write(curChip : UInt8, cmd : UInt8) : Int32
      chip = @chipTable[ChipType::Ym2610][curChip].as(Yuno::Chips::YM2610)
      port = (cmd & 0x01) << 1
      chip.write(curChip, port | 0x00, @vgm.data[@ip + 1])
      chip.write(curChip, port | 0x01, @vgm.data[@ip + 2])
      3
    end

    # Writes to the µPD7759 core's registers.
    @[AlwaysInline]
    private def upd7759Write : Int32
      curChip : UInt8 = (@vgm.data[@ip + 1] & 0x80) >> 7
      chip = @chipTable[ChipType::Upd7759][curChip].as(Yuno::Chips::Upd7759)
      chip.write(curChip, @vgm.data[@ip + 1] & 0x7F, @vgm.data[@ip + 2])
      3
    end

    # Writes to the ES5503 core's registers.
    @[AlwaysInline]
    private def es5503Write : Int32
      curChip : UInt8 = (@vgm.data[@ip + 1] & 0x80) >> 7
      port = @vgm.data[@ip + 0x01] & 0x7F
      offset = @vgm.data[@ip + 0x02]
      data = @vgm.data[@ip + 0x03]
      chip = @chipTable[ChipType::Es5503][curChip].as(Yuno::Chips::ES5503)
      chip.write(curChip.to_u8!, offset, data, port)
      4
    end

    # Writes to the YM3812 core's registers.
    @[AlwaysInline]
    private def ym3812Write(curChip : UInt8) : Int32
      chip = @chipTable[ChipType::Ym3812][curChip].as(Yuno::Chips::YM3812)
      chip.write(curChip, 0x00, @vgm.data[@ip + 1])
      chip.write(curChip, 0x01, @vgm.data[@ip + 2])
      3
    end

    # Writes to the YMF262 core's registers.
    @[AlwaysInline]
    private def ymf262Write(curChip : UInt8, cmd : UInt8) : Int32
      chip = @chipTable[ChipType::Ymf262][curChip].as(Yuno::Chips::YMF262)
      port = (cmd & 0x01) << 1
      chip.write(curChip, port, @vgm.data[@ip + 1])
      chip.write(curChip, port | 0x01, @vgm.data[@ip + 2])
      3
    end

    # Writes to the VSU-VUE core's registers.
    @[AlwaysInline]
    private def vsuWrite : Int32
      curChip : UInt8 = ((@vgm.data[@ip + 1] & 0x80) >> 7).to_u8!
      chip = @chipTable[ChipType::VsuVue][curChip].as(Yuno::Chips::VsuVue)
      port = (@vgm.data[@ip + 1] & 0x7F).to_u32!
      offset = @vgm.data[@ip + 2].to_u32!
      data = @vgm.data[@ip + 3]
      chip.write(curChip, (port << 8) | offset, data)
      4
    end

    # Writes to the YM2612 core's registers.
    @[AlwaysInline]
    private def ym2612Write(curChip : UInt8, cmd : UInt8) : Int32
      chip = @chipTable[ChipType::Ym2612][0].as(Yuno::Chips::YM2612)
      port = (cmd & 0x01) << 1
      chip.write(curChip, port | 0x00, @vgm.data[@ip + 1])
      chip.write(curChip, port | 0x01, @vgm.data[@ip + 2])
      3
    end

    # Writes to the SN764xx core's registers.
    @[AlwaysInline]
    private def sn76489Write(curChip : UInt8) : Int32
      chip = @chipTable[ChipType::Sn76489][curChip].as(Yuno::Chips::SN76489)
      chip.write(curChip, 0, @vgm.data[@ip + 1])
      2
    end

    # Writes to the SN764xx core's registers (Game Gear-specific).
    @[AlwaysInline]
    private def gameGearWrite(curChip : UInt8) : Int32
      chip = @chipTable[ChipType::Sn76489][curChip].as(Yuno::Chips::SN76489)
      chip.write(curChip, 1, @vgm.data[@ip + 1])
      2
    end

    # Writes to the RF5C164 core's memory
    @[AlwaysInline]
    private def rf5c164MemWrite(curChip : UInt8) : Int32
      chip = @chipTable[ChipType::Rf5c164][curChip].as(Yuno::Chips::Rf5c164)
      tmpShort : Int16 = getInt16LE(@ip + 1).to_i16!
      chip.memWrite(tmpShort, @vgm.data[@ip + 3])
      4
    end

    # Writes to the RF5C164 core's registers
    @[AlwaysInline]
    private def rf5c164Write(curChip : UInt8) : Int32
      chip = @chipTable[ChipType::Rf5c164][curChip].as(Yuno::Chips::Rf5c164)
      chip.write(curChip, @vgm.data[@ip + 1], @vgm.data[@ip + 2])
      3
    end

    # Writes to the Y8950 core's registers
    @[AlwaysInline]
    private def y8950Write(curChip : UInt8) : Int32
      chip = @chipTable[ChipType::Y8950][curChip].as(Yuno::Chips::Y8950)
      chip.write(curChip, 0x00, @vgm.data[@ip + 1])
      chip.write(curChip, 0x01, @vgm.data[@ip + 2])
      3
    end

    # Writes to the YM3526 core's registers
    @[AlwaysInline]
    private def ym3526Write(curChip : UInt8) : Int32
      chip = @chipTable[ChipType::Ym3526][curChip].as(Yuno::Chips::YM3526)
      chip.write(curChip, 0x00, @vgm.data[@ip + 1])
      chip.write(curChip, 0x01, @vgm.data[@ip + 2])
      3
    end

    # Writes to the YMF278B core's registers
    # @[AlwaysInline]
    # private def ymf278BWrite : Int32
    #  curChip : UInt8 = ((@vgm.data[@ip + 1] & 0x80) >> 7).to_u8!
    #  chip = @chipTable[ChipType::Ymf278b][curChip].as(Yuno::Chips::YMF278B)
    #  port = (@vgm.data[@ip + 1] & 0x7F)
    #  chip.write(curChip, 1, port, @vgm.data[@ip + 2], @vgm.data[@ip + 3])
    #  4
    # end

    # Writes to the YMF278B core's registers
    # @[AlwaysInline]
    # private def ymf271Write : Int32
    #  curChip : UInt8 = ((@vgm.data[@ip + 1] & 0x80) >> 7).to_u8!
    #  chip = @chipTable[ChipType::Ymf271][curChip].as(Yuno::Chips::YMF271)
    #  port = (@vgm.data[@ip + 1] & 0x7F)
    #  chip.write(curChip, 1, port, @vgm.data[@ip + 2], @vgm.data[@ip + 3])
    #  4
    # end

    # Writes to the YM2413 core's registers
    @[AlwaysInline]
    private def ym2413Write(curChip : UInt8) : Int32
      chip = @chipTable[ChipType::Ym2413][curChip].as(Yuno::Chips::YM2413)
      chip.write(curChip, 0x00, @vgm.data[@ip + 1])
      chip.write(curChip, 0x01, @vgm.data[@ip + 2])
      3
    end

    # Writes to the PWM core's registers
    @[AlwaysInline]
    private def pwmWrite : Int32
      curChip : UInt8 = ((@vgm.data[@ip + 1] & 0x80) >> 7).to_u8!
      chip = @chipTable[ChipType::Pwm][curChip].as(Yuno::Chips::Pwm)
      chip.write(curChip, @vgm.data[@ip + 1] & 0x0F, @vgm.data[@ip + 2], (@vgm.data[@ip + 1] & 0xF0) >> 4)
      3
    end

    # Writes to the NES APU core's registers.
    @[AlwaysInline]
    private def nesWrite : Int32
      curChip : UInt8 = ((@vgm.data[@ip + 1] & 0x80) >> 7).to_u8!
      chip = @chipTable[ChipType::NesApu][curChip]
      chip.write(curChip, @vgm.data[@ip + 1] & 0x7F, @vgm.data[@ip + 2])
      3
    end

    # Writes to the X1-010 core's registers
    @[AlwaysInline]
    private def x1_010Write : Int32
      curChip : UInt8 = ((@vgm.data[@ip + 1] & 0x80) >> 7).to_u8!
      chip = @chipTable[ChipType::X1_010][curChip].as(Yuno::Chips::X1_010)
      port = (@vgm.data[@ip + 1].to_u16! & 0x7F) << 8
      offset = @vgm.data[@ip + 2]
      chip.write(curChip, port | offset, @vgm.data[@ip + 3])
      4
    end

    # Writes to the SAA1099 core's registers.
    @[AlwaysInline]
    private def saa1099Write : Int32
      curChip : UInt8 = ((@vgm.data[@ip + 1] & 0x80) >> 7).to_u8!
      chip = @chipTable[ChipType::Saa1099][curChip]
      chip.write(curChip, @vgm.data[@ip + 1] & 0x7F, @vgm.data[@ip + 2])
      3
    end

    # Writes to the Wonderswan core's registers.
    @[AlwaysInline]
    private def wonderswanWrite : Int32
      curChip : UInt8 = ((@vgm.data[@ip + 1] & 0x80) >> 7).to_u8!
      chip = @chipTable[ChipType::Wonderswan][curChip]
      chip.write(curChip, 0x80 | @vgm.data[@ip + 1] & 0x7F, @vgm.data[@ip + 2])
      3
    end

    # Writes to the Wonderswan core's memory
    @[AlwaysInline]
    private def wonderswanRAMWrite : Int32
      curChip : UInt8 = ((@vgm.data[@ip + 1] & 0x80) >> 7).to_u8!
      chip = @chipTable[ChipType::Wonderswan][curChip].as(Yuno::Chips::Wonderswan)
      tmpShort = (getInt16BE(@ip + 1) & 0x7FFF).to_u16!
      chip.writeRam(tmpShort, @vgm.data[@ip + 3])
      4
    end

    ###
    ### PCM Instructions
    ###
    ### Each instruction returns the number of instructions that the player
    ### should advance after calling these functions.
    ###

    # Loads PCM ROM data from the VGM file's data stream.
    private def loadPCMStream : Int32
      tempByte : UInt8 = @vgm.data[@ip + 2]
      tempLong : Int32 = getInt32LE(@ip + 3)
      curChip = 0
      if (tempLong & 0x80000000) != 0
        tempLong &= 0x7FFFFFFF
        curChip = 1
      end

      # Determine how to load the data.
      case tempByte & 0xC0
      when 0x00, 0x40 # Database block
        if @timesPlayed == 0
          Yuno.dlog!("PCM loading: Database block")
          @dacControl.addPcmData(tempByte, tempLong.to_u32!, @vgm.data[@ip + 7..])
        end

      when 0x80 # ROM/RAM dump
        Yuno.dlog!("PCM loading: ROM/RAM dump")

        unless @timesPlayed != 0
          romSize = getInt32LE(@ip + 7)
          dataStart = getInt32LE(@ip + 0x0B)
          dataLen = tempLong - 8
          romDataPos = @ip + 0x0F

          Yuno.dlog!("  PCM ROM size: #{romSize}, data pos: #{dataStart}, data len: #{dataLen}, " \
                     "ROM pos:  #{romDataPos}")

          # Write the PCM data to the correct chip.
          case tempByte
          when 0x80 # SegaPCM ROM image
            Yuno.dlog!("  Loading SegaPCM ROM image")
            chip = @chipTable[ChipType::SegaPcm][curChip]
            chip.as(Yuno::Chips::SegaPCM).writeRom(romSize, dataStart, dataLen, @vgm.data[@ip + 0x0F..])

          when 0x81 # YM2608 Delta-T ROM image
            Yuno.dlog!("  Loading YM2608 Delta-T ROM image")
            chip = @chipTable[ChipType::Ym2608][curChip]
            chip.as(Yuno::Chips::YM2608).writeRom(0x02, romSize, dataStart, dataLen, @vgm.data[@ip + 0x0F..])

          when 0x82, # YM2610 ADPCM ROM image
               0x83  # YM2610 Delta-T ROM Image
            Yuno.dlog!("  Loading YM2610 ADPCM/Delta-T ROM image")
            chip = @chipTable[ChipType::Ym2610][curChip]
            tempByte = 0x01u8 + (tempByte - 0x82u8)
            chip.as(Yuno::Chips::YM2610).writeRom(tempByte, romSize, dataStart, dataLen, @vgm.data[@ip + 0x0F..])

          when 0x86 # YMZ280B ROM image
            Yuno.dlog!("  Loading YMZ280B ROM image")
            chip = @chipTable[ChipType::Ymz280b][curChip]
            chip.as(Yuno::Chips::YMZ280B).writeRom(romSize, dataStart, dataLen, @vgm.data[@ip + 0x0F..])

          when 0x88 # Y8950 Delta-T ROM image
            Yuno.dlog!("  Loading Y8950 DeltaT ROM image")
            chip = @chipTable[ChipType::Y8950][curChip]
            chip.as(Yuno::Chips::Y8950).writeRom(romSize, dataStart, dataLen, @vgm.data[@ip + 0x0F..])

          when 0x89
            Yuno.dlog!("  Loading MultiPCM ROM image")
            chip = @chipTable[ChipType::MultiPcm][curChip]
            chip.as(Yuno::Chips::MultiPCM).writeRom(romSize, dataStart, dataLen, @vgm.data[@ip + 0x0F..])

          when 0x8A
            Yuno.dlog!("  Loading uPD7759 ROM image")
            chip = @chipTable[ChipType::Upd7759][curChip]
            chip.as(Yuno::Chips::Upd7759).writeRom(romSize, dataStart, dataLen, @vgm.data[@ip + 0x0F..])

          when 0x8B
            Yuno.dlog!("  Loading OKI MSM6295 ROM image")
            chip = @chipTable[ChipType::Okim6295][curChip]
            chip.as(Yuno::Chips::OKIMSM6295).writeRom(romSize, dataStart, dataLen, @vgm.data[@ip + 0x0F..])

          when 0x8C
            Yuno.dlog!("  Loading Konami K054539 ROM image")
            chip = @chipTable[ChipType::K054539][curChip]
            chip.as(Yuno::Chips::K054539).writeRom(romSize, dataStart, dataLen, @vgm.data[@ip + 0x0F..])

          when 0x8D
            Yuno.dlog!("  Loading C140 ROM image")
            chip = @chipTable[ChipType::C140][curChip]
            chip.as(Yuno::Chips::C140).writeRom(romSize, dataStart, dataLen, @vgm.data[@ip + 0x0F..])

          when 0x8E
            Yuno.dlog!("  Loading Konami K053260 ROM image")
            chip = @chipTable[ChipType::K053260][curChip]
            chip.as(Yuno::Chips::K053260).writeRom(romSize, dataStart, dataLen, @vgm.data[@ip + 0x0F..])

          when 0x8F
            Yuno.dlog!("  Loading QSound ROM image")
            chip = @chipTable[ChipType::Qsound][curChip]
            chip.as(Yuno::Chips::QSound).writeRom(romSize, dataStart, dataLen, @vgm.data[@ip + 0x0F..])

          when 0x91 # X1-010 ROM image
            Yuno.dlog!("  Loading X1-010 ROM image")
            chip = @chipTable[ChipType::X1_010][curChip]
            chip.as(Yuno::Chips::X1_010).writeRom(romSize, dataStart, dataLen, @vgm.data[@ip + 0x0F..])

          when 0x92 # C352 ROM image
            Yuno.dlog!("  Loading C352 ROM image")
            chip = @chipTable[ChipType::C352][curChip]
            chip.as(Yuno::Chips::C352).writeRom(romSize, dataStart, dataLen, @vgm.data[@ip + 0x0F..])

          when 0x93 # GA20 ROM image
            Yuno.dlog!("  Loading Irem GA20 ROM image")
            chip = @chipTable[ChipType::Ga20][curChip]
            chip.as(Yuno::Chips::GA20).writeRom(romSize, dataStart, dataLen, @vgm.data[@ip + 0x0F..])


          else
            Yuno.warn("Unsupported PCM ROM image type: #{sprintf("$%02x", tempByte)}")
          end
        end

      when 0xC0 # RAM write
        romData : Slice(UInt8) = unless Yuno.bitflag?(tempByte, 0x20)
                                   dataStart = getInt16LE(@ip + 0x07)
                                   dataLen = tempLong - 0x02
                                   @vgm.data[@ip + 0x09..]
                                 else
                                   dataStart = getInt32LE(@ip + 0x07)
                                   dataLen = tempLong - 0x04
                                   @vgm.data[@ip + 0x0B..]
                                 end

        case tempByte
        when 0xC1 # RF5C164 RAM Database
          Yuno.dlog!("  Loading data into RF5C164 RAM")
          chip = @chipTable[ChipType::Rf5c164][curChip]
          chip.as(Yuno::Chips::Rf5c164).writeRam(dataStart, dataLen, romData)

        when 0xC2 # NES APU RAM
          Yuno.dlog!("  Loading data into NES APU RAM")
          chip = @chipTable[ChipType::NesApu][curChip]
          chip.as(Yuno::Chips::Nes).writeRam(dataStart, dataLen, romData)

        when 0xE1 # ES5503 RAM
          Yuno.dlog!("  Loading data into ES5503 RAM")
          chip = @chipTable[ChipType::Es5503][curChip]
          chip.as(Yuno::Chips::ES5503).writeRam(dataStart, dataLen, romData)

        else
          {% if flag?(:yunosynth_debug) %}
            Yuno.warn("Unsupported chip RAM write: #{sprintf("$%02x", tempByte)}")
          {% end %}
        end
      end

      7 + tempLong
    end

    # Loads PCM RAM data from the VGM file's data stream.
    private def pcmRamWrite : Int32
      curChip : UInt8 = (@vgm.data[@ip + 2] & 0x80) >> 7
      tempByte : UInt8 = @vgm.data[@ip + 2] & 0x7F
      dataStart : Int32 = getInt24LE(@ip + 3)
      tempLong : Int32 = getInt24LE(@ip + 6)
      dataLen : Int32 = getInt24LE(@ip + 9)

      dataLen += 0x01000000 if dataLen == 0

      romData = @dacControl.getPointerFromPcmBank(tempByte, dataStart.to_u32!)
      unless romData.nil?
        case tempByte
        when 0x02
          chip = @chipTable[ChipType::Rf5c164][curChip]
          chip.as(Yuno::Chips::Rf5c164).writeRam(tempLong, dataLen, romData)
        end
      end

      0x0C
    end
  end
end
