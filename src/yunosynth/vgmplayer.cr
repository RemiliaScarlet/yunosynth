#### YunoSynth
#### Copyright (C) 2023-2024 Remilia Scarlet <remilia@posteo.jp>
#### Portions based on VGMPlay, Copyright (C) Valley Bell
####
#### This program is free software: you can redistribute it and/or
#### modify it under the terms of the GNU Affero General Public
#### License as published by the Free Software Foundation, either
#### version 3 of the License, or (at your option) any later version.
####
#### This program is distributed in the hope that it will be useful,
#### but WITHOUT ANY WARRANTY; without even the implied warranty of
#### MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#### Affero General Public License for more details.
####
#### You should have received a copy of the GNU Affero General Public License
#### along with this program.  If not, see <https://www.gnu.org/licenses/>.
require "./common"
require "./vgmfile"
require "./abstract-chips"
require "./vgmplayersettings"
require "./vgmplayerinst"
require "./dither"

####
#### VGM Playback
####
#### Most of the playback parts of this code were taken from VGMPlay.
####

module Yuno
  # The `VgmPlayer` class is used to play VGM files by rendering them to PCM data.
  class VgmPlayer
    # The minimum supported output sample rate.
    MIN_SAMPLE_RATE = 8000_u32

    # :nodoc:
    VOLUME_MODIFIER_WRAP = 0xC0

    # :nodoc:
    VGM_SAMPLE_RATE = 44100

    # The currently loaded settings for the player.
    getter settings : VgmPlayerSettings = VgmPlayerSettings.new

    # The currently loaded `VgmFile` instance.
    getter vgm : VgmFile

    # The output sample rate.
    getter sampleRate : UInt32 = 44100

    # The minimum size for the buffer to use to pass to `#render`.  This is
    # per-channel.
    getter minBufferSize : Int32 = 100

    # The total number of samples rendered so far.
    getter playTime : Int32 = 0

    # The main output volume.  If you want to change the volume, change this
    # first.  `1.0` is the "normal" output volume.
    getter mainVolume : Float64 = 1.0

    # When `true`, then the player has reached the end of the VGM file.
    getter? atEnd : Bool = false

    # The number of times the VGM file has currently played one full loop.  This
    # starts at 0.
    property timesPlayed : UInt32 = 0

    # The set of loaded chip instances.
    protected getter chipTable : ChipTable = ChipTable.new

    # The minimum number of samples that something that uses this
    # class needs to allocate for its PCM buffers.
    #protected getter samplesPerBuffer : Int32 = 100

    # The main volume will be multiplied by this when calculating the final
    # output volume.  When this is needed, the VGM file's header usually sets
    # the appropriate value.  This is automatically calculated and is provided
    # for informational purposes.
    getter volumeModifier : Float64 = 1.0

    ##
    ## Playback fields
    ##

    @vgmSamplePlayed : Int32 = 0
    @vgmSamplePos : Int32 = 0
    @vgmPlaybackRate : UInt32 = 0
    @vgmPlaybackRateMul : UInt32 = 1
    @vgmPlaybackRateDiv : UInt32 = 1
    @vgmSampleRateMul : UInt32 = 1
    @vgmSampleRateDiv : UInt32 = 1
    @vgmSampleRate : UInt32 = VGM_SAMPLE_RATE.to_u32

    ##
    ## Additional fields
    ##

    # The resampler we'll use to resample the output PCM data.
    @resampler : Resampler = Resampler.new(44100, nil)

    # Current final output volume.
    @outputVolume : Float64 = 1.0

    # VGM command stream instruction pointer
    @ip : Int32 = 0

    # Holds the rendered left and right channels.
    @streamBuffers : OutputBuffers

    # Used by #fillBuffers to calculate the final output values.
    @tempBuf : Sample(Int32) = Sample(Int32).new(0, 0)

    # Used to manage DAC stuff (which is our way of saying streamed PCM
    # samples).
    @dacControl : DacController

    # If this is `true`, then `#play` has been called and cannot be called
    # again.
    getter? playing : Bool = false

    # When true, then `#setupChipSet` has been called already.
    @chipSetCreated : Bool = false

    @dither : Ditherer = Ditherer.new
    @leftBuf32 : Array(Int32)
    @rightBuf32 : Array(Int32)

    # The size of the buffers used for rendering.  This is provided when
    # creating a new instance of the class.  See `#render` for details and
    # important notes.
    getter bufferSize : Int32

    def self.minBufferSize(sampleRate : Int) : Int32
      Math.max(100, sampleRate.to_i32!.tdiv(100 * 2))
    end

    # Creates a new `VgmPlayer` instance that will play the given VGM file.
    # `bufferSize` is the size of buffers that you will use for rendering audio.
    # See `#render` for more details and important notes.  `bufferSize` must be
    # even, and must be greater than or equal to the minimum buffer size for the
    # requested sample rate.
    #
    # If the `VgmFile` uses any chips that are not currently supported by
    # YunoSynth, then this will raise an `UnsupportedChipError`.
    def initialize(@vgm : VgmFile, @bufferSize : Int32, newSettings : VgmPlayerSettings? = nil)
      raise YunoError.new("Buffer size must be even") unless bufferSize.even?
      @minBufferSize = @sampleRate.tdiv(100 * 2).to_i32!

      if @bufferSize < @minBufferSize
        raise YunoError.new("Buffer size is too small, minimum is #{@minBufferSize} for the requested sample rate")
      end

      @leftBuf32 = Array(Int32).new(@bufferSize, 0i32)
      @rightBuf32 = Array(Int32).new(@bufferSize, 0i32)

      # Create the default settings if none were passed in.
      newSettings = VgmPlayerSettings.new if newSettings.nil?

      # Check the desired output sample rate.
      @sampleRate = newSettings.sampleRate
      raise YunoError.new("Unsupported sample rate") if sampleRate < MIN_SAMPLE_RATE

      # Create the internal buffers.
      #@samplesPerBuffer = @minBufferSize.tdiv(2)
      @streamBuffers = [Slice(Int32).new(100, 0),
                        Slice(Int32).new(100, 0)]

      # Store the settings
      @settings = newSettings if newSettings

      # Create a DAC Controller.
      @dacControl = DacController.new(@sampleRate)

      # Check for unsupported chips.
      chips = @vgm.chipsUsed
      unless (unsup = chips.keys & Yuno.unsupportedChips).empty?
        raise UnsupportedChipError.new("VGM file contains chips not yet supported by YunoSynth: #{unsup.join(" ")}")
      end

      # This is used to adjust chip output volume.  It is a per-VGM
      # setting that VGM authors can use to adjust the volumes of
      # their VGM files.
      volModBase : Int32 = if @vgm.header.volumeModifier <= VOLUME_MODIFIER_WRAP
                             @vgm.header.volumeModifier.to_i32!
                           elsif @vgm.header.volumeModifier == VOLUME_MODIFIER_WRAP + 0x01
                             VOLUME_MODIFIER_WRAP.to_i32! - 0x100
                           else
                             @vgm.header.volumeModifier.to_i32! - 0x100
                           end

      # Calculate our final volume modifier.
      @volumeModifier = 2.0 ** (volModBase / 0x20)

       # Specifically set this with the method so that the calculations are all
       # up to date.
      self.mainVolume = 1.0
    end

    @[AlwaysInline]
    private def pcmSampleToVgmSample(sampleNum : Int64) : UInt32
      VgmFile.pcmSampleToVgmSample(sampleNum, @vgmSampleRateDiv.to_i64!, @vgmSampleRateMul.to_i64!)
    end

    @[AlwaysInline]
    private def vgmSampleToPcmSample(sampleNum : Int64) : UInt32
      VgmFile.vgmSampleToPcmSample(sampleNum, @vgmSampleRateDiv.to_i64!, @vgmSampleRateMul.to_i64!)
    end

    # Returns the total number of PCM samples that the loaded VGM will generate
    # when the loop is ignored and it is played back.
    @[AlwaysInline]
    def totalPcmSamples : UInt32
      VgmFile.vgmSampleToPcmSample(@vgm.header.totalSamples, @vgmSampleRateDiv, @vgmSampleRateMul)
    end

    # Returns the total number of PCM samples that the loaded VGM will generate
    # in one loop when it is played back.
    @[AlwaysInline]
    def loopPcmSamples : UInt32
      VgmFile.vgmSampleToPcmSample(@vgm.header.loopSamples, @vgmSampleRateDiv, @vgmSampleRateMul)
    end

    # Interprets *sampleCount* PCM samples worth of VGM commands, sending them to the chip emulators.
    private def interpretVgm(sampleCount : UInt32) : Nil
      inst : UInt8 = 0 # The command instruction.

      samplePlayed : UInt32 = pcmSampleToVgmSample(@vgmSamplePlayed.to_i64! + sampleCount.to_i64!)
      data = @vgm.data

      tempByte : UInt8 = 0
      #tempShort : UInt16 = 0
      streamID : UInt8 = 0
      curChip : UInt8 = 0 # TODO: Most instructions calculate this themselves.
                          # A few do not.  This should be made consistent.

      while @vgmSamplePos <= samplePlayed # && !@atEnd
        inst = data[@ip]
        #STDOUT << sprintf("Inst %02x$ at %08x$\n", inst, @ip)

        if inst >= 0x70 && inst <= 0x8F
          case inst & 0xF0
          when 0x70
            @vgmSamplePos += (inst & 0x0F) + 1

          when 0x80
            tempByte = @dacControl.getDacFromPcmBank
            if @vgm.header.ym2612Clock != 0
              chip = @chipTable[ChipType::Ym2612][0].as(Yuno::Chips::YM2612) # HACK hardcoded to 0
              chip.write(0, 0, 0x2A)
              chip.write(0, 1, tempByte)
            end
            @vgmSamplePos += (inst & 0x0F)
          end

          @ip += 1
        else
          # This first case statement may modify the instruction.  This is done
          # so that `inst` can be adjusted to the correct instruction, and
          # curChip can be adjusted to specify the second chip of that type.
          curChip = 0
          case inst
          when 0x30
            if Yuno.bitflag?(@vgm.header.sn76489Clock, 0x40000000)
              inst += 0x20
              curChip = 0x01
            end

          when 0x3F
            if Yuno.bitflag?(@vgm.header.sn76489Clock, 0x40000000)
              inst += 0x10
              curChip = 0x01
            end

          when 0xA1
            if Yuno.bitflag?(@vgm.header.ym2413Clock, 0x40000000)
              inst -= 0x50
              curChip = 0x01
            end

          when 0xA2, 0xA3
            if Yuno.bitflag?(@vgm.header.ym2612Clock, 0x40000000)
              inst -= 0x50
              curChip = 0x01
            end

          when 0xA4
            if Yuno.bitflag?(@vgm.header.ym2151Clock, 0x40000000)
              inst -= 0x50
              curChip = 0x01
            end

          when 0xA5
            if Yuno.bitflag?(@vgm.header.ym2203Clock, 0x40000000)
              inst -= 0x50
              curChip = 0x01
            end

          when 0xA6, 0xA7
            if Yuno.bitflag?(@vgm.header.ym2608Clock, 0x40000000)
              inst -= 0x50
              curChip = 0x01
            end

          when 0xA8, 0xA9
            if Yuno.bitflag?(@vgm.header.ym2610Clock, 0x40000000)
              inst -= 0x50
              curChip = 0x01
            end

          when 0xAA
            if Yuno.bitflag?(@vgm.header.ym3812Clock, 0x40000000)
              inst -= 0x50
              curChip = 0x01
            end

          when 0xAB
            if Yuno.bitflag?(@vgm.header.ym3526Clock, 0x40000000)
              inst -= 0x50
              curChip = 0x01
            end

          when 0xAC
            if Yuno.bitflag?(@vgm.header.y8950Clock, 0x40000000)
              inst -= 0x50
              curChip = 0x01
            end

          when 0xAD
            if Yuno.bitflag?(@vgm.header.ymz280bClock, 0x40000000)
              inst -= 0x50
              curChip = 0x01
            end

          when 0xAE, 0xAF
            if Yuno.bitflag?(@vgm.header.ymf262Clock, 0x40000000)
              inst -= 0x50
              curChip = 0x01
            end
          end

          # Most of the instructions are defined in vgmplayerinst.cr.
          case inst
          ###
          ### Chip Commands
          ###

          when 0xB9 then @ip += huc6280Write
          when 0x54 then @ip += ym2151Write(curChip)
          when 0xD2 then @ip += k051649Write(curChip)
          when 0xA0 then @ip += ay8910Write
          when 0x31 then @ip += ay8910SetStereoMask
          when 0xE1 then @ip += c352Write
          when 0xB5 then @ip += multiPCMWrite
          when 0xC3 then @ip += multiPCMWriteMemory
          when 0x5D then @ip += ymz280bWrite(curChip)
          when 0xB7 then @ip += okiMsm6258Write
          when 0xC4 then @ip += qsoundWrite(curChip)
          when 0xB8 then @ip += okiMsm6295Write
          when 0xB3 then @ip += dmgWrite
          when 0x55 then @ip += ym2203Write(curChip)
          when 0xD3 then @ip += k054539Write
          when 0xD4 then @ip += c140Write
          when 0xBA then @ip += k053260Write
          when 0xC0 then @ip += segaPCMWrite(curChip)
          when 0xBF then @ip += ga20Write
          when 0x56, 0x57 then @ip += ym2608Write(curChip, inst)
          when 0xB6 then @ip += upd7759Write
          when 0x58, 0x59 then @ip += ym2610Write(curChip, inst)
          when 0xD5 then @ip += es5503Write
          when 0x5A then @ip += ym3812Write(curChip)
          when 0x5E, 0x5F then @ip += ymf262Write(curChip, inst)
          when 0xC7 then @ip += vsuWrite
          when 0x52, 0x53 then @ip += ym2612Write(curChip, inst)
          when 0x50 then @ip += sn76489Write(curChip)
          when 0x4F then @ip += gameGearWrite(curChip)
          when 0xC1, 0xC2 then @ip += rf5c164MemWrite(curChip)
          when 0xB1 then @ip += rf5c164Write(curChip)
          when 0x5C then @ip += y8950Write(curChip)
          when 0x5B then @ip += ym3526Write(curChip)
          when 0xB4 then @ip += nesWrite
          when 0xC8 then @ip += x1_010Write
          when 0xBD then @ip += saa1099Write
          when 0xB2 then @ip += pwmWrite
          when 0xBC then @ip += wonderswanWrite
          when 0xC6 then @ip += wonderswanRAMWrite
          when 0x51 then @ip += ym2413Write(curChip)

          ###
          ### DAC Commands
          ###

          when 0xE0 # Seek to PCM data bank pos
            @dacControl.seekToPcmBank(0, getInt32LE(@ip + 1).to_u32!)
            @ip += 0x05

          when 0x90 # DAC: Setup chip
            streamID = @vgm.data[@ip + 1]
            if streamID == 0xFF
              @ip += 5
            else
              chipType : UInt8 = @vgm.data[@ip + 2] & 0x7F
              chipNumber : UInt8 = (@vgm.data[@ip + 2] & 0x80) >> 7
              dacCommand : UInt16 = getInt16BE(@ip + 3).to_u16!
              @dacControl.setupChip(ChipType.from_value(chipType), chipNumber, streamID, dacCommand)
              @ip += 5
            end

          when 0x91 # DAC: Set data
            streamID = @vgm.data[@ip + 1]
            if streamID == 0xFF || !@dacControl.streamEnabled?(streamID)
              @ip += 5
            else
              @dacControl.assignPCMData(streamID, @vgm.data[@ip + 2], @vgm.data[@ip + 3], @vgm.data[@ip + 4])
              @ip += 5
            end

          when 0x92 # DAC: set frequency
            streamID = @vgm.data[@ip + 1]
            if streamID == 0xFF || !@dacControl.streamEnabled?(streamID)
              @ip += 6
            else
              @dacControl.setFrequency(streamID, getInt32LE(@ip + 2).to_u32!)
              @ip += 6
            end

          when 0x93 # DAC: play from start position
            streamID = @vgm.data[@ip + 1]
            if !@dacControl.streamEnabled?(streamID) || @dacControl.bankCount(streamID) == 0
              @ip += 0x0B
            else
              @dacControl.start(streamID, getInt32LE(@ip + 2).to_u32!, @vgm.data[@ip + 6], getInt32LE(@ip + 7).to_u32!)
              @ip += 0x0B
            end

          when 0x94 # DAC: stop immediately
            streamID = @vgm.data[@ip + 1]
            if streamID == 0xFF  || !@dacControl.streamEnabled?(streamID)
              @ip += 2
            else
              if streamID < 0xFF
                @dacControl.stop(streamID)
              else
                @dacControl.stopAll
              end

              @ip += 2
            end

          when 0x95 # DAC: Play block
            streamID = @vgm.data[@ip + 1]
            if streamID == 0xFF || !@dacControl.streamEnabled?(streamID) ||@dacControl.bankCount(streamID) == 0
              @ip += 5
            else
              @dacControl.playBlock(streamID, getInt16LE(@ip + 2).to_u16!, @vgm.data[@ip + 4])
              @ip += 5
            end

          ###
          ### Other Commands
          ###

          when 0x67 then @ip += loadPCMStream
          when 0x68 then @ip += pcmRamWrite

          # Instructions below here are defined locally for various reasons.
          when 0x61 # Delay sample by x amount
            @vgmSamplePos += getInt16LE(@ip + 1).to_i32!
            @ip += 3

          when 0x62 # Delay sample by 1/60s
            @vgmSamplePos += 735
            @ip += 1

          when 0x63 # Delay sample by 1/50s
            @vgmSamplePos += 882
            @ip += 1

          when 0x66 # End of file
            @timesPlayed += 1

            if @vgm.header.loopOffset != 0
              # We have loop data.  Do the loop.
              @ip = ((@vgm.header.loopOffset + @vgm.header.loopOffsetOffset) - @vgm.header.dataOffset).to_i32!
              if @ip >= 0 && @ip < data.size # Ensure we're within bounds
                @vgmSamplePos -= @vgm.header.loopSamples.to_i32!
                @vgmSamplePlayed -= vgmSampleToPcmSample(@vgm.header.loopSamples.to_i64!)
                samplePlayed = pcmSampleToVgmSample(@vgmSamplePlayed.to_i64! + sampleCount.to_i64!)

                Yuno.dlog!("Looped!
  IP: #{@ip}
  Data Length: #{@vgm.data.size}
  Data Offset: #{@vgm.header.dataOffset}
  Loop Offset: #{@vgm.header.loopOffset}
  Loop Offset stored at: #{@vgm.header.loopOffsetOffset}
  VGMSamplePos:    #{@vgmSamplePos}
  VGMSamplePlayed: #{@vgmSamplePlayed}
  SamplePlayed:    #{samplePlayed}")
              else
                Yuno.error("Aborting playback, instruction pointer ended up in odd place:
  IP: #{@ip}
  Data Length: #{@vgm.data.size}
  Data Offset: #{@vgm.header.dataOffset}
  Loop Offset: #{@vgm.header.loopOffset}
  Loop Offset stored at: #{@vgm.header.loopOffsetOffset}")
                @atEnd = true

                # Reset the chips
                resetChips
                break
              end
            else
              Yuno.dlog!("No loop info")
              @atEnd = true
              resetChips if @settings.hardStop?
              break
            end

          else
            # Skip unimplemented, but reserved, instructions.
            case inst & 0xF0
            when 0x00, 0x10, 0x20 then @ip += 1
            when 0x30 then @ip += 2
            when 0x40, 0xB0 then @ip += 3
            when 0xD0 then @ip += 4
            when 0xF0 then @ip += 5
            else raise sprintf("Unknown VGM instruction: $%02x at data offset $%08x", inst, @ip)
            end
          end
        end
      end
    end

    # Interprets *sampleCount* PCM samples worth of VGM commands.
    @[AlwaysInline]
    private def interpretFile(sampleCount : UInt32) : Nil
      @dacControl.update(sampleCount - 1) if sampleCount > 1
      interpretVgm(sampleCount)
      @dacControl.update(1) if sampleCount != 0

      @playTime += sampleCount
      @vgmSamplePlayed += sampleCount
    end

    # Initializes the resampler for a specific chip.
    private def setupResampling(chip : AbstractChip) : Nil
      Yuno.dlog!("#{chip.type} sample rate: #{chip.sampleRate}, target sample rate: #{@sampleRate}")

      if chip.sampleRate < @sampleRate
        chip.resamplerType = Resampler::ResamplerType::Upsampling
        Yuno.dlog!("#{chip.type}: Upsampling")
      elsif chip.sampleRate > @sampleRate
        chip.resamplerType = Resampler::ResamplerType::Downsampling
        Yuno.dlog!("#{chip.type}: Downsampling")
      else
        chip.resamplerType = Resampler::ResamplerType::Copy
        Yuno.dlog!("#{chip.type}: No resampling (copying)")
      end

      chip.curSampleNum = 0
      chip.nextSampleNum = 0
      chip.lastSampleNum = 0
      chip.lastSample.left = 0
      chip.lastSample.right = 0

      case chip.resamplerType
      when .upsampling?
        # Pre-generate the first sample
        chip.update(@streamBuffers, 1)
        chip.nextSample.left = @streamBuffers.unsafe_fetch(0)[0]
        chip.nextSample.right = @streamBuffers.unsafe_fetch(1)[0]

      else
        chip.nextSample.left = 0
        chip.nextSample.right = 0
      end
    end

    # Sets up resampling for all allocated chips.
    private def setupResampling : Nil
      @resampler = Resampler.new(@sampleRate, @streamBuffers)

      Yuno.eachChip(@chipTable) do |chip, _|
        setupResampling(chip)

        # Set up resampling for its paired chip as well.
        if chip.isPaired? && (paired = chip.paired)
          setupResampling(paired)
        end
      end
    end

    # Clamps a 64-bit signed integer value to a 32-bit signed integer range.
    @[AlwaysInline]
    private def clampInt64ToInt32(value : Int64) : Int32
      value.clamp(Int32::MIN, Int32::MAX).to_i32!
    end

    # Sets the main output volume of the rendered PCM data.  `1.0` is the
    # "normal" output volume.
    @[AlwaysInline]
    def mainVolume=(val : Float64) : Nil
      @mainVolume = val
      @outputVolume = 256.0 * (@mainVolume * @volumeModifier) + 0.5
    end

    # Fills the *left* and *right* buffers with new PCM data.
    private def fillBuffers(left : Array(Int32)|Slice(Int32), right : Array(Int32)|Slice(Int32)) : Nil
      curVolume = @outputVolume.to_i32!
      outLeft : Int32 = 0
      outRight : Int32 = 0

      {% unless flag?(:yunosynth_wd40) %}
        raise YunoError.new("Buffer size mismatch, left != right") unless left.size == right.size
      {% end %}

      left.size.times do |sampleNum|
        # Generate and resample the new sample.
        interpretFile(1)
        @resampler.resample(@chipTable, @tempBuf, 1)

        # Adjust the volume for the output
        outLeft = clampInt64ToInt32(@tempBuf.left.to_i64! * curVolume)
        outRight = clampInt64ToInt32(@tempBuf.right.to_i64! * curVolume)

        # Store the new sample
        left.unsafe_put(sampleNum, outLeft)
        right.unsafe_put(sampleNum, outRight)

        # Still needs to be adjusted for the resampler
        @tempBuf.left = ((@tempBuf.left >> 5) * curVolume).to_i32! >> 11
        @tempBuf.right = ((@tempBuf.right >> 5) * curVolume).to_i32! >> 11
      end
    end

    # Given a VGM file and an output PCM sample rate, this calculates multiplier
    # and divider values that can be used for resampling.
    #
    # The returned `Tuple` contains (in order):
    #
    # 1. The multiplier for the PCM sample rate
    # 2. The divider for the PCM sample rate
    @[AlwaysInline]
    def self.calcResampling(sampleRate, vgm : VgmFile) : Tuple(UInt32, UInt32)
      rateMul, rateDiv, _, _ = calcResampling(sampleRate, vgm)
      {rateMul, rateDiv}
    end

    # Given a VGM file, an output sample rate, and some other information, this
    # calculates multiplier and divider values that can be used for resampling.
    # The `playbackRate` parameter is a multiplier of how fast you intend to
    # play back the VGM file.  A value of `0` means to use the default playback
    # rate.  The `vgmSampleRate` parameter is the rate of VGM sampling rate.
    #
    # In nearly all cases, you want to instead use
    # `VgmPlayer.calcResampling(UInt32, VgmFile)`.
    #
    # The returned `Tuple` contains (in order):
    #
    # 1. The multiplier for the PCM sample rate
    # 2. The divider for the PCM sample rate
    # 3. The multiplier for the VGM sample rate
    # 4. The divider for the VGM sample rate
    def self.calcResampling(sampleRate : UInt32,
                            vgm : VgmFile,
                            playbackRate : UInt32 = 0,
                            vgmSampleRate : UInt32 = VGM_SAMPLE_RATE.to_u32) : Tuple(UInt32, UInt32, UInt32, UInt32)
      rateMul : UInt32 = 0
      rateDiv : UInt32 = 0
      playbackRateMul : UInt32 = 0
      playbackRateDiv : UInt32 = 0

      if playbackRate == 0 || vgm.header.rate == 0
        playbackRateMul = 1u32
        playbackRateDiv = 1u32
      else
        val = vgm.header.rate.gcd(playbackRate.to_big_i)
        playbackRateMul = vgm.header.rate // val
        playbackRateDiv = playbackRate // val
      end

      rateMul = sampleRate * playbackRateMul
      rateDiv = vgmSampleRate * playbackRateDiv

      val = rateMul.gcd(rateDiv.to_big_i)
      rateMul = rateMul // val
      rateDiv = rateDiv // val

      {rateMul, rateDiv, playbackRateMul, playbackRateDiv}
    end

    # Resets the player to the beginning.
    def reset : Nil
      @ip = 0
      @vgmSamplePlayed = 0
      @vgmSamplePos = 0

      Yuno.dlog!("Sample Rate: #{@sampleRate}")
      Yuno.dlog!("VGM Header Rate: #{@vgm.header.rate}")

      rateMul, rateDiv, playbackMul, playbackDiv = VgmPlayer.calcResampling(@sampleRate, @vgm,
                                                                            @vgmPlaybackRate, @vgmSampleRate)

      @vgmSampleRateMul = rateMul
      @vgmSampleRateDiv = rateDiv
      @vgmPlaybackRateMul = playbackMul
      @vgmPlaybackRateDiv = playbackDiv

      Yuno.dlog!("VGM Playback Rate: #{@vgmPlaybackRate}")
      Yuno.dlog!("VGM Header Rate: #{@vgm.header.rate}")
      Yuno.dlog!("VGM Playback Rate Mul: #{@vgmPlaybackRateMul}")
      Yuno.dlog!("VGM Playback Rate Div: #{@vgmPlaybackRateDiv}")
      Yuno.dlog!("VGM Sample Rate: #{@vgmSampleRate}")
      Yuno.dlog!("VGM Sample Rate Mul: #{@vgmSampleRateMul}")
      Yuno.dlog!("VGM Sample Rate Div: #{@vgmSampleRateDiv}")
    end

    # Resets all of the allocated chips.
    private def resetChips : Nil
      Yuno.eachChip(@chipTable) do |chip, idx|
        chip.reset(idx.to_u8)

        if chip.isPaired? && (paired = chip.paired)
          paired.reset(idx.to_u8)
        end
      end
    end

    # Starts the given chip.
    private def startChip(chip : AbstractChip, idx : Int, previousChip : AbstractChip? = nil)
      startFlags = chip.getStartFlags(@vgm)

      case chip
      when Yuno::Chips::DMG
        # Apply settings for DMG
        if @settings.dmgBoostWaveChan?
          startFlags.as(Yuno::Chips::DMG::DMGFlags).flags = 3u8
        else
          startFlags.as(Yuno::Chips::DMG::DMGFlags).flags = 0u8
        end
        chip.start(idx.to_u8, chip.getClock, startFlags)

      when Yuno::Chips::YM2612
        # Apply settings for YM2612
        if @settings.ym2612PseudoStereo?
          startFlags.as(Yuno::Chips::YM2612::YM2612Flags).flags &= ~(0x01 << 2)
          startFlags.as(Yuno::Chips::YM2612::YM2612Flags).flags |= 1 << 2
        end
        chip.start(idx.to_u8, chip.getClock, startFlags)

      when Yuno::Chips::SN76489
        chip.start(idx.to_u8, chip.getClock, startFlags, previousChip)

      else
        # All other chips use the default way of calling
        chip.start(idx.to_u8, chip.getClock, startFlags)
      end

      chip.getVolModifier
    end

    # Starts all allocated chips.
    private def startChips : Nil
      # Remi: This is some weird hacky stuff.
      volModifier : UInt32 = 0

      # Start each chip.  Also collect the volume modification value so that we
      # can ensure a proper output level for the given set of chips.
      @chipTable.each do |key, val|
        val.each_with_index do |chip, idx|
          case chip
          when Yuno::Chips::SN76489
            # We may need to pass in the previous chip for the SN76489 if we
            # have more than one and are on the second+ chip, and on an odd chip
            # number.  This is used, for example, by the NeoGeo Pocket Color and
            # its T6W28 chip.
            if idx > 0 && idx.odd?
              # Pass in the previous chip.
              volModifier += startChip(chip, idx, val[idx - 1])
            else
              # Treat as any other chip.
              volModifier += startChip(chip, idx)
            end
          else
            # Default chip startup
            volModifier += startChip(chip, idx)
          end
        end
      end

      # Reset all chips.
      resetChips

      # Adjust the output volume of the chips.
      while volModifier < 0x200 && volModifier != 0
        Yuno.eachChip(@chipTable) do |chip, _|
          chip.volume *= 2
          #chip.volume *= 2 if chip.paired
        end
        volModifier *= 2
      end

      while volModifier > 0x300
        Yuno.eachChip(@chipTable) do |chip, _|
          chip.volume = chip.volume.tdiv(2)
          #chip.volume = chip.volume.tdiv(2) if chip.paired
        end
        volModifier = volModifier.tdiv(2)
      end
    end

    # Allocates all needed chips and stores them in the `@chipTable`.
    private def setupChipSet : Nil
      return if @chipSetCreated  # May have been created with `chipNamesUsed` or `chipsUsed`.
      totalChips : Int32 = 0
      @vgm.chipsUsed.each do |chipType, count|
        count.times do |idx|
          chipNum = @chipTable.has_key?(chipType) ? @chipTable[chipType].size : 0
          prevChip = if idx > 0
                       @chipTable[chipType][-1]
                     else
                       nil
                     end
          chip = chipType.makeInstance(chipNum, totalChips, vgm, @sampleRate, 0, @sampleRate, @settings, prevChip)

          if chip
            unless @chipTable[chipType]?
              @chipTable[chipType] = [] of AbstractChip
            end
            @chipTable[chipType] << chip
          end
          totalChips += 1
        end
      end

      @dacControl.chipTable = @chipTable
      @chipSetCreated = true
    end

    # Starts playback.  This must be called before calling `#render`.  Calling
    # this more than once is effectively a non-op.
    def play : Nil
      return if @playing
      setupChipSet
      startChips
      reset
      setupResampling
      @playing = true

      # Workaround for YM2612 files trimmed with VGMTool.
      @chipTable.each do |ct, chips|
        if ct.ym2612?
          Yuno.dlog!("Setting isVgmInit for YM2612")
          chips.each(&.as(Yuno::Chips::YM2612).isVgmInit=(true))
        end
      end

      interpretFile(0)

      # Same workaround for YM2612 files trimmed with VGMTool, but we can turn
      # this off now.
      @chipTable.each do |ct, chips|
        if ct.ym2612?
          Yuno.dlog!("Clearing isVgmInit for YM2612")
          chips.each(&.as(Yuno::Chips::YM2612).isVgmInit=(false))
        end
      end
    end

    # Sets "#atEnd?` to `true`.
    def stop : Nil
      @atEnd = true
    end

    # Loops through the chip table, collecting all of the names for chips found
    # within it.  If *useShort* is `true`, then the short names are collected,
    # otherwise the long names are collected.  Returns a hash table where the
    # keys are the `ChipType` and the values are an array of all of the names.
    private def collectChipNamesByType(useShort : Bool) : Hash(ChipType, Array(String))
      ret : Hash(ChipType, Array(String)) = {} of ChipType => Array(String)
      ngpCount : Int32 = 0
      ngpName : String = ""
      name : String = ""

      @chipTable.each do |chipType, chipCollection|
        chipCollection.each do |chip|
          name = useShort ? chip.shortName : chip.name

          # The SN76489 has to be handled in a different way because of its
          # special "NeoGeo Pocket Color" mode.
          if chip.is_a?(Yuno::Chips::SN76489) && chip.ngpMode?
            ngpCount += 1
            ngpName = name
          else
            if ret.has_key?(chipType)
              ret[chipType] << name
            else
              ret[chipType] = [name]
            end
          end
        end
      end

      # Add in any NeoGeo Pocket Color chips.  These are always divided by 2
      # since each chip is technically a core in a T6W28 (an SN76489
      # derivative), and there are two cores per T6W28.
      if ngpCount > 0
        if ret.has_key?(ChipType::Sn76489)
          ngpCount.tdiv(2).times { |_| ret[ChipType::Sn76489] << ngpName }
        else
          ret[ChipType::Sn76489] = Array(String).new(ngpCount.tdiv(2)) { |_| ngpName }
        end
      end

      ret
    end

    # Returns the list of all the fancy names for the chips that are used in
    # this VGM.  This only returns a valid set after the `#play` method has been
    # run.
    #
    # If `useShort` is `true`, then the short names are returned for each chip
    # instead of the full name.
    def chipNamesUsed(useShort : Bool = false) : Array(String)
      setupChipSet
      names = collectChipNamesByType(useShort)
      ret = [] of String
      uniqueNames = {} of String => Int32

      # Collect the count of all of the unique names.
      names.each_value do |chipNames|
        chipNames.each do |chipName|
          if uniqueNames.has_key?(chipName)
            uniqueNames[chipName] += 1
          else
            uniqueNames[chipName] = 1
          end
        end
      end

      # Now build the return value
      uniqueNames.each do |name, count|
        if count == 1
          ret << name
        else
          ret << sprintf("%ix %s", count, name)
        end
      end

      ret
    end

    # Returns the `Hash` where the keys are `ChipType`s for the chips that are
    # used in this VGM, and the values are the number of instances of that chip.
    def chipsUsed : Hash(ChipType, Int32)
      setupChipSet
      ret = {} of ChipType => Int32

      @chipTable.each do |ct, chip|
        if !ret.has_key?(ct) && !chip.empty?
          ret[ct] = chip.size
        else
          ret[ct] += chip.size
        end
      end

      ret
    end

    ############################################################################
    ###
    ### Rendering Methods
    ###
    ### For each supported destination format, there are two (or three) options:
    ###
    ###  * Render to left/right, converting or dithering the audio as needed
    ###  * Render to left/right, converting the audio using a fast but inferior
    ###    method compared to dithering (not for all formats)
    ###  * Render interleaved audio, converting or dithering the audio as needed
    ###  * Render interleaved audio, converting the audio using a fast but
    ###    inferior method compared to dithering (not for all formats)
    ###
    ### Internally we render to signed 32-bit integer PCM, so the int32 methods
    ### are technically "fastest".
    ###

    # Renders new PCM data to the left and right buffers, and advances the
    # playback state.  The audio is rendered in signed 32-bit PCM format (the
    # native format internally).
    #
    # You must call `play` once before calling this.
    #
    # The size of `left` and `right` MUST be even and MUST be equal to the
    # `#bufferSize` provided when you created this instance.  If your program is
    # compiled without the `-Dyunosynth_wd40`, this will be checked, otherwise
    # this check is left out at compile time.
    def render(left : Array(Int32)|Slice(Int32), right : Array(Int32)|Slice(Int32)) : Nil
      {% unless flag?(:yunosynth_wd40) %}
        unless left.size == @bufferSize
          raise YunoError.new("Buffer size mismatch: #{left.size} != #{@bufferSize}")
        end
      {% end %}
      fillBuffers(left, right)
    end

    # Renders new PCM data to the left and right buffers, and advances the
    # playback state.  The audio is rendered in signed 32-bit PCM format (the
    # native format internally), then automatically dithered to signed 16-bit
    # PCM using high quality dithering.
    #
    # You must call `play` once before calling this.
    #
    # The size of `left` and `right` MUST be even and MUST be equal to the
    # `#bufferSize` provided when you created this instance.  If your program is
    # compiled without the `-Dyunosynth_wd40`, this will be checked, otherwise
    # this check is left out at compile time.
    def render(left : Array(Int16)|Slice(Int16), right : Array(Int16)|Slice(Int16)) : Nil
      {% unless flag?(:yunosynth_wd40) %}
        unless left.size == @leftBuf32.size && right.size == @rightBuf32.size
          raise YunoError.new("Buffer size mismatch")
        end
      {% end %}
      fillBuffers(@leftBuf32, @rightBuf32)

      @leftBuf32.size.times do |idx|
        left.put!(idx, @dither.ditherI32toI16(@leftBuf32.get!(idx)))
        right.put!(idx, @dither.ditherI32toI16(@rightBuf32.get!(idx)))
      end
    end

    # Renders new PCM data to the left and right buffers, and advances the
    # playback state.  The audio is rendered in signed 32-bit PCM format (the
    # native format internally), then automatically converted to signed 16-bit
    # PCM using a slightly lower quality (but slightly faster) sample
    # conversion.
    #
    # You must call `play` once before calling this.
    #
    # The size of `left` and `right` MUST be even and MUST be equal to the
    # `#bufferSize` provided when you created this instance.  If your program is
    # compiled without the `-Dyunosynth_wd40`, this will be checked, otherwise
    # this check is left out at compile time.
    def renderLQ(left : Array(Int16)|Slice(Int16), right : Array(Int16)|Slice(Int16)) : Nil
      {% unless flag?(:yunosynth_wd40) %}
        unless left.size == @leftBuf32.size && right.size == @rightBuf32.size
          raise YunoError.new("Buffer size mismatch")
        end
      {% end %}
      fillBuffers(@leftBuf32, @rightBuf32)

      @leftBuf32.size.times do |idx|
        left.put!(idx, (@leftBuf32.get!(idx) >> 16).to_i16!)
        right.put!(idx,(@rightBuf32.get!(idx) >> 16).to_i16!)
      end
    end

    # Renders new PCM data to the left and right buffers, and advances the
    # playback state.  The audio is rendered in signed 32-bit PCM format (the
    # native format internally), then automatically converted to 64-bit IEEE
    # floating point audio.
    #
    # You must call `play` once before calling this.
    #
    # The size of `left` and `right` MUST be even and MUST be equal to the
    # `#bufferSize` provided when you created this instance.  If your program is
    # compiled without the `-Dyunosynth_wd40`, this will be checked, otherwise
    # this check is left out at compile time.
    def render(left : Array(Float64)|Slice(Float64), right : Array(Float64)|Slice(Float64)) : Nil
      {% unless flag?(:yunosynth_wd40) %}
        unless left.size == @leftBuf32.size && right.size == @rightBuf32.size
          raise YunoError.new("Buffer size mismatch")
        end
      {% end %}
      fillBuffers(@leftBuf32, @rightBuf32)

      @leftBuf32.size.times do |idx|
        left.put!(idx, @leftBuf32.get!(idx) * Ditherer::INV_INT32_MAX)
        right.put!(idx, @rightBuf32.get!(idx) * Ditherer::INV_INT32_MAX)
      end
    end

    # Renders new PCM data to the left and right buffers, and advances the
    # playback state.  The audio is rendered in signed 32-bit PCM format (the
    # native format internally), then automatically converted to 32-bit IEEE
    # floating point audio.
    #
    # You must call `play` once before calling this.
    #
    # The size of `left` and `right` MUST be even and MUST be equal to the
    # `#bufferSize` provided when you created this instance.  If your program is
    # compiled without the `-Dyunosynth_wd40`, this will be checked, otherwise
    # this check is left out at compile time.
    def render(left : Array(Float32)|Slice(Float32), right : Array(Float32)|Slice(Float32)) : Nil
      {% unless flag?(:yunosynth_wd40) %}
        unless left.size == @leftBuf32.size && right.size == @rightBuf32.size
          raise YunoError.new("Buffer size mismatch")
        end
      {% end %}
      fillBuffers(@leftBuf32, @rightBuf32)

      @leftBuf32.size.times do |idx|
        left.put!(idx, (@leftBuf32.get!(idx) * Ditherer::INV_INT32_MAX).to_f32!)
        right.put!(idx, (@rightBuf32.get!(idx) * Ditherer::INV_INT32_MAX).to_f32!)
      end
    end

    # Renders new interleaved PCM data to `dest` as interleaved audio, and
    # advances the playback state.  The audio is rendered in signed 32-bit
    # integer format (the native format internally).
    #
    # You must call `play` once before calling this.
    #
    # The size of `` MUST be exactly twice the `#bufferSize` that was requested
    # when you created this instance.  If your program is compiled without the
    # `-Dyunosynth_wd40`, this will be checked, otherwise this check is left out
    # at compile time.
    def render(dest : Array(Int32)|Slice(Int32)) : Nil
      {% unless flag?(:yunosynth_wd40) %}
        unless (dest.size >> 1) == @bufferSize
          raise YunoError.new("Buffer size mismatch: #{dest.size} / 2 != #{@bufferSize}")
        end
      {% end %}
      fillBuffers(@leftBuf32, @rightBuf32)
      didx : Int32 = 0
      @bufferSize.times do |sidx|
        dest.put!(didx, @leftBuf32.get!(sidx))
        dest.put!(didx + 1, @rightBuf32.get!(sidx))
        didx += 2
      end
    end

    # Renders new PCM interleaved data to `dest` as interleaved audio, and
    # advances the playback state.  The audio is rendered in signed 32-bit
    # integer format (the native format internally), then automatically dithered
    # to signed 16-bit PCM using high quality dithering.
    #
    # You must call `play` once before calling this.
    #
    # The size of `` MUST be exactly twice the `#bufferSize` that was requested
    # when you created this instance.  If your program is compiled without the
    # `-Dyunosynth_wd40`, this will be checked, otherwise this check is left out
    # at compile time.
    def render(dest : Array(Int16)|Slice(Int16)) : Nil
      {% unless flag?(:yunosynth_wd40) %}
        unless (dest.size >> 1) == @bufferSize
          raise YunoError.new("Buffer size mismatch: #{dest.size} / 2 != #{@bufferSize}")
        end
      {% end %}
      fillBuffers(@leftBuf32, @rightBuf32)
      didx : Int32 = 0
      @bufferSize.times do |sidx|
        dest.put!(didx, @dither.ditherI32toI16(@leftBuf32.get!(sidx)))
        dest.put!(didx + 1, @dither.ditherI32toI16(@rightBuf32.get!(sidx)))
        didx += 2
      end
    end

    # Renders new PCM interleaved data to `dest` as interleaved audio, and
    # advances the playback state.  The audio is rendered in signed 32-bit
    # integer format (the native format internally), then automatically
    # converted to signed 16-bit PCM using a slightly lower quality (but
    # slightly faster) sample conversion.
    #
    # You must call `play` once before calling this.
    #
    # The size of `` MUST be exactly twice the `#bufferSize` that was requested
    # when you created this instance.  If your program is compiled without the
    # `-Dyunosynth_wd40`, this will be checked, otherwise this check is left out
    # at compile time.
    def renderLQ(dest : Array(Int16)|Slice(Int16)) : Nil
      {% unless flag?(:yunosynth_wd40) %}
        unless (dest.size >> 1) == @bufferSize
          raise YunoError.new("Buffer size mismatch: #{dest.size} / 2 != #{@bufferSize}")
        end
      {% end %}
      fillBuffers(@leftBuf32, @rightBuf32)
      didx : Int32 = 0
      @bufferSize.times do |sidx|
        dest.put!(didx, (@leftBuf32.get!(sidx) >> 16).to_i16!)
        dest.put!(didx + 1, (@rightBuf32.get!(sidx) >> 16).to_i16!)
        didx += 2
      end
    end

    # Renders new interleaved PCM data to `dest` as interleaved audio, and
    # advances the playback state.  The audio is rendered in signed 32-bit PCM
    # format (the native format internally), then automatically converted to
    # 64-bit IEEE floating point audio.
    #
    # You must call `play` once before calling this.
    #
    # The size of `` MUST be exactly twice the `#bufferSize` that was requested
    # when you created this instance.  If your program is compiled without the
    # `-Dyunosynth_wd40`, this will be checked, otherwise this check is left out
    # at compile time.
    def render(dest : Array(Float64)|Slice(Float64)) : Nil
      {% unless flag?(:yunosynth_wd40) %}
        unless (dest.size >> 1) == @leftBuf32.size
          raise YunoError.new("Buffer size mismatch: #{dest.size >> 1} != #{@leftBuf32.size}")
        end
      {% end %}
      fillBuffers(@leftBuf32, @rightBuf32)
      didx : Int32 = 0
      @leftBuf32.size.times do |sidx|
        dest.put!(didx, @leftBuf32.get!(sidx) * Ditherer::INV_INT32_MAX)
        dest.put!(didx + 1, @rightBuf32.get!(sidx) * Ditherer::INV_INT32_MAX)
        didx += 2
      end
    end

    # Renders new PCM interleaved data to `dest` as interleaved audio, and
    # advances the playback state.  The audio is rendered in signed 32-bit PCM
    # format (the native format internally), then automatically converted to
    # 32-bit IEEE floating point audio.
    #
    # You must call `play` once before calling this.
    #
    # The size of `` MUST be exactly twice the `#bufferSize` that was requested
    # when you created this instance.  If your program is compiled without the
    # `-Dyunosynth_wd40`, this will be checked, otherwise this check is left out
    # at compile time.
    def render(dest : Array(Float32)|Slice(Float32)) : Nil
      {% unless flag?(:yunosynth_wd40) %}
        unless (dest.size >> 1) == @leftBuf32.size
          raise YunoError.new("Buffer size mismatch: #{dest.size >> 1} != #{@leftBuf32.size}")
        end
      {% end %}
      fillBuffers(@leftBuf32, @rightBuf32)
      didx : Int32 = 0

      @leftBuf32.size.times do |sidx|
        dest.put!(didx, (@leftBuf32.get!(sidx) * Ditherer::INV_INT32_MAX).to_f32!)
        dest.put!(didx + 1, (@rightBuf32.get!(sidx) * Ditherer::INV_INT32_MAX).to_f32!)
        didx += 2
      end
    end
  end
end
