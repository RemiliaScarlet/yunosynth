#### YunoSynth
#### Copyright (C) 2023-2024 Remilia Scarlet <remilia@posteo.jp>
####
#### This program is free software: you can redistribute it and/or
#### modify it under the terms of the GNU Affero General Public
#### License as published by the Free Software Foundation, either
#### version 3 of the License, or (at your option) any later version.
####
#### This program is distributed in the hope that it will be useful,
#### but WITHOUT ANY WARRANTY; without even the implied warranty of
#### MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#### Affero General Public License for more details.
####
#### You should have received a copy of the GNU Affero General Public License
#### along with this program.  If not, see <https://www.gnu.org/licenses/>.
require "./common"

####
#### GD3 Tag Support
####

module Yuno
  # A GD3 tag stores metadata for a VGM file.  It is analogous to an ID3 tag in
  # an MP3 file.
  class Gd3Tag
    private GD3_MAGIC = "Gd3 "

    # The version of the GD3 tag specification that's supported.
    GD3_VERSION = 256_u32 # [0, 1, 0, 0] in bytes

    # Represents an error that occurred while reading a GD3 tag.
    class Error < YunoError
    end

    # The version of the GD3 tag data.
    getter version : UInt32 = GD3_VERSION

    # The name of the song, in English.
    getter trackNameEn : String = ""

    # The name of the song, in Japanese.
    getter trackNameJp : String = ""

    # The name of the game this song is from, in English.
    getter gameNameEn : String = ""

    # The name of the game this song is from, in Japanese.
    getter gameNameJp : String = ""

    # The name of the system/arcade board that this game appeared on, in English.
    getter systemNameEn : String = ""

    # The name of the system/arcade board that this game appeared on, in Japanese.
    getter systemNameJp : String = ""

    # The name(s) of the composer(s) of this song, in English.
    getter authorNameEn : String = ""

    # The name(s) of the composer(s) of this song, in Japanese.
    getter authorNameJp : String = ""

    # The release date of the game this song is from.
    getter releaseDate : String = ""

    # The name of the person or group who created this VGM file.
    getter creator : String = ""

    # Any additional notes about the song or VGM file.
    getter notes : String = ""

    # Creates a new `GD3Tag` instance.
    def initialize
    end

    # Creates a new `GD3Tag` instance by reading data from *io*, which
    # should already be at the position of the raw GD3 data.
    def initialize(io : IO)
      # Check for the GD3 magic bytes to ensure this is GD3 data we're about to read.
      unless io.read_string(GD3_MAGIC.size) == GD3_MAGIC
        raise Yuno::Gd3Tag::Error.new("No valid GD3 tag header found")
      end

      # Check that this GD3 data is of a supported version.
      @version = io.readUInt32
      unless @version == GD3_VERSION
        Yuno.warn("GD3 tag version doesn't match what is expected")
      end

      # Get the expected length of GD3 data.
      len = io.readUInt32
      startedAt = io.pos # This is here because `len` is relative

      # Read the fields.
      @trackNameEn = readWcharString(io)
      @trackNameJp = readWcharString(io)
      @gameNameEn = readWcharString(io)
      @gameNameJp = readWcharString(io)
      @systemNameEn = readWcharString(io)
      @systemNameJp = readWcharString(io)
      @authorNameEn = readWcharString(io)
      @authorNameJp = readWcharString(io)
      @releaseDate = readWcharString(io)
      @creator = readWcharString(io)
      @notes = readWcharString(io)

      # Check that the amount of data we've read matches what is
      # expected.
      unless io.pos == startedAt + len
        Yuno.dlog!("NOTE: GD3 tag data size mismatch: #{len} != #{io.pos - startedAt}")
        io.pos += len - (io.pos - startedAt)
      end
    end

    # Reads a C++-like `wchar` from *io*.
    private def readWcharString(io : IO) : String
      # Ugh...

      buf = Slice(UInt16).new(2)
      buf[1] = 0
      ch : UInt16 = 0
      String.build do |str|
        loop do
          buf[0] = io.read_bytes(UInt16, IO::ByteFormat::LittleEndian)
          break unless buf[0] != 0
          str << String.from_utf16(buf.to_unsafe)[0]
        end
      end
    end
  end
end
