#### YunoSynth
#### Copyright (C) 2023-2024 Remilia Scarlet <remilia@posteo.jp>
####
#### This program is free software: you can redistribute it and/or
#### modify it under the terms of the GNU Affero General Public
#### License as published by the Free Software Foundation, either
#### version 3 of the License, or (at your option) any later version.
####
#### This program is distributed in the hope that it will be useful,
#### but WITHOUT ANY WARRANTY; without even the implied warranty of
#### MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#### Affero General Public License for more details.
####
#### You should have received a copy of the GNU Affero General Public License
#### along with this program.  If not, see <https://www.gnu.org/licenses/>.

####
#### Settings for VgmPlayer instances
####

module Yuno
  # Settings for a `VgmPlayer` instance.
  class VgmPlayerSettings
    # The default target sample rate for playback.
    DEFAULT_SAMPLE_RATE = 44100_u32

    # The target sample rate to use for playback.
    property sampleRate : UInt32 = DEFAULT_SAMPLE_RATE

    # When `true`, then extra messages may be printed to the console.
    property? verboseMessages : Bool = false

    # When `true`, then `AbstractChip#reset` will be called on all chips when
    # stopping playback.
    property? hardStop : Bool = false

    ###
    ### Emulator Settings
    ###

    # When `true`, then the "wave" channel on the DMG is boosted.
    property? dmgBoostWaveChan : Bool = true

    # The emulation core to use for the YM2151.
    property ym2151Core : Yuno::Chips::YM2151::Core = Yuno::Chips::YM2151::Core::Mame

    # The emulation core to use for the YM2151.
    property huc6280Core : Yuno::Chips::HuC6280::Core = Yuno::Chips::HuC6280::Core::Ootake

    # When using the MAME core for the YM2612, this instructs the the chip
    # emulator to update the left/right channels alternatively, creating a nice
    # pseudo-stereo effect
    property? ym2612PseudoStereo : Bool = false

    # Creates a new `VgmPlayerSettings` instance.
    def initialize
    end
  end
end
