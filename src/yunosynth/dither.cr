#### YunoSynth
#### Copyright (C) 2023-2024 Remilia Scarlet <remilia@posteo.jp>
####
#### This program is free software: you can redistribute it and/or
#### modify it under the terms of the GNU Affero General Public
#### License as published by the Free Software Foundation, either
#### version 3 of the License, or (at your option) any later version.
####
#### This program is distributed in the hope that it will be useful,
#### but WITHOUT ANY WARRANTY; without even the implied warranty of
#### MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#### Affero General Public License for more details.
####
#### You should have received a copy of the GNU Affero General Public License
#### along with this program.  If not, see <https://www.gnu.org/licenses/>.
require "./common"

module Yuno
  # High-quality dithering.
  class Ditherer
    @rnd : Random = Random.new(Time.local.to_unix_ms)
    @rand1 : Int32 = 0
    @rand2 : Int32 = 0
    @feedback1 : Float64 = 0.0
    @feedback2 : Float64 = 0.0

    # :nodoc:
    MAX_RAND = 2147483647_i32

    # :nodoc:
    INV_INT32_MAX = 1.0 / Int32::MAX

    def initialize
    end

    @[AlwaysInline]
    def ditherI32toI16(sample : Int32) : Int16
      sampleF64 : Float64 = sample * INV_INT32_MAX
      w : Float64 = (1i64 << 15).to_f64!
      wi : Float64 = 1.0 / w
      d : Float64 = wi / MAX_RAND
      o : Float64 = wi * 0.5

      @rand2 = @rand1
      @rand1 = @rnd.rand(MAX_RAND)
      in2 : Float64 = sampleF64 + (0.5 * ((@feedback1 + @feedback2) - @feedback2))
      tmp : Float64 = in2 + o + (d * (@rand1 &- @rand2))
      ret : Int16 = (w * tmp).to_i16!
      ret = ret &- 1 if tmp < 0.0
      @feedback2 = @feedback1
      @feedback1 = in2 - (wi * ret.to_i64!)
      ret
    end
  end
end
