#### YunoSynth
#### Copyright (C) 2023-2024 Remilia Scarlet <remilia@posteo.jp>
#### Based on VGMPlay, Copyright (C) Valley Bell
####
#### This program is free software: you can redistribute it and/or
#### modify it under the terms of the GNU Affero General Public
#### License as published by the Free Software Foundation, either
#### version 3 of the License, or (at your option) any later version.
####
#### This program is distributed in the hope that it will be useful,
#### but WITHOUT ANY WARRANTY; without even the implied warranty of
#### MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#### Affero General Public License for more details.
####
#### You should have received a copy of the GNU Affero General Public License
#### along with this program.  If not, see <https://www.gnu.org/licenses/>.

####
#### Nearly all of this code was taken from VGMPlay, but turned into a class.
####

module Yuno
  # Fixed-point resampler.  This implements four modes: low-quality (but fast)
  # resampling, upsampling, downsampling, and copying.
  #
  # This is where YunoSynth actually calls the update functions for the chips.
  private class Resampler
    alias SLInt = UInt32

    # The mode of the resampler.
    enum ResamplerType
      # High quality upsampling.
      Upsampling

      # No resampling, just copy to the output.
      Copy

      # High quality downsampling.
      Downsampling
    end

    # :nodoc:
    FIXPNT_BITS = 11u32 # Could be changed.  11 bits seems good and accurate,
                        # though.  Change the SLInt alias to UInt64 if this goes
                        # above 11, though.  Also change the .to_u32! code in the
                        # resamplers as needed.

    # :nodoc:
    FIXPNT_FACT = (1u32 << FIXPNT_BITS)

    # :nodoc:
    FIXPNT_MASK = FIXPNT_FACT - 1

    # Playback sample rate
    @sampleRate : UInt32 = 0

    # PCM data from the chips gets put into this buffer when we update the
    # chips.
    @streamBuffers : OutputBuffers

    # Creates a new `Resampler` instance.  Resampled data will be read from
    # `buffers` whenever `#resample` is called.
    def initialize(@sampleRate : UInt32, buffers : OutputBuffers?)
      @streamBuffers = if buffers
                         buffers
                       else
                         [] of Slice(Int32)
                       end
    end

    # Resamples audio data to `ret`.  In all cases, `len` should be 1.
    def resample(chipTable : ChipTable, ret : Sample(Int32), len : UInt32) : Nil
      {% if flag?(:yunosynth_debug) %}
        raise "len != 1" unless len == 1
      {% end %}

      paired : AbstractChip? = nil

      # Update and resample all of the chips.
      Yuno.eachChip(chipTable) do |chip, _|
        case chip.resamplerType
        in .upsampling? then resample_upsample(chip, ret, len)
        in .copy? then resample_copying(chip, ret, len)
        in .downsampling? then resample_downsample(chip, ret, len)
        end

        if chip.lastSampleNum >= chip.sampleRate
          chip.lastSampleNum -= chip.sampleRate
          chip.nextSampleNum -= chip.sampleRate
          chip.curSampleNum = chip.curSampleNum &- @sampleRate
        end

        # TODO ensure this doesn't mean a chip is updated twice.
        if chip.isPaired? && (paired = chip.paired)
          case paired.resamplerType
          in .upsampling? then resample_upsample(paired, ret, len)
          in .copy? then resample_copying(paired, ret, len)
          in .downsampling? then resample_downsample(paired, ret, len)
          end

          if paired.lastSampleNum >= paired.sampleRate
            paired.lastSampleNum -= paired.sampleRate
            paired.nextSampleNum -= paired.sampleRate
            paired.curSampleNum = paired.curSampleNum &- @sampleRate
          end
        end
      end
    end

    ###
    ### Fixed point stuff
    ###

    @[AlwaysInline]
    private def getFraction(x)
      x & FIXPNT_MASK
    end

    @[AlwaysInline]
    private def getNFraction(x)
      (FIXPNT_FACT - x) & FIXPNT_MASK
    end

    @[AlwaysInline]
    private def fp2iFloor(x)
      x.tdiv(FIXPNT_FACT)
    end

    @[AlwaysInline]
    private def fp2iCeil(x)
      (x + FIXPNT_MASK).tdiv(FIXPNT_FACT)
    end

    ###
    ### Resampling modes
    ###

    # High quality upsampling.
    private def resample_upsample(chip : AbstractChip, ret : Sample(Int32), len : UInt32) : Nil
      curBufL : Slice(Int32) = @streamBuffers[0]
      curBufR : Slice(Int32) = @streamBuffers[1]
      chipSampleRate : UInt64 = chip.sampleRate.to_u64!
      inPosL : SLInt = (FIXPNT_FACT.to_u64! * chip.curSampleNum.to_u64! * chipSampleRate).tdiv(@sampleRate).to_u32!
      inPre : UInt32 = fp2iFloor(inPosL)
      inNow : UInt32 = fp2iCeil(inPosL)
      inPos : UInt32 = 0
      sampleFraction : UInt32 = 0
      tempSampleL : Int64 = 0
      tempSampleR : Int64 = 0

      curBufL[0] = chip.lastSample.left
      curBufR[0] = chip.lastSample.right
      curBufL[1] = chip.nextSample.left
      curBufR[1] = chip.nextSample.right

      streamPoint : OutputBuffers = [curBufL[2..], curBufR[2..]]
      if fn = chip.updateFn
        fn.call(streamPoint, inNow - chip.nextSampleNum)
      else
        chip.update(streamPoint, inNow - chip.nextSampleNum)
      end

      inBase : UInt32 = (FIXPNT_FACT.to_i64! + (inPosL.to_i64! - chip.nextSampleNum.to_i64! * FIXPNT_FACT.to_i64!)).to_u32!
      sampleCount : UInt32 = FIXPNT_FACT.to_u32!
      chip.lastSampleNum = inPre
      chip.nextSampleNum = inNow

      # The original code expects us to have an array of return samples.
      # However, the resampler is only ever called with a single element.  In
      # the interest of efficiency, the `len` parameter is ignored here, with
      # the original code is commented out.

      #len.times do |outPos|
        #inPos = inBase + (FIXPNT_FACT * outPos.to_u32! * chip.sampleRate // @sampleRate)
        inPos = inBase
        inPre = fp2iFloor(inPos)
        inNow = fp2iCeil(inPos)
        sampleFraction = getFraction(inPos).to_u32!

        # Linear interpolation
        tempSampleL = (curBufL[inPre].to_i64! * (FIXPNT_FACT.to_i64! - sampleFraction.to_i64!)) +
                      (curBufL[inNow].to_i64! * sampleFraction.to_i64!)
        tempSampleR = (curBufR[inPre].to_i64! * (FIXPNT_FACT.to_i64! - sampleFraction.to_i64!)) +
                      (curBufR[inNow].to_i64! * sampleFraction.to_i64!)
        #ret[outPos].left  += (tempSampleL * chip.volume // sampleCount).to_i32!
        #ret[outPos].right += (tempSampleR * chip.volume // sampleCount).to_i32!
        ret.left  += (tempSampleL * chip.volume).tdiv(sampleCount).to_i32!
        ret.right += (tempSampleR * chip.volume).tdiv(sampleCount).to_i32!
      #end

      chip.lastSample.left = curBufL.unsafe_fetch(inPre)
      chip.lastSample.right = curBufR.unsafe_fetch(inPre)
      chip.nextSample.left = curBufL.unsafe_fetch(inNow)
      chip.nextSample.right = curBufR.unsafe_fetch(inNow)
      #chip.curSampleNum += len
      chip.curSampleNum += 1
    end

    # Not actually resampling, just copies data from the chip's output buffer to
    # our destination buffer and applies volume.  This is used for situations
    # where the ratio of chip's output sample rate and the playback rate matches
    # 1:1.
    @[AlwaysInline]
    private def resample_copying(chip : AbstractChip, ret : Sample(Int32), len : UInt32) : Nil
      curBufL : Slice(Int32) = @streamBuffers[0]
      curBufR : Slice(Int32) = @streamBuffers[1]
      chip.nextSampleNum = (chip.curSampleNum.to_i64! * chip.sampleRate.to_i64!).tdiv(@sampleRate.to_i64!).to_u32!
      if fn = chip.updateFn
        fn.call(@streamBuffers, len)
      else
        chip.update(@streamBuffers, len)
      end

      # The original code expects us to have an array of return samples.
      # However, the resampler is only ever called with a single element.  In
      # the interest of efficiency, the `len` parameter is ignored here, with
      # the original code is commented out.

      #len.times do |outPos|
        #ret[outPos].left  += curBufL[outPos] * chip.volume
        #ret[outPos].right += curBufR[outPos] * chip.volume
        ret.left  += curBufL[0] * chip.volume
        ret.right += curBufR[0] * chip.volume
      # end

      chip.curSampleNum += len
      chip.lastSampleNum = chip.nextSampleNum
    end

    # High quality downsampling.
    private def resample_downsample(chip : AbstractChip, ret : Sample(Int32), len : UInt32) : Nil
      curBufL : Slice(Int32) = @streamBuffers.unsafe_fetch(0)
      curBufR : Slice(Int32) = @streamBuffers.unsafe_fetch(1)
      chipSampleRate : UInt64 = chip.sampleRate.to_u64!
      inPosL : SLInt = (FIXPNT_FACT.to_u64! * (chip.curSampleNum.to_u64! + len.to_u64!) * chipSampleRate).tdiv(@sampleRate).to_u32!
      sampleFraction : UInt32 = 0
      tempSampleL : Int64 = 0
      tempSampleR : Int64 = 0
      sampleCount : UInt32 = 0
      inPre : UInt32 = 0
      inNow : UInt32 = 0

      chip.nextSampleNum = fp2iCeil(inPosL).to_u32!
      curBufL[0] = chip.lastSample.left
      curBufR[0] = chip.lastSample.right
      streamPoint : OutputBuffers = [curBufL[1..], curBufR[1..]]
      if fn = chip.updateFn
        fn.call(streamPoint, chip.nextSampleNum - chip.lastSampleNum)
      else
        chip.update(streamPoint, chip.nextSampleNum - chip.lastSampleNum)
      end
      inPosL = (FIXPNT_FACT.to_u64! * chip.curSampleNum * chipSampleRate).tdiv(@sampleRate).to_u32!

      # Add 1.0 to avoid negative indicies
      inBase : UInt32 = (FIXPNT_FACT.to_u64! + (inPosL.to_i64! - chip.lastSampleNum.to_i64! * FIXPNT_FACT.to_i64!)).to_u32!
      inPosNext : UInt32 = inBase

      # The original code expects us to have an array of return samples.
      # However, the resampler is only ever called with a single element.  In
      # the interest of efficiency, the `len` parameter is ignored here, with
      # the original code is commented out.

      #len.times do |outPos|
        inPos = inPosNext
        #inPosNext = inBase + (FIXPNT_FACT * (outPos + 1) * chip.sampleRate // @sampleRate).to_u32
        inPosNext = inBase + (FIXPNT_FACT.to_i64! * chip.sampleRate.to_i64!).tdiv(@sampleRate.to_i64!).to_u32!

        # First fractional sample
        sampleFraction = getNFraction(inPos).to_u32!
        if sampleFraction != 0
          inPre = fp2iFloor(inPos)
          tempSampleL = curBufL[inPre].to_i64! * sampleFraction
          tempSampleR = curBufR[inPre].to_i64! * sampleFraction
        else
          tempSampleL = 0
          tempSampleR = 0
        end
        sampleCount = sampleFraction

        # Last fractional sample
        sampleFraction = getFraction(inPosNext)
        inPre = fp2iFloor(inPosNext)
        if sampleFraction != 0
          tempSampleL += curBufL[inPre].to_i64! * sampleFraction
          tempSampleR += curBufR[inPre].to_i64! * sampleFraction
          sampleCount += sampleFraction
        end

        # Whole samples in-between
        inNow = fp2iCeil(inPos)
        sampleCount += (inPre - inNow) * FIXPNT_FACT
        while inNow < inPre
          tempSampleL += curBufL[inNow].to_i64! * FIXPNT_FACT
          tempSampleR += curBufR[inNow].to_i64! * FIXPNT_FACT
          inNow += 1
        end

        #ret[outPos].left  += (tempSampleL * chip.volume // sampleCount).to_i32!
        #ret[outPos].right += (tempSampleR * chip.volume // sampleCount).to_i32!
        ret.left  += (tempSampleL * chip.volume).tdiv(sampleCount).to_i32!
        ret.right += (tempSampleR * chip.volume).tdiv(sampleCount).to_i32!
      # end

      chip.lastSample.left = curBufL.unsafe_fetch(inPre)
      chip.lastSample.right = curBufR.unsafe_fetch(inPre)
      #chip.curSampleNum += len
      chip.curSampleNum += 1
      chip.lastSampleNum = chip.nextSampleNum
    end
  end
end
