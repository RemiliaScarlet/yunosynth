#### YunoSynth
#### Copyright (C) 2023-2024 Remilia Scarlet <remilia@posteo.jp>
####
#### This program is free software: you can redistribute it and/or
#### modify it under the terms of the GNU Affero General Public
#### License as published by the Free Software Foundation, either
#### version 3 of the License, or (at your option) any later version.
####
#### This program is distributed in the hope that it will be useful,
#### but WITHOUT ANY WARRANTY; without even the implied warranty of
#### MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#### Affero General Public License for more details.
####
#### You should have received a copy of the GNU Affero General Public License
#### along with this program.  If not, see <https://www.gnu.org/licenses/>.
require "compress/gzip"
require "zstd"
require "libremiliacr"

{% begin %}
  {% if compare_versions(Zstd::VERSION, "1.2.0") <= 0 %}
    {% puts "Monkey patching the ZStandard bindings to fix a memory leak" %}
    class Zstd::Compress::IO
      def close : Nil
        return if @closed
        @closed = true
        write_loop Lib::ZstdEndDirective::ZstdEEnd
        @io.close if @sync_close
      ensure
        @ctx.close
      end
    end

    class Zstd::Decompress::IO
      def close : Nil
        return if @closed
        @closed = true
        @io.close if @sync_close
      ensure
        @ctx.close
      end
    end
  {% end %}
{% end %}

module Yuno
  # Detects and decompresses VGM streams.  This is designed so that external
  # libraries can extend the functionality of this class without needing to
  # reopen it.
  class VgmDecompressor
    # A function used to check if an `IO` is compressed.
    alias CheckerFn = Proc(IO, Bool)

    # A function used to decompress `IO` to a new `IO::Memory`.
    alias DecompFn = Proc(IO, IO::Memory)

    # A method for checking for compressed data, and decompressing that data.
    alias FuncSet = NamedTuple(ext: Array(String), checker: CheckerFn, decomp: DecompFn)

    # All known decompression methods.  The Symbol key is mostly just
    # so we have a key of some sort, it isn't used for anything else.
    @@funcs : Hash(Symbol, FuncSet) = {} of Symbol => FuncSet

    # The set of all `FuncSet`s that can be used to check for compressed data,
    # and decompress it.
    class_getter funcs

    ###
    ### GZip Compression
    ###

    # Returns `true` if `io` contains GZipped data, or `false` otherwise.  This
    # requires `io` to support `IO#pos=`.
    def self.isGz?(io : IO) : Bool
      buf = Bytes.new(3)
      io.withExcursion { io.read(buf) }
      (buf[0] == 0x1F && buf[1] == 0x8B && buf[2] == 0x08)
    end

    # Decompresses `io` into an `IO::Memory`, then returns the new IO.
    def self.ungz(io : IO) : IO::Memory
      ret = IO::Memory.new
      Compress::Gzip::Reader.open(io) { |gzio| IO.copy(gzio, ret) }
      ret.rewind
      ret
    end

    ###
    ### BZip2 Compression
    ###

    # Returns `true` if `io` contains BZip2 data, or `false` otherwise.  This
    # requires `io` to support `IO#pos=`.
    def self.isBz2?(io : IO) : Bool
      buf = Bytes.new(4)
      io.withExcursion { io.read(buf) }
      buf[0] == 'B'.ord &&
        buf[1] == 'Z'.ord &&
        buf[2] == 'h'.ord &&
        (buf[3] >= '1'.ord && buf[3] <= '9'.ord)
    end

    # Decompresses `io` into an `IO::Memory`, then returns the new IO.
    def self.unbzip2(io : IO) : IO::Memory
      ret = IO::Memory.new
      RemiLib::Compression::BZip2::Reader.open(io) { |bz| IO.copy(bz, ret) }
      ret.rewind
      ret
    end

    ###
    ### ZStandard Compression
    ###

    # Returns `true` if `io` contains ZStandard data, or `false` otherwise.  This
    # requires `io` to support `IO#pos=`.
    def self.isZstd?(io : IO) : Bool
      buf = Bytes.new(4)
      io.withExcursion { io.read(buf) }
      (buf[0] == 0x28 && buf[1] == 0xB5 && buf[2] == 0x2f && buf[3] == 0xFD)
    end

    # Decompresses `io` into an `IO::Memory`, then returns the new IO.
    def self.unzstd(io : IO) : IO::Memory
      ret = IO::Memory.new
      Zstd::Decompress::IO.open(io) { |zstd| IO.copy(zstd, ret) }
      ret.rewind
      ret
    end

    ###
    ### Main Public Methods
    ###

    # Registers a new method for checking for compressed data, and decompressing
    # that data.  `ident` can be used to reference the `FuncSet` from `#funcs`
    # after registering.  `extension` is a set of file extensions that can be
    # used by `#getHint` for this compression scheme.  The extension should
    # include the period and should be entirely lowercase, e.g. `".vgz"`.
    #
    # Note that `ident` cannot be `:none`.
    def self.registerFunctions(ident : Symbol, extensions : Array(String), checkFn : CheckerFn, decompFn : DecompFn) : Nil
      raise "Cannot register an ident of :none" if ident == :none

      # Ensure this isn't a duplicate compression method.
      if @@funcs[ident]?
        raise "Duplicate decompression function set found: #{ident}"
      else
        @@funcs[ident] = {ext: extensions, checker: checkFn, decomp: decompFn}
      end
    end

    # Checks to see if `io` needs to be decompressed.  If it does, this
    # decompresses the stream into RAM.  This then returns an IO that can be
    # used to read uncompressed data, which may be the original IO if it was not
    # compressed.  `io` must support `IO#pos`.
    #
    # If `hint` is provided and is a known decompression scheme found in
    # `#funcs`, then that scheme will be tried first to offer a speedup in load
    # times.  You can get a hint by using `#getHint`.
    def self.maybeDecompress(io : IO, hint : Symbol? = nil) : IO
      if hint
        raise "hint cannot be :none" if hint == :none
        if hintedSet = @@funcs[hint]?
          if hintedSet[:checker].call(io)
            # Hint was successful
            io.rewind
            return hintedSet[:decomp].call(io)
          end
        end
      end

      # Hint was not successful.  Check known methods and decompress using them
      # if one is found.
      io.rewind # Ensure we're act the correct position.
      @@funcs.each_value do |fnSet|
        if fnSet[:checker].call(io)
          io.rewind
          return fnSet[:decomp].call(io)
        end
      end

      # Not compressed with a known method.
      io.rewind
      io
    end

    # Looks at the extension of `filename` and attempts to guess the compression
    # method (if any).  This will return a `Symbol` if it was able to guess, or
    # `nil` otherwise.
    #
    # The symbol return will reference one of the known compression methods in
    # `#funcs`.  This symbol can then be passed to `#maybeDecompress`.
    #
    # This returns `nil` if it thinks that `filename` is an uncompressed VGM.
    def self.getHint(filename : String|Path) : Symbol?
      ext = Path[filename].extension.downcase
      @@funcs.keys.find do |key|
        @@funcs[key][:ext].any?(ext)
      end
    end
  end

  # Register the method for VGZ files.
  VgmDecompressor.registerFunctions(:gz, [".vgz", ".gz"], ->VgmDecompressor.isGz?(IO), ->VgmDecompressor.ungz(IO))


  # Register the method for BZip2-compressed VGM files.
  VgmDecompressor.registerFunctions(:bzip2, [".vgb", ".bz2"],
                                    ->VgmDecompressor.isBz2?(IO),
                                    ->VgmDecompressor.unbzip2(IO))

  # Register the method for ZStandard-compressed VGM files.
  Yuno::VgmDecompressor.registerFunctions(:zstd, [".vgzst", ".zst"],
                                          ->VgmDecompressor.isZstd?(IO),
                                          ->VgmDecompressor.unzstd(IO))
end
