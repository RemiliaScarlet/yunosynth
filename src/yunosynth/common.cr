#### YunoSynth
#### Copyright (C) 2023-2024 Remilia Scarlet <remilia@posteo.jp>
####
#### This program is free software: you can redistribute it and/or
#### modify it under the terms of the GNU Affero General Public
#### License as published by the Free Software Foundation, either
#### version 3 of the License, or (at your option) any later version.
####
#### This program is distributed in the hope that it will be useful,
#### but WITHOUT ANY WARRANTY; without even the implied warranty of
#### MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#### Affero General Public License for more details.
####
#### You should have received a copy of the GNU Affero General Public License
#### along with this program.  If not, see <https://www.gnu.org/licenses/>.

####
#### Common Types and Functions
####

class Array(T)
  @[AlwaysInline]
  def get!(idx)
    {% if flag?(:yunosynth_debug) %}
      self.[idx]
    {% else %}
      self.unsafe_fetch(idx)
    {% end %}
  end

  @[AlwaysInline]
  def put!(idx, val)
    {% if flag?(:yunosynth_debug) %}
      self.[idx] = val
    {% else %}
      self.unsafe_put(idx, val)
    {% end %}
  end

  @[AlwaysInline]
  def incf!(idx, delta = 1)
    {% begin %}
      {% unless [UInt8, UInt16, UInt32, UInt64, UInt128,
                 Int8, Int16, Int32, Int64, Int128,
                 Float32, Float64].includes?(T) %}
        raise "Cannot use Array#incf! with an array of type #{T}"
      {% else %}
        {% if flag?(:yunosynth_debug) %}
          self.[idx] += delta
        {% else %}
          self.unsafe_put(idx, self.unsafe_fetch(idx) &+ delta)
        {% end %}
        self
      {% end %}
    {% end %}
  end

  @[AlwaysInline]
  def decf!(idx, delta = 1)
        {% begin %}
      {% unless [UInt8, UInt16, UInt32, UInt64, UInt128,
                 Int8, Int16, Int32, Int64, Int128,
                 Float32, Float64].includes?(T) %}
        raise "Cannot use Array#decf! with an array of type #{T}"
      {% else %}
        {% if flag?(:yunosynth_debug) %}
          self.[idx] -= delta
        {% else %}2
          self.unsafe_put(idx, self.unsafe_fetch(idx) &- delta)
        {% end %}
        self
      {% end %}
    {% end %}
  end
end

struct Slice
  @[AlwaysInline]
  def get!(idx)
    {% if flag?(:yunosynth_debug) %}
      self.[idx]
    {% else %}
      self.unsafe_fetch(idx)
    {% end %}
  end

  @[AlwaysInline]
  def put!(idx, val)
    {% if flag?(:yunosynth_debug) %}
      self.[idx] = val
    {% else %}
      self.unsafe_put(idx, val)
    {% end %}
  end

  @[AlwaysInline]
  def incf!(idx, delta = 1)
    {% begin %}
      {% unless [UInt8, UInt16, UInt32, UInt64, UInt128,
                 Int8, Int16, Int32, Int64, Int128,
                 Float32, Float64].includes?(T) %}
        raise "Cannot use Slice#incf! with a Slice of type #{T}"
      {% else %}
        {% if flag?(:yunosynth_debug) %}
          self.[idx] += delta
        {% else %}
          self.unsafe_put(idx, self.unsafe_fetch(idx) &+ delta)
        {% end %}
        self
      {% end %}
    {% end %}
  end

  @[AlwaysInline]
  def decf!(idx, delta = 1)
    {% begin %}
      {% unless [UInt8, UInt16, UInt32, UInt64, UInt128,
                 Int8, Int16, Int32, Int64, Int128,
                 Float32, Float64].includes?(T) %}
        raise "Cannot use Slice#decf! with an Slice of type #{T}"
      {% else %}
        {% if flag?(:yunosynth_debug) %}
          self.[idx] -= delta
        {% else %}2
          self.unsafe_put(idx, self.unsafe_fetch(idx) &- delta)
        {% end %}
        self
      {% end %}
    {% end %}
  end
end

module Yuno
  # The internal sample format in YunoSynth.
  alias StreamSample = Int32

  # :nodoc:
  # A special type of buffer made of two arrays, where each element is a slice
  # of samples.  This is used for PCM rendering.
  alias OutputBuffers = Array(Slice(StreamSample))

  # :nodoc:
  # few emulators that have paired chips need a fake buffer to render into.  I
  # believe this is done for synchronization purposes.
  FAKE_BUF = [
    Slice(StreamSample).new(0, 0i32),
    Slice(StreamSample).new(0, 0i32)
  ]

  # Base class for all YunoSynth-related errors.
  class YunoError < Exception
  end

  # :nodoc:
  # Wraps `RemiLib.log.dlog!` so that it's only presesnt in the
  # compiled code if `-Dyunosynth_debug` is defined.
  macro dlog!(msg)
    {% if flag?(:yunosynth_debug) %}
      RemiLib.log.dlog!({{msg}})
    {% end %}
  end

  # :nodoc:
  # Wraps `RemiLib.log.warn`.
  macro warn(msg)
    RemiLib.log.warn({{msg}})
  end

  # :nodoc:
  # Wraps `RemiLib.log.error`.
  macro error(msg)
    RemiLib.log.error({{msg}})
  end

  # :nodoc:
  # A single stereo sample.
  class Sample(T)
    # The left channel value for this sample.
    property left : T

    # The right channel value for this sample.
    property right : T

    # Creates a new `Sample(T)` with the given left and right channel values.
    def initialize(@left : T, @right : T)
    end
  end

  # Represents the type of a single chip.  The numerical value associated with
  # one of these values will always match the `CHIP_ID` of the chips.
  enum ChipType
    # Texas Instruments SN76489
    Sn76489

    # Yamaha YM2413 (OPLL)
    Ym2413

    # Yamaha YM2612 (OPN2)
    Ym2612

    # Yamaha YM2151 (OPM)
    Ym2151

    # Sega SegaPCM (315-5218)
    SegaPcm

    # Ricoh RF5C69 (aka RF5C164, RF5C105).  RF5C164 has a separate entry.
    Rf5c68

    # Yamaha YM2203 (OPN)
    Ym2203

    # Yamaha YM2608 (OPNA)
    Ym2608

    # Yamaha YM2610 and YM2610B (OPNB)
    Ym2610

    # Yamaha YM3812 (OPL2)
    Ym3812

    # Yamaha YM3526 (OPL)
    Ym3526

    # Yamaha Y8950 (MSX-Audio)
    Y8950

    # Yamaha YMF262 (OPL3)
    Ymf262

    # Yamaha YMF278B (OPL4)
    Ymf278b

    # Yamaha YMF271 (OPX)
    Ymf271

    # Yamaha YMZ280B (PCMD8)
    Ymz280b

    # Ricoh RF5C164
    Rf5c164

    # Sega PWM (32x)
    Pwm

    # General Instruments AY-1-8910 / AY-3-8910 / AY-3-8912 / AY-3-8913 / Yamaha YM2149 / Yamaha YM2149F
    Ay8910

    # Nintendo DMG (GameBoy/GameBoy Color)
    Dmg

    # Nintendo APU (NES)
    NesApu

    # Sega MultiPCM / Yamaha YMW258-F
    MultiPcm

    # NEC µPD7759
    Upd7759

    # OKI M6258
    Okim6258

    # OKI M6295
    Okim6295

    # Konami 051649
    K051649

    # Konami 054539
    K054539

    # Hudson HuC6280
    Huc6280

    # Namco C140 / C219
    C140

    # Konami 053260
    K053260

    # Atari POKEY
    Pokey

    # QSound DSP16A (Capcom CP System II)
    Qsound

    # Sega Saturn Custom Sound Processor / Yamaha YMF292
    Scsp

    # Bandai Wonderswan/Wonderswan Color
    Wonderswan

    # Nintendo VSU-VUE (Virtual Boy)
    VsuVue

    # Philips SAA1099
    Saa1099

    # Ensoniq ES5503
    Es5503

    # Ensoniq ES5506
    Es5506

    # Namco C352
    C352

    # Seta X1-010
    X1_010

    # Irem GA20
    Ga20

    # A special value indicating an uninitialized value.
    Unknown = 0xFF

    # Creates a new `AbstractChip` instance of the type that is represented by
    # `self`.
    def makeInstance(curCount : Int32, absChipCount : Int32, vgm : VgmFile, sampleRate : UInt32, samplingMode : UInt8,
                     playerSampleRate : UInt32, settings : VgmPlayerSettings,
                     previousChip : AbstractChip?) : AbstractChip?
      case self
      when .huc6280? then Yuno::Chips::HuC6280.new(curCount, absChipCount, vgm, sampleRate, samplingMode,
                                                   playerSampleRate, emuCore: settings.huc6280Core)
      when .k051649? then Yuno::Chips::K051649.new(curCount, absChipCount, vgm, sampleRate, samplingMode,
                                                   playerSampleRate)
      when .ay8910? then Yuno::Chips::Ay8910.new(curCount, absChipCount, vgm, sampleRate, samplingMode,
                                                 playerSampleRate)
      when .c352? then Yuno::Chips::C352.new(curCount, absChipCount, vgm, sampleRate, samplingMode, playerSampleRate)
      when .ym2151? then Yuno::Chips::YM2151.new(curCount, absChipCount, vgm, sampleRate, samplingMode,
                                                 playerSampleRate, emuCore: settings.ym2151Core)
      when .multi_pcm? then Yuno::Chips::MultiPCM.new(curCount, absChipCount, vgm, sampleRate, samplingMode,
                                                      playerSampleRate)
      when .ymz280b? then Yuno::Chips::YMZ280B.new(curCount, absChipCount, vgm, sampleRate, samplingMode,
                                                   playerSampleRate)
      when .okim6258? then Yuno::Chips::OKIMSM6258.new(curCount, absChipCount, vgm, sampleRate, samplingMode,
                                                       playerSampleRate)
      when .qsound? then Yuno::Chips::QSound.new(curCount, absChipCount, vgm, sampleRate, samplingMode,
                                                 playerSampleRate)
      when .okim6295? then Yuno::Chips::OKIMSM6295.new(curCount, absChipCount, vgm, sampleRate, samplingMode,
                                                       playerSampleRate)
      when .dmg? then Yuno::Chips::DMG.new(curCount, absChipCount, vgm, sampleRate, samplingMode, playerSampleRate)
      when .ym2203? then Yuno::Chips::YM2203.new(curCount, absChipCount, vgm, sampleRate, samplingMode,
                                                 playerSampleRate)
      when .k054539? then Yuno::Chips::K054539.new(curCount, absChipCount, vgm, sampleRate, samplingMode,
                                                   playerSampleRate)
      when .c140? then Yuno::Chips::C140.new(curCount, absChipCount, vgm, sampleRate, samplingMode, playerSampleRate)
      when .k053260? then Yuno::Chips::K053260.new(curCount, absChipCount, vgm, sampleRate, samplingMode,
                                                   playerSampleRate)
      when .sega_pcm? then Yuno::Chips::SegaPCM.new(curCount, absChipCount, vgm, sampleRate, samplingMode,
                                                    playerSampleRate)
      when .ga20? then Yuno::Chips::GA20.new(curCount, absChipCount, vgm, sampleRate, samplingMode, playerSampleRate)
      when .ym2608? then Yuno::Chips::YM2608.new(curCount, absChipCount, vgm, sampleRate, samplingMode,
                                                 playerSampleRate)
      when .upd7759? then Yuno::Chips::Upd7759.new(curCount, absChipCount, vgm, sampleRate, samplingMode,
                                                   playerSampleRate)
      when .ym2610? then Yuno::Chips::YM2610.new(curCount, absChipCount, vgm, sampleRate, samplingMode,
                                                 playerSampleRate)
      when .es5503? then Yuno::Chips::ES5503.new(curCount, absChipCount, vgm, sampleRate, samplingMode,
                                                 playerSampleRate)
      when .ym3812? then Yuno::Chips::YM3812.new(curCount, absChipCount, vgm, sampleRate, samplingMode,
                                                 playerSampleRate)
      when .ymf262? then Yuno::Chips::YMF262.new(curCount, absChipCount, vgm, sampleRate, samplingMode,
                                                 playerSampleRate)
      when .vsu_vue? then Yuno::Chips::VsuVue.new(curCount, absChipCount, vgm, sampleRate, samplingMode,
                                                  playerSampleRate)
      when .ym2612? then Yuno::Chips::YM2612.new(curCount, absChipCount, vgm, sampleRate, samplingMode,
                                                 playerSampleRate)
      when .sn76489? then Yuno::Chips::SN76489.new(curCount, absChipCount, vgm, sampleRate, samplingMode,
                                                   playerSampleRate, previousChip)
      when .rf5c164? then Yuno::Chips::Rf5c164.new(curCount, absChipCount, vgm, sampleRate, samplingMode,
                                                   playerSampleRate)
      when .y8950? then Yuno::Chips::Y8950.new(curCount, absChipCount, vgm, sampleRate, samplingMode, playerSampleRate)
      when .ym3526? then Yuno::Chips::YM3526.new(curCount, absChipCount, vgm, sampleRate, samplingMode,
                                                 playerSampleRate)
      when .nes_apu? then Yuno::Chips::Nes.new(curCount, absChipCount, vgm, sampleRate, samplingMode, playerSampleRate)
      when .x1_010? then Yuno::Chips::X1_010.new(curCount, absChipCount, vgm, sampleRate, samplingMode,
                                                 playerSampleRate)
      when .saa1099? then Yuno::Chips::Saa1099.new(curCount, absChipCount, vgm, sampleRate, samplingMode,
                                                   playerSampleRate)
      when .pwm? then Yuno::Chips::Pwm.new(curCount, absChipCount, vgm, sampleRate, samplingMode, playerSampleRate)
      when .wonderswan? then Yuno::Chips::Wonderswan.new(curCount, absChipCount, vgm, sampleRate, samplingMode,
                                                         playerSampleRate)
      when .ym2413? then Yuno::Chips::YM2413.new(curCount, absChipCount, vgm, sampleRate, samplingMode,
                                                 playerSampleRate)
      when .unknown? then raise "Attempted to start unknown chip"
      else nil
      end
    end

    # Returns the short name of this `ChipType` as a string.
    #
    # Note: for the most accurate name when using a VGM file, instantiating a
    # chip instance and using the `AbstractChip#shortName` method will be much
    # more accurate.  This is because some chips have a different name depending
    # on what they were used for, and this can only be determined after parsing
    # the VGM."
    def shortName : String
      case self
      in ChipType::Sn76489 then "SN76489"
      in ChipType::Ym2413 then "YM2413"
      in ChipType::Ym2612 then "YM2612"
      in ChipType::Ym2151 then "YM2151"
      in ChipType::SegaPcm then "SegaPCM"
      in ChipType::Rf5c68 then "RF5C68"
      in ChipType::Ym2203 then "YM2203"
      in ChipType::Ym2608 then "YM2608"
      in ChipType::Ym2610 then "YM2610"
      in ChipType::Ym3812 then "YM3812"
      in ChipType::Ym3526 then "YM3526"
      in ChipType::Y8950 then "Y8950"
      in ChipType::Ymf262 then "YMF262"
      in ChipType::Ymf278b then "YMF278B"
      in ChipType::Ymf271 then "YMF271"
      in ChipType::Ymz280b then "YMZ280B"
      in ChipType::Rf5c164 then "RF5C164"
      in ChipType::Pwm then "32x PWM"
      in ChipType::Ay8910 then "AY-1-8910"
      in ChipType::Dmg then "DMG"
      in ChipType::NesApu then "NES APU"
      in ChipType::MultiPcm then "MultiPCM"
      in ChipType::Upd7759 then "µPD7759"
      in ChipType::Okim6258 then "M6258"
      in ChipType::Okim6295 then "M6295"
      in ChipType::K051649 then "051649"
      in ChipType::K054539 then "054539"
      in ChipType::Huc6280 then "HuC6280"
      in ChipType::C140 then "C140"
      in ChipType::K053260 then "053260"
      in ChipType::Pokey then "POKEY"
      in ChipType::Qsound then "QSound"
      in ChipType::Scsp then "SCSP"
      in ChipType::Wonderswan then "Wonderswan"
      in ChipType::VsuVue then "VSU-VUE"
      in ChipType::Saa1099 then "SAA1099"
      in ChipType::Es5503 then "ES5503"
      in ChipType::Es5506 then "ES5506"
      in ChipType::C352 then "C352"
      in ChipType::X1_010 then "X1-010"
      in ChipType::Ga20 then "GA20"
      in ChipType::Unknown then "Unknown"
      end
    end

    # Returns the long name of this `ChipType` as a string.
    #
    # Note: for the most accurate name when using a VGM file, instantiating a
    # chip instance and using the `AbstractChip#name` method will be much more
    # accurate.  This is because some chips have a different name depending on
    # what they were used for, and this can only be determined after parsing the
    # VGM."
    def niceName : String
      case self
      in ChipType::Sn76489 then "Texas Instruments SN76489 and derivatives"
      in ChipType::Ym2413 then "Yamaha YM2413 (OPLL)"
      in ChipType::Ym2612 then "Yamaha YM2612/YM3438 (OPN2)"
      in ChipType::Ym2151 then "Yamaha YM2151 (OPM)"
      in ChipType::SegaPcm then "Sega SegaPCM (315-5218)"
      in ChipType::Rf5c68 then "Ricoh RF5C68/RF5C105" # RF5C164 isn't listed here because it's handled separately
      in ChipType::Ym2203 then "Yamaha YM2203 (OPN)"
      in ChipType::Ym2608 then "Yamaha YM2608 (OPNA)"
      in ChipType::Ym2610 then "Yamaha YM2610/YM2610B (OPNB)"
      in ChipType::Ym3812 then "Yamaha YM3812 (OPL2)"
      in ChipType::Ym3526 then "Yamaha YM3526 (OPL)"
      in ChipType::Y8950 then "Yamaha Y8950 (MSX-Audio)"
      in ChipType::Ymf262 then "Yamaha YMF262 (OPL3)"
      in ChipType::Ymf278b then "Yamaha YMF278B (OPL4)"
      in ChipType::Ymf271 then "Yamaha YMF271 (OPX)"
      in ChipType::Ymz280b then "Yamaha YMZ280B (PCMD8)"
      in ChipType::Rf5c164 then "Ricoh RF5C164"
      in ChipType::Pwm then "Sega 32x PWM"
      in ChipType::Ay8910 then "General Instruments AY-1-8910 and derivatives"
      in ChipType::Dmg then "Nintendo DMG (GameBoy/GameBoy Color)"
      in ChipType::NesApu then "Nintendo APU"
      in ChipType::MultiPcm then "Sega MultiPCM"
      in ChipType::Upd7759 then "NEC µPD7759"
      in ChipType::Okim6258 then "OKI M6258"
      in ChipType::Okim6295 then "OKI M6295"
      in ChipType::K051649 then "Konami 051649"
      in ChipType::K054539 then "Konami 054539"
      in ChipType::Huc6280 then "Hudson HuC6280"
      in ChipType::C140 then "Namco C140/C219"
      in ChipType::K053260 then "Konami 053260"
      in ChipType::Pokey then "Atari POKEY"
      in ChipType::Qsound then "QSound DSP16A (Capcom CP System II)"
      in ChipType::Scsp then "Sega Saturn Custom Sound Processor"
      in ChipType::Wonderswan then "Bandai Wonderswan"
      in ChipType::VsuVue then "Nintendo VSU-VUE (Virtual Boy)"
      in ChipType::Saa1099 then "Philips SAA1099"
      in ChipType::Es5503 then "Ensoniq ES5503"
      in ChipType::Es5506 then "Ensoniq ES5506"
      in ChipType::C352 then "Namco C352"
      in ChipType::X1_010 then "Seta X1-010"
      in ChipType::Ga20 then "Irem GA20"
      in ChipType::Unknown then "Unknown chip"
      end
    end
  end

  # Returns a list of chip types that are currently supported by this library.
  def self.supportedChips : Array(ChipType)
    [
      ChipType::Ay8910,
      ChipType::C140,
      ChipType::C352,
      ChipType::Dmg,
      ChipType::Es5503,
      ChipType::Ga20,
      ChipType::Huc6280,
      ChipType::K051649,
      ChipType::K053260,
      ChipType::K054539,
      ChipType::MultiPcm,
      ChipType::NesApu,
      ChipType::Pwm,
      ChipType::Okim6258,
      ChipType::Okim6295,
      ChipType::Qsound,
      ChipType::Rf5c164,
      ChipType::Saa1099,
      ChipType::SegaPcm,
      ChipType::Sn76489,
      ChipType::Upd7759,
      ChipType::VsuVue,
      ChipType::Wonderswan,
      ChipType::X1_010,
      ChipType::Y8950,
      ChipType::Ym2151,
      ChipType::Ym2203,
      ChipType::Ym2413,
      ChipType::Ym2608,
      ChipType::Ym2610,
      ChipType::Ym2612,
      ChipType::Ym3526,
      ChipType::Ym3812,
      ChipType::Ymf262,
      ChipType::Ymz280b
    ]
  end

  # Returns a list of chip types that are not currently supported by this
  # library.
  def self.unsupportedChips : Array(ChipType)
    [
      ChipType::Es5506,
      ChipType::Pokey,
      ChipType::Scsp,
      ChipType::Ymf271,
      ChipType::Ymf278b,
      ChipType::Rf5c68
    ]
  end

  # :nodoc:
  # Wrapper for `RemiLib.bitflag?`.  This macro actually originated here,
  # written by me (Remi), but it was useful enough to abstract to the library.
  # Rather than changing the code everywhere to call the macro correctly, this
  # wrapper was added.
  #
  # This is basically shorthand for `(foo & value) != 0`.
  macro bitflag?(thing, flag)
    ::bitflag?({{thing}}, {{flag}})
  end

  # :nodoc:
  # Converts the first two bytes in *data* to a little-endian signed 32-bit
  # integer.
  @[AlwaysInline]
  protected def self.readInt32LE(data : Bytes) : Int32
    (data[3].to_i32! << 24) |
    (data.get!(2).to_i32! << 16) |  # We've already indexed 3 elements, so 2, 1, and 0 are safe
    (data.get!(1).to_i32! <<  8) |
     data.get!(0).to_i32!
  end

  # :nodoc:
  # Converts the first two bytes in *data* to a little-endian signed 16-bit
  # integer.
  @[AlwaysInline]
  protected def self.readInt16LE(data : Bytes) : Int16
    (data[1].to_i16! <<  8) |
     data.get!(0).to_i16! # We already indexed 1 element, so 0 is safe
  end
end
