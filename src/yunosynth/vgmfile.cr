#### YunoSynth
#### Copyright (C) 2023-2024 Remilia Scarlet <remilia@posteo.jp>
#### Portions are based on VGMPlay, Copyright (C) Valley Bell
####
#### This program is free software: you can redistribute it and/or
#### modify it under the terms of the GNU Affero General Public
#### License as published by the Free Software Foundation, either
#### version 3 of the License, or (at your option) any later version.
####
#### This program is distributed in the hope that it will be useful,
#### but WITHOUT ANY WARRANTY; without even the implied warranty of
#### MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#### Affero General Public License for more details.
####
#### You should have received a copy of the GNU Affero General Public License
#### along with this program.  If not, see <https://www.gnu.org/licenses/>.
require "./gd3tag"
require "./common"
require "./vgmdecompressor"

module Yuno
  # A virtual representation of a VGM file.
  class VgmFile
    # Represents an error that occured while reading an VGM file.
    class Error < YunoError
    end

    # The magic four bytes at the start of a VGM file.
    VGM_MAGIC = "Vgm "

    # Represents the header data within a VGM file.
    class Header
      # :nodoc:
      # Extra metadata for a field in a VGM file header.
      annotation HeaderField
      end

      # Defines a field within a VGM file header.  This is used to save a lot of
      # repetitive typing.  This macro:
      #
      # * Defines a getter and applies a `HeaderField` annotation to it with the
      #   appropriate data.
      # * Defines a method to get (`<name>Offset`) if *offset* is greater than
      #   zero.
      # * Defines a setter function (`<name>Offset`) if *offset* is greater than
      #   zero.
      # * Sets the visibility to `protected` is *internal* is `true`, otherwise
      #   everything is public.
      #
      # The other parameters are:
      #
      # * *name*: The name of the field.
      # * *default*: The default value of the field.
      # * *noread*: Indicates that this field should not be read automatically,
      #   it is read manually.
      # * *since*: The minimum VGM spec version for this field.  Older VGMs
      #   exclude newer fields.
      private macro defField(name, type, default, *, noread = false, offset = 0, since = 0, internal = false)
        {% visibility = nil %}
        {% if internal %}
          {% visibility = :protected %}
        {% end %}

        @[HeaderField(noread: {{noread}}, since: {{since}}, offset: {{offset}})]
        {%if visibility %} {{visibility}} {% end %} getter {{name.id}} : {{type.id}} = {{default}}

        {% if offset > 0 %}
          @[AlwaysInline]
          {%if visibility %} {{visibility}} {% end %} def {{name.id}}Offset
            {{offset}}
          end

          protected def {{name.id}}=(value : {{type.id}}) : Nil
            @{{name.id}} = value
          end
        {% end %}
      end

      defField(relativeEof, UInt32, 0, noread: true)
      defField(version, UInt32, 0, noread: true)
      defField(sn76489Clock, UInt32, 0, offset: 0x0C)
      defField(ym2413Clock, UInt32, 0, offset: 0x10)
      defField(gd3Offset, UInt32, 0, offset: 0x14)
      defField(totalSamples, UInt32, 0, offset: 0x18)
      defField(loopOffset, UInt32, 0, offset: 0x1C)
      defField(loopSamples, UInt32, 0, offset: 0x20)
      defField(rate, UInt32, 0, since: 0x00000101, offset: 0x24)

      defField(sn76489Feedback, UInt16, 0, since: 0x00000110, offset: 0x28)
      defField(sn76489ShiftRegisterWidth, UInt8, 0, since: 0x00000110, offset: 0x2A)
      defField(sn76489Flags, UInt8, 0, since: 0x00000151, offset: 0x2B)
      defField(ym2612Clock, UInt32, 0, since: 0x00000110, offset: 0x2C)
      defField(ym2151Clock, UInt32, 0, since: 0x00000110, offset: 0x30)

      defField(dataOffset, UInt32, 0, since: 0x00000150, offset: 0x34)

      defField(segaPcmClock, UInt32, 0, since: 0x00000151, offset: 0x38)
      defField(segaPcmInterfaceReg, UInt32, 0, since: 0x00000151, offset: 0x3C)
      defField(rf5c68Clock, UInt32, 0, since: 0x00000151, offset: 0x40)
      defField(ym2203Clock, UInt32, 0, since: 0x00000151, offset: 0x44)
      defField(ym2608Clock, UInt32, 0, since: 0x00000151, offset: 0x48)
      defField(ym2610Clock, UInt32, 0, since: 0x00000151, offset: 0x4C)
      defField(ym3812Clock, UInt32, 0, since: 0x00000151, offset: 0x50)
      defField(ym3526Clock, UInt32, 0, since: 0x00000151, offset: 0x54)
      defField(y8950Clock, UInt32, 0, since: 0x00000151, offset: 0x58)
      defField(ymf262Clock, UInt32, 0, since: 0x00000151, offset: 0x5C)
      defField(ymf278bClock, UInt32, 0, since: 0x00000151, offset: 0x60)
      defField(ymf271Clock, UInt32, 0, since: 0x00000151, offset: 0x64)
      defField(ymz280bClock, UInt32, 0, since: 0x00000151, offset: 0x68)
      defField(rf5c164Clock, UInt32, 0, since: 0x00000151, offset: 0x6C)
      defField(pwmClock, UInt32, 0, since: 0x00000151, offset: 0x70)

      defField(ay8910Clock, UInt32, 0, since: 0x00000151, offset: 0x74)
      defField(ay8910ChipType, UInt8, 0, since: 0x00000151, offset: 0x78)
      defField(ay8910Flags, UInt8, 0, since: 0x00000151, offset: 0x79)
      defField(ayYm2203Flags, UInt8, 0, since: 0x00000151, offset: 0x7A)
      defField(ayYm2608Flags, UInt8, 0, since: 0x00000151, offset: 0x7B)

      defField(volumeModifier, UInt8, 0, since: 0x00000160, offset: 0x7C)
      defField(reserved2, UInt8, 0, since: 0x00000160, offset: 0x7D)
      defField(loopBase, Int8, 0, since: 0x00000160, offset: 0x7E)
      defField(loopModifier, UInt8, 0, since: 0x00000151, offset: 0x7F)

      defField(dmgClock, UInt32, 0, since: 0x00000161, offset: 0x80)
      defField(nesApuClock, UInt32, 0, since: 0x00000161, offset: 0x84)
      defField(multiPcmClock, UInt32, 0, since: 0x00000161, offset: 0x88)
      defField(upd7759Clock, UInt32, 0, since: 0x00000161, offset: 0x8C)
      defField(okim6258Clock, UInt32, 0, since: 0x00000161, offset: 0x90)
      defField(okim6258Flags, UInt8, 0, since: 0x00000161, offset: 0x94)
      defField(k054539Flags, UInt8, 0, since: 0x00000161, offset: 0x95)
      defField(c140ChipType, UInt8, 0, since: 0x00000161, offset: 0x96)

      defField(reservedFlags, UInt8, 0, since: 0x00000161, offset: 0x97)

      defField(okim6295Clock, UInt32, 0, since: 0x00000161, offset: 0x98)
      defField(k051649Clock, UInt32, 0, since: 0x00000161, offset: 0x9C)
      defField(k054539Clock, UInt32, 0, since: 0x00000161, offset: 0xA0)
      defField(huc6280Clock, UInt32, 0, since: 0x00000161, offset: 0xA4)
      defField(c140Clock, UInt32, 0, since: 0x00000161, offset: 0xA8)
      defField(k053260Clock, UInt32, 0, since: 0x00000161, offset: 0xAC)
      defField(pokeyClock, UInt32, 0, since: 0x00000161, offset: 0xB0)
      defField(qsoundClock, UInt32, 0, since: 0x00000161, offset: 0xB4)
      defField(scspClock, UInt32, 0, since: 0x00000171, offset: 0xB8)

      defField(extraHeaderOffset, UInt32, 0, since: 0x00000170, offset: 0xBC)
      defField(wonderswanClock, UInt32, 0, since: 0x00000171, offset: 0xC0)
      defField(vsuClock, UInt32, 0, since: 0x00000171, offset: 0xC4)
      defField(saa1099Clock, UInt32, 0, since: 0x00000171, offset: 0xC8)
      defField(es5503Clock, UInt32, 0, since: 0x00000171, offset: 0xCC)
      defField(es5506Clock, UInt32, 0, since: 0x00000171, offset: 0xD0)
      defField(es5503NumChannels, UInt8, 0, since: 0x00000171, offset: 0xD4)
      defField(es5506NumChannels, UInt8, 0, since: 0x00000171, offset: 0xD5)
      defField(c352ClockDiv, UInt8, 0, since: 0x00000171, offset: 0xD6)
      defField(reserved3, UInt8, 0, since: 0x00000171, offset: 0xD7)
      defField(x1_010Clock, UInt32, 0, since: 0x00000171, offset: 0xD8)
      defField(c352Clock, UInt32, 0, since: 0x00000171, offset: 0xDC)
      defField(ga20Clock, UInt32, 0, since: 0x00000171, offset: 0xE0)
      defField(reserved4, UInt32, 0, since: 0x00000171, offset: 0xE4)

      # Creates a new `Header` instance by reading data from *io*.
      protected def initialize(io : IO)
        # Check for the VGM magic bytes.
        unless io.read_string(4) == VGM_MAGIC
          raise VgmFile::Error.new("Not a VGM file")
        end

        # Read the first two fields manually.
        @relativeEof = io.read_bytes(UInt32, IO::ByteFormat::LittleEndian)
        @version = io.read_bytes(UInt32, IO::ByteFormat::LittleEndian)

        # Now read the rest of the fields.  We skip any fields marked :noread
        # since those are read manually (such as the two fields directly above).
        {% begin %}
          {% properties = {} of Nil => Nil %}
          {% for field in @type.instance_vars %}
            {% ann = field.annotation(Yuno::VgmFile::Header::HeaderField) %}
            {% if ann %}
              {% unless ann[:noread] %}
                {% properties[field.id] = {
                   type: field.type,
                   since: (ann[:since] || 0),
                   offset: (ann[:offset] || 0)
                 } %}
              {% end %}
            {% end %}
          {% end %}

          # Read the fields.
          {% for field, prop in properties %}
            if @version >= {{prop[:since]}}
              %offset = {{prop[:offset]}}
              io.pos = %offset if %offset > 0
              @{{field.id}} = io.read_bytes({{prop[:type]}}, IO::ByteFormat::LittleEndian)
            end
          {% end %}

          # HACK If the SN76489 field is 0, but we have a YM2612 and any of the
          # other SN764xx fields are set, assume there's an SN76489 anyway and
          # using the default clock.
          #
          # A number of VGMs seem to do this.
          if sn76489Clock == 0 && ym2612Clock != 0 &&
             (sn76489Feedback != 0 || sn76489Flags != 0 || sn76489ShiftRegisterWidth != 0)
            Yuno.dlog!("VGM has a YM2612, but its SN76489 clock is 0.  " \
                       "Assuming an SN764xx is present and using the default clock rate.")
            @sn76489Clock = 3579545u32
          end

        {% end %}
      end

      protected def initialize
      end

      # Changes the relative EOF of the VGM command data.
      protected def relativeEof=(value : UInt32) : Nil
        @relativeEof = value
      end
    end

    # Represents the extra header within a VGM file.
    class ExtraHeader
      # Represents a clock value within the extra header.
      class Clock
        # The ID of the chip that this clock value is for.
        property chipType : ChipType

        # The clock value.
        property clock : UInt32

        protected def initialize(io : IO)
          chipTypeByte = io.read_byte ||
                         raise YunoError.new("Could not read ChipType in Extra Header Clock Data")
          @chipType = Yuno::ChipType.from_value(chipTypeByte) ||
                      raise YunoError.new(sprintf("Unknown ChipType in Extra Header Clock Data: $%02x",
                                                  chipTypeByte))

          @clock = io.readUInt32
          Yuno.dlog!("    ExHeader clock for #{@chipType}: #{@clock}")
        end
      end

      # Represents a modified volume level within the extra header.
      class Volume
        # The ID of the chip that this volume level is for.
        property chipId : UInt8

        # When bit 0 is set, this is for the second chip.
        property flags : UInt8

        # The new volume value.
        property volume : UInt16

        protected def initialize(io : IO)
          @chipId = io.read_byte || raise YunoError.new("Could not read ChipType in Extra Header Volume Data")
          @flags = io.read_byte || raise YunoError.new("Could not read Flags in Extra Header Volume Data")
          @volume = io.readUInt16
        end
      end

      # The modified clock values within this extra header.
      getter clocks : Hash(ChipType, Array(Clock)) = {} of ChipType => Array(Clock)

      # The modified volume values within this extra header.
      getter volumes : Hash(UInt8, Array(Volume)) = {} of UInt8 => Array(Volume)

      protected def initialize(io : IO)
        initOffset = io.pos

        headerSize = io.readUInt32
        if headerSize == 4 # Nothing to read
          Yuno.dlog!("Extra header is empty")
          return
        elsif headerSize > 12
          headerSize = 12 # Clamp to max size of the header
        end

        headerLimit = initOffset + headerSize
        Yuno.dlog!(sprintf("  Extra header position limit: $%08x", headerLimit))

        clockOffsetPos = io.pos
        if clockOffsetPos < headerLimit
          clockOffset = io.readInt32
          Yuno.dlog!(sprintf("Extra header clock offset value is at $%08x", clockOffsetPos + clockOffset))
        else
          clockOffset = 0
        end

        volumeOffsetPos = io.pos
        if volumeOffsetPos < headerLimit
          volumeOffset = io.readInt32
          Yuno.dlog!(sprintf("Extra header volume offset value is at $%08x", volumeOffsetPos + volumeOffset))
        else
          volumeOffset = 0
        end

        # Read clock values
        unless clockOffset == 0
          io.pos = clockOffsetPos + clockOffset
          entryCount = io.read_byte || raise YunoError.new("Could not read the number of Clock entries in the Extra Header")
          entryCount.times do |_|
            clock = Clock.new(io)
            @clocks[clock.chipType] = [] of Clock unless @clocks.has_key?(clock.chipType)
            @clocks[clock.chipType] << clock
          end
        else
          Yuno.dlog!("  No clock values in extra header")
        end

        # Read volume values
        unless volumeOffset == 0
          io.pos = volumeOffsetPos + volumeOffset
          entryCount = io.read_byte || raise YunoError.new("Could not read the number of Volume entries in the Extra Header")
          entryCount.times do |_|
            vol = Volume.new(io)
            @volumes[vol.chipId] = [] of Volume unless @volumes.has_key?(vol.chipId)
            @volumes[vol.chipId] << vol
          end
        else
          Yuno.dlog!("  No volume values in extra header")
        end
      end
    end

    # Returns the header associated with this VGM.
    getter header : Header

    # Returns the GD3 tag associated with this VGM.
    getter gd3Tag : Gd3Tag

    # Returns the data (that is, the stream of instructions and data bytes) associated with this VGM.
    getter data : Bytes

    protected getter dataOffset : UInt32

    # The extra header, if any, in this VGM file.
    getter extraHeader : ExtraHeader? = nil

    @knownUsedChips : Hash(ChipType, Int32)?
    @unsupportedChips : Array(ChipType)?

    protected def initialize
      @header = Header.new
      @gd3Tag = Gd3Tag.new
      @data = Bytes.new(0)
      @dataOffset = 0
    end

    # Creates a new `VgmFile` instance by reading data from `io`.  The data in
    # `io` cannot be compressed.  If you need to read possibly compressed data
    # from an `::IO`, then use `VgmFile.fromIO` instead.  The IO must be able to
    # seek.
    def initialize(io : IO)
      startAt = io.pos

      # Read header (including file magic)
      @header = Header.new(io)

      # Adjust data offset
      Yuno.dlog!(sprintf("Pre-adjustment VGM data offset in header: $%08x", @header.dataOffset))
      @header.dataOffset = case @header.dataOffset
                           when 0, 0x0C then 0x40_u32
                           else @header.dataOffset + 0x34
                           end

      Yuno.dlog!(sprintf("  VGM data offset is now: $%08x", @header.dataOffset))

      # Determine the offset to the VGM command data.  If the VGM data starts at
      # absolute offset 0x40, this will contain value 0x0C. For VGMs using a VGM
      # spec prior to v1.50, this should be 0 and the VGM data must start at
      # offset 0x40.
      @dataOffset = @header.dataOffset
      if @dataOffset < 0x40
        Yuno.dlog!(sprintf("Invalid data offset: %02x$", @dataOffset))
        @dataOffset = 0x40
      end

      Yuno.dlog!(sprintf("Post-adjustment VGM data offset: $%08x (header: $%08x)", @dataOffset, @header.dataOffset))

      if @header.loopOffset != 0 && header.loopSamples == 0
        Yuno.dlog!("Ignoring zero-sample loop")
        @header.loopOffset = 0
      end

      # Do we have an extra header?
      if @header.extraHeaderOffset != 0
        Yuno.dlog!(sprintf("Reading extra header at $%08x", @header.extraHeaderOffset + @header.extraHeaderOffsetOffset))
        io.pos = @header.extraHeaderOffset + @header.extraHeaderOffsetOffset
        @extraHeader = ExtraHeader.new(io)
      end

      # Read GD3 tag
      io.pos = @header.gd3Offset + @header.gd3OffsetOffset
      @gd3Tag = Gd3Tag.new(io)

      # If the VGM data starts at an offset that is lower than 0x100, all
      # overlapping header bytes have to be handled as they were zero.  So set
      # these to zero now, because they're actually data.
      if @header.dataOffset < 0x100
        clearUnusedHeaderData
      end

      # Read data
      io.pos = @header.dataOffset
      Yuno.dlog!(sprintf("Reading VGM data starting at $%08x", io.pos))
      @data = io.getb_to_end

      # A VGM file can, optionally, start its command data within the header
      # space itself.  When it does this, the unused fields are treated as if
      # they were zero.  So we need to determine where the command data
      # _actually_ starts.
      totalSize = io.pos - startAt
      if @header.relativeEof == 0 || @header.relativeEof > totalSize
        Yuno.dlog!(sprintf("Invalid VGM EOF offset: %02x$ (should be: %02x$)",
                           @header.relativeEof, totalSize))
        @header.relativeEof = totalSize.to_u32
      end

      # Adjust loop offset.
      if @header.loopOffset >= @header.relativeEof
        @header.loopOffset = 0
      end

      # VGMs using the VGM spec v1.0 and earlier always set this to zero.
      if @header.version < 0x00000101
        @header.rate = 0
      end

      # Older versions of the VGM spec use the YM2413 clock for the YM2612 and
      # YM2151.
      if @header.version < 0x00000110
        @header.ym2612Clock = @header.ym2413Clock
        @header.ym2151Clock = @header.ym2413Clock
      end

      # Older versions of the VGM spec do not have these fields.
      if @header.version < 0x00000150
        @header.segaPcmClock = 0
        @header.segaPcmInterfaceReg = 0
      end
    end

    # Loads a VGM file from `path`, returning a new `VgmFile` instance.  If the
    # VGM is compressed using a supported compression scheme, it will first be
    # decompressed into RAM.
    def self.load(path : Path|String) : VgmFile
      File.open(path, "rb") do |file|
        hint = VgmDecompressor.getHint(path)
        io = VgmDecompressor.maybeDecompress(file, hint)
        VgmFile.new(io)
      end
    end

    # Loads a VGM file from `io`, returning a new `VgmFile` instance.  The data
    # in IO may be compressed.  The IO must be able to seek.
    def self.fromIO(io : IO) : VgmFile
      source = VgmDecompressor.maybeDecompress(io)
      VgmFile.new(source)
    end

    # Reads the first four bytes from `io` to check for the magic bytes that
    # indicate a VGM file.  If they're found, this returns `true`, otherwise it
    # returns `false`.  The IO must be seekable.
    def self.validVgmFile?(io : IO) : Bool
      # ameba:disable Style/RedundantBegin
      begin
        source = VgmDecompressor.maybeDecompress(io)
        source.read_string(4) == VGM_MAGIC
      rescue err : Exception
        raise Error.new("Bad VGM file: #{err}")
      end
    end

    # Reads the first four bytes from the given file to check for the magic
    # bytes that indicate a VGM file.  If they're found, this returns `true`,
    # otherwise it returns `false`.
    def self.validVgmFile?(path : Path|String) : Bool
      File.open(path) do |file|
        validVgmFile?(file)
      end
    end

    # Returns `true` if this VGM file contains loop information, or `false`
    # otherwise.
    def hasLoopInfo? : Bool
      @header.loopOffset != 0
    end

    # Returns `true` if `chip` is used at least once in this VGM, or `false`
    # otherwise.
    def chipUsed?(chip : ChipType) : Bool
      # WARNING Technically there isn't anything saying that a clock value _has_
      # to be set to 0 in the specs, just that it _should_ be set to 0.  As
      # such, VgmPlayer#chipNamesUsed is probably better suited to get a list of
      # used chips.

      case chip
      in ChipType::Sn76489 then @header.sn76489Clock != 0
      in ChipType::Ym2413 then @header.ym2413Clock != 0
      in ChipType::Ym2612 then @header.ym2612Clock != 0
      in ChipType::Ym2151 then @header.ym2151Clock != 0
      in ChipType::SegaPcm then @header.segaPcmClock != 0
      in ChipType::Rf5c68 then @header.rf5c68Clock != 0
      in ChipType::Ym2203 then @header.ym2203Clock != 0
      in ChipType::Ym2608 then @header.ym2608Clock != 0
      in ChipType::Ym2610 then @header.ym2610Clock != 0
      in ChipType::Ym3812 then @header.ym3812Clock != 0
      in ChipType::Ym3526 then @header.ym3526Clock != 0
      in ChipType::Y8950 then @header.y8950Clock != 0
      in ChipType::Ymf262 then @header.ymf262Clock != 0
      in ChipType::Ymf278b then @header.ymf278bClock != 0
      in ChipType::Ymf271 then @header.ymf271Clock != 0
      in ChipType::Ymz280b then @header.ymz280bClock != 0
      in ChipType::Rf5c164 then @header.rf5c164Clock != 0
      in ChipType::Pwm then @header.pwmClock != 0
      in ChipType::Ay8910 then @header.ay8910Clock != 0
      in ChipType::Dmg then @header.dmgClock != 0
      in ChipType::NesApu then @header.nesApuClock != 0
      in ChipType::MultiPcm then @header.multiPcmClock != 0
      in ChipType::Upd7759 then @header.upd7759Clock != 0
      in ChipType::Okim6258 then @header.okim6258Clock != 0
      in ChipType::Okim6295 then @header.okim6295Clock != 0
      in ChipType::K051649 then @header.k051649Clock != 0
      in ChipType::K054539 then @header.k054539Clock != 0
      in ChipType::Huc6280 then @header.huc6280Clock != 0
      in ChipType::C140 then @header.c140Clock != 0
      in ChipType::K053260 then @header.k053260Clock != 0
      in ChipType::Pokey then @header.pokeyClock != 0
      in ChipType::Qsound then @header.qsoundClock != 0
      in ChipType::Scsp then @header.scspClock != 0
      in ChipType::Wonderswan then @header.wonderswanClock != 0
      in ChipType::VsuVue then @header.vsuClock != 0
      in ChipType::Saa1099 then @header.saa1099Clock != 0
      in ChipType::Es5503 then @header.es5503Clock != 0
      in ChipType::Es5506 then @header.es5506Clock != 0
      in ChipType::C352 then @header.c352Clock != 0
      in ChipType::X1_010 then @header.x1_010Clock != 0
      in ChipType::Ga20 then @header.ga20Clock != 0
      in ChipType::Unknown then raise "Attempted to check for existence of unknown chip: #{chip}"
      end
    end

    # Returns the current clock rate for a given `ChipType`.
    def getChipClock(chip : ChipType) : UInt32
      # WARNING Technically there isn't anything saying that a clock value _has_
      # to be set to 0 in the specs, just that it _should_ be set to 0.  As
      # such, VgmPlayer#chipNamesUsed is probably better suited to get a list of
      # used chips.

      case chip
      in ChipType::Sn76489 then @header.sn76489Clock
      in ChipType::Ym2413 then @header.ym2413Clock
      in ChipType::Ym2612 then @header.ym2612Clock
      in ChipType::Ym2151 then @header.ym2151Clock
      in ChipType::SegaPcm then @header.segaPcmClock
      in ChipType::Rf5c68 then @header.rf5c68Clock
      in ChipType::Ym2203 then @header.ym2203Clock
      in ChipType::Ym2608 then @header.ym2608Clock
      in ChipType::Ym2610 then @header.ym2610Clock
      in ChipType::Ym3812 then @header.ym3812Clock
      in ChipType::Ym3526 then @header.ym3526Clock
      in ChipType::Y8950 then @header.y8950Clock
      in ChipType::Ymf262 then @header.ymf262Clock
      in ChipType::Ymf278b then @header.ymf278bClock
      in ChipType::Ymf271 then @header.ymf271Clock
      in ChipType::Ymz280b then @header.ymz280bClock
      in ChipType::Rf5c164 then @header.rf5c164Clock
      in ChipType::Pwm then @header.pwmClock
      in ChipType::Ay8910 then @header.ay8910Clock
      in ChipType::Dmg then @header.dmgClock
      in ChipType::NesApu then @header.nesApuClock
      in ChipType::MultiPcm then @header.multiPcmClock
      in ChipType::Upd7759 then @header.upd7759Clock
      in ChipType::Okim6258 then @header.okim6258Clock
      in ChipType::Okim6295 then @header.okim6295Clock
      in ChipType::K051649 then @header.k051649Clock
      in ChipType::K054539 then @header.k054539Clock
      in ChipType::Huc6280 then @header.huc6280Clock
      in ChipType::C140 then @header.c140Clock
      in ChipType::K053260 then @header.k053260Clock
      in ChipType::Pokey then @header.pokeyClock
      in ChipType::Qsound then @header.qsoundClock
      in ChipType::Scsp then @header.scspClock
      in ChipType::Wonderswan then @header.wonderswanClock
      in ChipType::VsuVue then @header.vsuClock
      in ChipType::Saa1099 then @header.saa1099Clock
      in ChipType::Es5503 then @header.es5503Clock
      in ChipType::Es5506 then @header.es5506Clock
      in ChipType::C352 then @header.c352Clock
      in ChipType::X1_010 then @header.x1_010Clock
      in ChipType::Ga20 then @header.ga20Clock
      in ChipType::Unknown then raise "Attempted to get clock for existence of unknown chip: #{chip}"
      end
    end

    # Returns the list of all chips that are used in this VGM.  The keys of the
    # `Hash` that is returned are the types of chips, while the values dictate
    # whether the chip is solo (1) or paired (2).
    #
    # Note that `VgmPlayer#chipNamesUsed` and `VgmPlayer#chipsUsed` is the
    # preferred way to get a list of chips that are used since these return more
    # information, such as the number of each chip.  This can still be used as a
    # quick way to get a list of `ChipType`s, however.
    def chipsUsed : Hash(ChipType, Int32)
      if ret = @knownUsedChips
        ret
      else
        ret = {} of ChipType => Int32

        ChipType.values.each do |ct|
          next if ct.unknown?
          unless ret[ct]?
            # Check for dual-instances
            if getChipClock(ct) != 0
              ret[ct] = (getChipClock(ct) & 0x40000000) != 0 ? 2 : 1
            end
          end
        end

        @knownUsedChips = ret
      end
    end

    # Returns the list of all chips that are used in this VGM that are currently
    # **not** supported by YunoSynth as far as playback is concerned.
    def unsupportedChips : Array(ChipType)
      if ret = @unsupportedChips
        ret
      else
        ret = [] of ChipType
        unsup = Yuno.unsupportedChips

        ChipType.values.each do |ct|
          next if ct.unknown?
          if getChipClock(ct) != 0
            if unsup.includes?(ct)
              ret << ct
            end
          end
        end

        @unsupportedChips = ret
      end
    end

    # Returns `true` if all chips used by this VGM file are supported by
    # YunoSynth as far as playback is concerned, or `false` otherwise.
    def validateChips : Bool
      unsupportedChips.empty?
    end

    # Retrieves a volume for `chip` given a `chipType` and the number of chips
    # of `chipType`.  If `paired` is nonNil, then the volume is retrieved for
    # `chip`'s internal paired chip of type `paired`.
    def getChipVolume(chip : AbstractChip, chipType : ChipType, chipCount : Int32, paired : ChipType? = nil) : UInt16
      # Get the base volume of the chip.
      volume : UInt16 = chip.baseVolume
      Yuno.dlog!("Chip #{chip.type} has a base volume of #{chip.baseVolume}")

      # We may need to modify some values for some special cases, so check those now.
      case chipType
      when .sn76489? # and SN76496
        # If T6W28, set volume divider to 01
        chipCount = 1 if Yuno.bitflag?(@header.sn76489Clock, 0x80000000)

      when .okim6295?
        # Check for CP System 1, which needs a different volume
        if @gd3Tag.systemNameEn.starts_with?("CP") || @gd3Tag.systemNameJp.starts_with?("CP")
          Yuno.dlog!("Using CP System 1 volume hack for OKI MSM6295")
          volume = 110
        end

      when .ym2203? # Check for YM2203's integrated AY-1-8910
        volume = volume.tdiv(2) if paired.try &.ay8910?
      end

      # Adjust for the number of chips.
      volume = volume.tdiv(chipCount) if chipCount > 1

      # There may be a modification to the volume in an extra header, so check
      # for the presence of an extra header now.
      if extra = @extraHeader
        # Extra header is present.
        #
        # Determine which chip we're modifying.  This is needed because chipType
        # is always the parent chip, and paired is only non-nil if we're
        # retrieving the volume for the paired chip.
        #
        # This is used just for debugging output.
        modChip = paired.nil? ? chipType.to_s : "#{chipType}'s paired #{paired}"

        Yuno.dlog!("Checking for extra header volume adjustment for #{modChip}")

        # The value in the extra header is still looked up via the parent chip.
        vol = extra.volumes[chipType.value]?

        # Might be the paired chip
        if vol.nil? && !paired.nil?
          # Paired chips have an upper bit set to indicate the paired chip,
          # because VGM files hijack the chip type to also act as an ID.
          vol = extra.volumes[chipType.value | 0x80]?
        end

        # Did the extra header have a volume modification?
        if vol
          # Bit 15 indicates whether the extra header volume value for this chip
          # is an absolute volume, or relative.
          #
          # Bit 15 = 0 means absolute.
          # Bit 15 = 1 means relative.
          if Yuno.bitflag?(vol[0].volume, 0x8000)
            Yuno.dlog!("  Found, relative volume adjustment: #{vol[0].volume & 0x7FFF}")
            volume = ((volume.to_u32! * (vol[0].volume & 0x7FFF).to_u32! + 0x80) >> 8).to_u16!
          else
            Yuno.dlog!("  Found, absolute volume adjustment: #{vol[0].volume}")
            volume = vol[0].volume

            # TODO handle option to double the SSG volume
          end

          # Return early for debug message reasons.
          Yuno.dlog!("Determined modified volume for #{modChip} of #{chipCount} chip(s): #{volume}")
          return volume

        else
          # There was no extra header modification found for this chip.
          Yuno.dlog!("  None found")
        end
      end

      # Return the determined volume value.
      Yuno.dlog!("Determined Volume for #{chipType} of #{chipCount} chip(s): #{volume}")
      volume
    end

    # Checks to see if a chip has an alternate clock value within the extra
    # header.  This can only happen if an extra header exists within the VGM.
    # If a value is found, this returns the new clock value, otherwise it
    # returns `nil`.
    #
    # An alternate header clock value is always for the second (or higher)
    # chip, never for the first chip.
    def getAlternateChipClock(chipNum : Int32, chipType : ChipType) : UInt32?
      return nil unless chipNum > 0
      if exHeader = @extraHeader
        if clocks = exHeader.clocks.[chipType]?
          if (chipNum - 1) < clocks.size
            ret = clocks[chipNum - 1].clock
            Yuno.dlog!("#{chipType} number #{chipNum} uses alternate clock: #{ret}")
            ret
          end
        end
      end
    end

    # Unsets any header value that was accidentally set.  This can be caused if
    # the data offset is < 0x100.
    private def clearUnusedHeaderData : Nil
      # NOTE At time of writing (16 March 2023), the GA20 clock field is the
      # last field in the header that is defined.  All fields after are
      # reserved, and so we don't worry about those.

      doffset = @header.dataOffset
      Yuno.dlog!(sprintf("Clearing header fields over $%08x", doffset))

      # Start at the bottom of the header and work back.
      @header.ga20Clock = 0 if doffset <= 0xE0
      @header.c352Clock = 0 if doffset <= 0xDC
      @header.x1_010Clock = 0 if doffset <= 0xD8
      @header.c352ClockDiv = 0 if doffset <= 0xD6
      @header.es5506NumChannels = 0 if doffset <= 0xD5
      @header.es5503NumChannels = 0 if doffset <= 0xD4
      @header.es5506Clock = 0 if doffset <= 0xD0
      @header.es5503Clock = 0 if doffset <= 0xCC
      @header.saa1099Clock = 0 if doffset <= 0xC8
      @header.vsuClock = 0 if doffset <= 0xC4
      @header.wonderswanClock = 0 if doffset <= 0xC0
      @header.extraHeaderOffset = 0 if doffset <= 0xBC
      @header.scspClock = 0 if doffset <= 0xB8
      @header.qsoundClock = 0 if doffset <= 0xB4
      @header.pokeyClock = 0 if doffset <= 0xB0
      @header.k053260Clock = 0 if doffset <= 0xAC
      @header.c140Clock = 0 if doffset <= 0xA8
      @header.huc6280Clock = 0 if doffset <= 0xA4
      @header.k054539Clock = 0 if doffset <= 0xA0
      @header.k051649Clock = 0 if doffset <= 0x9C
      @header.okim6295Clock = 0 if doffset <= 0x98
      @header.reservedFlags = 0 if doffset <= 0x97
      @header.c140ChipType = 0 if doffset <= 0x96
      @header.k054539Flags = 0 if doffset <= 0x95
      @header.okim6258Flags = 0 if doffset <= 0x94
      @header.okim6258Clock = 0 if doffset <= 0x90
      @header.upd7759Clock = 0 if doffset <= 0x8C
      @header.multiPcmClock = 0 if doffset <= 0x88
      @header.nesApuClock = 0 if doffset <= 0x84
      @header.dmgClock = 0 if doffset <= 0x80
      @header.loopModifier = 0 if doffset <= 0x7F
      @header.loopBase = 0 if doffset <= 0x7E
      @header.reserved2 = 0 if doffset <= 0x7D
      @header.volumeModifier = 0 if doffset <= 0x7C
      @header.ayYm2608Flags = 0 if doffset <= 0x7B
      @header.ayYm2203Flags = 0 if doffset <= 0x7A
      @header.ay8910Flags = 0 if doffset <= 0x79
      @header.ay8910ChipType = 0 if doffset <= 0x78
      @header.ay8910Clock = 0 if doffset <= 0x74
      @header.pwmClock = 0 if doffset <= 0x70
      @header.rf5c164Clock = 0 if doffset <= 0x6C
      @header.ymz280bClock = 0 if doffset <= 0x68
      @header.ymf271Clock = 0 if doffset <= 0x64
      @header.ymf278bClock = 0 if doffset <= 0x60
      @header.ymf262Clock = 0 if doffset <= 0x5C
      @header.y8950Clock = 0 if doffset <= 0x58
      @header.ym3526Clock = 0 if doffset <= 0x54
      @header.ym3812Clock = 0 if doffset <= 0x50
      @header.ym2610Clock = 0 if doffset <= 0x4C
      @header.ym2608Clock = 0 if doffset <= 0x48
      @header.ym2203Clock = 0 if doffset <= 0x44
      @header.rf5c68Clock = 0 if doffset <= 0x40
    end

    # Converts a playback sample index number into a VGM sample index number.
    # That is, this converts a PCM sample position into a VGM sample position.
    @[AlwaysInline]
    def self.pcmSampleToVgmSample(sampleNum : Int64, sampleRateDiv : Int64, sampleRateMul : Int64) : UInt32
      (sampleNum * sampleRateDiv).tdiv(sampleRateMul).to_u32!
    end

    # Converts a VGM sample index number into a playback sample index number.
    # That is, this converts a VGM sample position into a PCM sample position.
    @[AlwaysInline]
    def self.vgmSampleToPcmSample(sampleNum : Int64, sampleRateDiv : Int64, sampleRateMul : Int64) : UInt32
      (sampleNum * sampleRateMul).tdiv(sampleRateDiv).to_u32!
    end
  end
end
