#### YunoSynth
#### Copyright (C) 2023-2024 Remilia Scarlet <remilia@posteo.jp>
#### Portions are Copyright (C) Valley Bell
####
#### This program is free software: you can redistribute it and/or
#### modify it under the terms of the GNU Affero General Public
#### License as published by the Free Software Foundation, either
#### version 3 of the License, or (at your option) any later version.
####
#### This program is distributed in the hope that it will be useful,
#### but WITHOUT ANY WARRANTY; without even the implied warranty of
#### MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#### Affero General Public License for more details.
####
#### You should have received a copy of the GNU Affero General Public License
#### along with this program.  If not, see <https://www.gnu.org/licenses/>.
require "./common"
require "./resampler"

####
#### Abstract Chips
####
#### All chip interfaces must inherit from AbstractChip.  A chip interface is
#### the public-facing interface to some internal emulation core (which there
#### may be more than one for a given type of chip).
####
#### Please see adding-chips.md in this repository for more information.
####

module Yuno
  # Raised when a chip is not supported by the current version of the library.
  class UnsupportedChipError < YunoError
  end

  # :nodoc:
  # A hash that maps a `ChipType` to an array of chip instances.
  alias ChipTable = Hash(ChipType, Array(AbstractChip))

  # :nodoc:
  alias SampleRateCallback = Proc(AbstractChip, UInt32, Nil)

  # :nodoc:
  # The base class that all emulator cores inherit from.
  abstract class AbstractEmulator
    # Fills `outputs` with `samples` new PCM data samples.
    abstract def update(outputs : OutputBuffers, samples : UInt32) : Nil
  end

  # Base class for all chip implementations.
  abstract class AbstractChip
    # Holds a set of flags for a chip.  Not all chips use this class.
    abstract class ChipFlags
    end

    # The sample rate for this chip.
    property sampleRate : UInt32 = 0

    # The sample rate of the VGM player that will use this chip.
    getter playerSampleRate : UInt32 = 0

    # The output volume of the chip.
    property volume : UInt16 = 0

    # What kind of resampling should be used for this chip.
    property resamplerType : Resampler::ResamplerType = Resampler::ResamplerType::Copy

    # The index of the current sample.
    protected property curSampleNum : UInt32 = 0

    # The index of the previous sample.
    protected property lastSampleNum : UInt32 = 0

    # The index of the next sample.
    protected property nextSampleNum : UInt32 = 0

    # The previous sample that was output.
    protected property lastSample : Sample(Int32) = Sample(Int32).new(0, 0)

    # The next sample for output.
    protected property nextSample : Sample(Int32) = Sample(Int32).new(0, 0)

    # Any extra "internal" chip associated with this chip.  For example, a
    # YM2203 has its own YM2149F embedded in it.
    protected property paired : AbstractChip? = nil

    # When `true`, then this chip is the paired chip for another chip.
    protected property? isPaired : Bool = false

    # The function that is called when something needs more audio data rendered.
    protected property updateFn : Proc(OutputBuffers, UInt32, Nil)?

    # Creates a new instance of this chip.
    abstract def initialize(chipNum : Int32, absChipCount : Int32,  vgm : VgmFile, playbackSampleRate : UInt32,
                            newSamplingMode : UInt8, newPlayerSampleRate : UInt32, *, emuCore : Symbol? = nil,
                            flags : ChipFlags? = nil)

    # Returns the `ChipType` for this chip.
    abstract def type : ChipType

    # Returns the human-readable name for this chip.
    abstract def name : String

    # Returns a short version of the human-readable name for this chip.
    abstract def shortName : String

    # A numeric ID that is used to identify the chip.  This is used internally
    # by VGM files.
    abstract def id : UInt32

    # Returns a symbol representing the emulation core that this chip is set up to use.
    abstract def emuCore : Symbol

    # Returns the base volume for this chip.  This is the value used by
    # `VgmFile#getChipVolume` to calculate the appropriate output volume for a
    # chip.
    abstract def baseVolume : UInt16

    # :nodoc:
    # The instance of this `AbstractEmulator`.
    abstract def chip : AbstractEmulator

    # Returns a symbol representing the default emulation core that this chip is set up to use.
    def self.defaultEmuCore : Symbol
      raise NotImplementedError.new("#defaultEmuCore not implemented for this type")
    end

    # Starts this chip, setting the clock rate to the given value.  Not all
    # chips use the `flags` or `previousChip` parameters.
    abstract def start(chipIndex : UInt8, clock : UInt32, flags : ChipFlags? = nil,
                       previousChip : AbstractChip? = nil) : UInt32

    # Gets the correct clock value for this chip using the given `VgmFile`.
    abstract def getClock : UInt32

    # Gets a set of flags, if any, that are needed to call the `#start` method.
    def getStartFlags(vgm : VgmFile) : ChipFlags?
      nil
    end

    # Updates the chip's internal state, then writes the new audio data to `outputs`.
    abstract def update(outputs : Array(Slice(Int32)), samples : Int) : Nil

    # Renders PCM data from the paired chip to `outputs`.  `samples` is the
    # number of samples to render.
    def updatePaired(outputs : OutputBuffers, samples : UInt32) : Nil
    end

    # Resets this chip to its initial state.
    abstract def reset(chipIndex : UInt8) : Nil

    # Reads a value from the chip at the given memory offset.
    abstract def read(chipIndex : UInt8, offset : Int) : UInt8|UInt16|UInt32

    # Writes a value to the given memory offset for this chip.  Not all chips
    # use the `port` parameter.
    abstract def write(chipIndex : UInt8, offset : Int, data : Int, port : UInt8 = 0) : Nil

    # Sets the mute mask for this chip.
    abstract def setMuteMask(chipIndex : UInt8, mask : UInt32) : Nil

    # Writes DAC commands to this chip.
    def writeDac(port : Int, command : Int, data : Int) : Nil
      write(0u8, command, data, port.to_u8!)
    end

    # Sets the stereo mask for this chip.  Not all chips may implement this.
    def setStereoMask(chipIndex : UInt8, mask : UInt32) : Nil
      raise NotImplementedError.new("#setStereoMask not implemented for this type")
    end

    # Gets a volume modification value for this chip.  This is used because not
    # all emulators output at the same level.
    def getVolModifier : UInt32
      0_u32
    end

    # :nodoc:
    # Changes the sample rate for *chip* to *newSampleRate*.
    #
    # This is based on code from VGMPlay.
    protected def self.changeChipSampleRate(chip : AbstractChip, newSampleRate : UInt32) : Nil
      {% if flag?(:yunosynth_debug) %}
        Yuno.dlog!("Sample rate change for #{chip.class}: #{chip.sampleRate} => #{newSampleRate}")
      {% end %}
      return if chip.sampleRate == newSampleRate

      chip.sampleRate = newSampleRate

      # Adjust the resampler type by comparing the new sample rate to the
      # playback rate.
      if chip.sampleRate < chip.playerSampleRate
        chip.resamplerType = Resampler::ResamplerType::Upsampling
      elsif chip.sampleRate > chip.playerSampleRate
        chip.resamplerType = Resampler::ResamplerType::Downsampling
      else
        chip.resamplerType = Resampler::ResamplerType::Copy
      end

      # Reset things that the resampler uses.
      chip.curSampleNum = 1
      chip.nextSampleNum -= chip.lastSampleNum
      chip.lastSampleNum = 0
    end
  end

  # Loops over all of the chips in the `ChipTable`, yielding an `AbstractChip`
  # and an `Int32` that is the index of that chip for that `ChipType`.
  def self.eachChip(table : ChipTable, &) : Nil
    table.each_value do |val|
      val.each_with_index do |chip, idx|
        yield chip, idx
      end
    end
  end
end
