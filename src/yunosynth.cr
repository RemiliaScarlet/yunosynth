#### YunoSynth
#### Copyright (C) 2023-2024 Remilia Scarlet <remilia@posteo.jp>
####
#### This program is free software: you can redistribute it and/or
#### modify it under the terms of the GNU Affero General Public
#### License as published by the Free Software Foundation, either
#### version 3 of the License, or (at your option) any later version.
####
#### This program is distributed in the hope that it will be useful,
#### but WITHOUT ANY WARRANTY; without even the implied warranty of
#### MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#### Affero General Public License for more details.
####
#### You should have received a copy of the GNU Affero General Public License
#### along with this program.  If not, see <https://www.gnu.org/licenses/>.
require "libremiliacr"
require "./yunosynth/*"
require "./yunosynth/chips/*"

# YunoSynth is a high-performance [VGM](https://vgmrips.net/) playback library
# written entirely in the Crystal programming language.  The goal is to provide
# native VGM playback in Crystal without bindings and almost no dependencies, a
# cleaned-up version of VGMPlay's code, and performance on par with VGMPlay.
module Yuno
  # The current version of the library.
  VERSION = "0.5.2"
end
