require "rake/clean"

VGMINFO_BIN = "vgminfo"
PULSE_PLAYER_BIN = "pulse-player"
SHARD = "shard.yml"
SHARD_LOCK = "shard.lock"
LIB_DIR = "./lib"

SOURCES = Rake::FileList["src/*.cr", "src/**/*.cr"]
VGMINFO_SRC = Rake::FileList["examples/vgminfo.cr"]
PULSE_PLAYER_SRC = Rake::FileList["examples/pulse-player.cr"]
SPEC_SOURCES = Rake::FileList["spec/*.cr"]

OPT_MODE_SUPER_DEBUG = "super-debug"
OPT_MODE_DEBUG = "debug"
OPT_MODE_NORMAL = "normal"
OPT_MODE_FAST = "speed"

file LIB_DIR => SHARD do |t|
  sh "shards install"
end

file SHARD_LOCK => SHARD do |t|
  sh "shards install"
end
CLOBBER << SHARD_LOCK
CLOBBER << LIB_DIR

def buildBin(t, args, mainSrc)
  args.with_defaults(:optMode => OPT_MODE_DEBUG)
  cmdLine = "crystal build -p"

  case args[:optMode].downcase
  when OPT_MODE_SUPER_DEBUG
    puts "=== Verbose Debug Build: #{t.name} ==="
    cmdLine += " --debug -Dyunosynth_debug"
  when OPT_MODE_DEBUG
    puts "=== Debug Build: #{t.name} ==="
    cmdLine += " --debug"
  when OPT_MODE_NORMAL
    puts "=== Normal Build: #{t.name} ==="
    cmdLine += " --release"
  when OPT_MODE_FAST
    puts "=== Fully Optimized Build: #{t.name} ==="
    cmdLine += " --release --no-debug -Dyunosynth_wd40"
  when "list", "help"
    puts "=== optMode Choices ===
* super-debug: Produce a debug build + lots of extra output.
* debug:       Produce a debug build.
* normal:      Produce an optimized build with minimal debug info.
* speed:       The default.  Produce a fully optimized binary with the
               '-Dyunosynth_wd40' flag, and with all debug information
               stripped."
    exit 0
  else
    puts "Unknown optMode: #{args[:optMode]}"
    exit 1
  end

  cmdLine += " -Dstrict_multi_assign -Dno_number_autocast"
  cmdLine += " #{mainSrc}"
  sh cmdLine
end

file VGMINFO_BIN, [:optMode] => SOURCES + VGMINFO_SRC + [SHARD, SHARD_LOCK] do |t, args|
  buildBin(t, args, VGMINFO_SRC)
end
CLEAN << VGMINFO_BIN

file PULSE_PLAYER_BIN, [:optMode] => SOURCES + PULSE_PLAYER_SRC + [SHARD, SHARD_LOCK] do |t, args|
  buildBin(t, args, PULSE_PLAYER_SRC)
end
CLEAN << PULSE_PLAYER_BIN

################################################################################

task :default => [:deps, :examples]

desc "Show target information"
task :help do
  puts "The following targets are available:

* examples[:optMode]
  This target builds the example programs.

  If optMode is 'super-debug', then this produces a build with full debug info,
  plus enables a bunch of debug output messages.

  If optMode is 'debug', then this produces a build with full debug info, but
  without a lot of debug messages.

  If optMode is 'normal', then this produces a build with the '--release'
  option.  The binaries include some debug info.

  If optMode is 'speed' (the default), then this produces a build with the both
  the '--release' option and '-Dyunosynth_wd40' flag.  The binaries will have
  all debug information stripped.

  You can use 'list' or 'help' to get a listing of these.

* help
  I think you just found this target ^_~

* deps
  Runs 'shards update' to update local dependencies.

* clean
  Removes the generated binary and uncompressed man page.

* clobber
  Same as clean, but also removes the lib directory, shards.lock, and
  compressed man page."
end

desc "Build Examples"
task :examples, [:optMode] => [VGMINFO_BIN, PULSE_PLAYER_BIN]

desc "Update Shards"
task :deps => [SHARD_LOCK, LIB_DIR]

desc "Run Ameba"
task :lint do |_|
  sh "ameba"
end

desc "Build Docs"
task :docs do |_|
  sh "rm -rf docs"
  sh "crystal docs"
end

desc "Run unit tests"
task :test do |_|
  sh "crystal spec -p"
end
