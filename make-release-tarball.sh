#!/bin/bash

set -e

if [[ -z "$1" ]]; then
    echo "Usage: `basename "$0"` <version without v>"
    exit 1
fi

fossil tarball v$1 yunosynth-$1.tar.gz --name yunosynth-$1
gunzip yunosynth-$1.tar.gz
bzip2 -9 yunosynth-$1.tar
tar tvjf yunosynth-$1.tar.bz2

echo "SHA256: $(sha256sum yunosynth-$1.tar.bz2 | cut -d ' ' -f 1)"
echo "MD5: $(md5sum yunosynth-$1.tar.bz2 | cut -d ' ' -f 1)"
echo "Size: $(stat -c '%s' yunosynth-$1.tar.bz2) bytes"
echo "Created yunosynth-$1.tar.bz2"
